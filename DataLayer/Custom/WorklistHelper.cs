﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer
{
    [Serializable]
    public class WorklistHelper
    {
        public string Folio { get; set; }
        public string SN { get; set; }
        public string FullName { get; set; }
        public DateTime StartDate { get; set; }
        public string UserID { get; set; }
    }

    [Serializable]
    public class WorklistData
    {
        public string Folio { get; set; }
        public string SN { get; set; }
        public string DocStatus { get; set; }
        public string DocNumber { get; set; }
        public string DocYear { get; set; }
        public string WorklistTme { get; set; }
        public DateTime DocDate { get; set; } 
        public int DocAgeSeconds { get; set; }
        public string CreatedDate { get; set; }
        public string DivName { get; set; }
        public string UserID { get; set; }
        public string Description { get; set; }
        public int Notices { get; set; }
        public DateTime? PaidDate { get; set; }
        public DateTime? ReceivedDate { get; set; } 
        public int? HoldFlag { get; set; }
        public string HypValue { get; set; }
        public decimal? TotalAmount { get; set; }
        public int NeedReply { get; set; }
        public int Rejected { get; set; }
        public int? STATUS_CD { get; set; }
        public string StatusName { get; set; }
        public string PvTypeName { get; set; }
        public string ActivityDateFrom { get; set; }
        public string ActivityDateTo { get; set; }
        public string PayMethodName { get; set; }
        public string BudgetNo { get; set; }
        public bool WorklistNotice { get; set; }
    }

    [Serializable]
    public class ApproveRejectData
    {
        public string Folio { get; set; }
        public string StrDocNo { get; set; }
        public string StrDocYear { get; set; }
        public int IntDocNo { get; set; }
        public int IntDocYear { get; set; }
    }
}
