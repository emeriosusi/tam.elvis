﻿using System;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Diagnostics;

namespace DataLayer.Model
{
    public class ContextWrap: IDisposable
    {
        protected ELVIS_DBEntities edb = null;
        protected EntityConnection ece = null;

        public ELVIS_DBEntities db
        {
            get
            {
                return DBElvis();
            }
            set
            {
                edb = value;
                if (edb != null)
                {
                    ece = (EntityConnection)edb.Connection;
                }
            }
        }

        public ELVIS_DBEntities DBElvis(EntityConnection co = null)
        {
            if (edb == null)
            {
                if (co == null)
                {
                    ece = new EntityConnection("name=ELVIS_DBEntities");
                }
                else
                {
                    ece = co;
                }
                if (ece.State == ConnectionState.Closed)
                {
                    ece.Open();
                }
                edb = new ELVIS_DBEntities(ece);
            }
            return edb;
        }

        protected IDbConnection _DB = null;
        public IDbConnection DB 
        {
            get
            {
                if (_DB == null)
                {
                    _DB = new SqlConnection(ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString);
                }
                if ((_DB.State == ConnectionState.Closed) || (_DB.State == ConnectionState.Broken))
                    _DB.Open();
                return _DB;
            }
        }

        private static void Disposal(EntityConnection e, ObjectContext db)
        {
            if (db != null)
            {
                if (db.Connection != null && db.Connection.State == ConnectionState.Open)
                {
                    db.Connection.Close();
                }
                db.Dispose();
                db = null;
                try
                {
                    e.Dispose();
                }
                catch (ObjectDisposedException ode)
                {
                    Debug.WriteLine(ode.Message);
                    e = null;
                }
            }
        }

        private static void Disposal(IDbConnection x)
        {
            if (x != null && x.State == ConnectionState.Open)
            {
                x.Close();
            }
        }

        public void Dispose()
        {
            Disposal(ece, edb);
            Disposal(DB);
            _DB = null;
        }
    }
}
