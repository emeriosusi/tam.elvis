﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Function;
using System.IO;

namespace ELVIS_EmailNotice
{
    class EmailNotice
    {
        static void Main(string[] args)
        {
            EmailFunction WorklistReminder = new EmailFunction("WorklistReminderTemplate.txt");
            WorklistReminder.Log("BEGIN");
            if (!WorklistReminder.isHoliday())
            {
                WorklistReminder.SendEmailReminder();
                WorklistReminder.Log("END.");
            }
            else
                WorklistReminder.Log("ERR\t{0}\t{1}", "", "NO EMAIL SENT ON HOLIDAY.");

        }
    }
}
