﻿using System;
using System.Net.Mail;
using System.IO;
using DataLayer.Model;
using DataLayer;
using Common.Data;
using Common;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.Objects;
using System.Threading;
using K2.Helper;
using Dapper;
using System.Configuration;

namespace ELVIS_EmailNotice
{
    class EmailFunction
    {
        private string K2ProjectName = "ELVISWorkflow";
        private string SMTPSERVER;
        private string MailPath;
        private DateTime lastTick = new DateTime(1900, 1, 1);
        private string MailTo = "";

        ELVIS_DBEntities _db = new ELVIS_DBEntities();

        private string path, name;

        private IDbConnection _DB = null;
        public IDbConnection DB
        {
            get
            {
                if (_DB == null)
                {
                    _DB = new SqlConnection(ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString);
                }
                return _DB;
            }
        }

        public string WorkPath
        {
            get
            {
                return path;
            }
        }

        public static string GetPath()
        {
            DateTime n = DateTime.Now;
            return Path.Combine(Environment.GetEnvironmentVariable("ALLUSERSPROFILE")
                , AppSetting.LdapDomain, AppSetting.ApplicationID, "log"
                , n.Year.ToString()
                , n.Month.ToString()
                , n.Day.ToString());
        }

        public static bool ForceDirectories(string d)
        {
            bool r = false;
            try
            {
                if (!Directory.Exists(d))
                {
                    Directory.CreateDirectory(d);
                }
                r = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return r;
        }

        public string GetSysText(string stype, string scode = "")
        {
            string r = "";
            try
            {
                string sql = string.Format("SELECT TOP 1 SYSTEM_VALUE_TXT "
                                         + " FROM dbo.TB_M_SYSTEM "
                                         + " WHERE SYSTEM_TYPE='{0}' "
                                         + " AND (NULLIF('{1}','') IS NULL OR SYSTEM_CD='{1}')", stype, scode);
                var q = DB.Query<string>(sql).ToList();
                if (q != null && q.Count > 0)
                {
                    r = q[0];
                }
            }
            catch (Exception ex)
            {
                Log("{0}", ex.Message + " " + Environment.NewLine + ex.StackTrace);
                r = null;
            }
            return r;
        }

        public EmailFunction(string mailPath)
        {
            path = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            this.MailPath = Path.Combine(path, mailPath);
            this.SMTPSERVER = GetSysText(AppSetting.SMTP_SERVER_FIELD_NAME);
            this.MailTo = GetSysText("EMAIL_NOTICE", "EML_TO");
            string logPath = GetPath();
            ForceDirectories(logPath);
            name = Path.Combine(logPath, "Email_Notice.txt");
            ReMoveFile(name);
        }

        public bool isHoliday()
        {
            bool r = DB.Query<System.Boolean>(
                string.Format("SELECT ELVIS_DB.dbo.fn_IsHoliday('{0}')"
                    , DateTime.Now.ToString("yyyy-MM-dd"))).FirstOrDefault();

            return r;
        }

        public bool GetUserPosDiv(string userName, ref string positionId, List<string> divs)
        {
            var q = (from u in _db.vw_User
                     where u.USERNAME == userName
                     select u);

            if (q.Any())
            {
                q.Select(a => a.DIVISION_ID)
                    .Distinct()
                    .ToList()
                    .ForEach(l => divs.Add(l));

                positionId = q.OrderBy(b => b.CLASS)
                    .Select(a => a.POSITION_ID)
                    .FirstOrDefault();
                Log("user {0} DIV: {1} POSITION: {2}"
                        , userName
                        , string.Join(";", divs.ToArray())
                        , positionId);
                return true;
            }
            else
            {
                divs.Clear();
                positionId = null;
            }
            return false;
        }

        public List<UserData> Sec(List<string> divs)
        {
            List<string> secretaryDivisions = new List<string>();

            if (AppSetting.FilterSecretaryDivision)
            {
                for (int i = 0; i < divs.Count - 1; i++)
                {
                    int a = 0;
                    if (int.TryParse(divs[i], out a) && (a > 100))
                        secretaryDivisions.Add(divs[i]);
                }
            }
            else
            {
                foreach (string d in divs)
                    secretaryDivisions.Add(d);
            }
            var q = (from u in _db.vw_User
                     where u.ROLE_ID == "ELVIS_SECRETARY"
                     select u).Where(x => secretaryDivisions.Contains(x.DIVISION_ID));

            List<UserData> ud = new List<UserData>();
            if (q.Any())
            {
                q.ToList().ForEach(se => ud.Add(new UserData()
                {
                    FIRST_NAME = se.FIRST_NAME,
                    LAST_NAME = se.LAST_NAME,
                    EMAIL = se.EMAIL,
                    USERNAME = se.USERNAME
                }));
            }
            StringBuilder sb = new StringBuilder("");
            sb.Append("\tSecretary for divison: ");
            foreach (string d in secretaryDivisions)
            {
                sb.Append(d + "\t");
            }
            sb.Append("\r\n");
            for (int i = 0; i < ud.Count; i++)
            {
                sb.AppendLine("\t\t\t\t\t" + ud[i].FIRST_NAME + " " + ud[i].LAST_NAME + " " + ud[i].EMAIL);
            }
            Log(sb.ToString(), "");
            return ud;
        }

        public void SendEmailReminder()
        {
            List<UserData> u = (from x in _db.vw_User
                                where x.USERNAME != null
                                   && x.ROLE_ID != null
                                   && x.ROLE_ID.StartsWith("ELVIS")
                                select new
                                    {
                                        x.USERNAME
                                    ,
                                        x.EMAIL
                                    ,
                                        x.FIRST_NAME
                                    ,
                                        x.LAST_NAME
                                    })
                                .Distinct()
                                .Select(z => new UserData()
                                {
                                    USERNAME = z.USERNAME,
                                    EMAIL = z.EMAIL,
                                    FIRST_NAME = z.FIRST_NAME,
                                    LAST_NAME = z.LAST_NAME
                                }).ToList();

            int j = 0;
            foreach (UserData user in u)
            {
                List<WorklistHelper> Worklist = getUserWorklist(user.USERNAME);
                int TotalWorklist = Worklist.Count();
                int TotalNotice = 0;

                //check if user has Worklist or Notice
                if (TotalWorklist != 0 || TotalNotice != 0)
                {
                    DateTime prevDay;
                    DateTime nextDay;
                    var oOut = new ObjectParameter("workstart", typeof(DateTime));
                    _db.GetWorkStart(DateTime.Now, oOut);
                    nextDay = Convert.ToDateTime(oOut.Value);

                    var oPre = new ObjectParameter("PrevDay", typeof(DateTime));
                    _db.GetPrevWorkingDay(DateTime.Now, oPre);
                    prevDay = Convert.ToDateTime(oPre.Value);

                    int NewWorklist = Worklist
                            .Where(i => i.StartDate.Date >= prevDay.Date && i.StartDate <= nextDay.Date)
                            .Select(i => i)
                            .Count();
                    ComposeWorklistReminder(TotalWorklist, NewWorklist, TotalNotice, user);

                    if (j % 3 == 0)
                    {
                        //sleep after sending 5 emails
                        if (AppSetting.SendEmail)
                            Thread.Sleep(AppSetting.MailWaitInterval);
                    }
                    j++;
                }
            }
        }

        public bool ComposeWorklistReminder(int _worklist, int _newWorklist, int _notice, UserData x)
        {
            bool code = false;

            try
            {
                MailMessage MailMessage = new MailMessage();
                // x.EMAIL = UserDataDetail.Where(i => i.USERNAME == x.USERNAME _username).Select(i => i.EMAIL).FirstOrDefault().ToString();
                Regex r = new Regex("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*");
                if (string.IsNullOrEmpty(x.EMAIL) || !r.IsMatch(x.EMAIL))
                {
                    Log("ERR: User {0} Invalid mail address : {1}", x.USERNAME, x.EMAIL);
                    return code;
                }

                MailAddress MailAddress = new MailAddress(x.EMAIL);
                MailMessage.To.Add(MailAddress);

                #region Body
                TextReader _textReader = new StreamReader(MailPath);
                string MailBody = _textReader.ReadToEnd();
                if (MailBody.Contains("[DESTINATION_USER]"))
                {

                    string FullName = (x.FIRST_NAME ?? "") + " " + (x.LAST_NAME ?? "");

                    MailBody = MailBody.Replace("[DESTINATION_USER]", FullName);

                }
                if (MailBody.Contains("[WORKLIST]"))
                {
                    MailBody = MailBody.Replace("[WORKLIST]", _worklist.ToString());
                }
                if (MailBody.Contains("[NEW]"))
                {
                    if (_newWorklist != 0)
                    {
                        MailBody = MailBody.Replace("[NEW]", "(" + _newWorklist.ToString() + " new)");
                    }
                    else
                    {
                        MailBody = MailBody.Replace("[NEW]", "");
                    }
                }
                if (MailBody.Contains("[NOTICE]"))
                {
                    MailBody = MailBody.Replace("[NOTICE]", _notice.ToString());
                }
                MailMessage.IsBodyHtml = true;
                MailMessage.Body = MailBody;
                #endregion

                #region subject
                MailMessage.Subject = "[ELVIS] Worklist to be approved";
                #endregion

                string posId = "";

                List<string> divs = new List<string>();
                List<UserData> myDear = new List<UserData>();
                if (GetUserPosDiv(x.USERNAME, ref posId, divs) && posId.Equals("DIR"))
                {
                    myDear = Sec(divs);
                }

                #region sender
                MailAddress _Sender = new MailAddress("elvis.admin-noreply@toyota.co.id");
                MailMessage.Sender = _Sender;
                MailMessage.From = _Sender;

                foreach (UserData b in myDear)
                {
                    MailMessage.CC.Add(b.EMAIL);
                }
                #endregion

                #region doSend
                SmtpClient client = new SmtpClient(SMTPSERVER);
                string xsent = "SENT";
                if (AppSetting.SendEmail)
                    client.Send(MailMessage);
                else
                {
                    xsent = "    ";
                }
                code = true;
                Log(xsent + " \"{1}\" <{0}>", x.EMAIL, x.USERNAME);
                if (myDear != null && myDear.Count > 0)
                {
                    Log("\tCC " + xsent + " TO: ");
                    foreach (UserData u in myDear)
                    {
                        Log("\t\t\t\"{0} {1}\" <{2}>", u.FIRST_NAME, u.LAST_NAME, u.EMAIL);
                    }
                }
                #endregion
            }
            catch (Exception Ex)
            {
                code = false;
                Log("FAIL SEND:{0} \r\n{1}\r\n{2}", x.EMAIL, Ex.Message, Ex.StackTrace);
            }
            return code;

        }

        public string tick(string s)
        {
            string t = "        \t";
            if (s != null && s.IndexOf(".") == s.Length - 1)
                lastTick = DateTime.Now.AddSeconds(-1);

            if ((lastTick - DateTime.Now).Seconds != 0)
            {
                lastTick = DateTime.Now;
                t = lastTick.ToString("HH:mm:ss") + "\t";
            }
            return t + s;
        }

        public static void ReMoveFile(string n)
        {
            if (File.Exists(n))
            {
                int i = 0;
                string newname;
                do
                {
                    string datum = File.GetCreationTime(n).ToString("yyyyMMdd");
                    string ss = "_" + datum + "_" + Base26(Convert.ToInt32(Math.Round(DateTime.Now.TimeOfDay.TotalSeconds)) + i++);
                    newname = Path.Combine(Path.GetDirectoryName(n),
                        Path.GetFileNameWithoutExtension(n)
                        + ss +
                        Path.GetExtension(n));
                } while (File.Exists(newname));
                File.Move(n, newname);
            }
        }

        public static string Base26(int i)
        {
            string x = "";
            int n;
            while (i > 0)
            {
                n = (i - 1) % 26;
                x = Convert.ToChar(n + 65).ToString() + x;
                i = (i - n) / 26;
            }
            return x;
        }

        protected List<WorklistHelper> getUserWorklist(string _UserName)
        {
            var _K2Helper = new K2Helper(
                    Common.AppSetting.K2_SERVER,
                    Common.AppSetting.K2_USER,
                    Common.AppSetting.K2_PASS,
                    Common.AppSetting.K2_DOMAIN,
                    Common.AppSetting.K2_LABEL,
                    Convert.ToUInt32(Common.AppSetting.K2_WORKFLOW_PORT),
                    Convert.ToUInt32(Common.AppSetting.K2_SMO_PORT));

            _K2Helper.Open();

            var lstAllWorklist = new List<WorklistHelper>();
            var lstOrigWorklist = _K2Helper.GetWorklistComplete(_UserName);
            var lstGrantor = GetGrantorByAttorney(_UserName, false);

            string _ProjectName = K2ProjectName;

            foreach (var kvp in lstOrigWorklist)
            {
                if (kvp.FullName.Contains(_ProjectName))
                {
                    WorklistHelper _WorklistHelper = new WorklistHelper();
                    _WorklistHelper.Folio = kvp.Folio;
                    _WorklistHelper.SN = kvp.SN;
                    _WorklistHelper.StartDate = kvp.StartDate;
                    _WorklistHelper.FullName = kvp.FullName;
                    _WorklistHelper.UserID = kvp.UserID;

                    lstAllWorklist.Add(_WorklistHelper);
                }
            }

            if (lstGrantor != null)
            {
                foreach (var item in lstGrantor)
                {
                    var grantorWorklist = _K2Helper.GetWorklistComplete(item.GRANTOR);

                    foreach (var kvp in grantorWorklist)
                    {
                        if (kvp.FullName.Contains(_ProjectName))
                        {
                            WorklistHelper _WorklistHelper = new WorklistHelper();
                            _WorklistHelper.Folio = kvp.Folio;
                            _WorklistHelper.SN = kvp.SN;
                            _WorklistHelper.StartDate = kvp.StartDate;
                            _WorklistHelper.FullName = kvp.FullName;
                            _WorklistHelper.UserID = kvp.UserID;

                            lstAllWorklist.Add(_WorklistHelper);
                        }
                    }
                }
            }
            _K2Helper.Close();

            return lstAllWorklist;
        }

        #region Create Log


        public void Log(string w, params object[] x)
        {

            try
            {
                if (x != null)
                {
                    w = string.Format(w, x);
                }
                File.AppendAllText(name, tick(w) + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace + ex.InnerException ?? "");
            }

        }

        #endregion

        #region Get Grantor By Attorney
        public List<TB_R_POA> GetGrantorByAttorney(string attorney, bool isAvailable)
        {
            try
            {
                var baseQuery = from poa in _db.TB_R_POA
                                where poa.ATTORNEY == attorney && poa.OFFICE_STATUS == isAvailable
                                select poa;

                if (baseQuery.Any()) return baseQuery.ToList();
            }
            catch (Exception ex)
            {
                Log("ERR {0}\r\n{1}", ex.Message, ex.StackTrace);
            }

            return null;
        }
        #endregion
    }
}
