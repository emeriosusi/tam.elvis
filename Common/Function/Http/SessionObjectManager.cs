﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.SessionState;

namespace Common.Http
{
    public class SessionObjectManager
    {        
        private HttpSessionState session;
        private string __keyPrefix = "_SOM_";
        private List<string> lstKeys;

        public SessionObjectManager(string prefix, HttpSessionState session)
        {       
            Prefix = prefix;
            this.session = session;
            this.lstKeys = new List<string>();
        }

        public string Prefix { set; get; }

        public void add(Type type, object sessionObject)
        {
            add(type.AssemblyQualifiedName, sessionObject);            
        }

        public void add(string key, object sessionObject)
        {
            string sessionKey = getSessionKey(key);
            object persistedObject = session[sessionKey];
            if (persistedObject != null)
            {
                session.Remove(sessionKey);
                lstKeys.Remove(sessionKey);
            }
            session[sessionKey] = sessionObject;
            lstKeys.Add(sessionKey);
        }

        public object get(Type type)
        {
            return get(type.AssemblyQualifiedName);
        }

        public object get(string key)
        {
            return session[getSessionKey(key)];
        }

        public void remove(Type type)
        {
            remove(type.AssemblyQualifiedName);
        }

        public void remove(string key)
        {
            session.Remove(getSessionKey(key));
        }

        public void cleanUp()
        {            
            int cntKey = lstKeys.Count;
            for(int i = 0; i < cntKey; i++)
            {
                session.Remove(lstKeys[i]);
            }
            lstKeys.Clear();
        }

        private string getSessionKey(string key)
        {
            return Prefix + __keyPrefix + key;
        }
    }
}
