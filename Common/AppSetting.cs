﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;
namespace Common
{
    public static class AppSetting
    {
        public static decimal ReadDecimal(string setting, decimal value = 0)
        {
            string b = ConfigurationManager.AppSettings[setting];
            if (string.IsNullOrEmpty(b))
                return value;
            else
            {
                decimal x = value;
                if (decimal.TryParse(b, out x))
                    return x;
                else
                    return value;
            }
        }

        public static int ReadInt(string setting, int value = 0)
        {
            string b = ConfigurationManager.AppSettings[setting];
            if (string.IsNullOrEmpty(b))
                return value;
            else
            {
                int x = value;
                if (Int32.TryParse(b, out x))
                    return x;
                else
                    return value;
            }
        }

        public static string Read(string setting, string value = "")
        {
            string b = ConfigurationManager.AppSettings[setting];
            if (string.IsNullOrEmpty(b))
                return value;
            else
            {
                return b;
            }
        }

        public static object ReadObject(string name, string pType)
        {
            object o = null;

            if ("String".Equals(pType, StringComparison.InvariantCultureIgnoreCase))
            {
                o = Read(name, null);
            }
            else if (pType.StartsWith("Int", StringComparison.InvariantCultureIgnoreCase))
            {
                o = ReadInt(name);
            }
            else if (pType.StartsWith("Bool", StringComparison.InvariantCultureIgnoreCase))
            {
                string bv = Read(name);

                bool r = false;
                if (string.IsNullOrEmpty(bv))
                {
                    o = null;
                } else {
                    r = bv.BooleanValue();
                    o = r;
                }
            }

            return o;
        }

        public static string GetNamespace()
        {
            string baseName = null;

            Assembly a = Assembly.GetEntryAssembly();
            if (a == null)
            {
                StackTrace t = new StackTrace(1, true);

                StackFrame[] f = t.GetFrames();

                if (f != null && f.Length > 0)
                    for (int i = 0; i < f.Length; i++)
                    {
                        StackFrame sf = f[i];
                        System.Reflection.MethodBase m = sf.GetMethod();

                        string cname = m.ReflectedType.FullName;
                        if (m.Name.Contains("_Start"))
                        {
                            a = Assembly.GetAssembly(m.ReflectedType);
                            break;
                        }
                    }
            }
            if (a != null)
            {
                baseName = a.ToString();
                if (!string.IsNullOrEmpty(baseName))
                    baseName = baseName.Substring(0, baseName.IndexOf(","));
            }
            return baseName;
        }


        public static Boolean BooleanValue(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return (Boolean)false;
            }
            else
            {
                if ((string.Compare("True", s, true) == 0) || (string.Compare("Yes", s, true) == 0))
                {
                    return (Boolean)true;
                }
                if ((string.Compare("False", s, true) == 0) || (string.Compare("No", s, true) == 0))
                {
                    return (Boolean)false;
                }
                int bv = 0;
                if (int.TryParse(s, out bv))
                {
                    return bv != 0;
                }
                return false;
            }
        }


        public static string UID { get; set; }

        static AppSetting()
        {
            string n = GetNamespace();
            AppSetting.ApplicationID = n;
            AppSetting.UID = n;


            /// fetch public property values using Reflection, set Default if supplied
            foreach (PropertyInfo p in typeof(AppSetting).GetProperties())
            {
                string pt = p.PropertyType.Name;
                string pn = p.Name;

                object o = ReadObject(pn, pt);

                if (o != null && !string.IsNullOrEmpty(pn))
                {
                    if (p.GetSetMethod() != null)
                    {
                        p.SetValue(null, o, null);
                    }
                }
                else
                {
                    if (p.GetCustomAttributes(true).Length > 0)
                    {
                        object[] defaultValueAttribute =
                            p.GetCustomAttributes(typeof(DefaultSettingValueAttribute), true);
                        if (defaultValueAttribute != null && defaultValueAttribute.Length > 0)
                        {
                            DefaultSettingValueAttribute dva =
                                defaultValueAttribute[0] as DefaultSettingValueAttribute;

                            if (dva != null)
                            {
                                if (p.GetSetMethod() != null)
                                {
                                    if (pt.StartsWith("String", StringComparison.InvariantCultureIgnoreCase))
                                        p.SetValue(null, dva.Value, null);
                                    else if (pt.StartsWith("Int", StringComparison.InvariantCultureIgnoreCase))
                                        p.SetValue(null, Convert.ToInt32(dva.Value), null);
                                    else if (pt.StartsWith("Bool", StringComparison.InvariantCultureIgnoreCase))
                                        p.SetValue(null, dva.Value.BooleanValue(), null);
                                }
                                o = dva.Value;
                            }
                        }
                    }
                }
            }

#if DEBUG
            if (!AppSetting.IsDebug) AppSetting.IsDebug = true;
#endif

            /// fetch each property value and fill into vit
            foreach (FieldInfo fi in
                    typeof(AppSetting).GetFields(
                        BindingFlags.NonPublic
                      | BindingFlags.Static
                      ))
            {
                string pt = fi.FieldType.Name;
                string fin = fi.Name;
                if (!fin.StartsWith("<"))
                {
                    object o = ReadObject(fin, pt);
                    fi.SetValue(null, o);
                    Debug.WriteLine(".{0}={1}", fin, o);
                }
            }
        }

        public static string LdapPath { get; set; }
        public static string LdapDomain { get; set; }
        public static string ApplicationID { get; set; }
        public static string BPH_EMAIL_FIELD_NAME { get; set; }
        public static string SMTP_SERVER_FIELD_NAME { get; set; }
        public static string BPH_EMAIL_SUBJECT_FIELD_NAME { get; set; }
        public static string K2_INTEGRATED { get; set; }
        public static string K2_SERVER { get; set; }
        public static string K2_SMO_PORT { get; set; }
        public static string K2_WORKFLOW_PORT { get; set; }
        public static string K2_USER { get; set; }
        public static string K2_PASS { get; set; }
        public static string K2_DOMAIN { get; set; }
        public static string K2_PORT { get; set; }
        public static string K2_LABEL { get; set; }

        [DefaultSettingValue("ELVISWorkflow\\PVRegistration")]
        public static string K2_PROCESS_FULLNAME { get; set; }

        public static string FTP_PROXY { get; set; }
        public static string TempFileUpload { get; set; }
        public static string ErrorUploadFilePath { get; set; }

        public static string SAP_NAME { get; set; }
        public static string SAPName
        {
            get { return SAPName; }
        }

        public static string SAP_CLIENT { get; set; }
        public static string SAPClient
        {
            get { return SAP_CLIENT; }
        }

        public static string SAP_LANG { get; set; }

        public static string SAPLang
        {
            get { return SAP_LANG; }
        }

        public static string SAP_ASHOST { get; set; }
        public static string SAPAsHost
        {
            get { return SAP_ASHOST; }
        }


        public static string SAP_SYSN { get; set; }
        public static string SAPSysn
        {
            get { return SAP_SYSN; }
        }

        public static string SAP_MAX_POOL_SIZE { get; set; }
        public static string SAPMaxPoolSize
        {
            get { return SAP_MAX_POOL_SIZE; }
        }
        public static string SAP_IDLE_TIMEOUT;
        public static string SAPIdleTimeout
        {
            get { return SAP_IDLE_TIMEOUT; }
        }

        public static string FTP_SERVER { get; set; }
        public static string FTP_USERID { get; set; }
        public static string FTP_PASS { get; set; }
        public static string FTP_DOWNLOADHTTP { get; set; }
        public static string ATTACH_PATH { get; set; }
        public static string ATTACH_DIR { get; set; }

        [DefaultSettingValue("dd.MM.yyyy HH:mm:ss")]
        public static string UPLOAD_DATE_FORMAT { get; set; }

        public static string DATE_TIME_FORMAT { get; set; }
        public static string PV_NOTICE_EMAIL_SUBJECT_FIELD_NAME { get; set; }
        public static string RV_NOTICE_EMAIL_SUBJECT_FIELD_NAME { get; set; }
        public static string PublicKey { get; set; }
        public static string NameSessionURL { get; set; }
        public static string NameSessionPageNameURL { get; set; }
        public static string SYSTEM_VER { get; set; }
        public static decimal FINANCE_DIVISION_CODE { get; set; }
        public static string ELVIS_ADMIN { get; set; }
        [DefaultSettingValue("0")]
        public static bool SKIP_BUDGET_CHECK { get; set; }
        [DefaultSettingValue("1")]
        public static bool BUDGET_CHECK_GET { get; set; }
        [DefaultSettingValue("1")]
        public static bool BUDGET_CHECK_BOOK { get; set; }
        [DefaultSettingValue("1")]
        public static bool BUDGET_CHECK_RELEASE { get; set; }
        [DefaultSettingValue("1")]
        public static bool BUDGET_CHECK_DELETE { get; set; }
        [DefaultSettingValue("1")]
        public static bool BUDGET_CHECK_REVERSE { get; set; }

        public static string CompanyName { get; set; }
        public static string CompanyLogo { get; set; }
        public static string[] CASH_CURRENCIES
        {
            get
            {
                return Read("CASH_CURRENCIES", "IDR,JPY,USD").Split(',');
            }
        }
        [DefaultSettingValue("0")]
        public static bool EXCLUDE_EXTERNAL_VENDOR { get; set; }

        [DefaultSettingValue("https://elvis.toyota.astra.co.id/Login.aspx")]
        public static string LOGIN_PAGE { get; set; }
        [DefaultSettingValue("1")]
        public static bool LIST_ON_LOAD
        {
            get
            {
                return (Read("LIST_ON_LOAD", "1") == "1");
            }
        }

        public static string[] RoundCurrency { get; set; }
        [DefaultSettingValue("0")]
        public static bool NOOP_K2 { get; set; }
        [DefaultSettingValue("0")]
        public static bool NOOP_SAP { get; set; }
        [DefaultSettingValue("0")]
        public static int TICK_CLASS { get; set; }

        [DefaultSettingValue("0")]
        public static int POST_NUMBER_FORMAT { get; set; }

        public static bool NoticeMailUsePort
        {
            get
            {
                return (ReadInt("NOTICE_MAIL_USE_PORT", 0)) != 0;
            }
        }
        [DefaultSettingValue("0")]
        public static bool FilterSecretaryDivision { get; set; }

        public static bool SendEmail { get; set; }

        [DefaultSettingValue("200")]
        public static int MailWaitInterval { get; set; }

        public static string LogLocationEnable { get; set; }
        public static bool LogToText { get; set; }

        public static string TempFileZIP { get; set; } 
        
        [DefaultSettingValue("0")]
        public static bool IsDebug { get; set; }
    }
}