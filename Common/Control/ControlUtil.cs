﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.ASPxEditors;

namespace Common.Control
{
    public class ControlUtil
    {
        public static int getComboBoxIndexByValue(ASPxComboBox comboBox, string value)
        {
            ListEditItemCollection itemCollection = comboBox.Items;
            int cntItem = itemCollection.Count;
            ListEditItem item;
            for (int i = 0; i < cntItem; i++)
            {
                item = itemCollection[i];
                if (item.Value.Equals(value))
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
