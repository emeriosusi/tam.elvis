﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Common.Control
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ExtendedTextBox runat=\"server\"></{0}:ExtendedTextBox>")]
    public class ExtendedTextBox : System.Web.UI.WebControls.TextBox, IPostBackEventHandler
    {  
        public ExtendedTextBox() : base()
        {
        }

        public delegate void dlgOnBlur(object sender, EventArgs e);
        public event dlgOnBlur Blur;

        public void RaisePostBackEvent(string eventArgument)
        {  
            if (Blur != null)
            {
                Blur(this, EventArgs.Empty);
            }            
        }        

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            base.Attributes.Add("onblur", String.Format("__doPostBack(\"{0}\",'')", UniqueID));
        }
    }
}
