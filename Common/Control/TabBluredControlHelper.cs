﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Common.Control
{
    public class TabBluredControlHelper
    {
        private string script;
        private const string REGISTRATION_KEY = "TabBluredControl_onKeypress";
        public const string KEY_CODE_TAB = "9";
        public const string KEY_CODE_UP = "38";
        public const string KEY_CODE_DOWN = "40";
        public const string KEY_CODE_RETURN = "13";

        public TabBluredControlHelper()
        {
            StringBuilder scriptBuilder = new StringBuilder();
            scriptBuilder.AppendLine("function evt_TabBluredControl_onKeypress(event,id) {");
            scriptBuilder.AppendLine("      var keyCode = \"\"; ");
            scriptBuilder.AppendLine("      if(event.ctrlKey) { ");
            scriptBuilder.AppendLine("          keyCode = \"ctrl:\" + keyCode; ");
            scriptBuilder.AppendLine("      }");
            scriptBuilder.AppendLine("      if(event.altKey) { ");
            scriptBuilder.AppendLine("          keyCode = \"alt:\" + keyCode; ");
            scriptBuilder.AppendLine("      }");
            scriptBuilder.AppendLine("      keyCode = keyCode + event.keyCode; ");
            scriptBuilder.AppendLine("      if((event.keyCode == 9 && !event.shiftKey) || ((event.keyCode == 13) && event.ctrlKey) || ");
            scriptBuilder.AppendLine("         ((event.keyCode == 38) && event.ctrlKey) || ");
            scriptBuilder.AppendLine("         ((event.keyCode == 40) && event.ctrlKey)) {");
            scriptBuilder.AppendLine("          __doPostBack(id,keyCode);");
            scriptBuilder.AppendLine("      }");
            scriptBuilder.AppendLine("}");

            script = scriptBuilder.ToString();
        }

        public TabBlurredKeypressEventArgs getArgument(string arg)
        {
            TabBlurredKeypressEventArgs eventArg = new TabBlurredKeypressEventArgs();
            string[] fracs = arg.Split(':');
            if (fracs.Length > 1)
            {
                string code;
                for (int i = 0; i < fracs.Length; i++)
                {
                    code = fracs[i];
                    if (code.Equals("alt"))
                    {
                        eventArg.HasAlternateKey = true;
                    }
                    else if (code.Equals("ctrl"))
                    {
                        eventArg.HasControlKey = true;
                    }
                    else
                    {
                        eventArg.KeyCode = code;
                    }
                }
            }
            else
            {
                eventArg.KeyCode = arg;
            }
            return eventArg;
        }

        public void init(ClientScriptManager scriptManager, System.Web.UI.WebControls.WebControl control)
        {
            if (!scriptManager.IsClientScriptBlockRegistered(this.GetType(), REGISTRATION_KEY))
            {
                scriptManager.RegisterClientScriptBlock(this.GetType(), REGISTRATION_KEY, script, true);
            }

            control.Attributes.Add("onkeypress", String.Format("evt_TabBluredControl_onKeypress(event, '{0}')", control.UniqueID));
        }
    }
}