﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Common.Control
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:TabBluredComboBox runat=\"server\"></{0}:TabBluredComboBox>")]
    public class TabBluredComboBox : DevExpress.Web.ASPxEditors.ASPxComboBox, IPostBackEventHandler
    {
        private string script;
        private TabBluredControlHelper tabBluredControl;

        public TabBluredComboBox()
            : base()
        {
            this.tabBluredControl = new TabBluredControlHelper();
        }

        public delegate void dlgOnTabKeypress(object sender, EventArgs e);
        public event dlgOnTabKeypress TabKeypress;
        public delegate void dlgOnReturnKeypress(object sender, EventArgs e);
        public event dlgOnReturnKeypress ReturnKeypress;
        public delegate void dlgOnUpKeypress(object sender, EventArgs e);
        public event dlgOnUpKeypress CtrlUpKeypress;
        public delegate void dlgOnDownKeypress(object sender, EventArgs e);
        public event dlgOnDownKeypress CtrlDownKeypress;

        protected override void RaisePostBackEvent(string eventArgument)
        {
            TabBlurredKeypressEventArgs eventArg = tabBluredControl.getArgument(eventArgument);
            if (eventArg.KeyCode.Equals(TabBluredControlHelper.KEY_CODE_TAB))
            {
                if (!eventArg.HasControlKey && !eventArg.HasAlternateKey && ((TabKeypress != null)))
                {
                    TabKeypress(this, eventArg);
                }
            }
            else if (eventArg.KeyCode.Equals(TabBluredControlHelper.KEY_CODE_UP))
            {
                if (eventArg.HasControlKey && (CtrlUpKeypress != null))
                {

                    CtrlUpKeypress(this, eventArg);
                }
            }
            else if (eventArg.KeyCode.Equals(TabBluredControlHelper.KEY_CODE_DOWN))
            {
                if (eventArg.HasControlKey && (CtrlDownKeypress != null))
                {
                    CtrlDownKeypress(this, eventArg);
                }
            }
            else if (eventArg.KeyCode.Equals(TabBluredControlHelper.KEY_CODE_RETURN))
            {
                if (eventArg.HasControlKey && (ReturnKeypress != null))
                {
                    ReturnKeypress(this, eventArg);
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            tabBluredControl.init(Page.ClientScript, this);
        }
    }
}