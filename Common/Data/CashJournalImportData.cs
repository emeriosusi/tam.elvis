﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class CashJournalImportData
    {
        public string CompanyCode { get; set; }
        public string CashJournal { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public string CashType { get; set; }
        public string TransType { get; set; } 
    }

    [Serializable]
    public enum CajoTrans
    {
        Save = 0,
        Post = 1,
        Delete = 2,
        Reverse = 3
    }

    [Serializable]
    public enum CajoCash
    {
        Payment = 'P',
        Receipt = 'R',
        Check = 'C'
    }

    [Serializable]
    public enum CajoStatus
    {
        Save = 'S',
        Post = 'P',
        Reverse = 'R',
        Delete = 'D',
        None = ' '
    }
    
    [Serializable]
    public enum CajoLock
    {
        Locked = 'X',
        Open= ' '
    }
}
