﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class NoticeHomeData
    {
        public string DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public decimal REFF_NO { get; set; }
        public int? REPLY_FLAG { get; set; }
        public string DESCRIPTION { get; set; }
        public string NOTICE_FROM { get; set; }
        public string NOTICE_TO { get; set; }
        public byte? NOTICE_FROM_CLASS { get; set; }
        public byte? NOTICE_TO_CLASS { get; set; }
        public string NOTICE_TO_NAME { get; set; }
        public string DIVISION_NAME { get; set; }
        public decimal TOTAL_AMOUNT_IDR { get; set; }
        public DateTime? NOTICE_DT { get; set; }
        public string USER_ROLE_TO { get; set; }
        public long ROW_NOTICE { get; set; }
        public string ACCRUED_NO { get; set; }
    }
}
