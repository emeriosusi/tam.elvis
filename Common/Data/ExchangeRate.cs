﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class ExchangeRate
    {
        public string CurrencyCode { set; get; }
        public DateTime ValidFrom { set; get; }
        public DateTime? ValidTo { set; get; }
        public decimal Rate { set; get; }
        public decimal? AmountMin { set; get; } 
    }
}
