﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class WorklistReportData
    {
        public string PicDiv { get; set; }
        public string DivNamePic { get; set; }
        public string PIC { get; set; }
        public string POSITION_NAME { get; set; }
        public string DivNameDoc { get; set; }
        public int? DOC_AGE { get; set; }
        public DateTime? MODIFIED { get; set; }
        public string MODIFIED_BY { get; set; } 
        public int? DOC_NO { get; set; }
        public string STATUS_NAME { get; set; }
        public string Description { get; set; }
        public decimal? TOTAL_AMOUNT { get; set; }
        public string TRANSACTION_NAME { get; set; }
        public string VENDOR_NAME { get; set; } 
    }
}
