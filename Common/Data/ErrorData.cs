﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    /// <summary>
    /// Class untuk Error Data
    /// ErrID = 1 ==> error karena user input / data tidak ada
    /// ErrID = 2 ==> error karena sistem (putus koneksi,loop forever dll)
    /// </summary>
    public class ErrorData 
    {

        public ErrorData()
        {
            _ErrID = 0;
            _ErrMsgID = "";
            _ErrMsg = "";
            _Data = null;
        }

        public ErrorData(int id, string mId, string msg, string[] data = null)
        {
            _ErrID = id;
            _ErrMsgID = mId;
            _ErrMsg = msg;
            _Data = data;
        }

        int _ErrID;
        public int ErrID
        {
            get { return _ErrID; }
            set { _ErrID = value; }
        }

        string _ErrMsg;
        public string ErrMsg
        {
            get { return _ErrMsg; }
            set { _ErrMsg = value; }
        }

        string _ErrMsgID;
        public string ErrMsgID
        {
            get { return _ErrMsgID; }
            set { _ErrMsgID = value; }
        }

        string[] _Data;
        public string[] Data
        {
            get { return _Data; }
            set { _Data = value; }
        }
    }
}
