﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{ 
    [Serializable]
    public class CashJournalExportData
    {
        public decimal? OpeningBalance;
        public decimal? TotalCashReceipt;
        public int? NumberCashReceipt;
        public decimal? TotalCheckReceipt;
        public int? NumberCheckReceipt;
        public decimal? TotalCashPayment;
        public int? NumberCashPayment;
        public decimal? ClosingBalance;
        public decimal? Cash;
        public string LockStatus;
        public string eMessage;
    }
}
