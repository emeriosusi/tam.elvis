﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class RoleAcessData
    {
        public string REG_NO { get; set; }
        public string USERNAME { get; set; }
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string SCREEN_ID { get; set; }
        public string OBJECT_ID { get; set; }
    }
    [Serializable]
    public class RoleData
    {
        public string ROLE_ID { get; set; }

        public string ROLE_NAME { get; set; }
    }
    [Serializable]
    public class UserRoleHeader
    {
        public decimal REG_NO { get; set; }
        public string USERNAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string EMAIL { get; set; }
        public string APPLICATION_ID { get; set; }
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string FULLNAME
        {
            get
            {
                if (!string.IsNullOrEmpty(FIRST_NAME) && !string.IsNullOrEmpty(LAST_NAME))
                {
                    return FIRST_NAME + " " + LAST_NAME;
                }
                else
                {
                    return FIRST_NAME ?? "" + LAST_NAME ?? "";
                }
            }
        }
        public string DIVISION_ID { get; set; }
    }
}
