﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._60SettlementForm
{
    [Serializable]
    public class SettlementFormHeaderData
    {
        public int? PV_SUS_NO { get; set; }
        public int PV_NO { get; set; }
        public int PV_YEAR { get; set; }
        public int? CONVERT_FLAG { get; set; }
        public string PV_TYPE { get; set; }
        public string DIVISION_ID { get; set; }
        public string VENDOR_NAME { get; set; }
        public string TRANSACTION_NAME { get; set; }
        public int? PV_TYPE_CD { get; set; }
        public string VENDOR_CD { get; set; }
        public int? VENDOR_GROUP_CD { get; set; }
        public int? TRANSACTION_CD { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public int? SETTLEMENT_STATUS { get; set; }
        public int STATUS_CD { get; set; }
        public string PAY_METHOD_CD { get; set; }
    }
}
