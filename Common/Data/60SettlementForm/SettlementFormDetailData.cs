﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._60SettlementForm
{
    [Serializable]
    public class SettlementFormDetailData
    {
        public int? SEQ_NO { get; set; }
        public int PV_SEQ_NO { get; set; }
        public int PV_SUS_NO { get; set; }
        public int PV_YEAR { get; set; }
        public int? SETTLEMENT_NO { get; set; }
        public decimal? SPENT_AMOUNT { get; set; }
        public decimal? SETTLEMENT_AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string COST_CENTER_NAME { get; set; }
        public string COST_CENTER_CD { get; set; }
        public string DESCRIPTION { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal AMOUNT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime CHANGED_DT { get; set; }
        public decimal REFF_NO { get; set; }
        public int SUSPENSE_NO_NEW { get; set; }
        public int SUSPENSE_NO_OLD { get; set; }
        public string ACTIVITY_DES { get; set; }
    }
}
