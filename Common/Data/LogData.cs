﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class LogData
    {
        public int RowNum { get; set; } 
        public int Id { get; set; }
        public int Seq { get; set; }
        public DateTime When { get; set; }
        public string Where { get; set; }
        public string Who { get; set; }
        public string What { get; set; }
        public string Remarks { get; set; }
        public string MsgId { get; set; }
        public string MsgType { get; set; }
        public string Sts{ get; set; }
    }
}
