﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class CashJournalData
    {
        public CashJournalImportData Import { get; set; }
        public CashJournalExportData Export { get; set; }
        public List<CashJournalTable> CashPayments { get; set; }
        public List<CashJournalTable> CashReceipts { get; set; }
        public List<CashJournalMessage> Message { get; set; } 

        public CashJournalData()
        {
            Import = new CashJournalImportData();
            Export = new CashJournalExportData();
            CashPayments = new List<CashJournalTable>();
            CashReceipts = new List<CashJournalTable>();
        }
    }
}
