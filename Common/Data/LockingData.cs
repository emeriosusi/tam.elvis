﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class LockingData
    {
        public int process_id { get; set; } 
        public string function_id  { get; set; } 
        private string _lock_reff;
        public string lock_reff  { get; set; } 
        public string user_id { get; set; } 
        public DateTime start_time { get; set; } 
    }
}
