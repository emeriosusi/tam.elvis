﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class UserRoleData
    {
        public string USERNAME { get; set; }
        public string REG_NO { get; set; }
        public string TITLE { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string EMAIL { get; set; }
        public string POSITION_ID { get; set; }
        public string POSITION_NAME { get; set; }
        public byte? CLASS { get; set; }
        public string SECTION_ID { get; set; }
        public string SECTION_NAME { get; set; }
        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public string DEPARTMENT_ID { get; set; }
        public string DEPARTMENT_NAME { get; set; }
        public int GROUP_CD { get; set;  }
        public string GROUP_NAME { get; set; }
        public string ROLE_ID { get; set; }
    }
}
