﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class CustomerData
    {
        public string CUSTOMER_CD { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string CUSTOMER_GROUP { get; set; }
        public string SEARCH_TERMS { get; set; }
        public string BUSINESS_TYPE { get; set; }
        public string SUJK { get; set; } 
        public string CONTACT_PERSON { get; set; }
        public string PHONE { get; set; }
        public int? SERVICE_YEAR { get; set; }         
        public string CONTRACT_STATUS { get; set; }
        public string DESCRIPTION { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
    }
}
