﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class BudgetMappingData
    {
        public int TRANSACTION_CD { get; set; }
        public string WBS_CODE { get; set; }
        public DateTime? VALID_FROM { get; set; }
        public DateTime? VALID_TO { get; set; }

        public string TRANSACTION_NAME { get; set; }
        public string WBS_CODE_NAME { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
