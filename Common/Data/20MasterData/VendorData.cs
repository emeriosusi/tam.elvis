﻿using System;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class VendorData
    {   
        public string VENDOR_CD { get; set; }
        public string VENDOR_NAME { get; set; }
        public int? VENDOR_GROUP_CD { get; set; }
        public string VENDOR_GROUP_NAME { get; set; }
        public string SEARCH_TERMS { get; set; }
        public string CONTACT_PERSON { get; set; }
        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public int? SUSPENSE_COUNT { get; set; } 
    }
}
