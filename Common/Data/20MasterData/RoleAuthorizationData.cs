﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    public class RoleAuthorizationData
    {
        public string ROLE_ID { get; set; }
        public string SCREEN_ID { get; set; }
        public string OBJECT_ID { get; set; }
    }

    public class RoleMasterData
    {
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string CHANGED_BY { get; set; }
    }    

    public class RoleScreenData
    {
        public string ROLE_ID { get; set; }
        public string SCREEN_ID { get; set; }
        public string CAPTION { get; set; }
        public int? object_count { get; set; }
        public string LevelID { get; set; }
        public int? Indent { get; set; }
    }
}
