﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.Web;
using System.ComponentModel;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class TransactionTypeData
    {
        public int TRANSACTION_CD { get; set; }
        public string TRANSACTION_NAME { get; set; }
        public int? TRANSACTION_LEVEL_CD { get; set; }
        public int GL_ACCOUNT { get; set; }
        public string STD_WORDING { get; set; }
        public int? GROUP_CD { get; set; }
        //public int? FT_CD { get; set; }
        public int? TEMPLATE_CD { get; set; }
        public int? CLOSE_FLAG { get; set; }
        public int? SERVICE_FLAG { get; set; }
        public int? BUDGET_FLAG { get; set; }
        public int? ATTACHMENT_FLAG { get; set; }
        public int? PRODUCTION_FLAG { get; set; }
        public int? COST_CENTER_FLAG { get; set; } 
        public string TEMPLATE_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
