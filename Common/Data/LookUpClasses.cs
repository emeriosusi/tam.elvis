﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class VendorCdData
    {
        public string VENDOR_CD { get; set; }
        public string SEARCH_TERMS { get; set; } 
        public string VENDOR_NAME { get; set; }
    }

    public class CostCenterCdData
    {
        public string COST_CENTER_CD { get; set; }
        public string COST_CENTER_NAME { get; set; }
        public string DIVISION { get; set; }
        public int PRODUCTION_FLAG { get; set; }
    }

    public class OtherAmountData
    {
        public string CURRENCY_CD { get; set; }
        public string CURRENCY_AMOUNT { get; set; }
        public decimal? TOTAL_AMOUNT { get; set; }
        public decimal? EXCHANGE_RATE { get; set; }
    }
    public class SapDocNoData
    {
        public string CURRENCY_CD { get; set; }
        public string INVOICE_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string SAP_CLEARING_DOC_NO { get; set; }
        public string PAYPROP_DOC_NO { get; set; }
        public string PAYPROP_ID { get; set; }
    }
    public class BudgetData
    {
        public string SEQ_NO { get; set; }
        public string WBS_NO { get; set; }
    }
    public class AttachmentData
    {
        public decimal REF_SEQ_NO { get; set; }
        public string ATTACH_CD { get; set; }
        public string DESCRIPTION { get; set; }
        public string FILE_NAME { get; set; }
        public string DIRECTORY { get; set; }
        public string ATTACH_NAME { get; set; }
        public string URL { get; set; } 
    }
}
