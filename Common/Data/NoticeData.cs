﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class NoticeData
    {
        public string DOC_NO { get; set; }
        public string DOC_YEAR { get; set; }
        public int NOTICE_NO { get; set; }
        public string NOTICE_FROM { get; set; }
        public string NOTICE_FROM_NAME { get; set; }
        public int REPLY_FLAG { get; set; }
        public DateTime? REPLY_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string USER_ROLE_FROM { get; set; }
        public string NOTICE_TO { get; set; }
        public string NOTICE_TO_NAME { get; set; }
        public string USER_ROLE_TO { get; set; }
        public string NOTICE_COMMENT { get; set; }
        public DateTime? NOTICE_DT { get; set; }
    }
}
