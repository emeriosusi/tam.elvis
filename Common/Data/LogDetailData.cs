﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class LogDetailData
    {
        public int process_id { get; set; }

        public int seq_no{ get; set; }

        public string msg_id{ get; set; }

        public string msg_type{ get; set; }

        public string err_location{ get; set; }

        public string err_message{ get; set; }

        public DateTime err_dt{ get; set; }
    }
}
