﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class BankKeysData
    {
        public string BANK_KEY { get; set; }
        public string BANK_NAME { get; set; }
        public string CITY { get; set; }
        public string BRANCH { get; set; }
    }
}
