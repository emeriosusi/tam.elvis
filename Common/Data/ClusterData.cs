﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Function;

namespace Common.Data
{
    [Serializable]
    public class ClusterData
    {
        public string ClusterId { get; set; }
        public string ClusterName { get; set; }
        public string StatusCodeList { get; set; }

        public int[] Status
        {
            get
            {
                List<int> r = new List<int>();
                if (!StatusCodeList.isEmpty())
                {
                    r.AddRange(StatusCodeList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => a.Int(0)).Distinct());
                }
                return r.ToArray();
            }
        }
    }
}
