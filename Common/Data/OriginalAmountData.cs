﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class OriginalAmountData
    {
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public int SEQ_NO { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal? SUSPENSE_AMOUNT { get; set; }
        public decimal? SPENT_AMOUNT { get; set; }
        public decimal AMOUNT { get; set; }
        public decimal? EXCHANGE_RATE { get; set; }
        public string COST_CENTER_CD { get; set; }
        public string DESCRIPTION { get; set; }
        public string INVOICE_NO { get; set; }
        public DateTime? INVOICE_DATE { get; set; }
        public int? ITEM_TRANSACTION_CD { get; set; }
        public int? GL_ACCOUNT { get; set; }
        public string TAX_NO { get; set; }
        public string TAX_CD { get; set; }
        #region EFB
        public string WHT_TAX_CODE { get; set; }
        public decimal? WHT_TAX_TARIFF { get; set; }
        public string WHT_TAX_ADDITIONAL_INFO { get; set; }
        public decimal? WHT_DPP_PPh_AMOUNT { get; set; }
        #endregion
        public DateTime CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string CHANGED_BY { get; set; } 
    }
}
