﻿using System;

namespace Common.Data
{
    [Serializable]
    public class ApproveRejectData
    {
        public string Folio { get; set; }
        public string StrDocNo { get; set; }
        public string StrDocYear { get; set; }
        public int IntDocNo { get; set; }
        public int IntDocYear { get; set; }

        #region 2018.08.07 wot.daniel + Mass Act CR
        public int DocCd { get; set; } 
        public int? StatusCd { get; set; }
        #endregion 
    }
}
