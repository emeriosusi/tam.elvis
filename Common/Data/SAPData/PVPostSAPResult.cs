using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Data.SAPData
{
    [Serializable]
    public class PVPostSAPResult
    {
        public string PV_NO { get; set; }
        public string PV_YEAR { get; set; }
        public string ITEM_NO { get; set; }
        public string CURRENCY_CD { get; set; }
        public string INVOICE_NO { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string SAP_DOC_YEAR { get; set; }
        public string SAP_CLEARING_DOC_NO { get; set; }
        public string SAP_CLEARING_DOC_YEAR { get; set; }
        public string MESSAGE { get; set; }
        public string STATUS_MESSAGE { get; set; }
        public string MESSAGE_TEXT { get; set; } 
    }
}
