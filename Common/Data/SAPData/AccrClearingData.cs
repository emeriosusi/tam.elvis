﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class AccrClearingData
    {
        public string AGKON { get; set; }
        public string BUDAT { get; set; }
        public string MONAT { get; set; }
        public string BUKRS { get; set; }
        public string WAERS { get; set; }
        public string AGUMS { get; set; }
        public string XNOPS { get; set; }
        public string XPOS1 { get; set; }
        public string XPOS2 { get; set; }
        public string SEL01 { get; set; }
        public string SEL02 { get; set; }
        public string ABPOS { get; set; }
    }
}
