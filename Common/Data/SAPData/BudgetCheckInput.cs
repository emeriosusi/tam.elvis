﻿using System;

namespace Common.Data.SAPData
{
    public enum BudgetCheckStatus
    {
        Get = 0,
        Book = 1,
        Release = 2,
        Delete = 3, 
        Reverse = 4
    }

    [Serializable]
    public class BudgetCheckInput
    {
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public int DOC_STATUS { get; set; }
        public string WBS_NO { get; set; }
        public decimal AMOUNT { get; set; } 
    }
}
