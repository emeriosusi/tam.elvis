﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class SyncSAPData
    {
        public string BUDGET_NO { get; set; }
        public decimal AVAILABLE_AMT { get; set; }
        public decimal BUDGET_AMT { get; set; }
        public decimal ACTUAL_AMT { get; set; }
        public decimal COMMITMENT_AMT { get; set; }
    }
}
