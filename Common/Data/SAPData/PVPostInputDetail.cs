using System;

namespace Common.Data.SAPData
{
    [Serializable]
    public class PVPostInputDetail
    {
        public string PV_NO { get; set; }
        public string PV_YEAR { get; set; }
        public int SEQ_NO { get; set; }
        public string INVOICE_NO { get; set; }
        public string GL_ACCOUNT { get; set; }
        public decimal? AMOUNT { get; set; }
        public decimal? SPENT_AMOUNT { get; set; }
        public string COST_CENTER { get; set; }
        public string WBS_ELEMENT { get; set; }
        public string ITEM_TEXT { get; set; }
        public string CURRENCY_CD { get; set; }
        public string TAX_CD { get; set; }
        public int PV_TYPE { get; set; }
        public DateTime PV_DATE { get; set; } 
    }
}
