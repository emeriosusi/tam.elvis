using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Data.SAPData
{
    [Serializable]
    public class RVPostInputDetail
    {
        public string RV_NO { get; set; }
        public string RV_YEAR { get; set; }
        public string SEQ_NO { get; set; }
        public string INVOICE_NO { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string AMOUNT { get; set; }
        public string COST_CENTER { get; set; }
        public string WBS_ELEMENT { get; set; }
        public string ITEM_TEXT { get; set; }
    }
}
