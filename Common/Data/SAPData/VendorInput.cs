﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    public class VendorInput
    {
        public string NAME {get; set;}
        public string DIVISION { get; set; }
        public string DIVISION_NAME { get; set; } 
        public string SEARCH_TERM { get; set; }
    }
}
