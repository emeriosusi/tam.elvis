﻿using System;

namespace Common.Data.SAPData
{
    [Serializable]
    public class PVPostInputHeader
    {
        public string PV_NO { get; set; }
        public string PV_YEAR { get; set; }        
        public int ITEM_NO { get; set; }
        public string PV_DATE { get; set; }
        public string PV_TYPE { get; set; }
        public string PV_NO_SUSPENSE { get; set; }
        public string PV_YEAR_SUSPENSE { get; set; }        
        public string TRANS_TYPE { get; set; }
        public string VENDOR { get; set; }
        public string VENDOR_GROUP { get; set; }
        public string INVOICE_NO { get; set; }
        public string TAX_NO { get; set; }
        public string PAYMENT_TERM { get; set; }
        public string PAYMENT_METHOD { get; set; }
        public string PLAN_PAYMENT_DATE { get; set; }
        public string POSTING_DATE { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal DPP_AMOUNT { get; set; }
        
        public string CURRENCY_CD { get; set; }
        public string TAX_CD { get; set; }
        public string HEADER_TEXT { get; set; }
        public string BANK_TYPE { get; set; }
        public string FROM_CURR { get; set; }
        public decimal? EXCHANGE_RATE { get; set; } 
        public decimal? EXRATE_SUSPENSE { get; set; }
        public decimal? DIFF_EXRATE { get; set; } 
        public int NO_SUSPENSE_FLAG { get; set; }
        public int? COST_CENTER_FLAG { get; set; }

        public string TAX_ASSIGNMENT { get; set; }
        public string ACC_INV_ASSIGNMENT { get; set; }
        public DateTime? TAX_DT { get; set; }
        
        // additional SAP output entry
        public string STATUS { get; set; }
        public int? SAP_DOC_NO { get; set; }
        public int? SAP_DOC_YEAR { get; set; }

        // additional member for calculation and store 
        public decimal SPENT_AMOUNT { get; set; }
        public DateTime? PV_DATE_SUSPENSE { get; set; }
        public int TRANSACTION_CD { get; set; }
        public int SEQ_NO { get; set; }
    }   
}
