﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class OneTimeVendorData
    {
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public string TITLE { get; set; }
        public string NAME1 { get; set; }
        public string NAME2 { get; set; }
        public string STREET { get; set; }
        public string CITY { get; set; }
        public string COUNTRY { get; set; }
        public string LANGUAGE_KEY { get; set; }
        public string BANK_KEY { get; set; }
        public string BANK_COUNTRY { get; set; }
        public string BANK_ACCOUNT { get; set; }
    }
}
