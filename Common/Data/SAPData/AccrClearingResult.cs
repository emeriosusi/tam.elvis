﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class AccrClearingResult
    {
        public string BELNR { get; set; }
        public string NOCLR { get; set; }
    }
}
