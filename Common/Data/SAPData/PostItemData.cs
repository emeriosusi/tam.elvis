﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class PostItemData
    {
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public int SEQ_NO { get; set; }
        public int ITEM_NO { get; set; }
        public string CURRENCY_CD { get; set; }
        public string INVOICE_NO { get; set; } 
        public int? SAP_DOC_NO { get; set; }
        public int? SAP_DOC_YEAR { get; set; } 
    }
}
