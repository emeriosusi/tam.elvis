﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    public class DocFiscal
    {
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public int FISCAL_YEAR { get; set; }         
    }

    public class FiscalDocs
    {
        public List<DocFiscal> list = new List<DocFiscal>();

        public void Add(int dNo, int dYear, int fYear)
        {
            DocFiscal x = (from a in list 
                           where a.DOC_NO == dNo 
                           && a.DOC_YEAR == dYear 
                           select a).FirstOrDefault();
            if (x == null) {
                list.Add(new DocFiscal() { DOC_NO = dNo, DOC_YEAR = dYear, FISCAL_YEAR = fYear });
            }
            else {
                x.FISCAL_YEAR = fYear;
            }
        }

        public void Del(int dNo, int dYear, int fYear)
        {
            int j = -1;
            for(int i = list.Count -1; i >= 0; i++) 
            {
                if (list[i].DOC_NO == dNo && list[i].DOC_YEAR == dYear) {
                    j = i;
                    break;
                }
            }
            if (j >= 0) 
            {
                list.RemoveAt(j);
            }
        }

        public int Doc(int dNo, int fYear)
        {
            DocFiscal x = (from a in list
                           where a.DOC_NO == dNo && a.FISCAL_YEAR == fYear
                           select a).FirstOrDefault();
            if (x != null)
            {
                return x.DOC_YEAR;
            }
            else
            {
                return 0;
            }

        }
        public int Fiscal(int dNo, int dYear) 
        {
            DocFiscal x = (from a in list
                           where a.DOC_NO == dNo && a.DOC_YEAR == dYear
                           select a).FirstOrDefault();
            if (x != null)
            {
                return x.FISCAL_YEAR;
            }
            else
            {
                return 0;
            }
        }

    }
}
