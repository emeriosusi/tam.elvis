﻿using Common.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class PVFormSimulationDetail
    {
        public int SEQ_NO { get; set; }
        public String ACCOUNT { get; set; }
        public String ACCOUNT_SHORT { get; set; }
        public String TX { get; set; }
        public String AMOUNT { get; set; }
        public String TEXT { get; set; }
        
        // fid.Hadid 20180608
        public String WBS_NO { get; set; }
        public String COST_CENTER_CD { get; set; }
        public Decimal AMOUNT_DEC { get; set; }
        public DataSAPMode dataMode { get; set; }
    }
}