﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class ApprovalData
    {
        public string CODE { get; set; } // UserPosition
        public string FULL_NAME { get; set; } // FIRST_NAME + LAST_NAME
        public DateTime? ACTUAL_DT { get; set; }
        public int STATUS { get; set; } // 0 = wait 1 = Approved 2 = rejected 
        public int? SKIP_FLAG { get; set; } // 1 = DIRECT TO ME 2 = CONCURRENT 
        public int STATUS_CD { get; set; } 
        public string PROCEED_BY { get; set; }
        public int CLASS { get; set; }
    }
}