﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    public class PVFormHistoryDetail
    {
        public int SequenceNumber { set; get; }
        public int PVNumber { get; set; }
        public int PVYear { get; set; }
        public int Version { get; set; }
        public string CostCenterCode { set; get; }
        public bool CostCenterCodeChange { get; set; }
        public string CostCenterName { set; get; }
        public bool CostCenterNameChange { get; set; }
        public string Description { set; get; }
        public bool DescriptionChange { get; set; }
        public string StandardDescriptionWording { set; get; }
        public bool StandardDescriptionWordingChange { get; set; }
        public string CurrencyCode { set; get; }
        public bool CurrencyCodeChange { get; set; }
        public decimal Amount { set; get; }
        public bool AmountChange { get; set; }
        public string TaxCode { set; get; }
        public bool TaxCodeChange { get; set; }
        public string TaxNumber { set; get; }
        public bool TaxNumberChange { set; get; }
        public int? ItemTransactionCode { set; get; }
        public bool ItemTransactionCodeChange { get; set; }
        public string ItemTransactionName { get; set; }
        public bool ItemTransactionNameChange { set; get; }
        public string WbsNumber { set; get; }
        public bool WbsNumberChange { get; set; }
        public string InvoiceNumber { set; get; }
        public bool InvoiceNumberChange { set; get; }
        public string InvoiceNumberUrl { set; get; }
        public bool InvoiceNumberUrlChange { set; get; }
        public bool IsDeleted { get; set; }
    }
}