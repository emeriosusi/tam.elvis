﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class PVFormSimulationData
    {
        public List<PVFormSimulationDetail> Details { get; set; }
        public String KEYS { get; set; }
        public String PV_NO { get; set; }
        public String PV_YEAR { get; set; }
        
        public String VENDOR_CD { get; set; }
        public String VENDOR_NAME { get; set; }
        public String TAX_CD { get; set; }
        public Decimal? TAX_VALUE { get; set; }
        public Decimal? WITHOLDING_TAX_VALUE { get; set; }
        public Decimal? TOTAL_AMOUNT_IDR { get; set; }
        public String SERVICE_FLAG { get; set; }
        public String STD_WORDING { get; set; }
        public int PV_TYPE_CD { get; set; }
        public DateTime PV_DATE { get; set; }
        public Int32? TRANSACTION_CODE { get; set; }
        public String TRANSACTION_NAME { get; set; }
        public String COMPANY_CODE { get; set; }
        public DateTime? POSTING_DATE { get; set; }
        public String FISCAL_YEAR { get; set; }
        public Int32? PERIOD { get; set; }
        public String REF_DOC { get; set; }
        public String WITHOLDING_TAX { get; set; }
        public String CURRENCY_CODE { get; set; }
        public String WARNING { get; set; }


    }
}