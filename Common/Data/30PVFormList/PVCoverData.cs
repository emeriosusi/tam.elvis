﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVList
{
    public class PVCoverData
    {
        public String INVOICE_NO { get; set; }
        public String CURRENCY { get; set; }
        public Decimal? AMOUNT { get; set; }
        public String TAX_NO { get; set; }
        public DateTime? INVOICE_DATE { get; set; }
        public Decimal? TAX_AMOUNT { get; set; }
    }
}
