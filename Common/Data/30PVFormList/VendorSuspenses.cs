﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class VendorSuspenses
    {
        public string VENDOR_CD { get; set; }
        public string VENDOR_NAME { get; set; }
        public string DIVISION_ID { get; set; }
        public int TRANSACTION_CD { get; set; } 
        public string TRANSACTION_NAME { get; set; }        
        public int PV_NO { get; set; }
        public int PV_YEAR { get; set; } 
    }
}
