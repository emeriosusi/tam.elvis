﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable] 
    public class InvoiceAmounts 
    {
        Dictionary<string, decimal> a = new Dictionary<string,decimal>();
        public decimal? this[string curr] 
        {
            get
            {
                if (a.Keys.Contains(curr)) 
                    return a[curr]; 
                else 
                    return null;
            }
            set 
            {
                if (value!= null)
                    if (a.Keys.Contains(curr)) 
                        a[curr] = (decimal) value; 
                    else 
                        a.Add(curr, value??0);
            }
        }
    }

    [Serializable]
    public class InvoiceSummaryData
    {
        public int SEQ { get; set; }
        public string INVOICE_NO { get; set; }
        public InvoiceAmounts AMOUNT { get; set; }

        public InvoiceSummaryData(int seq, string num, string curr, decimal amt)
        {
            SEQ = seq;
            INVOICE_NO = num;
            AMOUNT = new InvoiceAmounts();
            AMOUNT[curr] = amt;
        }
    }
}
