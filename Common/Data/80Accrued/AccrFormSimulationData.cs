﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccrFormSimulationData
    {
        public static readonly string SUSPAID = "PAID";
        public static readonly string SUSUNPAID = "NOT PAID";
        public List<AccrFormSimulationDetail> Details { get; set; }
        public String KEYS { get; set; }
        public String ACCRUED_NO { get; set; }
        public Int32 TRANSACTION_CODE { get; set; }
        public String TRANSACTION_NAME { get; set; }
        public DateTime DOC_DATE { get; set; }
        public String REF_DOC { get; set; }
        public String REF_DOC_YEAR { get; set; }
        public String CURRENCY_CODE { get; set; }
        public String COMPANY_CODE { get; set; }
        public DateTime? POSTING_DATE { get; set; }
        public String WITHOLDING_TAX { get; set; }
        public Int32 FISCAL_YEAR { get; set; }
        public Int32 PERIOD { get; set; }
        public String BOOKING_NO { get; set; }
        public String TRANS_TYPE { get; set; }
        public String DIVISION_NAME { get; set; }
        public String DOC_CURRENCY { get; set; }
        public String COMPANY_CD { get; set; }
        public DateTime POSTED_DATE { get; set; }
        public int? SUSPENSE_NO { get; set; }
        public String WBS_NO { get; set; }
        public String COST_CENTER { get; set; }
        public String SAP_DOC_NO { get; set; }
        public int? SAP_DOC_YEAR { get; set; }
        public String PAID_STS { get; set; }

    }
}