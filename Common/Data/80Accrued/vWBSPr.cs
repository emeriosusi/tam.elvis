﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class vWBSPr
    {
        public string WbsNumber { get; set; }
        public string Description { get; set; }
    }
}
