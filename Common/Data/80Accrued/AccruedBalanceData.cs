﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccruedBalanceData
    {
        public string BOOKING_NO { get; set; }
        public string REFF_NO { get; set; }
        public string WBS_NO_OLD { get; set; }
        public string WBS_NO_PR { get; set; }
        public int PV_TYPE_CD { get; set; }
        public string DIVISION_ID { get; set; }
        public int? SUSPENSE_NO_OLD { get; set; }
        public int? SUSPENSE_NO_NEW { get; set; }
        public DateTime CREATED_DT { get; set; }
        public DateTime? UPDATED_DT { get; set; }
        public int? POSTING_FLAG { get; set; }
        public int? BOOKING_STS { get; set; }
        public DateTime? EXPIRED_DT { get; set; }

        public decimal? INIT_AMT { get; set; }
        public decimal? SPENT_AMT { get; set; }
        public decimal? AVAILABLE_AMT { get; set; }
        public decimal? BLOKING_AMT { get; set; }
        public decimal? OUTSTANDING_AMT { get; set; }
        public decimal? BUDGET_AMT { get; set; }
    }
}
