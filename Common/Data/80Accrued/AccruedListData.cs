﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccruedListData
    {
        readonly private string green = "#00ff00";
        readonly private string red = "#ff0000";
        readonly private string yellow = "#fffb00";

        private string workflow_status;
        public string WORKFLOW_STATUS
        {
            get
            {
                return workflow_status;
            }
            set
            {
                workflow_status = value;
                if (workflow_status == null)
                {
                    return;
                }
                else if (workflow_status.Equals("0"))
                {
                    WORKFLOW_STATUS_COLOR = red;
                }
                else if (workflow_status.Equals("1"))
                {
                    WORKFLOW_STATUS_COLOR = green;
                }
            }
        }
        public string WORKFLOW_STATUS_COLOR { get; set; }

        public int? SEQ_NO { get; set; }
        public string ACCRUED_NO { get; set; }
        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public int STATUS_CD { get; set; }
        public string SUBMISSION_STATUS { get; set; }
        public decimal TOT_AMOUNT { get; set; }
        public string PIC_CURRENT { get; set; }
        public string PIC_NEXT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string MODULE_CD { get; set; }
        public int BUDGET_YEAR { get; set; }
    }
}
