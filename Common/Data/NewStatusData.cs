﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class NewStatusData
    {
        public int STATUS_CD { get; set; }
        public string STATUS_NAME { get; set; }
        public string PAY_METHOD_CD { get; set; } 
    }
}
