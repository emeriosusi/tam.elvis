﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class ComboClassData
    {
        public string COMBO_CD { get; set; }
        public string COMBO_NAME { get; set; }
    }
}
