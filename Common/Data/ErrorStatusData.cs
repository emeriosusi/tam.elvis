﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    namespace Common.Data
    {
        public class ErrorStatusData
        {
            public int Status_CD { get; set; } 

            public string Status_Name { get; set; } 
        }
    }
}
