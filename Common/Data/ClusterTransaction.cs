﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq;
using Common.Function;

namespace Common.Data
{
    [Serializable]
    public class ClusterTransaction
    {
        public string ClusterId { get; set; }
        public string TransactionIdList { get; set; }

        public int[] TransactionId
        {
            get
            {
                List<int> r = new List<int>();
                if (!TransactionIdList.isEmpty())
                    r.AddRange(TransactionIdList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => a.Int()).Distinct());
                return r.ToArray();

            }

        }
    }
}
