﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Data._40RVFormList
{
    [Serializable]
    public class RVApprovalData
    {
        #region Header
        public int RV_NO { get; set; }
        public int RV_YEAR { get; set; }
        public DateTime RV_DATE { get; set; }        
        public String RV_TYPE { get; set; }
        public String STATUS_NAME { get; set; }
        public int STATUS_CD { get; set; }
        public String VENDOR_CD { get; set; }
        public String VENDOR_NAME { get; set; }
        public String TRANSACTION_TYPE { get; set; }
        public String PAYMENT_METHOD { get; set; }
        public String ISSUING_DIVISION { get; set; }
        public String DIVISION_ID { get; set; }
        public DateTime? DUE_DATE { get; set; }
        public String STD_WORDING { get; set; }
        public String HOLD_BY { get; set; }
        public int HOLD_FLAG { get; set; }
        public int? COUNTER_FLAG { get; set; }
        #endregion

        #region Detail
        public String COST_CENTER_CD { get; set; }
        public String COST_CENTER_NAME { get; set; }
        public String DESCRIPTION { get; set; }
        public String CURRENCY { get; set; }
        public String CURRENCY_CD { get; set; }
        public String AMOUNT { get; set; }
        public decimal? AMOUNT_VALUE { get; set; }
        #endregion

    }
}
