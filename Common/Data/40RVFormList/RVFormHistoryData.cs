﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Common.Data._40RVFormList
{
    public class RVFormHistoryData
    {
        public List<RVFormHistoryDetail> Details { get; set; }
        public bool DetailDeleted { get; set; }
        public int? RVNumber { get; set; }
        public int? RVYear { get; set; }
        public int? StatusCode { get; set; }
        public string PaymentMethodCode { get; set; }
        public int? VendorGroupCode { get; set; }
        public string VendorCode { get; set; }
        public int? RVTypeCode { get; set; }
        public int? TransactionCode { get; set; }
        public int? SuspenseNumber { get; set; }
        public string BudgetNumber { get; set; }
        public DateTime? ActivityDate { get; set; }
        public DateTime? ActivityDateTo { get; set; }
        public string DivisionID { get; set; }
        public DateTime? RVDate { get; set; }
        public string TaxCode { get; set; }
        public string TaxInvoiceNumber { get; set; }
        public DateTime? PostingDate { get; set; }
        //public DateTime? PlanningPaymentDate { get; set; }
        public int? BankType { get; set; }
        public bool TaxCalculated { get; set; }
        public bool SingleWbsUsed { get; set; }
        public int Version { get; set; }
        public DateTime? UpdatedDt { get; set; }
        public string UpdatedBy { get; set; }
        public string RVTypeName { get; set; }
        public string VendorGroupName { get; set; }
        public string PaymentMethodName { get; set; }
        public string StatusName { get; set; }
        public string VendorName { get; set; }
        public string TransactionName { get; set; }
        public int? SetNoPv { get; set; }
        public int? SetNoRv { set; get; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? PaidDate { get; set; }
        public DateTime? ReceivedDate { get; set; } 
        public int? WorkflowStatus { get; set; }
        //public int? SettlementStatus { get; set; }
        public decimal? ReferenceNo { get; set; }
        public int? LocationCode { get; set; }
        public DateTime? SubmitHcDocDate { get; set; }
        public string HoldBy { set; get; }
        public int? HoldFlag { get; set; }
        public string ModifiedBy { get; set; }
        public decimal? TotalAmount { get; set; }
    }
}