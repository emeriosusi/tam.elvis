﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._40RVFormList
{
    [Serializable]
    public class RVFormDetailData
    {
        public int SEQ_NO { get; set; }
        public int RV_NO { get; set; }
        public int RV_YEAR { get; set; }
        public string COST_CENTER_CD { get; set; }
        public string CURRENCY_CD { get; set; }
        public string DESCRIPTION { get; set; }
        public float AMOUNT { get; set; }
        public string INVOICE_NO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
