﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class AnnouncementData
    {
        public int INFORMATION_CD { get; set; }
        public string INFORMATION_TITLE { get; set; }
        public string INFORMATION_DESC { get; set; }
        public string DIVISION_LIST { get; set; }
        public string POSITION_LIST { get; set; }
        public DateTime RELEASE_DATE { get; set; }
        public DateTime EXPIRE_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
