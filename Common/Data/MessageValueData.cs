﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class MessageValueData
    {
        private string _VALUE1;
        public string VALUE1
        {
            get { return _VALUE1; }
            set { _VALUE1 = value; }
        }

        private string _VALUE2;
        public string VALUE2
        {
            get { return _VALUE2; }
            set { _VALUE2 = value; }
        }

        private string _VALUE3;
        public string VALUE3
        {
            get { return _VALUE3; }
            set { _VALUE3 = value; }
        }

        private string _VALUE4;
        public string VALUE4
        {
            get { return _VALUE4; }
            set { _VALUE4 = value; }
        }

        private string _VALUE5;
        public string VALUE5
        {
            get { return _VALUE5; }
            set { _VALUE5 = value; }
        }

        private string _VALUE6;
        public string VALUE6
        {
            get { return _VALUE6; }
            set { _VALUE6 = value; }
        }
    }
}
