﻿using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Common.Function; 

namespace Common
{
    public class IniSQL
    {
        IniSections data = null;
        public static readonly string MY_ID = "IniSQL";

        public string QueryPath
        {
            get
            {
                string dir = ConfigurationManager.AppSettings["SQLFilesDir"];
                if (!dir.Contains(":\\"))
                {
                    dir = HttpContext.Current.Server.MapPath(dir);
                }
                return dir;
            }
        }

        public void Init()
        {
            //if (HttpContext.Current != null && HttpContext.Current.Application != null)
            //{
                data = Heap.Get<IniSections>(MY_ID);
                        
                if (data == null)
                {
                    data = new IniSections();
                    
                    //HttpContext.Current.Application.Add(MY_ID, data);
                    Heap.Set<IniSections>(MY_ID, data);

                    var ak = ConfigurationManager.AppSettings.AllKeys.Where(x => x.StartsWith("SQLFiles"));
                    string[] f;
                    if (ak.Any())
                    {
                        f = ak.ToArray();
                        for (int i = 0; i < f.Length; i++)
                        {
                            string qx = f[i];
                            if (f[i].Substring(8).Int(0) > 0)
                            {
                                f[i] = ConfigurationManager.AppSettings[f[i]];
                            }
                            else f[i] = "";
                        }
                    }
                    else
                        f = new string[] { "q.sql" };

                    for (int i = 0; i < f.Length; i++)
                    {
                        if (f[i].isEmpty()) continue;

                        string p = Path.Combine(QueryPath, f[i]);
                        Ini ini = new Ini(p);
                        foreach (string s in ini.Sections)
                        {
                            data[s] = ini.Section[s];
                        }
                    }

                }

            //}
        }

        public IniSQL()
        {
            Init();
        }

        public void ResetCache()
        {
            //if (HttpContext.Current != null && HttpContext.Current.Application != null)
            //{
                // HttpContext.Current.Application[MY_ID] = null;
                Heap.Set<IniSections>(MY_ID, null);
                Init();
            //}
        }
        public string this[string Index]
        {
            get
            {
                /// non value index not acceptable 
                if (Index.isEmpty()) return null;

                if (data == null) 
                    Init();

                // init failed 
                if (data == null) return null;

                if (data != null) 
                {
                    string qq = data[Index];
                    if (!qq.isEmpty())
                        return qq;
                    else 
                    {
                        string t = null;
                        string fn = Path.Combine(QueryPath, CommonFunction.SanitizeFilename(Index) + ".sql");
                        if (File.Exists(fn)) 
                        {
                            t = File.ReadAllText(fn);
                            if (!t.isEmpty())
                            {
                                if (!t.StartsWith("--"))
                                {
                                    t = string.Format("-- {0}\r\n\r\n",  Index) + t;
                                }
                                data[Index] = t;
                            }
                        }
                        return t;
                    }

                }
                else
                    return null;
            }
        }
    }
}
