﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Common;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Diagnostics;

namespace Common
{
    public class LoggingLogic : IDisposable
    {
        private string userId = "";
        private int processId = 0;
        private string _path = GetPath();
        private string _logfile = "";

        protected IDbConnection _DB = null;
        public IDbConnection Db
        {
            get
            {
                if (_DB == null)
                {
                    _DB = new SqlConnection(ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString);
                }
                if ((_DB.State == ConnectionState.Closed) || (_DB.State == ConnectionState.Broken))
                    _DB.Open();
                return _DB;
            }
        }

        public void Dispose()
        {
            if (_DB != null)
            {
                if ((_DB as SqlConnection).State == ConnectionState.Open)
                {
                    _DB.Close();
                }
                _DB.Dispose();
                _DB = null;
            }
        }

        public int PID
        {
            get
            {
                return processId;
            }
            set
            {
                processId = value;
            }
        }

        public static string MaxLenDef(string f, int len, string def = "?")
        {
            return (String.IsNullOrEmpty(f))
                    ? def
                    : ((f.Length > len)
                        ? f.Substring(0, len)
                        : f);
        }

        public int Log(
            string _msgId,
            string _errMessage,
            string _errLocation = "",
            string _userID = "",
            string _functionID = "",
            int ID = 0,
            string _remarks = null,
            string _type = null,
            string _sts = null)
        {
            if (ID < 1 && processId > 0)
                ID = processId;
            else if (ID > 0)
                processId = ID;

            if (string.IsNullOrEmpty(_userID) && !string.IsNullOrEmpty(userId))
            {
                _userID = userId;
            }
            if ((!string.IsNullOrEmpty(_userID)) && String.Compare(userId, _userID, true) != 0)
            {
                userId = _userID;
            }

            DynamicParameters dyn = new DynamicParameters();
            dyn.Add("@what", _errMessage);
            dyn.Add("@user", _userID);
            dyn.Add("@where", _errLocation);
            dyn.Add("@pid", ID, DbType.Int32, ParameterDirection.InputOutput);
            dyn.Add("@id", _msgId);
            dyn.Add("@type", _type);
            dyn.Add("@func", _functionID);
            dyn.Add("@sts", _sts);
            dyn.Add("@rem", _remarks);

            try
            {
                Db.Execute("sp_PutLog", dyn, null, null, CommandType.StoredProcedure);
                ID = dyn.Get<int>("@pid");
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return ID;
        }


        public static bool ForceDirectories(string d)
        {
            bool r = false;
            try
            {
                if (!Directory.Exists(d))
                {
                    Directory.CreateDirectory(d);
                }
                r = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return r;
        }

        private int _seq = 0;
        public void Say(string uid, string word, params object[] x)
        {
            if (_seq < 1)
            {
                if (!ForceDirectories(_path)) return;
            }
            _logfile = Path.Combine(_path, ((string.IsNullOrEmpty(uid)) ? "0.log" : uid + ".log"));
            _seq++;
            try
            {
                File.AppendAllText(_logfile, "\r\n"
                    + tick((x != null) ? String.Format(word, x) : word));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(word);
            }
        }

        public static void err(Exception ex)
        {
            say("except", Trace(ex));
        }

        public static string tick(string s, string fn = null)
        {
            string last = Heap.Get<string>("tick[" + (fn ?? "") + "]");
            string tock = DateTime.Now.ToString("HH:mm:ss");
            if (string.Compare(last, tock, false) == 0)
                tock = new string(' ', 8);
            else
            {
                last = tock;
                Heap.Set<string>("tick[" + (fn ?? "") + "]", last);
            }
            return tock + "    " + s;
        }

        public static string GetPath()
        {
            DateTime n = DateTime.Now;
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
                , AppSetting.LdapDomain, AppSetting.ApplicationID, "log"
                , n.Year.ToString()
                , n.Month.ToString()
                , n.Day.ToString());
        }


        public static string GetTempPath(string username = "", int pid = 0)
        {
            DateTime n = DateTime.Now;
            string p = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            if (!string.IsNullOrEmpty(username))
            {
                string vt = pid.ToString();
                if (pid <= 0)
                {
                    vt = string.Format("{0:yyyyMMdd}", n);
                }
                return Path.Combine(p
                    , AppSetting.LdapDomain, AppSetting.ApplicationID, "tmp"
                    , username
                    , vt
                    );
            }
            else
            {
                return Path.Combine(p
                    , AppSetting.LdapDomain, AppSetting.ApplicationID, "tmp"
                    , n.Year.ToString()
                    , n.Month.ToString()
                    , n.Day.ToString());
            }
        }

        public static void say(string x)
        {
            string path = GetPath();

            string logfile = Path.Combine(path, "0.log");
            try
            {
                if (!ForceDirectories(path)) return;
                File.AppendAllText(logfile, "\r\n"
                    + tick(x));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public static void say(string uid, string word, params object[] x)
        {
            string path = GetPath();

            string logfile = Path.Combine(path, ((string.IsNullOrEmpty(uid)) ? "0.log" : Common.Function.CommonFunction.CleanFilename(uid) + ".log"));

            try
            {
                if (!ForceDirectories(path)) throw new Exception("Cannot create said dir for " + uid);
                File.AppendAllText(logfile, "\r\n"
                    + tick((x != null && x.Length > 0) ? String.Format(word.Replace("|", "\r\n\t"), x) : word, uid));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public static string Trace(Exception ex, string ignoreList = null)
        {
            StackTrace t = (ex == null)
                         ? new StackTrace(1, true)
                         : new StackTrace(ex, 1, true);

            StringBuilder b = new StringBuilder("");

            Exception x = ex;
            int tabs = 0;
            while (x != null && tabs <= 10)
            {
                Type xt = x.GetType();
                string xName = (xt != null) ? "[" + xt.FullName + "]" : "";
                b.AppendFormat("{0} {1} {2}\r\n", new string(' ', 2 * tabs), xName, x.Message);
                x = x.InnerException;
                ++tabs;
            }

            StackFrame[] f = t.GetFrames();
            string[] ign = (!string.IsNullOrEmpty(ignoreList))
                         ? ignoreList.Split(new char[] { ',', ' ', '\t' })
                         : new string[] { };
            if (f != null && f.Length > 0)
                for (int i = 0; i < f.Length; i++)
                {
                    StackFrame sf = f[i];
                    System.Reflection.MethodBase m = sf.GetMethod();

                    string cname = ((m != null && m.ReflectedType != null) ? m.ReflectedType.FullName : "null");

                    if (ign.Contains(cname))
                        continue;

                    b.AppendFormat("{0,4} {1} {2} \r\n", sf.GetFileLineNumber(), cname, m.Name);
                }

            return b.ToString();
        }
    }
}
