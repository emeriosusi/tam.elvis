﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching;
using System.Threading; 

namespace Common
{
    public static class Heap
    {
        public static int Duration = -420; // 7 minutes in heaven

        public static ObjectCache _mc = null;
        public static ObjectCache mc
        {
            get
            {
                if (_mc == null)
                {
                    using (ExecutionContext.SuppressFlow())
                    {
                        _mc = MemoryCache.Default;                        
                    }
                }
                return _mc;
            }
        }

        public static bool Set<T>(string key, T value, int newDuration = -420)
        {
            ObjectCache m = mc;
            int duration = (newDuration != Duration) ? newDuration : Duration;
            
            bool hasit = false;
            if (m.Contains(key))
            {
                hasit = true;
                m.Remove(key);
            }
            if (value != null)
            {
                CacheItemPolicy c = null;
                if (duration <= 0)
                {
                    c = new CacheItemPolicy()
                    {
                        SlidingExpiration = TimeSpan.FromSeconds(-1*((duration==0)?-3:duration))
                    };
                }
                else
                {
                    c = new CacheItemPolicy()
                    {
                        AbsoluteExpiration = DateTime.Now.AddSeconds(duration)
                    };
                }

                m.Add(key, value, c);
            }
            return hasit;
        }

        public static T Get<T>(string key) {
            T value=default(T);
            ObjectCache m = mc;
            try
            {
                if (m.Contains(key))
                {
                    value = (T)m.Get(key);
                }
            }
            catch
            {
            }
            return value;
        }

        public static bool Has(string key)
        {
            return mc.Contains(key);
        }

        public static string[] Keys()
        {
            return mc.Select(a => a.Key).ToArray();
        }

        public static void Clear()
        {
            foreach (var a in mc)
            {
                mc.Remove(a.Key);
            }
        }

        
    }

    
}
