﻿using System;
using System.Web.Services;


namespace ELVISServices
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    [System.Xml.Serialization.XmlInclude(typeof(PVData))]
    [System.Xml.Serialization.XmlInclude(typeof(PVDetailData))]
    public class ELVISServices : System.Web.Services.WebService
    {
        #region 20130419 dan -
        //protected readonly PVData pvdata = new PVData();
        //protected PVDetailData details = new PVDetailData();
        #endregion

        [WebMethod]
        public PVOutput PVSave(string PaymentMethodCode, string VendorCode ,	
            int PVTypeCode ,	int TransactionCode ,	string BudgetNumber ,	
            string ActivityDate ,	string ActivityDateTo ,	string DivisionID ,	
            DateTime PostingDate ,	int BankType , decimal ReffNo, int approvalLevel, 
            string userName, string SuspenceNo,	bool SingleWbsUsed, 
            bool financeHeader, bool submitting, PVDetailData[] Details)
        {
            #region 20130419 dan -
            // PVOutput resultSave = new PVOutput();
           // resultSave =  pvdata.save(PaymentMethodCode, VendorCode, 
           //     PVTypeCode, TransactionCode, BudgetNumber, ActivityDate, 
           //     ActivityDateTo, DivisionID, PostingDate, BankType, ReffNo, 
           //     approvalLevel, userName, SuspenceNo, SingleWbsUsed, 
           //     financeHeader, submitting, Details);          
            //return resultSave;
            #endregion

            return (new VoucherService()).SaveData(
                PaymentMethodCode, 
                VendorCode,
                PVTypeCode, 
                TransactionCode, 
                BudgetNumber, 
                ActivityDate,
                ActivityDateTo, 
                DivisionID, 
                PostingDate, 
                BankType, 
                ReffNo,
                approvalLevel, 
                userName, 
                SuspenceNo, 
                SingleWbsUsed,
                financeHeader, 
                submitting, 
                Details);
        }

        //[WebMethod]
        //public string PVSaveDetail(string[] detailData)
        //{
        //    return "Test Passing List";
        //}

        //[WebMethod]
        //public int getPVCount(PVData[] PV)
        //{
        //    int count = 0;
        //    if (PV != null)
        //    {
        //        count = PV.Count();
        //    }
        //    return count;
        //}
        //[WebMethod]
        //public bool InsertPV(PVData PVHeader, PVDetailData[] PVDetail)
        //{
        //    bool retVal = true;
            
        //    return retVal;
        //}

        
    }
}