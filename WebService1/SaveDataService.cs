﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Function;
using DataLayer.Model;
using Common;

namespace ELVISServices
{
    [Serializable]
    public class VoucherService : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();
        private GlobalResourceData _resource = new GlobalResourceData();

        public PVOutput SaveData(
           string PaymentMethodCode, string VendorCode, int PVTypeCode,
           int TransactionCode, string BudgetNumber,
           string ActivityDate, string ActivityDateTo,
           string DivisionID, DateTime PostingDate,
           int BankType, decimal ReffNo,
           int approveLevel, string userName,
           string SuspenceNo, bool SingleWbsUsed,
           bool financeHeader, bool submitting,
           PVDetailData[] detailData)
        {
            #region Init

            bool Ok = false;
            string FUNC_ID = "SAP_HR";
            string LOCATION = "VoucherService.SaveData";
            int VendorGroupCode = 1;
            int ProcessID = 0;

            int version = 0;
            decimal totalAmount;
            PVDetailData pvd = new PVDetailData();
            PVFormTable pvtab = new PVFormTable();
            DateTime today = DateTime.Now;

            PVOutput r = new PVOutput()
            {
                PV_NO = 0,
                PV_YEAR = 0,
                STATUS_PV = "S",
                ERR_MESSAGE = ""
            };

            PVFormData f = new PVFormData();
            f.isSubmit = true;
            f.PVNumber = null;
            f.PVYear = null;
            f.PaymentMethodCode = PaymentMethodCode;
            f.VendorGroupCode = VendorGroupCode;
            f.VendorCode = VendorCode;
            f.PVTypeCode = PVTypeCode;
            f.TransactionCode = TransactionCode;
            f.BudgetNumber = BudgetNumber;

            f.DivisionID = DivisionID;
            f.PVDate = today;
            f.ReferenceNo = ReffNo;
            f.BankType = BankType;
            f.PostingDate = PostingDate;
            f.UserName = userName;
            if (f.isSubmit)
                f.StatusCode = 10;
            if (financeHeader)
                f.StatusCode = 21;

            if (detailData != null)
                for (int i = 0; i < detailData.Length; i++)
                {
                    PVDetailData d = detailData[i];
                    f.Details.Add(new PVFormDetail()
                    {
                        SequenceNumber = i + 1,
                        ItemTransaction = 1,
                        CostCenterCode = d.CostCenterCode,
                        Description = d.Description,
                        CurrencyCode = d.CurrencyCode,
                        Amount = d.Amount.Dec(),
                        InvoiceNumber = d.InvoiceNumber,
                        WbsNumber = d.WbsNumber
                    });
                }

            submitting = true;

            totalAmount = TotalAmount(detailData);

            UserData u = new UserData()
            {
                USERNAME = userName
            };

            #endregion

            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;

                    bool isDouble = isDoublePost(TransactionCode, ReffNo);

                    ProcessID = logic.Log.Log("INF", "PV from SAP", LOCATION, userName, FUNC_ID);

                    bool isLocking = logic.Lock.IsLocked("SAP_HR_Int", TransactionCode.ToString() + "|" + ReffNo.ToString());

                    if (isLocking == true)
                    {
                        throw new Exception("Data has been lock by another process");
                    }

                    logic.Lock.DoLock(ProcessID, "SAP_HR_Int", TransactionCode.ToString() + "|" + ReffNo.ToString(), userName);

                    if (!string.IsNullOrEmpty(VendorCode))
                    {
                        string fmt = "0000000000.##";
                        int vnd = VendorCode.Int();
                        string vendors = vnd.ToString(fmt);
                        CodeConstant vendorGroup = logic.PVForm.getVendorGroupNameByVendor(vendors);
                        if (vendorGroup.Code == string.Empty)
                        {
                            r.ERR_MESSAGE = "Vendor not found in Master Vendor";
                        }
                        f.VendorGroupCode = vendorGroup.Code.Int();
                        f.VendorCode = VendorCode;
                    }
                    else
                    {
                        f.VendorCode = null;
                    }

                    if (string.IsNullOrEmpty(ActivityDate) || ActivityDate == "0000-00-00")
                    {
                        f.ActivityDate = DateTime.Now;

                    }
                    else
                    {
                        f.ActivityDate = DateTime.Parse(ActivityDate);
                    }
                    if (string.IsNullOrEmpty(ActivityDateTo) || ActivityDateTo == "0000-00-00")
                    {
                        f.ActivityDateTo = DateTime.Now;
                    }
                    else
                    {
                        f.ActivityDateTo = DateTime.Parse(ActivityDateTo);
                    }
                    f.SuspenseNumber = SuspenceNo.Int();
                    int SusNo = SuspenceNo.Int();

                    int SusYe = db.TB_R_PV_H
                                .Where(su => su.PV_NO == f.SuspenseNumber)
                                .Select(x => x.PV_YEAR)
                                .FirstOrDefault();

                    f.SuspenseYear = SusYe;

                    if (isDouble)
                    {
                        throw new Exception("Double Post PV with Transaction Code: "
                            + TransactionCode.ToString()
                            + " Refference No: " + ReffNo.ToString());
                    }
                    else
                    {
                        Ok = logic.PVForm.saveHeader(f, u, financeHeader, out version, db);
                    }


                    if (!financeHeader && Ok)
                    {
                        r.PV_NO = f.PVNumber ?? 0;
                        r.PV_YEAR = f.PVYear ?? 0;
                        
                        if (f.isSubmit)
                        {
                            logic.PVForm.saveOri(f, u, db);
                        }
                        logic.PVForm.delExistingDetail(f, u, db);

                        logic.PVForm.saveDetail(f, u, version, db);

                        /* Amount */
                        logic.PVForm.saveAmounts(f, u, db);

                        db.SaveChanges();
                        logic.Log.Log("INF", "PV " + r.PV_NO + " " + r.PV_YEAR, LOCATION,  userName, FUNC_ID);
                    }

                    Ok = postToK2(
                        r.PV_NO, r.PV_YEAR,
                        DivisionID, userName,
                        TransactionCode, f.StatusCode ?? 0,
                        VendorCode, totalAmount,
                        true, approveLevel);

                    if (!Ok)
                    {
                        logic.Log.Log("ERR", "Post K2 error", LOCATION, userName, FUNC_ID);
                    }
                }
                catch (Exception ex)
                {
                    r.ERR_MESSAGE += ex.Message;
                    r.STATUS_PV = "E";
                    LoggingLogic.err(ex);
                    logic.Log.Log("ERR", ex.Message, "", userName, "SAP_HR");
                }
                finally
                {
                    logic.Lock.DoUnlock(ProcessID);
                }
            }


            return r;
        }

        public decimal TotalAmount(PVDetailData[] detail)
        {
            PVDetailData pvd;
            List<ExchangeRate> lstExchangeRate = logic.PVForm.getExchangeRates();
            decimal totalIDR = 0;
            decimal amount;

            int count = detail.Count();

            for (int i = 0; i < count; i++)
            {
                pvd = detail[i];
                amount = pvd.Amount;

                if (pvd.CurrencyCode.ToUpper().Equals("IDR"))
                {
                    totalIDR += amount;
                }

                else
                {
                    foreach (ExchangeRate rate in lstExchangeRate)
                    {
                        if (rate.CurrencyCode.Equals(pvd.CurrencyCode))
                        {
                            totalIDR += amount * rate.Rate;
                            break;
                        }
                    }
                }
            };

            return totalIDR;
        }

        public bool isDoublePost(int trCode, decimal ReffNo)
        {
            return db.TB_R_PV_H
                    .Where(i => i.TRANSACTION_CD == trCode
                        && i.REFFERENCE_NO == ReffNo).Count() > 0;
        }


        public bool postToK2(
            int PVNumber, int PVYear,
            string divisionCD,
            string UserName,
            int TransCode,
            int statusCD,
            string vendorCD,
            decimal totalAmount,
            bool payingVoucher,
            int appLevel)
        {
            bool k2success = false;
            try
            {
                string moduleCode = "1";

                if (!payingVoucher)
                {
                    moduleCode = "2";
                }
                Dictionary<string, string> submitData = new Dictionary<string, string>();
                submitData.Add("ApplicationID", "ELVIS");
                submitData.Add("ApprovalLevel", "1");
                submitData.Add("ApproverClass", "1");
                submitData.Add("ApproverCount", "1");
                submitData.Add("CurrentDivCD", divisionCD);
                submitData.Add("CurrentPIC", UserName);

                string entertain = "0";
                int? transactionCode = TransCode;
                List<int?> lstEntertainmentTransactions = logic.Persist.getEntertainmentTransactionCodes();
                if (lstEntertainmentTransactions != null)
                {
                    foreach (int? c in lstEntertainmentTransactions)
                    {
                        if (transactionCode == c)
                        {
                            entertain = "1";
                            break;
                        }
                    }
                }
                submitData.Add("Entertain", entertain);

                submitData.Add("K2Host", "");
                submitData.Add("LimitClass", "0");
                submitData.Add("ModuleCD", moduleCode);
                submitData.Add("NextClass", "0");
                submitData.Add("PIC", divisionCD);
                submitData.Add("Reff_No", PVNumber.ToString() + PVYear.ToString());
                string registerDate = string.Format("{0:G}", DateTime.Now);
                submitData.Add("RegisterDt", registerDate);

                PVFormData formData = new PVFormData();
                formData.PVNumber = PVNumber;
                formData.PVYear = PVYear;

                decimal? prevDocStatus = getPreviousDocumentStatus(formData);
                string val = "0";
                if ((prevDocStatus != null) && (prevDocStatus.Value == 0))
                {
                    val = "1";
                }
                submitData.Add("RejectedFinance", val);
                submitData.Add("StatusCD", statusCD.ToString());
                submitData.Add("StepCD", "");
                submitData.Add("VendorCD", vendorCD);
                submitData.Add("TotalAmount", totalAmount.ToString());

                UserData userData = new UserData();
                userData.USERNAME = UserName;
                userData.DIV_CD = decimal.Parse(divisionCD);
                string commands = _resource.GetResxObject("K2ProcID", "Form_Registration_Submit_New");

                k2success = logic.WorkFlow.K2SendCommandWithoutCheckWorklist(PVNumber.ToString() + PVYear.ToString(),
                                                                userData, commands, submitData);

                System.Threading.Thread.Sleep(5000);
                k2success = Approve(PVNumber.ToString() + PVYear.ToString(), UserName, divisionCD, appLevel);
            }
            catch (Exception ex)
            {
                k2success = false;
                LoggingLogic.err(ex);
                throw;
            }
            return k2success;
        }

        public int? getPreviousDocumentStatus(PVFormData formData)
        {
            decimal referenceNumber = decimal.Parse(formData.PVNumber.ToString() + formData.PVYear.ToString());
            var seqNumber = (from t in db.TB_H_DISTRIBUTION_STATUS
                             where (t.REFF_NO == referenceNumber)
                             select t.SEQ_NO);
            if (seqNumber.Any())
            {
                int? statusCode = (from t in db.TB_H_DISTRIBUTION_STATUS
                                   where (t.REFF_NO == referenceNumber) && (t.SEQ_NO == seqNumber.Max())
                                   select t.SEQ_NO).Max();
                return statusCode;
            }

            return null;
        }

        public bool Approve(string _DocNo, string userName, string divCD, int ApproveLvl)
        {
            bool _RetVal = false;
            string posID = string.Empty;
            string _Command = _resource.GetResxObject("K2ProcID", "Form_Registration_Approval");
            int i = 2;

            while (i <= ApproveLvl)
            {
                if (i == 2)
                {
                    posID = "SH";
                }
                else if (i == 3)
                {
                    posID = "DPH";
                }
                else if (i == 4)
                {
                    posID = "DH";
                }
                else if (i == 5)
                {
                    posID = "SGM";
                }

                var userApprove = (from v in db.vw_User
                                   where (v.DIVISION_ID == divCD) && (v.POSITION_ID == posID)
                                   select v.USERNAME).FirstOrDefault();

                UserData userData = new UserData();
                userData.USERNAME = userApprove;

                try
                {
                    Dictionary<string, string> _DataField = new Dictionary<string, string>();
                    _DataField.Add("REFF_NO", _DocNo);
                    _DataField.Add("CurrentPIC", userData.USERNAME);
                    _RetVal = logic.WorkFlow.K2SendCommandWithCheckWorklist(_DocNo, userData, _Command, _DataField);
                }
                catch (Exception Ex)
                {
                    LoggingLogic.err(Ex);
                    throw;
                }

                System.Threading.Thread.Sleep(2000);
                i++;

            }
            return _RetVal;

        }
    }
}