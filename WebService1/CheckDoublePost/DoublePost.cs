﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLayer.Model;
using DataLayer;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Entity;
using System.Data.EntityClient;
using BusinessLogic.VoucherForm;
using BusinessLogic.CommonLogic;
using BusinessLogic;
using Common.Data;
using ELVISServices.FormDataPV;
using System.Collections;

namespace ELVISServices.CheckDoublePost
{
    public class DoublePost
    {

        ELVIS_DBEntities db = new ELVIS_DBEntities();

        public bool isDoublePost(int trCode, decimal ReffNo)
        {            

            var q = db.TB_R_PV_H
                    .Where(i => i.TRANSACTION_CD == trCode
                        && i.REFFERENCE_NO == ReffNo);
            if (q.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }            

        }

    }
}