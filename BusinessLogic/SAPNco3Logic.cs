﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Common;
using Common.Data;
using Common.Data.SAPData;
using Common.Function;
using SAP.Middleware.Connector;
using Common.Data._80Accrued;

namespace BusinessLogic
{
    public class SAPConfigChange : IDestinationConfiguration
    {
        private string SAPUser;
        private string SAPPassword;

        public SAPConfigChange(string _SAPUser, string _SAPPassword)
        {
            this.SAPUser = _SAPUser;
            this.SAPPassword = _SAPPassword;
        }

        #region IDestinationConfiguration Members

        public bool ChangeEventsSupported()
        {
            return false;
        }

        //public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public RfcConfigParameters GetParameters(string destinationName)
        {
            if (destinationName == Common.AppSetting.SAPName)
            {
                RfcConfigParameters _RfcConfigParameters = new RfcConfigParameters();
                _RfcConfigParameters.Add(RfcConfigParameters.User, this.SAPUser);
                _RfcConfigParameters.Add(RfcConfigParameters.Password, this.SAPPassword);
                _RfcConfigParameters.Add(RfcConfigParameters.Client, Common.AppSetting.SAPClient);
                _RfcConfigParameters.Add(RfcConfigParameters.Language, Common.AppSetting.SAPLang);
                _RfcConfigParameters.Add(RfcConfigParameters.AppServerHost, Common.AppSetting.SAPAsHost);
                _RfcConfigParameters.Add(RfcConfigParameters.SystemNumber, Common.AppSetting.SAPSysn);
                _RfcConfigParameters.Add(RfcConfigParameters.PeakConnectionsLimit, Common.AppSetting.SAPMaxPoolSize);
                _RfcConfigParameters.Add(RfcConfigParameters.IdleTimeout, Common.AppSetting.SAPIdleTimeout);
                return _RfcConfigParameters;
            }
            else
                return null;
        }

        RfcDestinationManager.ConfigurationChangeHandler changeHandler;
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged
        {
            add
            {
                changeHandler = value;
            }
            remove
            {
                //do nothing
            }
        }
        public void removeDestination()
        {
            changeHandler("SAPNCO", new RfcConfigurationEventArgs(RfcConfigParameters.EventType.DELETED));
        }
        #endregion
    }

    public class SAPNco3Logic : ISAPLogic
    {
        RfcDestinationManager.ConfigurationChangeHandler changeHandler;
        public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged
        {
            add
            {
                changeHandler = value;
            }
            remove
            {
                //do nothing
            }
        }
        public void removeDestination()
        {
            changeHandler("SAPNCO", new RfcConfigurationEventArgs(RfcConfigParameters.EventType.DELETED));
        }
        private RfcDestination SapRfcDestination;
        private RfcRepository SapRfcRepository;
        private RfcSessionManager SapSession;
        private CommonFunction.Num2Str decFmt = CommonFunction.PostCurr;
        private IDestinationConfiguration _config;

        private LogicFactory logic = LogicFactory.Get();

        protected readonly Common.Function.GlobalResourceData _globalResource = new Common.Function.GlobalResourceData();

        protected void SetPostingNumberFormat()
        {
            switch (AppSetting.POST_NUMBER_FORMAT)
            {
                case 1:
                    decFmt = CommonFunction.DecToStr;
                    break;
                default:
                    break;
            }
        }

        public SAPNco3Logic()
        {
            SapRfcDestination = RfcDestinationManager.GetDestination("SAPNCO");

            SapRfcRepository = SapRfcDestination.Repository;

            errs = new List<string>();

            SetPostingNumberFormat();

        }

        public SAPNco3Logic(string _SAPUser, string _SAPPassword)
        {
            //<add NAME="SAPNCO" USER="sapdirect" PASSWD="toyota" CLIENT="300" LANG="EN" ASHOST="10.16.19.30" SYSNR="30" MAX_POOL_SIZE="10" IDLE_TIMEOUT="10" />
            errs = new List<string>();
            SetPostingNumberFormat();
            _config = new SAPConfigChange(_SAPUser, _SAPPassword);
            try
            {
                RfcDestinationManager.RegisterDestinationConfiguration(_config);
                SapRfcDestination = RfcDestinationManager.GetDestination("SAPNCO");
                RfcSessionManager.BeginContext(SapRfcDestination);
                SapRfcRepository = SapRfcDestination.Repository;
            }
            catch (RfcInvalidStateException e)
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("Invalid State " + e.Message);
            }
            catch (RfcCommunicationException e)
            {
                // network problem...
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("Network Problem " + e.Message);
            }
            catch (RfcLogonException e)
            {
                // user could not logon...
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("Bad Username or password " + e.Message);
            }
            catch (RfcAbapBaseException e)
            {
                // The function module returned an ABAP exception, an ABAP message// or an ABAP class-based exception..
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
                throw new Exception("The function module returned an ABAP exception " + e.Message);
            }
        }

        public void init()
        {
            RfcSessionManager.BeginContext(SapRfcDestination);
            errs.Clear();
        }

        public void del()
        {
            RfcSessionManager.EndContext(SapRfcDestination);
        }

        private void unreg()
        {
            if (_config == null) return;
            try
            {
                RfcDestinationManager.UnregisterDestinationConfiguration(_config);
            }
            catch (Exception ex)
            {
                Err(ex);
            }
        }

        private List<string> errs;
        public string LastMessageText
        {
            get
            {
                if (errs == null || errs.Count < 1)
                    return null;

                return (errs[errs.Count - 1]);
            }
        }

        private string _INF = "MSTD00001INF";
        private LoggingLogic _Log = new LoggingLogic();


        private void CajoToStru(CashJournalTable x, IRfcStructure t)
        {
            t.SetValue("BUSINESS_TRANSACTION", x.BusinessTransaction);
            t.SetValue("AMOUNT", x.Amount.str());
            t.SetValue("TEXT", x.Text);
            t.SetValue("VENDOR", x.Vendor);
            t.SetValue("CUSTOMER", x.Customer);
            t.SetValue("POSTING_DATE", x.PostingDate.SAPdate());
            t.SetValue("DOC_DATE", x.DocDate.SAPdate());
            t.SetValue("REFERENCE", x.Reference);
            t.SetValue("DOC_NO", x.DocNo);
            t.SetValue("INTERNAL_DOC_NO", x.InternalDocNo);
            t.SetValue("ASSIGNMENT", x.Assignment);
            t.SetValue("DOCUMENT_STATUS", x.DocumentStatus);
        }

        private void CajoToStruMaintain(CashJournalTable x, IRfcStructure t)
        {
            t.SetValue("REFERENCE", x.Reference);
            t.SetValue("INTERNAL_DOC_NO", x.InternalDocNo);
            if (!x.Reason.isEmpty())
                t.SetValue("REASON", x.Reason);

        }

        private CashJournalTable CajoFromStru(IRfcStructure x, LogHelper l, string key, string title)
        {
            bool OK = l.Get(x, key);
            if (!OK)
                return null;

            CashJournalTable c = new CashJournalTable()
            {
                BusinessTransaction = l["BUSINESS_TRANSACTION"],
                Amount = l["AMOUNT"].SAPval(),
                Text = l["TEXT"],
                Vendor = l["VENDOR"],
                Customer = l["CUSTOMER"],
                PostingDate = l["POSTING_DATE"].DateNull(),
                DocDate = l["DOC_DATE"].DateNull(),
                Reference = l["REFERENCE"],
                DocNo = l["DOC_NO"],
                InternalDocNo = l["INTERNAL_DOC_NO"],
                Assignment = l["ASSIGNMENT"],
                DocumentStatus = l["DOCUMENT_STATUS"]
            };
            l.Log(key, title);
            return c;
        }

        public CashJournalData CajoRetrieve(CashJournalImportData import, string uid)
        {
            CashJournalData d = new CashJournalData();
            d.Import = import;
            string fn = "ZBAPI_FBCJ_RETRIEVE";

            string _loc = "CajoRetrieve()";
            string fid = _globalResource.FunctionId("FCN_FBCJ_RETRIEVE");
            int pid = _Log.Log(_INF, "Start " + fn, _loc, uid, fid, 0);
            try
            {
                init();

                LogHelper lo = new LogHelper(_Log, _loc,
                    new string[] {
                    "IMPORT",
                        "COMPANY_CODE,CASH_JOURNAL,PERIOD_FROM,PERIOD_TO",
                    "EXPORT",
                        "OPENING_BALANCE,TOTAL_CASH_RECEIPTS,NUMBER_CASH_RECEIPTS,TOTAL_CHECK_RECEIPTS,"
                        + "NUMBER_CHECK_RECEIPTS,TOTAL_CASH_PAYMENTS,NUMBER_CASH_PAYMENTS,"
                        + "CLOSING_BALANCE,CASH,LOCK_STATUS,E_MESSAGE",
                    "TABLES",
                        "BUSINESS_TRANSACTION,AMOUNT,TEXT,VENDOR,"
                        + "CUSTOMER,POSTING_DATE,DOC_DATE,REFERENCE,DOC_NO,INTERNAL_DOC_NO,ASSIGNMENT,DOCUMENT_STATUS",
                    }, uid, pid);

                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                f.SetValue("COMPANY_CODE", "TAM");
                f.SetValue("CASH_JOURNAL", import.CashJournal);
                f.SetValue("PERIOD_FROM", import.PeriodFrom.SAPInDate());
                f.SetValue("PERIOD_TO", import.PeriodTo.SAPInDate());

                lo.Put("IMPORT",
                        "TAM",
                        import.CashJournal,
                        import.PeriodFrom.SAPInDate(),
                        import.PeriodTo.SAPInDate());

                _Log.Log(_INF, "Invoke " + fn);
                f.Invoke(SapRfcDestination);

                CashJournalExportData cje = d.Export;
                lo.Get(f, "EXPORT");

                cje.OpeningBalance = lo["OPENING_BALANCE"].Dec();
                cje.TotalCashReceipt = lo["TOTAL_CASH_RECEIPTS"].Dec();
                cje.NumberCashReceipt = lo["NUMBER_CASH_RECEIPTS"].Int();
                cje.TotalCheckReceipt = lo["TOTAL_CHECK_RECEIPTS"].Dec();
                cje.NumberCheckReceipt = lo["NUMBER_CHECK_RECEIPTS"].Int();
                cje.TotalCashPayment = lo["TOTAL_CASH_PAYMENTS"].Dec();
                cje.NumberCashPayment = lo["NUMBER_CASH_PAYMENTS"].Int();
                cje.ClosingBalance = lo["CLOSING_BALANCE"].Dec();
                cje.Cash = lo["CASH"].Dec();
                cje.LockStatus = lo["LOCK_STATUS"];
                cje.eMessage = lo["E_MESSAGE"];

                lo.Put("EXPORT");

                List<CashJournalTable> tp = d.CashPayments;
                IRfcTable p = f.GetTable("CASH_PAYMENTS");
                for (int i = 0; i < p.Count; i++)
                {
                    CashJournalTable t = CajoFromStru(p[i], lo, "TABLES", "CASH_PAYMENTS");
                    tp.Add(t);
                }

                List<CashJournalTable> tr = d.CashReceipts;
                IRfcTable r = f.GetTable("CASH_RECEIPTS");
                for (int i = 0; i < r.Count; i++)
                {
                    CashJournalTable t = CajoFromStru(r[i], lo, "TABLES", "CASH_RECEIPTS");
                    tr.Add(t);
                }
            }
            catch (Exception ex)
            {
                Err(ex);
                throw;
            }
            finally
            {
                unreg();
                del();
            }
            return d;
        }

        public List<CashJournalMessage> CajoMaintain(
            CashJournalImportData import,
            List<CashJournalTable> tables,
            string uid)
        {
            List<CashJournalMessage> m = new List<CashJournalMessage>();

            string fn = "ZBAPI_FBCJ_MAINTAIN";

            string _loc = "CajoMaintain()";
            string fid = _globalResource.FunctionId("FCN_FBCJ_RETRIEVE");
            int pid = _Log.Log(_INF, "Start " + fn, _loc, uid, fid, 0);
            try
            {
                init();

                LogHelper lo = new LogHelper(_Log, _loc,
                    new string[] {
                    "IMPORT",
                        "COMPANY_CODE,CASH_JOURNAL,CASH_TYPE,TRANS_TYPE",
                    "DETAIL",
                        "BUSINESS_TRANSACTION,AMOUNT,TEXT,VENDOR,"
                        + "CUSTOMER,POSTING_DATE,DOC_DATE,REFERENCE,DOC_NO,INTERNAL_DOC_NO,ASSIGNMENT,DOCUMENT_STATUS",
                    "MESSAGE",
                        "BUSINESS_TRANSACTION,AMOUNT,TEXT,VENDOR,CUSTOMER,"
                        + "REFERENCE,INTERNAL_DOC_NO,MESSAGE",
                    "DELETE",
                        "REFERENCE,INTERNAL_DOC_NO",
                    "REVERSE",
                        "REFERENCE,INTERNAL_DOC_NO,REASON"
                    }, uid, pid);

                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                f.SetValue("COMPANY_CODE", "TAM");
                f.SetValue("CASH_JOURNAL", import.CashJournal);
                f.SetValue("CASH_TYPE", import.CashType);
                f.SetValue("TRANS_TYPE", import.TransType);
                IRfcTable details = f.GetTable("DETAIL");
                IRfcStructure tx;

                lo.Put("IMPORT",
                        "TAM",
                        import.CashJournal,
                        import.CashType,
                        import.TransType);

                for (int i = 0; i < tables.Count; i++)
                {
                    tx = SapRfcRepository.GetStructureMetadata("ZBAPI_FBCJ_DETAIL").CreateStructure();
                    CashJournalTable t = tables[i];


                    if (import.TransType.Int() == (int)CajoTrans.Save)
                    {
                        lo.Put("DETAIL",
                            t.BusinessTransaction,
                            t.Amount,
                            t.Text,
                            t.Vendor,
                            t.Customer,
                            t.PostingDate.SAPdate(),
                            t.DocDate.SAPdate(),
                            t.Reference,
                            t.DocNo,
                            t.InternalDocNo,
                            t.Assignment,
                            t.DocumentStatus);

                        CajoToStru(t, tx);
                    }
                    else if (import.TransType.Int() == (int)CajoTrans.Delete
                        || import.TransType.Int() == (int)CajoTrans.Post)
                    {
                        lo.Log("DELETE", "Delete",
                           t.Reference,
                           t.InternalDocNo);

                        CajoToStruMaintain(t, tx);
                    }
                    else if (import.TransType.Int() == (int)CajoTrans.Reverse)
                    {
                        lo.Log("REVERSE", "Reverse",
                            t.Reference,
                            t.InternalDocNo,
                            t.Reason);

                        CajoToStruMaintain(t, tx);
                    }
                    details.Append(tx);
                }
                f.SetValue("DETAIL", details);

                _Log.Log(_INF, "Invoke " + fn);
                f.Invoke(SapRfcDestination);

                List<CashJournalTable> tp = new List<CashJournalTable>();
                IRfcTable dt = f.GetTable("DETAIL");
                for (int i = 0; i < dt.Count; i++)
                {
                    CashJournalTable det = CajoFromStru(dt[i], lo, "DETAIL", "DETAIL");

                    tp.Add(det);
                }


                IRfcTable r = f.GetTable("MESSAGE");
                for (int i = 0; i < r.Count; i++)
                {
                    IRfcStructure X = r[i];
                    lo.Get(X, "MESSAGE");
                    CashJournalMessage mx = new CashJournalMessage()
                    {
                        BusinessTransaction = lo["BUSINESS_TRANSACTION"],
                        Amount = lo["AMOUNT"].SAPval(),
                        Text = lo["TEXT"],
                        Vendor = lo["VENDOR"],
                        Customer = lo["CUSTOMER"],
                        Reference = lo["REFERENCE"],
                        InternalDocNo = lo["INTERNAL_DOC_NO"],
                        Message = lo["MESSAGE"]
                    };
                    m.Add(mx);

                    lo.Put("MESSAGE");
                }
            }
            catch (Exception ex)
            {
                Err(ex);
                throw;
            }
            finally
            {
                unreg();
                del();
            }
            return m;
        }
        
        public List<PVPostSAPResult> GetResultPostPVToSAP(
            List<PVPostInputHeader> _PVPostInputHeader,
            List<PVPostInputDetail> _PVPostInputDetail,
            List<OneTimeVendorData> _vendorData,
            int _ProcessId,
            string uid,
            ref string errorMessage)
        {
            string _loc = "GetResultPostPVToSAP()";
            string fid = _globalResource.FunctionId("FCN_PV_POST_TO_SAP");
            FiscalDocs f = new FiscalDocs();
            string fn = "ZBAPI_POST_PV";
            List<PVPostSAPResult> _PVPostSAPResult = null;
            LogHelper lo = new LogHelper(_Log, _loc, new string[]{
                "ZBAPI_HDR_POST_PV",

                      "PV_NO,PV_YEAR,ITEM_NO,PV_DATE,PV_TYPE,PV_NO_SUSPENSE,PV_YEAR_SUSPENSE,"
                    + "TRANS_TYPE,VENDOR,VENDOR_GRP,INVOICE_NO,TAX_NO,PAYMENT_TERM,"
                    + "PAYMENT_METHOD,PLAN_PAYMENT_DT,POSTING_DT,TOTAL_AMOUNT,DPP_AMOUNT,CURRENCY,"
                    + "TAX_CODE,HEADER_TEXT,BANK_TYPE,FROM_CURR,EXCHANGE_RATE,EXRATE_SUSPENSE,"
                    + "DIFF_EXRATE,NO_SUSPENSE_FLAG,COST_CENTER_FLAG,TAX_ASSIGNMENT,"
                    + "ACC_INF_ASSIGNMENT,TAX_DT"
                    ,

                "ZBAPI_IN_POST_PV",
                    "PV_NO,PV_YEAR,ITEM_NO,GL_ACCOUNT,AMOUNT,COST_CENTER"
                    + ",WBS_ELEMENT,ITEM_TEXT",

                "ZBAPI_ONE_TIME_VENDOR",
                    "PV_NO,PV_YEAR,TITLE,NAME1,NAME2,STREET,CITY,BANK_KEY,BANK_ACCOUNT",

                "HEADER",
                    "PV_NO,PV_YEAR,INVOICE_NO,CURRENCY,STATUS,SAP_DOC_NO,SAP_DOC_YEAR,ITEM_NO",

                "MESSAGE",
                    "PV_NO,PV_YEAR,ITEM_NO,MESSAGE_TEXT"
            }, uid, _ProcessId);
            try
            {
                init();
                IRfcFunction BAPIFunction = SapRfcRepository.CreateFunction(fn);
                #region headers
                IRfcTable headers = BAPIFunction.GetTable("HEADER");
                IRfcStructure header;

                string fiscalYear;
                for (int i = 0; i < _PVPostInputHeader.Count; i++)
                {
                    PVPostInputHeader h = _PVPostInputHeader[i];
                    fiscalYear = logic.Sys.FiscalYear(h.PV_DATE.dateSAP()).str();
                    f.Add(h.PV_NO.Int(), h.PV_YEAR.Int(), fiscalYear.Int());
                    int fsYear = 0;
                    if (h.PV_DATE_SUSPENSE != null)
                        fsYear = logic.Sys.FiscalYear((DateTime)h.PV_DATE_SUSPENSE);

                    header = SapRfcRepository.GetStructureMetadata("ZBAPI_HDR_POST_PV").CreateStructure();
                    bool hasFraction = !AppSetting.RoundCurrency.Contains(h.CURRENCY_CD);
                    lo.Set(header, "ZBAPI_HDR_POST_PV",
                            h.PV_NO,
                            fiscalYear,
                            h.SEQ_NO.str(),
                            h.PV_DATE,
                            h.PV_TYPE,
                            h.PV_NO_SUSPENSE,
                            fsYear.str(),
                            h.TRANS_TYPE,
                            h.VENDOR,
                            h.VENDOR_GROUP,
                            h.INVOICE_NO,
                            h.TAX_NO, // ((h.TAX_CD ?? "").Equals("V1")) ? h.TAX_NO: "",  /// removed logic for TAM 
                            h.PAYMENT_TERM,
                            h.PAYMENT_METHOD,
                            h.PLAN_PAYMENT_DATE,
                            h.POSTING_DATE,
                            // only settlement not rounding total amount 
                            (string.Compare(h.PV_TYPE, "3") == 0) ? h.TOTAL_AMOUNT.str() : h.TOTAL_AMOUNT.SAPdecfmt(hasFraction),
                            h.DPP_AMOUNT.SAPdecfmt(hasFraction),
                            h.CURRENCY_CD,
                            h.TAX_CD,
                            h.HEADER_TEXT,
                            h.BANK_TYPE,
                            h.FROM_CURR,
                            h.EXCHANGE_RATE.str(),
                            h.EXRATE_SUSPENSE == null ? "" : h.EXRATE_SUSPENSE.ToString(),
                            h.DIFF_EXRATE.SAPdecfmt(),
                            h.NO_SUSPENSE_FLAG.str(),
                            (h.COST_CENTER_FLAG ?? 0).str(),
                            h.TAX_ASSIGNMENT,
                            h.ACC_INV_ASSIGNMENT,
                            h.TAX_DT.SAPdate()
                            );
                    headers.Append(header);
                    lo.Put("ZBAPI_HDR_POST_PV");

                }

                #endregion

                #region inputDetail
                IRfcTable details = BAPIFunction.GetTable("DETAIL");
                IRfcStructure detail;
                
                for (int i = 0; i < _PVPostInputDetail.Count; i++)
                {
                    PVPostInputDetail d = _PVPostInputDetail[i];

                    decimal amount = 0;

                    if (d.PV_TYPE == 3)
                    {
                        if (d.SPENT_AMOUNT.HasValue)
                        {
                            amount = d.SPENT_AMOUNT.Value;
                        }
                    }
                    else
                    {
                        if (d.AMOUNT.HasValue)
                        {
                            amount = d.AMOUNT.Value;
                        }
                    }

                    fiscalYear = logic.Sys.FiscalYear(d.PV_DATE).str();
                    f.Add(d.PV_NO.Int(), d.PV_YEAR.Int(), fiscalYear.Int());

                    detail = SapRfcRepository.GetStructureMetadata("ZBAPI_IN_POST_PV").CreateStructure();
                    bool frac = !AppSetting.RoundCurrency.Contains(d.CURRENCY_CD);

                    lo.Set(detail, "ZBAPI_IN_POST_PV",
                        d.PV_NO,
                        fiscalYear,
                        d.SEQ_NO.str(),
                        d.GL_ACCOUNT,
                        amount.SAPdecfmt(frac),
                        d.COST_CENTER,
                        d.WBS_ELEMENT,
                        d.ITEM_TEXT);

                    details.Append(detail);

                    lo.Put("ZBAPI_IN_POST_PV");
                }
                #endregion

                #region inputVendors
                IRfcTable vendors = null;
                if (_vendorData != null && _vendorData.Count > 0)
                    vendors = BAPIFunction.GetTable("ONE_TIME_VENDOR");
                IRfcStructure v;
                if (_vendorData != null)
                {
                    for (int i = 0; i < _vendorData.Count; i++)
                    {
                        OneTimeVendorData d = _vendorData[i];
                        int fYear = f.Fiscal(d.DOC_NO, d.DOC_YEAR);

                        v = SapRfcRepository.GetStructureMetadata("ZBAPI_ONE_TIME_VENDOR").CreateStructure();

                        lo.Set(v, "ZBAPI_ONE_TIME_VENDOR",
                            d.DOC_NO.str(),
                            fYear.str(),
                            d.TITLE,
                            d.NAME1,
                            d.NAME2,
                            d.STREET,
                            d.CITY,
                            d.BANK_KEY,
                            d.BANK_ACCOUNT);

                        vendors.Append(v);
                        lo.Put("ZBAPI_ONE_TIME_VENDOR");
                    }
                }
                #endregion

                BAPIFunction.SetValue("HEADER", headers);
                BAPIFunction.SetValue("DETAIL", details);
                if (vendors != null && vendors.Count > 0)
                    BAPIFunction.SetValue("ONE_TIME_VENDOR", vendors);
                _Log.Log(_INF, "Invoke " + fn);
                BAPIFunction.Invoke(SapRfcDestination);

                #region Collect the result
                IRfcTable BAPIResult;
                PVPostSAPResult _r = null;
                BAPIResult = BAPIFunction.GetTable("HEADER");
                if (BAPIResult.Count > 0)
                {
                    _PVPostSAPResult = new List<PVPostSAPResult>();
                    for (int i = 0; i < BAPIResult.Count; i++)
                    {
                        IRfcStructure x = BAPIResult[i];
                        lo.Get(x, "HEADER");


                        _r = new PVPostSAPResult()
                        {
                            PV_NO = lo["PV_NO"],
                            INVOICE_NO = lo["INVOICE_NO"],
                            CURRENCY_CD = lo["CURRENCY"],
                            MESSAGE = lo["STATUS"],
                            SAP_DOC_NO = lo["SAP_DOC_NO"],
                            SAP_DOC_YEAR = lo["SAP_DOC_YEAR"],
                            ITEM_NO = lo["ITEM_NO"]
                        };


                        _r.PV_YEAR = f.Doc(_r.PV_NO.Int(), lo["PV_YEAR"].Int()).str();
                        _PVPostSAPResult.Add(_r);

                        lo.Put("HEADER");

                    }
                }
                #endregion
                string _Message = string.Empty;
                IRfcTable BAPIResult1 = BAPIFunction.GetTable("MESSAGE");
                if (BAPIResult1.Count > 0)
                {
                    for (int i = 0; i < BAPIResult1.Count; i++)
                    {
                        IRfcStructure mi = BAPIResult1[i];
                        lo.Get(mi, "MESSAGE");

                        FillPostMessage(_PVPostSAPResult
                            , lo["PV_NO"]
                            , f.Doc(_r.PV_NO.Int(), lo["PV_YEAR"].Int()).str()
                            , lo["ITEM_NO"]
                            , lo["MESSAGE_TEXT"]);
                        string m = lo.Text("MESSAGE", "MESSAGE");

                        _Message += m;


                        lo.Put("MESSAGE");

                        errorMessage = lo["MESSAGE_TEXT"];
                        errs.Add(errorMessage);
                    }
                }

            }
            catch (Exception ex)
            {
                Err(ex);
                throw;
            }
            finally
            {
                unreg();
                del();

            }
            return _PVPostSAPResult;
        }


        private void FillPostMessage(List<PVPostSAPResult> l,
                string pvNo, string pvYear, string itemNo, string messageText)
        {
            var qmsg = l.Where(a => a.PV_NO == pvNo && a.PV_YEAR == pvYear && a.ITEM_NO == itemNo);
            PVPostSAPResult PR = null;
            if (qmsg.Any())
            {
                PR = qmsg.FirstOrDefault();
            }
            if (PR != null)
            {
                PR.MESSAGE_TEXT = (PR.MESSAGE_TEXT.isEmpty() ? "" : PR.MESSAGE_TEXT + ";") + messageText;
            }
            else
            {
                PR = new PVPostSAPResult()
                {
                    PV_NO = pvNo,
                    PV_YEAR = pvYear,
                    ITEM_NO = itemNo,
                    MESSAGE_TEXT = messageText
                };
                l.Add(PR);
            }

        }

        public List<PVPostSAPResult> ReversePV(ref List<PVReverseInput> _i, string uid = "")
        {
            List<PVPostSAPResult> r = new List<PVPostSAPResult>();
            PVReverseInput _o;
            string _loc = "ReversePV";

            string fid = _globalResource.FunctionId("FCN_PV_REVERSE");
            int pid = _Log.Log(_INF, "Start " + _loc, _loc, uid, fid, 0);

            String fn = "ZBAPI_REVERSE_PV";
            LogHelper lo = new LogHelper(
                _Log, _loc, new string[] {
                "ZBAPI_IN_REVERSE_PV",
                    "PV_NO,PV_YEAR,ITEM_NO,POSTING_DT",
                "INPUT",
                    "PV_NO,PV_YEAR,ITEM_NO,POSTING_DT,STATUS,SAP_DOC_NO,SAP_DOC_YEAR",
                "MESSAGE",
                    "PV_NO,PV_YEAR,ITEM_NO,MESSAGE_TEXT"
                }, uid, 0);
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inTable = f.GetTable("INPUT");
                for (int i = 0; i < _i.Count; i++)
                {
                    IRfcStructure _in;
                    _in = SapRfcRepository.GetStructureMetadata("ZBAPI_IN_REVERSE_PV").CreateStructure();

                    lo.Set(_in, "ZBAPI_IN_REVERSE_PV"
                        , _i[i].PV_NO
                        , _i[i].PV_YEAR
                        , _i[i].ITEM_NO
                        , _i[i].POSTING_DT);

                    _inTable.Append(_in);

                    lo.Put("ZBAPI_IN_REVERSE_PV");
                }
                f.SetValue("INPUT", _inTable);
                _Log.Log(_INF, "Invoke " + fn);
                f.Invoke(SapRfcDestination);

                IRfcTable rIn;
                rIn = f.GetTable("INPUT");
                if (rIn.Count > 0)
                {
                    for (int i = 0; i < rIn.Count; i++)
                    {
                        _o = new PVReverseInput();
                        IRfcStructure x = rIn[i];
                        lo.Get(x, "INPUT");

                        _o.PV_NO = lo["PV_NO"];
                        _o.PV_YEAR = lo["PV_YEAR"];
                        _o.ITEM_NO = lo["ITEM_NO"];
                        _o.POSTING_DT = lo["POSTING_DT"];
                        _o.STATUS = lo["STATUS"];
                        _o.SAP_DOC_NO = lo["SAP_DOC_NO"];
                        _o.SAP_DOC_YEAR = lo["SAP_DOC_YEAR"];

                        PVReverseInput ra = (from rx in _i
                                             where rx.PV_NO == _o.PV_NO
                                             && rx.PV_YEAR == _o.PV_YEAR
                                             && rx.ITEM_NO == _o.ITEM_NO
                                             select rx).FirstOrDefault();
                        if (ra != null) ra.STATUS = _o.STATUS;

                        if (_o.STATUS.Equals("S"))
                        {
                            r.Add(new PVPostSAPResult()
                            {
                                PV_NO = _o.PV_NO,
                                PV_YEAR = _o.PV_YEAR,
                                ITEM_NO = _o.ITEM_NO,
                                STATUS_MESSAGE = "S",
                                SAP_CLEARING_DOC_NO = _o.SAP_DOC_NO,
                                SAP_CLEARING_DOC_YEAR = _o.SAP_DOC_YEAR
                            });
                        }
                        lo.Put("INPUT");

                    }
                }

                IRfcTable m = f.GetTable("MESSAGE");
                string _Message = "";
                for (int i = 0; i < m.Count; i++)
                {
                    PVPostSAPResult O = null;
                    IRfcStructure x = m[i];

                    string no, yy, item;
                    no = lo["PV_NO"];
                    yy = lo["PV_YEAR"];
                    item = lo["ITEM_NO"];
                    bool isNew = false;
                    O = r.Where(p => p.PV_NO.Equals(no) && p.PV_YEAR.Equals(yy) && p.ITEM_NO.Equals(item)).FirstOrDefault();
                    if (O == null)
                    {
                        O = new PVPostSAPResult()
                        {
                            PV_NO = no,
                            PV_YEAR = yy,
                            ITEM_NO = item
                        };
                        isNew = true;
                    }

                    lo.Get(x, "MESSAGE");
                    O.MESSAGE_TEXT = lo["MESSAGE_TEXT"];
                    if (isNew)
                        r.Add(O);

                    lo.Put("MESSAGE");
                    string msg = fn + lo.Text("MESSAGE", "Message");

                    errs.Add(O.MESSAGE_TEXT);
                    _Message += msg;
                }
            }
            catch (Exception ex)
            {
                Err(ex);
                r.Add(new PVPostSAPResult()
                {
                    PV_NO = "",
                    PV_YEAR = "",
                    ITEM_NO = "",
                    MESSAGE_TEXT = ex.Message
                });
            }
            finally
            {
                unreg();
                del();

            }
            return r;
        }

        public List<RVCheckHeader> CheckRV(
            List<RVCheckHeader> rvHeader
            , List<RVCheckDetail> rvDetail
            , ref List<RVCheckResult> results
            , int pid
            , int method)
        {
            List<RVCheckHeader> r = new List<RVCheckHeader>();
            string _loc = "CHECK_RV(" + method + ")";
            string fid = _globalResource.FunctionId("FCN_RV_CHECK_SAP");
            string fn = "ZBAPI_CHECK_RV";
            _Log.Log(_INF, "Start " + fn, _loc, "", fid, 0);
            LogHelper lo = new LogHelper(_Log, "CheckRV()", new string[] {
                    "INPUT",
                        "RV_NO,RV_YEAR,ITEM_NO,RV_DATE,RV_TYPE,TRANS_TYPE"
                        + ",VENDOR_NO,POSTING_DT,AMOUNT,CURRENCY,TEXT,METHODE_FLAG"
                        + ",PV_NO_SUSPENSE,PV_YEAR_SUSPENSE,ITEM_NO_SUSPENSE"
                        + ",FROM_CURR,EXRATE_SUSPENSE,EXCHANGE_RATE,DIFF_EXRATE"
                        + ",REFUND_FLAG,TEMP_ACC_AMOUNT,TEMP_ACC_ORI_AMOUNT"
                        + ",COST_CENTER_FLAG"
                        + ",STATUS,SAP_DOC_NO,SAP_DOC_YEAR",
                    "DETAIL",
                        "RV_NO,RV_YEAR,ITEM_NO,GL_ACCOUNT,AMOUNT,COST_CENTER"
                        + ",WBS_ELEMENT,ITEM_TEXT",
                    "MESSAGE",
                        "RV_NO,RV_YEAR,ITEM_NO,MESSAGE_TEXT"
            }, "", pid);
            try
            {
                init();
                IRfcFunction BAPIFunction = SapRfcRepository.CreateFunction(fn);
                #region _inputHeader
                IRfcTable _inputHeader = BAPIFunction.GetTable("INPUT");
                IRfcStructure _h;
                SystemSettingLogic ss = new SystemSettingLogic();

                for (int i = 0; i < rvHeader.Count; i++)
                {
                    RVCheckHeader h = rvHeader[i];
                    string sYear = "";
                    if (h.PV_DATE_SUSPENSE != null)
                    {
                        int fYear = logic.Sys.FiscalYear((DateTime)h.PV_DATE_SUSPENSE);
                        sYear = fYear.str();
                    }

                    _h = SapRfcRepository.GetStructureMetadata("ZBAPI_HDR_CHECK_RV").CreateStructure();

                    lo.Set(_h, "INPUT",
                        h.RV_NO, h.RV_YEAR, h.ITEM_NO,
                        h.RV_DATE, h.RV_TYPE, h.TRANS_TYPE,
                        h.VENDOR_NO,
                        h.POSTING_DT, h.AMOUNT, h.CURRENCY, h.TEXT, method.str(),
                        h.PV_NO_SUSPENSE, sYear, h.ITEM_NO_SUSPENSE,
                        h.FROM_CURR, h.EXRATE_SUSPENSE, h.EXCHANGE_RATE,
                        h.DIFF_EXRATE,
                        h.REFUND_FLAG, h.TEMP_ACC_AMOUNT, h.TEMP_ACC_ORI_AMOUNT,
                        (h.COST_CENTER_FLAG ?? 0).str()
                        );

                    _inputHeader.Append(_h);

                    lo.Log("INPUT", "ZBAPI_HDR_CHECK_RV");
                }
                #endregion


                #region InputDetail
                IRfcTable _inputDetail = BAPIFunction.GetTable("DETAIL");
                IRfcStructure _d;

                for (int i = 0; i < rvDetail.Count; i++)
                {
                    RVCheckDetail d = rvDetail[i];
                    _d = SapRfcRepository.GetStructureMetadata("ZBAPI_IN_POST_RV").CreateStructure();
                    lo.Set(_d, "DETAIL",
                        d.RV_NO,
                        d.RV_YEAR,
                        d.ITEM_NO,
                        d.GL_ACCOUNT,
                        d.AMOUNT,
                        d.COST_CENTER,
                        d.WBS_ELEMENT,
                        d.ITEM_TEXT);

                    _inputDetail.Append(_d);

                    lo.Log("DETAIL", "ZBAPI_IN_POST_RV");
                }
                #endregion

                BAPIFunction.SetValue("INPUT", _inputHeader);
                if (method == 2)
                {
                    BAPIFunction.SetValue("DETAIL", _inputDetail);
                }

                _Log.Log(_INF, "Invoke " + fn);
                BAPIFunction.Invoke(SapRfcDestination);
                results = new List<RVCheckResult>();
                #region Collect the result
                IRfcTable BAPIResult;

                BAPIResult = BAPIFunction.GetTable("INPUT");
                if (BAPIResult.Count > 0)
                {

                    for (int i = 0; i < BAPIResult.Count; i++)
                    {
                        RVCheckHeader a = new RVCheckHeader();
                        IRfcStructure X = BAPIResult[i];

                        lo.Get(X, "INPUT");

                        a.RV_NO = lo["RV_NO"];
                        a.RV_YEAR = lo["RV_YEAR"];
                        a.ITEM_NO = lo["ITEM_NO"];
                        a.RV_DATE = lo["RV_DATE"];
                        a.RV_TYPE = lo["RV_TYPE"];
                        a.TRANS_TYPE = lo["TRANS_TYPE"];
                        a.VENDOR_NO = lo["VENDOR_NO"];
                        a.POSTING_DT = lo["POSTING_DT"];
                        a.AMOUNT = lo["AMOUNT"];
                        a.CURRENCY = lo["CURRENCY"];
                        a.TEXT = lo["TEXT"];
                        a.METHODE_FLAG = lo["METHODE_FLAG"];
                        a.PV_NO_SUSPENSE = lo["PV_NO_SUSPENSE"];
                        a.PV_YEAR_SUSPENSE = lo["PV_YEAR_SUSPENSE"];
                        a.ITEM_NO_SUSPENSE = lo["ITEM_NO_SUSPENSE"];

                        a.FROM_CURR = lo["FROM_CURR"];
                        a.EXRATE_SUSPENSE = lo["EXRATE_SUSPENSE"];
                        a.EXCHANGE_RATE = lo["EXCHANGE_RATE"];
                        a.REFUND_FLAG = lo["REFUND_FLAG"];
                        a.TEMP_ACC_AMOUNT = lo["TEMP_ACC_AMOUNT"];
                        a.TEMP_ACC_ORI_AMOUNT = lo["TEMP_ACC_ORI_AMOUNT"];

                        a.STATUS = lo["STATUS"];
                        a.SAP_DOC_NO = lo["SAP_DOC_NO"];
                        a.SAP_DOC_YEAR = lo["SAP_DOC_YEAR"];

                        r.Add(a);
                        lo.Log("INPUT", "CHECK RV RESULT");
                        RVCheckResult CR = results.Where(g => g.RV_NO == lo["RV_NO"]
                            && g.RV_YEAR == lo["RV_YEAR"]
                            && g.ITEM_NO == lo["ITEM_NO"]).FirstOrDefault();
                        if (CR == null)
                        {
                            results.Add(new RVCheckResult()
                            {
                                RV_NO = lo["RV_NO"],
                                RV_YEAR = lo["RV_YEAR"],
                                ITEM_NO = lo["ITEM_NO"],
                                STATUS = lo["STATUS"],
                                SAP_DOC_NO = lo["SAP_DOC_NO"].Int(),
                                SAP_DOC_YEAR = lo["SAP_DOC_YEAR"].Int(),
                            });
                        }

                    }
                }
                #endregion

                IRfcTable BAPImsg = BAPIFunction.GetTable("MESSAGE");

                if (BAPImsg.Count > 0)
                {

                    string _Message = string.Empty;
                    for (int i = 0; i < BAPImsg.Count; i++)
                    {
                        IRfcStructure x = BAPImsg[i];
                        lo.Get(x, "MESSAGE");
                        var qm = results.Where(
                            c => c.RV_YEAR == lo["RV_YEAR"]
                            && c.RV_NO == lo["RV_NO"]
                            && c.ITEM_NO == lo["ITEM_NO"]);
                        RVCheckResult CR = null;
                        if (qm.Any())
                        {
                            CR = qm.FirstOrDefault();
                        }

                        if (CR == null)
                        {
                            CR = new RVCheckResult()
                            {
                                RV_NO = lo["RV_NO"],
                                RV_YEAR = lo["RV_YEAR"],
                                ITEM_NO = lo["ITEM_NO"],
                                MESSAGE_TEXT = lo["MESSAGE_TEXT"]
                            };
                            results.Add(CR);
                        }
                        else
                        {
                            CR.MESSAGE_TEXT += ";" + lo["MESSAGE_TEXT"];
                        }

                        lo.Put("MESSAGE");
                        errs.Add(lo["MESSAGE_TEXT"]);
                        _Message += lo["MESSAGE_TEXT"];
                    }
                }

            }
            catch (Exception ex)
            {
                Err(ex);
                throw;
            }
            finally
            {
                unreg();
                del();

            }

            return r;
        }

        public List<PVPostSAPResult> GetResultCheckedByAcc(List<PVPostInputHeader> _PVCheck, int _ProcessId, int method)
        {
            string _loc = "GetResultCheckedByAcc(" + method + ")";
            string fid = _globalResource.FunctionId("FCN_PV_CHECK_SAP");
            string fn = "ZBAPI_CHECK_PV";
            _Log.Log(_INF, "Start " + fn, _loc, "", fid, _ProcessId);
            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                        "ZBAPI_HDR_CHECK_PV",
                            "PV_NO,PV_YEAR,ITEM_NO,PV_DATE,PV_TYPE,VENDOR,POSTING_DT"
                            + ",AMOUNT,CURRENCY,TEXT,TRANS_TYPE,METHODE_FLAG",
                        "INPUT",
                            "PV_NO,PV_YEAR,ITEM_NO,CURRENCY,STATUS,SAP_DOC_NO,SAP_DOC_YEAR",
                        "MESSAGE",
                            "PV_NO,PV_YEAR,ITEM_NO,MESSAGE_TEXT"
                        }
                        , "", _ProcessId);

            List<PVPostSAPResult> _CheckResult = null;
            try
            {
                init();
                IRfcFunction BAPIFunction = SapRfcRepository.CreateFunction(fn);
                #region headers
                IRfcTable headers = BAPIFunction.GetTable("INPUT");
                IRfcStructure header;
                SystemSettingLogic ss = new SystemSettingLogic();
                FiscalDocs f = new FiscalDocs();
                int skip = 0;
                for (int i = 0; i < _PVCheck.Count; i++)
                {
                    PVPostInputHeader h = _PVCheck[i];
                    if (h.PV_TYPE == "3" && h.TOTAL_AMOUNT < 1) { skip++; continue; }
                    int fYear = logic.Sys.FiscalYear(h.PV_DATE.dateSAP());
                    string fiscalYear = fYear.str();
                    f.Add(h.PV_NO.Int(), h.PV_YEAR.Int(), fYear);
                    bool hasFraction = !AppSetting.RoundCurrency.Contains(h.CURRENCY_CD);
                    header = SapRfcRepository.GetStructureMetadata("ZBAPI_HDR_CHECK_PV").CreateStructure();

                    lo.Set(header, "ZBAPI_HDR_CHECK_PV",
                        h.PV_NO,
                        fiscalYear,
                        h.SEQ_NO.ToString(),
                        h.PV_DATE,
                        h.PV_TYPE,
                        h.VENDOR,
                        h.POSTING_DATE,
                        h.TOTAL_AMOUNT.SAPdecfmt(hasFraction),
                        h.CURRENCY_CD,
                        h.HEADER_TEXT,
                        h.TRANSACTION_CD.ToString(),
                        method.str());

                    headers.Append(header);

                    lo.Put("ZBAPI_HDR_CHECK_PV");
                }
                #endregion

                BAPIFunction.SetValue("INPUT", headers);
                _Log.Log(_INF, "Invoke " + fn, "");
                BAPIFunction.Invoke(SapRfcDestination);

                #region Collect the result
                IRfcTable BAPIResult;
                PVPostSAPResult _r = null;
                BAPIResult = BAPIFunction.GetTable("INPUT");
                if (BAPIResult.Count > 0)
                {
                    _CheckResult = new List<PVPostSAPResult>();
                    for (int i = 0; i < BAPIResult.Count; i++)
                    {
                        IRfcStructure x = BAPIResult[i];

                        lo.Get(x, "INPUT");

                        _r = new PVPostSAPResult()
                        {
                            PV_NO = lo["PV_NO"],
                            PV_YEAR = f.Doc(lo["PV_NO"].Int(), lo["PV_YEAR"].Int()).str(),
                            ITEM_NO = lo["ITEM_NO"],
                            CURRENCY_CD = lo["CURRENCY"],
                            MESSAGE = lo["STATUS"],
                            SAP_CLEARING_DOC_NO = lo["SAP_DOC_NO"],
                            SAP_CLEARING_DOC_YEAR = lo["SAP_DOC_YEAR"]
                        };

                        _CheckResult.Add(_r);

                        lo.Put("INPUT");
                    }
                }
                #endregion

                IRfcTable BAPIResult1 = BAPIFunction.GetTable("MESSAGE");
                if (BAPIResult1.Count > 0)
                {
                    string _Message = string.Empty;
                    for (int i = 0; i < BAPIResult1.Count; i++)
                    {
                        IRfcStructure x = BAPIResult1[i];
                        lo.Get(x, "MESSAGE");

                        FillPostMessage(_CheckResult
                            , lo["PV_NO"]
                            , f.Doc(lo["PV_NO"].Int(), lo["PV_YEAR"].Int()).str()
                            , lo["ITEM_NO"]
                            , lo["MESSAGE_TEXT"]);
                        errs.Add(lo["MESSAGE_TEXT"]);
                        _Message += lo.Text("MESSAGE", "MESSAGE"); ;

                        lo.Put("MESSAGE");
                    }
                }


            }
            catch (Exception ex)
            {
                Err(ex);
                throw;
            }
            finally
            {
                unreg();
                del();
            }
            return _CheckResult;
        }

        /// <summary>
        /// Written by Akhmad Nuryanto, 22/09/2012
        /// Execute BAPI Function and get String available amount 
        /// ex: 15,000,000.00 (20%)
        /// </summary>
        /// <param name="sWBSNo"></param>
        /// <param name="sFiscalYear"></param>
        /// <returns></returns>
        public String getRemainingBudget(String sWBSNo, String sFiscalYear)
        {
            string _loc = "getRemainingBudget()";
            int pid = _Log.Log(_INF, string.Format("Start {0}({1},{2})", _loc, sWBSNo, sFiscalYear), _loc, "", "", 0);
            String result = "";
            String fn = "ZBAPI_GET_BUDGET_AMOUNT";
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("ZBAPI_INPUT");
                IRfcStructure _inputStructure;
                _inputStructure = SapRfcRepository.GetStructureMetadata("ZBAPI_IN_BUDGET_AMOUNT").CreateStructure();
                _inputStructure.SetValue("WBS_NUMBER", sWBSNo);
                _inputStructure.SetValue("FISCAL_YEAR", sFiscalYear);
                _inputHeader.Append(_inputStructure);

                f.SetValue("ZBAPI_INPUT", _inputHeader);
                _Log.Log(_INF, "Invoke " + fn);
                f.Invoke(SapRfcDestination);

                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("ZBAPI_OUTPUT");
                if (BAPIResult.Count > 0)
                {
                    String sCurrentAmount = BAPIResult.GetString("CURRENT_AMOUNT");
                    String sAvailableAmount = BAPIResult.GetString("AVAILABLE_AMOUNT_EOA");
                    //String sMessage = f.GetString("MESSAGE");

                    if (sCurrentAmount != null && sAvailableAmount != null)
                    {
                        decimal dCurrentAmount = decimal.Parse(sCurrentAmount);
                        decimal dAvailableAmount = decimal.Parse(sAvailableAmount);
                        if (dCurrentAmount != 0)
                        {
                            decimal dPercentage = dAvailableAmount / dCurrentAmount * 100;

                            result = string.Format("{0:#,#.##}", dAvailableAmount)
                                + " (" + string.Format("{0:#,#.##}", dPercentage) + "%)";
                        }
                    }

                    _Log.Log(_INF, String.Format("GetRemainingBudget|"
                         + "CURRENT_AMOUNT : {0}|"
                         + "AVAILABLE_AMOUNT NO : {1}|"
                         + "MESSAGE : {2}|"
                         + "RESULT  :{3}"
                        , new object[] { sCurrentAmount, sAvailableAmount, "", result }
                        ).Replace("|", "\r\n\t"));

                }

            }
            catch (Exception ex)
            {
                Err(ex);
            }
            finally
            {
                unreg();
                del();

            }
            return result;
        }

        public double num(string s)
        {
            s = s.Trim();
            double mul = 1;
            if (s.Length > 0 && (s.LastIndexOf("-") == (s.Length - 1)))
            {
                s = s.Substring(0, s.Length - 1);
                mul = -1;
            }
            double d = 0;
            if (!Double.TryParse(s, out d))
                d = 0;
            d = d * mul;
            return d;
        }

        public string GetRemainingBudget(int docno, int docyear, string wbs)
        {
            BudgetCheckResult o = BudgetCheck(new BudgetCheckInput()
            {
                DOC_STATUS = (int)BudgetCheckStatus.Get,
                DOC_NO = docno,
                DOC_YEAR = docyear,
                WBS_NO = wbs
            });
            double rem = num(o.AVAILABLE);
            double all = num(o.ORIGINAL);
            double pct;
            string sPct;
            if (all <= 0 || all < rem)
            {
                pct = 0;
                sPct = "";
            }
            else
            {
                pct = rem / all * 100;
                sPct = string.Format(" ({0:#,#.##} %)", pct);
            }

            return string.Format("{0:N0}{1}", rem, sPct);
        }

        /// Written by Daniel, 26/11/2012
        /// Execute BAPI Function ZBAPI_BUDGET_CHECK 
        /// INPUT : ZINPUT_BUDGET_CHECK
        ///     SYSTEM_ID
        ///     DOC_NO
        ///     DOC_YEAR
        ///     DOC_STATUS 
        ///         0 : get remaining 
        /// 	    1 : book @SUBMIT
        /// 	    2 : release @POST 
        /// 	    3 : delete 
        /// 	    4 : reverse (posting di edit)        
        /// 	WBS_NO
        /// 	AMOUNT
        /// OUTPUT : ZOUTPUT_BUDGET_CHECK
        ///     SYSTEM_ID 
        ///     DOC_NO
        ///     DOC_YEAR
        ///     STATUS 
        ///     MESSAGE        
        public BudgetCheckResult BudgetCheck(BudgetCheckInput _i, string uid = "")
        {
            BudgetCheckResult _o = null;
            string fid = _globalResource.FunctionId("FCN_BUDGET_CHECK");
            string _loc = "BudgetCheck()";

            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                "ZINPUT_BUDGET_CHECK",
                    "SYSTEM_ID,DOC_NO,DOC_YEAR,DOC_STATUS,WBS_NO,AMOUNT",
                "OUTPUT",
                    "DOC_NO,DOC_YEAR,STATUS,MESSAGE,ORIGINAL,AVAILABLE"
                }, uid, 0);

            String fn = "ZBAPI_BUDGET_CHECK";
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("INPUT");
                IRfcStructure _inputStructure;
                _inputStructure = SapRfcRepository.GetStructureMetadata("ZINPUT_BUDGET_CHECK").CreateStructure();
                lo.Set(_inputStructure, "ZINPUT_BUDGET_CHECK"
                    , AppSetting.ApplicationID
                    , _i.DOC_NO.str()
                    , _i.DOC_YEAR.str()
                    , _i.DOC_STATUS.str()
                    , _i.WBS_NO
                    , _i.AMOUNT.SAPnum());

                _inputHeader.Append(_inputStructure);
                lo.Put("ZINPUT_BUDGET_CHECK");
                f.SetValue("INPUT", _inputHeader);

                _Log.Log(_INF, "Invoke " + fn, _loc, uid, "", _Log.PID);
                f.Invoke(SapRfcDestination);



                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("OUTPUT");
                if (BAPIResult.Count > 0)
                {
                    _o = new BudgetCheckResult();
                    lo.Get(BAPIResult, "OUTPUT");

                    _o.DOC_NO = lo["DOC_NO"].Int();
                    _o.DOC_YEAR = lo["DOC_YEAR"].Int();
                    _o.STATUS = lo["STATUS"];
                    _o.MESSAGE = lo["MESSAGE"];
                    _o.ORIGINAL = lo["ORIGINAL"];
                    _o.AVAILABLE = lo["AVAILABLE"];

                    lo.Put("OUTPUT");
                    errs.Add(_o.MESSAGE);
                }

            }
            catch (Exception ex)
            {
                Err(ex);
                _o = new BudgetCheckResult()
                {
                    DOC_NO = _i.DOC_NO,
                    DOC_YEAR = _i.DOC_YEAR,
                    MESSAGE = ex.Message,
                    STATUS = "E"
                };
                errs.Add(ex.Message);
            }
            finally
            {
                unreg();
                del();

            }
            return _o;
        }

        ///  mock procedure, temporary until _BUDGET_CHECK complete        
        public BudgetCheckResult _BudgetCheck_(BudgetCheckInput _i)
        {
            return new BudgetCheckResult()
            {
                DOC_NO = _i.DOC_NO,
                DOC_YEAR = _i.DOC_YEAR,
                STATUS = "S",
                MESSAGE = ""
            };
        }

        public VendorPostResult PostVendor(VendorInput v, int _ProcessId, string User)
        {
            string fn = "ZBAPI_CREATE_VENDOR";
            string fid = _globalResource.FunctionId("FCN_POST_VENDOR");
            string _loc = "PostVendor";
            string Op = String.Format(
                    "CREATE_VENDOR INPUT|"
                    + "NAME         : {0}|"
                    + "DIVISION     : {1}|"
                    + "DIVISION_NAME: {2}|"
                    + "SEARCH_TERM  : {3}"
                    , new object[] { v.NAME, v.DIVISION, v.DIVISION_NAME, v.SEARCH_TERM, _ProcessId })
                    .Replace("|", "\r\n\t");

            int pid = _Log.Log("MSTD00066INF", Op, _loc, User, fid, _ProcessId);

            VendorPostResult r = new VendorPostResult();
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                f.SetValue("NAME", v.NAME);
                f.SetValue("DIVISION", v.DIVISION);
                f.SetValue("DIVISION_NAME", v.DIVISION_NAME);
                f.SetValue("SEARCH_TERM", v.SEARCH_TERM);
                _Log.Log(_INF, "Invoke " + fn, _loc, User, "", pid);
                f.Invoke(SapRfcDestination);
                // _t.Append(_in);
                VendorPostResult o = new VendorPostResult()
                {
                    STATUS = f.GetString("STATUS"),
                    VENDOR_NO = f.GetString("VENDOR_NO"),
                    MESSAGE = f.GetString("MESSAGE")
                };
                errs.Add(o.MESSAGE);
                r = o;
                string _o =
                    String.Format("CREATE_VENDOR OUTPUT:|"
                     + "STATUS    : {0}|"
                     + "VENDOR NO : {1}|"
                     + "MESSAGE   : {2}"
                    , new object[]
                        {
                            o.STATUS
                            , o.VENDOR_NO
                            , o.MESSAGE
                        }).Replace("|", "\r\n\t");

                _Log.Log("MSTD00067INF", _o, _loc, User, "", pid);

            }
            catch (Exception ex)
            {
                Err(ex);
            }
            finally
            {
                unreg();
                del();

            }
            return r;
        }

        private void Err(Exception ex)
        {
            int id = _Log.Log("MSTD00002ERR", ex.Message);
            LoggingLogic.err(ex);
            Type t = ex.GetType();
            string xtype = (t != null) ? t.FullName : "?";            
        }


        //**************************//
        //  ELVIS 2018 ENHANCEMENT  //
        //           START          //
        //**************************//
        public List<BudgetRemaining> BudgetRemaining(List<string> budgetNo, string uid = "")
        {
            List<BudgetRemaining> _o = new List<BudgetRemaining>();
            string fid = _globalResource.FunctionId("FCN_BUDGET_REMAINING");
            string _loc = "RemainingBudget()";

            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                "ZSTFI_BUDGET_POST",
                    "POSID",
                "ZBUDGET_RETURN",
                    "POSID,WTGES"
                }, uid, 0);

            String fn = "ZFMFI_BUDGET";
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("ZWBS_ELEMENT");
                IRfcStructure _inputStructure;

                foreach (string bud in budgetNo)
                {
                    _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_BUDGET_POST").CreateStructure();
                    lo.Set(_inputStructure, "ZSTFI_BUDGET_POST", bud);

                    _inputHeader.Append(_inputStructure);
                }

                lo.Put("ZSTFI_BUDGET_POST");
                f.SetValue("ZWBS_ELEMENT", _inputHeader);

                _Log.Log(_INF, "Invoke " + fn, _loc, uid, "", _Log.PID);
                f.Invoke(SapRfcDestination);

                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("ZBUDGET_RETURN");

                BudgetRemaining b = null;
                for (int i = 0; i < BAPIResult.Count; i++)
                {
                    IRfcStructure row = BAPIResult[i];

                    b = new BudgetRemaining();
                    lo.Get(row, "ZBUDGET_RETURN");

                    b.BUDGET_NO = lo["POSID"];
                    decimal remSap = decimal.Parse(lo["WTGES"]);

                    // fid.Hadid 20180713. subtract Remainint Amt from SAP
                    // with outstanding amount of the budget
                    decimal ost = logic.Look.GetOutstandingAmountBudget(b.BUDGET_NO);
                    b.REMAINING_AMT = remSap - ost;

                    _o.Add(b);
                }
                lo.Put("ZBUDGET_RETURN");

            }
            catch (Exception ex)
            {
                Err(ex);
                errs.Add(ex.Message);
            }
            finally
            {
                unreg();
                del();

            }
            return _o;
        }
        public List<SyncSAPData> SynchronizeBudget(List<SyncSAPData> data, string uid = "")
        {
            List<SyncSAPData> _o = new List<SyncSAPData>();
            string fid = _globalResource.FunctionId("FCN_ACCRUED_BUDGET_SYNC");
            string _loc = "SynchronizeBudget()";

            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                "ZSTFI_BUDGET_POST",
                    "POSID",
                "ZBUDGET_RETURN",
                    "POSID,WLP00,WLP01,WLP02,WLP03"
                }, uid, 0);

            String fn = "ZFMFI_SYNC_BUDGET";
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("ZWBS_ELEMENT");
                IRfcStructure _inputStructure;

                foreach (SyncSAPData d in data)
                {
                    _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_BUDGET_POST").CreateStructure();
                    lo.Set(_inputStructure, "ZSTFI_BUDGET_POST", d.BUDGET_NO);

                    _inputHeader.Append(_inputStructure);
                }

                lo.Put("ZSTFI_BUDGET_POST");
                f.SetValue("ZWBS_ELEMENT", _inputHeader);

                _Log.Log(_INF, "Invoke " + fn, _loc, uid, "", _Log.PID);
                f.Invoke(SapRfcDestination);

                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("ZBUDGET_RETURN");

                SyncSAPData b = null;
                for (int i = 0; i < BAPIResult.Count; i++)
                {
                    IRfcStructure row = BAPIResult[i];

                    b = new SyncSAPData();
                    lo.Get(row, "ZBUDGET_RETURN");

                    b.BUDGET_NO = lo["POSID"];

                    decimal tempAmt = 0;
                    Decimal.TryParse(lo["WLP00"], out tempAmt);
                    b.AVAILABLE_AMT = tempAmt;

                    Decimal.TryParse(lo["WLP01"], out tempAmt);
                    b.BUDGET_AMT = tempAmt;

                    Decimal.TryParse(lo["WLP02"], out tempAmt);
                    b.ACTUAL_AMT = tempAmt;

                    Decimal.TryParse(lo["WLP03"], out tempAmt);
                    b.COMMITMENT_AMT = tempAmt;

                    _o.Add(b);
                }
                lo.Put("ZBUDGET_RETURN");

            }
            catch (Exception ex)
            {
                Err(ex);
                errs.Add(ex.Message);
            }
            finally
            {
                unreg();
                del();

            }
            return _o;
        }
        public List<ShiftingBudget> ShiftingBudget(List<ShiftingBudget> data, string uid = "")
        {
            List<ShiftingBudget> _o = new List<ShiftingBudget>();
            string fid = _globalResource.FunctionId("FCN_ACCRUED_SHIFTING");
            string _loc = "ShiftingBudget()";

            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                "ZSTFI_SHIFTING_BUDGET",
                    "POSID,AMOUNT,TYPE,SGTEXT",
                "ZBUDGET_RETURN",
                    "POSID,BELNR"
                }, uid, 0);

            String fn = "ZFMFI_SHIFTING_BUDGET";
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("ZWBS_ELEMENT");
                IRfcStructure _inputStructure;

                foreach (ShiftingBudget d in data)
                {
                    _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_SHIFTING_BUDGET").CreateStructure();
                    lo.Set(_inputStructure, "ZSTFI_SHIFTING_BUDGET"
                        , d.BUDGET_NO
                        , d.SHIFTING_AMT.SAPnum()
                        , d.SHIFTING_TYPE
                        , d.SHIFTING_DESC);

                    _inputHeader.Append(_inputStructure);
                }

                lo.Put("ZSTFI_SHIFTING_BUDGET");
                f.SetValue("ZWBS_ELEMENT", _inputHeader);

                _Log.Log(_INF, "Invoke " + fn, _loc, uid, "", _Log.PID);
                f.Invoke(SapRfcDestination);

                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("ZBUDGET_RETURN");

                ShiftingBudget b = null;
                for (int i = 0; i < BAPIResult.Count; i++)
                {
                    IRfcStructure row = BAPIResult[i];

                    b = new ShiftingBudget();
                    lo.Get(row, "ZBUDGET_RETURN");

                    b.BUDGET_NO = lo["POSID"];
                    b.DOC_CHANGE_NO = lo["BELNR"];

                    _o.Add(b);
                }
                lo.Put("ZBUDGET_RETURN");

            }
            catch (Exception ex)
            {
                Err(ex);
                errs.Add(ex.Message);
            }
            finally
            {
                unreg();
                del();

            }
            return _o;
        }
        public List<AccrFormSimulationData> SuspenseCheckPaid(List<AccrFormSimulationData> data, string uid = "")
        {
            string fid = _globalResource.FunctionId("FCN_ACCRUED_SUS_CHECK");
            string _loc = "SuspenseCheckPaid()";

            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                "ZSTFI_POST_STATUS",
                    "COMP_CODE,FISC_YEAR,DOC_NUMSAP",
                "STATUS_RETURN",
                    "COMP_CODE,FISC_YEAR,DOC_NUMSAP,ZSTATUS"
                }, uid, 0);

            String fn = "ZFM_FI_CHECK_STATUS";
            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                IRfcTable _inputHeader = f.GetTable("POST_STATUS");
                IRfcStructure _inputStructure;

                foreach (AccrFormSimulationData d in data)
                {
                    _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_POST_STATUS").CreateStructure();
                    lo.Set(_inputStructure, "ZSTFI_POST_STATUS"
                        , d.COMPANY_CD
                        , d.SAP_DOC_YEAR.str()
                        , d.SAP_DOC_NO);

                    _inputHeader.Append(_inputStructure);
                }

                lo.Put("ZSTFI_POST_STATUS");
                f.SetValue("POST_STATUS", _inputHeader);

                _Log.Log(_INF, "Invoke " + fn, _loc, uid, "", _Log.PID);
                f.Invoke(SapRfcDestination);

                IRfcTable BAPIResult;
                BAPIResult = f.GetTable("STATUS_RETURN");

                for (int i = 0; i < BAPIResult.Count; i++)
                {
                    IRfcStructure row = BAPIResult[i];

                    lo.Get(row, "STATUS_RETURN");

                    string retDocNo = lo["DOC_NUMSAP"];
                    string retDocYy = lo["FISC_YEAR"];

                    var ds = data.Where(x => x.SAP_DOC_NO == retDocNo && x.SAP_DOC_YEAR.str() == retDocYy);
                    if (ds != null)
                    {
                        foreach (var d in ds)
                        {
                            d.PAID_STS = lo["ZSTATUS"];
                        }
                    }
                }
                lo.Put("STATUS_RETURN");

            }
            catch (Exception ex)
            {
                Err(ex);
                errs.Add(ex.Message);
            }
            finally
            {
                unreg();
                del();

            }
            return data;
        }
        public object AccruedPosting(AccrPostData data, string uid, string fid, string _loc)
        {
            AccrPostResult retSuc = null;
            List<AccrSAPError> retErr = null;
            bool isSuccess = true;
            bool sendDocReq = !data.DOC_REQUEST.isEmpty();

            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                "ZSTFI_ACCOUNTGL",
                    "ITEMNO_ACC,GL_ACCOUNT,ITEM_TEXT,COMP_CODE,BUS_AREA,FIS_PERIOD,FISC_YEAR"
                    + ",PSTNG_DATE,VALUE_DATE,WBS_ELEMENT,ORDERID,FUNDS_CTR,COSTCENTER,TAX_CODE",
                "ZSTFI_ACCOUNTVENDOR",
                    "ITEMNO_ACC,VENDOR_NO,BUS_AREA,ITEM_TEXT,BLINE_DATE,PYMT_CUR,PYMT_AMT,SP_GL_IND,TAX_CODE",
                "ZSTFI_CURRENCYAMOUNT",
                    "ITEMNO_ACC,CURRENCY,AMT_DOCCUR,DEBIT_CREDIT",
                "BAPI_RETURN",
                    "COMP_CODE,FISC_YEAR,DOC_NUMSAP,DOC_NUMCORE,MESSAGE",
                "T_RETURN",
                    "TYPE,ID,NUMBER,MESSAGE,LOG_NO,LOG_MSG_NO,MESSAGE_V1,MESSAGE_V2,MESSAGE_V3,MESSAGE_V4,PARAMETER,ROW,FIELD,SYSTEM"
                }, uid, 0);

            String fn;
            if (!sendDocReq)
                fn = "ZFM_FI_ACCRUED_POSTING";
            else
                fn = "ZFM_FI_ACCRUED_POSTING2";

            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                #region Import
                f.SetValue("USERNAME", data.USERNAME);
                f.SetValue("HEADER_TXT", data.HEADER_TXT);
                f.SetValue("COMP_CODE", data.COMP_CODE);
                f.SetValue("DOC_DATE", data.DOC_DATE);
                f.SetValue("PSTNG_DATE", data.PSTNG_DATE);
                f.SetValue("TRANS_DATE", data.TRANS_DATE);
                f.SetValue("FISC_YEAR", data.FISC_YEAR);
                f.SetValue("FIS_PERIOD", data.FIS_PERIOD);
                f.SetValue("REF_DOC_NO", data.REF_DOC_NO);
                f.SetValue("DOC_TYPE", data.DOC_TYPE);
                f.SetValue("GROUP_DOC", data.GROUP_DOC);
                if (sendDocReq)
                    f.SetValue("DOC_REQUEST", data.DOC_REQUEST);
                #endregion

                IRfcTable _inputHeader = null;
                IRfcStructure _inputStructure = null;

                #region Account GL

                if (data.ACCOUNT_GL.Count > 0)
                {
                    _inputHeader = f.GetTable("ACCOUNT_GL");

                    foreach (var gl in data.ACCOUNT_GL)
                    {
                        _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_ACCOUNTGL").CreateStructure();

                        lo.Set(_inputStructure, "ZSTFI_ACCOUNTGL"
                            , gl.ITEMNO_ACC
                            , gl.GL_ACCOUNT
                            , gl.ITEM_TEXT
                            , gl.COMP_CODE
                            , gl.BUS_AREA
                            , gl.FIS_PERIOD
                            , gl.FISC_YEAR
                            , gl.PSTNG_DATE
                            , gl.VALUE_DATE
                            , gl.WBS_ELEMENT
                            , gl.ORDERID
                            , gl.FUNDS_CTR
                            , gl.COSTCENTER
                            , gl.TAX_CODE);

                        _inputHeader.Append(_inputStructure);
                    }

                    lo.Put("ZSTFI_ACCOUNTGL");
                    f.SetValue("ACCOUNT_GL", _inputHeader);

                }
                #endregion

                #region Account Vendor

                if (data.ACCOUNT_VENDOR.Count > 0)
                {
                    _inputHeader = f.GetTable("ACCOUNT_VENDOR");

                    foreach(var acc in data.ACCOUNT_VENDOR)
                    {
                        _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_ACCOUNTVENDOR").CreateStructure();

                        lo.Set(_inputStructure, "ZSTFI_ACCOUNTVENDOR"
                            , acc.ITEMNO_ACC
                            , acc.VENDOR_NO
                            //, "0000500000" // TO BE DELETED
                            , acc.BUS_AREA
                            , acc.ITEM_TEXT
                            , acc.BLINE_DATE
                            , acc.PYMT_CUR
                            , acc.PYMT_AMT
                            , acc.SP_GL_IND
                            , acc.TAX_CODE);

                        _inputHeader.Append(_inputStructure);
                    }

                    lo.Put("ZSTFI_ACCOUNTVENDOR");
                    f.SetValue("ACCOUNT_VENDOR", _inputHeader);
                }
                #endregion

                #region Currency

                if (data.CURRENCYAMOUNT.Count > 0)
                {
                    _inputHeader = f.GetTable("CURRENCYAMOUNT");

                    foreach (var cur in data.CURRENCYAMOUNT)
                    {
                        _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_CURRENCYAMOUNT").CreateStructure();

                        lo.Set(_inputStructure, "ZSTFI_CURRENCYAMOUNT"
                            , cur.ITEMNO_ACC
                            , cur.CURRENCY
                            , cur.AMT_DOCCUR
                            , cur.DEBIT_CREDIT);

                        _inputHeader.Append(_inputStructure);
                    }

                    lo.Put("ZSTFI_CURRENCYAMOUNT");
                    f.SetValue("CURRENCYAMOUNT", _inputHeader);
                }

                #endregion

                _Log.Log(_INF, "Invoke " + fn, _loc, uid, "", _Log.PID);
                f.Invoke(SapRfcDestination);

                #region Get Result

                IRfcTable BAPIResult = f.GetTable("BAPI_RETURN");

                if (BAPIResult != null && BAPIResult.Count > 0)
                {
                    lo.Get(BAPIResult, "BAPI_RETURN");

                    retSuc = new AccrPostResult();
                    retSuc.COMP_CODE = lo["COMP_CODE"];
                    retSuc.FISC_YEAR = lo["FISC_YEAR"];
                    retSuc.DOC_NUMSAP = lo["DOC_NUMSAP"];
                    retSuc.DOC_NUMCORE = lo["DOC_NUMCORE"];
                    retSuc.MESSAGE = lo["MESSAGE"];

                    lo.Put("BAPI_RETURN");

                    if (retSuc.DOC_NUMSAP.Trim().isEmpty())
                    {
                        isSuccess = false;

                        BAPIResult = f.GetTable("T_RETURN");

                        lo.Get(BAPIResult, "T_RETURN");

                        retErr = new List<AccrSAPError>();
                        AccrSAPError err = null;
                        for (int i = 0; i < BAPIResult.Count; i++)
                        {
                            IRfcStructure row = BAPIResult[i];
                            lo.Get(row, "T_RETURN");

                            err = new AccrSAPError();
                            err.TYPE = lo["TYPE"];
                            err.ID = lo["ID"];
                            err.NUMBER = lo["NUMBER"];
                            err.MESSAGE = lo["MESSAGE"];
                            err.LOG_NO = lo["LOG_NO"];
                            err.LOG_MSG_NO = lo["LOG_MSG_NO"];
                            err.MESSAGE_V1 = lo["MESSAGE_V1"];
                            err.MESSAGE_V2 = lo["MESSAGE_V2"];
                            err.MESSAGE_V3 = lo["MESSAGE_V3"];
                            err.MESSAGE_V4 = lo["MESSAGE_V4"];
                            err.PARAMETER = lo["PARAMETER"];
                            err.ROW = lo["ROW"];
                            err.FIELD = lo["FIELD"];
                            err.SYSTEM = lo["SYSTEM"];

                            retErr.Add(err);
                        }

                        lo.Put("T_RETURN");
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                Err(ex);
                errs.Add(ex.Message);
            }
            finally
            {
                unreg();
                del();
            }

            if (isSuccess) return retSuc;
            else return retErr;
        }
        public object AccruedClearing(AccrClearingData data, string uid, string fid, string _loc)
        {
            AccrClearingResult retSuc = null;
            List<AccrSAPError> retErr = null;
            bool isSuccess = true;

            LogHelper lo = new LogHelper(_Log, _loc, new string[] {
                "ZSTFI_CLEARING",
                    "AGKON,BUDAT,MONAT,BUKRS,WAERS,AGUMS,XNOPS,XPOS1,XPOS2,SEL01,SEL02,ABPOS",
                "ZBAPI_CLEAR_RETURN",
                    "BELNR,NOCLR"
                }, uid, 0);

            String fn = "ZFM_FI_POST_CLEARING";

            try
            {
                init();
                IRfcFunction f = SapRfcRepository.CreateFunction(fn);

                #region ZBAPI_CLEARING

                IRfcTable _inputHeader = f.GetTable("ZBAPI_CLEARING");
                IRfcStructure _inputStructure = SapRfcRepository.GetStructureMetadata("ZSTFI_CLEARING").CreateStructure();

                lo.Set(_inputStructure, "ZSTFI_CLEARING"
                    , data.AGKON
                    , data.BUDAT
                    , data.MONAT
                    , data.BUKRS
                    , data.WAERS
                    , data.AGUMS
                    , data.XNOPS
                    , data.XPOS1
                    , data.XPOS2
                    , data.SEL01
                    , data.SEL02
                    , data.ABPOS);

                _inputHeader.Append(_inputStructure);

                f.SetValue("ZBAPI_CLEARING", _inputHeader);
                #endregion

                _Log.Log(_INF, "Invoke " + fn, _loc, uid, "", _Log.PID);
                f.Invoke(SapRfcDestination);

                #region Get Result

                IRfcTable BAPIResult = f.GetTable("ZBAPI_CLEAR_RETURN");

                if (BAPIResult != null && BAPIResult.Count > 0)
                {
                    isSuccess = true;
                    // only retrieve last item in BAPIResult
                    lo.Get(BAPIResult[BAPIResult.Count - 1], "ZBAPI_CLEAR_RETURN");

                    retSuc = new AccrClearingResult();
                    retSuc.BELNR = lo["BELNR"];
                    retSuc.NOCLR = lo["NOCLR"];

                    lo.Put("ZBAPI_CLEAR_RETURN");
                }
                else
                {
                    isSuccess = false;
                    retErr = new List<AccrSAPError>();

                    retErr.Add(new AccrSAPError
                    {
                        TYPE = "E",
                        NUMBER = "000",
                        MESSAGE = "No return in ZBAPI_CLEAR_RETURN",
                    });
                }

                #endregion
            }
            catch (Exception ex)
            {
                Err(ex);
                errs.Add(ex.Message);
            }
            finally
            {
                unreg();
                del();
            }

            if (isSuccess) return retSuc;
            else return retErr;
        }

        //**************************//
        //            END           //
        //**************************//
    }


}
