﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class WBSStructure
    {        
        public string WbsNumber { set; get; }  
        public string Description { set; get; }
    }

    [Serializable]
    public class PICCurentNext
    {
        public string ACCR_NO { set; get; }
        public string STATUS_CD { set; get; }
        public string PROCCED_BY { set; get; }
    }

    public class CurentNextResult
    {
        public string ACCR_NO { set; get; }
        public string PICCureent { set; get; }
        public string PICNext { set; get; }
    }
}
