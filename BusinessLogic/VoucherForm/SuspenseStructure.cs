﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class SuspenseStructure
    {        
        public string WbsNumber { set; get; }
        public string Description { set; get; }
        public decimal? ReffNo { set; get; }
        public string PVNo { set; get; }
        public decimal? TotalAmount { set; get; }
        public string VendorName { set; get; }
        public string CurrCode { set; get; }
        public DateTime? ActivityDateTo { set; get; }
        public string CostCenter { set; get; }
        public string ActivityDesc { set; get; }
    }
}
