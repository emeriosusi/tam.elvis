﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class FormAttachment
    {
        public FormAttachment()
        {
            Description = "";
        }

        public bool Blank { set; get; }
        public string Url { get; set; }
        public decimal SequenceNumber { get; set; }
        public string SequenceNumberInString { set; get; }

        public string ReferenceNumber{   get; set; }

        public string CategoryCode { get; set; }

        public string CategoryName { get; set; }

        public string PATH { get; set; }

        public string FileName { get; set; }
        public string DisplayFilename { set; get; }

        public string Description { get; set; }
        
        //fid.Hadid 20180706
        public string CreatedBy { get; set; }
    }
}