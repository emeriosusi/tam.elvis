﻿using BusinessLogic.AccruedForm;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using Common.Data._30PVFormList;
using Common.Data._80Accrued;
using Common.Data.SAPData;
using Common.Function;
using Dapper;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    public class FormPersistence : LogicBase
    {
        protected LogicFactory logic = LogicFactory.Get();
        protected DateTime today = DateTime.Now;
        protected string logi;

        private string _UserName;

        protected List<string> _errs;

        public CommonExcelUpload ceu = null; 

        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }
            
        
        protected NumberFormatInfo numberFormatInfo;
        public int[] suspenseTransactionCd;
        public int financeDirectorate = 0;
        public int BoardOfDirector = 0;

        public FormPersistence()
        {
            NumberFormatInfo formatInfo = CultureInfo.CurrentCulture.NumberFormat;
            numberFormatInfo = (NumberFormatInfo)formatInfo.Clone();
            numberFormatInfo.CurrencySymbol = "";
            _errs = new List<string>();

            string sWBSValid = "WBS_VALIDATION";
            financeDirectorate = logic.Sys.GetValue(sWBSValid, "FINANCE_DIR_DIV") ?? 0;
            BoardOfDirector = logic.Sys.GetValue(sWBSValid, "BOD_DIV") ?? 0;
            suspenseTransactionCd = (logic.Sys.GetText("PvForm", "SUSPENSE_TRANSACTION_CD") ?? "")
                .Split(',')
                .Select(x => x.Int(0))
                .Distinct()
                .ToArray();
            logi = "";
        }


        public virtual string getDocType(int? code)
        {
            return "";
        }

        public virtual List<CodeConstant> getDocTypes()
        {
            return new List<CodeConstant>();
        }
        public TransactionType GetTransactionType(int code)
        {
            return logic.Base.GetTransactionType(code);
        }

        protected List<GlAccountMappingData> _glAccountMapping = null;
        protected List<GlAccountMappingData> glAccountMapping
        {
            get
            {
                if (_glAccountMapping == null)
                {
                    _glAccountMapping = hQu<GlAccountMappingData>("GetGLAccountMapping").ToList();
                }
                return _glAccountMapping;
            }
        }


        protected int[] DivNoProductionFlagCheck()
        {
            string[] divs = logic.Sys.GetArray("FORM_AUTH", "SKIP_PRODUCTION_FLAG_CHECK_DIV");
            int[] r;
            if (divs == null || divs.Length < 1)
                r = new int[] { 5, 8 };
            else
            {
                r = new int[divs.Length];

                for (int i = 0; i < divs.Length; i++)
                {
                    r[i] = divs[i].Int();
                }
            }
            return r;
        }

        public int? GLAccount(int transactionCd, int itemTransactionCd)
        {
            int? r = null;
            if ((transactionCd > 0) && (itemTransactionCd > 0))
                r = glAccountMapping
                        .Where(g => g.TRANSACTION_CD == transactionCd && g.ITEM_TRANSACTION_CD == itemTransactionCd)
                        .Select(a => a.GL_ACCOUNT)
                        .FirstOrDefault();
            return r;
        }

        public int? GLAccount(string CostCenterCd, int transactionCd, int? itemTransactionCd = null)
        {
            IEnumerable<int> gli = hQx<int>("GetGLAccount", new { cc = CostCenterCd, tx = transactionCd, itx = itemTransactionCd });
            if (gli != null && gli.Any())
                return gli.FirstOrDefault();
            else
            {
                logic.Say("GLAcc", "GLAccount ({0},{1},{2}) not found", CostCenterCd, transactionCd.str(), itemTransactionCd.str()??"NULL");
                return null;
            }
        }
     
        public List<TransactionType> getTransactionTypes(
            UserData u = null,
            int Tag = -1,
            int VendorGroup = -1,
            bool isSuspense = false,
            bool hasDetail = true,
            bool isAuthorizedFinance = false,
            bool userDivisionCanUploadDetail = false,
            string _screenID = "")
        {
            bool isSecretary = false;
            List<TransactionType> l = new List<TransactionType>();
            List<int> divs = new List<int>();
            string divCd = "";
            if (u != null)
            {
                isSecretary = u.Roles.Contains("ELVIS_SECRETARY");
                int d = 0;
                divs = u.Divisions
                        .Where(a => int.TryParse(a, out d))
                        .Select(x => x.Int())
                        .ToList();
                divCd = Convert.ToString(u.DIV_CD);
            }
            string divst = string.Join(",", divs.ToArray());
            int isPVFormList = _screenID.Equals("ELVIS_PVFormList") ? 1 : 0;

            return hQx<TransactionType>("sp_GetTransactionTypesAccr",
                new
                {
                    divCd = divCd,
                    isPVFormList = isPVFormList,
                    userDIVs = divst,
                    userDivisionCanUploadDetail = ((userDivisionCanUploadDetail) ? 1 : 0),
                    isSecretary = ((isSecretary) ? 1 : 0),
                    tag = Tag,
                    isSuspense = ((isSuspense) ? 1 : 0)
                }, null, true, null, CommandType.StoredProcedure).ToList();
        }

        public string getPaymentMethodName(string code)
        {
            return hQx<string>("getPaymentMethodName", new { code = code }).FirstOrDefault();

        }

        public CodeConstant getVendorGroupNameByVendor(string vendorCode)
        {
            return hQu<CodeConstant>("getVendorGroupNameByVendor", StrTo.RemoveQuotes(vendorCode)).FirstOrDefault();
        }
        public List<CodeConstant> getCurrencyCodeList()
        {
            return hQu<CodeConstant>("getCurrencyCodeList").ToList();
        }
        public List<CodeConstant> getCurrencyCodeListWOEurSgd()
        {
            return hQu<CodeConstant>("getCurrencyCodeListWOEurSgd").ToList();
        }
        public string getInitialDocumentStatus()
        {
            return getDocumentStatus(0);
        }
        public int getDocumentStatusCode(string status)
        {
            return hQx<int>("getDocumentStatusCode", new { statusName = status }).FirstOrDefault();

        }
        public string getDocumentStatus(int code)
        {
            return hQx<string>("GetDocumentStatusName", new { code = code }).FirstOrDefault();
     
        }
        public string getUserRoleID(string username)
        {
            return hQx<string>("getUserRoleID", new { username = username }).FirstOrDefault();
    
        }
        public string getUserRoleName(string username)
        {
            return hQx<string>("getUserRoleName", new { username = username }).FirstOrDefault();
     
        }
        public VendorBank GetVendorBank(string vendorCode, string key)
        {
            return hQx<VendorBank>("GetVendorBank", new { vendorCode = vendorCode, key = key }).FirstOrDefault();
        }
       
        public IQueryable<VendorBank> GetVendorBanks(string vendorCode, string key = "")
        {
            return hQx<VendorBank>("GetVendorBanks", new { VendorCode = vendorCode, Key = key }).Select(v => v).AsQueryable();
        }

        public List<WBSStructure> WbsNumbers(string bookingID = "")
        {
            return Qx<WBSStructure>("GetWbsNumberFromAccrBalance", new { bookingID = bookingID }).ToList();
        }

       public List<WBSStructure> GetWbsNumbers(UserData userData, PVFormData f)
        {
            int? txCode = f.TransactionCode;
            string division = userData.DIV_CD.ToString();
            if (txCode == null)
            {
                return new List<WBSStructure>(0);
            }
            List<WBSStructure> lstWbsResult = new List<WBSStructure>();

            try
            {
                int fiscalYear = logic.Sys.FiscalYear(DateTime.Now);
                StringBuilder sql = new StringBuilder(
                            "-- getWbsNumbers\r\n\r\n" +
                            "-- DIV_CD: " + division + " TRANSCATION_TYPE: " + txCode.str() + "\r\n" +
                            "\r\n" +
                            "SELECT DISTINCT v.WbsNumber, v.[Description] \r\n" +
                            "  FROM dbo.vw_WBS v \r\n" +
                            " WHERE (v.Year = " + fiscalYear.str() + ") \r\n"
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );

                decimal BOD = BoardOfDirector;
                int tt = f.TransactionCode ?? 0;

                bool isGlAccountProject = hQu<int>("GetIsGLAccountProject", tt).FirstOrDefault() > 0;

                string divCondition = "";

                if (userData.DIV_CD >= financeDirectorate)
                {
                    sql.AppendLine("   -- Finance Directorate " + BOD.str());
                    divCondition = " v.Division = " + BOD.str();
                }
                else
                {
                    StringMap pad = logic.Sys.GetDict("PAD_BUDGET_MAPPING");
                    if (pad.Has(division))
                    {
                        sql.AppendLine("   -- IN PAD BUDGET MAPPING" + pad.KV());
                        string padDivisions = Quoten(pad.Get(division));

                        divCondition = " v.Division IN (" + padDivisions + ") ";
                    }
                    else
                    {
                        sql.AppendLine("   -- NOT IN PAD BUDGET MAPPING");
                        divCondition = " v.Division = '" + division + "'";
                    }
                }

                sql.AppendLine(
                          "   AND ( \r\n"
                        + ((isGlAccountProject)
                            ? "   -- is     GL ACCOUNT PROJECT \r\n       "
                            : "   -- is NOT GL ACCOUNT PROJECT \r\n     " + divCondition + "\r\n      AND NOT\r\n       ")
                        + "      (LEFT(WbsNumber, 1) IN ('P', 'S')) \r\n"
                        + "   )\r\n");


                lstWbsResult = Db.Query<WBSStructure>(sql.ToString()).ToList();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }

        public void DebugWbs(string mark, List<WBSStructure> w, string Username)
        {
            string fWbs = "wbs_" + CommonFunction.CleanFilename(Username).Replace(".", "_");
            StringBuilder so = new StringBuilder("");
            foreach (var o in w)
            {
                so.AppendFormat("{0}\t{1}\r\n", o.WbsNumber, o.Description);
            }
            LoggingLogic.say(fWbs + "_" + mark + ".txt", so.ToString());
        }

        public List<WBSStructure> getWbsNumbers(UserData userData, PVFormData f)
        {
            int? txCode = f.TransactionCode;
            string division = userData.DIV_CD.ToString();
            if (txCode == null)
            {
                return new List<WBSStructure>(0);
            }
            List<WBSStructure> wbsNumbers = new List<WBSStructure>();

            try
            {
                int fiscalYear = logic.Sys.FiscalYear(DateTime.Now);
                decimal BOD = BoardOfDirector;
                int tt = f.TransactionCode ?? 0;
                bool isGlAccountProject = hQu<int>("GetIsGLAccountProject", tt).FirstOrDefault() > 0;

                wbsNumbers = hQx<WBSStructure>("sp_GetWbsNumbers",
                        new
                        {
                            fiscalYear = fiscalYear
                        ,
                            isFinanceDirectorate = ((userData.DIV_CD >= financeDirectorate) ? 1 : 0)
                        ,
                            division = division
                        ,
                            isProject = (isGlAccountProject ? 1 : 0)
                        ,
                            transactionCode = tt
                        }
                        , null, true, null, CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return wbsNumbers;
        }

        //fid.Hadid
        public List<WBSStructure> getWbsNumbersAccrued(UserData userData)
        {
            string fWbs = "wbsByUser_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
            string division = userData.DIV_CD.ToString();

            List<WBSStructure> lstWbsResult = new List<WBSStructure>();

            try
            {
                // List<string> GlAccProjects = logic.Sys.GetList("GL_ACCOUNT_PROJECT");

                //int fiscalYear = DateTime.Now.Year - 1; // cuman buat testing tar ilangin -1 nya
                int fiscalYear = DateTime.Now.Year;
                StringBuilder sql = new StringBuilder(
                            "-- getWbsNumbersByUser\r\n\r\n" +
                            "-- DIV_CD: " + division + " \r\n" +
                    // "-- GL_ACCOUNT_PROJECT: " + string.Join(";", GlAccProjects.ToArray()) + "\r\n" +
                            "\r\n" +
                            "SELECT DISTINCT v.WbsNumber, v.[Description] \r\n" +
                            "  FROM dbo.vw_WBS v \r\n" +
                            " WHERE (v.Year = " + (fiscalYear).str() + ") \r\n"
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );

                decimal BOD = BoardOfDirector;

                string divCondition = "";
                if (userData.DIV_CD >= financeDirectorate)
                {
                    sql.AppendLine("   -- Finance Directorate " + BOD.str());
                    divCondition = " AND v.Division = " + BOD.str();
                }
                else
                {
                    StringMap pad = logic.Sys.GetDict("PAD_BUDGET_MAPPING");
                    if (pad.Has(division))
                    {
                        sql.AppendLine("   -- IN PAD BUDGET MAPPING" + pad.KV());
                        string padDivisions = Quoten(pad.Get(division));

                        divCondition = " AND v.Division IN (" + padDivisions + ") ";
                    }
                    else
                    {
                        sql.AppendLine("   -- NOT IN PAD BUDGET MAPPING");
                        divCondition = " AND v.Division = '" + division + "'";
                    }
                }
                sql.AppendLine(divCondition);

                lstWbsResult = Db.Query<WBSStructure>(sql.ToString()).ToList();
               

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }
        public List<BookingNoStructure> getBookingNo(UserData userData, PVFormData f)
        {
            string fWbs = "bookingNo_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
            int? txCode = f.TransactionCode;
            string division = userData.DIV_CD.ToString();
            if (txCode == null || txCode != logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
            {
                return new List<BookingNoStructure>(0);
            }
            List<BookingNoStructure> lstBookingNo = new List<BookingNoStructure>();

            try
            {
                StringBuilder sql = new StringBuilder(
                            "-- getBookingNo\r\n\r\n" +
                            "-- DIV_CD: " + division + " TRANSCATION_TYPE: " + txCode.str() + "\r\n" +
                    // "-- GL_ACCOUNT_PROJECT: " + string.Join(";", GlAccProjects.ToArray()) + "\r\n" +
                            "\r\n" +
                            "SELECT DISTINCT b.BOOKING_NO BookingNo, v.WbsNumber, v.[Description] \r\n" +
                            "  FROM dbo.TB_R_ACCR_BALANCE b \r\n" +
                            "  JOIN vw_WBS v on b.WBS_NO_OLD = v.WbsNumber \r\n" +
                            "  JOIN TB_R_ACCR_LIST_D d on b.BOOKING_NO = d.BOOKING_NO \r\n" +
                            "  JOIN TB_R_ACCR_LIST_H h on d.ACCRUED_NO = h.ACCRUED_NO \r\n" +
                            " WHERE (b.BOOKING_STS = 0) \r\n" +
                            "   AND h.STATUS_CD = 105  AND b.PV_TYPE_CD = '1' \r\n"
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );

                decimal BOD = BoardOfDirector;

                string divCondition = "";
                if (userData.DIV_CD >= financeDirectorate)
                {
                    sql.AppendLine("   -- Finance Directorate " + BOD.str());
                    divCondition = " b.Division = " + BOD.str();
                }
                else
                {
                    StringMap pad = logic.Sys.GetDict("PAD_BUDGET_MAPPING");
                    if (pad.Has(division))
                    {
                        sql.AppendLine("   -- IN PAD BUDGET MAPPING" + pad.KV());
                        string padDivisions = Quoten(pad.Get(division));

                        divCondition = " b.Division IN (" + padDivisions + ") ";
                    }
                    else
                    {
                        sql.AppendLine("   -- NOT IN PAD BUDGET MAPPING");
                        divCondition = " b.DIVISION_ID = '" + division + "'";
                    }
                }
                sql.AppendLine("\r\n  AND");
                sql.AppendLine(divCondition);

                lstBookingNo = Db.Query<BookingNoStructure>(sql.ToString()).ToList();
                

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstBookingNo;
        }
        //end fid.Hadid

        public string Quoten(string nonquoted, string splitChar = ",", string quoteChar = "'")
        {
            if (nonquoted.isEmpty())
                return nonquoted;

            string[] splitz = nonquoted.Split(new string[] { splitChar }, StringSplitOptions.None);
            for (int i = 0; i < splitz.Length; i++)
            {
                string v = splitz[i].Trim();

                if (!(v.StartsWith(quoteChar) && v.EndsWith(quoteChar)))
                {
                    v = quoteChar + v + quoteChar;
                }
                else if (!v.StartsWith(quoteChar))
                {
                    v = quoteChar + v;
                }
                else if (!v.EndsWith(quoteChar))
                {
                    v = v + quoteChar;
                }
                splitz[i] = v;
            }
            return string.Join(splitChar, splitz);
        }

        public string getDescriptionDefaultWording(int? transactionTypeCode)
        {
            if (transactionTypeCode.HasValue)
            {
                return hQx<string>("getDescriptionDefaultWording", new { tt = @transactionTypeCode }).FirstOrDefault();
            }

            return null;
        }
        public List<VendorData> getVendorCodes()
        {
            return hQx<VendorData>("getVendorCodes", new { EXCLUDE_EXTERNAL_VENDOR = Common.AppSetting.EXCLUDE_EXTERNAL_VENDOR }).ToList();
        }
        public string getVendorCodeName(string code)
        {
            return hQx<string>("GetVendorNameByCode", new { code = code }).FirstOrDefault();
        }
        public string getVendorCodeByName(string name)
        {
            return Qx<string>("GetVendorCodeByName", new { name = name }).FirstOrDefault();
        }
        
        public List<CodeConstant> getCostCenterCodes(int productionFlag = 0, string div = "")
        {
            if (div.Int() >= financeDirectorate) div = BoardOfDirector.str(); // when director login assume from board of director division

            return hQx<CodeConstant>("GetCostCenterCodesByProdFlagDiv", new { @div = div, @prodFlag = productionFlag }).ToList();
        }
        public List<CostCenterCdData> GetCostCenters()
        {
            return hQu<CostCenterCdData>("GetCostCenters").ToList();
        }
        public CostCenterCdData getCostCenterByCode(string code, string div = "")
        {
            return Qx<CostCenterCdData>("GetCostCentersByCodeDiv", new { code = code, div = div }).FirstOrDefault();
        }
        public List<SAPDocumentNumber> getSAPDocumentNumbers(PVFormData formData)
        {
            return Qx<SAPDocumentNumber>("getSAPDocumentNumbers", new { docNo = formData.PVNumber??0, docYear = formData.PVYear??0 }).ToList();
        }

        public string getAttachmentCategoryName(string code)
        {
            return hQx<string>("GetAttachmentCategoryName", new { code = code }).FirstOrDefault();
        }
        public List<CodeConstant> getAttachmentCategories()
        {
            return hQu<CodeConstant>("getAttachmentCategories").ToList();
        }
        public string getCostCenterDescription(string code)
        {
            return hQx<string>("getCostCenterDescription", new { code = StrTo.RemoveQuotes(code) }).FirstOrDefault();
        }
        public List<ExchangeRate> getExchangeRates()
        {
            return (logic.Look.getExchangeRates());
        }
        public List<ExchangeRate> getExchangeRatesPerNo(int num, int year)
        {
            return Qx<ExchangeRate>("getExchangeRatesPerNo", new { anum = num, ayear = year }).ToList();
        }

        
        public IQueryable<BankKeysData> GetBankKeys()
        {
            return hQx<BankKeysData>("GetBankKey", null).AsQueryable();
        }

        public string getDivisionName(string divisionId)
        {
            return hQx<string>("getDivisionName", new { DivisionId = divisionId }).FirstOrDefault();
        }

        public List<Division> getDivisions()
        {
            return hQu<Division>("getDivisions").ToList();
        }


        public int? getPreviousDocumentStatus(PVFormData formData)
        {
            decimal referenceNumber = decimal.Parse(formData.PVNumber.ToString() + formData.PVYear.ToString());
            return hQu<int?>("getPreviousDistributionHistorySeq", referenceNumber).FirstOrDefault();

        }
        public void fetchAttachment(PVFormData formData, int pid = 0)
        {
            string pvNumber = formData.PVNumber.ToString();
            string pvYear = (formData.PVYear ?? DateTime.Now.Year).str();
            string refNumber = String.Concat(pvNumber, pvYear);
            if ((formData.PVNumber ?? 0) < 1)
            {
                refNumber = "T" + pid.str() + pvYear;
            }
            AttachmentTable attachmentTable = formData.AttachmentTable;

            fetchAttachment(refNumber, attachmentTable, pid);

           
        }
        
        //fid.Hadid
        public void fetchAttachment(string refNumber, AttachmentTable attachmentTable, int pid = 0)
        {
           

            List<FormAttachment> lstAttachment = attachmentTable.Attachments;
            lstAttachment.Clear();
            
            List<FormAttachment> ax = Qx<FormAttachment>("GetAttachmentByReff", new { REFERENCE_NO = refNumber }).ToList();
            foreach (var a in ax)
            {
                a.Blank = false;
                a.Url = CommonFunction.CombineUrl(Common.AppSetting.FTP_DOWNLOADHTTP, a.PATH, a.FileName);
            }
            lstAttachment.AddRange(ax);
            attachmentTable.addBlankRow();
        }
        //End fid.Hadid

        public void deleteAttachmentInfo(FormAttachment formAttachment)
        {
            Do("deleteAttachmentInfo", new { reffNo = formAttachment.ReferenceNumber, Filename = formAttachment.FileName });

        }
        public void saveAttachmentInfo(FormAttachment formAttachment, UserData userData)
        {
            Do("saveAttachmentInfo", 
                    new
                    {
                            ReferenceNumber= formAttachment.ReferenceNumber, 
                            FileName = formAttachment.FileName,
                            CategoryCode = formAttachment.CategoryCode,
                            Description = formAttachment.Description,
                            PATH = formAttachment.PATH,
                            UserName = userData.USERNAME
                    });            
            
        }

        public bool postToK2(PVFormData formData, UserData userData, bool payingVoucher)
        {
            ITick its = Ticker.Tick;
            its.In("postToK2");
            bool postResult = false;
            try
            {
                string moduleCode = "1";
                moduleCode = (payingVoucher) ? "1" : "2";

                Dictionary<string, string> submitData = new Dictionary<string, string>();
                submitData.Add("ApplicationID", Common.AppSetting.ApplicationID);
                submitData.Add("ApprovalLevel", "1");
                submitData.Add("ApproverClass", "1");
                submitData.Add("ApproverCount", "1");
                submitData.Add("CurrentDivCD", userData.DIV_CD.ToString());
                submitData.Add("CurrentPIC", userData.USERNAME);
                submitData.Add("EmailAddress", userData.EMAIL);
                string entertain = "0";
                int? transactionCode = formData.TransactionCode;
                List<int?> lstEntertainmentTransactions = getEntertainmentTransactionCodes();
                if (lstEntertainmentTransactions != null)
                {
                    foreach (int? c in lstEntertainmentTransactions)
                    {
                        if (transactionCode == c)
                        {
                            entertain = "1";
                            break;
                        }
                    }
                }
                submitData.Add("Entertain", entertain);

                submitData.Add("K2Host", "");
                submitData.Add("LimitClass", "0");
                submitData.Add("ModuleCD", moduleCode);
                submitData.Add("NextClass", "0");
                submitData.Add("PIC", userData.DIV_CD.ToString());
                submitData.Add("Reff_No", formData.PVNumber.ToString() + formData.PVYear.ToString());
                string registerDate = string.Format("{0:G}", DateTime.Now);
                submitData.Add("RegisterDt", registerDate);

                decimal? prevDocStatus = getPreviousDocumentStatus(formData);
                string val = "0";
                if ((prevDocStatus != null) && (prevDocStatus.Value == 0))
                {
                    val = "1";
                }
                submitData.Add("RejectedFinance", val);
                submitData.Add("StatusCD", formData.StatusCode.ToString());
                submitData.Add("StepCD", "");
                submitData.Add("VendorCD", formData.VendorCode);

                decimal totalAmount = 0;
                PVFormTable formTable = formData.FormTable;
                if (formTable != null)
                {
                    totalAmount = formData.GetTotalAmount();
                    totalAmount = Math.Round(totalAmount);
                }
                string amt = totalAmount.ToString();
                // prevent overflow, K2 receive only int data type
                if (totalAmount > int.MaxValue) amt = int.MaxValue.ToString();
                if (totalAmount < int.MinValue) amt = int.MinValue.ToString();

                submitData.Add("TotalAmount", amt);

                //added by Akhmad Nuryanto, delete worklist if resubmitting form after rejected
                if (formData.isResubmitting)
                {
                   
                    logic.k2.deleteprocess(formData.PVNumber.ToString() + formData.PVYear.ToString());

                }
                //end of addition by Akhmad Nuryanto

                its.In("K2SendCommandWithCheckWorklist");
                if (formData.PostK2ByWorklistChecking)
                {

                    postResult = logic.WorkFlow.K2SendCommandWithCheckWorklist(formData.PVNumber.ToString() + formData.PVYear.ToString(),
                                                                    userData, formData.K2Command, submitData);

                }
                else
                {

                    postResult = logic.WorkFlow.K2SendCommandWithoutCheckWorklist(formData.PVNumber.ToString() + formData.PVYear.ToString(),
                                                                        userData, formData.K2Command, submitData);
                }
                its.Out();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }
            finally
            {
                its.Out();
            }
            formData.PostedK2 = postResult;
            return postResult;
        }
       
        public List<int?> getEntertainmentTransactionCodes()
        {
            return hQu<int?>("getEntertainmentTransactionCodes").ToList();
        }

        protected string me = "";

        public bool Upload(string xlsFilename, string metaFile,
            List<PVFormDetail> dd,
            ref string[][] derr,
            int TransactionCd, PVFormData datas,
            UserData ux, int pid)
        {
            bool Ok = false;
            me = "FormPersistence.Upload";

            // bool isBudget = isBudgeted(TransactionCd);
            int[] noProdCheck = DivNoProductionFlagCheck();

            _errs.Clear();

            if (!System.IO.File.Exists(xlsFilename))
            {
                _errs.Add("Uploaded File not found");
            }

            string _meta = System.IO.File.ReadAllText(metaFile);
            if (string.IsNullOrEmpty(_meta))
            {
                _errs.Add("Metadata not found");
            }
            if (_errs.Count > 0) return false;

            ceu = new CommonExcelUpload();
            ceu.UserName = ux.USERNAME;
            ceu.PID = pid;

            ceu.LogWriter = logic.Msg.WriteMessage;
            Dictionary<string, object> _data;
            _data = ceu.Read(_meta, xlsFilename);

            if (_data.Keys.Count < 1)
            {
                _errs.Add("Empty file");
                return false;
            }

            // validate mandatory field and field length  
            // => this is *very* critical because process will fail 
            // when inserted data is not valid
            Ok = ceu.Check();

            // validate Detail 
            DataTable x = ceu.Rows;

            if (!Ok)
            {
                ShowFeedback(ceu,  x, ref derr);
                return false;

                /// FOR TESTING OVERRIDE 
                /// Ok = true;
            }

            PutUpload(x, TransactionCd, datas.VendorCode, pid, xlsFilename, ux, ref dd, ref derr); /// formerly use steps in PutData

            Ok = (_errs.Count < 1);
            
            return Ok;
        }


        public bool ShowFeedback(CommonExcelUpload ceu, DataTable x, ref string[][] derr)
        {
            foreach (string s in ceu.ReadError)
            {
                _errs.Add(s);
            }

            if (x.Rows.Count > 0)
            {
                derr = new string[x.Rows.Count][];
                int ri = 0;
                int colCount = x.Columns.Count;
                foreach (DataRow r in x.Rows)
                {
                    derr[ri] = new string[colCount];

                    for (int i = 0; i < colCount; i++)
                    {
                        derr[ri][i] = Convert.ToString(r[i]);
                    }
                    ri++;
                }
            }
            else
            {
                derr = new string[1][];
                derr[0] = new string[5];
                for (int i = 0; i < 5; i++)
                {
                    derr[0][i] = "";
                }
                derr[0][15] = CommonFunction.CommaJoin(ceu.ReadError, "\r\n");
            }

            return (_errs.Count < 1);
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x">DataTable as input data, structures as described in metadata, must have same column count with user defined data table </param>
        /// <param name="TransactionCd">required param for header </param>
        /// <param name="VendorCd">required param for header</param>
        /// <param name="pid"> process id related to upload, for monitoring and atomicity</param>
        /// <param name="xlsFilename">uploaded file name</param>
        /// <param name="ux">User object</param>
        /// <param name="dd">Output data structure containing processed uploaded contents </param>
        /// <param name="derr">Output data structure containing errors </param>
        /// <returns></returns>
        public bool PutUpload(DataTable x
                , int TransactionCd
                , string VendorCd
                , int pid
                , string xlsFilename
                , UserData ux
            // output
                , ref List<PVFormDetail> dd
                , ref string[][] derr)
        {
            string spName = Qx<string>("GetSpFromTx",
                    new { tt = TransactionCd.str() }).FirstOrDefault();

            Exec("sp_PutUploadH",
                    new
                    {
                        pid = pid,
                        transactionCd = TransactionCd,
                        vendorCd = VendorCd,
                        uploadFileName = Path.GetFileName(xlsFilename),
                        uid = ux.USERNAME
                    },
                        null, null, CommandType.StoredProcedure);

            if (!spName.isEmpty() && x != null)
            {
                SqlBulkCopy bulk = new SqlBulkCopy(Db as SqlConnection);

                DecorateUpload(x, pid, ux.USERNAME);
                bulk.DestinationTableName = "TB_T_UPLOAD_D";
                bulk.ColumnMappings.Clear();
                for (int i = 0; i < x.Columns.Count; i++)
                {
                    bulk.ColumnMappings.Add(x.Columns[i].ColumnName, x.Columns[i].ColumnName);
                }
                PreExecuteTest(x, ux, pid);

                bulk.WriteToServer(x);

                Exec(spName, new { pid = pid, uid = ux.USERNAME });
            }

            dd.AddRange(Qx<PVFormDetail>("GetLastUpload", new { pid = pid }).ToList());
            List<ErrorUploadData> ErrUploads = Qx<ErrorUploadData>("GetErrorUploadData", new { pid = pid }).ToList();
            if (ErrUploads != null && ErrUploads.Count>0)
                _errs.AddRange(ErrUploads.Select(e => e.ERRMSG));
            return true;
        }

        public void DecorateUpload(DataTable x, int pid, string createdBy)
        {
            if (x.Columns.IndexOf("PROCESS_ID") < 0)
            {
                x.Columns.Add("PROCESS_ID", typeof(int));
            }
            if (x.Columns.IndexOf("CREATED_BY") < 0)
            {
                x.Columns.Add("CREATED_BY", typeof(string));
            }
            if (x.Columns.IndexOf("CREATED_DT") < 0)
            {
                x.Columns.Add("CREATED_DT", typeof(DateTime));
            }
            if (x.Columns.IndexOf("SEQ_NO") < 0)
            {
                x.Columns.Add("SEQ_NO", typeof(int));
            }
            List<int> existingNum = new List<int>();
            List<int> d = new List<int>();
            int maxseq = 0;
            for (int i = 0; i < x.Rows.Count; i++)
            {
               DataRow r= x.Rows[i] ;
               int seq = (r["SEQ_NO"] as int?)??0;
               if (seq > maxseq) maxseq = seq;
               if (seq <= 0)
                   d.Add(i);
               else
               {
                   if (existingNum.Contains(seq))
                   {
                       d.Add(i);
                   }
                   else
                       existingNum.Add(seq);
               }

            }

            if (d.Count > 0)
            {
                for (int i = 0; i < d.Count; i++)
                {
                    DataRow r = x.Rows[d[i]];
                    maxseq ++;
                    r["SEQ_NO"] = maxseq;
                }
            }

            for (int i = 0; i < x.Rows.Count; i++)
            {
                DataRow r = x.Rows[i];

                r["PROCESS_ID"] = pid;
                r["CREATED_BY"] = createdBy;
                r["CREATED_DT"] = DateTime.Now;
            }
        }


        public void PreExecuteTest(DataTable x, UserData ux, int pid)
        {
            StringBuilder stru = new StringBuilder("DataTableStructures\r\n");

            if (logic.Sys.GetNum("test.CommonExcelUpload/DataTableStru") == 1)
            {
                for (int i = 0; i < x.Columns.Count; i++)
                {
                    DataColumn c = x.Columns[i];
                    stru.AppendFormat("[{0}]\t{1}\t{2}\t{3}\r\n", i, c.ColumnName, c.DataType.ToString(), c.MaxLength.ToString());
                }
                logic.Log.Log("MSTD00001INF", stru.ToString(), "DataTableStru", ux.USERNAME, "9.1.1", pid);
            }

            if (logic.Sys.GetNum("test.CommonExcelUpload/PreExecuteTest") == 1)
            {
                StringBuilder b = new StringBuilder("");
                for (int j = 0; j < x.Columns.Count; j++)
                {
                    if (j > 0) b.Append(";");
                    b.Append(x.Columns[j].ColumnName);
                }
                b.Append("\r\n");
                for (int i = 0; i < x.Rows.Count; i++)
                {
                    DataRow r = x.Rows[i];
                    for (int j = 0; j < x.Columns.Count; j++)
                    {
                        if (j > 0) b.Append(";");
                        string s = r[j].str();
                        if (!s.isEmpty() && (s.Contains('\r') || s.Contains('\n')))
                        {
                            s = s.Replace("\r", " ");
                            s = s.Replace("\n", " ");
                            b.Append(s);
                        }
                        else
                            b.Append(r[j]);

                    }
                    b.Append("\r\n");
                }
                logic.Log.Log("MSTD00001INF", b.ToString(), "DataContents", ux.USERNAME, "9.1.1", pid);
            }


        }

        public bool GetGlAccountCostCenterFlag(int gla)
        {
            var x = Qx<int?>("GetGlAccountCostCenterFlag", new { GlAccount = gla });

            return (x.FirstOrDefault() ?? 0) == 1;

        
        }

        public bool isAccrDoc(string doc)
        {
            string code = doc.Substring(0, 2);
            if (code == "LA" || code == "BS" || code == "AE")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string getAccrCode(string doc)
        {
            string code = doc.Substring(0, 2);
            return code;
        }

        public List<WorklistData> GetWorklistByDoc(string doc, string sql)
        {
            string k = sql + "(" + doc + ")";
            string SQL = string.Format(logic.SQL[sql], logic.SQL["GetSplitByComma"]);
            List<WorklistData> h = Heap.Get<List<WorklistData>>(k);
            List<WorklistData> _q = new List<WorklistData>();
            if (h != null)
                return h;
            try
            {
                IDbConnection _db = logic.Cx.DB;
                if (!doc.isEmpty())
                {
                    DynamicParameters dp = new DynamicParameters();
                    dp.Add("@p1", doc);
                    _q = _db.Query<WorklistData>(SQL, dp, null, true, null, CommandType.Text).ToList();
                    Heap.Set<List<WorklistData>>(k, _q, Heap.Duration * 5);
                }
            }
            catch (Exception e)
            {
                LoggingLogic.say(sql, SQL + "\r\n@p1='{0}'\r\n" , doc);
                LoggingLogic.err(e);
            }
            return _q;
        }

        public void FillWorklistData(List<WorklistData> w)
        {
            List<string> pv = new List<string>();
            List<string> rv = new List<string>();
            List<string> accr = new List<string>();
            List<string> accrList = new List<string>();
            List<string> accrShifting = new List<string>();
            List<string> accrExtend = new List<string>();

            for (int i = 0; i < w.Count; i++)
            {
                string reff = w[i].Folio.Trim();

                if (!isAccrDoc(w[i].Folio)) // if folio is not a accrued document
                {
                    int docNo = reff.Substring(0, reff.Length - 4).Int(0);
                    string _no = docNo.str();
                    if (docNo > 0 && docNo < 600000000)
                    {
                        pv.Add(_no);
                    }
                    else if (docNo >= 600000000)
                    {
                        rv.Add(_no);
                    }
                }
                else
                {
                    switch (getAccrCode(reff))
                    {
                        case "LA":
                            accrList.Add(reff);
                            break;
                        case "BS":
                            accrShifting.Add(reff);
                            break;
                        case "AE":
                            accrExtend.Add(reff);
                            break;
                        default:
                            accr.Add(reff);
                            break;
                    }
                }
            }
            string pvs = string.Join(",", pv.Distinct().ToArray());
            string rvs = string.Join(",", rv.Distinct().ToArray());
            
            string accrLists = string.Join(",", accrList.Distinct().ToArray());
            string accrShiftings = string.Join(",", accrShifting.Distinct().ToArray());
            string accrExtends = string.Join(",", accrExtend.Distinct().ToArray());
            
            List<WorklistData> q = new List<WorklistData>();

            q.AddRange(GetWorklistByDoc(pvs, "WorklistPV"));
            q.AddRange(GetWorklistByDoc(rvs, "WorklistRV"));
            
            q.AddRange(GetWorklistByDoc(accrLists, "WorklistAccruedList"));
            q.AddRange(GetWorklistByDoc(accrShiftings, "WorklistAccruedShifting"));
            q.AddRange(GetWorklistByDoc(accrExtends, "WorklistAccruedExtend"));
            
            if (q != null && q.Count > 0)
            {
                for (int i = 0; i < w.Count; i++)
                {
                    WorklistData d = q.Where(a => a.Folio == w[i].Folio).FirstOrDefault();
                    if (d != null)
                    {
                        BaseBusinessLogic.CopyWorklistData(w[i], d);
                    }
                }
            }

            for (int i = w.Count - 1; i >= 0; i--)
            {
                if (string.IsNullOrEmpty(w[i].StatusName))
                    w.RemoveAt(i);
            }
        }

        public string GetDivName(string folio, UserData userData)
        {
            string divName = "";

            string code = folio.Substring(0, 2);
            divName = logic.AccrList.GetDivNameFromFolio(code, folio, userData);
            
            return divName;
        }
        public string DownloadError(int _processId, string uploadedFile, string sourceFile, string targetDirectory, string[][] e)
        {
            string fileName = 
                    Util.GetTmp(targetDirectory,
                    Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(uploadedFile))
                            + "_" + _processId.str() + "_error", ".txt");

            File.WriteAllText(fileName, string.Join("\r\n", _errs));

            return Path.GetFileName(fileName);
        }

        public DateTime GetDocDate(string no, string year)
        {
            DateTime DocDate = new DateTime();
            DateTime? dt = Qx<DateTime?>("GetDocDate", new { docNo = no, docYear = year }).FirstOrDefault();
            if (dt != null) DocDate = dt??new DateTime();

            return DocDate;
        }


        public long GetDocNo(byte DocCd, string username)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@doc_cd", DocCd);
            d.Add("@user", username);
            d.Add("@NO", null, DbType.Int32, ParameterDirection.Output);

            Exec("sp_GetDocNumber", d);

            long ID = d.Get<int>("@NO");
            return ID;
        }


        /* Start Rinda Rahayu 20160411 */
        public DateTime GetPlanningPaymentDate(int pvCode, int year)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@REFF_NO", pvCode + "" + year);
            d.Add("@MODULE_CD", null);
            d.Add("@PlanningPaymentDate", null, DbType.DateTime, ParameterDirection.Output);

            Exec("sp_GetPlanningPaymentDate", d);

            DateTime planningPaymentDate = d.Get<DateTime>("@PlanningPaymentDate");

            return planningPaymentDate;
        }
        /* End Rinda Rahayu 20160411 */

        public void fetchNote(PVFormData formData)
        {
            List<FormNote> l = Qu<FormNote>("GetListNoticeByDocNoYear", formData.PVNumber, formData.PVYear).ToList();
            if (l.Any())
            {
                formData.Notes.Clear();
                l.ForEach(x => formData.Notes.Add(x));
            }
        }

        //fid.Hadid
        public void fetchNote(BaseAccrData formData)
        {
            List<FormNote> l = Qu<FormNote>("GetListNoticeByDocNoYear", formData.ReffNo, formData.CreatedDt.Year).ToList();
            if (l.Any())
            {
                formData.Notes.Clear();
                l.ForEach(x => formData.Notes.Add(x));
            }
            //fid.pras
            else formData.Notes.Clear();
        }
        //end fid.Hadid
        public List<FormAttachment> getAttachments(string referenceNo)
        {
            return Qu<FormAttachment>("getAttachments", referenceNo).ToList();
        }

        public bool MoveAttachment(string src, decimal seq, string dst, string yy)
        {
            string olde = src + yy;
            string neo = dst + yy;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var q = (from a in db.TB_R_ATTACHMENT
                         where a.REFERENCE_NO == olde && a.REF_SEQ_NO == seq
                         select a).FirstOrDefault();
                if (q != null)
                {
                    string adir = "";
                    string[] a = q.DIRECTORY.Split('/');
                    if (a.Length > 0)
                    {
                        a[a.Length - 1] = dst;
                        adir = string.Join("/", a, 0, a.Length);
                    }

                    db.AddToTB_R_ATTACHMENT(new TB_R_ATTACHMENT()
                    {
                        REFERENCE_NO = neo,
                        REF_SEQ_NO = q.REF_SEQ_NO,
                        ATTACH_CD = q.ATTACH_CD,
                        DESCRIPTION = q.DESCRIPTION,
                        DIRECTORY = adir,
                        FILE_NAME = q.FILE_NAME,
                        CREATED_DT = DateTime.Now,
                        CREATED_BY = q.CREATED_BY
                    });
                    db.DeleteObject(q);

                    db.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
        }

        public virtual PVFormData search(int pvNumber, int pvYear)
        {
            return null;
        }
        public virtual AccrFormData search(string refNo)
        {
            return null;
        }

        protected virtual List<PVFormDetail> GetDetail(PVFormData f, ELVIS_DBEntities db)
        {
            return new List<PVFormDetail>();
        }

        public void LoadOri(PVFormData f)
        {
            f.FormTable.DataList = GetOri(f, db);
        }

        protected List<PVFormDetail> GetOri(PVFormData f, ELVIS_DBEntities db)
        {
            int _no, _yy;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? DateTime.Now.Year;
            List<PVFormDetail> l = new List<PVFormDetail>();

            var detailResult = logic.Base.GetOriginal(_no, _yy);

            if (!detailResult.Any())
                return l;

            PVFormTable formTable = f.FormTable;
            List<CodeConstant> lstCostCenterCodes = getCostCenterCodes();
            string standardDescriptionWording = null;
            PVFormDetail formDetail;
            int seqNumber = 0;

            string vendorName = getVendorCodeName(f.VendorCode);
            if (vendorName != null)
            {
                vendorName = vendorName.Trim();
            }
            string divisionName = getDivisionName(f.DivisionID);
            foreach (var detail in detailResult)
            {
                if (standardDescriptionWording == null)
                {
                    standardDescriptionWording = getDescriptionDefaultWording(f.TransactionCode);
                }

                seqNumber++;
                formDetail = new PVFormDetail()
                {
                    Persisted = true,
                    SequenceNumber = detail.SEQ_NO,
                    DisplaySequenceNumber = seqNumber,
                    Amount = Math.Round(detail.AMOUNT),
                    CostCenterCode = detail.COST_CENTER_CD,
                    CostCenterName = getCostCenterDescription(detail.COST_CENTER_CD),
                    CurrencyCode = detail.CURRENCY_CD,
                    Description = detail.DESCRIPTION,
                    StandardDescriptionWording = standardDescriptionWording,
                    //WbsNumber = detail.WBS_NO,
                    InvoiceNumber = detail.INVOICE_NO,
                    GlAccount = detail.GL_ACCOUNT.str(),

                    TaxCode = detail.TAX_CD,
                    TaxNumber = detail.TAX_NO,
                    ItemTransaction = detail.ITEM_TRANSACTION_CD,

                    #region EFB
                    WHT_TAX_CODE = detail.WHT_TAX_CODE,
                    WHT_TAX_TARIFF = detail.WHT_TAX_TARIFF,
                    WHT_TAX_ADDITIONAL_INFO = detail.WHT_TAX_ADDITIONAL_INFO,
                    WHT_DPP_PPh_AMOUNT = detail.WHT_DPP_PPh_AMOUNT
                    #endregion
                };

                if (formDetail.ItemTransaction == null)
                {
                    formDetail.ItemTransaction = 1;
                }

                formDetail.InvoiceNumberUrl = string.Format(
                    "/50Invoice/InvoiceListDet.aspx?invoiceNo={0}"
                    + "&invoiceDate={1}&vendorCode={2}&vendorName={3}"
                    + "&division={4}&invoiceStatus={5}&pvNo={6}&pvYear={7}",
                    formDetail.InvoiceNumber,
                    string.Format("{0:dd MMM yyyy}", detail.INVOICE_DATE),
                    f.VendorCode, vendorName, divisionName, f.StatusCode,
                    f.PVNumber, f.PVYear);

                formDetail.CostCenterName = (from a in lstCostCenterCodes
                                             where a.Code == formDetail.CostCenterCode
                                             select a.Description).FirstOrDefault();
                l.Add(formDetail);
            }
            return l;
        }

        public virtual bool save(PVFormData formData, UserData userData, bool financeHeader)
        {
            string fn = ((formData.PVNumber ?? 0) == 0 || (formData.PVYear ?? 0) == 0) ?
                    formData.ScreenType + "_%TIME%_%MS%" + "save"
                    : string.Format("{0}{1}", formData.PVNumber, formData.PVYear);

            System.IO.File.WriteAllText(Util.GetTmp(LoggingLogic.GetPath(), fn, ".txt")
                , formData.dump());

            return true;
        }
        public virtual bool save(AccrFormData formData, UserData userData)
        {
            string fn = (formData.AccruedNo.isEmpty() ?
                    "Accrued_%TIME%_%MS%" + "save"
                    : string.Format("{0}", formData.AccruedNo));

            System.IO.File.WriteAllText(Util.GetTmp(LoggingLogic.GetPath(), fn, ".txt")
                , formData.dump());

            return true;
        }
        
        public virtual Dictionary<string, decimal> GetSuspenseAmounts(int pvNo, int pvYear)
        {
            return null;
        }

        protected string cat(string x, string y)
        {
            return ((x ?? "") + " " + (y ?? "")).Trim();
        }

        public virtual string GetSummary(UserData u, string _imagePath, string template, int? num, int? year)
        {
            return null;
        }

        public virtual string WriteSummary(string imagePath, string outputPath,
                              UserData _UserData,
                              string contentTemp)
        {
            return null;
        }

        public virtual bool Reverse(string uid, PVFormData f)
        {
            return true;
        }

        public virtual bool Reverse(string uid, AccrFormData f)
        {
            return true;
        }

        public virtual bool BudgetCheckReverse(string uid, PVFormData d, ref string m)
        {
            if (Common.AppSetting.SKIP_BUDGET_CHECK || !Common.AppSetting.BUDGET_CHECK_REVERSE)
            {
                return true;
            }
            if (d.BudgetNumber.isEmpty()) return true;
            int fiscalYear = logic.Sys.FiscalYear(d.PVDate ?? new DateTime(StrTo.NULL_YEAR, 1, 1));
            BudgetCheckInput iBu = new BudgetCheckInput()
            {
                DOC_NO = d.PVNumber ?? 0,
                //DOC_YEAR = d.PVYear ?? 0,
                DOC_YEAR = fiscalYear,
                DOC_STATUS = (int)BudgetCheckStatus.Reverse,
                WBS_NO = d.BudgetNumber,
                AMOUNT = Math.Round((decimal)d.GetTotalAmount())
            };
            BudgetCheckResult o = logic.SAP.BudgetCheck(iBu, UserName);
            m = o.MESSAGE;
            return (o.STATUS.Equals("S"));
        }

        public bool WaitStatusChanged(int no, int year, int DocCd, int Status, int timeout = 10)
        {
            int status = 0;
            int x = 0;
            if (no == 0 || year == 0 || timeout > 60000) return false;
           
                status = Qx<int>("GetLastStatusByDocNo", new { docCd = DocCd, no = no, yy = year }).FirstOrDefault();
               
                while (status == Status && x < timeout)
                {
                    status = Qx<int>("GetLastStatusByDocNo", new { docCd = DocCd, no = no, yy = year }).FirstOrDefault();
                    
                    System.Threading.Thread.Sleep(500);
                    x++;
                }
            
            return status != Status;
        }

        public UserData Sup(UserData x)
        {
            string DCD = x.DIV_CD.str();
            string DpCD = x.DEPARTMENT_ID;
            string uid = x.USERNAME;
            int CLASS = 1;
            string SID = null;
            UserData o = new UserData();

            var q = (from u in db.vw_User
                     where u.USERNAME != null && u.USERNAME == uid
                     select u).FirstOrDefault();
            if (q != null)
            {
                CLASS = q.CLASS ?? 0;
                SID = q.SECTION_ID;
            }

            var r = (from y in db.vw_User
                     where (SID == null || (y.SECTION_ID != null && y.SECTION_ID == SID))
                     && (DpCD == null || (y.DEPARTMENT_ID != null && y.DEPARTMENT_ID == DpCD))
                     && (DCD == null || (y.DIVISION_ID != null && y.DIVISION_ID == DCD))
                     && (y.CLASS != null && y.CLASS > CLASS)
                     select y).FirstOrDefault();


            if (r != null)
            {
                o.FIRST_NAME = r.FIRST_NAME;
                o.LAST_NAME = r.LAST_NAME;
                o.USERNAME = r.USERNAME;
                o.DIV_CD = r.DIVISION_ID.Dec();
                o.DEPARTMENT_ID = r.DEPARTMENT_ID;
                o.TITLE = r.TITLE;
            }

            return o;
        }

       
        public void save(int no, int yy, OneTimeVendorData otv)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var q = from o in db.TB_R_ONE_TIME_VENDOR
                        where o.PV_NO == no
                        && o.PV_YEAR == yy
                        select o;

                TB_R_ONE_TIME_VENDOR v = null;
                bool isNew = false;
                if (q.Any())
                {
                    v = q.FirstOrDefault();
                }
                else
                {
                    isNew = true;
                    v = new TB_R_ONE_TIME_VENDOR()
                    {
                        PV_NO = no,
                        PV_YEAR = yy,
                    };
                }
                v.TITLE = otv.TITLE;
                v.NAME1 = otv.NAME1;
                v.NAME2 = otv.NAME2;
                v.STREET = otv.STREET;
                v.CITY = otv.CITY;

                v.BANK_KEY = otv.BANK_KEY;

                v.BANK_ACCOUNT = otv.BANK_ACCOUNT;
                if (isNew)
                {
                    db.AddToTB_R_ONE_TIME_VENDOR(v);
                }
                db.SaveChanges();
            }
        }

        public void saveOri(PVFormData formData, UserData userData, ELVIS_DBEntities db)
        {
            // get Existing 
            int _no, _yy;
            _no = formData.PVNumber ?? 0;
            _yy = formData.PVYear ?? 0;
            var q = from oa in db.TB_R_ORIGINAL_AMOUNT
                    where oa.DOC_NO == _no && oa.DOC_YEAR == _yy
                    select oa;

            if (q.Any())
            {
                List<OriginalAmountData> oa = new List<OriginalAmountData>();
                List<OriginalAmountData> oadd = new List<OriginalAmountData>();

                foreach (var o in q.ToList())
                {
                    PVFormDetail fd = formData.Details
                        .Where(a => a.SequenceNumber == o.SEQ_NO)
                        .FirstOrDefault();

                    if (fd == null)
                    {
                        oa.Add(new OriginalAmountData()
                        {
                            DOC_NO = o.DOC_NO,
                            DOC_YEAR = o.DOC_YEAR,
                            SEQ_NO = o.SEQ_NO
                        });
                    }
                }

                // remove existing 
                foreach (OriginalAmountData o in oa)
                {
                    var d = (from ox in db.TB_R_ORIGINAL_AMOUNT
                             where ox.DOC_YEAR == _yy
                             && ox.DOC_NO == _no
                             && ox.SEQ_NO == o.SEQ_NO
                             select ox).FirstOrDefault();
                    if (d != null)
                        db.DeleteObject(d);
                }
            }

            List<ExchangeRate> le = getExchangeRates();

            foreach (PVFormDetail d in formData.Details)
            {
                int _seq = d.SequenceNumber;

                var x = (from ox in db.TB_R_ORIGINAL_AMOUNT
                         where ox.DOC_YEAR == _yy
                         && ox.DOC_NO == _no
                         && ox.SEQ_NO == _seq
                         select ox).FirstOrDefault();

                decimal er = (decimal)(from e in le
                                       where e.CurrencyCode == d.CurrencyCode
                                       select e.Rate).FirstOrDefault();
                LoggingLogic.say(_no.str(), "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}",
                     _no,
                     _yy,
                     _seq,
                     d.CurrencyCode,
                     er,
                    Math.Round(d.Amount),
                     0,
                     0,
                     d.CostCenterCode,
                     d.Description,
                     d.InvoiceNumber,
                     d.ItemTransaction,
                     d.GlAccount,
                     d.TaxNumber,
                     d.TaxCode,
                     userData.USERNAME,
                     today,
                     d.WHT_TAX_CODE,
                     d.WHT_TAX_TARIFF,
                     d.WHT_TAX_ADDITIONAL_INFO,
                     d.WHT_DPP_PPh_AMOUNT
                    );
                if (x != null)
                {
                    x.CURRENCY_CD = d.CurrencyCode;
                    x.EXCHANGE_RATE = er;
                    x.AMOUNT = Math.Round((decimal)d.Amount);
                    x.SPENT_AMOUNT = 0;
                    x.SUSPENSE_AMOUNT = 0;

                    x.COST_CENTER_CD = d.CostCenterCode;
                    x.DESCRIPTION = d.Description;
                    x.INVOICE_NO = d.InvoiceNumber;
                    x.ITEM_TRANSACTION_CD = d.ItemTransaction;
                    x.GL_ACCOUNT = d.GlAccount.isEmpty() ?
                            GLAccount(formData.TransactionCode ?? 0, d.ItemTransaction ?? 0)
                            : d.GlAccount.Int();
                    x.TAX_NO = d.TaxNumber;
                    x.TAX_CD = d.TaxCode;

                    x.CHANGED_BY = userData.USERNAME;
                    x.CHANGED_DT = today;
                    #region EFB
                    x.WHT_TAX_CODE = d.WHT_TAX_CODE;
                    x.WHT_TAX_TARIFF = d.WHT_TAX_TARIFF;
                    x.WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO;
                    x.WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT;
                    #endregion
                    db.SaveChanges();
                }
                else
                {
                    db.AddToTB_R_ORIGINAL_AMOUNT(
                        new TB_R_ORIGINAL_AMOUNT()
                        {
                            DOC_NO = _no,
                            DOC_YEAR = _yy,
                            SEQ_NO = _seq,
                            CURRENCY_CD = d.CurrencyCode,
                            EXCHANGE_RATE = er,
                            AMOUNT = Math.Round((decimal)d.Amount),
                            SPENT_AMOUNT = 0,
                            SUSPENSE_AMOUNT = 0,
                            COST_CENTER_CD = d.CostCenterCode,
                            DESCRIPTION = d.Description,
                            INVOICE_NO = d.InvoiceNumber,
                            ITEM_TRANSACTION_CD = d.ItemTransaction,
                            GL_ACCOUNT = d.GlAccount.isEmpty() ?
                                GLAccount(formData.TransactionCode ?? 0, d.ItemTransaction ?? 0)
                                : d.GlAccount.Int(),
                            TAX_NO = d.TaxNumber,
                            TAX_CD = d.TaxCode,
                            CREATED_BY = userData.USERNAME,
                            CREATED_DT = today,

                            #region EFB
                            WHT_TAX_CODE = d.WHT_TAX_CODE,
                            WHT_TAX_TARIFF = d.WHT_TAX_TARIFF,
                            WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO,
                            WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT
                            #endregion
                });
                }
                db.SaveChanges();
            }
        }


        public virtual List<BudgetCheckInput> GetBudgetNumberHistory(int _no, int _year, string BudgetNo)
        {
            return new List<BudgetCheckInput>();
        }

        public List<PVFormDetail> GetPV(PVFormData f, ELVIS_DBEntities db)
        {
            int _no, _yy;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? DateTime.Now.Year;
            List<PVFormDetail> l = new List<PVFormDetail>();

            var detailQuery = from t in db.TB_R_PV_D
                              where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                              select t;
            var detailResult = detailQuery.ToList();
            if (!detailResult.Any())
            {
                _yy--;
                detailQuery = from t in db.TB_R_PV_D
                              where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                              select t;
                detailResult = detailQuery.ToList();
                if (!detailResult.Any())
                {
                    _yy += 2;
                    detailQuery = from t in db.TB_R_PV_D
                                  where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                                  select t;
                    detailResult = detailQuery.ToList();
                }
            }
            if (!detailResult.Any()) return l;

            PVFormTable formTable = f.FormTable;
            string standardDescriptionWording = null;
            PVFormDetail formDetail;
            int seqNumber = 0;

            string vendorName = getVendorCodeName(f.VendorCode);
            if (vendorName != null)
            {
                vendorName = vendorName.Trim();
            }
            string divisionName = getDivisionName(f.DivisionID);
            foreach (var detail in detailResult)
            {
                if (standardDescriptionWording == null)
                {
                    standardDescriptionWording = getDescriptionDefaultWording(f.TransactionCode);
                }

                seqNumber++;
                formDetail = new PVFormDetail()
                {
                    Persisted = true,
                    SequenceNumber = detail.SEQ_NO,
                    DisplaySequenceNumber = seqNumber,
                    Amount = detail.AMOUNT,
                    CostCenterCode = detail.COST_CENTER_CD,
                    CostCenterName = getCostCenterDescription(detail.COST_CENTER_CD),
                    CurrencyCode = detail.CURRENCY_CD,
                    Description = detail.DESCRIPTION,
                    StandardDescriptionWording = standardDescriptionWording,
                    InvoiceNumber = detail.INVOICE_NO,
                    GlAccount = detail.GL_ACCOUNT.str(),
                    FromCurr = detail.FROM_CURR,
                    FromSeq = detail.FROM_SEQ,
                    ItemNo = detail.ITEM_NO,
                    TaxCode = detail.TAX_CD,
                    TaxNumber = detail.TAX_NO,
                    InvoiceDate = detail.INVOICE_DATE,
                    ItemTransaction = detail.ITEM_TRANSACTION_CD,
                    #region EFB
                    WHT_TAX_CODE = detail.WHT_TAX_CODE,
                    WHT_TAX_TARIFF = detail.WHT_TAX_TARIFF,
                    WHT_TAX_ADDITIONAL_INFO = detail.WHT_TAX_ADDITIONAL_INFO,
                    WHT_DPP_PPh_AMOUNT = detail.WHT_DPP_PPh_AMOUNT
                    #endregion
                };

                if (formDetail.ItemTransaction == null)
                {
                    formDetail.ItemTransaction = 1;
                }

                formDetail.InvoiceNumberUrl = string.Format(
                    "/50Invoice/InvoiceListDet.aspx?invoiceNo={0}"
                    + "&invoiceDate={1}&vendorCode={2}&vendorName={3}"
                    + "&division={4}&invoiceStatus={5}&pvNo={6}&pvYear={7}",
                    formDetail.InvoiceNumber,
                    string.Format("{0:dd MMM yyyy}", detail.INVOICE_DATE),
                    f.VendorCode, vendorName, divisionName, f.StatusCode,
                    f.PVNumber, f.PVYear);
                //formDetail.CostCenterName = getCostCenterDescription(formDetail.CostCenterCode);
                l.Add(formDetail);
            }

            return l;
        }

        private string DescCurrRate(string desc, string curr, decimal Rate)
        {
            string d = "";
            if (!desc.isEmpty())
                d = desc;
            if (d.Length > 20)
                d = d.Substring(0, 20);
            d += " | " + curr + " " + CommonFunction.Eval_Curr("IDR", Rate);
            return d;
        }

        private string DescCashierPolicy(string curr, decimal original, decimal rate)
        {
            return string.Format("{0} {1} | Rate:{2:N2}", curr, CommonFunction.Eval_Curr(curr, original), rate);
        }

        public void CashierPolicy(List<PVFormDetail> d,
            bool leaveZero = false,
            int docNo = 0, int docYear = 0,
            List<ExchangeRate> exchangeRates = null,
            int MaxSeq = 0,
            int exRateDocType = 2,
            int PVTypeCode = 1)
        {
            if (d == null || d.Count < 1) // never work on empty list
                return;

            List<ExchangeRate> rates = null;
            if (exchangeRates == null)
                rates = logic.Look.GetCashierPolicyExchangeRates(exRateDocType);
            else
                rates = exchangeRates;

            Dictionary<string, decimal> amo = FormTableHelper.AmountByCurrency(d);
            Dictionary<string, decimal> capo = new Dictionary<string, decimal>();
            CashierPolicyList capol = new CashierPolicyList();
            string[] RoundCurrency = logic.Sys.RoundCurrencies();
            foreach (string k in amo.Keys)
            {
                ExchangeRate rate = (from e in rates select e)
                        .Where(x => x.CurrencyCode == k)
                        .FirstOrDefault();
                if (rate != null && rate.AmountMin != null && rate.AmountMin > 0)
                {
                    decimal amt = amo[k];
                    decimal dn = Math.Floor(amt / rate.AmountMin ?? 1);
                    decimal dr = amt - dn * (rate.AmountMin ?? 1);

                    capol.Add(k, amt - dr, 1);
                    capol.Add(k, dr, rate.Rate);
                }
                else
                {
                    capol.Add(k, amo[k], 1);
                }
            }

            var qCp = from x in capol.Data
                      select new
                      {
                          Currency = (x.Rate != 1) ? "IDR" : x.Curr,
                          Amount = (x.Rate == 1) ? x.Amount : (x.Amount * x.Rate)
                      };
            if (qCp.Any())
            {
                var qCpSum = qCp.GroupBy(a => a.Currency)
                    .Select(b => new
                    {
                        CURRENCY_CD = b.Key,
                        AMOUNT = b.Sum(x => x.Amount)
                    });
                if (qCpSum.Any())
                {
                    foreach (var cps in qCpSum)
                    {
                        capo[cps.CURRENCY_CD] = cps.AMOUNT;
                    }
                }
            }
            /// do value comparison from summarized 
            /// apply policy
            List<CashierPolicyData> Rp = (from g in capol.Data where g.Rate != 1 select g).ToList();
            List<PVFormDetail> neuD = new List<PVFormDetail>();
            int maxSeq = (from dx in d select dx.SequenceNumber).Max();
            if (MaxSeq > 0)
                maxSeq = MaxSeq;

            List<int> delSeq = new List<int>();

            foreach (CashierPolicyData rp in Rp)
            {
                foreach (PVFormDetail de in
                                      d.Where(x => x.CurrencyCode == rp.Curr)
                                        .OrderByDescending(xx => xx.Amount))
                {
                    if (rp.Amount <= 0) break;
                    decimal diff = 0;
                    int fseq = de.SequenceNumber;
                    //if (skipList != null && skipList.Contains(fseq))
                    //    continue;
                    decimal originalAmount = rp.Amount;

                    if (de.Amount > rp.Amount)
                    {
                        diff = de.Amount - rp.Amount;
                        de.Amount = diff;

                        rp.Policy = rp.Policy + rp.Amount;
                        rp.Amount = 0;
                    }
                    else if (de.Amount == rp.Amount)
                    {
                        diff = de.Amount;
                        de.Amount = 0;
                        rp.Policy = rp.Policy + diff;
                        rp.Amount = 0;

                        delSeq.Add(de.SequenceNumber);
                    }
                    else
                    {
                        diff = de.Amount;
                        de.Amount = 0;

                        delSeq.Add(de.SequenceNumber);
                        rp.Policy = rp.Policy + diff;
                        rp.Amount = rp.Amount - diff;
                    }

                    if (PVTypeCode != 3)
                        rp.Total = Math.Round(rp.Policy * rp.Rate); // 20140102 wot.daniel + /// rounding to prevent error saving 
                    else
                        rp.Total = rp.Policy * rp.Rate;  /// 20140117 wot.daniel + do not round for Settlement - prevent posting error 

                    ++maxSeq;
                    de.FromSeq = fseq;
                    de.Persisted = false;
                    neuD.Add(new PVFormDetail()
                    {
                        SequenceNumber = maxSeq,
                        FromSeq = fseq,
                        CurrencyCode = "IDR",
                        FromCurr = rp.Curr,
                        Amount = rp.Total,
                        Description = DescCashierPolicy(rp.Curr, originalAmount, rp.Rate),
                        CostCenterCode = de.CostCenterCode,
                        CostCenterName = de.CostCenterName,
                        GlAccount = de.GlAccount,
                        ItemTransaction = de.ItemTransaction,
                        InvoiceNumber = de.InvoiceNumber,
                        StandardDescriptionWording = de.StandardDescriptionWording,
                        TaxCode = de.TaxCode,
                        TaxNumber = de.TaxNumber,
                        Persisted = false
                    });
                }
            }
            delSeq = delSeq.Distinct().ToList();
            d.AddRange(neuD);
            if (!leaveZero)
                for (int i = d.Count - 1; i >= 0; i--)
                {
                    if (delSeq.Contains(d[i].SequenceNumber))
                    {
                        d.RemoveAt(i);
                    }
                }
        }

        public void ReSequence(List<PVFormDetail> d, List<PVFormDetail> suspenses, List<PVFormDetail> spent = null)
        {
            int lastSu = suspenses.Select(a => a.SequenceNumber).Max();
            int lastSe = 0;
            if (spent != null)
            {
                lastSe = spent.Select(b => b.SequenceNumber).Max();
            }
            int seqMax = Math.Max(lastSu, lastSe);

            for (int i = 0; i < d.Count; i++)
            {
                PVFormDetail se = d[i];
                if (se.FromSeq != null && se.SequenceNumber != se.FromSeq)
                {
                    PVFormDetail su = (from x in suspenses
                                       where x.FromSeq != null
                                       && x.FromSeq == se.FromSeq
                                       && x.SequenceNumber != x.FromSeq
                                       select x).FirstOrDefault();

                    if (su != null)
                    {
                        if (su.SequenceNumber != se.SequenceNumber) /// skip already correct 
                        {

                            /// find similar sequence number on d 
                            /// when found assign other sequence number on sequence when it is not found on s 

                            PVFormDetail lastD = (from y in d where y.SequenceNumber == su.SequenceNumber select y).FirstOrDefault();
                            if (lastD != null)
                            {
                                seqMax++;
                                lastD.SequenceNumber = seqMax;
                            }
                            if (!se.Equals(lastD))
                            {
                                se.SequenceNumber = su.SequenceNumber;
                            }
                        }
                    }
                    else if (spent != null)
                    {
                        PVFormDetail stl = (from u in spent
                                            where u.FromSeq != null
                                            && u.FromSeq == se.FromSeq
                                            && u.SequenceNumber != u.FromSeq
                                            select u).FirstOrDefault();
                        if (stl != null)
                        {
                            if (stl.SequenceNumber != se.SequenceNumber) // skip already correct seq no
                            {
                                /// find similar sequence number on d 
                                /// when found assign other sequence number on sequence when it is not found on s 

                                PVFormDetail lu = (from z in d where z.SequenceNumber == stl.SequenceNumber select z).FirstOrDefault();
                                if (lu != null)
                                {
                                    seqMax++;
                                  
                                    lu.SequenceNumber = seqMax; /// move non available to new ly assigned number 
                                }
                                if (!se.Equals(lu))
                                {
                                    se.SequenceNumber = stl.SequenceNumber;
                                }
                            }
                        }
                        else
                        {
                            /// just put it into new seq num 
                            seqMax++;
                            se.SequenceNumber = seqMax;
                        }
                    }
                }
            }
        }

        public bool FillNonCashierPolicySettlement(List<PVFormDetail> setl, List<PVFormDetail> sus)
        {
            if (setl == null || setl.Count < 1 || sus == null || sus.Count < 1)
                return false;

            /// find settlement that has original currency but has no idr cashier policy amount 
            List<int> toAdd = new List<int>();
            List<int> SusSeq = new List<int>();

            for (int i = 0; i < setl.Count; i++)
            {
                PVFormDetail a = setl[i];
                if (!a.FromCurr.isEmpty() || a.CurrencyCode == "IDR")
                    continue;
                bool hasCashierPolicy = false;
                for (int j = i + 1; j < setl.Count; j++)
                {
                    if (setl[j].FromCurr == a.CurrencyCode && setl[j].FromSeq == a.SequenceNumber)
                    {
                        hasCashierPolicy = true;
                        break;
                    }
                }
                if (!hasCashierPolicy)
                {
                    /// find if suspense has Cashier policy, if it does, add 
                    var SuspenseSeq = sus.Where(s => s.FromCurr == a.CurrencyCode && s.FromSeq == a.SequenceNumber);
                    if (SuspenseSeq.Any())
                    {
                        toAdd.Add(i);
                        SusSeq.Add(SuspenseSeq.FirstOrDefault().SequenceNumber);
                    }
                }
            }
            /// fill from suspense 
            if (toAdd.Count > 0)
            {
                for (int x = 0; x < toAdd.Count; x++)
                {
                    PVFormDetail ax = setl[toAdd[x]];
                    PVFormDetail fd = new PVFormDetail();
                    fd.Copy(ax);

                    fd.CurrencyCode = "IDR";
                    fd.Amount = 0;
                    fd.FromCurr = ax.CurrencyCode;
                    fd.FromSeq = ax.SequenceNumber;
                    ax.FromSeq = ax.SequenceNumber;
                    ax.Persisted = false;
                    fd.SequenceNumber = SusSeq[x];
                    fd.Persisted = false;
                    setl.Add(fd);
                }
            }
            return true;

        }

        protected int GetSettlementMaxSeqNo(ELVIS_DBEntities db, int suspenseNo, int suspenseYear)
        {
            var q = from a in db.TB_R_SET_FORM_D
                    where a.PV_SUS_NO == suspenseNo && a.PV_YEAR == suspenseYear
                    select a.SEQ_NO;
            int r = 0;
            if (q.Any())
            {
                r = q.Max();
            }
            return r;
        }

        protected int ReplaceSettlement(ELVIS_DBEntities db,
                int? suspenseNo, int? suspenseYear,
                int docNo, int docYear,
                List<PVFormDetail> leSettle,
                string userName)
        {
            DateTime today = DateTime.Now;
            int maxSeq = 0;

            foreach (var d in leSettle)
            {

                if (d.SequenceNumber > maxSeq)
                    maxSeq = d.SequenceNumber;

                var q = from sde in db.TB_R_SET_FORM_D
                        where sde.PV_SUS_NO == suspenseNo
                          && sde.PV_YEAR == suspenseYear
                          && sde.SEQ_NO == d.SequenceNumber
                        select sde;
                if (q.Any())
                {
                    var x = q.FirstOrDefault();
                    if (x != null)
                    {
                        x.FROM_SEQ = d.FromSeq;
                        x.SPENT_AMOUNT = d.Amount;
                        x.SETTLEMENT_NO = docNo;
                        x.SETTLEMENT_YEAR = docYear;
                        x.CHANGED_BY = userName;
                        x.CHANGED_DT = today;
                    }
                }
                else
                {
                    db.AddToTB_R_SET_FORM_D(new TB_R_SET_FORM_D()
                    {
                        PV_SUS_NO = suspenseNo ?? 0,
                        PV_YEAR = suspenseYear ?? 0,
                        SEQ_NO = d.SequenceNumber,
                        FROM_SEQ = d.FromSeq,
                        SETTLEMENT_YEAR = docYear,
                        SETTLEMENT_NO = docNo,
                        SPENT_AMOUNT = d.Amount,
                        CREATED_DT = today,
                        CREATED_BY = userName
                    });
                }
            }
            db.SaveChanges();
            return maxSeq;
        }

        #region simulate

        protected readonly string notBalance = "Total Amount Header & Detail is not balance";
        protected readonly string noWitholding = "This transaction should have witholding tax";
        protected readonly string notVerified = "Hardcopy Document has not been Verified by Counter";


        public virtual List<PVFormSimulationData> GetSimulationData(String docNo, String docYear)
        {
            return null;
        }
        public virtual List<AccrFormSimulationData> GetSimulationData(String refNo)
        {
            return null;
        }

        #endregion

        public virtual void updateBalanceOutstanding(PVFormData formData, UserData userData, bool isSubmit)
        {
            return;
        }

        public virtual void updateBalanceVoucherPosting(PVFormData formData, UserData userData)
        {
            return;
        }

        public int GetLastStatusDocument(int no, int year, int DocCd)
        {
            return Qx<int>("GetLastStatusByDocNo", new { docCd = DocCd, no = no, yy = year }).FirstOrDefault();
        }

        public void SetCancelFlag(string accrNo, UserData userData, int code)
        {
            Qx<int>("SetCancelFlagAccr", new { code= code, UserName = userData.USERNAME, accrNo = accrNo}).First();          
        }
          
     	public List<OutstandingSuspense> GetUnsettledByDivision(string DivisionId)
        {
            return Qu<OutstandingSuspense>("GetUnsettledByDivision", DivisionId).ToList();
        }
        
    }
}