﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.IO;
using System.Linq;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Function;
using DataLayer.Model;
using DataLayer;


namespace BusinessLogic.VoucherForm
{
    public class RVFormLogic : FormPersistence
    {
        public int getDocumentNo(string uid = "")
        {
            return ((int)GetDocNo(2, uid));
        }

        public override List<CodeConstant> getDocTypes()
        {
            return CommonData.RVTypes;
        }

        public override string getDocType(int? code)
        {
            string coda = (code ?? 0).str();
            return CommonData.rvTypeCodes
                .Where(a => a.Code == coda)
                .Select(b => b.Description)
                .FirstOrDefault();
        }

        protected override List<PVFormDetail> GetDetail(PVFormData f, ELVIS_DBEntities DB)
        {
            int _no, _yy;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? DateTime.Now.Year;
            List<PVFormDetail> l = new List<PVFormDetail>();

            var detailQuery = from t in DB.TB_R_RV_D
                              where (t.RV_YEAR == _yy) && (t.RV_NO == _no)
                              select t;
            var detailResult = detailQuery.ToList();
            if (!detailResult.Any())
            {
                _yy--;
                detailQuery = from t in DB.TB_R_RV_D
                              where (t.RV_YEAR == _yy) && (t.RV_NO == _no)
                              select t;
                detailResult = detailQuery.ToList();
                if (!detailResult.Any())
                {
                    _yy += 2;
                    detailQuery = from t in DB.TB_R_RV_D
                                  where (t.RV_YEAR == _yy) && (t.RV_NO == _no)
                                  select t;
                    detailResult = detailQuery.ToList();
                }
            }
            if (!detailResult.Any()) return l;
            PVFormTable formTable = f.FormTable;
            List<CodeConstant> lstCostCenterCodes = getCostCenterCodes();
            string standardDescriptionWording = null;
            PVFormDetail formDetail;
            int seqNumber = 0;

            string vendorName = getVendorCodeName(f.VendorCode);
            string divisionName = getDivisionName(f.DivisionID);
            foreach (var detail in detailResult)
            {
                if (standardDescriptionWording == null)
                {
                    standardDescriptionWording = getDescriptionDefaultWording(f.TransactionCode);
                }

                seqNumber++;
                formDetail = new PVFormDetail()
                {
                    Persisted = true,
                    SequenceNumber = detail.SEQ_NO,
                    DisplaySequenceNumber = seqNumber,
                    Amount = detail.AMOUNT,
                    CostCenterCode = detail.COST_CENTER_CD,
                    CurrencyCode = detail.CURRENCY_CD,
                    Description = detail.DESCRIPTION,
                    StandardDescriptionWording = standardDescriptionWording,
                    WbsNumber = detail.WBS_NO,
                    GlAccount = detail.GL_ACCOUNT.str(),
                    InvoiceNumber = detail.INVOICE_NO,
                    FromCurr = detail.FROM_CURR,
                    FromSeq = detail.FROM_SEQ,
                    ItemNo = detail.ITEM_NO,
                    TaxCode = detail.TAX_CD,
                    TaxNumber = detail.TAX_NO,
                    ItemTransaction = detail.ITEM_TRANSACTION_CD
                };

                formDetail.InvoiceNumberUrl = string.Format(
                    "~/50Invoice/InvoiceListDet.aspx?invoiceNo={0}"
                    + "&invoiceDate={1}&vendorCode={2}&vendorName={3}"
                    + "&division={4}&invoiceStatus={5}",
                        formDetail.InvoiceNumber,
                        string.Format("{0:dd/MM/yyy}", f.ActivityDate),
                        f.VendorCode, vendorName,
                        divisionName, f.StatusCode);
                formDetail.CostCenterName = (from a in lstCostCenterCodes
                                             where a.Code == formDetail.CostCenterCode
                                             select a.Description).FirstOrDefault();
                l.Add(formDetail);
            }

            return l;
        }

        public override PVFormData search(int rvNumber, int rvYear)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var query = from t in db.TB_R_RV_H
                            where (t.RV_YEAR == rvYear) && (t.RV_NO == rvNumber)
                            select t;
                var result = query.ToList();
                if (!result.Any())
                    return null;

                PVFormData data = new PVFormData(2);
                var d = result[0];
                data.PVNumber = d.RV_NO;
                data.PVYear = d.RV_YEAR;
                data.ActivityDate = d.ACTIVITY_DATE_FROM;
                data.ActivityDateTo = d.ACTIVITY_DATE_TO;
                data.BudgetNumber = d.BUDGET_NO;
                // FID.Ridwan 10072018
                data.BookingNo = d.BUDGET_NO;
                data.DivisionID = d.DIVISION_ID;
                data.PaymentMethodCode = d.PAY_METHOD_CD;
                data.PVDate = d.RV_DATE;
                data.PVTypeCode = d.RV_TYPE_CD;
                data.StatusCode = d.STATUS_CD;
                data.SuspenseNumber = d.SUSPENSE_NO;
                data.SuspenseYear = d.SUSPENSE_YEAR;
                data.TransactionCode = d.TRANSACTION_CD;
                data.VendorCode = d.VENDOR_CD;
                data.VendorGroupCode = d.VENDOR_GROUP_CD;
                data.HoldingUser = d.HOLD_BY;
                data.OnHold = (d.HOLD_FLAG.HasValue
                        && d.HOLD_FLAG > 0
                        );
                data.COUNTER_FLAG = d.COUNTER_FLAG;
                data.PostingDate = d.POSTING_DATE;
                data.TaxCalculated = data.TaxCode != null;
                data.Canceled = (d.CANCEL_FLAG ?? 0) == 1;
                data.FormTable.DataList = GetDetail(data, db);

                data.UserName = UserName;

                return data;
            }

        }

        protected int saveHistoryHeader(PVFormData f, UserData u, ELVIS_DBEntities DB)
        {
            var historyVersions =
                DB.TB_H_RV_H
                  .Where(
                     rv => rv.RV_NO == f.PVNumber
                        && rv.RV_YEAR == f.PVYear)
                  .Select(t => t.VERSION)
                  .ToList();

            int lastVersion = 1;

            if (historyVersions != null && historyVersions.Count > 0)
                lastVersion = historyVersions.Max() + 1;

            DateTime today = DateTime.Now;

            TB_H_RV_H hHistory = new TB_H_RV_H()
            {
                POSTING_DATE = f.PostingDate,
                STATUS_CD = f.StatusCode.Value,
                RV_NO = f.PVNumber.Value,
                RV_YEAR = f.PVYear.Value,
                PAY_METHOD_CD = f.PaymentMethodCode,
                VENDOR_GROUP_CD = f.VendorGroupCode,
                VENDOR_CD = f.VendorCode,
                RV_TYPE_CD = f.PVTypeCode,
                TRANSACTION_CD = f.TransactionCode,
                BUDGET_NO = f.BudgetNumber,
                ACTIVITY_DATE_FROM = f.ActivityDate,
                ACTIVITY_DATE_TO = f.ActivityDateTo,
                DIVISION_ID = f.DivisionID,
                RV_DATE = f.PVDate.Value,
                CREATED_DATE = today,
                CREATED_BY = u.USERNAME,
                VERSION = lastVersion
            };
            DB.TB_H_RV_H.AddObject(hHistory);
            return lastVersion;
        }

        public void saveHistoryDetail(PVFormData f, UserData u, int lastVersion, ELVIS_DBEntities DB)
        {
            DateTime today = DateTime.Now;
            foreach (PVFormDetail detail in f.Details)
            {
                DB.TB_H_RV_D.AddObject(new TB_H_RV_D()
                {
                    SEQ_NO = detail.SequenceNumber,
                    VERSION = lastVersion,
                    RV_NO = f.PVNumber.Value,
                    RV_YEAR = f.PVYear.Value,
                    AMOUNT = detail.Amount,
                    COST_CENTER_CD = detail.CostCenterCode,
                    CURRENCY_CD = detail.CurrencyCode,
                    TAX_CD = detail.TaxCode,
                    TAX_NO = detail.TaxNumber,
                    ITEM_TRANSACTION_CD = detail.ItemTransaction,
                    DESCRIPTION = detail.Description,
                    CREATED_BY = u.USERNAME,
                    CREATED_DT = today
                });
            }
        }

        public bool saveHeader(PVFormData formData, UserData userData, bool financeHeader, out int version, ELVIS_DBEntities DB)
        {
            bool Ok = false;
            int _no = formData.PVNumber ?? 0;
            int _yy = formData.PVYear ?? 0;
            version = 1;

            TB_R_RV_H h = null;
            try
            {
                bool added = false;
                List<TB_R_RV_H> existingHeaders = null;
                existingHeaders = (from t in DB.TB_R_RV_H
                                   where (t.RV_NO == _no) && (t.RV_YEAR == _yy)
                                   select t).ToList();
                if (existingHeaders.Any())
                {
                    h = existingHeaders[0];
                }
                else
                {
                    added = true;
                    h = new TB_R_RV_H();
                    _no = (int)GetDocNo(2, userData.USERNAME);
                    formData.PVNumber = _no;
                    formData.PVYear = DateTime.Now.Year;
                    DB.TB_R_RV_H.AddObject(h);
                }

                if (formData.isSubmit)
                {
                    formData.StatusCode = 10;
                }

                if (financeHeader)
                {
                    // formData.StatusCode = 21;
                    h.PAY_METHOD_CD = formData.PaymentMethodCode;
                    h.POSTING_DATE = formData.PostingDate;
                    h.CHANGED_BY = userData.USERNAME;
                    h.CHANGED_DATE = today;
                }
                else
                {
                    h.RV_NO = formData.PVNumber.Value;
                    h.RV_YEAR = formData.PVYear.Value;
                    h.PAY_METHOD_CD = formData.PaymentMethodCode;
                    h.VENDOR_GROUP_CD = formData.VendorGroupCode;
                    h.VENDOR_CD = formData.VendorCode;
                    h.RV_TYPE_CD = formData.PVTypeCode;
                    h.TRANSACTION_CD = formData.TransactionCode;
                    h.BUDGET_NO = formData.BudgetNumber;
                    h.ACTIVITY_DATE_FROM = formData.ActivityDate;
                    h.ACTIVITY_DATE_TO = formData.ActivityDateTo;
                    h.DIVISION_ID = formData.DivisionID;
                    h.RV_DATE = formData.PVDate.Value;
                    h.REFFERENCE_NO = formData.ReferenceNo;
                    if (added)
                    {
                        h.CREATED_DATE = today;
                        h.CREATED_BY = userData.USERNAME;
                    }
                    else
                    {
                        h.CHANGED_DATE = today;
                        h.CHANGED_BY = userData.USERNAME;
                    }
                    h.TOTAL_AMOUNT = formData.GetTotalAmount();

                    if (formData.SuspenseNumber != null)
                    {
                        h.SUSPENSE_NO = formData.SuspenseNumber;
                        h.SUSPENSE_YEAR = formData.SuspenseYear;
                    }
                }
                h.STATUS_CD = formData.StatusCode.Value;

                if (formData.isSubmit)
                {
                    h.CANCEL_FLAG = 0;
                    version = saveHistoryHeader(formData, userData, DB);
                    //LoggingLogic.say(logi, "saveHistoryHeader {0}", version);
                }
                DB.SaveChanges();
                Ok = true;
            }
            catch (Exception ex)
            {
                Ok = false;
                LoggingLogic.err(ex);
                throw;
            }
            return Ok;
        }

        public void delExistingDetail(PVFormData formData, UserData userData, ELVIS_DBEntities DB)
        {
            int _no = formData.PVNumber ?? 0;
            int _yy = formData.PVYear ?? 0;

            List<TB_R_RV_D> existingDetails =
                    (from t in DB.TB_R_RV_D
                     where (t.RV_NO == _no) && (t.RV_YEAR == _yy)
                     select t).ToList();
            List<string> invoices = new List<string>();
            if (!existingDetails.Any()) return;

            /* Update existing records */
            bool matched = false;
            List<PVFormDetail> deletedRows = new List<PVFormDetail>();
            foreach (TB_R_RV_D d in existingDetails)
            {
                matched = false;
                foreach (PVFormDetail detail in formData.Details)
                {
                    if (detail.SequenceNumber == d.SEQ_NO)
                    {
                        d.AMOUNT = detail.Amount;

                        d.COST_CENTER_CD = detail.CostCenterCode;
                        d.CURRENCY_CD = detail.CurrencyCode;
                        d.TAX_CD = detail.TaxCode;
                        d.TAX_NO = detail.TaxNumber;
                        d.ITEM_TRANSACTION_CD = detail.ItemTransaction;
                        d.WBS_NO = detail.WbsNumber;
                        d.GL_ACCOUNT = GLAccount(formData.TransactionCode ?? 0, d.ITEM_TRANSACTION_CD ?? 0);
                        d.DESCRIPTION = detail.Description;
                        d.FROM_SEQ = detail.FromSeq;
                        d.FROM_CURR = detail.FromCurr;

                        d.CHANGED_DT = today;
                        d.CHANGED_BY = userData.USERNAME;

                        detail.Persisted = true;
                        matched = true;
                        break;
                    }
                }

                /* Delete record if none matched in the memory */
                if (!matched)
                {
                    deletedRows.Add(new PVFormDetail()
                    {
                        SequenceNumber = d.SEQ_NO,
                        FromSeq = d.FROM_SEQ,
                        CurrencyCode = d.CURRENCY_CD,
                        FromCurr = d.FROM_CURR,
                        Amount = d.AMOUNT,
                        Description = d.DESCRIPTION,
                        ItemTransaction = d.ITEM_TRANSACTION_CD,
                        ItemNo = d.ITEM_NO,
                        CostCenterCode = d.COST_CENTER_CD,
                        GlAccount = d.GL_ACCOUNT.str(),
                        TaxCode = d.TAX_CD,
                        TaxNumber = d.TAX_NO,
                        WbsNumber = d.WBS_NO
                    });


                    DB.TB_R_RV_D.DeleteObject(d);
                }
                else
                {
                    if (!string.IsNullOrEmpty(d.INVOICE_NO))
                        invoices.Add(d.INVOICE_NO + "|" + formData.VendorCode);

                }
            }
            invoices = invoices.Distinct().ToList();
            foreach (string inv in invoices)
            {
                string[] ainv = inv.Split('|');
                if (ainv.Length > 1)
                {
                    string ino = ainv[0];
                    string iv = ainv[1];
                    var qi = (from ih in DB.TB_R_INVOICE_H
                              where ih.INVOICE_NO == ino
                              && ih.VENDOR_CD == iv
                              select ih);
                    int invoiceUpdated = 0;
                    foreach (var updatedInvoice in qi)
                    {
                        updatedInvoice.TRANSACTION_CD = formData.TransactionCode;
                        ++invoiceUpdated;
                    }
                    if (invoiceUpdated > 0)
                        DB.SaveChanges();
                }
            }
            // LogDetails(logi, deletedRows);

        }

        private void saveDetail(PVFormData formData, UserData userData, int version, ELVIS_DBEntities DB)
        {
            //LoggingLogic.say(logi, "saveDetail");
            //LogDetails(logi, formData.Details);

            TB_R_RV_D x;
            foreach (PVFormDetail d in formData.Details)
            {
                if (!d.Persisted)
                {
                    //maxSequenceNumber++;
                    x = DB.TB_R_RV_D
                        .Where(t => t.RV_NO == formData.PVNumber.Value
                            && t.RV_YEAR == formData.PVYear.Value
                            && t.SEQ_NO == d.SequenceNumber)
                        .FirstOrDefault();

                    if (x == null)
                    {
                        x = new TB_R_RV_D()
                        {
                            SEQ_NO = d.SequenceNumber,
                            RV_NO = formData.PVNumber.Value,
                            RV_YEAR = formData.PVYear.Value,
                            AMOUNT = d.Amount,
                            COST_CENTER_CD = d.CostCenterCode,
                            CURRENCY_CD = d.CurrencyCode,
                            TAX_CD = d.TaxCode,
                            TAX_NO = d.TaxNumber,
                            ITEM_TRANSACTION_CD = d.ItemTransaction,
                            GL_ACCOUNT = GLAccount(formData.TransactionCode ?? 0, d.ItemTransaction ?? 0),
                            WBS_NO = d.WbsNumber,
                            DESCRIPTION = d.Description,
                            FROM_CURR = d.FromCurr,
                            FROM_SEQ = d.FromSeq,
                            ITEM_NO = d.ItemNo,
                            CREATED_BY = userData.USERNAME,
                            CREATED_DT = today
                        };
                        DB.TB_R_RV_D.AddObject(x);
                    }
                    else
                    {
                        x.AMOUNT = d.Amount;
                        x.COST_CENTER_CD = d.CostCenterCode;
                        x.CURRENCY_CD = d.CurrencyCode;
                        x.TAX_CD = d.TaxCode;
                        x.TAX_NO = d.TaxNumber;
                        x.ITEM_TRANSACTION_CD = d.ItemTransaction;
                        x.GL_ACCOUNT = GLAccount(formData.TransactionCode ?? 0, d.ItemTransaction ?? 0);
                        x.WBS_NO = d.WbsNumber;
                        x.DESCRIPTION = d.Description;
                        x.FROM_CURR = d.FromCurr;
                        x.FROM_SEQ = d.FromSeq;
                        x.ITEM_NO = d.ItemNo;
                        x.CHANGED_BY = userData.USERNAME;
                        x.CHANGED_DT = today;
                    }
                    DB.SaveChanges();
                    d.Persisted = true;
                }
            }
            /* Detail History */
            if (formData.isSubmit)
            {
                saveHistoryDetail(formData, userData, version, DB);
                //LoggingLogic.say(logi, "saveHistoryDetail {0}", version);
            }

        }

        private void saveAmounts(PVFormData formData, UserData userData, ELVIS_DBEntities DB)
        {
            int _no, _yy;
            _no = formData.PVNumber ?? 0;
            _yy = formData.PVYear ?? 0;

            List<TB_R_PVRV_AMOUNT> existingAmounts =
                               (from t in DB.TB_R_PVRV_AMOUNT
                                where (t.DOC_NO == _no) && (t.DOC_YEAR == _yy)
                                select t).ToList();
            OrderedDictionary mapTotalAmount = formData.FormTable.TotalAmountMap();
            if (existingAmounts.Any())
            {
                object objAmount;
                foreach (TB_R_PVRV_AMOUNT d in existingAmounts)
                {
                    objAmount = mapTotalAmount[d.CURRENCY_CD];
                    if (objAmount == null)
                    {
                        DB.TB_R_PVRV_AMOUNT.DeleteObject(d);
                    }
                    else
                    {
                        d.TOTAL_AMOUNT = Convert.ToDecimal(objAmount);
                        d.CHANGED_BY = userData.USERNAME;
                        d.CHANGED_DATE = today;
                    }
                }
                DB.SaveChanges();
            }

            List<ExchangeRate> exchangeRates = getExchangeRates();
            existingAmounts = (from t in DB.TB_R_PVRV_AMOUNT
                               where (t.DOC_NO == _no) && (t.DOC_YEAR == _yy)
                               select t).ToList();
            bool processed;
            TB_R_PVRV_AMOUNT tblAmount;
            ICollection lstKey = mapTotalAmount.Keys;
            foreach (string key in lstKey)
            {
                processed = false;
                foreach (TB_R_PVRV_AMOUNT amount in existingAmounts)
                {
                    if (key.Equals(amount.CURRENCY_CD))
                    {
                        processed = true;
                        break;
                    }
                }

                if (!processed)
                {
                    decimal xr = (from er in exchangeRates
                                  where er.CurrencyCode.Equals(key)
                                  select er.Rate).FirstOrDefault();

                    decimal exchangeRate = xr;

                    tblAmount = new TB_R_PVRV_AMOUNT()
                    {
                        CREATED_BY = userData.USERNAME,
                        CREATED_DATE = today,
                        CURRENCY_CD = key,
                        EXCHANGE_RATE = exchangeRate,
                        DOC_NO = _no,
                        DOC_YEAR = _yy,
                        TOTAL_AMOUNT = Convert.ToDecimal(mapTotalAmount[key])
                    };

                    DB.TB_R_PVRV_AMOUNT.AddObject(tblAmount);
                }
            }
        }

        private int saveSettlementDetail(PVFormData f, UserData u, ELVIS_DBEntities DB)
        {
            int _no, _yy, maxseq;
            maxseq = 0;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? 0;

            //LoggingLogic.say(logi, "RV.saveSettlementDetail {0}{1}\r\n", _no, _yy);
            bool doCashierPolicy = (f.ReferenceNo == null || f.ReferenceNo != 1);

            var ter = (from se in DB.TB_R_RV_H
                       where se.RV_NO == _no && se.RV_YEAR == _yy
                       select se).FirstOrDefault();
            int? susno = null;
            int? susye = null;
            if (ter != null)
            {
                susno = ter.SUSPENSE_NO;
                susye = ter.SUSPENSE_YEAR;
            }
            else
            {
                //LoggingLogic.say(logi, "RV not found");
            }

            PVFormData asu = new PVFormData()
            {
                PVNumber = susno,
                PVYear = susye
            };
            //LogDetails(logi, f.Details);

            //LoggingLogic.say(logi, "\r\n\t\tSuspense {0}{1}", asu.PVNumber, asu.PVYear);

            List<PVFormDetail> pvSuspense = GetPV(asu, DB);
            //LogDetails(logi, pvSuspense);
            int maxSu = 0;
            if (pvSuspense != null && pvSuspense.Count > 0)
            {
                maxSu = pvSuspense.Select(a => a.SequenceNumber).Max();
            }
            // take original suspense 

            List<OriginalAmountData> osu = null;
            if (susno != null && susye != null)
            {
                //LoggingLogic.say(logi, "GetOriginal({0}, {1})", susno, susye);
                osu = logic.Base.GetOriginal((int)susno, (int)susye);
            }

            List<OriginalAmountData> ose = new List<OriginalAmountData>();

            //LoggingLogic.say(logi, "\r\n\t\tOriginal Suspense");
            //LogOri(logi, osu);

            int maxSe = osu.Select(o => o.SEQ_NO).Max();
            maxseq = Math.Max(maxSu, maxSe);

            List<PVFormDetail> spending = new List<PVFormDetail>();
            List<PVFormDetail> fse = f.Details;

            var sus = (osu != null && osu.Any()) ? osu.OrderBy(x => x.SEQ_NO).AsQueryable() : null;
            if (sus != null)
            {
                foreach (var s in sus)
                {
                    var inRv = f.Details.Where(a => a.SequenceNumber == s.SEQ_NO);
                    if (inRv.Any())
                        spending.Add(new PVFormDetail()
                        {
                            SequenceNumber = s.SEQ_NO,
                            CurrencyCode = s.CURRENCY_CD,
                            Amount = s.SPENT_AMOUNT ?? 0,
                            FromCurr = null,
                            Persisted = false,
                            ExrateSuspense = s.EXCHANGE_RATE,
                            CostCenterCode = s.COST_CENTER_CD,
                            Description = s.DESCRIPTION,
                            FromSeq = null,
                            ItemNo = null,
                            InvoiceNumber = s.INVOICE_NO,
                            InvoiceDate = s.INVOICE_DATE
                        });
                }
            }

            //LoggingLogic.say(logi, "\r\n\t\tSpending");
            //LogDetails(logi, spending);

            List<ExchangeRate> oe = logic.Look.GetExchangeRatesFromOriginal(susno ?? 0, susye ?? 0);

            if (doCashierPolicy)
                CashierPolicy(spending, true, susno ?? 0, susye ?? 0, oe, maxseq, 1);

            //LoggingLogic.say(logi, "\r\n\t\tSpending Cashier policy");
            //LogDetails(logi, spending);

            if (doCashierPolicy)
            {
                List<PVFormDetail> spent = GetSettleDetail(DB, susno ?? 0, susye ?? 0);

                ReSequence(spending, pvSuspense, spent);
                //LoggingLogic.say(logi, "\r\n\t\tSpending Cashier policy Resequence");
                //LogDetails(logi, spending);
            }

            List<PVFormDetail> d = new List<PVFormDetail>();
            // generate settlement detail in form of List <PVFormDetail>
            // based on values found in suspense vs settlement detail
            var qsu = (pvSuspense != null && pvSuspense.Any()) ? pvSuspense
                    .OrderBy(a => a.FromSeq)
                    .ThenByDescending(a => a.SequenceNumber).ToList()
                    : null;

            //LoggingLogic.say(logi, "\r\n\t\tSpent");
            //LogDetails(logi, spending);

            maxseq = ReplaceSettlement(DB, susno, susye, _no, _yy, spending, u.USERNAME);

            //LoggingLogic.say(logi, "RV.saveSettlementDetail DONE", _no, _yy);
            return maxseq;
        }

        public override bool save(PVFormData formData, UserData userData, bool financeHeader)
        {
            bool Ok = true;

            base.save(formData, userData, financeHeader);
            today = DateTime.Now;
            /* Header */
            DbTransaction TX = null;
            using (ContextWrap co = new ContextWrap())
            {
                ELVIS_DBEntities db = co.db;

                try
                {
                    TX = db.Connection.BeginTransaction();

                    Ok = SaveData(db, formData, userData, financeHeader);

                    if (Ok)
                    {
                        TX.Commit();
                    }
                }
                catch (Exception ex)
                {
                    Ok = false;
                    if (TX!=null)
                    TX.Rollback();
                    LoggingLogic.err(ex);
                }
            }

            return Ok;
        }

        public bool SaveData(ELVIS_DBEntities DB,
            PVFormData formData, UserData userData,
            bool financeHeader)
        {
            int version = 1;
            //int pvNumber = formData.PVNumber.Value;
            //int pvYear = formData.PVYear.Value;
            bool headerSaved = saveHeader(formData, userData, financeHeader, out version, DB);
            logi = string.Format("{0}{1}", formData.PVNumber ?? 0, formData.PVYear ?? 0);
            Util.ReMoveFile(Path.Combine(LoggingLogic.GetPath(), logi + ".log"));

            if (!financeHeader && headerSaved)
            {
                if (formData.isSubmit)
                    saveOri(formData, userData, DB);
                bool doCashierPolicy = (formData.ReferenceNo == null || formData.ReferenceNo != 1);

                if (formData.PaymentMethodCode == "C" && formData.isSubmit && !financeHeader)
                {

                    int suNo = formData.SuspenseNumber ?? 0;
                    int suYe = formData.SuspenseYear ?? 0;

                    int maxSe = 0;
                    if (formData.PVTypeCode == 2)
                        maxSe = saveSettlementDetail(formData, userData, DB);

                    PVFormData sus = null;
                    if (formData.PVTypeCode == 2)
                        sus = logic.PVForm.search(suNo, suYe);

                    int maxSu = 0;
                    if (sus != null && sus.Details != null && sus.Details.Count > 0)
                        maxSu = sus.Details.Select(a => a.SequenceNumber).Max();

                    int maxSeq = Math.Max(maxSu, maxSe);

                    if (doCashierPolicy)
                        CashierPolicy(formData.Details, true, formData.PVNumber ?? 0, formData.PVYear ?? 0);

                    if (formData.PVTypeCode == 2)
                    {
                        if (sus != null && doCashierPolicy)
                        {
                            //LoggingLogic.say(logi, "BEFORE ReSequence");
                            //LogDetails(logi, formData.Details);

                            List<PVFormDetail> spent = GetSettleDetail(DB, suNo, suYe);

                            ReSequence(formData.Details, sus.Details, spent);

                            //LoggingLogic.say(logi, "AFTER ReSequence");
                            //LogDetails(logi, formData.Details);

                            // add settlement detail not found by cashier policy as 0 amont 
                            FillNonCashierPolicySettlement(formData.Details, sus.Details);
                            //LoggingLogic.say(logi, "Fill Non Cashier Policy Settlement");
                            //LogDetails(logi, formData.Details);
                        }
                    }
                }

                // List<PVFormDetail> lstDetail = formData.FormTable.sanityCheck();

                //LoggingLogic.say(logi, "Detail");
                //LogDetails(logi, formData.Details);
                /* Detail */
                delExistingDetail(formData, userData, DB);

                // bool syncBudgetNo = SyncBudgetNo(formData, lstDetail);

                saveDetail(formData, userData, version, DB);

                /* Amount */
                saveAmounts(formData, userData, DB);

                DB.SaveChanges();

            }
            return headerSaved;
        }

        private List<PVFormDetail> GetSettleDetail(ELVIS_DBEntities DB, int suNo, int suYe)
        {
            var q = from t in DB.TB_R_SET_FORM_D
                    where t.PV_SUS_NO == suNo && t.PV_YEAR == suYe
                    select t;
            List<PVFormDetail> r = null;
            if (q.Any())
            {
                r = q.Select(x => new PVFormDetail()
                {
                    SequenceNumber = x.SEQ_NO,
                    FromSeq = x.FROM_SEQ,
                    Amount = x.SPENT_AMOUNT
                }).ToList();
            }
            return r;
        }

        private bool SyncBudgetNo(PVFormData formData, List<PVFormDetail> lstDetail)
        {
            bool syncBudgetNo = (formData.BudgetNumber != null) && (!formData.BudgetNumber.Equals(""));
            List<string> lstCheckedWbsNo = new List<string>();

            foreach (PVFormDetail d in lstDetail)
            {
                if (!string.IsNullOrEmpty(d.WbsNumber))
                {
                    if (!lstCheckedWbsNo.Contains(d.WbsNumber))
                    {
                        syncBudgetNo = true;
                        break;
                    }
                }
            }
            return syncBudgetNo;
        }

        public override Dictionary<string, decimal> GetSuspenseAmounts(int pvNo, int pvYear)
        {
            int? suspenseNo = (from s in db.TB_R_RV_H
                               where s.RV_NO == pvNo && s.RV_YEAR == pvYear
                                  && s.RV_TYPE_CD == 2
                               select s.SUSPENSE_NO).FirstOrDefault();
            Dictionary<string, decimal> da = new Dictionary<string, decimal>();

            if (suspenseNo != null)
            {
                var suspenseQ = (from y in db.TB_R_RV_D
                                 where (y.RV_NO == suspenseNo) && (y.RV_YEAR == pvYear)
                                 select new { CURRENCY_CD = y.CURRENCY_CD, AMOUNT = y.AMOUNT });
                var amounts = suspenseQ
                             .GroupBy(c => c.CURRENCY_CD)
                             .Select(x => new
                             {
                                 CURRENCY_CD = x.Key,
                                 AMOUNT = x.Sum(X => (X.AMOUNT))
                             });
                foreach (var suma in amounts)
                {
                    da.Add(suma.CURRENCY_CD, suma.AMOUNT);
                }
            }
            return da;
        }


        public override string GetSummary(UserData u, string _imagePath, string template, int? num, int? year)
        {
            return base.GetSummary(u, _imagePath, template, num, year);
        }

        public override bool Reverse(string uid, PVFormData f)
        {
            return true;
        }

        public override List<PVFormSimulationData> GetSimulationData(String docNo, String docYear)
        {
            return null;
        }

        public static bool CanEdit(bool hasAccess, bool hasWorklist, int status,
                string role, bool onHold, int? vendorGroupCode,
                int pvType, bool hasAuthorizedCreatorRole)
        {
            bool e = false;

            if (!hasAccess)
            {
                e = false;
            }
            else if (((pvType == 2) && (status < 20 && (status != 0 && hasAuthorizedCreatorRole))))
            {
                e = false;
            }
            else if (
                   (new int[] { 0 }).Contains(status)
                  && hasAuthorizedCreatorRole
                    )
            {
                e = true;
            }
            else if ((new int[] { }).Contains(status))
            {
                e = false;
            }
            //commented by Vandy --> berdasarkan nilai Hold flag-nya, sekarang yg bisa edit cuma hold flag 1 aja, itu juga cuma edit attachment
            //else if (onHold)
            //{
            //    e = true;
            //}
            else if (hasWorklist)
            {
                e = true;
            }
            else
            {
                e = false;
                // err = "?";
            }
            return e;
        }

        #region Accrued
        public override void updateBalanceOutstanding(PVFormData pv, UserData user, bool isSubmit)
        {
            int mult = isSubmit ? 1 : -1;

            var q = Qx<string>("UpdateBalanceOutstandingRV", new { RvNo = pv.PVNumber, RvYear = pv.PVYear, Param = mult, UserName = user.USERNAME });
        }
        #endregion

    }
}
