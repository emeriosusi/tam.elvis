﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Web.ASPxGridView;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class AttachmentTable
    {
        public AttachmentTable()
        {
            attachments = new List<FormAttachment>();
        }

        public bool Editing { set; get; }

        private List<FormAttachment> attachments;
        public List<FormAttachment> Attachments
        {
            get
            {
                return attachments;
            }
        }

        public FormAttachment getBlankAttachment()
        {            
            foreach (FormAttachment att in attachments)
            {
                if (att.Blank)
                {
                    return att;
                }
            }

            FormAttachment attachment = new FormAttachment();
            attachments.Add(attachment);
            return attachment;
        }

        public FormAttachment get(int sequenceNumber)
        {
            foreach (FormAttachment at in attachments)
            {
                if (at.SequenceNumber == sequenceNumber)
                {
                    return at;
                }
            }

            return null;
        }

        public void closeEditing()
        {
            int cntAttachment = attachments.Count;
            FormAttachment attachment;
            for (int i = 0; i < cntAttachment; i++)
            {
                attachment = attachments[i];
                if (attachment.Blank)
                {
                    attachments.RemoveAt(i);
                    break;
                }
            }

            Editing = false;
        }

        public void openEditing()
        {
            addBlankRow();
            Editing = true;
        }

        public void addBlankRow()
        {
            if (attachments.Count > 0 && attachments[attachments.Count - 1].Blank) return;

            attachments.Add(new FormAttachment() {            
                Blank = true,
                SequenceNumber = attachments.Count + 1
            });
        }

        public void reset()
        {
            attachments.Clear();
        }
    }
}
