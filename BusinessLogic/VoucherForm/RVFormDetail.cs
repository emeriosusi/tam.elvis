﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    public class RVFormDetail
    {
        public bool Persisted {set; get; }
        public int SequenceNumber { set; get; }
        public string CostCenterCode { set; get; }
        public string CostCenterName { set; get; }
        public string Description { set; get; }
        public string DescriptionDefaultWording { set; get; }
        public string CurrencyCode { set; get; }
        public double Amount { set; get; }
        public string InvoiceNumber { set; get; }
    }
}
