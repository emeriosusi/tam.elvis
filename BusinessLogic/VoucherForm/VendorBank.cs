﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    public class VendorBank
    {
        public string VendorCode { get; set; }
        public string Country { get; set; }
        public string Key { get; set; }
        public string Account { get; set; } 
        public string Type { get; set; } 
        public string AccountHolder { get; set; } 
        public string Name { get; set; }
        public string Currency { get; set; }
    }
}
