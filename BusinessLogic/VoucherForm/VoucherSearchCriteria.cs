﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using Common.Function;

namespace BusinessLogic.VoucherForm
{
    public class VoucherSearchCriteria
    {
        public bool
            ignoreDate, ignoreNum, ignoreType, ignoreVendorCode
            , ignoreIssuingDivision, ignoreLastStatus, ignoreTransactionType
            , ignorePaymentMethod, ignoreVendorGroup, ignoreActivityDate
            , ignoreSubmitDate, ignorePlanningPaymentDate
            , ignoreTransDate, ignorePostingDate, ignoreVerifiedDate
            , ignoreUnknownEdit
            , ignoreNumEndsWith, ignoreNumStartsWith, ignoreNumContains
            , ignoreBy, ignoreNextApprover
            , isCanceled, isDeleted;
        public int
            holdFlag, settlementStatus, notice, transactionType, vendorGroup,
            numFrom, numTo, docType, iBy, iNext;
        public short workflowStatus;
        public string sItemPre, sItemPost, sBy, sByPre, sByPost
            ,sNext, sNextPre, sNextPost;
        public string[] divs;

        public DateTime
            docDateFrom, docDateTo,
            activityDateFrom, activityDateTo,
            submitDateFrom, submitDateTo,
            planPaymentDateFrom, planPaymentDateTo,
            transDateFrom, transDateTo,
            postingDateFrom, postingDateTo,
            verifiedDateFrom, verifiedDateTo;

        public string vendorCode, unknownEdit, issuingDivision, paymentMethod;

        public List<int> lastStatus;

        private void init()
        {
            sItemPre = "";
            sItemPost = "";
            sByPre = "";
            sBy = "";
            iBy = 0;
            
            sByPost = "";
            iNext = 0;
            sNextPre = "";
            sNextPost = "";

            numFrom = 0;
            numTo = 0;
            docType = 0;
            holdFlag = -1;
            settlementStatus = -1;
            workflowStatus = -1;
            notice = -1;
            transactionType = 0;
            vendorGroup = 0;
            docDateFrom = new DateTime();
            docDateTo = new DateTime();
            activityDateFrom = new DateTime();
            activityDateTo = new DateTime();
            submitDateFrom = new DateTime();
            submitDateTo = new DateTime();
            planPaymentDateFrom = new DateTime();
            planPaymentDateTo = new DateTime();
            postingDateFrom = new DateTime();
            postingDateTo = new DateTime();
            transDateFrom = new DateTime();
            postingDateTo = new DateTime();
            verifiedDateFrom = new DateTime();
            verifiedDateTo = new DateTime();
            vendorCode = "";
            unknownEdit = "";
            issuingDivision = "";
            paymentMethod = "";
            ignoreDate = true;
            ignoreNum = true;
            ignoreType = true;
            ignoreVendorCode = true;
            ignoreIssuingDivision = true;
            ignoreLastStatus = true;
            ignoreTransactionType = true;
            ignorePaymentMethod = true;
            ignoreVendorGroup = true;
            ignoreActivityDate = true;
            ignoreSubmitDate = true;
            ignorePlanningPaymentDate = true;
            ignoreTransDate = true;
            ignorePostingDate = true;
            ignoreVerifiedDate = true;
            ignoreUnknownEdit = true;
            ignoreNumEndsWith = true;
            ignoreNumStartsWith = true;
            ignoreNumContains = true;
            ignoreBy = true;
            ignoreNextApprover = true;
            isCanceled = false;
            isDeleted = false;
            divs = new string[] {};
        }

        public VoucherSearchCriteria()
        {
            init();
        }

        public VoucherSearchCriteria(
            string _dateFrom, string _dateTo,
            string _noFrom, string _noTo,
            string _docType, string _vendorCode,
            string _issuingDivision, List<int> _lastStatus,
            string _notice, string _transactionType,
            string _paymentMethod, string _vendorGroup,
            string _activityDateFrom, string _activityDateTo,
            string _submitDateFrom, string _submitDateTo,
            string _planningPaymentDateFrom, string _planningPaymentDateTo,
            string _transDateFrom, string _transDateTo,
            string _postingDateFrom, string _postingDateTo,
            string _verifiedDateFrom, string _verifiedDateTo,
            string _WorkflowStatus,
            string _holdFlag,
            string _settlementStatus,
            string _unknownEdit,
            string _by,
            string _next,
            bool _canceled,
            bool _deleted)
        {
            //default ignore all searching criteria
            init();

            #region build searching criteria

            if (!string.IsNullOrEmpty(_dateFrom) && !string.IsNullOrEmpty(_dateTo))
            {
                ignoreDate = false;
                docDateFrom = DateTime.ParseExact(_dateFrom, "dd/MM/yyyy", null);
                docDateTo = DateTime.ParseExact(_dateTo, "dd/MM/yyyy", null);
            }

            if (!_noFrom.isEmpty() || !_noTo.isEmpty())
            {
                if (_noFrom.IndexOf("*") < 0 && _noTo.IndexOf("*") < 0)
                {
                    ignoreNum = false;
                    if (!_noFrom.isEmpty()) numFrom = _noFrom.Int(-1); else numFrom = -1;
                    if (!_noTo.isEmpty()) numTo = _noTo.Int(-1); else numTo = -1;
                    if (numTo < 0 && numFrom > 0)
                        numTo = numFrom;
                    else if (numFrom < 0 && numTo > 0)
                        numFrom = numTo;
                    else if (numFrom < 0 && numTo < 0)
                        ignoreNum = true;
                }
                else
                {
                    string sNo = (_noFrom.isEmpty()) ? _noTo : _noFrom;

                    int iItem = LikeLogic.ignoreWhat(_noFrom, ref sItemPre, ref sItemPost);
                    switch (iItem)
                    {
                        case LikeLogic.igFIRST:
                            ignoreNumEndsWith = false;
                            break;
                        case LikeLogic.igLAST:
                            ignoreNumStartsWith = false;
                            break;
                        case LikeLogic.igFIRSTLAST:
                            ignoreNumContains = false;
                            break;
                        case LikeLogic.igMIDDLE:
                            ignoreNumEndsWith = false;
                            ignoreNumStartsWith = false;
                            break;
                        default: break;
                    }
                }
            }

            if (!string.IsNullOrEmpty(_docType) && Convert.ToInt16(_docType) != 0)
            {
                ignoreType = false;
                docType = Convert.ToInt16(_docType);
            }

            if (!string.IsNullOrEmpty(_vendorCode))
            {
                ignoreVendorCode = false;
                vendorCode = _vendorCode;
            }

            if (!_issuingDivision.isEmpty()) //   !string.IsNullOrEmpty(_issuingDivision) && Convert.ToInt16(_issuingDivision) != 0)
            {
                ignoreIssuingDivision = false;
                
                issuingDivision = _issuingDivision;

                if (!issuingDivision.isEmpty() && issuingDivision.Contains(";")) 
                {
                    divs = issuingDivision.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                }
            }
            
            ignoreLastStatus = _lastStatus.Count == 0;
            if (!ignoreLastStatus) lastStatus = _lastStatus;


            if (!string.IsNullOrEmpty(_notice))
            {

                if (!Int32.TryParse(_notice, out notice)) notice = -1;
            }
            else { notice = -1; }


            if (!string.IsNullOrEmpty(_holdFlag))
            {
                if (!Int32.TryParse(_holdFlag, out holdFlag)) holdFlag = -1;
            }
            else
            {
                holdFlag = -1;
            }

            if (!string.IsNullOrEmpty(_settlementStatus))
            {
                if (!Int32.TryParse(_settlementStatus, out settlementStatus)) settlementStatus = -1;
            }
            else
            {
                settlementStatus = -1;
            }

            if (!string.IsNullOrEmpty(_unknownEdit))
            {
                ignoreUnknownEdit = false;
                unknownEdit = _unknownEdit;
            }

            if (!string.IsNullOrEmpty(_transactionType) && Convert.ToInt16(_transactionType) != 0)
            {
                ignoreTransactionType = false;
                transactionType = Convert.ToInt16(_transactionType);
            }

            if (!string.IsNullOrEmpty(_paymentMethod))
            {
                ignorePaymentMethod = false;
                paymentMethod = _paymentMethod;
            }

            if (!string.IsNullOrEmpty(_vendorGroup) && Convert.ToInt16(_vendorGroup) != 0)
            {
                ignoreVendorGroup = false;
                vendorGroup = Convert.ToInt16(_vendorGroup);
            }

            if (!string.IsNullOrEmpty(_activityDateFrom) && !string.IsNullOrEmpty(_activityDateTo))
            {
                ignoreActivityDate = false;
                activityDateFrom = DateTime.ParseExact(_activityDateFrom, "dd/MM/yyyy", null);
                activityDateTo = DateTime.ParseExact(_activityDateTo, "dd/MM/yyyy", null);
            }


            if (!string.IsNullOrEmpty(_submitDateFrom) && !string.IsNullOrEmpty(_submitDateTo))
            {
                ignoreSubmitDate = false;
                submitDateFrom = DateTime.ParseExact(_submitDateFrom, "dd/MM/yyyy", null);
                submitDateTo = DateTime.ParseExact(_submitDateTo, "dd/MM/yyyy", null);
            }


            if (!string.IsNullOrEmpty(_planningPaymentDateFrom) && !string.IsNullOrEmpty(_planningPaymentDateTo))
            {
                ignorePlanningPaymentDate = false;
                planPaymentDateFrom = DateTime.ParseExact(_planningPaymentDateFrom, "dd/MM/yyyy", null);
                planPaymentDateTo = DateTime.ParseExact(_planningPaymentDateTo, "dd/MM/yyyy", null);
            }

            if (!string.IsNullOrEmpty(_transDateFrom) && !string.IsNullOrEmpty(_transDateTo))
            {
                ignoreTransDate = false;
                transDateFrom = DateTime.ParseExact(_transDateFrom, "dd/MM/yyyy", null);
                transDateTo = DateTime.ParseExact(_transDateTo, "dd/MM/yyyy", null);
            }

            if (!string.IsNullOrEmpty(_postingDateFrom) && !string.IsNullOrEmpty(_postingDateTo))
            {
                ignorePostingDate = false;
                postingDateFrom = DateTime.ParseExact(_postingDateFrom, "dd/MM/yyyy", null);
                postingDateTo = DateTime.ParseExact(_postingDateTo, "dd/MM/yyyy", null);
            }

            if (!string.IsNullOrEmpty(_verifiedDateFrom) && !string.IsNullOrEmpty(_verifiedDateTo))
            {
                ignoreVerifiedDate = false;
                verifiedDateFrom = DateTime.ParseExact(_verifiedDateFrom, "dd/MM/yyyy", null);
                verifiedDateTo = DateTime.ParseExact(_verifiedDateTo, "dd/MM/yyyy", null);
            }

            if (string.IsNullOrEmpty(_WorkflowStatus) || !Int16.TryParse(_WorkflowStatus, out workflowStatus))
            {
                workflowStatus = -1;
            }

            ignoreBy = _by.isEmpty();
            sBy = _by;

            if (!_by.isEmpty())
            {
                iBy = LikeLogic.ignoreWhat(_by, ref sByPre, ref sByPost);                    
            }

            ignoreNextApprover = _next.isEmpty();
            sNext = _next;
            if (!_next.isEmpty())
            {
                iNext = LikeLogic.ignoreWhat(_next, ref sNextPre, ref sNextPost);
            }

            isCanceled = _canceled;
            isDeleted = _deleted;

            #endregion

        }
    }
}
