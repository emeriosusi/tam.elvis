﻿using System;   
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI.WebControls;
using Common.Function;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class VoucherDetailIndex
    {
        private List<PVFormDetail> _details = null;
        public VoucherDetailIndex(List<PVFormDetail> details)
        {
            _details = details;
        }

        public PVFormDetail this[int index]
        {
            get
            {
                if (_details != null && (index >= 0) && (index < _details.Count))
                {
                    return _details[index];
                }
                else
                {
                    PVFormDetail d = null;
                    if (index>=0 && index == _details.Count)
                    {
                        d = new PVFormDetail()
                        {
                            CostCenterCode = "",
                            CostCenterName = "",
                            Amount = 0,
                            Description = "",
                            StandardDescriptionWording = "",
                            SequenceNumber = 0,
                            Persisted = false,
                            WbsNumber = "",
                            TaxCode = "",
                            TaxNumber = ""
                        };
                        _details.Add(d);
                    }
                    return d;
                }
            }

            set
            {
                if (_details != null && (index >= 0) && (index < _details.Count))
                {
                    _details[index] = value;
                }
                else
                {
                    if (index >= 0 && index == _details.Count)
                    {
                        _details.Add(value);
                    }
                }
            }
            
        }
    }

    [Serializable]
    public class PVFormTable
    {
        public const int MAX_PAGE_ROW = 5;
                
        private string standardDescriptionWording;
        private int lastEditingIndex;
        //public List<CodeConstant> currs; 
        public int EditIndex
        {
            get
            {
                return lastEditingIndex;
            }

            set
            {
                lastEditingIndex = value;
            }
        }


        public PVFormTable()
        {
            this.lstData = new List<PVFormDetail>();
            this.standardDescriptionWording = "";
            this.lastEditingIndex = 0;
            // Persistence = new PVFormLogic();
            // currs = persistence.getCurrencyCodeList();
            //NumberFormatInfo formatInfo = CultureInfo.CurrentCulture.NumberFormat;
            //numberFormatInfo = (NumberFormatInfo)formatInfo.Clone();
            //numberFormatInfo.CurrencySymbol = "";
        }

        //public void addDetail(PVFormDetail detail)
        //{
        //    lstData.Add(detail);
        //}

        private List<PVFormDetail> lstData;
        public List<PVFormDetail> DataList
        {
            set
            {
                lstData = value;
            }

            get
            {
                return lstData;
            }
        }

        private VoucherDetailIndex _rows = null;
        public VoucherDetailIndex rows
        {
            get
            {
                if (_rows == null)
                {
                    _rows = new VoucherDetailIndex(DataList);
                }
                return _rows;                
            }
        }

        //private int _no = -1;
        //public int DOC_NO
        //{
        //    get {
        //        return _no;
        //    }
        //}

        //private int _year = -1;
        //public int DOC_YEAR {
        //    get {
        //        return _year;
        //    }
        //}

        //public void SetDoc(int _no_, int _year_) {
        //    _year = _year_;
        //    _no = _no_ ;
        //}

        //private FormPersistence persistence;
        //public FormPersistence Persistence
        //{
        //    set
        //    {
        //        this.persistence = value;
        //    }
        //}

        //private ASPxGridView g;
        //public ASPxGridView TableGrid
        //{
        //    set
        //    {
        //        g = value;
        //    }

        //    get
        //    {
        //        return g;
        //    }
        //}        

        //public void initFirstRow()
        //{
        //    DataList.Clear();
        //    //addNewRow(false);
        //    // adjustGridHeight(); // 20130411 dan - fixed grid height no matter what
        //    reload();
        //    g.StartEdit(0);
        //    lastEditingIndex = 0;
        //    g.SettingsBehavior.AllowFocusedRow = false;
        //}               
        //public bool Editing
        //{
        //    get
        //    {
        //        return g.EditingRowVisibleIndex > -1;
        //    }
        //}
        #region 20121227 dan - 
        //public void enable(bool enabled)
        //{
        //    if (!enabled)
        //    {
        //        g.CancelEdit();
        //        sanityCheck();
        //        reload();
        //    }
        //    g.Enabled = enabled;
        //}
        #endregion
        //public void reload()
        //{
        //    g.DataSource = DataList;
        //    g.SettingsPager.PageSize = DataList.Count;
        //    g.DataBind();

        //    // adjustGridHeight(); // 2013-04-11 dan - fixed row height no matter what
        //}
        //public void setStandardDescriptionWording(string wording)
        //{
        //    this.standardDescriptionWording = wording;
        //    List<PVFormDetail> dataList = DataList;
        //    foreach (PVFormDetail d in dataList)
        //    {
        //        d.StandardDescriptionWording = wording;
        //    }
        //}
        //public string getDescriptionDefaultWording()
        //{
        //    return standardDescriptionWording;
        //}

        public string StandardWording
        {
            get
            {
                return standardDescriptionWording;
            }

            set
            {
                this.standardDescriptionWording = value;
                List<PVFormDetail> dataList = DataList;
                foreach (PVFormDetail d in dataList)
                {
                    d.StandardDescriptionWording = value;
                }
            }
        }

        //public List<PVFormDetail> getValues()
        //{
        //    g.UpdateEdit();
        //    return sanityCheck();
        //}
        //public void Update()
        //{
        //    if (g.IsEditing)
        //    {
        //        g.UpdateEdit();
        //    }            
        //}
        //public PVFormData Data
        //{
        //    get;
        //    set;
        //}


        //public int NextSeq()
        //{
        //    if (DataList.Count < 1)
        //        return 1;
        //    else
        //    {
        //        int maxSeq = DataList.Count + 1;
        //        for (int i = 0; i < DataList.Count; i++)
        //        {
        //            if (DataList[i].SequenceNumber > maxSeq)
        //            {
        //                maxSeq = DataList[i].SequenceNumber + 1;
        //            }
        //        }
        //        return maxSeq;
        //    }
        //}

        //public void addNewRow(bool taxCalculated)
        //{
        //    List<PVFormDetail> dataList = DataList;
        //    int seqNumber = dataList.Count + 1; /// this line causes error when user delete line 1 and then addNewRow
        //    seqNumber = NextSeq(); 
        //    PVFormDetail data = new PVFormDetail()
        //    {
        //        Persisted = false,
        //        CostCenterCode = "",
        //        Amount = 0,
        //        CostCenterName = "",
        //        CurrencyCode = "",
        //        Description = "",
        //        StandardDescriptionWording = getDescriptionDefaultWording(),
        //        InvoiceNumber = "",
        //        SequenceNumber = seqNumber,
        //        DisplaySequenceNumber = seqNumber,
        //        ItemTransaction = 1,
        //        TaxCode = taxCalculated ? "V1" : "V0" // to be replaced with app variable
        //    };
        //    if (dataList.Count >= 1)
        //    {
        //        PVFormDetail firstData = dataList[0];
        //        data.CostCenterCode = firstData.CostCenterCode;
        //        data.CostCenterName = firstData.CostCenterName;
        //    }
        //    dataList.Add(data);
        //}
        # region 20130411 dan -
        //public void adjustGridHeight()
        //{
        //    int cntData = lstData.Count;
        //    if (cntData >= MAX_PAGE_ROW)
        //    {
        //        g.Settings.VerticalScrollableHeight = 28 * MAX_PAGE_ROW;
        //    }
        //    else
        //    {
        //        g.Settings.VerticalScrollableHeight = 40 * cntData;
        //    }
        //    g.ScrollToVisibleIndexOnClient = cntData - 1;
        //}
        #endregion

        //public void deleteAllRow()
        //{
        //    List<PVFormDetail> dataList = DataList;
        //    dataList.Clear();
        //    initFirstRow();
        //}
        
        //public int getSelectedRow()
        //{
        //    reload();
        //    WebDataSelection selection = g.Selection;
        //    int cntData = DataList.Count;
        //    for (int i = 0; i < cntData; i++)
        //    {
        //        if (selection.IsRowSelected(i))
        //        {
        //            return i;
        //        }
        //    }

        //    return 0;
        //}
        public List<PVFormDetail> cloneDetails() {
            List<PVFormDetail> resultList = new List<PVFormDetail>(lstData.Count);
            resultList.AddRange(lstData);
            return resultList;
        }

        //public int OnSelectionChanged(object sender, EventArgs args)
        //{
        //    if (Editing)
        //    {
        //        g.UpdateEdit();
        //        int idxSelected = getSelectedRow();
        //        g.StartEdit(idxSelected);
        //        return idxSelected;
        //    }
        //    else
        //        return -1;
        //}
        //public void OnCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs arg)
        //{
            
        //}

        

        //public void startEditing()
        //{
        //    GridViewColumn column = g.Columns[VouCol.DELETION_CONTROL];
        //    column.Visible = true;
        //    reload();

        //    g.CancelEdit();
        //    int cntData = DataList.Count;
        //    if (cntData > 0)
        //    {
        //        lastEditingIndex = cntData - 1;
        //        g.StartEdit(lastEditingIndex);
        //    }
        //    else
        //    {
        //        initFirstRow();
        //    }
        //}
        //public void startBrowsing()
        //{
        //    g.CancelEdit();
        //    ASPxGridViewPagerSettings pager = g.SettingsPager;
        //    pager.PageSize = MAX_PAGE_ROW;
        //    pager.Mode = GridViewPagerMode.ShowPager;

        //    GridViewColumn column = g.Columns[VouCol.DELETION_CONTROL];
        //    column.Visible = false;
        //    reload();
        //}
        //public void cancelEditing()
        //{
        //    g.CancelEdit();
        //    cancelNonPersistedData();
        //    lstData = sanityCheck();
        //}
        //public void cancelNonPersistedData()
        //{
        //    List<PVFormDetail> dataList = DataList;
        //    List<PVFormDetail> lstDeleted = new List<PVFormDetail>();
        //    foreach (PVFormDetail d in dataList)
        //    {
        //        if (!d.Persisted)
        //        {
        //            lstDeleted.Add(d);
        //        }
        //    }

        //    foreach (PVFormDetail d in lstDeleted)
        //    {
        //        dataList.Remove(d);
        //    }
        //}
        
        
        //public bool hasEmptyDescription()
        //{
        //    int c = lstData.Where(d=> d.Description.isEmpty()).Count();
        //    return c > 0;
        //}
        
        //public bool hasEmptyCostCenter()
        //{
        //    int c = (from d in lstData 
        //             where (d.CostCenterCode == null || d.CostCenterCode == "")
        //             select 1).Count();
        //    return (c > 0);
        //}

        //public bool validCostCenter(int[] GlAccountNoCC, bool CostCenterFlag, List<string> e)
        //{
        //    string[] glAccountMandatoryCC = new string[] {"5", "6", "7"};
        //    int ccOk = 0;
        //    if (CostCenterFlag)
        //    {
                
        //        foreach (var g in lstData)
        //        {
        //            bool canBlank = false;
        //            if (!g.GlAccount.isEmpty() && g.GlAccount.Length > 1)
        //            {
        //                string x = "";
        //                x = g.GlAccount.Substring(0, 1);
        //                canBlank = !glAccountMandatoryCC.Contains(x) 
        //                         || GlAccountNoCC.Contains(g.GlAccount.Int());
        //            }
        //            if (g.CostCenterCode.isEmpty() == canBlank)
        //            {
        //                ccOk++;
        //            }
        //            else
        //            {
        //                e.Add(string.Format("seq# {0} Cost Center '{1}' must be {2} for GlAccount [{3}]",
        //                    g.SequenceNumber, 
        //                    g.CostCenterCode, 
        //                    (canBlank?"Empty": "Filled"), 
        //                    g.GlAccount)); 
        //            }
        //        }
               
        //    }
        //    else
        //    {
        //        foreach (var h in lstData)
        //        {
        //            if (h.CostCenterCode.isEmpty())
        //                ccOk++;
        //            else
        //            {
        //                e.Add(string.Format("seq# {0} Cost Center must be empty", h.SequenceNumber));
        //            }

        //        }
        //    }
        //    return ccOk == lstData.Count;
        //}

        //public bool hasInvalidDetail(bool ignoreMinus=false)
        //{
        //    int cntDataList = lstData.Count;
        //    PVFormDetail d;
        //    for (int i = 0; i < cntDataList; i++)
        //    {
        //        d = lstData[i];

        //        if (string.IsNullOrEmpty(d.CurrencyCode) || ((d.Amount < 0) && !ignoreMinus))
        //        {
        //            return true;
        //        }
        //    }

        //    return false;
        //}
        //public List<PVFormDetail> sanityCheck()
        //{
        //    List<PVFormDetail> dataList = DataList;
        //    List<PVFormDetail> cleanedDataList = new List<PVFormDetail>();
                        
        //    int cntDataList = dataList.Count;
        //    PVFormDetail d;
        //    for (int i = 0; i < cntDataList; i++)
        //    {
        //        d = dataList[i];
        //        d.CostCenterCode = d.CostCenterCode != null ? d.CostCenterCode : "";
        //        d.CostCenterName = d.CostCenterName != null ? d.CostCenterName : "";
        //        d.CurrencyCode = d.CurrencyCode != null ? d.CurrencyCode : "";
        //        d.Description = d.Description != null ? d.Description : "";
        //        d.StandardDescriptionWording = d.StandardDescriptionWording != null ? d.StandardDescriptionWording : "";
        //        d.InvoiceNumber = d.InvoiceNumber != null ? d.InvoiceNumber : "";

        //        if (!d.CurrencyCode.Trim().Equals("") 
        //            //&& d.Amount != 0 // 20121212 dan - for settlement 
        //            )
        //        {
        //            cleanedDataList.Add(d);
        //        }
        //    }

        //    return cleanedDataList;      
        //}
        //public void reset()
        //{
        //    this.lstData.Clear();
        //    this.standardDescriptionWording = "";
        //}        
        //public void resetDisplaySequenceNumber()
        //{
        //    int cntData = DataList.Count;
        //    PVFormDetail detail;
        //    for (int i = 0; i < cntData; i++)
        //    {
        //        detail = lstData[i];
        //        detail.DisplaySequenceNumber = i + 1;
        //    }
        //}
        //public void ReSequence()
        //{
        //    bool needReseq = false;
        //    for (int i = 0; i < DataList.Count; i++)
        //    {
        //        if (DataList[i].SequenceNumber != i)
        //        {
        //            needReseq = true;
        //            break;
        //        }
        //    }
        //    if (needReseq)
        //    {
        //        int j = 0;
        //        foreach (PVFormDetail d in DataList.OrderBy(x => x.SequenceNumber))
        //        {
        //            j++;
        //            d.SequenceNumber = j;
        //        }
        //    }
        //}
        //public List<PVFormDetail> getBlankRowCleanedDetails()
        //{
        //    List<PVFormDetail> lstClone = new List<PVFormDetail>(lstData.Count);
        //    foreach (PVFormDetail d in lstData)
        //    {
        //        d.CostCenterCode = d.CostCenterCode != null ? d.CostCenterCode : "";
        //        d.CostCenterName = d.CostCenterName != null ? d.CostCenterName : "";
        //        d.CurrencyCode = d.CurrencyCode != null ? d.CurrencyCode : "";
        //        d.Description = d.Description != null ? d.Description : "";
        //        d.StandardDescriptionWording = d.StandardDescriptionWording != null ? d.StandardDescriptionWording : "";
        //        d.InvoiceNumber = d.InvoiceNumber != null ? d.InvoiceNumber : "";

        //        if(!string.IsNullOrEmpty(d.CostCenterCode) ||
        //           !string.IsNullOrEmpty(d.CurrencyCode) ||
        //           !string.IsNullOrEmpty(d.Description) || (d.Amount >= 0))
        //        {
        //            lstClone.Add(d);
        //        }
        //    }
        //    return lstClone;
        //}

        //private NumberFormatInfo numberFormatInfo = null;
        //public NumberFormatInfo BlankCurrencyNumberFormatInfo
        //{
        //    get
        //    {
        //        if (numberFormatInfo == null)
        //        {
        //            NumberFormatInfo formatInfo = CultureInfo.CurrentCulture.NumberFormat;
        //            numberFormatInfo = (NumberFormatInfo)formatInfo.Clone();
        //            numberFormatInfo.CurrencySymbol = "";
        //        }

        //        return numberFormatInfo;
        //    }
        //}

        //private Literal _ltTotalAmount;
        //public Literal TotalAmountWriteHolder
        //{
        //    set
        //    {
        //        this._ltTotalAmount = value;
        //    }

        //    get
        //    {
        //        return _ltTotalAmount;
        //    }
        //}

        
        



        //public Dictionary<string, decimal> AmountByCurrency()
        //{
        //    return AmountByCurrency(lstData);
        //}

        //public OrderedDictionary createTotalAmountMap()
        //{
        //    OrderedDictionary map = new OrderedDictionary();
        //    object objAmount;
        //    decimal totalAmount = 0;
        //                foreach (PVFormDetail d in lstData)
        //    {
        //        if (!string.IsNullOrEmpty(d.CurrencyCode))
        //        {
        //            objAmount = map[d.CurrencyCode];
        //            if (objAmount == null)
        //            {
        //                totalAmount = 0;
        //            }
        //            else
        //            {
        //                totalAmount = objAmount.Dec();
        //            }

        //            map[d.CurrencyCode] = totalAmount + d.Amount;
        //        }                
        //    }

        //    return map;
        //}

        //public int EmptyCostCenter()
        //{
        //    foreach (PVFormDetail d in lstData)
        //    {
        //        d.CostCenterCode = "";
        //        d.CostCenterName = "";
        //    }
        //    return 0;
        //}

        
        //public int InvoiceCount()
        //{
        //    return (from a in DataList
        //            select a.InvoiceNumber).Distinct()
        //            .Where(x=>!x.isEmpty())
        //            .Count();
        //}
    }
}