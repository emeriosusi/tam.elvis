﻿using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data.SAPData;
using Common.Enum;
using Common.Function;
using DataLayer.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;

namespace BusinessLogic.VoucherForm
{
    public class PVFormLogic : FormPersistence
    {
        readonly List<CodeConstant> docTypes = CommonData.pvTypeCodes;
        public override List<CodeConstant> getDocTypes()
        {
            return CommonData.PVTypes;
        }

        public override string getDocType(int? code)
        {
            string coda = (code ?? 0).str();
            return CommonData.pvTypeCodes
                .Where(a => a.Code == coda)
                .Select(b => b.Description)
                .FirstOrDefault();
        }

        public int getDocumentNo(string uid = "")
        {
            return ((int)GetDocNo(1, uid));
        }

        protected override List<PVFormDetail> GetDetail(PVFormData f, ELVIS_DBEntities DB)
        {
            return GetPV(f, DB);
        }

        public List<PVFormDetail> GetFormDetail(int _no, int _yy)
        {
            List<PVFormDetail> re = new List<PVFormDetail>();
            var q = db.TB_R_PV_D
                    .Where(p => p.PV_NO == _no && p.PV_YEAR == _yy);
            if (q.Any())
            {
                foreach (var r in q)
                {
                    re.Add(new PVFormDetail()
                    {
                        CostCenterCode = r.COST_CENTER_CD,
                        Amount = r.AMOUNT,
                        CurrencyCode = r.CURRENCY_CD,
                        FromCurr = r.FROM_CURR,
                        Description = r.DESCRIPTION,
                        GlAccount = r.GL_ACCOUNT.str(),
                        ItemTransaction = r.ITEM_TRANSACTION_CD,
                        ItemNo = r.ITEM_NO,
                        InvoiceNumber = r.INVOICE_NO,
                        InvoiceDate = r.INVOICE_DATE,
                        SequenceNumber = r.SEQ_NO,
                        FromSeq = r.FROM_SEQ,
                        TaxCode = r.TAX_CD,
                        TaxNumber = r.TAX_NO,
                        Persisted = false
                    });

                }
            }

            return re;
        }

        public override PVFormData search(int pvNumber, int pvYear)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var query = db.TB_R_PV_H
                            .Where(t => (t.PV_YEAR == pvYear) && (t.PV_NO == pvNumber));

                var result = query.ToList();
                if (result.Any())
                {
                    PVFormData data = new PVFormData(1);
                    var d = result[0];
                    data.PVNumber = d.PV_NO;
                    data.PVYear = d.PV_YEAR;

                    data.ActivityDate = d.ACTIVITY_DATE_FROM;
                    data.ActivityDateTo = d.ACTIVITY_DATE_TO;
                    data.BudgetNumber = d.BUDGET_NO;
                    //fid.pras 25/06/2018 initial edit pvform page
                    data.BookingNo = d.BUDGET_NO;
                    data.DivisionID = d.DIVISION_ID;
                    data.PaymentMethodCode = d.PAY_METHOD_CD;
                    data.PVDate = d.PV_DATE;
                    data.PVTypeCode = d.PV_TYPE_CD;
                    data.StatusCode = d.STATUS_CD;
                    data.SuspenseNumber = d.SUSPENSE_NO;
                    data.SuspenseYear = d.SUSPENSE_YEAR;
                    data.TransactionCode = d.TRANSACTION_CD;
                    data.VendorCode = d.VENDOR_CD;
                    data.VendorGroupCode = d.VENDOR_GROUP_CD;
                    data.HoldingUser = d.HOLD_BY;
                    data.OnHold = (d.HOLD_FLAG.HasValue
                        && d.HOLD_FLAG > 0
                        );
                    data.HOLD_FLAG = d.HOLD_FLAG;
                    data.COUNTER_FLAG = d.COUNTER_FLAG;
                    data.PostingDate = d.POSTING_DATE;
                    data.PlanningPaymentDate = d.PLANNING_PAYMENT_DATE;
                    data.BankType = d.BANK_TYPE;
                    data.TaxCalculated = data.TaxCode != null;
                    data.SubmitHC = d.SUBMIT_HC_DOC_DATE;
                    data.ReferenceNo = d.REFFERENCE_NO;
                    data.Canceled = (d.CANCEL_FLAG ?? 0) == 1;
                    data.FormTable.DataList = GetDetail(data, db);
                    data.UserName = UserName;
                    return data;
                }
            }

            return null;
        }

        public int saveHistoryHeader(PVFormData f, UserData u, ELVIS_DBEntities DB)
        {

            var Vers = DB.TB_H_PV_H
                         .Where(h =>
                                h.PV_NO == f.PVNumber
                             && h.PV_YEAR == f.PVYear)
                         .Select(h => h.VERSION)
                         .ToList();
            int lastVersion = 1;
            if (Vers != null && Vers.Count > 0)
            {
                lastVersion = Vers.Max() + 1;
            }

            TB_H_PV_H hHistory = new TB_H_PV_H()
            {
                POSTING_DATE = f.PostingDate,
                PLANNING_PAYMENT_DATE = f.PlanningPaymentDate,
                BANK_TYPE = f.BankType,
                STATUS_CD = f.StatusCode.Value,
                PV_NO = f.PVNumber.Value,
                PV_YEAR = f.PVYear.Value,
                PAY_METHOD_CD = f.PaymentMethodCode,
                VENDOR_GROUP_CD = f.VendorGroupCode,
                VENDOR_CD = f.VendorCode,
                PV_TYPE_CD = f.PVTypeCode,
                TRANSACTION_CD = f.TransactionCode,
                BUDGET_NO = f.BudgetNumber,
                ACTIVITY_DATE_FROM = f.ActivityDate,
                ACTIVITY_DATE_TO = f.ActivityDateTo,
                DIVISION_ID = f.DivisionID,
                PV_DATE = f.PVDate.Value,
                TOTAL_AMOUNT = f.GetTotalAmount(),
                CREATED_DATE = today,
                CREATED_BY = u.USERNAME,
                VERSION = lastVersion
            };

            DB.TB_H_PV_H.AddObject(hHistory);
            DB.SaveChanges();
            return lastVersion;
        }

        public void saveHistoryDetail(PVFormData f, UserData u, int lastVersion, ELVIS_DBEntities DB)
        {
            /* Detail History */
            int lastSeqNo = 1;

            foreach (PVFormDetail detail in f.Details)
            {
                lastSeqNo++;
                int seq = lastSeqNo;
                if (detail.SequenceNumber != 0) seq = detail.SequenceNumber;

                DB.TB_H_PV_D.AddObject(new TB_H_PV_D()
                {
                    SEQ_NO = seq,
                    VERSION = lastVersion,
                    PV_NO = f.PVNumber.Value,
                    PV_YEAR = f.PVYear.Value,
                    AMOUNT = (detail.CurrencyCode == "IDR" ? Math.Round(detail.Amount) : detail.Amount),
                    COST_CENTER_CD = detail.CostCenterCode,
                    CURRENCY_CD = detail.CurrencyCode,
                    TAX_CD = detail.TaxCode,
                    TAX_NO = detail.TaxNumber,
                    ITEM_TRANSACTION_CD = detail.ItemTransaction,
                    DESCRIPTION = detail.Description,
                    INVOICE_NO = detail.InvoiceNumber,
                    CREATED_BY = u.USERNAME,
                    CREATED_DT = today
                });
            }
        }

        public void AppendAmounts(Dictionary<string, double> d, string k, double v)
        {
            if (d.Keys.Contains(k))
            {
                double a = d[k];

                d[k] = a + v;
            }
            else
            {
                d[k] = v;
            }
        }

        private bool saveSettlementDetail(PVFormData f, UserData u, ELVIS_DBEntities DB)
        {
            int _no, _yy;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? 0;

            var ter = DB.TB_R_PV_H
                      .Where(se => se.PV_NO == _no && se.PV_YEAR == _yy)
                      .FirstOrDefault();
            int? susno = null, susye = null;
            if (ter != null)
            {
                susno = ter.SUSPENSE_NO;
                susye = ter.SUSPENSE_YEAR;
            }

            PVFormData asu = new PVFormData()
            {
                PVNumber = susno,
                PVYear = susye
            };

            List<PVFormDetail> lisu = GetDetail(asu, DB);

            // take original suspense 

            List<OriginalAmountData> osu = null;
            if (susno != null && susye != null)
            {
                osu = logic.Base.GetOriginal((int)susno, (int)susye);
            }

            List<ExchangeRate> exchangeRates = getExchangeRates();


            List<PVFormDetail> fse = f.Details;
            List<PVFormDetail> d = new List<PVFormDetail>();
            // generate settlement detail in form of List <PVFormDetail>
            // based on values found in suspense vs settlement detail
            var qsu = lisu
                    .OrderBy(a => a.FromSeq)
                    .ThenBy(a => a.SequenceNumber).ToList();

            for (int i = 0; i < fse.Count; i++)
            {
                PVFormDetail dse = fse[i];

                PVFormDetail dsu = lisu.Where(
                    su => su.SequenceNumber == dse.SequenceNumber
                        && ((dse.FromSeq != null && (su.FromSeq == dse.FromSeq || su.FromSeq == null))
                              || dse.FromSeq == null
                            )
                        ).FirstOrDefault();
                PVFormDetail dx = null;
                string addor = "";

                if (dsu != null)
                {
                    addor = "Suspense.AMOUNT+Settle.AMOUNT";

                    decimal exchangeRateSettlement = 1;
                    decimal exchangeRateSuspense = 1;
                    if (dse.CurrencyCode.Equals("IDR") && !dse.FromCurr.isEmpty())
                    {
                        var exCur = exchangeRates.Where(x => x.CurrencyCode == dse.FromCurr);
                        if (exCur.Any())
                        {
                            exchangeRateSettlement = exCur.Select(x => x.Rate).FirstOrDefault();
                        }

                        var xSu = osu.Where(y => y.CURRENCY_CD == dse.FromCurr);
                        if (xSu.Any())
                        {
                            exchangeRateSuspense = xSu.Select(y => y.EXCHANGE_RATE).FirstOrDefault() ?? 0;
                        }
                    }

                    decimal settleAmount = dse.Amount;

                    if (exchangeRateSettlement > 1 && exchangeRateSuspense > 1)
                    {
                        settleAmount = (settleAmount / exchangeRateSettlement) * exchangeRateSuspense;
                    }

                    dx = new PVFormDetail()
                    {
                        SequenceNumber = dse.SequenceNumber,
                        CurrencyCode = dse.CurrencyCode,

                        Amount = settleAmount + dsu.Amount,
                        Description = dse.Description,
                        GlAccount = dse.GlAccount,
                        ItemTransaction = dse.ItemTransaction,
                        Persisted = false
                    };
                }
                else
                {
                    decimal spent = 0;
                    OriginalAmountData o = null;
                    int oSeq = dse.FromSeq ?? dse.SequenceNumber;

                    var oo = osu.Where(ox => ox.SEQ_NO == oSeq);
                    if (oo.Any())
                    {
                        o = oo.FirstOrDefault();
                        var sf = fse.Where(a => a.SequenceNumber == oSeq);
                        decimal seA = 0;
                        if (sf.Any())
                        {
                            PVFormDetail dSea = sf.FirstOrDefault();
                            if (dSea != null)
                                seA = dSea.Amount;
                        }

                        spent = (o.SPENT_AMOUNT ?? 0) - o.AMOUNT - seA;
                        if (dse.FromSeq != null)
                        {
                            spent = spent * o.EXCHANGE_RATE ?? 1;
                        }
                    }

                    if (o != null)
                    {
                        addor = "Original.SPENT_AMOUNT";

                        dx = new PVFormDetail()
                        {
                            SequenceNumber = dse.SequenceNumber,
                            CurrencyCode = dse.CurrencyCode,
                            Amount = spent,
                            Description = dse.Description,
                            GlAccount = dse.GlAccount,
                            ItemTransaction = dse.ItemTransaction,
                            Persisted = false
                        };

                    }
                }
                if (dx != null)
                {
                    d.Add(dx);
                }
            }

            ReplaceSettlement(DB, susno, susye, _no, _yy, d, u.USERNAME);

            return true;
        }

        public bool saveHeader(PVFormData formData, UserData userData,
            bool financeHeader, out int version, ELVIS_DBEntities DB)
        {
            bool headerSaved = false;
            TB_R_PV_H h = null;
            version = 1;
            try
            {
                bool added = false;
                int _no, _yy;
                _no = formData.PVNumber ?? 0;
                _yy = formData.PVYear ?? 0;
                var existingHeaders = (from t in DB.TB_R_PV_H
                                       where (t.PV_NO == _no)
                                          && (t.PV_YEAR == _yy)
                                          && (_no != 0)
                                       select t).ToList();


                if (existingHeaders != null && existingHeaders.Any())
                {
                    h = existingHeaders[0];
                }
                else
                {
                    added = true;
                    h = new TB_R_PV_H();
                    _no = (int)GetDocNo(1, userData.USERNAME);
                    formData.PVNumber = _no;
                    formData.PVYear = DateTime.Now.Year;
                    DB.TB_R_PV_H.AddObject(h);
                }

                if (formData.isSubmit && !financeHeader)
                {
                    string buNo = formData.BudgetNumber;
                    //fid.Hadid
                    //PV with trans type accrued
                    string bookNo = formData.BookingNo ?? "";
                    //end fid.Hadid

                    if (!buNo.isEmpty() && buNo.IndexOf("E-") == 0)
                    {
                        string buNoYY = buNo.Substring(2, 2);
                        int fiscalYear = logic.Sys.FiscalYear(formData.PVDate ?? new DateTime(StrTo.NULL_YEAR, 1, 1));
                        string buFiscal = fiscalYear.str();
                        buFiscal = buFiscal.Substring(buFiscal.Length - 2, 2);
                        if (string.Compare(buNoYY, buFiscal, true) != 0)
                        {
                            formData.BudgetNumber = buNo.Substring(0, 2) + buFiscal + buNo.Substring(4, buNo.Length - 4);
                        }
                    }
                    //fid.Hadid
                    //PV with trans type accrued
                    else if (!bookNo.isEmpty())
                    {
                        formData.BudgetNumber = bookNo;
                    }
                    //end fid.Hadid
                }

                if (financeHeader)
                {
                    //(commented by Vandy - no longer need because 21 --> verified by budget) formData.StatusCode = 21;
                    h.PAY_METHOD_CD = formData.PaymentMethodCode; /// 2013.06.24 dan + 
                    h.POSTING_DATE = formData.PostingDate;
                    h.PLANNING_PAYMENT_DATE = formData.PlanningPaymentDate;
                    h.BANK_TYPE = formData.BankType;
                    h.CHANGED_BY = userData.USERNAME;
                    h.CHANGED_DATE = today;
                }
                else
                {
                    h.PV_NO = formData.PVNumber.Value;
                    h.PV_YEAR = formData.PVYear.Value;
                    h.PAY_METHOD_CD = formData.PaymentMethodCode;
                    h.VENDOR_GROUP_CD = formData.VendorGroupCode;
                    h.VENDOR_CD = formData.VendorCode;
                    h.PV_TYPE_CD = formData.PVTypeCode;
                    h.TRANSACTION_CD = formData.TransactionCode;
                    h.BUDGET_NO = formData.BudgetNumber;
                    //h.BUDGET_NO = formData.BookingNo;
                    h.ACTIVITY_DATE_FROM = formData.ActivityDate;
                    h.ACTIVITY_DATE_TO = formData.ActivityDateTo;
                    h.DIVISION_ID = formData.DivisionID;
                    h.PV_DATE = formData.PVDate.Value;
                    h.REFFERENCE_NO = formData.ReferenceNo;

                    if (h.BUDGET_NO.isEmpty())
                    {
                        h.BUDGET_NO = formData.BookingNo;
                    }

                    if (added)
                    {
                        h.CREATED_DATE = today;
                        h.CREATED_BY = userData.USERNAME;
                    }
                    else
                    {
                        h.CHANGED_DATE = today;
                        h.CHANGED_BY = userData.USERNAME;
                    }
                    h.TOTAL_AMOUNT = formData.GetTotalAmount();

                    if (formData.SuspenseNumber != null && formData.SuspenseNumber != int.MinValue)
                    {
                        h.SUSPENSE_NO = formData.SuspenseNumber;
                        h.SUSPENSE_YEAR = formData.SuspenseYear;
                    }
                }
                h.STATUS_CD = formData.StatusCode ?? 0;

                if (formData.isSubmit)
                {
                    h.CANCEL_FLAG = 0;
                    version = saveHistoryHeader(formData, userData, DB);
                }

                DB.SaveChanges();

                headerSaved = true;
            }
            catch (Exception ex)
            {
                headerSaved = false;
                LoggingLogic.err(ex);
                throw;
            }
            return headerSaved;
        }

        public void delExistingDetail(PVFormData formData, UserData u, ELVIS_DBEntities DB)
        {
            int _no = formData.PVNumber ?? 0;
            int _yy = formData.PVYear ?? 0;

            List<TB_R_PV_D> existingDetails =
                   (from t in DB.TB_R_PV_D
                    where (t.PV_NO == _no) && (t.PV_YEAR == _yy)
                    select t).ToList();
            List<string> invoices = new List<string>();

            if (!existingDetails.Any()) return;
            List<TB_R_PV_D> lstDeleted = new List<TB_R_PV_D>();

            /* Update existing records */
            bool matched = false;
            foreach (TB_R_PV_D d in existingDetails)
            {
                matched = false;
                foreach (PVFormDetail detail in formData.Details)
                {
                    if (detail.SequenceNumber == d.SEQ_NO)
                    {
                        d.AMOUNT = (detail.CurrencyCode == "IDR" ? Math.Round(detail.Amount) : detail.Amount);
                        d.COST_CENTER_CD = detail.CostCenterCode;
                        d.CURRENCY_CD = detail.CurrencyCode;
                        d.TAX_CD = detail.TaxCode;
                        d.TAX_NO = detail.TaxNumber;
                        d.ITEM_TRANSACTION_CD = detail.ItemTransaction;
                        d.GL_ACCOUNT = detail.GlAccount.isEmpty()
                            ? GLAccount(detail.CostCenterCode, formData.TransactionCode ?? 0, detail.ItemTransaction ?? 0)
                            : detail.GlAccount.Int();
                        d.FROM_CURR = detail.FromCurr;
                        d.FROM_SEQ = detail.FromSeq;
                        d.DESCRIPTION = detail.Description;
                        d.CHANGED_DT = today;
                        d.CHANGED_BY = u.USERNAME;
                        #region EFB
                        d.WHT_TAX_CODE = detail.WHT_TAX_CODE;
                        d.WHT_TAX_TARIFF = detail.WHT_TAX_TARIFF;
                        d.WHT_TAX_ADDITIONAL_INFO = detail.WHT_TAX_ADDITIONAL_INFO;
                        d.WHT_DPP_PPh_AMOUNT = detail.WHT_DPP_PPh_AMOUNT;
                        #endregion
                        detail.Persisted = true;
                        matched = true;

                        break;
                    }
                }

                /* Delete record if none matched in the memory */
                if (!matched)
                {
                    DB.TB_R_PV_D.DeleteObject(d);
                    DB.SaveChanges();
                }
                else
                {
                    if (!string.IsNullOrEmpty(d.INVOICE_NO))
                        invoices.Add(d.INVOICE_NO + "|" + formData.VendorCode);
                }
            }
            invoices = invoices.Distinct().ToList();
            foreach (string inv in invoices)
            {
                string[] ainv = inv.Split('|');
                if (ainv.Length > 1)
                {
                    string ino = ainv[0];
                    string iv = ainv[1];
                    var qi = (from ih in DB.TB_R_INVOICE_H
                              where ih.INVOICE_NO == ino
                              && ih.VENDOR_CD == iv
                              select ih);
                    int invoiceUpdated = 0;
                    foreach (var updatedInvoice in qi)
                    {
                        updatedInvoice.TRANSACTION_CD = formData.TransactionCode;
                        ++invoiceUpdated;
                    }
                    if (invoiceUpdated > 0)
                        DB.SaveChanges();
                }
            }
        }

        public void saveDetail(PVFormData formData, UserData userData, int version,
            ELVIS_DBEntities DB)
        {
            foreach (PVFormDetail d in formData.Details)
            {
                if (!d.Persisted)
                {
                    int? glacc = null;

                    if (d.GlAccount.isEmpty())
                        glacc = GLAccount(d.CostCenterCode, formData.TransactionCode ?? 0, d.ItemTransaction);
                    else
                        glacc = d.GlAccount.Int();

                    TB_R_PV_D x = db.TB_R_PV_D
                        .Where(a => a.PV_NO == formData.PVNumber.Value
                            && a.PV_YEAR == formData.PVYear.Value
                            && a.SEQ_NO == d.SequenceNumber)
                        .FirstOrDefault();
                    if (x == null)
                    {
                        x = new TB_R_PV_D()
                        {
                            SEQ_NO = d.SequenceNumber,
                            PV_NO = formData.PVNumber.Value,
                            PV_YEAR = formData.PVYear.Value,
                            AMOUNT = (d.CurrencyCode == "IDR" ? Math.Round(d.Amount) : d.Amount),
                            COST_CENTER_CD = d.CostCenterCode,
                            CURRENCY_CD = d.CurrencyCode,
                            TAX_CD = d.TaxCode,
                            TAX_NO = d.TaxNumber,
                            TAX_DT = d.TaxDate,
                            ITEM_TRANSACTION_CD = d.ItemTransaction,
                            GL_ACCOUNT = glacc,
                            DESCRIPTION = d.Description,
                            FROM_CURR = d.FromCurr,
                            FROM_SEQ = d.FromSeq,
                            ITEM_NO = d.ItemNo,
                            INVOICE_DATE = d.InvoiceDate,
                            INVOICE_NO = d.InvoiceNumber,
                            TAX_ASSIGNMENT = d.TaxAssignment,
                            DPP_AMOUNT = d.DppAmount,
                            ACC_INF_ASSIGNMENT = d.AccInfAssignment,
                            CREATED_BY = userData.USERNAME,
                            CREATED_DT = today,
                            #region EFB
                            WHT_TAX_CODE = d.WHT_TAX_CODE,
                            WHT_TAX_TARIFF = d.WHT_TAX_TARIFF,
                            WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO,
                            WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT
                            #endregion
                        };
                        DB.TB_R_PV_D.AddObject(x);
                    }
                    else
                    {
                        x.AMOUNT = (d.CurrencyCode == "IDR" ? Math.Round(d.Amount) : d.Amount);
                        x.COST_CENTER_CD = d.CostCenterCode;
                        x.CURRENCY_CD = d.CurrencyCode;
                        x.TAX_CD = d.TaxCode;
                        x.TAX_NO = d.TaxNumber;
                        x.TAX_DT = d.TaxDate;
                        x.ITEM_TRANSACTION_CD = d.ItemTransaction;
                        x.GL_ACCOUNT = glacc;
                        x.DESCRIPTION = d.Description;
                        x.FROM_CURR = d.FromCurr;
                        x.FROM_SEQ = d.FromSeq;
                        x.ITEM_NO = d.ItemNo;
                        x.INVOICE_DATE = d.InvoiceDate;
                        x.INVOICE_NO = d.InvoiceNumber;
                        x.TAX_ASSIGNMENT = d.TaxAssignment;
                        x.DPP_AMOUNT = d.DppAmount;
                        x.ACC_INF_ASSIGNMENT = d.AccInfAssignment;
                        x.CHANGED_BY = userData.USERNAME;
                        x.CHANGED_DT = today;
                        #region EFB
                        x.WHT_TAX_CODE = d.WHT_TAX_CODE;
                        x.WHT_TAX_TARIFF = d.WHT_TAX_TARIFF;
                        x.WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO;
                        x.WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT;
                        #endregion
                    }

                    DB.SaveChanges();
                    d.Persisted = true;
                }
            }

            /* Detail History */
            if (formData.isSubmit)
            {
                saveHistoryDetail(formData, userData, version, DB);
                DB.SaveChanges();
            }
        }


        public void saveAmounts(PVFormData formData, UserData userData, ELVIS_DBEntities DB)
        {
            int _no, _yy;
            _no = formData.PVNumber ?? 0;
            _yy = formData.PVYear ?? 0;
            List<TB_R_PVRV_AMOUNT> existingAmounts =
                (from t in DB.TB_R_PVRV_AMOUNT
                 where (t.DOC_NO == _no) && (t.DOC_YEAR == _yy)
                 select t).ToList();

            OrderedDictionary mapTotalAmount = formData.FormTable.TotalAmountMap();
         
            if (existingAmounts.Any())
            {
                object objAmount;
                foreach (TB_R_PVRV_AMOUNT d in existingAmounts)
                {

                    objAmount = mapTotalAmount[d.CURRENCY_CD];
                    

                    if (objAmount == null)
                    {
                        
                        DB.TB_R_PVRV_AMOUNT.DeleteObject(d);
                    }
                    else
                    {
                        
                        d.TOTAL_AMOUNT = Convert.ToDecimal(objAmount);
                        d.CHANGED_BY = userData.USERNAME;
                        d.CHANGED_DATE = today;
                    }
                }
                
                DB.SaveChanges();
                
            }

            List<ExchangeRate> exchangeRates = getExchangeRates();
            existingAmounts = (from t in DB.TB_R_PVRV_AMOUNT
                               where (t.DOC_NO == _no) && (t.DOC_YEAR == _yy)
                               select t).ToList();
            bool processed;
            TB_R_PVRV_AMOUNT tblAmount;
            ICollection lstKey = mapTotalAmount.Keys;
            foreach (string key in lstKey)
            {
                processed = false;
                foreach (TB_R_PVRV_AMOUNT amount in existingAmounts)
                {
                    if (key.Equals(amount.CURRENCY_CD))
                    {
                        processed = true;
                        break;
                    }
                }

                if (!processed)
                {
                    decimal xr = (from er in exchangeRates
                                  where er.CurrencyCode.Equals(key)
                                  select er.Rate).FirstOrDefault();
                    decimal exchangeRate = xr;

                    tblAmount = new TB_R_PVRV_AMOUNT()
                    {
                        CREATED_BY = userData.USERNAME,
                        CREATED_DATE = today,
                        CURRENCY_CD = key,
                        EXCHANGE_RATE = exchangeRate,
                        DOC_NO = _no,
                        DOC_YEAR = _yy,
                        TOTAL_AMOUNT = Convert.ToDecimal(mapTotalAmount[key])
                    };

                    DB.TB_R_PVRV_AMOUNT.AddObject(tblAmount);
                    DB.SaveChanges();
                }
            }
        }

        public void SettlementSuspenseNo(int _no, int _yy, ref int susno, ref int susye)
        {
            PVListData d = Qx<PVListData>("GetSuspenseNo", new { no = _no, year = _yy }).FirstOrDefault();
            if (!d.SUSPENSE_NO.isEmpty())
                susno = d.SUSPENSE_NO.Int();
            if (!d.SUSPENSE_YEAR.isEmpty())
                susye = d.SUSPENSE_YEAR.Int();
        }

        public List<int> GetNoCashierPolicyLines(int _no, int _yy, List<PVFormDetail> Details)
        {
            /// find cashier policy that don't need any Cashier policy 
            // find detail that only has one line related to
            // suspense original amount and set that line to one line in posting

            List<int> noCashierPolicy = new List<int>();

            int susno = 0;
            int susye = 0;

            SettlementSuspenseNo(_no, _yy, ref susno, ref susye);

            List<OriginalAmountData> osu = logic.Base.GetOriginal(susno, susye);

            List<PVFormDetail> desu = GetFormDetail(susno, susye);

            // when spent > ori suspense and cashier policy is applied for settlement 
            // but not for suspense then disable cashier policy for that detail 

            foreach (var v in Details) // change it to original amount 
            {
                PVFormDetail d = desu.Where(
                    x => x.SequenceNumber == v.SequenceNumber
                    && x.CurrencyCode == v.CurrencyCode
                    && x.FromCurr.isEmpty())
                    .FirstOrDefault();
                if (d != null)
                {
                    OriginalAmountData dosu = osu.Where(y => y.SEQ_NO == d.SequenceNumber).FirstOrDefault();

                    if ((dosu.AMOUNT == d.Amount) && (dosu.SPENT_AMOUNT > dosu.AMOUNT))
                    {
                        noCashierPolicy.Add(v.SequenceNumber);
                    }
                }
            }
            return noCashierPolicy;
        }

        public override bool save(PVFormData formData, UserData userData, bool financeHeader)
        {
            bool Ok = true;
            today = DateTime.Now;

            base.save(formData, userData, financeHeader);
            /* Header */
            DbTransaction TX = null;
            using (ContextWrap eco = new ContextWrap())
            {
                ELVIS_DBEntities db = eco.db;

                try
                {
                    TX = db.Connection.BeginTransaction();

                    Ok = SaveData(db, formData, userData, financeHeader);

                    if (Ok)
                        TX.Commit();
                }
                catch (Exception ex)
                {
                    if (TX!=null)
                    TX.Rollback();
                    LoggingLogic.err(ex);
                    Ok = false;
                }
            }
            return Ok;
        }

        public bool SaveData(
            ELVIS_DBEntities DB,
            PVFormData formData, UserData userData,
            bool financeHeader)
        {
            bool Ok = false;
            int version = 1;
            int _no = formData.PVNumber ?? 0;

            Ok = saveHeader(formData, userData, financeHeader, out version, DB);

            logi = string.Format("{0}{1}", formData.PVNumber ?? 0, formData.PVYear ?? 0);
            Util.ReMoveFile(Path.Combine(LoggingLogic.GetPath(), logi + ".log"));

            if (!financeHeader && Ok)
            {
                if (formData.isSubmit)
                {
                    saveOri(formData, userData, DB);
                }
                bool doCashierPolicy = (formData.ReferenceNo == null || formData.ReferenceNo != 1);

                if (formData.PaymentMethodCode == "C" && formData.isSubmit && !financeHeader)
                {
                    int suNo = formData.SuspenseNumber ?? 0;
                    int suYe = formData.SuspenseYear ?? 0;

                    PVFormData sus = null;
                    if (formData.PVTypeCode == 3)
                        sus = search(suNo, suYe);

                    int maxSu = 0;
                    if (sus != null && sus.Details != null && sus.Details.Count > 0)
                        maxSu = sus.Details.Select(a => a.SequenceNumber).Max();

                    int maxSe = 0;
                    maxSe = GetSettlementMaxSeqNo(DB, suNo, suYe);

                    int maxSeq = Math.Max(maxSu, maxSe);

                    if (doCashierPolicy)
                        CashierPolicy(formData.Details
                            , true // 0 (zero) amount intact 
                            , formData.PVNumber ?? 0, formData.PVYear ?? 0, null, maxSeq, 1);

                    if (formData.PVTypeCode == 3)
                    {

                        if (sus != null && doCashierPolicy)
                        {
                            ReSequence(formData.Details, sus.Details);

                            // add settlement detail not found by cashier policy as 0 amont 
                            FillNonCashierPolicySettlement(formData.Details, sus.Details);
                        }

                        saveSettlementDetail(formData, userData, DB);
                    }
                }
                
                /* Detail */
                delExistingDetail(formData, userData, DB);

                saveDetail(formData, userData, version, DB);

                /* Amount */
                saveAmounts(formData, userData, DB);

                DB.SaveChanges();

            }

            return Ok;
        }

        public override Dictionary<string, decimal> GetSuspenseAmounts(int pvNo, int pvYear)
        {
           
            Dictionary<string, decimal> da = new Dictionary<string, decimal>();
            List<AmountData> d= new List<AmountData>();
            d = Qu<AmountData>("GetSuspenseAmountsByCurrency", pvNo, pvYear).ToList();
            if (d.Any())
            {
                foreach (var suma in d)
                {
                    da.Add(suma.CURRENCY_CD, suma.TOTAL_AMOUNT??0);
                }
            }

            return da;
        }

        public void syncBudgetNo(PVFormData f, List<PVFormDetail> lstDetail)
        {
            bool syncBudgetNo = (f.BudgetNumber != null) && (!f.BudgetNumber.Equals(""));
            List<string> lstCheckedWbsNo = new List<string>();
            foreach (PVFormDetail d in lstDetail)
            {
                if (!string.IsNullOrEmpty(d.WbsNumber))
                {
                    if (!lstCheckedWbsNo.Contains(d.WbsNumber))
                    {
                        syncBudgetNo = true;
                        break;
                    }
                }
            }
        }

        public static bool CanEdit(bool hasAccess, bool hasWorklist, int status,
                string role, bool onHold, int? vendorGroupCode,
                int pvType, bool hasAuthorizedCreatorRole, string budgetNo = "")
        {
            bool e = false;

            if (!hasAccess)
            {
                e = false;
            }
            else if (((pvType == 3) && (status < 20 && (status != 0 && hasAuthorizedCreatorRole))))
            {
                e = false;
            }
            else if (
                   ((new int[] { 0 }).Contains(status) || status == 40 || status == 42)
                  && hasAuthorizedCreatorRole
                    )
            {
                e = true;
            }
            else if ((status == 27) || (status == 28) || (status == 29))
            {
                e = false;
            }
            else if (hasWorklist)
            {
                e = true;
            }
            else
            {
                e = false;
            }
            return e;
        }

        public override string GetSummary(UserData u, string _imagePath, string template, int? num, int? year)
        {
            string tn, vCode, vName, wbs;

            int docNo = num ?? 0;
            int docYear = year ?? 0;

            var qH = (from h in db.TB_R_PV_H
                      where h.PV_NO == docNo && h.PV_YEAR == docYear
                      select h).FirstOrDefault();

            var qD = (from d in db.TB_R_PV_D
                      where d.PV_NO == docNo && d.PV_YEAR == docYear
                      select d);

            tn = (from tx in db.TB_M_TRANSACTION_TYPE
                  where tx.TRANSACTION_CD == qH.TRANSACTION_CD
                  select tx.TRANSACTION_NAME).FirstOrDefault();

            vCode = qH.VENDOR_CD;
            vCode = (vCode ?? "").Trim();
            vName = (from v in db.vw_Vendor
                     where v.VENDOR_CD == vCode
                     select v.VENDOR_NAME).FirstOrDefault();
            List<InvoiceSummaryData> _list = new List<InvoiceSummaryData>();

            wbs = qH.BUDGET_NO;
            int i = 0;
            foreach (TB_R_PV_D invD in qD)
            {
                bool hasInv = false;
                foreach (InvoiceSummaryData d in _list)
                {
                    if (d.INVOICE_NO == invD.INVOICE_NO)
                    {
                        hasInv = true;
                        decimal? a = null;
                        if (d.AMOUNT[invD.CURRENCY_CD] != null && invD.AMOUNT != null)
                        {
                            a = invD.AMOUNT + d.AMOUNT[invD.CURRENCY_CD];
                        }
                        else if (invD.AMOUNT != null)
                        {
                            a = invD.AMOUNT;
                        }
                        if (a != null)
                            d.AMOUNT[invD.CURRENCY_CD] = a;
                    }
                }
                if (!hasInv)
                    _list.Add(new InvoiceSummaryData(
                        ++i,
                        invD.INVOICE_NO,
                        invD.CURRENCY_CD,
                        invD.AMOUNT));
            }

            if (_list.Count < 1) return "";

            string html = File.ReadAllText(template);
            int j = _list.Count;
            Templet t = new Templet(html);
            Filler fh = t.Mark("COMPANY_NAME,TRANSACTION_NAME,CHECKER,PREPARER,VENDOR_CODE,VENDOR_NAME,WBS_NO");
            UserData su = Sup(u);
            string checker = (su.FIRST_NAME + " " + su.LAST_NAME).Trim();
            string preparer = (u.FIRST_NAME + " " + u.LAST_NAME).Trim();
            fh.Set(AppSetting.CompanyName, tn, checker, preparer, vCode, vName, wbs);

            Filler fd = t.Mark("rows", "SEQ,INVOICE_NO,IDR,USD,JPY");
            i = 0;
            while (i < j)
            {
                var dx = _list[i];
                fd.Add(
                    dx.SEQ,
                    dx.INVOICE_NO,
                    String.Format("{0:N0}", dx.AMOUNT["IDR"]),
                    String.Format("{0:N2}", dx.AMOUNT["USD"]),
                    String.Format("{0:N0}", dx.AMOUNT["JPY"]));
                i++;
            }

            // save to file 
            return t.Get();
        }

        public override string WriteSummary(string imagePath, string outputPath,
                              UserData _UserData,
                              string contentTemp)
        {
            Document doc = new Document(PageSize.A4, 40, 40, 40, 40);
            String _FileName = "Invoice_Summary_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
            System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;
            String username = String.Format("{0} {1}", _UserData.FIRST_NAME, _UserData.LAST_NAME);
            username = myTI.ToTitleCase(username.ToLower());
            HTMLWorker hw = new HTMLWorker(doc);
            MemoryStream ms = new MemoryStream();
            PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);
            PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

            Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(AppSetting.CompanyLogo)));
            StyleSheet sh = new StyleSheet();

            doc.Open();
            PdfContentByte cb = PDFWriter.DirectContent;

            string[] contentPages = contentTemp.Split(new string[] { "<new/>" }, StringSplitOptions.None);
            int pageNum = 0;

            foreach (string content in contentPages)
            {
                if (string.IsNullOrEmpty(content)) continue;
                if (pageNum > 0)
                    doc.NewPage();

                img.SetAbsolutePosition(40, 750);
                doc.Add(img);

                List<IElement> elements = HTMLWorker.ParseToList(new StringReader(content), sh);
                Int16 ind = 0;
                // logo
                PdfPTable table = elements[ind++] as PdfPTable;

                table.SetWidths(new float[] { 3f, 17f });
                doc.Add(table);
                // title
                Paragraph par = elements[ind++] as Paragraph;
                par.SpacingBefore = 3f;
                par.SpacingAfter = 17f;
                doc.Add(par);
                //Checked by prepared by
                PdfPTable t1 = new PdfPTable(new float[] { 2f, 3f });
                t1.WidthPercentage = 100;

                table = elements[ind++] as PdfPTable;
                table.WidthPercentage = 100;
                table.SetWidths(new float[] { 4f, 4f });
                PdfPCell[] cc = table.Rows[0].GetCells();
                cc[0].PaddingTop = 0;
                cc[1].PaddingTop = 0;
                cc = table.Rows[1].GetCells();
                cc[0].MinimumHeight = 50;
                cc[1].MinimumHeight = 50;
                cc = table.Rows[2].GetCells();
                cc[0].MinimumHeight = 20;
                cc[1].MinimumHeight = 20;

                t1.DefaultCell.Border = 0;
                t1.AddCell(new Paragraph(""));
                t1.DefaultCell.FixedHeight = 72;
                t1.AddCell(new PdfPCell(table));

                doc.Add(t1);

                par = new Paragraph("\n");
                par.SpacingBefore = 1f;
                par.SpacingAfter = 1f;
                doc.Add(par);

                // invoice header
                table = null;
                while (ind < elements.Count && table == null)
                {
                    table = elements[ind++] as PdfPTable;

                }
                if (table == null) break;
                table.SetWidths(new float[] { 5f, 4f, 8f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();

                    foreach (var cell in cells)
                    {
                        if (cell != null)
                        {
                            cell.PaddingTop = -3;
                        }
                    }
                }
                doc.Add(table);

                doc.Add(new Paragraph("\n"));
                // invoice detail

                table = null;
                while (ind < elements.Count && table == null)
                {
                    table = elements[ind++] as PdfPTable;
                }
                if (table == null) break;
                //table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 1f, 3f, 2f, 2f, 2f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();
                    for (int j = 0; j < cells.Length; j++)
                    {
                        PdfPCell cell = cells[j];
                        if (cell != null)
                        {
                            cell.PaddingTop = -3;
                            cell.PaddingRight = 10;
                            cell.PaddingLeft = 0;
                        }
                    }
                }

                doc.Add(table);

                pageNum++;
            }
            doc.Close();

            string outfile = Path.Combine(outputPath, _FileName);
            File.WriteAllBytes(outfile, ms.ToArray());
            return _FileName;
        }

        #region Simulation

        /*** Added by Akhmad Nuryanto, 14/09/2012
         *** Simulation
         ***
         */
        public override List<PVFormSimulationData> GetSimulationData(String docNo, String docYear)
        {
            List<PVFormSimulationData> simulationData = new List<PVFormSimulationData>();
            int _no = docNo.Int();
            int _yy = docYear.Int();
            Decimal totalAmount = 0;
            String amuntTemp = "";

            using (ContextWrap co = new ContextWrap())
            {
                ELVIS_DBEntities DB = co.db;

                var q = (from p in DB.vw_PvPostSimulation_H
                         where p.PV_NO == _no
                         && p.PV_YEAR == _yy
                         select p);

                //Rinda Rahayu 20160412
                foreach (var dataHeader in q)
                {
                    totalAmount += dataHeader.AMOUNT;
                }
                //End Rinda Rahayu 20160412

                if (q.Any())
                {
                    foreach (var data in q)
                    {
                        PVFormSimulationData _h =
                            new PVFormSimulationData()
                            {
                                KEYS = data.PV_NO + "||" + data.PV_YEAR + "||" + data.CURRENCY_CD + "||" + data.FROM_CURR,
                                PV_NO = data.PV_NO.str(),
                                PV_YEAR = data.PV_YEAR.str(),
                                PV_DATE = data.PV_DATE,
                                PV_TYPE_CD = data.PV_TYPE_CD ?? 0,
                                TRANSACTION_CODE = data.TRANSACTION_CD,
                                TRANSACTION_NAME = data.TRANSACTION_NAME,
                                COMPANY_CODE = data.COMPANY_CD,
                                POSTING_DATE = data.POSTING_DATE,
                                FISCAL_YEAR = data.FISCAL_YEAR.ToString(),
                                PERIOD = data.PERIOD,
                                REF_DOC = data.REF_DOC,
                                WITHOLDING_TAX = data.WITHOLDING_TAX,
                                CURRENCY_CODE = data.CURRENCY_CD,
                                VENDOR_CD = data.VENDOR_CD,

                                VENDOR_NAME = data.VENDOR_NAME,
                                TAX_CD = data.TAX_CD,
                                TAX_VALUE = data.TAX_VALUE,
                                WITHOLDING_TAX_VALUE = data.WITHOLDING_TAX_VALUE,
                                SERVICE_FLAG = data.SERVICE_FLAG.ToString(),
                                TOTAL_AMOUNT_IDR = data.TOTAL_AMOUNT_IDR + data.TAX_VALUE - data.WITHOLDING_TAX_VALUE
                            };


                        var det = (from p in DB.vw_PVPostSimulation_D
                                   where p.PV_NO == data.PV_NO && p.PV_YEAR == data.PV_YEAR
                                   && p.CURRENCY_CD == data.CURRENCY_CD
                                   && p.FROM_CURR == data.FROM_CURR
                                   select p);

                        List<PVFormSimulationDetail> _details = null;
                        Decimal totalAmountDetail = 0;
                        int i = 2;
                        _details = new List<PVFormSimulationDetail>();
                        switch (_h.PV_TYPE_CD)
                        {
                            case 1: // direct 
                                if (det.Any())
                                {

                                    // expense 
                                    foreach (var dataDetail in det)
                                    {
                                        if (string.IsNullOrEmpty(_h.STD_WORDING))
                                            _h.STD_WORDING = dataDetail.STD_WORDING;

                                        totalAmountDetail += dataDetail.AMOUNT;
                                        _details.Add(new PVFormSimulationDetail()
                                        {
                                            SEQ_NO = i,
                                            ACCOUNT = Convert.ToString(dataDetail.GL_ACCOUNT),
                                            ACCOUNT_SHORT = dataDetail.GL_ACCOUNT_NAME,
                                            TX = dataDetail.TAX_CD,
                                            //AMOUNT = dataDetail.AMOUNT_IDR.ToString(),
                                            //AMOUNT = string.Format("{0:#,#.##}", dataDetail.AMOUNT) + " ",
                                            AMOUNT = string.Format("{0:#,#}", dataDetail.AMOUNT) + " ",
                                            TEXT = cat(dataDetail.STD_WORDING, dataDetail.DESCRIPTION),
                                            // fid.Hadid 20180608
                                            WBS_NO = dataDetail.WBS_NO,
                                            COST_CENTER_CD = dataDetail.COST_CENTER_CD,
                                            AMOUNT_DEC = dataDetail.AMOUNT,
                                            dataMode = DataSAPMode.GL
                                        });

                                        i++;
                                    }
                                }

                                if (!string.IsNullOrEmpty(data.WITHOLDING_TAX))
                                {
                                    //write witholding record in detail here

                                    _details.Add(new PVFormSimulationDetail()
                                    {
                                        SEQ_NO = i,
                                        ACCOUNT = data.ACCOUNT_WITHOLDING_TAX,
                                        ACCOUNT_SHORT = data.ACCOUNT_WITHOLDING_TAX_DESC,
                                        TX = "",
                                        //AMOUNT = data.WITHOLDING_TAX_VALUE.ToString() + "-",
                                        AMOUNT = string.Format("{0:#,#.##}", data.WITHOLDING_TAX_VALUE) + "-",
                                        TEXT = "",
                                        // fid.Hadid 20180608
                                        WBS_NO = "",
                                        COST_CENTER_CD = "",
                                        AMOUNT_DEC = data.WITHOLDING_TAX_VALUE ?? 0,
                                        dataMode = DataSAPMode.GL
                                    });

                                    i++;
                                }

                                _details.Insert(0, new PVFormSimulationDetail()
                                {
                                    SEQ_NO = 1,
                                    ACCOUNT = _h.VENDOR_CD,
                                    ACCOUNT_SHORT = _h.VENDOR_NAME,
                                    TX = _h.TAX_CD,
                                    //AMOUNT = _PVFormSimulationData.TOTAL_AMOUNT_IDR.ToString() + "-",
                                    // AMOUNT = string.Format("{0:#,#.##}", data.AMOUNT) + "-",
                                    //AMOUNT = string.Format("{0:#,#.##}", totalAmountDetail) + "-", // Rinda Rahayu 20160324
                                    AMOUNT = string.Format("{0:#,#}", totalAmountDetail) + "-", // Rinda Rahayu 20160509
                                    TEXT = _h.STD_WORDING,
                                    // fid.Hadid 20180608
                                    AMOUNT_DEC = totalAmountDetail * -1,
                                    dataMode = DataSAPMode.Vendor

                                });

                                break;

                            case 2: // suspense 
                                if (data.AMOUNT != 0)
                                    // Start Rinda Rahayu 20160512
                                    if (data.CURRENCY_CD.Equals("IDR"))
                                    {
                                        amuntTemp = string.Format("{0:#,#}", data.AMOUNT);
                                    }
                                    else
                                    {
                                        amuntTemp = string.Format("{0:#,#.##}", data.AMOUNT);
                                    }
                                // End Rinda Rahayu 20160512

                                _details.Insert(0, new PVFormSimulationDetail() // 
                                {
                                    SEQ_NO = 1,
                                    ACCOUNT = _h.VENDOR_CD,
                                    ACCOUNT_SHORT = _h.VENDOR_NAME,
                                    TX = _h.TAX_CD,

                                    //AMOUNT = string.Format("{0:#,#.##}", data.AMOUNT) + "-",                                            
                                    //AMOUNT = string.Format("{0:#,#}", data.AMOUNT) + "-", // Rinda Rahayu 20160509
                                    AMOUNT = amuntTemp + "-", // Rinda Rahayu 20160512

                                    TEXT = _h.STD_WORDING,
                                    // fid.Hadid 20180608
                                    AMOUNT_DEC = data.AMOUNT * -1,
                                    dataMode = DataSAPMode.Vendor
                                });

                                break;

                            case 3: // settlement 

                                if (data.AMOUNT == 0)
                                {

                                    // Start Rinda Rahayu 20160512
                                    if (data.CURRENCY_CD.Equals("IDR"))
                                    {
                                        amuntTemp = string.Format("{0:#,#}", data.AMOUNT);
                                    }
                                    else
                                    {
                                        amuntTemp = string.Format("{0:#,#.##}", data.AMOUNT);
                                    }
                                    // End Rinda Rahayu 20160512

                                    // VENDOR -
                                    _details.Insert(0, new PVFormSimulationDetail()
                                    {
                                        SEQ_NO = 1,
                                        ACCOUNT = _h.VENDOR_CD,
                                        ACCOUNT_SHORT = _h.VENDOR_NAME,
                                        TX = _h.TAX_CD,

                                        //AMOUNT = string.Format("{0:#,#.##}", _h.TOTAL_AMOUNT_IDR) + "-",
                                        //AMOUNT = string.Format("{0:#,#}", _h.TOTAL_AMOUNT_IDR) + "-", // Rinda Rahayu 20160509
                                        AMOUNT = amuntTemp + "-", // Rinda Rahayu 20160512

                                        TEXT = _h.STD_WORDING,
                                        // fid.Hadid 20180608
                                        dataMode = DataSAPMode.Vendor
                                    });

                                    if (det.Any())
                                    {
                                        // EXPENSE +
                                        foreach (var dataDetail in det)
                                        {
                                            if (string.IsNullOrEmpty(_h.STD_WORDING))
                                                _h.STD_WORDING = dataDetail.STD_WORDING;

                                            totalAmountDetail += dataDetail.AMOUNT;
                                            _details.Add(new PVFormSimulationDetail()
                                            {
                                                SEQ_NO = i,
                                                ACCOUNT = Convert.ToString(dataDetail.GL_ACCOUNT),
                                                ACCOUNT_SHORT = dataDetail.DESCRIPTION,
                                                TX = dataDetail.TAX_CD,

                                                //AMOUNT = string.Format("{0:#,#.##}", dataDetail.AMOUNT) + " ",
                                                AMOUNT = string.Format("{0:#,#}", dataDetail.AMOUNT) + " ", // Rinda Rahayu 20160509
                                                TEXT = cat(dataDetail.STD_WORDING, dataDetail.DESCRIPTION),
                                                // fid.Hadid 20180608
                                                dataMode = DataSAPMode.GL
                                            });

                                            i++;
                                        }
                                    }

                                }
                                else if (data.AMOUNT > 0)
                                {
                                    i = 1;
                                    if (det.Any())
                                    {

                                        foreach (var dataDetail in det)
                                        {
                                            if (string.IsNullOrEmpty(_h.STD_WORDING))
                                                _h.STD_WORDING = dataDetail.STD_WORDING;
                                            // expense 
                                            totalAmountDetail += dataDetail.AMOUNT;
                                            _details.Add(new PVFormSimulationDetail()
                                            {
                                                SEQ_NO = i++,
                                                ACCOUNT = Convert.ToString(dataDetail.GL_ACCOUNT),
                                                ACCOUNT_SHORT = dataDetail.GL_ACCOUNT_NAME,
                                                TX = dataDetail.TAX_CD,

                                                //AMOUNT = string.Format("{0:#,#.##}", dataDetail.SPENT_AMOUNT) + " ",
                                                AMOUNT = string.Format("{0:#,#}", dataDetail.SPENT_AMOUNT) + " ", // Rinda Rahayu 20160509
                                                TEXT = dataDetail.STD_WORDING,
                                                // fid.Hadid 20180608
                                                dataMode = DataSAPMode.GL
                                            });
                                            // VENDOR -
                                            _details.Add(new PVFormSimulationDetail()
                                            {
                                                SEQ_NO = i++,
                                                ACCOUNT = _h.VENDOR_CD,
                                                ACCOUNT_SHORT = _h.VENDOR_NAME,
                                                TX = _h.TAX_CD,

                                                //AMOUNT = string.Format("{0:#,#.##}", (dataDetail.SPENT_AMOUNT ?? 0) - dataDetail.AMOUNT) + "-",
                                                AMOUNT = string.Format("{0:#,#}", (dataDetail.SPENT_AMOUNT ?? 0) - dataDetail.AMOUNT) + "-", // Rinda Rahayu 20160509

                                                TEXT = _h.STD_WORDING,
                                                // fid.Hadid 20180608
                                                dataMode = DataSAPMode.Vendor
                                            });
                                            // VENDOR -
                                            _details.Add(new PVFormSimulationDetail()
                                            {
                                                SEQ_NO = i++,
                                                ACCOUNT = _h.VENDOR_CD,
                                                ACCOUNT_SHORT = _h.VENDOR_NAME,
                                                TX = _h.TAX_CD,

                                                // AMOUNT = string.Format("{0:#,#.##}", dataDetail.AMOUNT) + "-",
                                                AMOUNT = string.Format("{0:#,#}", dataDetail.AMOUNT) + "-", // Rinda Rahayu 20160509

                                                TEXT = _h.STD_WORDING,
                                                // fid.Hadid 20180608
                                                dataMode = DataSAPMode.Vendor
                                            });

                                        }
                                    }


                                }
                                else if (data.AMOUNT < 0) // rv 
                                {
                                    i = 1;
                                    if (det.Any())
                                    {
                                        // expense 
                                        foreach (var dataDetail in det)
                                        {
                                            if (string.IsNullOrEmpty(_h.STD_WORDING))
                                                _h.STD_WORDING = dataDetail.STD_WORDING;

                                            totalAmountDetail += dataDetail.AMOUNT;
                                            // EXPENSE + 
                                            _details.Add(new PVFormSimulationDetail()
                                            {
                                                SEQ_NO = i++,
                                                ACCOUNT = Convert.ToString(dataDetail.GL_ACCOUNT),
                                                ACCOUNT_SHORT = dataDetail.GL_ACCOUNT_NAME,
                                                TX = dataDetail.TAX_CD,

                                                //AMOUNT = string.Format("{0:#,#.##}", dataDetail.SPENT_AMOUNT) + " ",
                                                AMOUNT = string.Format("{0:#,#}", dataDetail.SPENT_AMOUNT) + " ",
                                                TEXT = dataDetail.STD_WORDING
                                            });
                                            // VENDOR -
                                            _details.Add(new PVFormSimulationDetail()
                                            {
                                                SEQ_NO = i++,
                                                ACCOUNT = _h.VENDOR_CD,
                                                ACCOUNT_SHORT = _h.VENDOR_NAME,
                                                TX = _h.TAX_CD,

                                                //AMOUNT = string.Format("{0:#,#.##}", dataDetail.AMOUNT) + "-",
                                                AMOUNT = string.Format("{0:#,#}", dataDetail.AMOUNT) + "-",

                                                TEXT = _h.STD_WORDING
                                            });

                                            // CLEARING +
                                            _details.Add(new PVFormSimulationDetail()
                                            {
                                                SEQ_NO = i++,
                                                ACCOUNT = "1154910",
                                                ACCOUNT_SHORT = "CLEARING", // _h.VENDOR_NAME,
                                                TX = _h.TAX_CD,

                                                //AMOUNT = string.Format("{0:#,#.##}", (dataDetail.SPENT_AMOUNT ?? 0) - dataDetail.AMOUNT) + " ",
                                                AMOUNT = string.Format("{0:#,#}", (dataDetail.SPENT_AMOUNT ?? 0) - dataDetail.AMOUNT) + " ",

                                                TEXT = _h.STD_WORDING
                                            });

                                        }
                                    }
                                }


                                break;
                            default:
                                break;
                        }

                        _h.Details = _details;

                        if (data.SERVICE_FLAG != null && data.SERVICE_FLAG.Equals("1") && data.WITHOLDING_TAX == null)
                        {
                            _h.WARNING = noWitholding;
                        }

                        // if (totalAmountDetail != data.AMOUNT && _h.PV_TYPE_CD != 2)
                        if (totalAmountDetail != totalAmount && _h.PV_TYPE_CD != 2) // Rinda Rahayu 20160412
                        {
                            _h.WARNING = notBalance;
                        }

                        if ((data.COUNTER_FLAG ?? 0) == 0)
                            _h.WARNING = notVerified;

                        simulationData.Add(_h);
                    }
                }
            }
            return simulationData;
        }

        public override bool Reverse(string uid, PVFormData f)
        {
            List<PVReverseInput> ri = new List<PVReverseInput>();
            string _no = f.PVNumber.str();

            string _year = f.PVYear.str();
            int no = f.PVNumber ?? 0;
            int yy = f.PVYear ?? 0;
            int fiscalYear = logic.Sys.FiscalYear(f.PVDate ?? new DateTime(StrTo.NULL_YEAR, 1, 1));
            var q = (from ps in db.TB_R_SAP_DOC_NO
                     where ps.DOC_NO == no
                        && ps.DOC_YEAR == yy
                     select ps);
            if (q.Any())
            {
                DateTime? postingDate = (from pv in db.TB_R_PV_H
                                         where pv.PV_NO == no
                                            && pv.PV_YEAR == yy
                                         select pv.POSTING_DATE)
                                        .FirstOrDefault();
                foreach (var d in q)
                {
                    ri.Add(new PVReverseInput()
                    {
                        PV_NO = d.DOC_NO.str(),
                        //PV_YEAR = d.DOC_YEAR.str(),
                        PV_YEAR = fiscalYear.str(),
                        ITEM_NO = d.ITEM_NO.str(),
                        POSTING_DT = postingDate.SAPdate(),
                        SAP_DOC_NO = d.SAP_DOC_NO.str(),
                        SAP_DOC_YEAR = d.SAP_DOC_YEAR.str()
                    });
                }
            }
            if (ri.Count > 0)
            {
                List<PVPostSAPResult> o = logic.SAP.ReversePV(ref ri, uid);

                int successes = 0;
                foreach (PVReverseInput rix in ri)
                {
                    if (rix.STATUS == "S")
                    {
                        successes++;
                    }
                }


                if (successes > 0) // any success Reverse through SAP deletes whole PV
                {
                    List<PVFormData> l = new List<PVFormData>();
                    UpdateReverseDoc(o);
                    l.Add(f);
                    logic.PVList.Delete(l, uid);

                }
                return successes == ri.Count;
            }
            else
                return false;
        }

        public bool UpdateReverseDoc(List<PVPostSAPResult> l)
        {

            foreach (PVPostSAPResult a in l)
            {
                int _no = a.PV_NO.Int();

                int _yy = db.TB_R_PV_D.Where(pv => pv.PV_NO == _no).Select(pv => pv.PV_YEAR).FirstOrDefault();

                if (_yy == 0)
                    _yy = a.PV_YEAR.Int();

                int _i = a.ITEM_NO.Int();

                var q = db.TB_R_SAP_DOC_NO
                        .Where(p => p.DOC_NO == _no && p.DOC_YEAR == _yy && p.ITEM_NO == _i)
                        .FirstOrDefault();
                if (q != null)
                {
                    q.SAP_CLEARING_DOC_NO = a.SAP_CLEARING_DOC_NO;
                    q.SAP_CLEARING_DOC_YEAR = a.SAP_CLEARING_DOC_YEAR.Int();
                    db.SaveChanges();
                }
                else
                {
                    var s = new TB_R_SAP_DOC_NO()
                    {
                        DOC_NO = _no,
                        DOC_YEAR = _yy,
                        ITEM_NO = _i,
                        SAP_CLEARING_DOC_NO = a.SAP_CLEARING_DOC_NO,
                        SAP_CLEARING_DOC_YEAR = a.SAP_CLEARING_DOC_YEAR.Int()
                    };
                    db.AddToTB_R_SAP_DOC_NO(s);
                    db.SaveChanges();
                }
            }
            return true;

        }
        public override List<BudgetCheckInput> GetBudgetNumberHistory(int _no, int _year, string BudgetNo)
        {
            List<BudgetCheckInput> r = new List<BudgetCheckInput>();
            var q = (from h in db.TB_H_PV_H
                     where h.PV_NO == _no && h.PV_YEAR == _year && h.BUDGET_NO != BudgetNo

                     select h.BUDGET_NO).Distinct();
            if (q.Any())
            {
                List<string> bus = q.ToList();
                foreach (string x in bus)
                {
                    var qx = (from h in db.TB_H_PV_H
                              where h.PV_NO == _no && h.PV_YEAR == _year && h.BUDGET_NO == x
                              orderby h.VERSION descending
                              select h).FirstOrDefault();
                    if (qx != null)
                    {
                        int yy = logic.Sys.FiscalYear(qx.PV_DATE);
                        r.Add(new BudgetCheckInput()
                        {
                            DOC_NO = qx.PV_NO,
                            DOC_YEAR = yy,
                            WBS_NO = qx.BUDGET_NO,
                            AMOUNT = qx.TOTAL_AMOUNT ?? 0
                        });
                    }
                }
            }
            return r;
        }
        #endregion

        #region Accrued
        public override void updateBalanceOutstanding(PVFormData pv, UserData user, bool isSubmit)
        {
            try
            {
                var bal = (from b in db.TB_R_ACCR_BALANCE
                           where b.BOOKING_NO == pv.BookingNo
                           select b).FirstOrDefault();

                int mult = isSubmit ? 1 : -1;
                if (bal != null)
                {
                    decimal totAmt = 0;
                    if (pv.PVTypeCode == 3)
                        totAmt = bal.INIT_AMT.HasValue ? bal.INIT_AMT.Value - pv.TotalAmount : 0;
                    else if (pv.PVTypeCode == 1)
                        totAmt = pv.TotalAmount; 

                    bal.OUTSTANDING_AMT = bal.OUTSTANDING_AMT + (totAmt * mult);
                    bal.CHANGED_BY = user.USERNAME;
                    bal.CHANGED_DT = DateTime.Now;

                    db.SaveChanges();
                }
            }
            catch(Exception e)
            {
                Logic.Log.Log("MSTD00002ERR", e.InnerException != null ? e.InnerException.Message : e.Message, "updateBalanceOutstanding");
                Handle(e);
            }
        }
        public override void updateBalanceVoucherPosting(PVFormData pv, UserData user)
        {
            try
            {
                var bal = (from b in db.TB_R_ACCR_BALANCE
                           where b.BOOKING_NO == pv.BookingNo
                           select b).FirstOrDefault();

                if (bal != null)
                {
                    bal.SPENT_AMT += pv.TotalAmount;
                    bal.AVAILABLE_AMT -= pv.TotalAmount;
                    bal.OUTSTANDING_AMT -= pv.TotalAmount;
                    bal.CHANGED_BY = user.USERNAME;
                    bal.CHANGED_DT = DateTime.Now;

                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logic.Log.Log("MSTD00002ERR", e.InnerException != null ? e.InnerException.Message : e.Message, "updateBalanceVoucherPosting");
                Handle(e);
            }
        }
        #endregion
    }
}