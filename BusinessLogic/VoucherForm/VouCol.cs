﻿namespace BusinessLogic.VoucherForm
{
    public static class VouCol
    {
        public static readonly string DELETION_CONTROL = "DeletionControl";
        public static readonly string SEQUENCE_NUMBER = "DisplaySequenceNumber";
        public static readonly string COST_CENTER_CODE = "CostCenterCode";
        public static readonly string COST_CENTER_NAME = "CostCenterName";
        public static readonly string DESCRIPTION = "Description";
        public static readonly string DESCRIPTION_DEFAULT_WORDING = "StandardDescriptionWording";
        public static readonly string CURRENCY_CODE = "CurrencyCode";
        public static readonly string AMOUNT = "Amount";
        public static readonly string INVOICE_NUMBER = "InvoiceNumber";
        public static readonly string TAX_CODE = "TaxCode";
        public static readonly string TAX_NUMBER = "TaxNumber";

        public static readonly string[] disabledFields = new string[]
        {
            DESCRIPTION
            , CURRENCY_CODE
            , AMOUNT 
            , INVOICE_NUMBER 
            , TAX_CODE 
            , TAX_NUMBER 
        };
    }
}
