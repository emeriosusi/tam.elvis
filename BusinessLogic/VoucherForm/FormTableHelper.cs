﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Function;

namespace BusinessLogic.VoucherForm
{
    public static class FormTableHelper
    {

        public static int EmptyCostCenter(this PVFormTable t)
        {
            if (t == null || t.DataList == null || t.DataList.Count < 1)
                return 0;
            foreach (PVFormDetail d in t.DataList)
            {
                d.CostCenterCode = "";
                d.CostCenterName = "";
            }
            return 0;
        }

        public static int InvoiceCount(this PVFormTable t)
        {
            return (from a in t.DataList
                    select a.InvoiceNumber).Distinct()
                    .Where(x => !x.isEmpty())
                    .Count();
        }

        public static Dictionary<string, decimal> AmountByCurrency(this PVFormTable t)
        {
            return AmountByCurrency(t.DataList);
        }

        public static Dictionary<string, decimal> AmountByCurrency(List<PVFormDetail> data)
        {
            Dictionary<string, decimal> a = new Dictionary<string, decimal>();
            foreach (PVFormDetail d in data)
            {
                if (!d.CurrencyCode.isEmpty())
                {
                    if (a.ContainsKey(d.CurrencyCode))
                    {
                        decimal v = a[d.CurrencyCode];
                        v = v + d.Amount;
                        a[d.CurrencyCode] = v;
                    }
                    else
                    {
                        a.Add(d.CurrencyCode, d.Amount);
                    }
                }
            }
            return a;
        }


        public static OrderedDictionary TotalAmountMap(this PVFormTable t)
        {
            List<PVFormDetail> lstData = t.DataList;

            OrderedDictionary map = new OrderedDictionary();
            object objAmount;
            decimal totalAmount = 0;
            foreach (PVFormDetail d in lstData)
            {
                if (!string.IsNullOrEmpty(d.CurrencyCode))
                {
                    objAmount = map[d.CurrencyCode];
                    if (objAmount == null)
                    {
                        totalAmount = 0;
                    }
                    else
                    {
                        totalAmount = objAmount.Dec();
                    }

                    map[d.CurrencyCode] = totalAmount + d.Amount;
                }
            }

            return map;
        }

        // Start Rinda Rahayu 20160406
        public static OrderedDictionary TotalAmountMapToBudget(this PVFormTable t)
        {
            List<PVFormDetail> lstData = t.DataList;

            OrderedDictionary map = new OrderedDictionary();
            object objAmount;
            decimal totalAmount = 0;
            String glAccount = "";
            foreach (PVFormDetail d in lstData)
            {
                if (!string.IsNullOrEmpty(d.CurrencyCode))
                {
                    objAmount = map[d.CurrencyCode];
                    if (objAmount == null)
                    {
                        totalAmount = 0;
                    }
                    else
                    {
                        totalAmount = objAmount.Dec();
                    }

                    if (totalAmount == 0)
                    {
                        map[d.CurrencyCode] = totalAmount + d.Amount;
                        glAccount = d.GlAccount;
                    }
                    else if (glAccount.Equals(d.GlAccount))
                    {
                        map[d.CurrencyCode] = totalAmount + d.Amount;
                    }


                }
            }

            return map;
        }
        // End Rinda Rahayu 20160406

        public static void addNewRow(this PVFormTable t, bool taxCalculated)
        {
            int seqNumber = t.DataList.Count + 1; /// this line causes error when user delete line 1 and then addNewRow
            seqNumber = t.NextSeq();
            PVFormDetail data = new PVFormDetail()
            {
                Persisted = false,
                CostCenterCode = "",
                Amount = 0,
                CostCenterName = "",
                CurrencyCode = "",
                Description = "",
                StandardDescriptionWording = t.StandardWording,
                InvoiceNumber = "",
                SequenceNumber = seqNumber,
                DisplaySequenceNumber = seqNumber,
                ItemTransaction = 1,
                TaxCode = taxCalculated ? "V1" : "V0" // to be replaced with app variable
            };
            if (t.DataList.Count >= 1)
            {
                PVFormDetail firstData = t.DataList[0];
                data.CostCenterCode = firstData.CostCenterCode;
                data.CostCenterName = firstData.CostCenterName;
                data.GlAccount = firstData.GlAccount;
            }
            t.DataList.Add(data);
        }

        public static void CancelNonPersistedData(this PVFormTable t)
        {
            List<PVFormDetail> lstDeleted = new List<PVFormDetail>();
            foreach (PVFormDetail d in t.DataList)
            {
                if (!d.Persisted)
                {
                    lstDeleted.Add(d);
                }
            }

            foreach (PVFormDetail d in lstDeleted)
            {
                t.DataList.Remove(d);
            }
        }

        public static int NextSeq(this PVFormTable t)
        {
            if (t.DataList.Count < 1)
                return 1;
            else
            {
                int maxSeq = t.DataList.Count + 1;
                for (int i = 0; i < t.DataList.Count; i++)
                {
                    if (t.DataList[i].SequenceNumber > maxSeq)
                    {
                        maxSeq = t.DataList[i].SequenceNumber + 1;
                    }
                }
                return maxSeq;
            }
        }

        public static List<PVFormDetail> sanityCheck(this PVFormTable t, bool isEditing = false)
        {
            List<PVFormDetail> dataList = t.DataList;
            List<PVFormDetail> cleanedDataList = new List<PVFormDetail>();

            int cntDataList = dataList.Count;
            PVFormDetail d;
            for (int i = 0; i < cntDataList; i++)
            {
                d = dataList[i];
                d.CostCenterCode = d.CostCenterCode != null ? d.CostCenterCode : "";
                d.CostCenterName = d.CostCenterName != null ? d.CostCenterName : "";
                d.CurrencyCode = d.CurrencyCode != null ? d.CurrencyCode : "";
                d.Description = d.Description != null ? d.Description : "";
                d.StandardDescriptionWording = d.StandardDescriptionWording != null ? d.StandardDescriptionWording : "";
                d.InvoiceNumber = d.InvoiceNumber != null ? d.InvoiceNumber : "";

                if (!d.CurrencyCode.Trim().Equals("")
                    //&& d.Amount != 0 // 20121212 dan - for settlement 
                    || isEditing)
                {
                    cleanedDataList.Add(d);
                }
            }

            return cleanedDataList;
        }

        public static bool hasEmptyDescription(this PVFormTable t)
        {
            int c = t.DataList.Where(d => d.Description.isEmpty()).Count();
            return c > 0;
        }

        public static decimal GetTotalAmount(this PVFormData f)
        {
            int docNo = f.PVNumber ?? 0;
            int docYear = f.PVYear ?? 0;
            OrderedDictionary mapTotalAmount = f.FormTable.TotalAmountMap();
            List<ExchangeRate> lstExchangeRate = null;
            LogicFactory lo = LogicFactory.Get();
            if (docNo > 0 && docYear > 0)
            {
                lstExchangeRate = lo.Persist.getExchangeRatesPerNo(docNo, docYear);
            }
            if (lstExchangeRate == null || lstExchangeRate.Count < 1)
            {
                lstExchangeRate = lo.Persist.getExchangeRates();
            }

            decimal totalIDR = 0;
            decimal amount;

            foreach (string key in mapTotalAmount.Keys)
            {
                amount = (decimal)mapTotalAmount[key];
                decimal Rate = 1;

                if (key.ToUpper().Equals("IDR"))
                {
                    totalIDR += amount;
                }
                else
                {
                    var rex = lstExchangeRate
                        .Where(a => a.CurrencyCode == key)
                        .Select(x => x.Rate);

                    if (rex.Any())
                    {
                        Rate = rex.FirstOrDefault();
                    }
                    else
                    {
                        List<ExchangeRate> lstExRate = lo.Persist.getExchangeRates();

                        lstExchangeRate= lstExchangeRate.Concat(
                            lstExRate.Where(y=> !lstExchangeRate.Any(x=> x.CurrencyCode == y.CurrencyCode))
                            ).ToList();
                        Rate = lstExchangeRate.Where(z => z.CurrencyCode == key).Select(a => a.Rate).FirstOrDefault();
                    }
                    totalIDR += amount * Rate;
                }
            }

            return totalIDR;
        }

        // Start Rinda Rahayu 20160406
        public static decimal GetTotalAmountToBudget(this PVFormData f)
        {
            int docNo = f.PVNumber ?? 0;
            int docYear = f.PVYear ?? 0;
            OrderedDictionary mapTotalAmount = f.FormTable.TotalAmountMapToBudget();
            List<ExchangeRate> lstExchangeRate = null;
            LogicFactory lo = LogicFactory.Get();
            if (docNo > 0 && docYear > 0)
            {
                lstExchangeRate = lo.Persist.getExchangeRatesPerNo(docNo, docYear);
            }
            if (lstExchangeRate == null || lstExchangeRate.Count < 1)
            {
                lstExchangeRate = lo.Persist.getExchangeRates();
            }

            decimal totalIDR = 0;
            decimal amount;

            foreach (string key in mapTotalAmount.Keys)
            {
                amount = (decimal)mapTotalAmount[key];
                decimal Rate = 1;

                if (key.ToUpper().Equals("IDR"))
                {
                    totalIDR += amount;
                }
                else
                {
                    var rex = lstExchangeRate
                        .Where(a => a.CurrencyCode == key)
                        .Select(x => x.Rate);

                    if (rex.Any())
                    {
                        Rate = rex.FirstOrDefault();
                    }
                    else
                    {
                        List<ExchangeRate> lstExRate = lo.Persist.getExchangeRates();

                        lstExchangeRate = lstExchangeRate.Concat(
                            lstExRate.Where(y => !lstExchangeRate.Any(x => x.CurrencyCode == y.CurrencyCode))
                            ).ToList();
                        Rate = lstExchangeRate.Where(z => z.CurrencyCode == key).Select(a => a.Rate).FirstOrDefault();
                    }
                    totalIDR += amount * Rate;
                }
            }

            return totalIDR;
        }
        // End Rinda Rahayu 20160406


        public static int EmptyCostCenterWithBannedGLAccount(this PVFormData Data, int[] GlAccounts)
        {
            int r = 0;
            LogicFactory lo = LogicFactory.Get();
            for (int i = Data.FormTable.DataList.Count - 1; i > 0; i--)
            {
                PVFormDetail d = Data.FormTable.DataList[i];
                if (!d.CostCenterCode.isEmpty())
                {
                    int? GL_ACCOUNT = lo.Persist.GLAccount(Data.TransactionCode ?? 0, d.ItemTransaction ?? 0);
                    if (GL_ACCOUNT != null && GlAccounts.Contains(GL_ACCOUNT ?? 0))
                    {
                        d.CostCenterCode = "";
                        d.CostCenterName = "";
                        r++;
                    }
                }
            }
            return r;
        }

        public static object Val(this OrderedDictionary d, string k, object v)
        {
            if (d[k] != null)
                return d[k];
            else
                return v;
        }

        public static PVFormDetail getDataBySequence(this PVFormTable t, int sequenceNumber)
        {
            return 
                t.DataList.Where(x => x.DisplaySequenceNumber == sequenceNumber).FirstOrDefault();
            //foreach (PVFormDetail d in t.DataList)
            //{
            //    if (d.DisplaySequenceNumber == sequenceNumber)
            //    {
            //        return d;
            //    }
            //}

            //return null;
        }

        public static void Reset(this PVFormTable t)
        {
            t.DataList.Clear();
            t.StandardWording = "";
        }

        public static void resetDisplaySequenceNumber(this PVFormTable t)
        {
            int cntData = t.DataList.Count;
            PVFormDetail detail;
            for (int i = 0; i < cntData; i++)
            {
                detail = t.DataList[i];
                detail.DisplaySequenceNumber = i + 1;
            }
        }

        public static void ReSequence(this PVFormTable t)
        {
            bool needReseq = false;
            for (int i = 0; i < t.DataList.Count; i++)
            {
                if (t.DataList[i].SequenceNumber != i)
                {
                    needReseq = true;
                    break;
                }
            }
            if (needReseq)
            {
                int j = 0;
                foreach (PVFormDetail d in t.DataList.OrderBy(x => x.SequenceNumber))
                {
                    j++;
                    d.SequenceNumber = j;
                }
            }
        }

        public static List<PVFormDetail> getBlankRowCleanedDetails(this PVFormTable t)
        {
            List<PVFormDetail> lstClone = new List<PVFormDetail>(t.DataList.Count);
            foreach (PVFormDetail d in t.DataList)
            {
                d.CostCenterCode = d.CostCenterCode != null ? d.CostCenterCode : "";
                d.CostCenterName = d.CostCenterName != null ? d.CostCenterName : "";
                d.CurrencyCode = d.CurrencyCode != null ? d.CurrencyCode : "";
                d.Description = d.Description != null ? d.Description : "";
                d.StandardDescriptionWording = d.StandardDescriptionWording != null ? d.StandardDescriptionWording : "";
                d.InvoiceNumber = d.InvoiceNumber != null ? d.InvoiceNumber : "";

                if (!string.IsNullOrEmpty(d.CostCenterCode) ||
                   !string.IsNullOrEmpty(d.CurrencyCode) ||
                   !string.IsNullOrEmpty(d.Description) || (d.Amount >= 0))
                {
                    lstClone.Add(d);
                }
            }
            return lstClone;
        }
        
        public static bool validTxInGlAccount(this PVFormTable t, TransactionType tx) 
        {
            int vCount = 0;
            if (t.DataList == null || t.DataList.Count < 1)
                return true;
            foreach(var d in t.DataList) {

                if (!d.GlAccount.isEmpty())
                {
                    if (d.GlAccount.StartsWith("5") && tx.ProductionFlag==1)
                    {
                        vCount++;
                    }
                    else if (d.GlAccount.StartsWith("7") && tx.ProductionFlag==2)
                    {
                        vCount++;
                    }
                    else
                        vCount++;
                }
                else
                    vCount++;
            }
            return vCount == t.DataList.Count;
        }

        public static bool validCostCenter(this PVFormTable t, int[] GlAccountNoCC, bool CostCenterFlag, List<string> e)
        {   
            /// for TAM cost center always valid 
            /// 
            return true; 

            //string[] glAccountMandatoryCC = new string[] { "5", "6", "7" };
            //int ccOk = 0;
            //if (CostCenterFlag)
            //{

            //    foreach (var g in t.DataList)
            //    {
            //        bool canBlank = false;
            //        if (!g.GlAccount.isEmpty() && g.GlAccount.Length > 1)
            //        {
            //            string x = "";
            //            x = g.GlAccount.Substring(0, 1);
            //            canBlank = !glAccountMandatoryCC.Contains(x)
            //                     || GlAccountNoCC.Contains(g.GlAccount.Int());
            //        }
            //        if (g.CostCenterCode.isEmpty() == canBlank)
            //        {
            //            ccOk++;
            //        }
            //        else
            //        {
            //            e.Add(string.Format("seq# {0} Cost Center '{1}' must be {2} for GlAccount [{3}]",
            //                g.SequenceNumber,
            //                g.CostCenterCode,
            //                (canBlank ? "Empty" : "Filled"),
            //                g.GlAccount));
            //        }
            //    }

            //}
            //else
            //{
            //    foreach (var h in t.DataList)
            //    {
            //        if (h.CostCenterCode.isEmpty())
            //            ccOk++;
            //        else
            //        {
            //            e.Add(string.Format("seq# {0} Cost Center must be empty", h.SequenceNumber));
            //        }

            //    }
            //}
            //return ccOk == t.DataList.Count;
        }

        public static bool hasInvalidDetail(this PVFormTable t, bool ignoreMinus = false)
        {
            int cntDataList = t.DataList.Count;
            PVFormDetail d;
            for (int i = 0; i < cntDataList; i++)
            {
                d = t.DataList[i];

                if (string.IsNullOrEmpty(d.CurrencyCode) || ((d.Amount < 0) && !ignoreMinus))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool UpdateGlAccount(this PVFormData f, PVFormLogic l)
        {  
            int i = 0; 
            foreach (PVFormDetail d in f.Details)
            {
                //d.GlAccount = l.GLAccount(d.CostCenterCode, f.TransactionCode ?? 0, d.ItemTransaction ?? 1).str();

                // Start Rinda Rahayu 20160329
                if(i == 0){ 
                    d.GlAccount = l.GLAccount(d.CostCenterCode, f.TransactionCode ?? 0, d.ItemTransaction ?? 1).str();
                }

                i++;
                // End Rinda Rahayu 20160329
            }
            return true;
        }


    }
}
