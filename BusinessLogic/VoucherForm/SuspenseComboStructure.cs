﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class SuspenseComboStructure
    {        
        public string WbsNumber { set; get; }
        public string Description { set; get; }
        public string SuspenseNo { set; get; }
    }
}
