﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class PVFormDetail
    {   
        public int SequenceNumber { get; set; }
        public string CostCenterCode { get; set; } 
        public string CostCenterName { get; set; }
        public string Description { get; set; }
        public string StandardDescriptionWording { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public string TaxCode { get; set; }
        public string TaxNumber { get; set; }
        public int? ItemTransaction { get; set; }
        public string WbsNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; } 
        public string GlAccount { get; set; }
        public string FromCurr { get; set; }
        public int? FromSeq { get; set; }
        public int? ItemNo { get; set; }
        public decimal DppAmount { get; set; }
        public string TaxAssignment { get; set; }
        public string AccInfAssignment { get; set; }
        public DateTime? TaxDate { get; set; }

        #region EFB
        public string WHT_TAX_CODE { get; set; }
        public decimal? WHT_TAX_TARIFF { get; set; }
        public string WHT_TAX_ADDITIONAL_INFO { get; set; }
        public decimal? WHT_DPP_PPh_AMOUNT { get; set; }
        #endregion
        public void Copy(PVFormDetail d)
        {
            SequenceNumber = d.SequenceNumber;
            FromCurr = d.FromCurr;
            CostCenterCode = d.CostCenterCode;
            CostCenterName = d.CostCenterName;
            Description = d.Description;
            StandardDescriptionWording = d.StandardDescriptionWording;
            CurrencyCode = d.CurrencyCode;
            Amount = d.Amount;
            TaxCode = d.TaxCode;
            TaxNumber = d.TaxNumber;
            ItemTransaction = d.ItemTransaction;
            WbsNumber = d.WbsNumber;
            InvoiceDate = d.InvoiceDate;
            InvoiceNumber = d.InvoiceNumber;
            GlAccount = d.GlAccount;
            FromSeq = d.FromSeq;
            ItemNo = d.ItemNo;
            DppAmount = d.DppAmount;
            TaxAssignment = d.TaxAssignment;
            AccInfAssignment = d.AccInfAssignment;
            TaxDate = d.TaxDate;

            Persisted = d.Persisted;
            ExrateSuspense = d.ExrateSuspense;
            DeletionControl = d.DeletionControl;
            DisplaySequenceNumber = d.DisplaySequenceNumber;

            #region EFB
            WHT_TAX_CODE = d.WHT_TAX_CODE;
            WHT_TAX_TARIFF = d.WHT_TAX_TARIFF;
            WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO;
            WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT;
            #endregion
        }


        // additional property for inner control 
        public bool Persisted { get; set; }
        public string DeletionControl { get; set; }
        public decimal? ExrateSuspense { get; set; }

        ///  additional property for display
        
        public int DisplaySequenceNumber { get; set; }
        public string AmountDisplay
        {
            get
            {
                return Common.Function.CommonFunction.Eval_Curr(CurrencyCode, Amount);
            }
        }
        public string InvoiceNumberUrl { get; set; }
        

    }
}