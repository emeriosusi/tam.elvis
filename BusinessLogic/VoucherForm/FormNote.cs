﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class FormNote
    {
        public string Message { get; set; }
        public int SeqNumber{ get; set; }
        public string SenderName { get; set; }
        public string SenderRole { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverRole { get; set; }
        public bool ToBeReplied { get; set; }
        public DateTime? Date {get; set;}
        public DateTime? ReplyDate { get; set; }
    }
}