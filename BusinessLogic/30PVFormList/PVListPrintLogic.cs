﻿using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._30PVList;
using Common.Function;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace BusinessLogic._30PVFormList
{
    public class PVListPrintLogic
    {
        //fid.pras 12072018
        private FormPersistence _fp = null;
        private FormPersistence fp
        {
            get
            {
                if (_fp == null)
                    _fp = new FormPersistence();
                return _fp;
            }
        }

        private LogicFactory logic = LogicFactory.Get();
        public string printCover(Page _page,
                               String imagePath,
                               String templatePath,
                               UserData _UserData,
                               List<PVListData> listData,
                               ref ErrorData _Err)
        {
            string outLink = "";
            try
            {
                Ini pvcover = new Ini(templatePath + "\\PVCoverTemplate.ini");
                string tempelCover = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateNew.html"));
                string tempelInvoice = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateInvoice.html"));
                string tempelTTD = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateTTD.html"));
                string tempelUnsettled = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateUnsettled.html"));
                Templet tCover = new Templet(tempelCover);
                Templet tInvoice = new Templet(tempelInvoice);
                Templet tUnsettled = new Templet(tempelUnsettled); 

                Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
                String _FileName = "PV_Cover_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");

                String contentTemp;
                HTMLWorker hw = new HTMLWorker(doc);
               

                MemoryStream ms = new MemoryStream();
                PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);

                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;
                doc.Open();

                StringBuilder amounts = new StringBuilder("");

                for (int i = 0; i < listData.Count; i++)
                {
                    PVListData data = listData[i];
                    tCover.Init(tempelCover);
                    tInvoice.Init(tempelInvoice);
                    tUnsettled.Init(tempelUnsettled);

                    Filler fCover = tCover.Mark("COMPANY_NAME,ISSUING_DIVISION,PV_DATE,PV_NO,"
                        + "VENDOR_NAME,TRANSACTION_TYPE,PAYMENT_METHOD,"
                        + "BUDGET_NO_DESC,DESC,PV_TYPE_NAME,AMOUNT,INVOICE,UNSETTLED,TTD,USERNAME,PRINT_DATE"); // Rinda Rahayu 20160328
                        
                    Filler fInvoice = tInvoice.Mark("rows", "INVOICE_NO,CURRENCY,"
                        + "val,TAX_NO,INVOICE_DATE,valTax");

                    Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(AppSetting.CompanyLogo)) );
                    img.ScaleToFit(pvcover["logo.width"].Int(), pvcover["logo.height"].Int());
                    System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                    String username = String.Format("{0} {1}", _UserData.FIRST_NAME, _UserData.LAST_NAME);
                    username = myTI.ToTitleCase(username.ToLower());

                    amounts.Clear();
                    List<AmountData> la = logic.Base.GetAmounts(data.PV_NO.Int(), data.PV_YEAR.Int());
                    if (la != null)
                        foreach (AmountData amt in la)
                        {
                            if (amt.TOTAL_AMOUNT != null && (amt.TOTAL_AMOUNT > 0))
                                amounts.AppendFormat(
                                    "<tr><td>{0}</td><td style='font-size: 16pt;' align='right'>{1}</td></tr>"
                                    , amt.CURRENCY_CD
                                    , CommonFunction.Eval_Curr(amt.CURRENCY_CD, amt.TOTAL_AMOUNT));
                        }

                    List<PVCoverData> listInvoiceData = null;

                    listInvoiceData = getInvoiceValue(data.PV_NO, data.PV_YEAR);
                    StringBuilder amtStr = new StringBuilder("");

                    int seq = 0;
                    if (listInvoiceData != null)
                        foreach (var invoice in listInvoiceData)
                        {
                            string val = "";
                            string valTax = "";

                            if (invoice.AMOUNT != null)
                            {
                                if (!AppSetting.RoundCurrency.Contains(invoice.CURRENCY))
                                {
                                    val = String.Format("{0:N2}", invoice.AMOUNT.Value);
                                    valTax = String.Format("{0:N2}", invoice.AMOUNT * invoice.TAX_AMOUNT);
                                }
                                else
                                {
                                    val = String.Format("{0:N0}", invoice.AMOUNT.Value);
                                    valTax = String.Format("{0:N0}", invoice.AMOUNT * invoice.TAX_AMOUNT);
                                }
                            }
                            else
                            {
                                val = " ";
                                valTax = " ";
                            }
                            fInvoice.Add(
                                (invoice.INVOICE_NO == "" ? " " : invoice.INVOICE_NO),
                                (invoice.CURRENCY == "" ? " " : invoice.CURRENCY),
                                val,
                                (invoice.TAX_NO == "" ? " " : invoice.TAX_NO),
                                (invoice.INVOICE_DATE == null ? " " : invoice.INVOICE_DATE.Value.ToString("dd/MM/yyyy")),
                                valTax);
                        }

                    if (listInvoiceData != null && listInvoiceData.Count > 0)
                    {
                        amtStr.Append(tInvoice.Get());
                    }
                    string barcodeFormat = "{0}\t{1}\r";
                    if (!pvcover["barcode.format"].isEmpty())
                    {
                        barcodeFormat = pvcover["barcode.format"];
                        barcodeFormat = barcodeFormat.Trim().Replace("\\t", "\t").Replace("\\r", "\r").Replace("\\n", "\n");
                    }

                    Barcode128 bar = new Barcode128();

                    bar.CodeType = Barcode.CODE128;
                    bar.ChecksumText = false;
                    bar.GenerateChecksum = false;
                    bar.StartStopText = false;
                    bar.Code = string.Format(barcodeFormat, data.PV_NO, data.PV_YEAR);

                    Image imgBarCode = null;

                    imgBarCode = bar.CreateImageWithBarcode(PDFWriter.DirectContent, null, null);

                    StyleSheet sh = new StyleSheet();
                    string budgetNoDesc = "";

                    //fid.pras 29/06/2018 budget no dgn format wbs_no/wbs_desc/booking_no

                    if   (!data.BUDGET_NO.isEmpty() 
                        && data.TRANSACTION_CD.Int() == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
                    {
                        List<WBSStructure> wbs = fp.WbsNumbers(data.BUDGET_NO);
                        string desc = "", wbsno = "";
                        if (wbs != null && wbs.Count > 0)
                        {
                            desc = wbs[0].Description;
                            wbsno = wbs[0].WbsNumber;
                        }
                        CodeConstant vendorGroup = fp.getVendorGroupNameByVendor(data.BUDGET_NO);
                        data.BUDGET_NO_FORMAT = (data.BUDGET_NO != null) ? wbsno + " / " + desc + " / ": "";

                        budgetNoDesc = data.BUDGET_NO_FORMAT + "  <br>" + data.BUDGET_NO;
                    }
                    
                    else if (!data.BUDGET_NO.isEmpty())
                        {
                            budgetNoDesc = data.BUDGET_NO + "-" + data.BUDGET_DESCRIPTION;
                        }
                    //fid.pras 29/06/2018 budget no dgn format wbs_no/wbs_desc/booking_no
                   

                    /* Start Rinda Rahayu 20160330*/
                    String ttdStr = tempelTTD; 

                    List<CodeConstant> tranSkipApproval = CommonData.GetTransactionTypesSkipApproval();
                    CodeConstant codeSkipApp = (tranSkipApproval != null && tranSkipApproval.Count > 0) 
                        ? tranSkipApproval[0] 
                        : new CodeConstant() { Code ="", Description="" } ;
                    String ttd = "";
                    if (tranSkipApproval.Count()  > 0 )
                    {
                        String[] strSplit = codeSkipApp.Description.Split(',');
                        ttd = (strSplit.Where(a => a.Equals(data.TRANSACTION_CD)).Any()) ? "" : ttdStr;
                    }
                   
                    string pvTypeName = "";
                    if (data.PV_TYPE_NAME != null && !data.PV_TYPE_NAME.Equals(""))
                    {
                        pvTypeName = data.PV_TYPE_NAME;
                    }
                    else
                    {
                        List<CodeConstant> pvTypeList = CommonData.GetPVTypes(0);
                        for (i = 0; i < pvTypeList.Count; i++)
                        {
                            CodeConstant codeCs = pvTypeList[i];
                            if (codeCs.Code.Equals(data.PV_TYPE_CD))
                            {
                                pvTypeName = codeCs.Description;
                                break;
                            }
                        }
                    }
                    /* End Rinda Rahayu 20160330*/

                    List<OutstandingSuspense> los = new List<OutstandingSuspense>();
                    if (string.Compare(data.PV_TYPE_CD.Trim(), "2") == 0) 
                        los =fp.GetUnsettledByDivision(data.DIVISION_ID);
                    string loses = "";
                    if (los.Any())
                    {
                        Filler fSUSPENSE_COUNT = tUnsettled.Mark("TITLE", "SUS");
                        fSUSPENSE_COUNT.Add(los.Count.str());

                        Filler fLINE = tUnsettled.Mark("LINE", "ROW");
                        if (los.Count <= 5)
                        {
                            foreach (var osu in los)
                            {
                                fLINE.Add(string.Format("{0} Rp. {1} - {2} - {3}{4}"
                                        , osu.DOC_NO
                                        , osu.TOTAL_AMOUNT.fmt()
                                        , osu.VENDOR_NAME
                                        , ((osu.DAYS_OUTSTANDING>0) ? osu.DAYS_OUTSTANDING.str()+" day" : "")
                                        , (osu.DAYS_OUTSTANDING>1) ?"s":"")                                        
                                        );
                            }
                                
                        }
                        else 
                        {
                            fLINE.Add(string.Format("TOTAL Rp. {0}", los.Sum(a=>a.TOTAL_AMOUNT).fmt()));
                            
                            /// mapped out to array 5x5 
                            string[][] lono = new string[][] 
                            { 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" }, 
                                new string[] { "", "", "", "", "" } 
                            };
                            int losj = (los.Count > 25) ? 25 : los.Count;
                            for (int losi = 0; losi < losj; losi++)
                            {
                                lono[losi / 5][losi % 5] = los[losi].DOC_NO.str();
                            }
                            if (los.Count > 24)
                            {
                                lono[4][4] = "...NOT SHOWN";
                            }
                            for (int mx = 0; mx < 5; mx++)
                            {
                                fLINE.Add(string.Format("{0} &nbsp; {1} &nbsp; {2} &nbsp; {3} &nbsp; {4} &nbsp;"
                                    , lono[mx][0], lono[mx][1], lono[mx][2], lono[mx][3], lono[mx][4]));                                
                            }
                            
                        }

                        loses =tUnsettled.Get();
                    }
                    else
                    {
                        loses = "";
                    }

                    fCover.Set(
                            AppSetting.CompanyName,
                            data.DIVISION_NAME,
                            data.PV_DATE.ToString("d/M/yyyy"),
                            data.PV_NO,
                            String.Format("{0}: {1}", data.VENDOR_CD, data.VENDOR_NAME),
                            data.TRANSACTION_NAME,
                            data.PAY_METHOD_NAME,
                            budgetNoDesc,
                            data.DESCRIPTION,
                            pvTypeName, // Rinda Rahayu 20160328
                            amounts.ToString(), amtStr.ToString(),
                            loses, 
                            ttd, //Rinda Rahayu 20160330                            
                            username,
                            DateTime.Now.ToString("d MMMM yyyy"));

                    contentTemp = tCover.Get();
                    // File.WriteAllText(Util.GetTmp(LoggingLogic.GetPath(), "pvCover_%TIME%", ".txt"), contentTemp);
                    List<IElement> elements = HTMLWorker.ParseToList(new StringReader(contentTemp), sh);

                    string padding = pvcover["MF.Padding"];
                    PrintHelper.padMF = padding.Int(PrintHelper.padMF);
                    string mfCols = pvcover["MF.Columns"];
                    string mfRows = pvcover["MF.Rows"];
                    string mfTable = pvcover["MF.Table"];

                    PdfPTable digits = new PdfPTable(10);
                    for (int ix = 0; ix < 10; ix++)
                    {
                        PdfPCell x10 = new PdfPCell(new Paragraph("  "));
                        x10.BorderWidthTop = 1;
                        x10.BorderWidthRight = 1;
                        x10.BorderWidthBottom = 1;
                        x10.BorderWidthLeft = (ix > 0) ? 0 : 1;
                        x10.PaddingTop = PrintHelper.padMF;
                        x10.PaddingBottom = PrintHelper.padMF;
                        x10.PaddingLeft = PrintHelper.padMF;
                        x10.PaddingRight = PrintHelper.padMF;
                        digits.AddCell(x10);
                    }
                    digits.WidthPercentage = 100;

                    float[] wtDigits = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                    PrintHelper.WidthsParse(pvcover, "MF.Digits", ref wtDigits);
                    digits.SetWidths(wtDigits);

                    float[] wtRow = new float[] { 17, 20 };
                    PrintHelper.WidthsParse(pvcover, "MF.Rows", ref wtRow);

                    float[] wtTable = new float[] { 5, 6 };
                    PrintHelper.WidthsParse(pvcover, "MF.Table", ref wtTable);

                    PdfPTable pRow = new PdfPTable(2);

                    PrintHelper.coverMFRow(pRow, new PdfPCell(digits), "SAP Invoice Document No");
                    PrintHelper.coverMFRow(pRow, new PdfPCell(digits), "SAP Payment Document No");
                    PrintHelper.coverMFRow(pRow, new PdfPCell(new Paragraph("")), "Payment Date");

                    pRow.SetWidths(wtRow);

                    pRow.WidthPercentage = 100;

                    PdfPTable tMF = new PdfPTable(2);
                    PdfPCell mf0 = new PdfPCell(imgBarCode);
                    mf0.BorderWidth = 0;
                    tMF.AddCell(mf0);
                    PdfPCell mf1 = new PdfPCell(pRow);
                    tMF.AddCell(mf1);

                    tMF.SetWidths(wtTable);
                    tMF.WidthPercentage = 100;
                    tMF.SpacingBefore = 0f;
                    tMF.SpacingAfter = 10f;
                    doc.Add(tMF);

                    int logoTop = pvcover["logo.top"].Int(680);
                    int logoLeft = pvcover["logo.left"].Int(40);

                    //img.SetAbsolutePosition(logoLeft, logoTop);
                    //doc.Add(img);
                    int elmnt = 1;
                    
                    PdfPTable tableHeader = elements[elmnt++] as PdfPTable;

                    PdfPCell[] hCells = tableHeader.Rows[0].GetCells();
                    hCells[0].AddElement(img);

                    tableHeader.SetWidths(new float[] { 1f, 7f });
                    //tableHeader.SpacingBefore = 1f;
                    //tableHeader.SpacingAfter = 1f;
                    doc.Add(tableHeader);

                    Paragraph par = elements[elmnt++] as Paragraph;
                    par.SpacingBefore = 10f;
                    par.SpacingAfter = 5f;
                    
                    doc.Add(par);

                    PdfPTable tableDetail = elements[elmnt++] as PdfPTable;

                    tableDetail.SetWidths(new float[] { 4f, 7f, 2f, 4f });

                    for (int j = 0; j < tableDetail.Rows.Count; j++)
                    {
                        PdfPCell[] cells = tableDetail.Rows[j].GetCells();

                        for (int k = 0; k < cells.Count(); k++)
                        {
                            PdfPCell cell = cells[k];
                            if (cell != null)
                            {
                                cell.NoWrap = true;

                                if (k == 1 && j == 1)
                                {
                                    // cell.PaddingTop = -17f;
                                }
                                else if (j != 1 && k % 2 != 0)
                                {
                                    cell.PaddingTop = -4f;
                                    cell.PaddingBottom = 3f;
                                }
                                else if (j != 1 && k % 2 == 0)
                                {
                                    cell.PaddingTop = -2f;
                                    cell.PaddingBottom = 4f;
                                }
                            }
                        }
                    }

                    PdfPTable wraper = new PdfPTable(1);
                    PdfPCell cellWrap = new PdfPCell(tableDetail);
                    cellWrap.BorderWidth = 1f;
                    wraper.AddCell(cellWrap);
                    wraper.WidthPercentage = 100f;

                    doc.Add(wraper);

                    doc.Add(new Paragraph("\n"));

                    PdfPTable tableAmount = elements[elmnt++] as PdfPTable;
                    tableAmount.SetWidths(new float[] { 1f, 4f });
                    tableAmount.WidthPercentage = 50f;

                    for (int j = 0; j < tableAmount.Rows.Count; j++)
                    {
                        PdfPCell[] cells = tableAmount.Rows[j].GetCells();

                        for (int k = 0; k < cells.Count(); k++)
                        {
                            PdfPCell cell = cells[k];
                            if (cell != null)
                            {
                                if (k % 2 != 0)
                                {
                                    cell.PaddingTop = -7f;
                                }
                                else
                                {
                                    cell.PaddingTop = -4f;
                                }
                            }
                        }
                    }
                    doc.Add(tableAmount);

                    
                    if (listInvoiceData != null && listInvoiceData.Count > 0)
                    {
                        doc.Add(elements[elmnt++] as PdfPTable);
                        PdfPTable tableInvoice = elements[elmnt++] as PdfPTable;


                        float[] aic = new float[] { 4f, 2f, 3f, 4f, 3f, 3f };
                        PrintHelper.WidthsParse(pvcover, "Invoice.Columns", ref aic);

                        tableInvoice.SetWidths(aic);

                        for (int j = 0; j < tableInvoice.Rows.Count; j++)
                        {
                            PdfPCell[] cells = tableInvoice.Rows[j].GetCells();

                            for (int k = 0; k < cells.Count(); k++)
                            {
                                PdfPCell cell = cells[k];
                                if (cell != null)
                                {
                                    if (j == 0)
                                        cell.PaddingTop = -3f;
                                    else
                                        cell.PaddingTop = -2f;
                                }
                            }
                        }

                        doc.Add(tableInvoice);
                        elmnt++;
                    }
                    if (los.Any())
                    {
                        PdfPTable tableLUS = elements[elmnt++] as PdfPTable;
                        doc.Add(tableLUS);
                    }
                    // Start Rinda Rahayu 20160330
                    if (!ttd.isEmpty())
                    {
                        PdfPTable tableTtd = elements[elmnt++] as PdfPTable;
                        tableTtd.TotalWidth = 540f;
                        tableTtd.SetWidths(new float[] { 8f, 98f });
                        tableTtd.CalculateHeights();
                        
                        tableTtd.WriteSelectedRows(pvcover["ttd.rowStart"].Int(0), pvcover["ttd.rowEnd"].Int(-1), 
                            (float) pvcover["ttd.xPos"].Num(4d), (float) pvcover["ttd.yPos"].Num(210d), PDFWriter.DirectContent);
                    }
                    // End Rinda Rahayu 20160330
                    
                    PdfPTable tableFooter = elements[elmnt++] as PdfPTable;
                    tableFooter.TotalWidth = 300f;
                    float[] tfcols = new float[] { 2f, 7f };
                    PrintHelper.WidthsParse(pvcover, "tableFooter.Columns", ref tfcols);
                    tableFooter.SetWidths(tfcols);
                    tableFooter.WriteSelectedRows(pvcover["tableFooter.rowStart"].Int(0), pvcover["tableFooter.rowEnd"].Int(-1), 
                        (float)pvcover["tableFooter.xPos"].Num(390), (float)pvcover["tableFooter.yPos"].Num(80), PDFWriter.DirectContent);

                    if (listData.Count - 1 > i)
                    {
                        doc.NewPage();
                    }
                }

                doc.Close();

                outLink = Xmit.Download(ms.GetBuffer(), _page, _FileName, ".pdf", "printCover", "printCoverPV");
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                _Err.ErrID = 2;
                _Err.ErrMsg = ex.Message;
            }
            return outLink;
        }

        public void ticketPrinted(int no, int year, string uid = null)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from p in db.TB_R_PV_H
                         where p.PV_NO == no && year == p.PV_YEAR
                         select p)
                         .FirstOrDefault();
                if (q != null)
                {
                    q.PRINT_TICKET_FLAG = 1;
                    if (!uid.isEmpty())
                    {
                        q.PRINT_TICKET_BY = uid;
                    }
                    db.SaveChanges();
                }
            }
        }

        public void printTicket(Page _page,
                                string imagePath,
                                string templatePath,
                                UserData _UserData,
                                List<PVListData> listData,
                                string cashierName,
                                ref ErrorData _Err)
        {
            try
            {
                TextReader reader = new StreamReader(templatePath + "\\PVTicketTemplate.htm");
                Document doc = new Document(PageSize.A4, 40, 40, 40, 40);
                String _FileName = "PV_Ticket_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                
                String contentBody = reader.ReadToEnd();
                String contentTemp;
                Decimal amountIDR = 0;
                Decimal amountUSD = 0;
                Decimal amountJPY = 0;
                int lastIndex = 0;
                Ini pvini = new Ini(templatePath + "\\PVCoverTemplate.ini");

                System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                String username = String.Format("{0} {1}", _UserData.FIRST_NAME, _UserData.LAST_NAME);
                username = myTI.ToTitleCase(username.ToLower());
                cashierName = myTI.ToTitleCase(cashierName.ToLower());

                HTMLWorker hw = new HTMLWorker(doc);

                MemoryStream ms = new MemoryStream();
                PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);

                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;
                doc.Open();
                Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(AppSetting.CompanyLogo)));
                img.ScaleToFit(pvini["logo.width"].Int(), pvini["logo.height"].Int());
                setValue(ref contentBody, "[COMPANY_NAME]", AppSetting.CompanyName);
                setValue(ref contentBody, "[ISSUING_DIVISION]", listData.FirstOrDefault().DIVISION_NAME);
                setValue(ref contentBody, "[PRINT_DATE]", DateTime.Now.ToString("d MMMM yyyy"));
                setValue(ref contentBody, "[FINANCE_CASHIER]", cashierName);
                setValue(ref contentBody, "[DIVISION_PIC]", username);

                contentTemp = contentBody;

                for (int i = 0; i < listData.Count; i++)
                {
                    PVListData data = listData[i];
                    int index = i < 7 ? i : i % 7;
                    bool printed = (data.PRINT_TICKET_FLAG ?? 0) != 0;
                    setValue(ref contentTemp, String.Format("[NO_{0}]", index), (i + 1).ToString());
                    setValue(ref contentTemp, String.Format("[PV_NO_{0}]", index), data.PV_NO + ((printed) ? "*" : ""));
                    setValue(ref contentTemp, String.Format("[PV_DATE_{0}]", index), data.PV_DATE.ToString("d/M/yyyy"));
                    setValue(ref contentTemp, String.Format("[VENDOR_{0}]", index), String.Format("{0}: {1}", data.VENDOR_CD, data.VENDOR_NAME));

                    if (contentTemp.Contains(String.Format("[IDR_VALUE_{0}]", index)))
                    {
                        String amt;
                        if (String.IsNullOrEmpty(data.AMOUNT_IDR))
                        {
                            amt = "";
                        }
                        else
                        {
                            amt = data.AMOUNT_IDR;
                            amountIDR += Decimal.Parse(data.AMOUNT_IDR);
                        }
                        contentTemp = contentTemp.Replace(String.Format("[IDR_VALUE_{0}]", index), amt);
                    }

                    if (contentTemp.Contains(String.Format("[USD_VALUE_{0}]", index)))
                    {
                        String amt;
                        if (String.IsNullOrEmpty(data.AMOUNT_USD))
                        {
                            amt = "";
                        }
                        else
                        {
                            amt = data.AMOUNT_USD;
                            amountUSD += Decimal.Parse(data.AMOUNT_USD);
                        }
                        contentTemp = contentTemp.Replace(String.Format("[USD_VALUE_{0}]", index), amt);
                    }

                    if (contentTemp.Contains(String.Format("[JPY_VALUE_{0}]", index)))
                    {
                        String amt;
                        if (String.IsNullOrEmpty(data.AMOUNT_JPY))
                        {
                            amt = "";
                        }
                        else
                        {
                            amt = data.AMOUNT_JPY;
                            amountJPY += Decimal.Parse(data.AMOUNT_JPY);
                        }
                        contentTemp = contentTemp.Replace(String.Format("[JPY_VALUE_{0}]", index), amt);
                    }

                    if (i > 5 && (i + 1) % 7 == 0)
                    {
                        writeDetailTicket(img,
                                          ref doc,
                                          ref contentTemp,
                                          amountIDR,
                                          amountUSD,
                                          amountJPY,
                                          PDFWriter.DirectContent);

                        contentTemp = contentBody;

                        amountIDR = 0;
                        amountUSD = 0;
                        amountJPY = 0;

                        doc.NewPage();
                    }
                    ticketPrinted(data.PV_NO.Int(), data.PV_YEAR.Int(), _UserData.USERNAME);
                    lastIndex++;
                }

                int mods = listData.Count % 7;

                if (mods > 0)
                {
                    for (int i = mods; i < 7; i++)
                    {
                        setValue(ref contentTemp, String.Format("[NO_{0}]", i), (lastIndex++ + 1).ToString());
                        setValue(ref contentTemp, String.Format("[PV_NO_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[PV_DATE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[VENDOR_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[IDR_VALUE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[USD_VALUE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[JPY_VALUE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[CHECK_{0}]", i), "&nbsp;");
                    }

                    writeDetailTicket(img,
                                      ref doc,
                                      ref contentTemp,
                                      amountIDR,
                                      amountUSD,
                                      amountJPY,
                                      PDFWriter.DirectContent);
                }

                doc.Close();
                reader.Close();

                _page.Response.Clear();
                _page.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.pdf", _FileName));
                _page.Response.ContentType = "application/pdf";
                _page.Response.Buffer = true;
                _page.Response.OutputStream.Write(ms.ToArray(), 0, ms.ToArray().Length);
                _page.Response.End();
            }
            catch (Exception ex)
            {
                _Err.ErrID = 2;
                _Err.ErrMsg = ex.Message;
            }
        }

        public void writeDetailTicket(Image img,
                                      ref Document doc,
                                      ref String contentTemp,
                                      Decimal amountIDR,
                                      Decimal amountUSD,
                                      Decimal amountJPY,
                                      PdfContentByte cb)
        {
            img.SetAbsolutePosition(40, 750);
            
            doc.Add(img);

            img.SetAbsolutePosition(40, 375);
            doc.Add(img);

            cb.SetLineWidth(1f);
            cb.SetGrayStroke(0f);
            cb.MoveTo(40f, 428f);
            cb.LineTo(553f, 428f);
            cb.Stroke();

            setValue(ref contentTemp, "[Total_IDR]", String.Format("{0:N0}", amountIDR));
            setValue(ref contentTemp, "[Total_USD]", String.Format("{0:N0}", amountUSD));
            setValue(ref contentTemp, "[Total_JPY]", String.Format("{0:N0}", amountJPY));

            StyleSheet sh = new StyleSheet();

            List<IElement> elements = HTMLWorker.ParseToList(new StringReader(contentTemp), sh);

            Int16 ind = 0;
            
            for (int l = 0; l < 2; l++)
            {
                PdfPTable table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 1f, 5f, 3f });
                doc.Add(table);

                Paragraph par = elements[ind++] as Paragraph;
                par.SpacingBefore = 5f;
                par.SpacingAfter = 10f;
                doc.Add(par);

                table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 3f, 5f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();

                    foreach (var cell in cells)
                    {
                        if (cell != null)
                        {
                            cell.PaddingTop = -3;
                        }
                    }
                }
                
                doc.Add(table);

                doc.Add(new Paragraph("\n"));

                table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 1.7f, 5.5f, 5f, 13.5f, 5.5f, 5.5f, 5.5f, 3.8f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();

                    for (int j = 0; j < cells.Count(); j++)
                    {
                        PdfPCell cell = cells[j];
                        if (cell != null)
                        {
                            if (i > 0)
                            {
                                cell.NoWrap = true;
                            }
                            cell.PaddingTop = -3;
                            if (j == cells.Count() - 1 && i > 1 && i < table.Rows.Count - 1)
                            {
                                PdfTemplate template = cb.CreateTemplate(30, 30);
                                template.SetLineWidth(0.5f);
                                template.Rectangle(5f, 1f, 8f, 8f);
                                template.Stroke();

                                img = Image.GetInstance(template);
                                Chunk chunk = new Chunk(img, 12f, 1f);

                                cell.AddElement(chunk);
                            }
                        }
                    }
                }
                doc.Add(table);

                table = elements[ind++] as PdfPTable;
                PdfPCell[] cl = table.Rows[0].GetCells();
                foreach (var c in cl)
                {
                    if (c != null)
                    {
                        c.PaddingTop = 5f;
                        c.PaddingBottom = 25f;
                    }
                }
                doc.Add(table);
            }
        }

        private List<PVCoverData> getInvoiceValue(string PV_NO, string PV_YEAR)
        {
            return logic.Base.Qu<PVCoverData>("GetInvoiceValue", PV_NO, PV_YEAR).ToList();
        }
        private void setValue(ref String content, String key, String value)
        {
            if (content.Contains(key))
            {
                content = content.Replace(key, value);
            }
        }
    }
}
