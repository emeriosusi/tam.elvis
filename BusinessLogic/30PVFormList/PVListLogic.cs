﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;
using Dapper;
using ICSharpCode.SharpZipLib.Zip;

namespace BusinessLogic._30PVList
{
    public class PVListLogic : VoucherListLogic
    {
        public const byte SAP_PAID_BY_CASHIER = 1;
        public const byte SAP_CHECK_BY_ACCOUNTING = 2;

        public IQueryable<vw_PV_List> SearchListIqueryable(
            VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;
            var q = (from p in db.vw_PV_List
                     select p);
            if (!c.ignoreDate)
                q = q.Where(x => (x.PV_DATE >= c.docDateFrom && x.PV_DATE <= c.docDateTo));
            if (!c.ignoreNum)
                q = q.Where(p => (p.PV_NO >= c.numFrom && p.PV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.PV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));
            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignorePlanningPaymentDate)
                q = q.Where(p => ((p.PLANNING_PAYMENT_DATE != null && p.PLANNING_PAYMENT_DATE >= c.planPaymentDateFrom && p.PLANNING_PAYMENT_DATE <= c.planPaymentDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => (p.PAID_DATE != null && p.PAID_DATE >= c.transDateFrom && p.PAID_DATE <= c.transDateTo));
            if (!c.ignorePostingDate)
                q = q.Where(p => (p.POSTING_DATE != null && p.POSTING_DATE >= c.postingDateFrom && p.POSTING_DATE <= c.postingDateTo));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => (p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.POSTING_DATE <= c.verifiedDateTo));
            if (c.settlementStatus > 0)
                q = q.Where(p => p.SETTLEMENT_STATUS == c.settlementStatus);
            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.PV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.PV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.PV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);
            /* 
            if (!c.ignoreUnknownEdit)
                q = q.Where(p => ((
                        (p.HOLD_FLAG != 0) && (p.VERSION > 1) &&
                        db.TB_R_DISTRIBUTION_STATUS.Any(d =>
                                d.ACTUAL_DT.HasValue &&
                                (c.unknownEdit == d.PROCCED_BY) &&
                                (p.REFF.HasValue && d.REFF_NO == p.REFF)))));
             */
            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);

            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));

            return q;
        }

        public IQueryable<vw_PV_List_Status_User> SearchListStatusUserIqueryable(
                VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;
            var q = (from p in db.vw_PV_List_Status_User
                     where (c.ignoreDate || (p.PV_DATE >= c.docDateFrom && p.PV_DATE <= c.docDateTo))
                     select p);

            if (!c.ignoreNum)
                q = q.Where(p => (p.PV_NO >= c.numFrom && p.PV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.PV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));

            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignorePlanningPaymentDate)
                q = q.Where(p => ((p.PLANNING_PAYMENT_DATE != null && p.PLANNING_PAYMENT_DATE >= c.planPaymentDateFrom && p.PLANNING_PAYMENT_DATE <= c.planPaymentDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => ((p.PAID_DATE != null && p.PAID_DATE >= c.transDateFrom && p.PAID_DATE <= c.transDateTo)));
            if (!c.ignorePostingDate)
                q = q.Where(p => (p.POSTING_DATE != null && p.POSTING_DATE >= c.postingDateFrom && p.POSTING_DATE <= c.postingDateTo));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => (p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.POSTING_DATE <= c.verifiedDateTo));
            if (c.settlementStatus > 0)
                q = q.Where(p => p.SETTLEMENT_STATUS == c.settlementStatus);
            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.PV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.PV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.PV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);

            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);

            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));
            return q;
        }

        public IQueryable<vw_PV_List_Status_Finance> SearchListStatusFinanceIqueryable(
            VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;
            var q = (from p in db.vw_PV_List_Status_Finance
                     where (c.ignoreDate || (p.PV_DATE >= c.docDateFrom && p.PV_DATE <= c.docDateTo))
                     select p);

            if (!c.ignoreNum)
                q = q.Where(p => (p.PV_NO >= c.numFrom && p.PV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.PV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));
            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignorePlanningPaymentDate)
                q = q.Where(p => ((p.PLANNING_PAYMENT_DATE != null && p.PLANNING_PAYMENT_DATE >= c.planPaymentDateFrom && p.PLANNING_PAYMENT_DATE <= c.planPaymentDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => ((p.PAID_DATE != null && p.PAID_DATE >= c.transDateFrom && p.PAID_DATE <= c.transDateTo)));
            if (!c.ignorePostingDate)
                q = q.Where(p => (p.POSTING_DATE != null && p.POSTING_DATE >= c.postingDateFrom && p.POSTING_DATE <= c.postingDateTo));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => (p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.POSTING_DATE <= c.verifiedDateTo));
            if (c.settlementStatus > 0)
                q = q.Where(p => p.SETTLEMENT_STATUS == c.settlementStatus);
            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.PV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.PV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.PV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);
            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);

            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));
            return q;
        }

        public PVHeaderData GetPVH(int no, int year)
        {
            return Qx<PVHeaderData>("GetPVH", new { pv_no = no, pv_year = year }).FirstOrDefault();
        }

        private bool ValidateActualOnTime(DateTime? planDate, DateTime? actualDate)
        {
            bool isValid = true;
            if (planDate != null)
            {
                if (actualDate == null)
                {
                    DateTime now = DateTime.Now;
                    if (now.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
                else
                {
                    if (actualDate.Value.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
            }
            return isValid;
        }

        private bool ValidateStatusCheckingOnTime(bool isUser, PVListData pv)
        {
            bool valid = true;

            if (isUser)
            {
                valid = ValidateActualOnTime(pv.PLAN_DT_SH, pv.ACTUAL_DT_SH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_DPH, pv.ACTUAL_DT_DPH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_DH, pv.ACTUAL_DT_DH);

                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_DIRECTOR, pv.ACTUAL_DT_DIRECTOR);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_VP, pv.ACTUAL_DT_VP);
            }
            else
            {
                valid = ValidateActualOnTime(pv.PLAN_DT_VERIFIED_COUNTER, pv.ACTUAL_DT_VERIFIED_COUNTER);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_VERIFIED_FINANCE, pv.ACTUAL_DT_VERIFIED_FINANCE);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_SH, pv.ACTUAL_DT_APPROVED_FINANCE_SH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_DPH, pv.ACTUAL_DT_APPROVED_FINANCE_DPH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_DH, pv.ACTUAL_DT_APPROVED_FINANCE_DH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_DIRECTOR, pv.ACTUAL_DT_APPROVED_FINANCE_DIRECTOR);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_POSTED_TO_SAP, pv.ACTUAL_DT_POSTED_TO_SAP);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_PAID, pv.ACTUAL_DT_PAID);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_CHECKED, pv.ACTUAL_DT_CHECKED);
            }

            return valid;
        }

        public bool Delete(List<PVFormData> listOfPVNo, string username)
        {
            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    foreach (PVFormData PVNo in listOfPVNo)
                    {
                        int pvNo = PVNo.PVNumber.Value;
                        int pvYear = PVNo.PVYear.Value;
                        // Get TB_R_PV_H
                        var q = (from i in DB.TB_R_PV_H
                                 where i.PV_NO == pvNo
                                 where i.PV_YEAR == pvYear
                                 select i).FirstOrDefault();
                        // decimal reffNo = Convert.ToDecimal((q.REFFERENCE_NO ?? 0));
                        string reff = pvNo.str() + pvYear.str();
                        decimal REFF_NO = reff.Dec();

                        /* deletion by set DELETED=1 */
                        /* 2012-11-24 dan -
                        if (q.REFFERENCE_NO != null)
                        {
                            string reff = Convert.ToString(q.REFFERENCE_NO);
                            // Delete TB_R_ATTACHMENT
                            var att = (from a in DB.TB_R_ATTACHMENT
                                       where a.REFERENCE_NO == reff
                                       select a).ToList();
                            if (att.Any())
                            {
                                foreach (var tmp in att)
                                {
                                    DB.DeleteObject(tmp);
                                    DB.SaveChanges();
                                }
                            }
                        }
                        
                        // Delete TB_R_NOTICE
                        var not = (from n in DB.TB_R_NOTICE
                                   where n.DOC_NO == pvNo
                                   where n.DOC_YEAR == pvYear
                                   select n).ToList();
                        if (not.Any())
                        {
                            foreach (var tmp in not)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        */
                        // Delete TB_R_DISTRIBUTION_STATUS
                        var dis = (from d in DB.TB_R_DISTRIBUTION_STATUS
                                   where d.REFF_NO == REFF_NO
                                   select d).ToList();
                        if (dis.Any())
                        {
                            foreach (var tmp in dis)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }


                        // Get TB_R_PV_DETAIL for Invoice checking and re-enable conversion
                        var pvdI = (from pi in DB.TB_R_PV_D
                                    where pi.PV_NO == pvNo
                                    where pi.PV_YEAR == pvYear
                                    select pi).ToList();
                        if (pvdI.Any())
                        {
                            foreach (var pInv in pvdI)
                            {
                                var v = (from inv in DB.TB_R_INVOICE_H
                                         where inv.PV_NO == pInv.PV_NO
                                         where inv.PV_YEAR == pInv.PV_YEAR
                                         select inv).FirstOrDefault();
                                if (v != null)
                                {
                                    v.PV_NO = null;
                                    v.PV_YEAR = null;
                                    v.STATUS_CD = -1;
                                    v.CONVERT_FLAG = 0;
                                    v.TRANSACTION_CD = null;
                                    v.CHANGED_BY = username;
                                    v.CHANGED_DT = DateTime.Now;
                                }
                            }
                        }



                        // Get the previous PV Form if any

                        var preP = (from v in DB.TB_R_PV_H
                                    where v.SET_NO_PV.HasValue && v.SET_NO_PV == pvNo
                                    // where v.SET_NO_PV.HasValue
                                    select v).FirstOrDefault();
                        if (preP != null)
                        {
                            int rvSettle = preP.SET_NO_RV ?? 0;
                            // Get the RV (if any)
                            var newR = (from r in DB.TB_R_RV_H
                                        where r.RV_NO == rvSettle
                                        where r.RV_YEAR == pvYear
                                        select r).FirstOrDefault();
                            if (newR != null)
                            {
                                // Delete the PVRV Amount of the particular RV
                                var newrv = (from n in DB.TB_R_PVRV_AMOUNT
                                             where n.DOC_NO == newR.RV_NO
                                             where n.DOC_YEAR == newR.RV_YEAR
                                             select n).ToList();
                                if (newrv.Any())
                                {
                                    foreach (var tmp in newrv)
                                    {
                                        DB.DeleteObject(tmp);
                                        DB.SaveChanges();
                                    }
                                }
                                // Delete RV Detail
                                var newRD = (from rd in DB.TB_R_RV_D
                                             where rd.RV_NO == newR.RV_NO
                                             where rd.RV_YEAR == newR.RV_YEAR
                                             select rd).ToList();
                                if (newRD.Any())
                                {
                                    foreach (var tmp in newRD)
                                    {
                                        DB.DeleteObject(tmp);
                                        DB.SaveChanges();
                                    }
                                }
                                newR.DELETED = 1;
                                // DB.DeleteObject(newR);
                                DB.SaveChanges();
                            }

                            // Get the settlement of the previous PV and re-enable conversion
                            var prevSetH = (from ps in DB.TB_R_SET_FORM_H
                                            where ps.PV_SUS_NO == preP.PV_NO
                                            where ps.PV_YEAR == preP.PV_YEAR
                                            select ps).FirstOrDefault();
                            if (prevSetH != null)
                            {
                                prevSetH.CONVERT_FLAG = 0;
                                prevSetH.CHANGED_BY = username;
                                prevSetH.CHANGED_DT = DateTime.Now;
                                DB.SaveChanges();
                            }

                            preP.SET_NO_PV = null;
                            preP.SET_NO_RV = null;
                            preP.CHANGED_BY = username;
                            preP.CHANGED_DATE = DateTime.Now;
                            DB.SaveChanges();
                        }

                        // Delete TB_R_SET_FORM_D
                        var setD = (from d in DB.TB_R_SET_FORM_D
                                    where d.PV_SUS_NO == pvNo
                                    where d.PV_YEAR == pvYear
                                    select d).ToList();
                        if (setD.Any())
                        {
                            foreach (var tmp in setD)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Delete TB_R_SET_FORM_H
                        var setH = (from h in DB.TB_R_SET_FORM_H
                                    where h.PV_SUS_NO == pvNo
                                    where h.PV_YEAR == pvYear
                                    select h).ToList();
                        if (setH.Any())
                        {
                            foreach (var tmp in setH)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        q.CANCEL_FLAG = 0;
                        q.DELETED = 1;
                        q.NEXT_APPROVER = "";
                        q.CHANGED_DATE = DateTime.Now;
                        q.CHANGED_BY = username;

                        BudgetCheckResult o;

                        // DB.DeleteObject(q); // 2012-11-24 dan - : delete logically not physically 
                        logic.k2.deleteprocess(q.PV_NO.ToString() + q.PV_YEAR.ToString());

                        BudgetCheckInput iBudget = new BudgetCheckInput()
                        {
                            DOC_STATUS = (int)BudgetCheckStatus.Delete,
                            DOC_NO = q.PV_NO,
                            DOC_YEAR = logic.Sys.FiscalYear(q.PV_DATE), //  q.PV_YEAR,
                            WBS_NO = q.BUDGET_NO,
                            AMOUNT = q.TOTAL_AMOUNT ?? 0
                        };

                        if (Common.AppSetting.SKIP_BUDGET_CHECK || !Common.AppSetting.BUDGET_CHECK_DELETE)
                        {
                            o = logic.SAP._BudgetCheck_(iBudget);
                        }
                        else
                        {
                            o = logic.SAP.BudgetCheck(iBudget);
                        }

                        DB.SaveChanges();
                    }
                    _result = true;
                    tx.Commit();
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public String getCashierName()
        {
            return Qx<string>("GetCashierName", null).FirstOrDefault();
        }

        #region Get String Builder

        private String DateTimeToString(DateTime? dateTimeParam, String format)
        {
            if (dateTimeParam != null)
            {
                DateTime dateTimeParam1 = (DateTime)dateTimeParam;
                return dateTimeParam1.ToString(format);
            }
            else
            {
                return null;
            }
        }

        public byte[] WorklistReport(
            int ProcessID, UserData userData,
            string serverPath,
            int docType,
            DateTime dFrom, DateTime dTo, string issuingDivision, int DelayTolerance)
        {
            string addi = logic.Sys.GetText("EXCEL_TEMPLATE", "");
            addi = addi.Replace("~", "").Replace("/", "\\");
            string template = Path.Combine(serverPath, addi);
            string sqlPath = Path.Combine(serverPath, "ref");
            string uPath = Path.Combine(serverPath, AppSetting.TempFileUpload.Substring(2, AppSetting.TempFileUpload.Length - 3));
            string doctype = (docType == 1) ? "PV" : "RV";
            string fn = Path.Combine(template, "WorklistDelayReportTemplate.xls");
            log.Log(_INF,
                string.Format("{0} WorklistReport(\r\n serverPath={1}, \r\n From={2}, \r\n To={3}, \r\n issuingDivision={4}, \r\n DelayTolerance={5})"
                , doctype
                , serverPath
                , dFrom.fmt(StrTo.SQL_DATUM)
                , dTo.fmt(StrTo.SQL_DATUM)
                , issuingDivision
                , DelayTolerance),
                  "WorklistReport", userData.USERNAME, "", ProcessID);

            Ini ini = new Ini(Path.Combine(sqlPath, "WorklistDelay.ini"));

            string sql1 = doctype + "WorklistReport";
            string sql2 = "Summary" + sql1;

            string sql = logic.SQL[sql1];
            sql = sql.Replace("@from", dFrom.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@to", dTo.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@issuingDivision", issuingDivision);
            sql = sql.Replace("@delay", DelayTolerance.str());

            var q = Db.Query<WorklistReportData>(sql).ToList();
            int rows = 0;
            if ((q != null) && (q.Count > 0))
            {
                rows = q.Count;
            }

            if (!File.Exists(fn))
            {
                errs.Add("no template file");
                log.Log("MSTD00002ERR", "Template file : '" + fn + "' not found");
                return null;
            }
            string filename = "";

            // open template 
            ExcelWriter o = new ExcelWriter(fn);
            string s1 = ini.Sections[1];
            o.MarkPage(s1);
            int seq = 0;
            o.ClearCells();
            bool oMarked = o.MarkColumns(ini[s1 + ".Column"]);

            if (!oMarked)
            {
                log.Log("MSTD00036WRN", "Marking Worklist column failed");
            }

            if (oMarked)
                foreach (var d in q)
                {
                    o.PutRow(
                        ++seq, d.DivNamePic, d.PIC, d.POSITION_NAME,
                        d.DivNameDoc, d.DOC_AGE,
                        d.MODIFIED, d.MODIFIED_BY, d.DOC_NO, d.STATUS_NAME,
                        d.Description, d.TOTAL_AMOUNT, d.TRANSACTION_NAME,
                        d.VENDOR_NAME);
                }

            o.ClearCells();

            bool oMarkHead = o.MarkLabels(ini[s1 + ".Header"]); //  "DOWNLOADED_BY,DATUM,DOC_DATE,DELAY_TOLERANCE,ROWS"

            if (!oMarkHead)
            {
                log.Log("MSTD00036WRN", "Marking template header failed");
            }
            else
            {
                o.PutCells(
                    userData.FIRST_NAME + " " + userData.LAST_NAME
                    , DateTime.Now.fmt(StrTo.XLS_DATE)
                    , dFrom.fmt(StrTo.XLS_DATE) + " to " + dTo.fmt(StrTo.XLS_DATE)
                    , DelayTolerance
                    , rows);
            }
            o.SetCalc();


            string aPath = Path.Combine(uPath,
                DateTime.Now.ToString("yyyy"));

            if (!Directory.Exists(aPath)) Directory.CreateDirectory(aPath);
            filename = Util.GetTmp(aPath, "WORKLIST_" + DateTime.Now.ToString("yyyyMMdd_hhmm"), ".xls");

            sql = logic.SQL[sql2];
            sql = sql.Replace("@from", dFrom.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@to", dTo.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@issuingDivision", StrTo.RemoveQuotes(issuingDivision));
            sql = sql.Replace("@delay", DelayTolerance.str());


            var qs = Db.Query<DocumentCountSummaryData>(sql).ToList();
            if (qs != null && qs.Count > 0)
            {
                string s0 = ini.Sections[0];
                o.MarkPage(s0);
                o.ClearCells();

                bool isMarked = o.MarkColumns(ini[s0 + ".Column"]);

                if (!isMarked)
                {
                    log.Log("MSTD00036WRN", "Marking template Summary column failed");
                }
                string lastDiv = "";
                string lastPic = "";
                int gtot = 0;

                List<DocumentCountSummaryData> l = new List<DocumentCountSummaryData>();
                int firstOne = -1;
                seq = 0;

                StringBuilder sb = new StringBuilder("");

                foreach (var s in qs)
                {
                    l.Add(s.Clone());
                    l[l.Count - 1].id = ++seq;

                    if (string.Compare(lastPic, s.PIC) == 0)
                    {
                        gtot += s.DOCUMENT_COUNT;

                    }
                    else
                    {
                        /// write gtot to first line per person
                        /// 
                        if (firstOne >= 0 && firstOne < l.Count)
                        {
                            l[firstOne].PERSON_TOTAL = gtot;
                        }

                        firstOne = seq - 1;
                        lastPic = s.PIC;

                        gtot = l[l.Count - 1].DOCUMENT_COUNT;

                    }

                }
                if (firstOne >= 0)
                {
                    l[firstOne].PERSON_TOTAL = gtot;

                }

                gtot = 0;
                int atot = 0;
                lastPic = "";
                foreach (var d in l)
                {
                    string sDiv = "";
                    string sPic = "";
                    string sPos = "";
                    if (string.Compare(lastDiv, d.DIV_PIC) != 0)
                    {
                        sDiv = d.DIV_PIC;
                        lastDiv = d.DIV_PIC;
                    }
                    if (string.Compare(lastPic, d.PIC) != 0)
                    {
                        sPos = d.POSITION_NAME;
                        sPic = d.PIC;
                        lastPic = d.PIC;
                    }
                    o.PutRow(
                        sDiv
                        , sPic
                        , sPos
                        , d.STATUS_NAME
                        , d.DOCUMENT_COUNT
                        , d.PERSON_TOTAL
                        , d.TOTAL_DOC_AGE
                        );

                    gtot += d.DOCUMENT_COUNT;
                    atot += (d.TOTAL_DOC_AGE ?? 0);

                }

                string[] ColNames = ini[s0 + ".Column"].Split(',');
                o.ColumnarGroup(ColNames[0]);
                o.ColumnarGroup(ColNames[1]);
                o.ColumnarGroup(ColNames[2]);
                o.ColumnarGroup(ColNames[ColNames.Length - 2]);

                o.PutRow("Grand Total", null, null, null, gtot, gtot, atot);

                o.RowGroup();

                o.ClearCells();
                bool isHeadMarked = o.MarkLabels(ini[s0 + ".Header"]); // "DOWNLOADED_BY,DATUM,DOC_DATE,DELAY_TOLERANCE"
                if (!isHeadMarked)
                {
                    log.Log("MSTD00036WRN", "Marking template header failed");
                }
                else
                {
                    o.PutCells(
                        userData.FIRST_NAME + " " + userData.LAST_NAME
                        , DateTime.Now.fmt(StrTo.XLS_DATE)
                        , dFrom.fmt(StrTo.XLS_DATE) + " to " + dTo.fmt(StrTo.XLS_DATE)
                        , DelayTolerance);
                }
                o.SetCalc();
            }

            return o.GetBytes();
        }

        public byte[] Get(
            string _username,
            string _imagePath,
            VoucherSearchCriteria c)
        {
            ExcelWriter x = new ExcelWriter();

            var qq = SearchListIqueryable(c);

            int sizeForAll = qq.Count();

            string header1 = "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                            "Downloaded Date : " + DateTime.Now.date() + ExcelWriter.COL_SEP +
                            "Total Record Download : " + sizeForAll;
            string header2 = "No|WF|Settl|PV No|Vendor Name|Transaction Type|" +
                            "Planning Payment Date|PV Type|Total Amount IDR|Last Status|Current PIC|" +
                            "PV Date|Submit Date|Verified Date|Posting Date|Paid Date|" +
                            "Issuing Division|Suspense No|" +
                            "PV Settlement No|RV Settlement No|Vendor Code|" +
                            "Payment Method|Budget No|Bank Type|SAP Doc No|" +
                            "Attachment|Activity Date{From|To}|" +
                            "Hold By|Version|Cancelled|Deleted|" +
                            "Hard Copy Verified|Ticket Printed|Ticket Printed by|" +
                            "Created{By|Date}|Changed{By|Date}";
            x.PutPage("General Info");
            x.PutHeader("PV List General Info", header1, header2, _imagePath);
            int rownum = 0;
            int pageSize = ExcelWriter.xlRowLimit;
            // the first eleven rows is used as header
            pageSize = pageSize - 11;
            int isNewPage = 1;
            int counter = 1;
            List<vw_PV_List> _list =
                qq.ToList();
            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (vw_PV_List dx in _list)
            {
                if (counter >= pageSize)
                {
                    counter = 1;
                    x.PutTail();
                    x.PutPage("General Info " + ++isNewPage);
                    x.PutHeader("PV List General Info", header1, header2, _imagePath);
                }

                x.Put(
                    ++rownum,
                    dx.WORKFLOW_STATUS, dx.SETTLEMENT_STATUS,
                    dx.PV_NO, dx.VENDOR_NAME, dx.TRANSACTION_NAME,
                    dx.PLANNING_PAYMENT_DATE.date(),
                    (doctype.Keys.Contains(dx.PV_TYPE_CD ?? 0)) ? doctype[dx.PV_TYPE_CD ?? 0] : "",
                    dx.TOTAL_AMOUNT_IDR.fmt(), dx.LAST_STATUS,
                    dx.NEXT_APPROVER_NAME,
                    dx.PV_DATE.date(),
                    dx.SUBMIT_DATE.date(),
                    dx.SUBMIT_HC_DOC_DATE.date(), // Rinda Rahayu 20160413 Verified Date
                    dx.POSTING_DATE.date(),
                    dx.PAID_DATE.date(),
                    dx.DIVISION_NAME, dx.SUSPENSE_NO,

                    dx.SET_NO_PV, dx.SET_NO_RV, dx.VENDOR_CD,
                    dx.PAY_METHOD_NAME, dx.BUDGET_NO, dx.BANK_TYPE, dx.SAP_DOC_NO,
                    ((dx.ATTACHMENT_QUANTITY) > 0) ? dx.ATTACHMENT_QUANTITY.str() : "",

                    dx.ACTIVITY_DATE_FROM.date(),
                    dx.ACTIVITY_DATE_TO.date(),
                    dx.HOLD_BY_NAME, dx.VERSION,
                    dx.CANCEL_FLAG, dx.DELETED,
                    dx.COUNTER_FLAG, dx.PRINT_TICKET_FLAG, dx.PRINT_TICKET_BY,
                    dx.CREATED_BY, dx.CREATED_DATE.minute(), dx.CHANGED_BY, dx.CHANGED_DATE.minute());
                counter++;
            }


            x.PutTail();

            x.PutPage("Status User");
            header2 = "No|PV No|PV Year|Registered|" +
                "Approved by SH{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by DpH{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by DH{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by Director{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by VP{Plan|Actual|Pln L/T|Act L/T}";
            x.PutHeader("PV List Status User", header1, header2, _imagePath);
            rownum = 0;
            isNewPage = 1;
            counter = 1;
            List<vw_PV_List_Status_User>
                _listUsr = this.SearchListStatusUserIqueryable(c)
                        .ToList();
            foreach (vw_PV_List_Status_User dx in _listUsr)
            {
                if (counter >= pageSize)
                {
                    counter = 1;
                    x.PutTail();
                    x.PutPage("Status User " + ++isNewPage);
                    x.PutHeader("PV List Status User", header1, header2, _imagePath);
                }

                x.Put(
                    ++rownum,
                    dx.PV_NO, dx.PV_YEAR, dx.REG_ACTUAL_DT.minute(),
                    dx.PLAN_DT_SH.minute(),
                    dx.ACTUAL_DT_SH.minute(),
                    dx.PLAN_LT_SH, dx.ACTUAL_LT_SH,
                    dx.PLAN_DT_DPH.minute(),
                    dx.ACTUAL_DT_DPH.minute(),
                    dx.PLAN_LT_DPH, dx.ACTUAL_LT_DPH,
                    dx.PLAN_DT_DH.minute(),
                    dx.ACTUAL_DT_DH.minute(),
                    dx.PLAN_LT_DH, dx.ACTUAL_LT_DH,
                    dx.PLAN_DT_DIRECTOR.minute(),
                    dx.ACTUAL_DT_DIRECTOR.minute(),
                    dx.PLAN_LT_DIRECTOR, dx.ACTUAL_LT_DIRECTOR,
                    dx.PLAN_DT_VP.minute(),
                    dx.ACTUAL_DT_VP.minute(),
                    dx.PLAN_LT_VP, dx.ACTUAL_LT_VP);
                counter++;
            }

            x.PutTail();

            x.PutPage("Status Finance");
            header2 = "No|PV No|PV Year|" +
                "Verified by Budget{Plan|Actual|Pln L/T|Act L/T}|" +
                "Verified by FD Staff{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by SH{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by DpH{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by DH{Plan|Actual|Pln L/T|Act L/T}|" +
                "Approved by Director{Plan|Actual|Pln L/T|Act L/T}|" +
                "Post to SAP{Plan|Actual|Pln L/T|Act L/T}|" +
                "Paid by Cashier{Plan|Actual|Pln L/T|Act L/T}|" +
                "Checked by Acc{Plan|Actual|Pln L/T|Act L/T}";
            x.PutHeader("PV List Status Finance", header1, header2, _imagePath);
            rownum = 0;
            isNewPage = 1;
            counter = 1;
            List<vw_PV_List_Status_Finance> _listFin =
                this.SearchListStatusFinanceIqueryable(c)
                        .ToList();
            foreach (vw_PV_List_Status_Finance dx in _listFin)
            {
                if (counter >= pageSize)
                {
                    counter = 1;
                    x.PutTail();
                    x.PutPage("Status Finance " + ++isNewPage);
                    x.PutHeader("PV List Status Finance", header1, header2, _imagePath);
                }

                x.Put(
                    ++rownum,
                    dx.PV_NO, dx.PV_YEAR,

                    dx.PLAN_DT_VERIFIED_BUDGET.minute(),
                    dx.ACTUAL_DT_VERIFIED_BUDGET.minute(),
                    dx.PLAN_LT_VERIFIED_BUDGET, dx.ACTUAL_LT_VERIFIED_BUDGET,

                    dx.PLAN_DT_VERIFIED_FINANCE.minute(),
                    dx.ACTUAL_DT_VERIFIED_FINANCE.minute(),
                    dx.PLAN_LT_VERIFIED_FINANCE, dx.ACTUAL_LT_VERIFIED_FINANCE,

                    dx.PLAN_DT_APPROVED_FINANCE_SH.minute(),
                    dx.ACTUAL_DT_APPROVED_FINANCE_SH.minute(),
                    dx.PLAN_LT_APPROVED_FINANCE_SH, dx.ACTUAL_LT_APPROVED_FINANCE_SH,

                    dx.PLAN_DT_APPROVED_FINANCE_DPH.minute(),
                    dx.ACTUAL_DT_APPROVED_FINANCE_DPH.minute(),
                    dx.PLAN_LT_APPROVED_FINANCE_DPH, dx.ACTUAL_LT_APPROVED_FINANCE_DPH,

                    dx.PLAN_DT_APPROVED_FINANCE_DH.minute(),
                    dx.ACTUAL_DT_APPROVED_FINANCE_DH.minute(),
                    dx.PLAN_LT_APPROVED_FINANCE_DH, dx.ACTUAL_LT_APPROVED_FINANCE_DH,

                    dx.PLAN_DT_APPROVED_FINANCE_DIRECTOR.minute(),
                    dx.ACTUAL_DT_APPROVED_FINANCE_DIRECTOR.minute(),
                    dx.PLAN_LT_APPROVED_FINANCE_DIRECTOR, dx.ACTUAL_LT_APPROVED_FINANCE_DIRECTOR,

                    dx.PLAN_DT_POSTED_TO_SAP.minute(),
                    dx.ACTUAL_DT_POSTED_TO_SAP.minute(),
                    dx.PLAN_LT_POSTED_TO_SAP, dx.ACTUAL_LT_POSTED_TO_SAP,

                    dx.PLAN_DT_PAID.minute(),
                    dx.ACTUAL_DT_PAID.minute(),
                    dx.PLAN_LT_PAID, dx.ACTUAL_LT_PAID,

                    dx.PLAN_DT_CHECKED.minute(),
                    dx.ACTUAL_DT_CHECKED.minute(),
                    dx.PLAN_LT_CHECKED,
                    dx.ACTUAL_LT_CHECKED);
                counter++;
            }

            x.PutTail();


            return x.GetBytes();
        }
        #endregion

        public PVFormHeaderData GetPVHeader(int _no, int _year)
        {
            return Qx<PVFormHeaderData>("GetPVHeader", new { no = _no, year = _year }).FirstOrDefault();
        }

        public List<PVFormHeaderData> GetEntertainmentList(DateTime postingFrom, DateTime postingTo)
        {
            return Qu<PVFormHeaderData>("GetEntertainmentList", postingFrom.fmt(StrTo.SQL_DATUM), postingTo.fmt(StrTo.SQL_DATUM)).ToList();
        }


        public byte[] GetCashierReport(
            IQueryable<vw_PV_List> q,
            string templateFile,
            string attachPath,
            int ProcessID,
            UserData by)
        {
            int rows = q.Count();

            log.Log(_INF,
              String.Format("GetCashierReport(\r\n pv count={0},\r\n template={1},\r\n attachPath={2})",
                  (q != null) ? rows.str() : "?", templateFile, attachPath),
                  "GetEntertain", (by != null) ? by.USERNAME : "", "", ProcessID);
            if (rows > 65500) // max rows by excel 2003 is 2 ^16
            {
                string err = "Too many rows, add more filter";
                errs.Add(err);
                log.Log("MSTD00002ERR", err);
                return null;
            }
            else if (rows < 1)
            {
                errs.Add("no Data");
                return null;
            }

            List<vw_PV_List> docs = q.ToList();
            if (!File.Exists(templateFile))
            {
                errs.Add("no template file");
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }
            string filename = "";

            // open template 
            ExcelWriter o = new ExcelWriter(templateFile);

            bool oMarked = o.MarkColumns(
                "No,PV No,Issuing Division,Vendor Name,Transaction Type,Planning Payment Date,"
                + "IDR,USD,JPY,PV Type,From,To,PV Date,Submit Date,Posting Date");
            if (!oMarked)
            {
                log.Log("MSTD00036WRN", "Marking template column failed");
            }

            int seq = 0;
            decimal total = 0;
            decimal totalIDR = 0;
            decimal totalUSD = 0;
            decimal totalJPY = 0;
            foreach (var pv in docs)
            {
                string reffNo = Convert.ToString(pv.PV_NO) + Convert.ToString(pv.PV_YEAR);
                string IDR = "";
                string JPY = "";
                string USD = "";
                List<OtherAmountData> oad = logic.Look.GetAmounts(pv.PV_NO, pv.PV_YEAR);
                OtherAmountData oa = null;
                oa = oad.Where(c => c.CURRENCY_CD == "IDR").FirstOrDefault();
                if (oa != null)
                {
                    IDR = CommonFunction.Eval_Curr("IDR", oa.TOTAL_AMOUNT);
                    totalIDR += (oa.TOTAL_AMOUNT ?? 0);
                }
                oa = oad.Where(c => c.CURRENCY_CD == "JPY").FirstOrDefault();
                if (oa != null)
                {
                    JPY = CommonFunction.Eval_Curr("JPY", oa.TOTAL_AMOUNT);
                    totalJPY += (oa.TOTAL_AMOUNT ?? 0);
                }
                oa = oad.Where(c => c.CURRENCY_CD == "USD").FirstOrDefault();
                if (oa != null)
                {
                    USD = CommonFunction.Eval_Curr("USD", oa.TOTAL_AMOUNT);
                    totalUSD += (oa.TOTAL_AMOUNT ?? 0);
                }

                o.PutRow(
                    ++seq, pv.PV_NO, pv.DIVISION_NAME,
                    pv.VENDOR_NAME,
                    pv.TRANSACTION_NAME,
                    pv.PLANNING_PAYMENT_DATE,
                    IDR,
                    USD,
                    JPY,
                    pv.PV_TYPE_NAME,
                    pv.ACTIVITY_DATE_FROM,
                    pv.ACTIVITY_DATE_TO,
                    pv.PV_DATE,
                    pv.SUBMIT_DATE,
                    pv.POSTING_DATE);
                total += (pv.TOTAL_AMOUNT_IDR ?? 0);
            }
            o.PutRow(
                    null, null, null,
                    null,
                    null,
                    null,
                    totalIDR,
                    totalUSD,
                    totalJPY,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "");
            o.ClearCells();
            bool oMarkHead = o.MarkLabels("BY,DATUM,ROWS");

            if (!oMarkHead)
            {
                log.Log("MSTD00036WRN", "Marking template header failed");
            }

            o.PutCells(
                by.FIRST_NAME + " " + by.LAST_NAME
                , DateTime.Now
                , docs.Count);
            o.SetCalc();

            // write template to temp download 
            string aPath = Path.Combine(attachPath,
                DateTime.Now.ToString("yyyy"));

            if (!Directory.Exists(aPath)) Directory.CreateDirectory(aPath);
            filename = Path.Combine(aPath, "PV_LIST_" + DateTime.Now.ToString("MMdd_hhmm") + ".xls");

            // o.Write(filename);

            // return temp download filename
            return o.GetBytes();
        }


        public byte[] GetEntertain(
            List<PVFormHeaderData> entertainmentPV,
            string templateFile,
            string attachPath,
            int ProcessID,
            UserData by,
            List<PVFormHeaderData> noAttachment)
        {
            using (ContextWrap CO = new ContextWrap())
            {
                var db = CO.db;
                log.Log(_INF,
                    String.Format("GetEntertain(\r\n pv count={0},\r\n template={1},\r\n attachPath={2})",
                        (entertainmentPV != null) ? Convert.ToString(entertainmentPV.Count) : "?", templateFile, attachPath),
                        "GetEntertain", (by != null) ? by.USERNAME : "", "", ProcessID);

                if (!File.Exists(templateFile))
                {
                    log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                    return null;
                }
                string filename = "";

                string attachmentType = logic.Sys.GetText("ATTACH_CATEGORY", "ENTERTAINMENT");
                if (attachmentType.isEmpty()) attachmentType = "5";


                List<TB_R_ATTACHMENT> allAttached = new List<TB_R_ATTACHMENT>();

                // open template 
                ExcelWriter o = new ExcelWriter(templateFile);
                bool oMarked = o.MarkColumns(
                    "NO,TANGGAL,TEMPAT DAN ALAMAT,JENIS, JUMLAH ,NAMA,POSISI,"
                    + "NAMA PERUSAHAAN,JENIS USAHA,KETERANGAN,PV,PIC,Referensi");
                if (!oMarked)
                {
                    log.Log("MSTD00036WRN", "Marking template column failed");
                }

                int seq = 0;
                foreach (PVFormHeaderData pv in entertainmentPV)
                {
                    string reffNo = Convert.ToString(pv.PV_NO) + Convert.ToString(pv.PV_YEAR);
                    var qa = from q in db.TB_R_ATTACHMENT
                             where reffNo.Equals(q.REFERENCE_NO)
                                && q.ATTACH_CD.Equals(attachmentType)
                                && q.FILE_NAME.Contains(".xls")
                             select q;
                    if (!qa.Any())
                    {
                        ++seq;
                        noAttachment.Add(pv);
                        object[] xx = new object[13];
                        xx[0] = seq;
                        xx[9] = "No file";
                        xx[10] = pv.PV_NO;
                        o.SetRow(xx);
                        // make destination row red 
                        o.SetStyleRed();
                        log.Log("MSTD00002ERR", "No file for PV: [" + pv.PV_NO + "]");
                    }
                    else
                    {
                        foreach (TB_R_ATTACHMENT x in qa)
                            allAttached.Add(x);
                    }
                }

                string lastReffNo = "";

                foreach (TB_R_ATTACHMENT a in allAttached)
                {
                    string attFile = attachPath
                            + Path.DirectorySeparatorChar
                            + a.DIRECTORY
                            + Path.DirectorySeparatorChar
                            + a.FILE_NAME;

                    if (File.Exists(attFile))
                        log.Log(_INF,
                                "READ Reff No:" + a.REFERENCE_NO +
                                " Seq:" + a.REF_SEQ_NO +
                                " File: " + a.FILE_NAME);
                    else
                    {
                        log.Log("MSTD00002ERR",
                            "No file Reff No :" + a.REFERENCE_NO +
                            " Seq:" + a.REF_SEQ_NO +
                            " File: " + a.FILE_NAME);

                        ++seq;

                        object[] z = new object[13];
                        z[0] = seq;
                        z[9] = "No File: '" + (a.FILE_NAME ?? "?")
                                + "' in \"" + (a.DIRECTORY ?? "?") + "\"";
                        if (a.REFERENCE_NO != null && a.REFERENCE_NO.Length > 4)
                            z[10] = a.REFERENCE_NO.Substring(0, a.REFERENCE_NO.Length - 4);
                        else
                            z[10] = "?";
                        o.SetRow(z);
                        // make destination row red 
                        o.SetStyleRed();

                        continue;
                    }
                    ExcelWriter ax = new ExcelWriter(attachPath
                            + Path.DirectorySeparatorChar
                            + a.DIRECTORY
                            + Path.DirectorySeparatorChar
                            + a.FILE_NAME);

                    oMarked = ax.MarkLabels("Nama:");
                    if (!oMarked)
                    {
                        log.Log(
                            "MSTD000036WRN",
                            "Marking input Template " + a.FILE_NAME
                            + " failed for Ref No :" + a.REFERENCE_NO);
                        break;
                    }
                    object[] nm = ax.ReadRow();
                    ax.ClearCells();
                    oMarked = ax.MarkColumns("No.,Tanggal,Tempat dan Alamat,Jenis,"
                        + "Jumlah (Rp),Nama,Posisi,Nama Perusahaan,Jenis Usaha,Keterangan");
                    if (!oMarked)
                    {
                        log.Log(
                            "MSTD000036WRN",
                            "Marking input Template column " + a.FILE_NAME
                            + " failed for Ref No: " + a.REFERENCE_NO);
                        continue;
                    }
                    // insert "a" contents into template 
                    object[] xx;
                    do
                    {
                        xx = ax.ReadRow();
                        if (xx == null) break;

                        int j = xx.Length;
                        if (!lastReffNo.Equals(a.REFERENCE_NO))
                        {
                            seq++;
                            xx[0] = seq;
                            lastReffNo = a.REFERENCE_NO;
                        }
                        else
                        {
                            xx[0] = null;
                        }

                        Array.Resize(ref xx, j + 3);
                        if (a.REFERENCE_NO != null && a.REFERENCE_NO.Length > 4)
                            xx[j] = a.REFERENCE_NO.Substring(0, a.REFERENCE_NO.Length - 4);
                        else
                            xx[j] = "?";
                        if (nm != null && nm[0] != null)
                            xx[j + 1] = nm[0];
                        else
                            log.Log("MSTD00002ERR", "Cannot read name cell");

                        o.SetRow(xx);

                    } while (xx.Length > 0 && xx[0] != null);
                }
                // write template to temp download 
                string aPath = attachPath
                    + Path.DirectorySeparatorChar
                    + DateTime.Now.ToString("yyyy")
                    + Path.DirectorySeparatorChar
                    + "EntertainmentList";

                return o.GetBytes();
            }
        }

        public void UpdateDetailItemNo(ELVIS_DBEntities db, List<PostItemData> l)
        {
            if (l == null || l.Count < 1) return;
            string lastreff = "";
            foreach (PostItemData d in l)
            {
                string reff = string.Format("{0}{1}", d.DOC_NO, d.DOC_YEAR);
                if (!lastreff.Equals(reff))
                {
                    //LoggingLogic.say(reff, "{0,10} {1,4} {2,3}{3,4}", "#", "", "seq", "itm");
                    lastreff = reff;
                }
                //LoggingLogic.say(reff, "{0,10} {1,4} {2,3}{3,4}", d.DOC_NO, d.DOC_YEAR, d.SEQ_NO, d.ITEM_NO);

                var q = db.TB_R_PV_D
                        .Where(p => p.PV_NO == d.DOC_NO && p.PV_YEAR == d.DOC_YEAR && p.SEQ_NO == d.SEQ_NO);

                if (q.Any())
                {
                    foreach (var x in q)
                    {
                        x.ITEM_NO = d.ITEM_NO;
                    }
                    db.SaveChanges();
                }
            }

        }

        public string getInvalidPrintStatusCover(string pv_no, string pv_year)
        {
            string status = null;
            decimal reffNo = Convert.ToDecimal(pv_no + pv_year);
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from p in db.TB_R_DISTRIBUTION_STATUS
                         where p.REFF_NO == reffNo
                         select p).FirstOrDefault();
                int statusCd = 0;
                if (q != null)
                {
                    var r = (from p in db.TB_R_DISTRIBUTION_STATUS.AsEnumerable()
                             where p.REFF_NO == reffNo
                             where Convert.ToString(p.STATUS_CD).StartsWith("1")
                             where !p.ACTUAL_DT.HasValue
                             select p.STATUS_CD);

                    if (r.Any())
                    {
                        statusCd = r.Max();
                    }
                }
                else
                {
                    statusCd = 10;
                }
                if (statusCd > 0)
                {
                    var s = (from p in db.TB_M_STATUS
                             where p.STATUS_CD == statusCd
                             select p).FirstOrDefault();
                    if (s != null)
                    {
                        status = s.STATUS_NAME;
                    }
                }
            }
            return status;
        }


        #region Added by Akhmad Nuryanto, 6/6/2012
        ///Post to SAP

        private bool PostAndSaveDocNo(
            int ProcessId,
            string uid,
            List<PVPostInputHeader> Headers,
            List<PVPostInputDetail> Details,
            List<OneTimeVendorData> Vendors,
            List<string> fails,
            List<string> succeeds,
            List<string> errs,
            Dictionary<string, bool> results)
        {
            string _loc = "PostAndSaveDocNo()";
            int postToSAPStatus = 27;
            List<PVPostSAPResult> _PostResults = new List<PVPostSAPResult>();
            if (Headers.Count < 1 && Details.Count < 1)
            {
                errs.Add("empty Input:");
                return false;
            }

            string errorMessage = "";
            _PostResults = logic.SAP.GetResultPostPVToSAP(
                    Headers,
                    Details,
                    Vendors,
                    ProcessId,
                    uid,
                    ref errorMessage);
            errs.Add(errorMessage);
            if (_PostResults.Count < 1)
            {
                log.Log(_INF, "PostToSAP Result Empty: ", _loc, uid, "", ProcessId);
                return false;
            }

            PVPostSAPResult[] ret = _PostResults
                .OrderBy(a => a.PV_YEAR)
                .ThenBy(a => a.PV_NO)
                .ThenBy(a => a.ITEM_NO)
                .ToArray();

            Ticker.In("PostToSapResult");
            Ticker.Spin("PV_NO	PV_YEAR	ITEM_NO	CURRENCY_CD	INVOICE_NO	SAP_DOC_NO	SAP_DOC_YEAR	SAP_CLEARING_DOC_NO	SAP_CLEARING_DOC_YEAR	MESSAGE	STATUS_MESSAGE	MESSAGE_TEXT");
            for (int pi = 0; pi < ret.Length; pi++)
            {
                PVPostSAPResult x = ret[pi];
                Ticker.Spin("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}"
                    ,
                    x.PV_NO, x.PV_YEAR, x.ITEM_NO, x.CURRENCY_CD, x.INVOICE_NO, x.SAP_DOC_NO, x.SAP_DOC_YEAR, x.SAP_CLEARING_DOC_NO, x.SAP_CLEARING_DOC_YEAR, x.MESSAGE, x.STATUS_MESSAGE, x.MESSAGE_TEXT
                    );
            }
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                int j = ret.Length;
                int i = 0;
                while (i < ret.Length)
                {
                    string _no = ret[i].PV_NO;
                    string _yy = ret[i].PV_YEAR;
                    string k = ret[i].PV_NO + ":" + ret[i].PV_YEAR;
                    bool posted = true;
                    while (i < ret.Length && k == ret[i].PV_NO + ":" + ret[i].PV_YEAR)
                    {
                        if (ret[i].SAP_DOC_NO.isEmpty() || (string.Compare(ret[i].MESSAGE, "E") == 0))
                            posted = false;
                        i++;
                    }

                    if (posted)
                    {
                        if (!succeeds.Contains(k))
                        {
                            succeeds.Add(k);
                            results[k] = true;
                        }
                        PVPostSAPResult[] sdoc = _PostResults.Where(x => x.PV_NO == _no && x.PV_YEAR == _yy).ToArray();
                        if (sdoc != null && sdoc.Length > 0)
                        {
                            List<PostItemData> postItems = new List<PostItemData>();

                            foreach (PVPostSAPResult item in sdoc)
                            {
                                postItems.Add(new PostItemData()
                                {
                                    DOC_NO = item.PV_NO.Int(),
                                    DOC_YEAR = item.PV_YEAR.Int(),
                                    ITEM_NO = item.ITEM_NO.Int(),
                                    INVOICE_NO = item.INVOICE_NO,
                                    CURRENCY_CD = item.CURRENCY_CD,
                                    SAP_DOC_NO = item.SAP_DOC_NO.isEmpty() ? (int?)null : (int?)item.SAP_DOC_NO.Int(),
                                    SAP_DOC_YEAR = item.SAP_DOC_YEAR.isEmpty() ? (int?)null : (int?)item.SAP_DOC_YEAR.Int()
                                });
                            }
                            UpdateSapDocNo(db, postItems);
                        }
                        int iNo = _no.Int();
                        int iYY = _yy.Int();

                        TB_R_PV_H _TB_R_PV_H = (from p in db.TB_R_PV_H
                                                where p.PV_NO == iNo && p.PV_YEAR == iYY
                                                select p).FirstOrDefault();
                        if (_TB_R_PV_H != null)
                        {
                            _TB_R_PV_H.STATUS_CD = postToSAPStatus;
                            if (_TB_R_PV_H.POSTING_DATE == null)
                                _TB_R_PV_H.POSTING_DATE = DateTime.Now;
                            db.SaveChanges();
                        }

                    }
                    else
                    {
                        if (!fails.Contains(k))
                        {
                            fails.Add(k);
                        }
                        results[k] = false;
                    }
                }

            }


            Ticker.Out();
            bool r = true;
            foreach (string k in results.Keys)
            {
                r = r && results[k];
            }
            return r;
        }

        public override bool PostToSap(List<string> keys, int _ProcessId, ref string _Message, UserData u, int? transType = null)
        {
            bool Ok = base.PostToSap(keys, _ProcessId, ref _Message, u);
            if (!Ok) return false;
            // return PVPostToSap(keys, _ProcessId, ref _Message, u.USERNAME);

            // fid Hadid. 20180709. If the Transaction Type of PV is Accrued Year End
            // then use new BAPI
            // return DoPost(keys, _ProcessId, ref _Message, u.USERNAME);       <----- original version before Accrued Enhancement
            if (transType != null && transType.Value == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
            {
                var sapErr = new List<AccrSAPError>();
                Ok = logic.AccrForm.PVAccrPostingDirect(keys, u, ref sapErr);

                if (sapErr.Any())
                {
                    List<string> msgErr = sapErr.Select(x => x.MESSAGE).ToList();
                    _Message = m.Message("MSTD00002ERR", CommonFunction.CommaJoin(msgErr));
                }
            }
            else
            {
                Ok = DoPost(keys, _ProcessId, ref _Message, u.USERNAME);
            }
            return Ok;
            // End enhancement
        }

        public bool DoPost(List<string> _Docs, int _ProcessId, ref string _Message, string uid = "")
        {
            string _loc = "DoPost()";
            int pid = log.Log(_INF, "Start " + _loc, _loc, uid, "", _ProcessId);

            bool result = false;

            List<string> _PvsPosted = new List<string>();

            List<string> _isNotReadyToBePosted = new List<string>();
            List<string> fails = new List<string>();
            List<string> succeeds = new List<string>();

            this["OK"] = succeeds;
            this["NG"] = fails;
            this["notready"] = _isNotReadyToBePosted;
            this["done"] = _PvsPosted;

            errs.Clear();
            Dictionary<string, bool> results = new Dictionary<string, bool>();
            List<TransactionType> tt;
            string _add_err = ". ";
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var db = co.db;
                    tt = logic.Base.GetTransactionTypes();
                    #region validation
                    foreach (string item in _Docs)
                    {
                        string reffNo = item.Replace(":", "");
                        Util.ReMoveFile(Path.Combine(LoggingLogic.GetPath(), reffNo + ".log"));

                        System.Data.Objects.ObjectParameter IsReady = new System.Data.Objects.ObjectParameter("IS_READY", "");
                        db.isReadyToPost(reffNo, IsReady);
                        if (IsReady == null || IsReady.Value.Equals("") || IsReady.Value.Equals("0"))
                        {
                            if (!_isNotReadyToBePosted.Contains(item))
                            {
                                _isNotReadyToBePosted.Add(item);
                            }
                        }
                        string[] nodanyear = item.Split(':');

                        int no = nodanyear[0].Int();
                        int year = nodanyear[1].Int();

                        PVHeaderData pvh = GetPVH(no, year);

                        int statusCd = 0;

                        if (pvh != null) statusCd = pvh.STATUS_CD;

                        if ((statusCd == 27) || (statusCd == 28) || (statusCd == 29))
                        {
                            if (!_PvsPosted.Contains(reffNo))
                                _PvsPosted.Add(reffNo);
                        }

                        if (checkOneTime && pvh.VENDOR_GROUP_CD == 4)
                        {
                            var qotv = (from v in db.TB_R_ONE_TIME_VENDOR
                                        where v.PV_NO == no && v.PV_YEAR == year
                                        select v);
                            if (!qotv.Any() && !_isNotReadyToBePosted.Contains(item))
                            {
                                _isNotReadyToBePosted.Add(item);
                            }
                        }

                        if ((pvh.PAY_METHOD_CD.Equals("T")) && (pvh.BANK_TYPE == null))
                        {
                            //var q = (new FormPersistence()).GetVendorBank(pvh.VENDOR_CD);
                            // int vendorbankCount = q.Count();
                            int vendorbankCount = Qx<int>("GetVendorBanksCountByVendor", new { @vendorCd = pvh.VENDOR_CD }).First();
                            if (vendorbankCount < 1 && pvh.VENDOR_GROUP_CD != 4)
                            {
                                _isNotReadyToBePosted.Add(item);
                                _add_err += m.Message("MSTD00002ERR", "Vendor doesn't have Bank ");

                                log.Log("MSTD00002ERR", "Vendor doesn't have Bank", _loc, uid, "", _ProcessId);
                            }
                            if (vendorbankCount > 1)
                            {
                                _isNotReadyToBePosted.Add(item);
                                _add_err += m.Message("MSTD00017WRN", "Bank Type");

                                log.Log("MSTD00017WRN", "Bank Type", _loc, uid, "", _ProcessId);
                            }
                        }
                    }
                    if (_PvsPosted.Count > 0)
                    {
                        _Message = m.Message("MSTD00075ERR", CommonFunction.CommaJoin(_PvsPosted));

                        log.Log("MSTD00075ERR", _Message, _loc, uid, "", _ProcessId);
                        return false;
                    }
                    if (_isNotReadyToBePosted.Count > 0)
                    {
                        _Message = m.Message("MSTD00078ERR", CommonFunction.CommaJoin(_isNotReadyToBePosted)) + _add_err;
                        log.Log("MSTD00078ERR", _Message, _loc, uid, "", _ProcessId);

                        return false;
                    }
                    #endregion

                    log.Log(_INF, "PV Post:\r\n\t" + CommonFunction.CommaJoin(_Docs).Replace(", ", "\r\n\t"), _loc, uid, "", _ProcessId);

                    List<PVPostInputHeader> _InputHeaders = new List<PVPostInputHeader>();
                    List<PVPostInputDetail> _InputDetails = new List<PVPostInputDetail>();
                    List<OneTimeVendorData> _InputVendors = new List<OneTimeVendorData>();

                    db.CommandTimeout = 300;

                    foreach (string k in _Docs)
                    {
                        string[] nodanyear = k.Split(':');
                        string _NO = nodanyear[0];
                        string _YEAR = nodanyear[1];
                        Int64 no = Int64.Parse(nodanyear[0]);
                        int year = int.Parse(nodanyear[1]);
                        var q = (from p in db.vw_PvPostInput
                                 where p.NO_YEAR_KEY == k
                                 orderby p.PV_YEAR, p.PV_NO, p.INVOICE_NO, p.CURRENCY_CD, p.FROM_CURR
                                 select p).ToList();

                        int hCount = q.Count;
                        if (hCount < 1) continue;

                        int i = 1;

                        List<PVFormDetail> sude = new List<PVFormDetail>();
                        List<OriginalAmountData> osu = new List<OriginalAmountData>();

                        if (q[0].PV_TYPE_CD == 3)
                        {
                            /// query suspense no for max item no
                            int suno, suye;
                            suno = q[0].SUSPENSE_NO ?? 0;
                            suye = q[0].SUSPENSE_YEAR ?? 0;

                            int? maxi = GetSuspenseMaxItemNo(suno, suye);
                            if (maxi.HasValue)
                                i = maxi.Value + 1;

                            sude = logic.PVForm.GetPV(new PVFormData()
                            {
                                PVNumber = suno,
                                PVYear = suye
                            }, db);

                            osu = logic.Base.GetOriginal(suno, suye);
                        }

                        // _log.Log(_INF, "select " + hCount + " header", _loc, uid, "", _ProcessId);

                        string lastKey = "";

                        List<string> _ListofPVDetail = new List<string>();
                        decimal TotalAmount = 0;
                        decimal TotalSpent = 0;
                        int qi = 0;


                        while (qi < hCount)
                        {
                            var h = q[qi];
                            string Year;

                            Year = Convert.ToString(h.PV_YEAR);
                            string sCode = h.TRANSACTION_CD.str();
                            string sWord = (from w in tt
                                            where w.Code.Equals(sCode)
                                            select w.StandardWording)
                                                 .FirstOrDefault();

                            string HeaderText = Textuality(sWord, h.DESCRIPTION);
                            int _no = h.PV_NO.Int(0);
                            int _yy = h.PV_YEAR.Int(0);

                            string ckey = string.Format("{0}:{1}:{2}:{3}:{4}"
                                    , h.PV_NO, h.PV_YEAR
                                    , h.INVOICE_NO
                                    , h.CURRENCY_CD
                                    , h.FROM_CURR);
                            lastKey = ckey;
                            TotalAmount = 0;
                            TotalSpent = 0;
                            List<PostItemData> po = new List<PostItemData>();
                            //bool isCashierPolicy = (h.SEQ_NO == h.FROM_SEQ);

                            decimal exrateSuspense = 1;
                            int noSuspenseFlag = 0;

                            if (h.ITEM_NO == null)
                                noSuspenseFlag = 1;

                            if (h.EXRATE_SUSPENSE == null && h.PV_TYPE_CD == 3)
                            {
                                string cur = h.FROM_CURR ?? h.CURRENCY_CD;
                                var ox = osu.Where(A => A.CURRENCY_CD == cur);
                                if (ox.Any())
                                {
                                    exrateSuspense = ox.Select(A => A.EXCHANGE_RATE).FirstOrDefault() ?? 0;
                                }
                            }
                            else
                            {
                                exrateSuspense = h.EXRATE_SUSPENSE ?? 0;
                            }

                            decimal? DiffExrate = null;
                            if (h.PV_TYPE_CD == 3 && !(h.FROM_CURR.isEmpty()))
                            {
                                if ((h.EXCHANGE_RATE ?? 0) > 0)
                                {
                                    decimal difExrateRatio = 1;
                                    decimal difExrate = exrateSuspense / (h.EXCHANGE_RATE ?? 1);

                                    difExrateRatio = difExrateRatio - difExrate;
                                    DiffExrate = Math.Round(h.AMOUNT * difExrateRatio, 2);
                                }
                            }

                            PVPostInputHeader _header = new PVPostInputHeader()
                            {
                                PV_NO = h.PV_NO.str(),
                                PV_YEAR = Year,
                                SEQ_NO = (((h.PV_TYPE_CD ?? 0) == 3) ? (h.ITEM_NO ?? i) : i),
                                PV_DATE = h.PV_DATE.SAPdate(),
                                PV_TYPE = h.PV_TYPE_CD.str(),

                                PV_NO_SUSPENSE = h.SUSPENSE_NO.str(),
                                PV_YEAR_SUSPENSE = h.SUSPENSE_YEAR.str(),
                                PV_DATE_SUSPENSE = GetSuspenseDate(h.SUSPENSE_NO.Int(), h.SUSPENSE_YEAR.Int()),
                                TRANS_TYPE = h.TRANSACTION_CD.str(),

                                VENDOR = h.VENDOR_CD,
                                VENDOR_GROUP = Convert.ToString(h.VENDOR_GROUP_CD),

                                INVOICE_NO = h.INVOICE_NO,
                                TAX_NO = h.TAX_NO,

                                PAYMENT_TERM = h.PAYMENT_TERM,
                                PAYMENT_METHOD = h.PAY_METHOD_CD,
                                PLAN_PAYMENT_DATE = (h.PLANNING_PAYMENT_DATE != null)
                                    ? ((DateTime)h.PLANNING_PAYMENT_DATE).SAPdate()
                                    : null,
                                POSTING_DATE = (h.POSTING_DATE != null)
                                    ? h.POSTING_DATE.SAPdate()
                                    : DateTime.Now.SAPdate(),

                                DPP_AMOUNT = (h.DPP_AMOUNT ?? 0),
                                CURRENCY_CD = h.CURRENCY_CD,
                                TAX_CD = h.TAX_CD,
                                HEADER_TEXT = HeaderText,
                                BANK_TYPE = h.BANK_TYPE.str(),
                                FROM_CURR = h.FROM_CURR,
                                EXCHANGE_RATE = h.EXCHANGE_RATE,
                                EXRATE_SUSPENSE = exrateSuspense,
                                DIFF_EXRATE = DiffExrate,
                                NO_SUSPENSE_FLAG = noSuspenseFlag,

                                COST_CENTER_FLAG = h.COST_CENTER_FLAG,
                                TAX_DT = h.TAX_DT,
                                TAX_ASSIGNMENT = h.TAX_ASSIGNMENT,
                                ACC_INV_ASSIGNMENT = h.ACC_INF_ASSIGNMENT

                            };

                            List<int> addedDetails = new List<int>();

                            while (qi < hCount && ckey == lastKey)
                            {
                                decimal? spent = 0;
                                if (AppSetting.RoundCurrency.Contains(h.CURRENCY_CD) && h.SPENT_AMOUNT != null)
                                {
                                    if (h.PV_TYPE_CD == 3)
                                    {
                                        /// refill spent with rounding fraction to balance 
                                        decimal spentAdd = h.SPENT_AMOUNT.Frac() + DiffExrate.Frac() - h.AMOUNT.Frac() - h.SUSPENSE_AMOUNT.Frac();
                                        spent = decimal.Truncate(h.SPENT_AMOUNT ?? 0) + Math.Round(spentAdd);
                                        log.Log("MSTD0001INF", string.Format("[{0}] spent add={1} spent={2} SPENT_AMOUNT={3}, DIFF_EXRATE={4} SETTLE={5}, SUSPENSE_AMOUNT={6}",
                                        h.SEQ_NO, spentAdd, spent, h.SPENT_AMOUNT, DiffExrate, h.AMOUNT, h.SUSPENSE_AMOUNT), _loc);
                                    }
                                    else
                                    {
                                        spent = decimal.Truncate(h.SPENT_AMOUNT ?? 0);
                                    }
                                }
                                else
                                {
                                    spent = h.SPENT_AMOUNT;
                                }

                                TotalAmount += h.AMOUNT;
                                TotalSpent += (spent ?? 0);

                                decimal? Amount = h.AMOUNT;
                                if (h.PV_TYPE_CD == 3)
                                    Amount = (spent ?? 0);

                                PVPostInputDetail _detail = new PVPostInputDetail()
                                {
                                    INVOICE_NO = h.INVOICE_NO,
                                    PV_NO = h.PV_NO.str(),
                                    PV_YEAR = h.PV_YEAR.str(),
                                    SEQ_NO = i,
                                    GL_ACCOUNT = h.GL_ACCOUNT.str(),
                                    AMOUNT = Amount,
                                    SPENT_AMOUNT = spent,
                                    COST_CENTER = h.COST_CENTER_CD.str(),
                                    WBS_ELEMENT = h.WBS_NO,
                                    // ITEM_TEXT = d.STD_WORDING + " " + d.DESCRIPTION, 20130108 dan -  sap cannot handle more than 100 chars for this column

                                    ITEM_TEXT = h.DESCRIPTION,
                                    CURRENCY_CD = h.CURRENCY_CD,
                                    TAX_CD = h.TAX_CD,
                                    PV_TYPE = (int)h.PV_TYPE_CD,
                                    PV_DATE = h.PV_DATE
                                };
                                if (h.PV_TYPE_CD == 3)
                                    _detail.SEQ_NO = h.ITEM_NO ?? i;

                                po.Add(new PostItemData()
                                {
                                    DOC_NO = h.PV_NO,
                                    DOC_YEAR = h.PV_YEAR,
                                    SEQ_NO = h.SEQ_NO,
                                    ITEM_NO = _detail.SEQ_NO
                                });

                                _InputDetails.Add(_detail);
                                addedDetails.Add(_InputDetails.Count - 1);

                                if (_ListofPVDetail.IndexOf(_NO + _YEAR) < 0)
                                {
                                    log.Log(_INF, "Detail :" + _NO + _YEAR, _loc, uid, "", _ProcessId);
                                    _ListofPVDetail.Add(_NO + _YEAR);
                                }

                                qi++;
                                if (qi < hCount)
                                {
                                    h = q[qi];
                                    ckey = string.Format("{0}:{1}:{2}:{3}:{4}"
                                        , h.PV_NO, h.PV_YEAR
                                        , h.INVOICE_NO, h.CURRENCY_CD, h.FROM_CURR);
                                }

                            }

                            _header.TOTAL_AMOUNT = TotalAmount;

                            //if (h.PV_TYPE_CD == 3)
                            //    _header.SEQ_NO = h.ITEM_NO ?? i;
                            if (((h.PV_TYPE_CD == 1 || h.PV_TYPE_CD == 2) && (TotalAmount <= 0))
                                 || (h.PV_TYPE_CD == 3 && TotalAmount <= 0 && TotalSpent <= 0)
                               )
                            {
                                // LoggingLogic.say("", "added details: {0} Input Details: {1}", addedDetails.Count, _InputDetails.Count);

                                /// do not post, delete detail 
                                for (int adi = addedDetails.Count - 1; adi >= 0; adi--)
                                {
                                    // LoggingLogic.say("", "i={0} addedDetails[i]={1}", adi, addedDetails[adi]);
                                    // LoggingLogic.say("", "      RemoveAt({0})", addedDetails[adi]);
                                    _InputDetails.RemoveAt(addedDetails[adi]);
                                }
                            }
                            else
                            {
                                UpdateDetailItemNo(db, po);
                                _InputHeaders.Add(_header);
                                i++;
                            }

                        }

                        OneTimeVendorData otv = null;
                        var qo = (from v in db.TB_R_ONE_TIME_VENDOR
                                  where v.PV_NO == no && v.PV_YEAR == year
                                  select v).FirstOrDefault();
                        if (qo != null)
                        {
                            otv = new OneTimeVendorData()
                            {
                                DOC_NO = qo.PV_NO,
                                DOC_YEAR = qo.PV_YEAR,
                                TITLE = qo.TITLE,
                                NAME1 = qo.NAME1,
                                NAME2 = qo.NAME2,
                                STREET = qo.STREET,
                                CITY = qo.CITY,
                                BANK_KEY = qo.BANK_KEY,
                                BANK_ACCOUNT = qo.BANK_ACCOUNT
                            };

                        }
                        if (otv != null) _InputVendors.Add(otv);
                    }

                    /// post and examine results 
                    result = PostAndSaveDocNo(_ProcessId, uid, _InputHeaders,
                        _InputDetails, _InputVendors, fails, succeeds, errs, results);

                    if (fails.Count > 0 && succeeds.Count > 0)
                    {
                        _Message = m.Message("MSTD00079INF",
                                CommonFunction.CommaJoin(succeeds),
                                CommonFunction.CommaJoin(fails));
                        log.Log("MSTD00079INF", _Message, _loc, uid, "", _ProcessId);
                    }
                    else if (fails.Count > 0)
                    {
                        _Message = m.Message("MSTD00068ERR", "PV");
                        log.Log("MSTD00068ERR", _Message, _loc, uid, "", _ProcessId);
                    }
                    else
                    {
                        _Message = m.Message("MSTD00067INF", "PV");
                        log.Log("MSTD00067INF", _Message, _loc, uid, "", _ProcessId);
                    }

                    StringBuilder serr = new StringBuilder("POST ErrorMessages:\r\n");
                    for (int i = 0; i < errs.Count - 1; i++)
                    {
                        serr.AppendLine(((i < _Docs.Count) ? _Docs[i] : "?") + "\t" + errs[i]);
                    }

                    if (succeeds.Count > 0)
                    {
                        foreach (string ok in succeeds)
                        {
                            BudgetRelease(ok);
                        }
                    }

                    log.Log(_INF, serr.ToString(), _loc, uid, "", _ProcessId);
                }
                catch (Exception ex)
                {
                    _Message = ex.Message;
                    log.Log("MSTD00076ERR", _Message, _loc, uid, "", _ProcessId);

                    LoggingLogic.err(ex);
                }
            log.Log(_INF, "End " + _loc, _loc, uid, "", _ProcessId);

            return result;
        }

        ///Added by Akhmad Nuryanto, 6/6/2012
        ///Post to SAP
        public bool PVCheck(List<string> _ListofPVListData, int _ProcessId, ref string _Message, string uid = "", int method = SAP_CHECK_BY_ACCOUNTING)
        {
            string _loc = string.Format("PVCheck({0})", method);
            // int checkedByAccStatus = 29;
            bool result = false;
            List<PVPostInputHeader> checkData = new List<PVPostInputHeader>();
            List<PVPostSAPResult> results = new List<PVPostSAPResult>();

            List<string> _isChecked = new List<string>();
            List<string> _isNotReadyToBeChecked = new List<string>();
            List<string> _isPaid = new List<string>();
            List<string> _FailedDocuments = new List<string>();
            List<string> _SucceedDocuments = new List<string>();

            this["OK"] = _SucceedDocuments;
            this["NG"] = _FailedDocuments;
            this["done"] = _isChecked;
            this["paid"] = _isPaid;
            this["notready"] = _isNotReadyToBeChecked;
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var db = co.db;
                    foreach (string reff in _ListofPVListData)
                    {
                        string[] nodanyear = reff.ToString().Split(':');
                        Int64 no = Int64.Parse(nodanyear[0]);
                        int year = int.Parse(nodanyear[1]);

                        string reff_no = reff.Replace(":", "");
                        //LoggingLogic.say(reff_no, "PVCheck");
                        var q = (from p in db.vw_PVCheckInput
                                 where p.PV_NO == no && p.PV_YEAR == year && p.ITEM_NO != null
                                 select p);
                        if (q.Any())
                        {
                            q = q.OrderBy(x => x.ITEM_NO)
                                .ThenBy(x => x.FROM_CURR);

                            string key;
                            int i = 1;
                            string lastKey = "";
                            PVPostInputHeader h;

                            List<vw_PVCheckInput> lic = q.ToList();
                            int ci = 0;

                            var data = lic[0];

                            key = data.PV_NO.str() + data.PV_YEAR.str();
                            if (!lastKey.Equals(key))
                            {
                                i = 1;
                                lastKey = key;
                            }
                            if (!string.IsNullOrEmpty(data.SAP_CLEARING_DOC_NO)) // already been checked
                            {
                                if (!_isChecked.Contains(key))
                                {
                                    _isChecked.Add(key);
                                }
                            }
                            else if (method == SAP_CHECK_BY_ACCOUNTING
                                    && data.ACTUAL_DT == null) // is not ready to be checked (not yet paid)
                            {
                                if (!_isNotReadyToBeChecked.Contains(key))
                                {
                                    _isNotReadyToBeChecked.Add(key);
                                }
                            }
                            else if (method == SAP_PAID_BY_CASHIER
                                    && data.ACTUAL_DT != null) // already checked 
                            {
                                if (!_isPaid.Contains(key))
                                {
                                    _isPaid.Add(key);
                                }
                            }

                            while (ci < lic.Count)
                            {
                                data = lic[ci];

                                int lastItem = data.ITEM_NO ?? i;
                                int SEQ_NO;
                                SEQ_NO = data.ITEM_NO ?? i;
                                if (SEQ_NO <= 0)
                                    SEQ_NO = i;

                                h = new PVPostInputHeader()
                                {
                                    PV_NO = Convert.ToString(data.PV_NO),
                                    PV_YEAR = data.PV_YEAR.str(),
                                    SEQ_NO = SEQ_NO,
                                    PV_DATE = (data.PV_DATE == null) ? "" : (data.PV_DATE ?? DateTime.Now).ToString("dd.MM.yyyy"),
                                    PV_TYPE = Convert.ToString(data.PV_TYPE_CD),
                                    VENDOR = data.VENDOR_CD,
                                    INVOICE_NO = data.INVOICE_NO,
                                    CURRENCY_CD = data.CURRENCY_CD,
                                    HEADER_TEXT = Textuality(data.STD_WORDING, data.DESCRIPTION),
                                    TRANSACTION_CD = (int)data.TRANSACTION_CD,
                                    POSTING_DATE = (method == SAP_CHECK_BY_ACCOUNTING) ? data.PAID_DATE.SAPdate() : DateTime.Now.SAPdate()
                                };
                                // LoggingLogic.say(reff_no, "[{0,3}] SEQ_NO={1,3}\t{2,4} TOTAL={3}\tSPENT={4}", ci, h.SEQ_NO, h.CURRENCY_CD, h.TOTAL_AMOUNT, h.SPENT_AMOUNT);

                                // bedakan POSTING_DATE dengan PAID_DATE 
                                // saat Cashier paid => posting date (sysdate) 
                                // saat Check by accounting => PAID DATE 
                                // _InputHeader.POSTING_DATE = ((DateTime)data.POSTING_DATE).ToString("dd.MM.yyyy");

                                decimal amt = 0;

                                while (ci < lic.Count && data.ITEM_NO == lastItem)
                                {
                                    //LoggingLogic.say(reff_no, "[{0,3}] Amount={0} + {1}", amt, data.AMOUNT);
                                    amt += data.AMOUNT;

                                    ci++;
                                    if (ci < lic.Count)
                                        data = lic[ci];
                                }

                                h.TOTAL_AMOUNT = amt;
                                //LoggingLogic.say(reff_no, "\t{0,3}\t{1,4} {2}\t{3}", h.SEQ_NO, h.CURRENCY_CD, h.TOTAL_AMOUNT, h.SPENT_AMOUNT);

                                checkData.Add(h);
                                i++;
                            }
                        }
                    }

                    if (_isChecked.Count > 0)
                    {
                        _Message = m.Message("MSTD00085ERR",
                            CommonFunction.CommaJoin(_isChecked));
                        logic.Log.Log("MSTD00085ERR", _Message, _loc, uid, "", _ProcessId);

                        return false;
                    }
                    else if (_isNotReadyToBeChecked.Count > 0)
                    {
                        _Message = m.Message("MSTD00086ERR",
                            CommonFunction.CommaJoin(_isNotReadyToBeChecked));
                        logic.Log.Log("MSTD00086ERR", _Message, _loc, uid, "", _ProcessId);

                        return false;
                    }
                    else if (_isPaid.Count > 0)
                    {
                        _Message = m.Message("MSTD00094WRN",
                            CommonFunction.CommaJoin(_isPaid));
                        logic.Log.Log("MSTD00094WRN", _Message, _loc, uid, "", _ProcessId);

                        return false;
                    }

                    if (checkData.Count > 0)
                    {
                        results = logic.SAP.GetResultCheckedByAcc(checkData, _ProcessId, method);
                        if (results.Count > 0)
                        {
                            List<PVPostSAPResult> itemSuccess = new List<PVPostSAPResult>();
                            foreach (PVPostSAPResult item in results)
                            {
                                string key = item.PV_NO + item.PV_YEAR;
                                if (item.MESSAGE == "S")
                                {
                                    //int DocNo = Convert.ToInt32(item.PV_NO);
                                    //int DocYear = Convert.ToInt32(item.PV_YEAR);

                                    itemSuccess.Add(item);
                                    if (!_SucceedDocuments.Contains(key))
                                    {
                                        _SucceedDocuments.Add(key);
                                    }

                                }
                                else
                                {
                                    if (!_FailedDocuments.Contains(key))
                                    {
                                        _FailedDocuments.Add(key);
                                    }
                                }
                            }


                            foreach (string k in _FailedDocuments)
                            {
                                /// remove success no document contained 
                                if (_SucceedDocuments.Contains(k))
                                {
                                    _SucceedDocuments.RemoveAt(_SucceedDocuments.IndexOf(k));
                                }
                            }

                            result = _SucceedDocuments.Count > 0;

                            foreach (PVPostSAPResult item in itemSuccess)
                            {
                                UpdateSuccessDoc(item);
                            }

                            if (_FailedDocuments.Count > 0 && _SucceedDocuments.Count > 0)
                            {
                                _Message = m.Message("MSTD00087INF",
                                    CommonFunction.CommaJoin(_SucceedDocuments),
                                    CommonFunction.CommaJoin(_FailedDocuments));
                                logic.Log.Log("MSTD00087INF", _Message, _loc, uid, "", _ProcessId);

                            }
                            else if (_FailedDocuments.Count > 0)
                            {
                                _Message = m.Message("MSTD00070ERR", "PV");
                                logic.Log.Log("MSTD00070ERR", _Message, _loc, uid, "", _ProcessId);

                            }
                            else
                            {
                                _Message = m.Message("MSTD00069INF", "PV");

                                logic.Log.Log("MSTD00069INF", _Message, _loc, uid, "", _ProcessId);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _Message = ex.Message;
                    logic.Log.Log("MSTD00076ERR", _Message, _loc, uid, "", _ProcessId);
                    LoggingLogic.err(ex);
                }

            return result;
        }

        private bool UpdateSuccessDoc(PVPostSAPResult p)
        {
            int _no = p.PV_NO.Int();
            int _yy = p.PV_YEAR.Int();
            int _it = p.ITEM_NO.Int();

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var r = (from s in db.TB_R_SAP_DOC_NO
                         where s.DOC_NO == _no && s.DOC_YEAR == _yy && s.ITEM_NO == _it
                         select s).FirstOrDefault();
                int u = 0;
                if (r != null)
                {
                    if (!p.SAP_CLEARING_DOC_NO.isEmpty())
                    {
                        r.SAP_CLEARING_DOC_NO = p.SAP_CLEARING_DOC_NO;
                        r.SAP_CLEARING_DOC_YEAR = p.SAP_CLEARING_DOC_YEAR.isEmpty() ? null : (int?)p.SAP_CLEARING_DOC_YEAR.Int();
                        u++;
                    }
                    if (p.SAP_DOC_NO != null)
                    {
                        r.SAP_DOC_NO = p.SAP_DOC_NO;
                        r.SAP_DOC_YEAR = p.SAP_DOC_YEAR.Int();
                        u++;
                    }
                }
                else
                {
                    db.AddToTB_R_SAP_DOC_NO(new TB_R_SAP_DOC_NO()
                    {
                        DOC_NO = p.PV_NO.Int(),
                        DOC_YEAR = p.PV_YEAR.Int(),
                        ITEM_NO = p.ITEM_NO.Int(),
                        SAP_DOC_NO = p.SAP_DOC_NO,
                        SAP_DOC_YEAR = p.SAP_DOC_YEAR.Int(),
                        SAP_CLEARING_DOC_NO = p.SAP_CLEARING_DOC_NO,
                        SAP_CLEARING_DOC_YEAR = p.SAP_CLEARING_DOC_YEAR.Int()
                    });
                    u++;
                }
                if (u > 0)
                {
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        #endregion of addition by Akhmad Nuryanto



        public bool Fill(PVListData z)
        {
            int _no = z.PV_NO.Int();
            int _yy = z.PV_YEAR.Int();

            bool filled = false;


            var q = Qu<PVListData>("GetPVListData", z.PV_NO, z.PV_YEAR).FirstOrDefault();

            if (q != null)
            {
                z.PV_DATE = (DateTime)q.PV_DATE;
                z.PV_TYPE_CD = q.PV_TYPE_CD.str();
                z.DIVISION_NAME = q.DIVISION_NAME;
                z.DIVISION_ID = q.DIVISION_ID;
                z.VENDOR_CD = q.VENDOR_CD;
                z.VENDOR_NAME = q.VENDOR_NAME;
                z.STATUS_CD = q.STATUS_CD.str();
                z.TRANSACTION_CD = q.TRANSACTION_CD.str();
                z.TRANSACTION_NAME = q.TRANSACTION_NAME;
                z.TOTAL_AMOUNT_IDR = q.TOTAL_AMOUNT_IDR;
                z.PAY_METHOD_CD = q.PAY_METHOD_CD;
                z.PAY_METHOD_NAME = q.PAY_METHOD_NAME;
                z.PLANNING_PAYMENT_DATE = q.PLANNING_PAYMENT_DATE;
                z.PRINT_TICKET_BY = q.PRINT_TICKET_BY;
                z.PRINT_TICKET_FLAG = q.PRINT_TICKET_FLAG;
                z.BUDGET_NO = q.BUDGET_NO;
                z.NEXT_APPROVER = q.NEXT_APPROVER;
                z.BUDGET_DESCRIPTION = q.BUDGET_DESCRIPTION;
                z.DESCRIPTION = q.DESCRIPTION;

                List<OtherAmountData> oad = logic.Look.GetAmounts(_no, _yy);
                OtherAmountData oa = null;

                oa = oad.Where(c => c.CURRENCY_CD == "IDR").FirstOrDefault();
                z.AMOUNT_IDR = (oa != null) ? CommonFunction.Eval_Curr("IDR", oa.TOTAL_AMOUNT) : "";
                oa = oad.Where(c => c.CURRENCY_CD == "JPY").FirstOrDefault();
                z.AMOUNT_JPY = (oa != null) ? CommonFunction.Eval_Curr("JPY", oa.TOTAL_AMOUNT) : "";
                oa = oad.Where(c => c.CURRENCY_CD == "USD").FirstOrDefault();
                z.AMOUNT_USD = (oa != null) ? CommonFunction.Eval_Curr("USD", oa.TOTAL_AMOUNT) : "";

                z.DELETED = q.DELETED;
                filled = true;
            }

            return filled;
        }


        private void BudgetRelease(string key)
        {
            string[] k = key.Split(':');
            if (k.Length > 1)
            {
                int docno, docyear;
                Int32.TryParse(k[0], out docno);
                Int32.TryParse(k[1], out docyear);
                using (ContextWrap co = new ContextWrap())
                {
                    var db = co.db;
                    var q = from pv in db.TB_R_PV_H
                            where pv.PV_NO == docno // && pv.PV_YEAR == docyear 
                            orderby pv.PV_YEAR descending
                            select new
                            {
                                pv.BUDGET_NO
                                ,
                                pv.TOTAL_AMOUNT
                                ,
                                pv.PV_DATE
                            };

                    var x = q.FirstOrDefault();
                    int fiscalYear = logic.Sys.FiscalYear(x.PV_DATE);

                    BudgetCheckInput iBudget = new BudgetCheckInput()
                    {
                        DOC_STATUS = (int)BudgetCheckStatus.Release,
                        DOC_NO = docno,
                        //DOC_YEAR = docyear,
                        DOC_YEAR = fiscalYear,
                        WBS_NO = x.BUDGET_NO,
                        AMOUNT = x.TOTAL_AMOUNT ?? 0
                    };

                    BudgetCheckResult o;
                    if (!iBudget.WBS_NO.isEmpty())
                        if (Common.AppSetting.SKIP_BUDGET_CHECK)
                        {
                            o = logic.SAP._BudgetCheck_(iBudget);
                        }
                        else
                        {
                            o = logic.SAP.BudgetCheck(iBudget);
                        }

                }
            }
        }

        private void SetHistoryData(PVFormHistoryData d, PVFormHistoryData x)
        {
            d.PVNumber = x.PVNumber;
            d.PVYear = x.PVYear;
            d.Version = x.Version;
            d.StatusCode = x.StatusCode;
            d.PaymentMethodCode = x.PaymentMethodCode;
            d.VendorGroupCode = x.VendorGroupCode;
            d.VendorCode = x.VendorCode;
            d.PVTypeCode = x.PVTypeCode;
            d.TransactionCode = x.TransactionCode;
            d.SuspenseNumber = x.SuspenseNumber;
            d.BudgetNumber = x.BudgetNumber;
            d.ActivityDate = x.ActivityDate;
            d.ActivityDateTo = x.ActivityDateTo;
            d.DivisionID = x.DivisionID;
            d.PVDate = x.PVDate;
            d.TaxCode = x.TaxCode;
            d.TaxInvoiceNumber = x.TaxInvoiceNumber;
            d.PostingDate = x.PostingDate;
            d.PlanningPaymentDate = x.PlanningPaymentDate;
            d.BankType = x.BankType;
            d.TaxCalculated = x.TaxCalculated;
            d.SingleWbsUsed = x.SingleWbsUsed;
            d.Version = x.Version;
            d.UpdatedDt = x.UpdatedDt;
            d.UpdatedBy = x.UpdatedBy;
            d.PVTypeName = x.PVTypeName;
            d.VendorGroupName = x.VendorGroupName;
            d.PaymentMethodName = x.PaymentMethodName;
            d.StatusName = x.StatusName;
            d.VendorName = x.VendorName;
            d.TransactionName = x.TransactionName;
            d.SetNoPv = x.SetNoPv;
            d.SetNoRv = x.SetNoRv;
            d.SubmitDate = x.SubmitDate;
            d.PaidDate = x.PaidDate;
            d.WorkflowStatus = x.WorkflowStatus;
            d.SettlementStatus = x.SettlementStatus;
            d.ReferenceNo = x.ReferenceNo;
            //d.LocationCode = x.LocationCode;
            d.SubmitHcDocDate = x.SubmitHcDocDate;
            d.HoldFlag = x.HoldFlag;
            d.HoldBy = x.HoldBy;
            d.ModifiedBy = x.ModifiedBy;
            d.TotalAmount = x.TotalAmount;
            d.Details = GetDetailHistory(x.Details, false);
        }

        private static List<PVFormHistoryDetail> GetDetailHistory(List<PVFormHistoryDetail> x, bool forDelete)
        {
            if (x != null)
            {
                List<PVFormHistoryDetail> y = new List<PVFormHistoryDetail>();
                foreach (var z in x)
                {
                    PVFormHistoryDetail dt = new PVFormHistoryDetail()
                    {
                        SequenceNumber = z.SequenceNumber,
                        PVNumber = z.PVNumber,
                        PVYear = z.PVYear,
                        Version = z.Version,
                        CostCenterCode = z.CostCenterCode,
                        CostCenterCodeChange = z.CostCenterCodeChange,
                        CostCenterName = z.CostCenterName,
                        CostCenterNameChange = z.CostCenterNameChange,
                        Description = z.Description,
                        DescriptionChange = z.DescriptionChange,
                        CurrencyCode = z.CurrencyCode,
                        CurrencyCodeChange = z.CurrencyCodeChange,
                        Amount = z.Amount,
                        AmountChange = z.AmountChange,
                        TaxCode = z.TaxCode,
                        TaxCodeChange = z.TaxCodeChange,
                        TaxNumber = z.TaxNumber,
                        TaxNumberChange = z.TaxNumberChange,
                        ItemTransactionCode = z.ItemTransactionCode,
                        ItemTransactionCodeChange = z.ItemTransactionCodeChange,
                        ItemTransactionName = z.ItemTransactionName,
                        ItemTransactionNameChange = z.ItemTransactionNameChange,
                        //WbsNumber = z.WbsNumber,
                        //WbsNumberChange = z.WbsNumberChange,
                        InvoiceNumber = z.InvoiceNumber,
                        InvoiceNumberChange = z.InvoiceNumberChange,
                        IsDeleted = forDelete
                    };

                    y.Add(dt);
                }

                return y;
            }
            return null;
        }

        private static PVFormHistoryDetail GetDetailHistoryObj(PVFormHistoryDetail z)
        {
            if (z != null)
            {
                PVFormHistoryDetail dt = new PVFormHistoryDetail()
                {
                    SequenceNumber = z.SequenceNumber,
                    PVNumber = z.PVNumber,
                    PVYear = z.PVYear,
                    Version = z.Version,
                    CostCenterCode = z.CostCenterCode,
                    CostCenterCodeChange = z.CostCenterCodeChange,
                    CostCenterName = z.CostCenterName,
                    CostCenterNameChange = z.CostCenterNameChange,
                    Description = z.Description,
                    DescriptionChange = z.DescriptionChange,
                    CurrencyCode = z.CurrencyCode,
                    CurrencyCodeChange = z.CurrencyCodeChange,
                    Amount = z.Amount,
                    AmountChange = z.AmountChange,
                    TaxCode = z.TaxCode,
                    TaxCodeChange = z.TaxCodeChange,
                    TaxNumber = z.TaxNumber,
                    TaxNumberChange = z.TaxNumberChange,
                    ItemTransactionCode = z.ItemTransactionCode,
                    ItemTransactionCodeChange = z.ItemTransactionCodeChange,
                    ItemTransactionName = z.ItemTransactionName,
                    ItemTransactionNameChange = z.ItemTransactionNameChange,
                    //WbsNumber = z.WbsNumber,
                    //WbsNumberChange = z.WbsNumberChange,
                    InvoiceNumber = z.InvoiceNumber,
                    InvoiceNumberChange = z.InvoiceNumberChange,
                    IsDeleted = z.IsDeleted
                };

                return dt;
            }
            return null;
        }

        private static List<PVFormHistoryDetail> GetChangedDetailValue(List<PVFormHistoryDetail> det)
        {
            List<PVFormHistoryDetail> tmp = null;
            if (det != null && det.Any())
            {
                tmp = new List<PVFormHistoryDetail>();
                foreach (var d in det)
                {
                    d.CostCenterCodeChange = true;
                    d.CostCenterNameChange = true;
                    d.DescriptionChange = true;
                    d.CurrencyCodeChange = true;
                    d.AmountChange = true;
                    d.TaxCodeChange = true;
                    d.TaxNumberChange = true;
                    d.ItemTransactionCodeChange = true;
                    d.ItemTransactionNameChange = true;
                    //d.WbsNumberChange = true;
                    d.InvoiceNumberChange = true;
                    tmp.Add(d);
                }
            }
            return tmp;
        }

        private static PVFormHistoryDetail GetChangedDetailValue(PVFormHistoryDetail d)
        {
            if (d != null)
            {
                d.CostCenterCodeChange = true;
                d.CostCenterNameChange = true;
                d.DescriptionChange = true;
                d.CurrencyCodeChange = true;
                d.AmountChange = true;
                d.TaxCodeChange = true;
                d.TaxNumberChange = true;
                d.ItemTransactionCodeChange = true;
                d.ItemTransactionNameChange = true;
                //d.WbsNumberChange = true;
                d.InvoiceNumberChange = true;
            }
            return d;
        }

        public List<PVFormHistoryData> GetHistoryData(String pvNoParam, String pvYearParam)
        {
            List<PVFormHistoryData> returnData = new List<PVFormHistoryData>();


            int pvNo = Convert.ToInt32(pvNoParam);
            int pvYear = Convert.ToInt16(pvYearParam);

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var qq = from p in db.TB_H_PV_H

                         join ve in db.vw_Vendor on p.VENDOR_CD equals ve.VENDOR_CD into vc
                         from vcItem in vc.DefaultIfEmpty()

                         join vg in db.TB_M_VENDOR_GROUP on p.VENDOR_GROUP_CD equals vg.VENDOR_GROUP_CD into vgGroup
                         from vgItem in vgGroup.DefaultIfEmpty()

                         join t in db.TB_M_TRANSACTION_TYPE on p.TRANSACTION_CD equals t.TRANSACTION_CD into tty
                         from ttyItem in tty.DefaultIfEmpty()

                         join pm in db.TB_M_PAYMENT_METHOD on p.PAY_METHOD_CD equals pm.PAY_METHOD_CD into pmtd
                         from pmtdItem in pmtd.DefaultIfEmpty()

                         join s in db.TB_M_STATUS on p.STATUS_CD equals s.STATUS_CD into sts
                         from stsItem in sts.DefaultIfEmpty()

                         join pvt in db.TB_M_PV_TYPE on p.PV_TYPE_CD equals pvt.PV_TYPE_CD into pty
                         from ptyItem in pty.DefaultIfEmpty()

                         join u in db.vw_User on p.CHANGED_BY equals u.USERNAME into vwUser
                         from vu in vwUser.DefaultIfEmpty()

                         where (p.PV_NO.Equals(pvNo) && p.PV_YEAR.Equals(pvYear))
                         orderby p.VERSION
                         select new
                         {
                             p.VERSION,
                             p.CHANGED_DATE,
                             p.CHANGED_BY,
                             vu.LAST_NAME,
                             p.PV_DATE,
                             p.PV_YEAR,
                             p.PV_NO,
                             p.REFFERENCE_NO,
                             p.ACTIVITY_DATE_FROM,
                             p.ACTIVITY_DATE_TO,
                             p.POSTING_DATE,
                             p.PV_TYPE_CD,
                             ptyItem.PV_TYPE_NAME,
                             p.DIVISION_ID,
                             p.VENDOR_GROUP_CD,
                             vgItem.VENDOR_GROUP_NAME,
                             p.BANK_TYPE,
                             p.PLANNING_PAYMENT_DATE,
                             p.STATUS_CD,
                             stsItem.STATUS_NAME,
                             p.TRANSACTION_CD,
                             ttyItem.TRANSACTION_NAME,
                             p.VENDOR_CD,
                             vcItem.VENDOR_NAME,
                             p.PAY_METHOD_CD,
                             pmtdItem.PAY_METHOD_NAME,
                             p.BUDGET_NO,
                             p.SUSPENSE_NO,
                             p.SET_NO_PV,
                             p.SET_NO_RV,
                             p.SUBMIT_DATE,
                             p.PAID_DATE,
                             p.WORKFLOW_STATUS,
                             p.SETTLEMENT_STATUS,
                             //p.LOCATION_CD,
                             p.SUBMIT_HC_DOC_DATE,
                             p.HOLD_BY,
                             p.HOLD_FLAG,
                             p.TOTAL_AMOUNT
                         };

                PVFormHistoryData old = null;
                foreach (var q in qq)
                {
                    PVFormHistoryData _new = new PVFormHistoryData();
                    PVFormHistoryData x = new PVFormHistoryData();

                    int TransactionCd = q.TRANSACTION_CD ?? -1;

                    _new.Version = q.VERSION;
                    _new.PVNumber = q.PV_NO;
                    _new.PVYear = q.PV_YEAR;
                    _new.UpdatedDt = q.CHANGED_DATE;
                    _new.UpdatedBy = q.CHANGED_BY;
                    _new.PVDate = q.PV_DATE;
                    _new.DivisionID = q.DIVISION_ID;

                    _new.PVTypeCode = q.PV_TYPE_CD;
                    _new.PVTypeName = q.PV_TYPE_NAME;
                    _new.VendorGroupCode = q.VENDOR_GROUP_CD;
                    _new.VendorGroupName = q.VENDOR_GROUP_NAME;
                    _new.PaymentMethodCode = q.PAY_METHOD_CD;
                    _new.PaymentMethodName = q.PAY_METHOD_NAME;
                    _new.StatusCode = q.STATUS_CD;
                    _new.StatusName = q.STATUS_NAME;
                    _new.VendorCode = q.VENDOR_CD;
                    _new.VendorName = q.VENDOR_NAME;
                    _new.TransactionCode = q.TRANSACTION_CD;
                    _new.TransactionName = q.TRANSACTION_NAME;
                    _new.ActivityDate = q.ACTIVITY_DATE_FROM;
                    _new.ActivityDateTo = q.ACTIVITY_DATE_TO;
                    _new.PostingDate = q.POSTING_DATE;
                    _new.PlanningPaymentDate = q.PLANNING_PAYMENT_DATE;
                    _new.BankType = q.BANK_TYPE;
                    _new.BudgetNumber = q.BUDGET_NO;
                    _new.SetNoPv = q.SET_NO_PV;
                    _new.SetNoRv = q.SET_NO_RV;
                    _new.SubmitDate = q.SUBMIT_DATE;
                    _new.PaidDate = q.PAID_DATE;
                    _new.WorkflowStatus = q.WORKFLOW_STATUS;
                    _new.SettlementStatus = q.SETTLEMENT_STATUS;
                    _new.ReferenceNo = q.REFFERENCE_NO;
                    //_new.LocationCode = q.LOCATION_CD;
                    _new.SubmitHcDocDate = q.SUBMIT_HC_DOC_DATE;
                    _new.HoldBy = q.HOLD_BY;
                    _new.HoldFlag = q.HOLD_FLAG;
                    _new.ModifiedBy = q.LAST_NAME;
                    _new.TotalAmount = q.TOTAL_AMOUNT;

                    // get detail
                    List<PVFormHistoryDetail> historyDetail = null;
                    var dd = from d in db.TB_H_PV_D
                             join c in db.vw_CostCenter on d.COST_CENTER_CD equals c.COST_CENTER into cctr
                             from subCctr in cctr.DefaultIfEmpty()
                             join it in db.TB_M_ITEM_TRANSACTION
                                on new { tcode = TransactionCd, titem = d.ITEM_TRANSACTION_CD ?? -1 }
                                equals new { tcode = it.TRANSACTION_CD, titem = it.ITEM_TRANSACTION_CD }
                             into itTrans
                             from subIt in itTrans.DefaultIfEmpty()
                             where (d.PV_NO.Equals(pvNo) && d.PV_YEAR.Equals(pvYear) && d.VERSION.Equals(_new.Version))
                             orderby d.SEQ_NO
                             select new
                             {
                                 d.SEQ_NO,
                                 d.VERSION,
                                 d.PV_YEAR,
                                 d.PV_NO,
                                 d.COST_CENTER_CD,
                                 // COST_CENTER_NAME = d.COST_CENTER_CD,
                                 COST_CENTER_NAME = subCctr.DESCRIPTION,
                                 d.CURRENCY_CD,
                                 d.DESCRIPTION,
                                 d.AMOUNT,
                                 // d.WBS_NO,
                                 d.INVOICE_NO,
                                 d.ITEM_TRANSACTION_CD,
                                 subIt.ITEM_TRANSACTION_DESC,
                                 d.TAX_CD,
                                 d.TAX_NO
                             };

                    if (dd.Any())
                    {
                        historyDetail = new List<PVFormHistoryDetail>();
                        foreach (var d in dd)
                        {
                            PVFormHistoryDetail dt = new PVFormHistoryDetail()
                            {
                                SequenceNumber = d.SEQ_NO,
                                PVNumber = d.PV_NO,
                                PVYear = d.PV_YEAR,
                                Version = d.VERSION,
                                CostCenterCode = d.COST_CENTER_CD,
                                CostCenterCodeChange = false,
                                CostCenterName = d.COST_CENTER_NAME,
                                CostCenterNameChange = false,
                                Description = d.DESCRIPTION,
                                DescriptionChange = false,
                                CurrencyCode = d.CURRENCY_CD,
                                CurrencyCodeChange = false,
                                Amount = d.AMOUNT,
                                AmountChange = false,
                                TaxCode = d.TAX_CD,
                                TaxCodeChange = false,
                                TaxNumber = d.TAX_NO,
                                TaxNumberChange = false,
                                ItemTransactionCode = d.ITEM_TRANSACTION_CD,
                                ItemTransactionCodeChange = false,
                                ItemTransactionName = d.ITEM_TRANSACTION_DESC,
                                ItemTransactionNameChange = false,
                                //WbsNumber = d.WBS_NO,
                                //WbsNumberChange = false,
                                InvoiceNumber = d.INVOICE_NO,
                                InvoiceNumberChange = false,
                                IsDeleted = false
                            };
                            historyDetail.Add(dt);
                        }

                        _new.Details = historyDetail;
                    }

                    SetHistoryData(x, _new);

                    if (old != null)
                    {
                        x.Version = _new.Version;
                        x.PVNumber = _new.PVNumber;
                        x.PVYear = _new.PVYear;

                        if (_new.PVDate.Equals(old.PVDate))
                            x.PVDate = null;

                        if (_new.DivisionID == null || _new.DivisionID.Equals(old.DivisionID))
                            x.DivisionID = null;

                        if (_new.PVTypeCode.Equals(old.PVTypeCode))
                        {
                            x.PVTypeCode = null;
                            x.PVTypeName = null;
                        }

                        if (_new.VendorGroupCode.Equals(old.VendorGroupCode))
                        {
                            x.VendorGroupCode = null;
                            x.VendorGroupName = null;
                        }

                        if (_new.PaymentMethodCode == null || _new.PaymentMethodCode.Equals(old.PaymentMethodCode))
                        {
                            x.PaymentMethodCode = null;
                            x.PaymentMethodName = null;
                        }

                        if (_new.StatusCode.Equals(old.StatusCode))
                        {
                            x.StatusCode = null;
                            x.StatusName = null;
                        }

                        if (_new.VendorCode == null || _new.VendorCode.Equals(old.VendorCode))
                        {
                            x.VendorCode = null;
                            x.VendorName = null;
                        }

                        if (_new.TransactionCode.Equals(old.TransactionCode))
                        {
                            x.TransactionCode = null;
                            x.TransactionName = null;
                        }

                        if (_new.ActivityDate.Equals(old.ActivityDate)) x.ActivityDate = null;
                        if (_new.ActivityDateTo.Equals(old.ActivityDateTo)) x.ActivityDateTo = null;
                        if (_new.PostingDate.Equals(old.PostingDate)) x.PostingDate = null;
                        if (_new.PlanningPaymentDate.Equals(old.PlanningPaymentDate)) x.PlanningPaymentDate = null;
                        if (_new.BankType.Equals(old.BankType)) x.BankType = null;
                        if (_new.BudgetNumber == null || _new.BudgetNumber.Equals(old.BudgetNumber)) x.BudgetNumber = null;
                        if (_new.SuspenseNumber.Equals(old.SuspenseNumber)) x.SuspenseNumber = null;
                        if (_new.SetNoPv.Equals(old.SetNoPv)) x.SetNoPv = null;
                        if (_new.SetNoRv.Equals(old.SetNoRv)) x.SetNoRv = null;
                        if (_new.SubmitDate.Equals(old.SubmitDate)) x.SubmitDate = null;
                        if (_new.PaidDate.Equals(old.PaidDate)) x.PaidDate = null;
                        if (_new.WorkflowStatus.Equals(old.WorkflowStatus)) x.WorkflowStatus = null;
                        if (_new.SettlementStatus.Equals(old.SettlementStatus)) x.SettlementStatus = null;
                        if (_new.ReferenceNo.Equals(old.ReferenceNo)) x.ReferenceNo = null;
                        //if (_new.LocationCode.Equals(old.LocationCode))
                        //{
                        //    x.LocationCode = null;
                        //}
                        if (_new.SubmitHcDocDate.Equals(old.SubmitHcDocDate)) x.SubmitHcDocDate = null;
                        if (_new.HoldBy == null || _new.HoldBy.Equals(old.HoldBy)) x.HoldBy = null;
                        if (_new.HoldFlag.Equals(old.HoldFlag)) x.HoldFlag = null;
                        if (_new.TotalAmount.Equals(old.TotalAmount)) x.TotalAmount = null;

                        // Compare details
                        if (_new.Details == null && old.Details != null)
                            x.Details = GetDetailHistory(old.Details, true);
                        else if (_new.Details != null && old.Details == null)
                            x.Details = GetChangedDetailValue(_new.Details);
                        else if (_new.Details != null && old.Details != null)
                        {
                            List<PVFormHistoryDetail> tmp = new List<PVFormHistoryDetail>();
                            PVFormHistoryDetail detTmp = new PVFormHistoryDetail();
                            foreach (var n in _new.Details)
                            {
                                bool isAdded = true;
                                foreach (var o in old.Details)
                                {
                                    if (o.SequenceNumber.Equals(n.SequenceNumber))
                                    {
                                        isAdded = false;
                                        bool removeFromList = true;
                                        detTmp = GetDetailHistoryObj(n);
                                        if ((n.CostCenterCode == null && o.CostCenterCode != null) ||
                                            (n.CostCenterCode != null && !n.CostCenterCode.Equals(o.CostCenterCode)))
                                        {
                                            detTmp.CostCenterCodeChange = true;
                                            detTmp.CostCenterNameChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.Description == null && o.Description != null) ||
                                            (n.Description != null && !n.Description.Equals(o.Description)))
                                        {
                                            detTmp.DescriptionChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.StandardDescriptionWording == null && o.StandardDescriptionWording != null) ||
                                            (n.StandardDescriptionWording != null && !n.StandardDescriptionWording.Equals(o.StandardDescriptionWording)))
                                        {
                                            detTmp.StandardDescriptionWordingChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.CurrencyCode == null && o.CurrencyCode != null) ||
                                            (n.CurrencyCode != null && !n.CurrencyCode.Equals(o.CurrencyCode)))
                                        {
                                            detTmp.CurrencyCodeChange = true;
                                            removeFromList = false;
                                        }

                                        if (!n.Amount.Equals(o.Amount))
                                        {
                                            detTmp.AmountChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.TaxCode == null && o.TaxCode != null) ||
                                            (n.TaxCode != null && !n.TaxCode.Equals(o.TaxCode)))
                                        {
                                            detTmp.TaxCodeChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.TaxNumber == null && o.TaxNumber != null) ||
                                            (n.TaxNumber != null && !n.TaxNumber.Equals(o.TaxNumber)))
                                        {
                                            detTmp.TaxNumberChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.ItemTransactionCode == null && o.ItemTransactionCode != null) ||
                                            (n.ItemTransactionCode != null && !n.ItemTransactionCode.Equals(o.ItemTransactionCode)))
                                        {
                                            detTmp.ItemTransactionCodeChange = true;
                                            detTmp.ItemTransactionNameChange = true;
                                            removeFromList = false;
                                        }

                                        //if ((n.WbsNumber == null && o.WbsNumber != null) ||
                                        //    (n.WbsNumber != null && !n.WbsNumber.Equals(o.WbsNumber)))
                                        //{
                                        //    detTmp.WbsNumberChange = true;
                                        //    removeFromList = false;
                                        //}

                                        if ((n.InvoiceNumber == null && o.InvoiceNumber != null) ||
                                            (n.InvoiceNumber != null && !n.InvoiceNumber.Equals(o.InvoiceNumber)))
                                        {
                                            detTmp.InvoiceNumberChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.InvoiceNumberUrl == null && o.InvoiceNumberUrl != null) ||
                                            (n.InvoiceNumberUrl != null && !n.InvoiceNumberUrl.Equals(o.InvoiceNumberUrl)))
                                        {
                                            detTmp.InvoiceNumberUrlChange = true;
                                            removeFromList = false;
                                        }

                                        if (!removeFromList)
                                            tmp.Add(detTmp);
                                    }
                                }
                                if (isAdded)
                                {
                                    detTmp = GetDetailHistoryObj(n);
                                    tmp.Add(GetChangedDetailValue(detTmp));
                                }
                            }
                            foreach (var o in old.Details)
                            {
                                bool isDeleted = true;
                                foreach (var n in _new.Details)
                                {
                                    if (o.SequenceNumber.Equals(n.SequenceNumber))
                                    {
                                        isDeleted = false;
                                        break;
                                    }
                                }
                                if (isDeleted)
                                {
                                    detTmp = GetDetailHistoryObj(o);
                                    detTmp.IsDeleted = true;
                                    tmp.Add(detTmp);
                                }
                            }

                            x.Details = tmp;
                        }
                    }
                    else
                    {
                        old = new PVFormHistoryData();
                    }
                    returnData.Add(x);
                    SetHistoryData(old, _new);
                }
            }
            returnData.Reverse();

            return returnData;
        }


        public List<CodeConstant> getDocumentStatus()
        {
            return base.getDocumentStatus("1");
        }


        public bool CanPrintTicketOn(DateTime Plan, int pvTypeCd)
        {
            // Start Rinda Rahayu 20160622
            bool resultTest = false;
            int intDate = ((int)DateTime.Now.Date.Subtract(Plan).TotalDays);
            if (intDate == -1 || intDate == -2 || intDate >= 0)
            {
                resultTest = true;
            }
            // End Rinda Rahayu 20160622

            return resultTest;

            /* return (
                 ((int)Plan.Subtract(DateTime.Now.Date).TotalDays) <= 0
                 );*/

        }

        public bool isCanDirect(string pvNoParam, string pvYearParam, string userName, decimal div)
        {
            int CanDirect = Qx<int>("GetCanDirectToMe", new { docno = pvNoParam, docyear = pvYearParam, username = userName }).FirstOrDefault();
            return (CanDirect != 0);
        }

        public override bool SetNextApprover(int _no, int _yy, string by)
        {
            bool r = false;
            try
            {
                Do("SetNextApprover", new { docno = _no, docyear = _yy, approver = by });
                r = true;
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return r;

        }

        #region Compare / Submit EFB
        public void UpdateStatusCompare(int PV_NO, int PV_YEAR, int PASSED, int ISSUE, int reCompare)
        {
            TB_R_PV_H _TB_R_PV_H = (from p in db.TB_R_PV_H
                                    where p.PV_NO == PV_NO && p.PV_YEAR == PV_YEAR
                                    select p).FirstOrDefault();
            if (_TB_R_PV_H != null)
            {
                string StatusCompare = "";
                if (reCompare != 0)
                {
                    StatusCompare = "NOT COMPARE";
                }
                else if (ISSUE == 0)
                {
                    StatusCompare = "MATCHED";
                }
                else if (PASSED == 0)
                {
                    StatusCompare = "UNMATCHED";
                }

                _TB_R_PV_H.STATUS_COMPARE = StatusCompare;
                db.SaveChanges();
            }
        }

        public void UpdateAfterCompare(int PV_NO)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@PV_NO", PV_NO);
            Exec("efb_sp_update_after_compare", d, null, 0);
        }
        public void RemoveAfterCompare(int PV_NO, string invoice_no)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@PV_NO", PV_NO);
            d.Add("@INVOICE_NO", invoice_no);
            Exec("efb_sp_remove_after_compare", d, null, 0);
        }
        public void UpdateEfakturAfterSubmit(string PV_NO, string template_cd)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@PV_NO", PV_NO);
            d.Add("@TEMPLATE_CD", template_cd);
            Exec("efb_sp_UpdateVatInEfaktur", d, null, 0);
        }

        public List<string> searchStatusCompare(string PV_NO)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format(" select top 1 STATUS_COMPARE from TB_R_PV_H where PV_NO={0}", PV_NO));
            List<string> l = new List<string>();
            using (ContextWrap co = new ContextWrap())
            {
                l = co.db.ExecuteStoreQuery<string>(sb.ToString(), new object[0]).ToList<string>();
            }
            return l;
        }

        public List<string> searchPdfWHT(string PV_NO)
        {
            //efb_sp_pdf_withholding
            List<string> l = new List<string>();
            l = Qx<string>("getPDFWithholding", new { PV_NO = PV_NO }).ToList();
           
            if (l == null)
                l = new List<string>();
            return l;
        }

        public string GenerateZipFile(List<string> FilePaths, string TempZipFolder, string zipFileName)
        {
            //string zipFileName = "ELVIS_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
            string zipFullPath = Path.Combine(@TempZipFolder, zipFileName);
            int i = 0;
            try
            {
                //Directory.CreateDirectory(@TempZipFolder);
                using (ZipOutputStream zip = new ZipOutputStream(File.Create(zipFullPath)))
                {
                    zip.SetLevel(3);
                    byte[] buffer = new byte[4096];

                    foreach (string filePath in FilePaths)
                    {
                        if (File.Exists(filePath)) //only when file exists
                        {
                            i++;
                            ZipEntry entry = new ZipEntry(Path.GetFileName(@filePath));

                            entry.DateTime = DateTime.Now;
                            zip.PutNextEntry(entry);

                            using (FileStream fs = File.OpenRead(@filePath))
                            {
                                int sourceBytes;
                                do
                                {
                                    sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                    zip.Write(buffer, 0, sourceBytes);

                                } while (sourceBytes > 0);
                            }
                        }
                    }
                    zip.Finish();
                    zip.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return i > 0 ? zipFullPath : string.Empty;
        }

        public IQueryable<vw_efb_compare_faktur_list> SearchListCompare(string PV_NO)
        {
            db.CommandTimeout = 3600;
            var q = (from p in db.vw_efb_compare_faktur_list.Where(x => (x.PV_NO == PV_NO))
                     select p);
            return q;
        }

        public void CompareViewGeneratorByPV(string PV_NO)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@PV_NO", PV_NO);
            //Exec("efb_sp_compare_invoice_efaktur_view_generator", d, null, 0);
            Exec("efb_sp_compare_invoice_efaktur", d, null, 0);
        }

        public List<string> valDuplicateTaxNo(string PV_NO, string TaxNo)
        {
            // StringBuilder sb = new StringBuilder();
            // sb.AppendLine(string.Format(" exec efb_sp_val_tax_no '{0}','{1}'", PV_NO, TaxNo));
            List<string> l = new List<string>();
            l = Qx<string>("getDuplicateTaxNo", new { PV_NO = PV_NO, TAX_NO = TaxNo }).ToList();
            /*
            using (ContextWrap co = new ContextWrap())
            {
                l = co.db.ExecuteStoreQuery<string>(sb.ToString(), new object[0]).ToList<string>();
            }
            */
            if (l == null)
                l = new List<string>();
            return l;
        }

        public void RollbackDataPVEFB(string PV_NO, string Flag)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@PV_NO", PV_NO);
            d.Add("@Flag", Flag);
            Exec("efb_sp_rollback_data_pv", d, null, 0);
        }
        #endregion
    }
}