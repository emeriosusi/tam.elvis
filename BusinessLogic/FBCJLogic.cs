﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.EntityClient;
using Common.Data;
using Common.Data._20MasterData;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using DataLayer;
using DataLayer.Model;
using Common.Function;
using Common;

namespace BusinessLogic
{
    public class FBCJLogic
    {
        LogicFactory logic = LogicFactory.Get();

        private List<CashJournalMessage> msg = null;

        public List<CashJournalMessage> LastMesssages
        {
            get
            {
                return msg;
            }
        }

        public CashJournalData Retrieve(string cashJournal, DateTime periodFrom, DateTime periodTo, string uid)
        {
            CashJournalData data = logic.SAP.CajoRetrieve(new CashJournalImportData() {
                CashJournal = cashJournal,
                PeriodFrom = periodFrom,
                PeriodTo = periodTo
                }, uid);
            foreach (var p in data.CashPayments)
            {
                p.Vendor = SapCodex(p.Vendor);
                p.Customer = SapCodex(p.Customer);
            }
            foreach (var r in data.CashReceipts)
            {
                r.Vendor = SapCodex(r.Vendor);
                r.Customer = SapCodex(r.Customer);
            }
            return data;
        }

        public List<CashJournalMessage> Maintain(CashJournalImportData im, List<CashJournalTable> t, string uid)
        {
            List<CashJournalMessage> m = new List<CashJournalMessage>();

            foreach (CashJournalTable c in t)
            {
                c.Vendor = UnCodex(c.Vendor);
                c.Customer = UnCodex(c.Customer);
            }
            m = logic.SAP.CajoMaintain(im, t, uid);
            msg = m;
            return m;
        }

        public List<string> GetBusinessTransactionPay()
        {
            return logic.Sys.GetArray("SAP", "ZBAPI_FBCJ.BUSINESS_TRANSACTION.PAYMENT", ",").ToList();
        }

        public List<string> GetBusinessTransactionReceipt()
        {
            return logic.Sys.GetArray("SAP", "ZBAPI_FBCJ.BUSINESS_TRANSACTION.RECEIPT", ",").ToList();
        }

        public List<CodeConstant> GetReason()
        {
            List<SystemMaster> x = logic.Sys.GetSystemMaster("FBCJ_REVERSE_REASON");
            if (x == null) return null;
            return x.Select(a => new CodeConstant()
            {
                Code = a.Code
                , Description = a.Value.str()
            }).ToList();
        }

        public List<CodeConstant> GetVendor()
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = from v in db.vw_Vendor
                        select new CodeConstant()
                        {
                            Code = v.VENDOR_CD,
                            Description = v.VENDOR_NAME
                        };

                return q.ToList();
            }
        }

        public List<VendorData> getVendorCodes()
        {
            List<VendorData> v = new List<VendorData>();
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from t in db.vw_Vendor
                         where !Common.AppSetting.EXCLUDE_EXTERNAL_VENDOR || (t.VENDOR_GROUP_CD != 3)
                         select new VendorData()
                         {
                             VENDOR_CD = t.VENDOR_CD,
                             VENDOR_NAME = t.VENDOR_NAME,
                             VENDOR_GROUP_CD = t.VENDOR_GROUP_CD,
                             SEARCH_TERMS = t.SEARCH_TERMS,
                             DIVISION_ID = t.DIVISION_ID
                         });
                
                if (q.Any())
                {
                    v = q.ToList();
                    foreach (VendorData x in v)
                    {
                        x.VENDOR_CD = SapCodex(x.VENDOR_CD);
                    }
                }
            }
            return v;
        }

        public static string UnCodex(string s)
        {
            long l = 0;
            if (long.TryParse(s, out l)) 
            {
                s = s.PadLeft(10, '0');
            }
            return s;
        }

        public static string SapCodex(string s)
        {
            long a = 0;
            
            if (long.TryParse(s, out a))
            {
                s = a.ToString();
            }
            return s;
        }

        public List<CustomerData> getCustomerCodes()
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from a in db.vw_Customer
                         select new CustomerData()
                         {
                             CUSTOMER_CD = a.CUSTOMER_CD,
                             CUSTOMER_NAME = a.CUSTOMER_NAME,
                             CUSTOMER_GROUP = a.CUSTOMER_GROUP,
                             SEARCH_TERMS = a.SEARCH_TERMS,
                             BUSINESS_TYPE = a.BUSINESS_TYPE,
                             SUJK = a.SUJK,
                             CONTACT_PERSON = a.CONTACT_PERSON,
                             PHONE = a.PHONE,
                             SERVICE_YEAR = a.SERVICE_YEAR,
                             CONTRACT_STATUS = a.CONTRACT_STATUS,
                             DESCRIPTION = a.DESCRIPTION,
                             CREATED_BY = a.CREATED_BY,
                             CREATED_DT = a.CREATED_DT
                         });
                List<CustomerData> r = new List<CustomerData>();

                if (q.Any())
                {
                    r = q.ToList();

                    foreach (CustomerData x in r)
                    {
                        x.CUSTOMER_CD = SapCodex(x.CUSTOMER_CD);
                    }
                }

                return r;
            }
        }

        

    }
}
