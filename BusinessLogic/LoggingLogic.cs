﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DataLayer;
using Common;
using Common.Data;
using Common.Enum;
using System.Data.Common;
using System.Data.Objects;
using System.Data.EntityClient;
using System.Data;
using DataLayer.Model;
using Dapper;

namespace BusinessLogic
{
    public class LoggingLogic: LogicBase
    {
        private string userId = "";
        private int processId = 0;
        private string _path = GetPath();
        private string _logfile = "";

        #region insert log header
        /// <summary>
        /// 
        /// </summary>
        /// <param name="function_id"></param>
        /// <param name="process_sts">0=success, 1=failed,2=failed by system(Error)</param>
        /// <param name="user_id"></param>
        /// <param name="remarks"></param>
        /// <returns></returns>
        //public bool InsertLogHeader(string function_id, string process_sts, string user_id, string remarks, ref int ProcessID)
        //{
        //    string typId = "INF";
        //    if (process_sts == "1")
        //    {
        //        typId = "WRN";
        //    }
        //    else if (process_sts == "2")
        //    {
        //        typId = "SYS";
        //    }
        //    ProcessID = Log(null, null, null, user_id, function_id, ProcessID, remarks, typId, process_sts);
        //    return ProcessID > 0;

        //}
        #endregion

        #region update log header
        /// <summary>
        /// 
        /// </summary>
        /// <param name="process_id"></param>
        /// <param name="process_sts"></param>0=success, 1=failed, 2=failed by system...
        /// <returns></returns>
        //public bool UpdateLogHeader(int process_id, string process_sts)
        //{

        //    return UpdateLogHeader(process_id, process_sts, "");
        //}

        //public bool UpdateLogHeader(int process_id, string process_sts, string remarks)
        //{
        //    return Log(null, null, null, null, null, process_id, remarks, null, process_sts) > 0;


        //}

        #endregion

        #region insert log detail
        /// <summary>
        /// 
        /// </summary>
        /// <param name="process_id"></param>you can find this value from method : get last process id
        /// <param name="seq_no"></param>you can find this value from method : get last sequence number
        /// <param name="msg_id"></param>
        /// <param name="msg_type"></param>
        /// <param name="err_location"></param>
        /// <param name="err_message"></param>
        /// <returns></returns>
        //public bool InsertLogDetail(int process_id, int seq_no, string msg_id, string msg_type, string err_location, string err_message)
        //{
            

        //    return Log(msg_id, err_message, err_location, null, "", process_id) > 0;
        //}

        #endregion

        //public bool InsertLogDetail(int process_id, string msg_id, string msg_type, string err_location, string err_message)
        //{
           
        //    return Log(msg_id, err_message, err_location, null, "", process_id) > 0;
        //}

     

        #region get last sequence number
        //public int GetLastSeqNo(int process_id)
        //{
        //    int? seq_no = db.TB_R_LOG_D
        //        .OrderByDescending(I => I.seq_no)
        //        .Where(i => i.process_id == process_id)
        //        .Select(x => x.seq_no)
        //        .FirstOrDefault();
        //    return (seq_no == null) ? (seq_no ?? 0 + 1) : 1;
        //}

        public int PID
        {
            get
            {
                return processId;
            }
            set
            {
                processId = value;
            }
        }

 
        #endregion

        //#region get logging detail
        //public object GetLogDetailData(int _processId, ref ErrorData Err)
        //{
        //    object _retVal = new object();
        //    using (ContextWrap co = new ContextWrap())
        //        try
        //        {
        //            var db = co.db;
        //            var q = (from i in db.vw_LogDetailData where i.process_id == _processId orderby i.seq_no ascending select i);
        //            if (q != null)
        //            {
        //                _retVal = q;
        //            }
        //        }
        //        catch (Exception Ex)
        //        {
        //            Err = new ErrorData();
        //            Err.ErrMsg = Ex.Message.ToString();
        //        }
        //    return _retVal;
        //}
        //#endregion

        //#region get log header data
        //public LogHeaderData GetLogHeaderData(int _processId)
        //{
        //    LogHeaderData _data = null;
        //    using (ContextWrap co = new ContextWrap())
        //        try
        //        {
        //            var db = co.db;
        //            var q = (from i in db.TB_R_LOG_H where i.process_id == _processId select i).FirstOrDefault();
        //            if (q != null)
        //            {
        //                _data = new LogHeaderData();
        //                _data.process_id = q.process_id;
        //                _data.process_dt = q.process_dt;
        //                _data.function_id = q.function_id;
        //                _data.process_sts = q.process_sts;
        //                _data.user_id = q.user_id;
        //                _data.end_dt = q.end_dt;
        //            }
        //        }
        //        catch (Exception Ex)
        //        {
        //            Err(Ex);
        //        }
        //    return _data;
        //}
        //#endregion

        public static string MaxLenDef(string f, int len, string def = "?")
        {
            return (String.IsNullOrEmpty(f))
                    ? def
                    : ((f.Length > len)
                        ? f.Substring(0, len)
                        : f);
        }

        public int Log(
            string _msgId,
            string _errMessage,
            string _errLocation = "",
            string _userID = "",
            string _functionID = "",
            int ID = 0,
            string _remarks = null,
            string _type = null,
            string _sts = null)
        {
            if (ID < 1 && processId > 0)
                ID = processId;
            else if (ID > 0)
                processId = ID;

            if (string.IsNullOrEmpty(_userID) && !string.IsNullOrEmpty(userId))
            {
                _userID = userId;
            }
            if ((!string.IsNullOrEmpty(_userID)) && String.Compare(userId, _userID, true) != 0)
            {
                userId = _userID;
            }

            using (ContextWrap co = new ContextWrap())
            {
                //ObjectParameter pid = new ObjectParameter("pid", ID);
                //co.db.PutLog(_errMessage, _userID, _errLocation, pid, _msgId, _type, _functionID, _sts, _remarks);

                DynamicParameters dyn = new DynamicParameters();
                dyn.Add("@what", _errMessage);
                dyn.Add("@user", _userID);
                dyn.Add("@where", _errLocation);
                dyn.Add("@pid", ID, DbType.Int32, ParameterDirection.InputOutput);
                dyn.Add("@id", _msgId);
                dyn.Add("@type", _type);
                dyn.Add("@func", _functionID);
                dyn.Add("@sts", _sts);
                dyn.Add("@rem", _remarks);

                Db.Execute("sp_PutLog", dyn, null, null, CommandType.StoredProcedure);
                ID = dyn.Get<int>("@pid");

                return ID;
                //if (pid.Value != null)
                //    return Convert.ToInt32(pid.Value);
                //else
                //    return 0;

            }
        }


        public static bool ForceDirectories(string d)
        {
            bool r = false;
            try
            {
                if (!Directory.Exists(d))
                {
                    Directory.CreateDirectory(d);
                }
                r = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return r;
        }

        private int _seq = 0;
        public void Say(string uid, string word, params object[] x)
        {
            if (_seq < 1)
            {
                if (!ForceDirectories(_path)) return;
            }
            _logfile = Path.Combine(_path, ((string.IsNullOrEmpty(uid)) ? "0.log" : uid + ".log"));
            _seq++;
            try
            {
                File.AppendAllText(_logfile, "\r\n"
                    + tick((x != null) ? String.Format(word, x) : word));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(word);
            }
        }

        public static void err(Exception ex)
        {
            say("except", ExceptionMessageTrace(ex));
        }

        public void Err(Exception ex)
        {
            say("except", ExceptionMessageTrace(ex));
        }

        public static string tick(string s)
        {
            return DateTime.Now.ToString("HH:mm:ss") + "\t" + s;
        }

        public static string GetPath()
        {
            DateTime n = DateTime.Now;
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)
                , AppSetting.LdapDomain, AppSetting.ApplicationID, "log"
                , n.Year.ToString()
                , n.Month.ToString()
                , n.Day.ToString());
        }


        public static string GetTempPath(string username="", int pid = 0)
        {
            DateTime n = DateTime.Now;
            string p= Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            if (!string.IsNullOrEmpty(username))
            {
                string vt = pid.ToString();
                if (pid <= 0)
                {
                    vt = string.Format("{0:yyyyMMdd}", n);
                }
                return Path.Combine(p
                    , AppSetting.LdapDomain, AppSetting.ApplicationID, "tmp"
                    , username
                    , vt
                    );
            }
            else
            {
                return Path.Combine(p
                    , AppSetting.LdapDomain, AppSetting.ApplicationID, "tmp"
                    , n.Year.ToString()
                    , n.Month.ToString()
                    , n.Day.ToString());
            }
        }

        public static void say(string x)
        {
            string path = GetPath();

            string logfile = Path.Combine(path, "0.log");
            try
            {
                if (!ForceDirectories(path)) return;
                File.AppendAllText(logfile, "\r\n"
                    + tick(x));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public static string SanitizeFilename(string insane)
        {
            string pattern = "[\\+/\\\\\\~#%&*{}/:<>?|\"-]";
            string replacement = "_";

            Regex regEx = new Regex(pattern);
            return Regex.Replace(regEx.Replace(insane, replacement), @"\s+", " ");
        }

        public static void say(string uid, string word, params object[] x)
        {
            string path = GetPath();

            string logfile = Path.Combine(path, ((string.IsNullOrEmpty(uid)) ? "0.log" : SanitizeFilename(uid) + ".log"));

            try
            {
                if (!ForceDirectories(path)) throw new Exception("Cannot create said dir for " + uid);
                File.AppendAllText(logfile, "\r\n"
                    + tick((x != null) ? String.Format(word.Replace("|", "\r\n\t"), x) : word));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public static string msg(Exception x)
        {
            string m = x.Message;
            if (x.InnerException != null)
                m += "\r\n" + x.InnerException.Message;
            return m;
        }

        public static string ExceptionMessageTrace(Exception x)
        {
            if (x == null) return null;
            Type xt = x.GetType();
            string xName = "";
            if (xt != null)
                xName = "[" + xt.FullName + "]";
            return xName
                + x.Message
                + ((x.InnerException != null) ? "\r\n\t" + x.InnerException.Message : "")
                + "\r\n\t"
                + x.StackTrace;
        }

        static string Tabs(int n)
        {
            return new String('\t', n);
        }

    }
}
