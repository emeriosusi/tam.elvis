﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Dynamic;
using Common;
using Dapper;
using DataLayer.Model;

namespace BusinessLogic
{
    public class LogicBase : IDisposable
    {
        protected ELVIS_DBEntities _db = null;

        protected ELVIS_DBEntities db
        {
            get
            {
                if (_db == null)
                {
                    LogicFactory fak = LogicFactory.Get();
                    ContextWrap cx = (fak != null) ? fak.Cx : null;
                    if (cx != null)
                        _db = cx.db;
                    else
                        _db = new ELVIS_DBEntities();
                }
                return _db;
            }   

            set
            {
                _db = value;
            }
        }

        public LogicFactory Logic
        {
            get
            {
                return LogicFactory.Get();
            }
        }
        
        protected IDbConnection _Db = null;
        protected IDbConnection Db
        {
            get
            {
                if (_Db == null)
                {
                    LogicFactory f = LogicFactory.Get();
                    ContextWrap cw = f != null ? f.Cx : null;
                    _Db = (cw != null)
                        ? cw.DB
                        : null;
                }
                return _Db;
            }
        }

        public void Dispose()
        {
            if (_db != null)
            {
                if (_db.Connection != null
                    && _db.Connection.State == ConnectionState.Open)
                {
                    _db.Connection.Close();
                }
                _db.Dispose();
                _db = null;
            }

            if (_Db != null)
            {
                if ((_Db as SqlConnection).State == ConnectionState.Open)
                {
                    _Db.Close();
                }
                _Db.Dispose();
                _Db = null;
            }
        }

        private StringBuilder errorLines = new StringBuilder("");
        protected void Handle(Exception ex)
        {
            LoggingLogic.err(ex);
        }

        public string LastErr
        {
            get
            {
                string lastErr = "";
                lastErr = errorLines.ToString();
                errorLines.Clear();
                return lastErr;
            }
        }

        protected IDbTransaction DbTx = null;
        protected bool DbBufferd = true;
        protected int? DbTimeOut = null;

        public void QuSet(IDbTransaction t, bool buf, int? timeout = null)
        {
            DbTx = t;
            DbBufferd = buf;
            DbTimeOut = timeout; 
        }

        public string GetSQL(string sqlID)
        {
            string sql = Logic.SQL[sqlID];
            if (string.IsNullOrEmpty(sql))
            {
                if (!string.IsNullOrEmpty(sqlID))
                    sql = sqlID;
                else
                    throw new Exception("no SQL");
            }
            return sql;
        }

        public DynamicParameters Dyn(object[] o)
        {
            DynamicParameters dyn = new DynamicParameters();
            int di = 0;
            if (o != null && o.Length > 0)
                foreach (object ox in o)
                {
                    ++di;
                    dyn.Add("@p" + di.ToString(), ox);
                }
            return dyn;
        }

        public IEnumerable<T> Qu<T>(string sqlID, params object[] o)
        {
            return Db.Query<T>(GetSQL(sqlID), Dyn(o)
                    , DbTx, DbBufferd, DbTimeOut);
        }

        public IEnumerable<T> Qx<T>(
                  string sqlID
                , dynamic param = null
                , IDbTransaction transaction = null
                , bool buffered = true
                , int? commandTimeout = null
                , CommandType? commandType = null)
        {
            return Db.Query<T>(GetSQL(sqlID), param as object
                    , transaction ?? DbTx, buffered, commandTimeout ?? DbTimeOut, commandType);
        }

        public IEnumerable<T> UQx<T>(
                  string sqlID
                , dynamic param = null)
        {
            IEnumerable<T> r = default(IEnumerable<T>);
            try
            {
                using (IDbTransaction tx = Db.BeginTransaction())
                {
                    r = Qx<T>(GetSQL(sqlID), param as object, tx, true, DbTimeOut, CommandType.Text);
                    tx.Commit();
                }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return r;
        }

        public string asString(object[] o) {
            StringBuilder x = new StringBuilder(""); 
            int i = 0;
            foreach (var oi in o)
            {
                if (i>0) x.Append(", ");
                if (oi != null)
                    x.Append(oi.ToString());
                else
                    x.Append("NULL");
                ++i;
            }
            return x.ToString(); 
        }

        public string asString(object o)
        {
            StringBuilder x = new StringBuilder("");
            int i = 0;
            foreach (var p in o.GetType().GetProperties())
            {
                if (i>0) x.Append(", ");
                x.Append(p.Name);
                x.Append("=");
                if (p.GetValue(o, null) != null)
                    x.Append(p.GetValue(o, null).ToString());
                else
                    x.Append("NULL");
                i++;
            }
            return x.ToString();            
        }

        public IEnumerable<T> hQu<T>(string sqlID, params object[] o)
        {
            string oKey = sqlID;
            if (o != null && o.Length > 0)
                oKey = oKey + "(" + asString(o) + ")";
            IEnumerable<T> v = Heap.Get<IEnumerable<T>>(oKey);
            if (v == null)
            {
                v = Db.Query<T>(GetSQL(sqlID), Dyn(o)
                        , DbTx, DbBufferd, DbTimeOut);
                if (v!=null) 
                    Heap.Set(oKey, v);
            }            
            return v;
        }

        public IEnumerable<T> hQx<T>(
                  string sqlID
                , dynamic param = null
                , IDbTransaction transaction = null
                , bool buffered = true
                , int? commandTimeout = null
                , CommandType? commandType = null)
        {
            string oKey = sqlID;
            if ((param as object)!= null)
                oKey = oKey + "(" + asString(param as object) + ")";
            IEnumerable<T> v = Heap.Get<IEnumerable<T>>(oKey);
            if (v == null)
            {
                v = Db.Query<T>(GetSQL(sqlID), param as object
                    , transaction ?? DbTx, buffered, commandTimeout ?? DbTimeOut, commandType);
                if (v != null)
                    Heap.Set(oKey, v);
            }
            return v;
        }

        public int Exec(
                  string spName
                , dynamic param = null
                , IDbTransaction transaction = null
                , int? commandTimeout = null
                , CommandType? commandType = CommandType.StoredProcedure)
        {
            return Db.Execute(spName, param as object
                , transaction ?? DbTx, commandTimeout ?? DbTimeOut, commandType);
        }

        public int Do(
                string SqlID
                , dynamic param = null
                , IDbTransaction transaction = null
                , int? commandTimeout = null
                , CommandType? commandType = CommandType.Text)
        {
            return Db.Execute(GetSQL(SqlID), param as object
                , transaction ?? DbTx, commandTimeout ?? DbTimeOut, commandType);
        }

        protected readonly Common.Function.GlobalResourceData resx = new Common.Function.GlobalResourceData();
    }
}
