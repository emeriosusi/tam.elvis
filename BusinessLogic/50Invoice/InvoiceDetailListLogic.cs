﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using Common.Data._50Invoice;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data;
using DataLayer.Model;
using Common.Data;
using System.IO;
using System.Diagnostics;
using BusinessLogic.CommonLogic;
using Common.Function;

namespace BusinessLogic._50Invoice
{
    public class InvoiceDetailListLogic
    {
        GlobalResourceData _globalResource = new GlobalResourceData();

        #region Search Inquiry
        public List<InvoiceDetailData> SearchInqury(string _invoiceNo, string _invoiceDate, string _vendorCode, string _vendorName,
            string _division, string _status, string _pvNo, string _pvYear, string _transaction_type)
        {
            List<InvoiceDetailData> result = new List<InvoiceDetailData>();
            int idx = 0;

            bool ignorePvNo = true;
            bool ignoreInvoiceDate = true;

            #region build searching criteria
            DateTime invoiceDate = DateTime.Now;
            if (!String.IsNullOrWhiteSpace(_invoiceDate))
            {
                ignoreInvoiceDate = false;
                invoiceDate = DateTime.ParseExact(_invoiceDate, "dd MMM yyyy", null);
            }
            int pvNo = 0, pvYear = 0;
            if (!String.IsNullOrWhiteSpace(_pvNo) && !String.IsNullOrWhiteSpace(_pvYear))
            {
                ignorePvNo = false;
                pvNo = Convert.ToInt32(_pvNo.Trim());
                pvYear = Convert.ToInt16(_pvYear.Trim());
            }
            #endregion
            using (ContextWrap co = new ContextWrap())
                try
                {
                    //default ignore all searching criteria
                    var _db = co.db;

                    #region Query
                    int status = Convert.ToInt32(_status.Trim());
                    var q = (from p in _db.vw_Invoice_D
                             where p.INVOICE_NO == _invoiceNo
                             where p.VENDOR_CD == _vendorCode
                             where (ignoreInvoiceDate || p.INVOICE_DATE == invoiceDate)
                             where (ignorePvNo || (p.PV_NO == pvNo && p.PV_YEAR == pvYear))
                             orderby p.SEQ_NO ascending
                             select new
                             {
                                 p.SEQ_NO,
                                 ITEM_TRANSACTION =
                                 (from k in _db.TB_M_ITEM_TRANSACTION
                                  where k.ITEM_TRANSACTION_CD == p.ITEM_TRANSACTION_CD
                                  select k.ITEM_TRANSACTION_DESC).FirstOrDefault(),
                                 p.ITEM_DESCRIPTION,
                                 p.AMOUNT,
                                 p.CURRENCY_CD,
                                 p.COST_CENTER_CD,
                                 p.WBS_NO,
                                 p.TAX_CD,
                                 STATUS_NAME =
                                 (from pp in _db.TB_M_STATUS
                                  where pp.STATUS_CD == status
                                  select pp.STATUS_NAME).FirstOrDefault()
                             }).ToList();

                    if (q.Any())
                    {
                        foreach (var data in q)
                        {
                            InvoiceDetailData _data = new InvoiceDetailData();
                            _data.GRID_NO = idx + 1;
                            _data.SEQ_NO = data.SEQ_NO;
                            _data.ITEM_TRANSACTION = data.ITEM_TRANSACTION;
                            _data.ITEM_DESCRIPTION = data.ITEM_DESCRIPTION;
                            _data.CURRENCY_CD = data.CURRENCY_CD;
                            _data.AMOUNT = String.Format("{0:N0}", data.AMOUNT);
                            _data.COST_CENTER_CD = data.COST_CENTER_CD;
                            _data.WBS_NO = data.WBS_NO;
                            _data.TAX_CD = data.TAX_CD;

                            _data.STATUS_NAME = data.STATUS_NAME;

                            result.Add(_data);
                            idx++;
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    throw;
                }

            return result;
        }

        public List<InvoiceDetailData> headerInqury(string _invoiceNo, string _divisionID, string _invoiceStatus, string _transaction_type)
        {
            List<InvoiceDetailData> result = new List<InvoiceDetailData>();
            using (ContextWrap co = new ContextWrap())
            try
            {
                #region Query
                var _db = co.db;
                int status = Convert.ToInt32(_invoiceStatus.Trim());
                int transaction_type = Convert.ToInt32(_transaction_type.Trim());
                var q = (from p in _db.TB_R_INVOICE_H
                         where p.INVOICE_NO == _invoiceNo
                         select new
                         {
                             DIVISION_ID =
                             ((from t5 in _db.vw_Division
                               where
                                 p.DIVISION_ID == _divisionID
                               select new
                               {
                                   t5.DIVISION_NAME
                               }).FirstOrDefault().DIVISION_NAME),
                             STATUS_NAME =
                                ((from t1 in _db.TB_M_STATUS
                                  where
                                    p.STATUS_CD == status
                                  select new
                                  {
                                      t1.STATUS_NAME
                                  }).FirstOrDefault().STATUS_NAME),
                             TRANSACTION_TYPE =
                                 ((from t6 in _db.TB_M_TRANSACTION_TYPE
                                   where
                                       p.TRANSACTION_CD == transaction_type
                                   select new
                                   {
                                       t6.TRANSACTION_NAME
                                   }).FirstOrDefault().TRANSACTION_NAME)
                         }).ToList();

                if (q.Any())
                {
                    foreach (var data in q)
                    {
                        InvoiceDetailData _data = new InvoiceDetailData();
                        _data.DIVISION_ID = data.DIVISION_ID;
                        _data.STATUS_NAME = data.STATUS_NAME;
                        _data.TRANSACTION_TYPE_CD = data.TRANSACTION_TYPE;

                        result.Add(_data);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }

            return result;
        }
        #endregion


        public string getTransactionName(int Code)
        {
            using (ContextWrap co = new ContextWrap())
            {
                return (from tt in co.db.TB_M_TRANSACTION_TYPE
                        where tt.TRANSACTION_CD == Code
                        select tt.TRANSACTION_NAME).FirstOrDefault();
            }
        }

        public InvoiceData getInvoiceInfo(string InvoiceNo)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = (from ii in _db.TB_R_INVOICE_H
                         where ii.INVOICE_NO == InvoiceNo
                         select new InvoiceData()
                         {
                             INVOICE_NO = ii.INVOICE_NO,
                             INVOICE_DATE = ii.INVOICE_DATE,
                             TRANSACTION_CD = ii.TRANSACTION_CD,
                         });
                return q.FirstOrDefault();
            }
        }
    }
}