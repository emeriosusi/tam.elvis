﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Data.SAPData;
using Common.Data._80Accrued;

namespace BusinessLogic
{
    public class NoOpSAP : ISAPLogic
    {

        public NoOpSAP()
        {
        }

        public BudgetCheckResult _BudgetCheck_(BudgetCheckInput _i)
        {
            return new BudgetCheckResult()
            {
                DOC_NO = _i.DOC_NO,
                DOC_YEAR = _i.DOC_YEAR,
                STATUS = "S",
                MESSAGE = "",
                AVAILABLE = "",
                ORIGINAL = ""
            };
        }

        public BudgetCheckResult BudgetCheck(BudgetCheckInput _i, string uid = "")
        {
            return new BudgetCheckResult()
            {
                DOC_NO = _i.DOC_NO,
                DOC_YEAR = _i.DOC_YEAR,
                STATUS = "S",
                MESSAGE = "",
                AVAILABLE = "",
                ORIGINAL = ""
            };
        }

        public List<CashJournalMessage> CajoMaintain(CashJournalImportData import, List<CashJournalTable> tables, string uid)
        {
            return new List<CashJournalMessage>();
        }

        public CashJournalData CajoRetrieve(CashJournalImportData import, string uid)
        {
            return new CashJournalData();
        }

        public List<RVCheckHeader> CheckRV(List<RVCheckHeader> rvHeader, List<RVCheckDetail> rvDetail, ref List<RVCheckResult> results, int pid, int method)
        {
            return new List<RVCheckHeader>();
        }

        public event SAP.Middleware.Connector.RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;

        public void del()
        {
        }

        public string GetRemainingBudget(int docno, int docyear, string wbs)
        {
            return getRemainingBudget("", "");
            //return null;
        }

        public string getRemainingBudget(string sWBSNo, string sFiscalYear)
        {
            // fid.Hadid. for dummy data
            decimal dAvailableAmount = 100000000;
            decimal dCurrentAmount = 150000000;
            decimal dPercentage = dAvailableAmount / dCurrentAmount * 100;

            return string.Format("{0:#,#.##}", dAvailableAmount)
                                + " (" + string.Format("{0:#,#.##}", dPercentage) + "%)";
            //return null;
        }

        public List<PVPostSAPResult> GetResultCheckedByAcc(List<PVPostInputHeader> _PVCheck, int _ProcessId, int method)
        {
            List<PVPostSAPResult> lsap = new List<PVPostSAPResult>();
            var h = _PVCheck.Select(
                    a => new PVPostSAPResult()
                    {
                        PV_NO = a.PV_NO,
                        PV_YEAR = a.PV_YEAR,
                        ITEM_NO = a.ITEM_NO.ToString(),
                        CURRENCY_CD = a.CURRENCY_CD,
                        SAP_DOC_NO = a.PV_NO,
                        SAP_DOC_YEAR = a.PV_YEAR,
                        INVOICE_NO = a.INVOICE_NO,
                        SAP_CLEARING_DOC_NO = "",
                        SAP_CLEARING_DOC_YEAR = "",
                        MESSAGE = "S",
                        MESSAGE_TEXT = "OK",
                        STATUS_MESSAGE = "CHECKED NOOP"
                    });
            lsap.AddRange(h.ToList());
            return new List<PVPostSAPResult>();
        }

        public List<PVPostSAPResult> GetResultPostPVToSAP(List<PVPostInputHeader> _PVPostInputHeader, List<PVPostInputDetail> _PVPostInputDetail, List<OneTimeVendorData> _vendorData, int _ProcessId, string uid, ref string errorMessage)
        {
            List<PVPostSAPResult> lsap = new List<PVPostSAPResult>();
            var h = _PVPostInputHeader.Select(
                    a => new PVPostSAPResult() {
                        PV_NO = a.PV_NO,
                        PV_YEAR = a.PV_YEAR,
                        ITEM_NO = a.ITEM_NO.ToString(),
                        CURRENCY_CD = a.CURRENCY_CD,
                        SAP_DOC_NO = a.PV_NO,
                        SAP_DOC_YEAR = a.PV_YEAR,
                        INVOICE_NO = a.INVOICE_NO,
                        SAP_CLEARING_DOC_NO = "",
                        SAP_CLEARING_DOC_YEAR = "",
                        MESSAGE = "S",
                        MESSAGE_TEXT = "OK",
                        STATUS_MESSAGE = "POSTED BY NOOP" });
            lsap.AddRange(h.ToList()); 
            return lsap;
        }

        public void init()
        {
        }

        public string LastMessageText
        {
            get { return ""; }
        }

        public double num(string s)
        {
            return 0;
        }

        public VendorPostResult PostVendor(VendorInput v, int _ProcessId, string User)
        {
            return new VendorPostResult();
        }

        public void removeDestination()
        {
            
        }

        public List<PVPostSAPResult> ReversePV(ref List<PVReverseInput> _i, string uid = "")
        {
            return new List<PVPostSAPResult>();
        }

        //**************************//
        //  ELVIS 2018 ENHANCEMENT  //
        //           START          //
        //**************************//
        LogicFactory logic = LogicFactory.Get();
        public List<BudgetRemaining> BudgetRemaining(List<string> budgetNo, string uid = "")
        {

            List<BudgetRemaining> ret = new List<BudgetRemaining>();

            foreach (string b in budgetNo)
            {
                decimal? remSap = logic.Look.getWBSDummy(b);
                decimal ost = logic.Look.GetOutstandingAmountBudget(b);

                ret.Add(new BudgetRemaining() { 
                    BUDGET_NO = b,
                    REMAINING_AMT = (remSap ?? 0) - ost
                });
            }
            return ret;
        }
        public List<SyncSAPData> SynchronizeBudget(List<SyncSAPData> data, string uid = "")
        {
            return null;
        }
        public List<ShiftingBudget> ShiftingBudget(List<ShiftingBudget> data, string uid = "")
        {
            data.ForEach(x =>
                {
                    string docNo = logic.Look.getSapDocNoDummy();

                    if (!string.IsNullOrEmpty(docNo))
                        x.DOC_CHANGE_NO = docNo;
                    else
                        x.DOC_CHANGE_NO = "NOTFOUND";

                });

            return data;
        }
        public List<AccrFormSimulationData> SuspenseCheckPaid(List<AccrFormSimulationData> data, string uid = "")
        {
            data.ForEach(x =>
                {
                    x.PAID_STS = AccrFormSimulationData.SUSPAID;
                });

            return data;
        }
        public object AccruedPosting(AccrPostData data, string uid, string fid, string _loc)
        {
            string docNo = logic.Look.getSapDocNoDummy();

            if (!string.IsNullOrEmpty(docNo))
            {
                var ret = new AccrPostResult();

                ret.COMP_CODE = data.COMP_CODE;
                ret.FISC_YEAR = DateTime.Now.Year.ToString();
                ret.DOC_NUMSAP = docNo;
                ret.DOC_NUMCORE = "DUMMY DATA";
                ret.MESSAGE = "DUMMY DATA";

                return ret;
            }
            else
            {
                var err = new AccrSAPError();

                err = new AccrSAPError();
                err.TYPE = "E";
                err.ID = "";
                err.NUMBER = "999";
                err.MESSAGE = "Failed to create dummy SAP Doc No";
                err.LOG_NO = "";
                err.LOG_MSG_NO = "";
                err.MESSAGE_V1 = "";
                err.MESSAGE_V2 = "";
                err.MESSAGE_V3 = "";
                err.MESSAGE_V4 = "";
                err.PARAMETER = "";
                err.ROW = "";
                err.FIELD = "";
                err.SYSTEM = "";

                return err;
            }
        }
        public object AccruedClearing(AccrClearingData data, string uid, string fid, string _loc)
        {
            string docNo = logic.Look.getSapDocNoDummy();

            if (!string.IsNullOrEmpty(docNo))
            {
                var ret = new AccrClearingResult();
                ret.BELNR = "DUMMY DATA";
                ret.NOCLR = docNo;

                return ret;
            }
            else
            {
                var err = new AccrSAPError();

                err = new AccrSAPError();
                err.TYPE = "E";
                err.ID = "";
                err.NUMBER = "999";
                err.MESSAGE = "Failed to create dummy SAP Doc No";
                err.LOG_NO = "";
                err.LOG_MSG_NO = "";
                err.MESSAGE_V1 = "";
                err.MESSAGE_V2 = "";
                err.MESSAGE_V3 = "";
                err.MESSAGE_V4 = "";
                err.PARAMETER = "";
                err.ROW = "";
                err.FIELD = "";
                err.SYSTEM = "";

                return err;
            }
        }

        //**************************//
        //            END           //
        //**************************//
    }
}
