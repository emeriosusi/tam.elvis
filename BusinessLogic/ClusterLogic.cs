﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data; 

namespace BusinessLogic
{
    public class ClusterLogic: LogicBase
    {
        public List<ClusterData> GetClusterData(string ClusterId = null, string ClusterName = null)
        {
            return Qx<ClusterData>("GetClusterData", new { id = ClusterId, name = ClusterName }).ToList();
        }

        public List<ClusterTransaction> GetClusterTransaction(string ClusterId = null)
        {
            return Qx<ClusterTransaction>("GetClusterTransaction", new { id = ClusterId }).ToList();
        }

        public List<TransactionCluster> GetTransactionCluster(string ClusterId = null)
        {
            return Qx<TransactionCluster>("GetTransactionCluster", new { id = ClusterId }).ToList();
        }

        public List<ClusterStatus> GetClusterStatus(String ClusterId = null)
        {
            return Qx<ClusterStatus>("GetClusterStatus", new { id = ClusterId }).ToList();
        }

        public int SetClusterData(string ClusterID=null, string ClusterName = "", string StatusCodes = "", int act=0)
        {
            return Do("SetCluster", new { stype = "CLUSTER", id = ClusterID, name = ClusterName, text = StatusCodes, user = "ws", act = act });
        }

        public int SetClusterTransaction(string ClusterID, string TransactionIdList="", int act = 0){
            int ret = Do("SetCluster", new {  stype = "CLUSTER_TRANSACTION", id = ClusterID, name="", text = TransactionIdList, user="ws", act = act});
            ret = Do("SetClusterTransactionClean", new { filterID = ClusterID });
            return ret;
        }

     
    }
}
