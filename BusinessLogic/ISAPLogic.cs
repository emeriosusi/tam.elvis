﻿using System;
using System.Collections.Generic;
using Common.Data;
using Common.Data.SAPData;
using Common.Data._80Accrued;

namespace BusinessLogic
{
    public interface ISAPLogic
    {
        BudgetCheckResult _BudgetCheck_(BudgetCheckInput _i);
        BudgetCheckResult BudgetCheck(BudgetCheckInput _i, string uid = "");
        List<CashJournalMessage> CajoMaintain(
            CashJournalImportData import, 
            List<CashJournalTable> tables, 
            string uid);
        CashJournalData CajoRetrieve(
            CashJournalImportData import, 
            string uid);
        List<RVCheckHeader> CheckRV(
            List<RVCheckHeader> rvHeader, 
            List<RVCheckDetail> rvDetail, 
            ref List<RVCheckResult> results, 
            int pid, 
            int method);
        event SAP.Middleware.Connector.RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;
        void del();
        string GetRemainingBudget(int docno, int docyear, string wbs);
        string getRemainingBudget(string sWBSNo, string sFiscalYear);
        List<PVPostSAPResult> GetResultCheckedByAcc(
            List<PVPostInputHeader> _PVCheck, 
            int _ProcessId, 
            int method);
        List<PVPostSAPResult> GetResultPostPVToSAP(
            List<PVPostInputHeader> _PVPostInputHeader, 
            List<PVPostInputDetail> _PVPostInputDetail, 
            List<OneTimeVendorData> _vendorData, 
            int _ProcessId, string uid, 
            ref string errorMessage);
        void init();
        string LastMessageText { get; }
        double num(string s);
        VendorPostResult PostVendor(
            VendorInput v, 
            int _ProcessId, 
            string User);
        void removeDestination();
        List<PVPostSAPResult> ReversePV(ref List<PVReverseInput> _i, string uid = "");

        //**************************//
        //  ELVIS 2018 ENHANCEMENT  //
        //           START          //
        //**************************//
        List<BudgetRemaining> BudgetRemaining(List<string> budgetNo, string uid = "");
        List<SyncSAPData> SynchronizeBudget(List<SyncSAPData> data, string uid = "");
        List<ShiftingBudget> ShiftingBudget(List<ShiftingBudget> data, string uid = "");
        List<AccrFormSimulationData> SuspenseCheckPaid(List<AccrFormSimulationData> data, string uid = "");
        object AccruedPosting(AccrPostData data, string uid, string fid, string _loc);
        object AccruedClearing(AccrClearingData data, string uid, string fid, string _loc);

        //**************************//
        //            END           //
        //**************************//
    }
}
