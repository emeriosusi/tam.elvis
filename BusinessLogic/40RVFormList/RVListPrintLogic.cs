﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using Common.Data;
using Common.Data._40RVList;
using Common.Function;

//fid.pras 13072018
using BusinessLogic;
using BusinessLogic.VoucherForm;

using System.Web.UI;
using BusinessLogic.CommonLogic;
using DataLayer.Model;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Data;
using System.Data.EntityClient;
using System.IO;

namespace BusinessLogic._40RVFormList
{
    public class RVListPrintLogic
    {
        //fid.pras 13072018
        private FormPersistence _fp = null;
        private FormPersistence fp
        {
            get
            {
                if (_fp == null)
                    _fp = new FormPersistence();
                return _fp;
            }
        }

        private LogicFactory logic = LogicFactory.Get();

        public string printCover(Page _page,
                               String imagePath,
                               String templatePath,
                               UserData _UserData,
                               List<RVListData> listData,
                               ref ErrorData _Err)
        {
            string outLink = "";
            try
            {
                string tempelCover = File.ReadAllText(Path.Combine(templatePath, "RVCoverTemplateNew.html"));
                string tempelTTD = File.ReadAllText(Path.Combine(templatePath, "PVCoverTemplateTTD.html"));
                Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
                String _FileName = "RV_Cover_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                Ini pvcover = new Ini(templatePath + "\\PVCoverTemplate.ini");
                Templet tCover = new Templet(tempelCover);

                String contentTemp;
                HTMLWorker hw = new HTMLWorker(doc);

                MemoryStream ms = new MemoryStream();
                PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);

                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;
                doc.Open();
                StringBuilder amounts = new StringBuilder("");


                for (int i = 0; i < listData.Count; i++)
                {
                    tCover.Init(tempelCover);
                    Filler fCover = tCover.Mark("COMPANY_NAME,ISSUING_DIVISION,RV_DATE,RV_NO,"
                       + "VENDOR_NAME,TRANSACTION_TYPE,PAYMENT_METHOD,"
                       + "BUDGET_NO_DESC,DESC,RV_TYPE_NAME,AMOUNT,TTD,USERNAME,PRINT_DATE"); // Rinda Rahayu 20160331
                       //+ "BUDGET_NO_DESC,DESC,AMOUNT,USERNAME,PRINT_DATE");
                    RVListData data = listData[i];
                    Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(AppSetting.CompanyLogo)));
                    img.ScaleToFit(85, 85);
                    System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                    String username = String.Format("{0} {1}", _UserData.FIRST_NAME, _UserData.LAST_NAME);
                    username = myTI.ToTitleCase(username.ToLower());
                                                            
                    amounts.Clear();
                    List<AmountData> la = logic.Base.GetAmounts(data.RV_NO.Int(), data.RV_YEAR.Int());
                    foreach (AmountData amt in la)
                    {
                        if (amt.TOTAL_AMOUNT != null && (amt.TOTAL_AMOUNT > 0))
                            amounts.AppendFormat(
                                "<tr><td>{0}</td><td style='font-size: 16pt;' align='right'>{1}</td></tr>"
                                , amt.CURRENCY_CD
                                , CommonFunction.Eval_Curr(amt.CURRENCY_CD, amt.TOTAL_AMOUNT));
                    }
                    
                    string budgetNoDesc = "";

                    //fid.pras 13/07/2018 budget no dgn format wbs_no/wbs_desc/booking_no

                    if (!data.BUDGET_NO.isEmpty() && data.TRANSACTION_CD.Int() == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
                    {

                        List<WBSStructure> wbs = fp.WbsNumbers(data.BUDGET_NO);
                        string desc="", wbsno="";
                        if (wbs != null && wbs.Count > 0)
                        {
                            desc = wbs[0].Description;
                            wbsno = wbs[0].WbsNumber;
                        }
                        // CodeConstant vendorGroup = fp.getVendorGroupNameByVendor(data.BUDGET_NO);
                        data.BUDGET_NO_FORMAT = (data.BUDGET_NO != null) ? wbsno + " / " + desc + " / " : "";

                        budgetNoDesc = data.BUDGET_NO_FORMAT + "  <br>" + data.BUDGET_NO;
                    }

                    else if (!data.BUDGET_NO.isEmpty())
                    {
                        budgetNoDesc = data.BUDGET_NO + "-" + data.BUDGET_DESCRIPTION;
                    }

                    /* Start Rinda Rahayu 20160331*/
                    string ttdStr = tempelTTD;

                    List<CodeConstant> tranSkipApproval = CommonData.GetTransactionTypesSkipApproval();
                    CodeConstant codeSkipApp = (tranSkipApproval != null && tranSkipApproval.Count > 0)
                        ? tranSkipApproval[0]
                        : new CodeConstant() { Code = "", Description = "" };
                    String ttd = "";
                    if (tranSkipApproval.Count() > 0)
                    {
                        String[] strSplit = codeSkipApp.Description.Split(',');
                        for (i = 0; i < strSplit.Length; i++)
                        {
                            if (strSplit[i].ToString().Equals(data.TRANSACTION_CD))
                            {
                                ttd = "";
                                break;
                            }
                            else
                            {
                                ttd = ttdStr;
                            }

                        }
                    }

                    string rvTypeName = "";
                    if (data.RV_TYPE_NAME != null && !data.RV_TYPE_NAME.Equals(""))
                    {
                        rvTypeName = data.RV_TYPE_NAME;
                    }
                    else
                    {
                        List<CodeConstant> pvTypeList = CommonData.GetRVTypes(0);
                        for (i = 0; i < pvTypeList.Count; i++)
                        {
                            CodeConstant codeCs = pvTypeList[i];
                            if (codeCs.Code.Equals(data.RV_TYPE_CD))
                            {
                                rvTypeName = codeCs.Description;
                                break;
                            }
                        }
                    }

                    /* End Rinda Rahayu 20160331*/

                    fCover.Set(
                           AppSetting.CompanyName,
                           data.DIVISION_NAME,
                           data.RV_DATE.ToString("d/M/yyyy"),
                           data.RV_NO,
                           String.Format("{0}: {1}", data.VENDOR_CD, data.VENDOR_NAME),
                           data.TRANSACTION_NAME,
                           data.PAY_METHOD_NAME,
                           budgetNoDesc,
                           data.DESCRIPTION,
                           rvTypeName, // Rinda Rahayu 20160331
                           amounts.ToString(),
                           ttd, // Rinda Rahayu 20160331
                           username,
                           DateTime.Now.ToString("d MMMM yyyy"));

                    string barcodeFormat = "{0}\t{1}\r";
                    if (!pvcover["barcode.format"].isEmpty())
                    {
                        barcodeFormat = pvcover["barcode.format"];
                        barcodeFormat = barcodeFormat.Trim()
                            .Replace("\\t", "\t")
                            .Replace("\\r", "\r")
                            .Replace("\\n", "\n");
                    }
                    Barcode128 bar = new Barcode128();

                    bar.CodeType = Barcode.CODE128;
                    bar.ChecksumText = false;
                    bar.GenerateChecksum = false;
                    bar.StartStopText = false;
                    bar.Code = string.Format(barcodeFormat, data.RV_NO, data.RV_YEAR);

                    Image imgBarCode = null;

                    imgBarCode = bar.CreateImageWithBarcode(PDFWriter.DirectContent, null, null);

                    StyleSheet sh = new StyleSheet();
                    contentTemp = tCover.Get();
                    //File.WriteAllText(Util.GetTmp(LoggingLogic.GetPath(), "rvCover_%TIME%", ".txt"), contentTemp);

                    List<IElement> elements = HTMLWorker.ParseToList(new StringReader(contentTemp), sh);

                    string padding = pvcover["MF.Padding"];
                    PrintHelper.padMF = padding.Int(PrintHelper.padMF);
                    string mfCols = pvcover["MF.Columns"];
                    string mfRows = pvcover["MF.Rows"];
                    string mfTable = pvcover["MF.Table"];

                    PdfPTable digits = new PdfPTable(10);
                    for (int ix = 0; ix < 10; ix++)
                    {
                        PdfPCell x10 = new PdfPCell(new Paragraph("  "));
                        x10.BorderWidthTop = 1;
                        x10.BorderWidthRight = 1;
                        x10.BorderWidthBottom = 1;
                        x10.BorderWidthLeft = (ix > 0) ? 0 : 1;
                        x10.PaddingTop = PrintHelper.padMF;
                        x10.PaddingBottom = PrintHelper.padMF;
                        x10.PaddingLeft = PrintHelper.padMF;
                        x10.PaddingRight = PrintHelper.padMF;
                        digits.AddCell(x10);
                    }
                    digits.WidthPercentage = 100;

                    float[] wtDigits = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                    PrintHelper.WidthsParse(pvcover, "MF.Digits", ref wtDigits);
                    digits.SetWidths(wtDigits);

                    float[] wtRow = new float[] { 17, 20 };
                    PrintHelper.WidthsParse(pvcover, "MF.Rows", ref wtRow);

                    float[] wtTable = new float[] { 5, 6 };
                    PrintHelper.WidthsParse(pvcover, "MF.Table", ref wtTable);

                    PdfPTable pRow = new PdfPTable(2);

                    PrintHelper.coverMFRow(pRow, new PdfPCell(digits), "SAP Receipt Document No");
                    PrintHelper.coverMFRow(pRow, new PdfPCell(new Paragraph("")), "Receipt Date");

                    pRow.SetWidths(wtRow);

                    pRow.WidthPercentage = 100;

                    PdfPTable tMF = new PdfPTable(2);
                    PdfPCell mf0 = new PdfPCell(imgBarCode);
                    mf0.BorderWidth = 0;
                    tMF.AddCell(mf0);
                    PdfPCell mf1 = new PdfPCell(pRow);
                    tMF.AddCell(mf1);

                    tMF.SetWidths(wtTable);
                    tMF.WidthPercentage = 100;
                    tMF.SpacingBefore = 0f;
                    tMF.SpacingAfter = 10f;
                    doc.Add(tMF);

                    int logoTop = pvcover["logo.top"].Int(680);
                    int logoLeft = pvcover["logo.left"].Int(40);

                    PdfPTable tableHeader = elements[0] as PdfPTable;
                    PdfPCell[] hCells = tableHeader.Rows[0].GetCells();
                    hCells[0].AddElement(img);
                    tableHeader.SetWidths(new float[] { 1f, 5f });
                    doc.Add(tableHeader);

                    Paragraph par = elements[1] as Paragraph;
                    par.SpacingBefore = 50f;
                    par.SpacingAfter = 10f;
                    doc.Add(par);

                    PdfPTable tableDetail = elements[2] as PdfPTable;
                    tableDetail.SetWidths(new float[] { 4f, 7f, 2f, 4f });

                    for (int j = 0; j < tableDetail.Rows.Count; j++)
                    {
                        PdfPCell[] cells = tableDetail.Rows[j].GetCells();

                        for (int k = 0; k < cells.Count(); k++)
                        {
                            PdfPCell cell = cells[k];
                            if (cell != null)
                            {
                                cell.NoWrap = true;

                                if (k == 1 && j == 1)
                                {
                                    // cell.PaddingTop = -17f;
                                }
                                else if (j != 1 && k % 2 != 0)
                                {
                                    cell.PaddingTop = -4f;
                                    cell.PaddingBottom = 3f;
                                }
                                else if (j != 1 && k % 2 == 0)
                                {
                                    cell.PaddingTop = -2f;
                                    cell.PaddingBottom = 4f;
                                }
                            }
                        }
                    }

                    PdfPTable wraper = new PdfPTable(1);
                    PdfPCell cellWrap = new PdfPCell(tableDetail);
                    cellWrap.BorderWidth = 1f;
                    wraper.AddCell(cellWrap);
                    wraper.WidthPercentage = 100f;

                    doc.Add(wraper);

                    doc.Add(new Paragraph("\n"));

                    PdfPTable tableAmount = elements[3] as PdfPTable;
                    tableAmount.SetWidths(new float[] { 1f, 3f });

                    for (int j = 0; j < tableAmount.Rows.Count; j++)
                    {
                        PdfPCell[] cells = tableAmount.Rows[j].GetCells();

                        for (int k = 0; k < cells.Count(); k++)
                        {
                            PdfPCell cell = cells[k];
                            if (cell != null)
                            {
                                if (k % 2 != 0)
                                {
                                    cell.PaddingTop = -7f;
                                }
                                else
                                {
                                    cell.PaddingTop = -4f;
                                }
                            }
                        }
                    }
                    doc.Add(tableAmount);           

                    // Start Rinda Rahayu 20160331
                    PdfPTable tableTtd = elements[4] as PdfPTable;
                    tableTtd.TotalWidth = 540f;
                    tableTtd.SetWidths(new float[] { 8f, 98f });
                    tableTtd.WriteSelectedRows(0, -1, 4f, 250f, PDFWriter.DirectContent);
                    // End Rinda Rahayu 20160331

                    PdfPTable tableFooter = elements[5] as PdfPTable;
                    tableFooter.TotalWidth = 300f;
                    tableFooter.SetWidths(new float[] { 2f, 7f });
                    tableFooter.WriteSelectedRows(0, -1, 350f, 100f, PDFWriter.DirectContent);

                    if (listData.Count - 1 > i)
                    {
                        doc.NewPage();
                    }
                }

                doc.Close();
                // reader.Close();

                outLink = Xmit.Download(ms.GetBuffer(), _page, _FileName, ".pdf", "printCover", "printCoverRV");
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                _Err.ErrID = 2;
                _Err.ErrMsg = ex.Message;                
            }
            return outLink;
        }

        public void printTicket(Page _page,
                                String imagePath,
                                String templatePath,
                                UserData _UserData,
                                List<RVListData> listData,
                                String cashierName,
                                ref ErrorData _Err)
        {
            try
            {
                TextReader reader = new StreamReader(templatePath + "\\RVTicketTemplate.htm");
                Document doc = new Document(PageSize.A4, 40, 40, 40, 40);
                String _FileName = "RV_Ticket_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                String contentBody = reader.ReadToEnd();
                String contentTemp;
                Decimal amountIDR = 0;
                Decimal amountUSD = 0;
                Decimal amountJPY = 0;
                int lastIndex = 0;

                System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                String username = String.Format("{0} {1}", _UserData.FIRST_NAME, _UserData.LAST_NAME);
                username = myTI.ToTitleCase(username.ToLower());
                cashierName = myTI.ToTitleCase(cashierName.ToLower());

                HTMLWorker hw = new HTMLWorker(doc);

                MemoryStream ms = new MemoryStream();
                PdfWriter PDFWriter = PdfWriter.GetInstance(doc, ms);

                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;
                doc.Open();
                Image img = Image.GetInstance(Path.Combine(imagePath, Path.GetFileName(AppSetting.CompanyLogo)));
                img.ScaleToFit(85f, 85f);
                setValue(ref contentBody, "[COMPANY_NAME]", AppSetting.CompanyName);
                setValue(ref contentBody, "[ISSUING_DIVISION]", listData.FirstOrDefault().DIVISION_NAME);
                setValue(ref contentBody, "[PRINT_DATE]", DateTime.Now.ToString("d MMMM yyyy"));
                setValue(ref contentBody, "[FINANCE_CASHIER]", cashierName);
                setValue(ref contentBody, "[DIVISION_PIC]", username);

                contentTemp = contentBody;

                for (int i = 0; i < listData.Count; i++)
                {
                    RVListData data = listData[i];
                    int index = i < 7 ? i : i % 7;

                    setValue(ref contentTemp, String.Format("[NO_{0}]", index), (i + 1).ToString());
                    setValue(ref contentTemp, String.Format("[RV_NO_{0}]", index), data.RV_NO);
                    setValue(ref contentTemp, String.Format("[RV_DATE_{0}]", index), data.RV_DATE.ToString("d/M/yyyy"));
                    setValue(ref contentTemp, String.Format("[VENDOR_{0}]", index), String.Format("{0}: {1}", data.VENDOR_CD, data.VENDOR_NAME));

                    if (contentTemp.Contains(String.Format("[IDR_VALUE_{0}]", index)))
                    {
                        String amt;
                        if (String.IsNullOrEmpty(data.AMOUNT_IDR))
                        {
                            amt = "";
                        }
                        else
                        {
                            amt = data.AMOUNT_IDR;
                            amountIDR += Decimal.Parse(data.AMOUNT_IDR);
                        }
                        contentTemp = contentTemp.Replace(String.Format("[IDR_VALUE_{0}]", index), amt);
                    }

                    if (contentTemp.Contains(String.Format("[USD_VALUE_{0}]", index)))
                    {
                        String amt;
                        if (String.IsNullOrEmpty(data.AMOUNT_USD))
                        {
                            amt = "";
                        }
                        else
                        {
                            amt = data.AMOUNT_USD;
                            amountUSD += Decimal.Parse(data.AMOUNT_USD);
                        }
                        contentTemp = contentTemp.Replace(String.Format("[USD_VALUE_{0}]", index), amt);
                    }

                    if (contentTemp.Contains(String.Format("[JPY_VALUE_{0}]", index)))
                    {
                        String amt;
                        if (String.IsNullOrEmpty(data.AMOUNT_JPY))
                        {
                            amt = "";
                        }
                        else
                        {
                            amt = data.AMOUNT_JPY;
                            amountJPY += Decimal.Parse(data.AMOUNT_JPY);
                        }
                        contentTemp = contentTemp.Replace(String.Format("[JPY_VALUE_{0}]", index), amt);
                    }

                    if (i > 5 && (i + 1) % 7 == 0)
                    {
                        writeDetailTicket(img,
                                          ref doc,
                                          ref contentTemp,
                                          amountIDR,
                                          amountUSD,
                                          amountJPY,
                                          PDFWriter.DirectContent);

                        contentTemp = contentBody;

                        amountIDR = 0;
                        amountUSD = 0;
                        amountJPY = 0;

                        doc.NewPage();
                    }

                    lastIndex++;
                }

                int mods = listData.Count % 7;

                if (mods > 0)
                {
                    for (int i = mods; i < 7; i++)
                    {
                        setValue(ref contentTemp, String.Format("[NO_{0}]", i), (lastIndex++ + 1).ToString());
                        setValue(ref contentTemp, String.Format("[RV_NO_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[RV_DATE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[VENDOR_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[IDR_VALUE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[USD_VALUE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[JPY_VALUE_{0}]", i), "&nbsp;");
                        setValue(ref contentTemp, String.Format("[CHECK_{0}]", i), "&nbsp;");
                    }

                    writeDetailTicket(img,
                                      ref doc,
                                      ref contentTemp,
                                      amountIDR,
                                      amountUSD,
                                      amountJPY,
                                      PDFWriter.DirectContent);
                }

                doc.Close();
                reader.Close();

                _page.Response.Clear();
                _page.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.pdf", _FileName));
                _page.Response.ContentType = "application/pdf";
                _page.Response.Buffer = true;
                _page.Response.OutputStream.Write(ms.ToArray(), 0, ms.ToArray().Length);
                _page.Response.End();
            }
            catch (Exception ex)
            {
                _Err.ErrID = 2;
                _Err.ErrMsg = ex.Message;
            }
        }

        public void writeDetailTicket(Image img,
                                      ref Document doc,
                                      ref String contentTemp,
                                      Decimal amountIDR,
                                      Decimal amountUSD,
                                      Decimal amountJPY,
                                      PdfContentByte cb)
        {
            img.SetAbsolutePosition(40, 750);
            doc.Add(img);

            img.SetAbsolutePosition(40, 375);
            doc.Add(img);

            cb.SetLineWidth(1f);
            cb.SetGrayStroke(0f);
            cb.MoveTo(40f, 428f);
            cb.LineTo(553f, 428f);
            cb.Stroke();

            setValue(ref contentTemp, "[Total_IDR]", String.Format("{0:N0}", amountIDR));
            setValue(ref contentTemp, "[Total_USD]", String.Format("{0:N0}", amountUSD));
            setValue(ref contentTemp, "[Total_JPY]", String.Format("{0:N0}", amountJPY));

            StyleSheet sh = new StyleSheet();

            List<IElement> elements = HTMLWorker.ParseToList(new StringReader(contentTemp), sh);
            Int16 ind = 0;
            for (int l = 0; l < 2; l++)
            {
                PdfPTable table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 3f, 11f, 6f });
                doc.Add(table);

                Paragraph par = elements[ind++] as Paragraph;
                par.SpacingBefore = 5f;
                par.SpacingAfter = 10f;
                doc.Add(par);

                table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 3f, 5f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();

                    foreach (var cell in cells)
                    {
                        if (cell != null)
                        {
                            cell.PaddingTop = -3;
                        }
                    }
                }
                doc.Add(table);

                doc.Add(new Paragraph("\n"));

                table = elements[ind++] as PdfPTable;
                table.SetWidths(new float[] { 1.7f, 5.5f, 5f, 13.5f, 5.5f, 5.5f, 5.5f, 3.8f });
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    PdfPCell[] cells = table.Rows[i].GetCells();

                    for (int j = 0; j < cells.Count(); j++)
                    {
                        PdfPCell cell = cells[j];
                        if (cell != null)
                        {
                            if (i > 0)
                            {
                                cell.NoWrap = true;
                            }
                            cell.PaddingTop = -3;
                            if (j == cells.Count() - 1 && i > 1 && i < table.Rows.Count - 1)
                            {
                                PdfTemplate template = cb.CreateTemplate(30, 30);
                                template.SetLineWidth(0.5f);
                                template.Rectangle(5f, 1f, 8f, 8f);
                                template.Stroke();

                                img = Image.GetInstance(template);
                                Chunk chunk = new Chunk(img, 12f, 1f);

                                cell.AddElement(chunk);
                            }
                        }
                    }
                }
                doc.Add(table);

                table = elements[ind++] as PdfPTable;
                PdfPCell[] cl = table.Rows[0].GetCells();
                foreach (var c in cl)
                {
                    if (c != null)
                    {
                        c.PaddingTop = 5f;
                        c.PaddingBottom = 25f;
                    }
                }
                doc.Add(table);
            }
        }

        private List<RVCoverData> getInvoiceValue(string RV_NO, string RV_YEAR)
        {
            Int32 pvNo;
            Int16 pvYear;
            #region build searching criteria
            if (!string.IsNullOrEmpty(RV_NO))
                pvNo = Int32.Parse(RV_NO);
            else
                pvNo = 0;

            if (!string.IsNullOrEmpty(RV_YEAR))
                pvYear = Int16.Parse(RV_YEAR);
            else
                pvYear = 0;
            #endregion

            List<RVCoverData> data = new List<RVCoverData>();
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var q = (from p in db.vw_RVCoverInvoiceList
                         where p.RV_NO == pvNo
                         where p.RV_YEAR == pvYear
                         select p).ToList();

                if (q.Any())
                {
                    RVCoverData resultData = new RVCoverData();

                    foreach (var _data in q)
                    {
                        resultData.CURRENCY = _data.CURRENCY_CD;
                        resultData.AMOUNT = _data.AMOUNT;
                        resultData.INVOICE_DATE = _data.INVOICE_DATE;
                        resultData.TAX_NO = _data.TAX_NO;
                        resultData.INVOICE_NO = _data.INVOICE_NO;
                        resultData.TAX_AMOUNT = _data.TAX_AMOUNT;

                        data.Add(resultData);
                    }
                }
            }
            return data;
        }

        private void setValue(ref String content, String key, String value)
        {
            if (content.Contains(key))
            {
                content = content.Replace(key, value);
            }
        }
    }
}
