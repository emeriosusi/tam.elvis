﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.IO;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;

namespace BusinessLogic._40RVList
{
    public partial class RVListLogic : VoucherListLogic
    {
        public bool RvCheck(List<string> _keys, int pid, ref string _Message, string uid = "", int method = RV_CHECK_BY_ACCOUNTING)
        {
            string _loc = string.Format("RvCheck({0})", method);
            int checkedStatus = (method == RV_CHECK_BY_ACCOUNTING) ? 68 : 61;
            bool result = false;
            List<RVCheckHeader> _headers = new List<RVCheckHeader>();
            List<RVCheckHeader> _ret = new List<RVCheckHeader>();
            List<RVCheckDetail> _details = new List<RVCheckDetail>();
            List<RVCheckResult> _results = new List<RVCheckResult>();

            List<string> _isChecked = new List<string>();
            List<string> _isNotReadyToBeChecked = new List<string>();
            List<string> _isPaid = new List<string>();
            List<string> fails = new List<string>();
            List<string> suceeds = new List<string>();

            FiscalDocs f = new FiscalDocs();

            this["OK"] = suceeds;
            this["NG"] = fails;
            this["done"] = _isChecked;
            this["paid"] = _isPaid;
            this["notready"] = _isNotReadyToBeChecked;
            int _no = 0;
            int _yy = 0;

            try
            {
                string key;
                int i = 1;

                RVCheckHeader h;
                //int? simpleAmountCheck= ss.GetValue("RV_CHECK", "SIMPLE_AMOUNT_CHECK");
                //bool sac = (simpleAmountCheck ?? 0) == 1;
                foreach (string reff in _keys)
                {
                    string[] nodanyear = _keys[0].ToString().Split(':');
                    _no = nodanyear[0].Int(0);
                    _yy = nodanyear[1].Int(0);
                    i = 0;
                    var q = from dx in db.vw_RVCheck
                            where dx.RV_NO == _no && dx.RV_YEAR == _yy
                            select dx;
                    List<PostItemData> po = new List<PostItemData>();
                    if (q.Any())
                    {
                        q = q.OrderBy(x => x.CURRENCY_CD)
                            .ThenBy(X => X.FROM_CURR);

                        i = (q.Select(x => x.ITEM_NO).Max() ?? 0) + 1;
                    }

                    List<vw_RVCheck> l = q.ToList();

                    int j = 0;
                    while (j < l.Count && l[j].NO_YEAR == reff)
                    {
                        var data = l[j];
                        h = new RVCheckHeader();
                        bool hasFractions = !AppSetting.RoundCurrency.Contains(data.CURRENCY_CD);
                        key = data.RV_NO.str() + data.RV_YEAR.str() + data.CURRENCY_CD + data.FROM_CURR;
                        decimal amt = 0;
                        decimal spent = 0;
                        // LoggingLogic.say(_no.str() + _yy.str(), "{0,2} {1,2} {2,3} {3,15} {4,15}", "T", "ME", "SEQ", "amt", "spent");
                        int itemNumber = i;
                        if (data.RV_TYPE_CD == 2)
                        {
                            if (i <= 0)
                            {
                                int? maxi = GetSuspenseMaxItemNo(data.SUSPENSE_NO ?? 0, data.SUSPENSE_YEAR ?? 0);
                                if (maxi.HasValue)
                                    i = maxi.Value + 1;
                            }
                            itemNumber = data.ITEM_NO ?? i;
                        }
                        else
                        {
                            if (i <= 0)
                            {
                                i = 1;
                            }
                            itemNumber = i;
                        }

                        int fYear = logic.Sys.FiscalYear(data.RV_DATE);
                        string fiscalYear = fYear.str();

                        h.RV_NO = data.RV_NO.str();
                        h.RV_YEAR = fiscalYear;
                        h.ITEM_NO = itemNumber.str();
                        h.RV_DATE = data.RV_DATE.SAPdate();
                        h.RV_TYPE = data.RV_TYPE_CD.str();
                        h.TRANS_TYPE = data.TRANSACTION_CD.str();
                        h.COST_CENTER_FLAG = data.COST_CENTER_FLAG;
                        if (method == RV_CHECK_BY_ACCOUNTING)
                            h.POSTING_DT = data.POSTING_DATE.SAPdate();
                        else
                            h.POSTING_DT = DateTime.Now.SAPdate();

                        h.CURRENCY = data.CURRENCY_CD;
                        h.TEXT = Textuality(data.STD_WORDING, data.DESCRIPTION);
                        h.VENDOR_NO = data.VENDOR_CD;
                        h.METHODE_FLAG = method.str();

                        h.FROM_CURR = data.FROM_CURR;

                        h.EXRATE_SUSPENSE = data.EXRATE_SUSPENSE.SAPnum(true);
                        h.EXCHANGE_RATE = data.EXCHANGE_RATE.SAPnum(true);

                        h.REFUND_FLAG = data.REFUND_FLAG.str();
                        h.TEMP_ACC_AMOUNT = data.TEMP_ACC_AMOUNT.SAPdecfmt(false);
                        h.TEMP_ACC_ORI_AMOUNT = data.TEMP_ACC_ORI_AMOUNT.SAPnum(hasFractions);
                        h.DIFF_EXRATE = null;

                        decimal diffExrate = 0;

                        if (!hasFractions  // make sure its round currency 
                            && (data.EXCHANGE_RATE ?? 0) > 0 // and divisible 
                            )
                        {
                            decimal difExrateRatio = 1;
                            decimal difExrate = (data.EXRATE_SUSPENSE ?? 0) / (data.EXCHANGE_RATE ?? 1);

                            difExrateRatio = difExrateRatio - difExrate;
                            diffExrate = Math.Round(data.AMOUNT * difExrateRatio, 2);
                        }

                        if (data.RV_TYPE_CD == 2 // is settlement 
                            && !data.FROM_CURR.isEmpty()  // from Cashier Policy
                            && data.REFUND_FLAG == 0  // only set Difference exchange rate for normal case 
                            )
                        {
                            h.DIFF_EXRATE = diffExrate.SAPdecfmt();
                        }

                        List<int> added = new List<int>();
                        while (j < l.Count && key.Equals(l[j].RV_NO.str() + l[j].RV_YEAR.str() + l[j].CURRENCY_CD + l[j].FROM_CURR))
                        {
                            if ((method == RV_RECEIVED_BY_CASHIER && data.STATUS_CD == 61)
                                  || (method == RV_CHECK_BY_ACCOUNTING && data.STATUS_CD == 69)) // already been checked   
                            {
                                if (!_isChecked.Contains(key))
                                {
                                    _isChecked.Add(key);
                                }
                            }
                            else if (method == RV_CHECK_BY_ACCOUNTING && data.STATUS_CD != 70) // is not ready to be checked (not yet paid)
                            {
                                if (!_isNotReadyToBeChecked.Contains(key))
                                {
                                    _isNotReadyToBeChecked.Add(key);
                                }
                            }
                            else
                            {
                                amt += data.AMOUNT;

                                if (data.RV_TYPE_CD == 2)
                                {
                                    decimal spentAdd = 0;
                                    bool hasFrac = !AppSetting.RoundCurrency.Contains(l[j].CURRENCY_CD);
                                    if ((!hasFrac) && (!h.FROM_CURR.isEmpty()))
                                    {
                                        if (data.REFUND_FLAG == 0)
                                        {
                                            spentAdd = data.SPENT_AMOUNT.Frac() + data.AMOUNT.Frac() - data.SUSPENSE_AMOUNT.Frac() - diffExrate.Frac();

                                            log.Log("MSTD0001INF", string.Format("[{0}] spent add={1}, SPENT_AMOUNT={2}, SUSPENSE={3}, AMOUNT={4}, DIFF_EXRATE={5}",
                                                h.ITEM_NO, spentAdd, data.SPENT_AMOUNT, data.SUSPENSE_AMOUNT, data.AMOUNT, diffExrate), _loc);
                                        }
                                        else if (data.REFUND_FLAG == 1 || data.REFUND_FLAG == 2)
                                        {
                                            decimal tempAcc = (data.SPENT_AMOUNT ?? 0) + data.AMOUNT - (data.SUSPENSE_AMOUNT ?? 0);
                                            spentAdd = (data.SPENT_AMOUNT ?? 0).Frac() + data.AMOUNT.Frac() - (data.SUSPENSE_AMOUNT ?? 0).Frac() - tempAcc.Frac();

                                            log.Log("MSTD0001INF", string.Format("[{0}] spent add={1}, SPENT_AMOUNT={2}, SUSPENSE={3}, AMOUNT={4}, tempAcc={5}",
                                                h.ITEM_NO, spentAdd, data.SPENT_AMOUNT, data.SUSPENSE_AMOUNT, data.AMOUNT, tempAcc), _loc);
                                        }
                                    }
                                    spent += (data.SPENT_AMOUNT ?? 0) + Math.Round(spentAdd);
                                }
                                else
                                {
                                    spent += (data.SPENT_AMOUNT ?? 0);
                                }
                                if (method == RV_CHECK_BY_ACCOUNTING)
                                {
                                    RVCheckDetail _d = new RVCheckDetail()
                                    {
                                        RV_NO = data.RV_NO.str(),
                                        RV_YEAR = fiscalYear,
                                        ITEM_NO = itemNumber.str(),
                                        GL_ACCOUNT = data.GL_ACCOUNT.str(),
                                        AMOUNT = data.SPENT_AMOUNT.SAPdecfmt(hasFractions),
                                        COST_CENTER = data.COST_CENTER_CD,
                                        WBS_ELEMENT = data.BUDGET_NO,
                                        ITEM_TEXT = data.DESCRIPTION
                                    };
                                    _details.Add(_d);
                                    added.Add(_details.Count - 1);
                                }

                            }
                            j++;
                        }

                        // LoggingLogic.say(_no.str()+ _yy.str(), "{0,2} {1,2} {2,3} {3,15} {4,15}", data.RV_TYPE_CD, method, data.SEQ_NO, amt, spent);
                        if ((data.RV_TYPE_CD == 1)
                            || (
                                  (data.RV_TYPE_CD == 2)
                                  && (
                                        ((method == RV_RECEIVED_BY_CASHIER) && (amt > 0))
                                          || ((method == RV_CHECK_BY_ACCOUNTING)
                                                     && ((amt > 0) || (spent > 0))
                                              )
                                     )
                               )
                            )
                        {
                            if (method == RV_CHECK_BY_ACCOUNTING) h.AMOUNT = amt.str(); else h.AMOUNT = amt.SAPdecfmt(hasFractions);
                            f.Add(data.RV_NO, data.RV_YEAR, fYear);

                            if (method == RV_CHECK_BY_ACCOUNTING)
                            {
                                h.PV_NO_SUSPENSE = data.SUSPENSE_NO.str();
                                h.PV_YEAR_SUSPENSE = data.SUSPENSE_YEAR.str();
                                h.PV_DATE_SUSPENSE = GetSuspenseDate(h.PV_NO_SUSPENSE.Int(), h.PV_YEAR_SUSPENSE.Int());
                            }

                            po.Add(new PostItemData()
                            {
                                DOC_NO = _no,
                                DOC_YEAR = _yy,
                                SEQ_NO = data.SEQ_NO,
                                ITEM_NO = itemNumber
                            });

                            _headers.Add(h);

                            i++;
                        }
                        else
                        {
                            /// delete details with amount = 0
                            for (int ai = added.Count - 1; ai >= 0; --ai)
                            {
                                _details.RemoveAt(added[ai]);
                            }
                        }
                    }
                    if (po.Count > 0)
                        UpdateDetailItemNo(db, po);
                }


                if (_isChecked.Count > 0)
                {
                    _Message = m.Message("MSTD00085ERR",
                        CommonFunction.CommaJoin(_isChecked));

                    log.Log("MSTD00085ERR", _Message, _loc, uid, "", pid);
                    return false;
                }
                else if (_isNotReadyToBeChecked.Count > 0)
                {
                    _Message = m.Message("MSTD00086ERR",
                        CommonFunction.CommaJoin(_isNotReadyToBeChecked));
                    log.Log("MSTD00086ERR", _Message, _loc, uid, "", pid);
                    return false;
                }
                else if (_isPaid.Count > 0)
                {
                    _Message = m.Message("MSTD00094WRN",
                        CommonFunction.CommaJoin(_isPaid));
                    log.Log("MSTD00094WRN", _Message, _loc, uid, "", pid);
                    return false;
                }

                if (_headers.Count > 0)
                {
                    _ret = logic.SAP.CheckRV(_headers, _details, ref _results, pid, method);
                    List<PostItemData> sapItem = new List<PostItemData>();
                    if (_results.Count > 0)
                    {
                        using (ContextWrap co = new ContextWrap())
                        {
                            var db = co.db;


                            RVCheckHeader[] ret = _ret
                            .OrderBy(a => a.RV_YEAR)
                            .ThenBy(a => a.RV_NO)
                            .ThenBy(a => a.ITEM_NO)
                            .ToArray();
                            int x = 0;
                            while (x < ret.Count())
                            {
                                var item = ret[x];

                                int DocNo = item.RV_NO.Int();
                                int DocYear = f.Doc(DocNo, item.RV_YEAR.Int()); /// get Doc Year from fiscal
                                string reff = item.RV_NO + DocYear.str();

                                bool posted = true;
                                                              

                                while (x < ret.Length && DocNo == ret[x].RV_NO.Int() 
                                        /// && DocYear == ret[x].RV_YEAR.Int() /// remarked condition because comparing fiscal year with document year makes endless loop 
                                    )
                                {
                                    if (!(ret[x].STATUS == "S")) posted = false;
                                    x++;
                                }

                                if (posted)
                                {
                                    if (!suceeds.Contains(reff))
                                    {
                                        suceeds.Add(reff);
                                    }

                                    if (method == RV_RECEIVED_BY_CASHIER)
                                    {
                                    }
                                    else
                                    {
                                        sapItem.Add(new PostItemData()
                                        {
                                            DOC_NO = DocNo,
                                            DOC_YEAR = DocYear,
                                            ITEM_NO = item.ITEM_NO.Int(),
                                            CURRENCY_CD = item.CURRENCY,
                                            SAP_DOC_NO = item.SAP_DOC_NO.isEmpty() ? (int?)null : (int?)item.SAP_DOC_NO.Int(),
                                            SAP_DOC_YEAR = item.SAP_DOC_YEAR.isEmpty() ? (int?)null : (int?)item.SAP_DOC_YEAR.Int()
                                        });
                                    }
                                }
                                else
                                {
                                    if (!fails.Contains(reff))
                                    {
                                        fails.Add(reff);
                                    }

                                }
                            }
                            if (suceeds.Count > 0)
                            {
                                foreach (string r in suceeds)
                                {
                                    int DocNo = (r.Length > 4) ? r.Substring(0, r.Length - 4).Int() : 0;
                                    int DocYear = (r.Length > 4) ? r.Substring(r.Length - 4, 4).Int() : 0;

                                    TB_R_RV_H _TB_R_RV_H = (from p in db.TB_R_RV_H
                                                            where p.RV_NO == DocNo && p.RV_YEAR == DocYear
                                                            select p).FirstOrDefault();
                                    if (_TB_R_RV_H != null)
                                    {
                                        if (_TB_R_RV_H.POSTING_DATE == null && method == RV_RECEIVED_BY_CASHIER)
                                            _TB_R_RV_H.POSTING_DATE = DateTime.Now.Date;
                                        _TB_R_RV_H.STATUS_CD = checkedStatus;
                                        db.SaveChanges();
                                    }
                                }

                                UpdateSapDocNo(db, sapItem);
                            }

                        }
                        result = (suceeds.Count > 0 && fails.Count < 1);


                        if (fails.Count > 0 && suceeds.Count > 0)
                        {
                            _Message = m.Message("MSTD00087INF",
                                CommonFunction.CommaJoin(suceeds),
                                CommonFunction.CommaJoin(fails));
                            log.Log("MSTD00087INF", _Message, _loc, uid, "", pid);
                        }
                        else if (fails.Count > 0)
                        {
                            _Message = m.Message("MSTD00070ERR", "RV");
                            log.Log("MSTD00070ERR", _Message, _loc, uid, "", pid);
                        }
                        else
                        {
                            _Message = m.Message("MSTD00069INF", "RV");
                            log.Log("MSTD00069INF", _Message, _loc, uid, "", pid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _Message = ex.Message;
                LoggingLogic.err(ex);                
                log.Log("MSTD00076ERR", ex.Message, _loc, uid, "", pid);

            }

            return result;
        }



        public void UpdateDetailItemNo(ELVIS_DBEntities db, List<PostItemData> l)
        {
            if (l == null || l.Count < 1) return;
            string lastreff = "";
            foreach (PostItemData d in l)
            {
                string reff = string.Format("{0}{1}", d.DOC_NO, d.DOC_YEAR);
                if (!lastreff.Equals(reff))
                {
                    // LoggingLogic.say(reff, "{0,10} {1,4} {2,3}{3,4}", "#", "", "seq", "itm");
                    lastreff = reff;
                }
                // LoggingLogic.say(reff, "{0,10} {1,4} {2,3}{3,4}", d.DOC_NO, d.DOC_YEAR, d.SEQ_NO, d.ITEM_NO);

                var q = from p in db.TB_R_RV_D
                        where p.RV_NO == d.DOC_NO && p.RV_YEAR == d.DOC_YEAR && p.SEQ_NO == d.SEQ_NO
                        select p;
                if (q.Any())
                {
                    foreach (var x in q)
                    {
                        x.ITEM_NO = d.ITEM_NO;
                    }
                    db.SaveChanges();
                }
            }
        }


    }
}
