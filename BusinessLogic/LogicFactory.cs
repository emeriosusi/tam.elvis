﻿using System;
using System.Web;
using System.Text; 
using System.Collections;
using System.Collections.Generic; 
using BusinessLogic._20MasterData;
using BusinessLogic._30PVFormList;
using BusinessLogic._30PVList;
using BusinessLogic._40RVFormList;
using BusinessLogic._40RVList;
using BusinessLogic._50Invoice;
using BusinessLogic._60SettlementForm;
using BusinessLogic._80Accrued;
using BusinessLogic.CommonLogic;
using BusinessLogic.PoA;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using DataLayer.Model;
using K2.Helper;
using BusinessLogic.AccruedForm;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace BusinessLogic
{
    public class LogicFactory : IDisposable
    {
        private static readonly string MyKey = "LogicFactory";
        private static readonly string IS_VALID_LOGIN = "isValidSessionLogin";
        private static readonly string LOG_PID = "logPID";
        private List<string> LogLocations = null;

        public static LogicFactory Be
        {
            get
            {
                LogicFactory x = null;
                if ( (HttpContext.Current != null) 
                   && HttpContext.Current.Items!=null 
                   && HttpContext.Current.Items.Contains(MyKey))
                {
                    x = HttpContext.Current.Items[MyKey] as LogicFactory;
                }
                return x;
            }
        }

        public static LogicFactory Get()
        {
            LogicFactory x = null;
            if (HttpContext.Current != null)
            {
                x = HttpContext.Current.Items[MyKey] as LogicFactory;
                if (x == null)
                {
                    x = new LogicFactory();
                    x.Init();
                    HttpContext.Current.Items[MyKey] = x;                    
                }
            }
            return x;             
        }

        public void Init()
        {
            if (string.IsNullOrEmpty(AppSetting.LogLocationEnable))
                LogLocations = null;
            else {
                LogLocations = new List<string>();
                LogLocations.AddRange(((AppSetting.LogLocationEnable).Split(new char[] { ',', ';', '\r', '\n', '\t', '*' }, StringSplitOptions.RemoveEmptyEntries)));
            }
            this.IDX = 0;
        }

        public int PROCESS_ID
        {
            get
            {
                int pid = 0;
                if (HttpContext.Current != null )
                {
                    if (HttpContext.Current.Session!= null && HttpContext.Current.Session[LOG_PID] != null) 
                        pid = Convert.ToInt32(HttpContext.Current.Session[LOG_PID]);
                    else if(HttpContext.Current.Items != null && HttpContext.Current.Items[LOG_PID] != null)
                        pid = Convert.ToInt32(HttpContext.Current.Items[LOG_PID]);

                }
                return pid; 
            }
            set
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Session != null)
                        HttpContext.Current.Session[LOG_PID] = value;
                    else if (HttpContext.Current.Items != null)
                        HttpContext.Current.Items[LOG_PID] = value;
                }
            }
        }

        private bool? _validSession = null;
        public bool? ValidSession
        {
            get
            {
                string v = null;
                if (HttpContext.Current != null)
                    v = HttpContext.Current.Items[IS_VALID_LOGIN] as string;
                if (string.IsNullOrEmpty(v))
                {
                    return null;
                }
                else
                {
                    return v.Equals("1");
                }

            }

            set
            {
                _validSession = value;
                if (HttpContext.Current != null)
                    if (_validSession ?? false)
                    {
                        HttpContext.Current.Items[IS_VALID_LOGIN] = "1";
                    }
                    else
                    {
                        HttpContext.Current.Items[IS_VALID_LOGIN] = "0";
                    }

            }
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
            HttpContext.Current.Items.Remove(MyKey);
            // LoggingLogic.say("0", "DEL LogicFactory", null);
            //me = null;
        }

        public readonly static string IDX_KF = "FactoryIDX";
        public int IDX
        {
            get
            {   
                int v = 0;
                if (Heap.Has(IDX_KF)) 
                    v = Heap.Get<int>(IDX_KF);
                v++;
                Heap.Set<int>(IDX_KF, v, Heap.Duration * 100);
                return v;
            }
            set
            {
                Heap.Set<int>(IDX_KF, value, Heap.Duration * 100);
            }
        }

        private ContextWrap _context = null;
        public ContextWrap Cx
        {
            get
            {
                if (_context == null)
                {
                    _context = new ContextWrap();
                }
                return _context;
            }
        }

        private LoggingLogic _log = null;
        public LoggingLogic Log
        {
            get
            {
                if (_log == null)
                {
                    _log = new LoggingLogic();
                }
                return _log;
            }
        }

        private LoggingLogic _say = null;
        public LoggingLogic Activity
        {
            get
            {
                if (_say == null)
                {
                    _say = new LoggingLogic();
                }
                return _say;
            }
        }

        public static string TrimUrl(string requestFilePath)
        {
            int j = requestFilePath.LastIndexOf("/");
            if (j >= 0)
            {
                return requestFilePath.Substring(j + 1).Replace(".aspx", "");
            }
            else
                return requestFilePath;
        }

        public int Say(string location, string w, params object[] x)
        {
            string uid = null;
            string loc = location;
            int pid = PROCESS_ID;

            if (   (LogLocations != null && (LogLocations.Count == 0 || LogLocations.Contains(location))) 
                    || (pid == 0)
                )
            {
                if (HttpContext.Current != null)
                {
                    UserData u = null;
                    if (HttpContext.Current.Session != null)
                    {
                        u = HttpContext.Current.Session["userData"] as UserData;
                    }
                    else
                    {
                        uid = "";
                    }
                    if (u != null)
                        uid = u.USERNAME;
                    if (HttpContext.Current.Request != null)
                        loc = TrimUrl(HttpContext.Current.Request.FilePath) + " " + location;
                }

                pid = Activity.Log("INF", string.Format(w, x), loc, uid, "", pid);
                PROCESS_ID = pid;
            }
            else
            {
                if (AppSetting.LogToText) 
                    Activity.Say(Common.Function.CommonFunction.CleanFilename(location), w, x); 
            }           
           
            return pid;
        }

        private WorkFlowLogic _wf = null;
        public WorkFlowLogic WorkFlow
        {
            get
            {
                if (_wf == null)
                {
                    _wf = new WorkFlowLogic();
                }
                return _wf;
            }
        }

        private WorkListLogic _worklist = null;
        public WorkListLogic WorkList
        {
            get
            {
                if (_worklist == null)
                {
                    UserData u = GetUserData();
                    if (u != null)
                    {
                        GetWorkList(u.USERNAME);
                    }
                    else
                        _worklist = new WorkListLogic();
                }
                return _worklist;
            }
        }

        public UserData GetUserData()
        {
            return (HttpContext.Current != null) ?
                        HttpContext.Current.Session["userData"] as UserData
                        : null;
        }

        public WorkListLogic GetWorkList(string userName)
        {
            if (_worklist == null)
                _worklist = new WorkListLogic(userName);

            return _worklist;
        }

        private PVApprovalLogic _pvApproval = null;
        public PVApprovalLogic PVApproval
        {
            get
            {
                if (_pvApproval == null)
                {
                    _pvApproval = new PVApprovalLogic();
                }
                return _pvApproval;
            }
        }

        private RVApprovalLogic _rvApproval = null;
        public RVApprovalLogic RVApproval
        {
            get
            {
                if (_rvApproval == null)
                {
                    _rvApproval = new RVApprovalLogic();
                }
                return _rvApproval;
            }
        }

        private PVListLogic _pvList = null;
        public PVListLogic PVList
        {
            get
            {
                if (_pvList == null)
                {
                    _pvList = new PVListLogic();
                }
                return _pvList;
            }
        }

        private PVListPrintLogic _pvPrint = null;
        public PVListPrintLogic PVPrint
        {
            get
            {
                if (_pvPrint == null)
                {
                    _pvPrint = new PVListPrintLogic();
                }
                return _pvPrint;
            }
        }

        private RVListPrintLogic _rvPrint = null;
        public RVListPrintLogic RVPrint
        {
            get
            {
                if (_rvPrint == null)
                {
                    _rvPrint = new RVListPrintLogic();
                }
                return _rvPrint;
            }
        }

        private RVListLogic _rvList = null;
        public RVListLogic RVList
        {
            get
            {
                if (_rvList == null)
                {
                    _rvList = new RVListLogic();
                }
                return _rvList;
            }
        }


        private MessageLogic _msg = null;
        public MessageLogic Msg
        {
            get
            {
                if (_msg == null)
                {
                    _msg = new MessageLogic();
                }
                return _msg;
            }
        }

        private PVRVWorkFlowLogic _pvrvWorkflow = null;
        public PVRVWorkFlowLogic PVRVWorkflow
        {
            get
            {
                if (_pvrvWorkflow == null)
                {
                    _pvrvWorkflow = new PVRVWorkFlowLogic();
                }
                return _pvrvWorkflow;
            }
        }

        private BaseBusinessLogic _base = null;
        public BaseBusinessLogic Base
        {
            get
            {
                if (_base == null)
                {
                    _base = new BaseBusinessLogic();
                }
                return _base;
            }
        }

        private PVFormLogic _pvForm = null;
        public PVFormLogic PVForm
        {
            get
            {
                if (_pvForm == null)
                {
                    _pvForm = new PVFormLogic();
                }
                return _pvForm;
            }
        }

        private RVFormLogic _rvForm = null;
        public RVFormLogic RVForm
        {
            get
            {
                if (_rvForm == null)
                {
                    _rvForm = new RVFormLogic();
                }
                return _rvForm;
            }
        }

        private RVFormListLogic _rvFormList = null;
        public RVFormListLogic RVFormList
        {
            get
            {
                if (_rvFormList == null)
                {
                    _rvFormList = new RVFormListLogic();
                }
                return _rvFormList;
            }
        }

        private FormPersistence _persist = null;
        public FormPersistence Persist
        {
            get
            {
                if (_persist == null)
                {
                    _persist = new FormPersistence();
                }
                return _persist;
            }
        }

        private NoticeLogic _notice = null;
        public NoticeLogic Notice
        {
            get
            {
                if (_notice == null)
                {
                    _notice = new NoticeLogic();
                }
                return _notice;
            }
        }


        private LookUpLogic _look = null;
        public LookUpLogic Look
        {
            get
            {
                if (_look == null)
                {
                    _look = new LookUpLogic();
                }
                return _look;
            }
        }


        private PoALogic _poa = null;
        public PoALogic PoA
        {
            get
            {
                if (_poa == null)
                {
                    _poa = new PoALogic();
                    if (HttpContext.Current!=null && HttpContext.Current.Server!=null)
                        _poa.emailTemplateDir = HttpContext.Current.Server.MapPath(this.Sys.GetText("EMAIL_TEMPLATE", "DIR"));
                }
                return _poa;
            }
        }

        private SystemSettingLogic _sys = null;
        public SystemSettingLogic Sys
        {
            get
            {
                if (_sys == null)
                {
                    _sys = new SystemSettingLogic();
                }
                return _sys;
            }
        }

        private UserLogic _user = null;
        public UserLogic User
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserLogic();
                }
                return _user;
            }
        }

        private VoucherListLogic _voucherList = null;
        public VoucherListLogic VoucherList
        {
            get
            {
                if (_voucherList == null)
                {
                    _voucherList = new VoucherListLogic();
                }
                return _voucherList;
            }
        }

        private ISAPLogic _sap = null;
        public ISAPLogic SAP
        {
            get
            {
                if (_sap == null)
                {
                    if (Common.AppSetting.NOOP_SAP)
                        _sap = new NoOpSAP();
                    else
                        _sap = new SAPNco3Logic();
                }
                return _sap;
            }
        }

        private IWorkflow _k2 = null;
        public IWorkflow k2
        {
            get
            {
                if (_k2 == null)
                {
                    if (Common.AppSetting.NOOP_K2)
                        _k2 = new NoOpK2Helper();
                    else
                        _k2 = new K2Helper();
                }
                return _k2;
            }
        }

        private VendorLogic _vendor = null;
        public VendorLogic Vendor
        {
            get
            {
                if (_vendor == null)
                {
                    _vendor = new VendorLogic();
                }
                return _vendor;
            }
        }

        private AnnouncementLogic _announce = null;
        public AnnouncementLogic Announce
        {
            get
            {
                if (_announce == null)
                {
                    _announce = new AnnouncementLogic();
                }
                return _announce;
            }
        }

        private InvoiceListLogic _invoice = null;
        public InvoiceListLogic Invoice
        {
            get
            {
                if (_invoice == null)
                {
                    _invoice = new InvoiceListLogic();
                }
                return _invoice;
            }
        }

        private SettlementFormLogic _settle = null;
        public SettlementFormLogic Settle
        {
            get
            {
                if (_settle == null)
                {
                    _settle = new SettlementFormLogic();
                }
                return _settle;
            }
        }

        private RoleLogic _role = null;
        public RoleLogic Role
        {
            get
            {
                if (_role == null)
                {
                    _role = new RoleLogic();
                }
                return _role;
            }
        }

        private ComboBoxLogic _combo = null;
        public ComboBoxLogic Combo
        {
            get
            {
                if (_combo == null)
                {
                    _combo = new ComboBoxLogic();
                }
                return _combo;
            }
        }

        private LockingLogic _lock = null;
        public LockingLogic Lock
        {
            get
            {
                if (_lock == null)
                {
                    _lock = new LockingLogic();
                }
                return _lock;
            }
        }

        private Common.IniSQL _sql = null;
        public Common.IniSQL SQL
        {
            get
            {
                if (_sql == null)
                {
                    _sql = new Common.IniSQL();
                }
                return _sql;
            }
        }

        //fid.Hadid
        private AccrFormLogic _accrForm = null;
        public AccrFormLogic AccrForm
        {
            get
            {
                if (_accrForm == null)
                {
                    _accrForm = new AccrFormLogic();
                }
                return _accrForm;
            }
        }

        private AccrListLogic _accrList = null;
        public AccrListLogic AccrList
        {
            get
            {
                if (_accrList == null)
                {
                    _accrList = new AccrListLogic();
                }
                return _accrList;
            }
        }
        //end fid.Hadid

        private AccrShiftingListLogic _accrShiftingList = null;
        public AccrShiftingListLogic AccrShiftingList
        {
            get
            {
                if (_accrShiftingList == null)
                {
                    _accrShiftingList = new AccrShiftingListLogic();
                }
                return _accrShiftingList;
            }
        }

        //fid.Taufik
        private AccrWBSLogic _accrWBS = null;
        public AccrWBSLogic AccrWBS
        {
            get
            {
                if (_accrWBS == null)
                {
                    _accrWBS = new AccrWBSLogic();
                }
                return _accrWBS;
            }
        }
        //end fid.Taufik

        //fid.Taufik
        private AccrBalanceLogic _accrBalance = null;
        public AccrBalanceLogic AccrBalance
        {
            get
            {
                if (_accrBalance == null)
                {
                    _accrBalance = new AccrBalanceLogic();
                }
                return _accrBalance;
            }
        }
        //end fid.Taufik

        //fid.Taufik
        private AccrExtendLogic _accrExtend = null;
        public AccrExtendLogic AccrExtend
        {
            get
            {
                if (_accrExtend == null)
                {
                    _accrExtend = new AccrExtendLogic();
                }
                return _accrExtend;
            }
        }
        //end fid.Taufik

        //fid.sandi
        private AccrShiftingLogic _accrShifting = null;
        public AccrShiftingLogic AccrShifting
        {
            get
            {
                if (_accrShifting == null)
                {
                    _accrShifting = new AccrShiftingLogic();
                }
                return _accrShifting;
            }
        }
        //end fid.sandi

        private MassActLogic _mastAct = null;
        public MassActLogic MassAct
        {
            get
            {
                if (_mastAct == null)
                {
                    _mastAct = new MassActLogic();
                }
                return _mastAct;
            }
        }
        private IDbConnection _DB = null;
        public IDbConnection DB
        {
            get
            {
                if (_DB == null)
                {
                    _DB = Cx.NewDB();                    
                }
                return _DB;
            }
        }

       

        
    }
}
