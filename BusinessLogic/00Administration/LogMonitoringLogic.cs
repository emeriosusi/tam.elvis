using BusinessLogic;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace BusinessLogic.Admin
{
	public class LogMonitoringLogic
	{
		public LogMonitoringLogic()
		{
		}

		public List<string> getUserID()
		{
			List<string> _retVal = new List<string>()
			{
				""
			};
			try
			{
				using (ContextWrap co = new ContextWrap())
				{
					List<string> userID = (
						from p in co.db.TB_R_LOG_H
						select p.user_id).Distinct<string>().ToList<string>();
					if (userID != null && userID.Count > 0)
					{
						foreach (string data in userID)
						{
							_retVal.Add(data);
						}
					}
				}
			}
			catch (Exception)
			{
			}
			return _retVal;
		}

		public List<string> listRecordPerPage()
		{
			return new List<string>()
			{
				"5",
				"10",
				"20"
			};
		}

		public object searchLog(DateTime procStartDate, DateTime procEndDate, string status, string functionId, string proccessID, string userID)
		{
			DateTime dateTime = procEndDate;
			object retVal = null;
			dateTime = dateTime.Add(new TimeSpan(23, 59, 59));
			using (ContextWrap co = new ContextWrap())
			{
				List<TB_R_LOG_H> q = (
					from i in co.db.TB_R_LOG_H
					where (i.process_dt >= procStartDate) && (i.process_dt <= dateTime)
					select i).ToList<TB_R_LOG_H>();
				if (status != "-1")
				{
					q = (
						from i in q
						where i.process_sts == status
						select i).ToList<TB_R_LOG_H>();
				}
				if (functionId != "")
				{
					q = (
						from i in q
						where i.function_id == functionId
						select i).ToList<TB_R_LOG_H>();
				}
				if (proccessID != "")
				{
					try
					{
						q = (
							from i in q
							where i.process_id == Convert.ToInt32(proccessID)
							select i).ToList<TB_R_LOG_H>();
					}
					catch (Exception exception)
					{
					}
				}
				if (userID != "")
				{
					q = (
						from i in q
						where i.user_id == userID
						select i).ToList<TB_R_LOG_H>();
				}
				retVal = q;
			}
			return retVal;
		}
	}
}