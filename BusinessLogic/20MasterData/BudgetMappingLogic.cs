﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using DataLayer.Model;

namespace BusinessLogic._20MasterData
{
    public class BudgetMappingLogic : LogicBase
    {

        #region Search Inquiry
        public List<BudgetMappingData> SearchInqury(string _tt, string _wbs)
        {
            List<BudgetMappingData> result = new List<BudgetMappingData>();
            using (ContextWrap co = new ContextWrap())

                try
                {
                    var _db = co.db;
                    //default ignore all searching criteria

                    string sItemPre = "", sItemPost = "";
                    int iWbs = LikeLogic.ignoreWhat(_wbs, ref sItemPre, ref sItemPost);

                    var v = _db.vw_BudgetMapping.AsQueryable();

                    #region build searching criteria

                    int code = 0;

                    if (!String.IsNullOrEmpty(_tt) && Int32.TryParse(_tt, out code))
                    {
                        v = v.Where(x => x.TRANSACTION_CD.Equals(code));
                    }

                    if (!String.IsNullOrEmpty(_wbs))
                    {
                        v = v.Where(x => x.WBS_CODE.Equals(_wbs));
                    }
                    #endregion

                    var q = (from p in v select p).ToList();

                    if (q.Any())
                    {
                        foreach (var data in q)
                        {
                            BudgetMappingData _data = new BudgetMappingData();
                            _data.TRANSACTION_CD = data.TRANSACTION_CD;
                            _data.TRANSACTION_NAME = data.TRANSACTION_NAME;
                            _data.WBS_CODE = data.WBS_CODE;
                            _data.WBS_CODE_NAME = data.WBS_CODE_NAME;
                            _data.VALID_FROM = data.VALID_FROM;
                            _data.VALID_TO = data.VALID_TO;
                            _data.CREATED_BY = data.CREATED_BY;
                            _data.CREATED_DT = data.CREATED_DT;
                            _data.CHANGED_BY = data.CHANGED_BY;
                            _data.CHANGED_DT = data.CHANGED_DT;
                            result.Add(_data);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Handle(ex);
                }

            return result;
        }
        #endregion

        #region delete
        public bool Delete(List<BudgetMappingData> _list)
        {
            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var _db = co.db;
                    tx = _db.Connection.BeginTransaction();

                    foreach (BudgetMappingData x in _list)
                    {
                        var q = (from i in _db.TB_M_BUDGET_MAPPING
                                 where i.TRANSACTION_CD.Equals(x.TRANSACTION_CD)
                                 && i.WBS_CODE.Equals(x.WBS_CODE)
                                 select i);
                        TB_M_BUDGET_MAPPING g = q.FirstOrDefault();
                        if (g != null)
                        {
                            _db.DeleteObject(g);
                            _db.SaveChanges();
                        }
                    }
                    _result = true;
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    Handle(ex);
                }
            return _result;
        }
        #endregion

        #region Add
        public bool InsertRow(BudgetMappingData g, UserData userData)
        {
            bool result = false;
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var _db = co.db;
                    TB_M_BUDGET_MAPPING dx = new TB_M_BUDGET_MAPPING();

                    var qTRN = (from i in _db.TB_M_TRANSACTION_TYPE
                                where i.TRANSACTION_CD.Equals(g.TRANSACTION_CD)
                                select i.TRANSACTION_NAME).FirstOrDefault();

                    if (qTRN.Any())
                    {
                        g.TRANSACTION_NAME = qTRN.ToString();
                    }

                    dx.TRANSACTION_CD = g.TRANSACTION_CD;

                    dx.WBS_CODE = g.WBS_CODE;
                    dx.VALID_FROM = g.VALID_FROM;
                    dx.VALID_TO = g.VALID_TO;

                    dx.CREATED_BY = userData.USERNAME;
                    dx.CREATED_DT = DateTime.Now;

                    _db.AddToTB_M_BUDGET_MAPPING(dx);
                    _db.SaveChanges();

                    result = true;
                }
                catch (Exception ex)
                {
                    Handle(ex);
                }
            return result;
        }
        #endregion

        #region Edit
        public bool EditRow(BudgetMappingData g, UserData userData)
        {
            bool result = false;
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var _db = co.db;
                    var dx = (from i in _db.TB_M_BUDGET_MAPPING
                              where i.TRANSACTION_CD.Equals(g.TRANSACTION_CD)
                                 && i.WBS_CODE.Equals(g.WBS_CODE)
                              select i).FirstOrDefault();

                    if (dx != null)
                    {
                        dx.TRANSACTION_CD = g.TRANSACTION_CD;
                        dx.WBS_CODE = g.WBS_CODE;
                        dx.VALID_FROM = g.VALID_FROM;
                        dx.VALID_TO = g.VALID_TO;
                        dx.CHANGED_BY = userData.USERNAME;
                        dx.CHANGED_DT = DateTime.Now;

                        _db.SaveChanges();
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    Handle(ex);
                }
            return result;
        }
        #endregion

        #region Get String Builder

        public byte[] Get(string _username, string _imagePath, List<BudgetMappingData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("Budget_Mapping");
            x.PutHeader("Budget Mapping",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Transaction Name|WBS Code|WBS Name|Valid From|Valid To|" +
                "Created{By|Date}|Changed{By|Date}",
                _imagePath);
            int rownum = 0;
            foreach (BudgetMappingData dx in _list)
            {
                x.Put(++rownum,
                    dx.TRANSACTION_NAME,
                    dx.WBS_CODE,
                    dx.WBS_CODE_NAME,
                    Convert.ToDateTime(dx.VALID_FROM).ToString("dd MMM yyyy"),
                    Convert.ToDateTime(dx.VALID_TO).ToString("dd MMM yyyy"),
                    dx.CREATED_BY,
                    dx.CREATED_DT,
                    dx.CHANGED_BY,
                    dx.CHANGED_DT);
            }
            x.PutTail();
            return x.GetBytes();
        }
        #endregion


    }
}
