﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using DataLayer.Model;

namespace BusinessLogic._20MasterData
{
    public class GlAccountMappingLogic : LogicBase
    {
        #region Search Inquiry
        public List<GlAccountMappingData> SearchInqury(string _code, string _item, string _gl)
        {
            List<GlAccountMappingData> result = new List<GlAccountMappingData>();
            using (ContextWrap co = new ContextWrap())
            try
            {
                var _db = co.db;

                //default ignore all searching criteria
                bool ignoreTransactionCd = true;
                string sItemPre = "", sItemPost = "";
                int iItem = LikeLogic.ignoreWhat(_item, ref sItemPre, ref sItemPost);
                
                var v = _db.vw_GlAccountMapping.AsQueryable();
                
                #region build searching criteria

                int code = 0;
                int item = 0;
                int gl = 0;
                if (!String.IsNullOrEmpty(_code) && Convert.ToInt32(_code) != 0)
                {
                    ignoreTransactionCd = false;
                    code = Convert.ToInt16(_code);
                }

                
                #endregion
                
                
                if (!ignoreTransactionCd)
                {
                    v = v.Where(x => x.TRANSACTION_CD.Equals(code));
                }

                if (!String.IsNullOrEmpty(_item) && Convert.ToInt32(_item) != 0)
                {
                    item = Convert.ToInt32(_item);
                    v = v.Where(x => x.ITEM_TRANSACTION_CD.Equals(item));
                }

                if (_gl != null && !String.IsNullOrEmpty(_gl) && Int32.TryParse(_gl,out gl))
                {
                    v = v.Where(x => x.GL_ACCOUNT.Equals(gl));
                }
                var q = (from p in v select p).ToList();

                if (q.Any())
                {
                    foreach (var data in q)
                    {
                        GlAccountMappingData _data = new GlAccountMappingData();
                        _data.TRANSACTION_CD = data.TRANSACTION_CD;
                        _data.TRANSACTION_NAME = data.TRANSACTION_NAME;
                        _data.ITEM_TRANSACTION_CD = data.ITEM_TRANSACTION_CD;
                        _data.ITEM_TRANSACTION_DESC = data.ITEM_TRANSACTION_DESC;
                        _data.GL_ACCOUNT = data.GL_ACCOUNT;
                        _data.NEW_ITEM_TRANSACTION_CD = data.NEW_ITEM_TRANSACTION_CD;
                        _data.NEW_ITEM_TRANSACTION_DESC = data.NEW_ITEM_TRANSACTION_DESC;
                        _data.NEW_FORMULA = data.NEW_FORMULA;
                        _data.CREATED_BY = data.CREATED_BY;
                        _data.CREATED_DT = data.CREATED_DT;
                        _data.CHANGED_BY = data.CHANGED_BY;
                        _data.CHANGED_DT = data.CHANGED_DT;
                        _data.SUM_UP_FLAG = data.SUM_UP_FLAG;
                        _data.SUM_UP_FLAG_NAME = data.SUM_UP_FLAG_NAME;
                        result.Add(_data);
                    }
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }

            return result;
        }   


        #endregion

        #region delete transaction type
        public bool Delete(List<GlAccountMappingData> _list)
        {
            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap co = new ContextWrap()) 
            try
            {
                var _db = co.db;
                tx = _db.Connection.BeginTransaction();

                foreach (GlAccountMappingData x in _list)
                {
                    var q = (from i in _db.TB_M_GL_ACCOUNT_MAPPING
                                where i.TRANSACTION_CD.Equals(x.TRANSACTION_CD)
                                && i.ITEM_TRANSACTION_CD.Equals(x.ITEM_TRANSACTION_CD)
                                select i);
                    TB_M_GL_ACCOUNT_MAPPING g = q.FirstOrDefault();
                    if (g != null) {  
                        _db.DeleteObject(g);
                        _db.SaveChanges();
                    }
                }
                _result = true;
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                Handle(ex);
            }            
            return _result;
        }
        #endregion

        #region Add 
        public bool InsertRow(GlAccountMappingData g, UserData userData)
        {
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            try
            {
                var _db = co.db;
                TB_M_GL_ACCOUNT_MAPPING dx = new TB_M_GL_ACCOUNT_MAPPING();

                var qTRN = (from i in _db.TB_M_TRANSACTION_TYPE
                            where i.TRANSACTION_CD.Equals(g.TRANSACTION_CD)
                            select i.TRANSACTION_NAME).FirstOrDefault();
                
                if (qTRN.Any())
                {
                    g.TRANSACTION_NAME = qTRN.ToString();                    
                }

                var qIT = _db.TB_M_ITEM_TRANSACTION
                            .Where(x => x.ITEM_TRANSACTION_CD.Equals(g.ITEM_TRANSACTION_CD))
                            .Select(i => i.ITEM_TRANSACTION_DESC).FirstOrDefault();
                if (qIT.Any())
                {
                    g.ITEM_TRANSACTION_DESC = qIT.ToString();                    
                }

                dx.TRANSACTION_CD = g.TRANSACTION_CD;
                dx.ITEM_TRANSACTION_CD = g.ITEM_TRANSACTION_CD;
                dx.GL_ACCOUNT = g.GL_ACCOUNT;
                dx.NEW_ITEM_TRANSACTION_CD = g.NEW_ITEM_TRANSACTION_CD;
                dx.NEW_FORMULA = g.NEW_FORMULA;
                dx.SUM_UP_FLAG = g.SUM_UP_FLAG;
                
                dx.CREATED_BY = userData.USERNAME;
                dx.CREATED_DT = DateTime.Now;

                _db.AddToTB_M_GL_ACCOUNT_MAPPING(dx);
                _db.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                Handle(ex);                
            }
            return result;
        }
        #endregion

        #region Edit 
        public bool EditRow(GlAccountMappingData g, UserData userData)
        {
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            try
            {
                var _db = co.db;
                var dx = (from i in _db.TB_M_GL_ACCOUNT_MAPPING 
                              where i.TRANSACTION_CD.Equals(g.TRANSACTION_CD)
                               && i.ITEM_TRANSACTION_CD.Equals(g.ITEM_TRANSACTION_CD)
                               select i).FirstOrDefault();

                if (dx != null)
                {
                    dx.GL_ACCOUNT = g.GL_ACCOUNT ;
                    dx.NEW_ITEM_TRANSACTION_CD = g.NEW_ITEM_TRANSACTION_CD;
                    dx.NEW_FORMULA = g.NEW_FORMULA;
                    dx.SUM_UP_FLAG = g.SUM_UP_FLAG;
                    dx.CHANGED_BY = userData.USERNAME;
                    dx.CHANGED_DT = DateTime.Now;

                    _db.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return result;
        }
        #endregion

        #region Get String Builder

        public byte[] Get(string _username, string _imagePath, List<GlAccountMappingData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("GL_Account_Mapping");
            x.PutHeader("GL Account Mapping",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Transaction Name|Item Transaction|GL Account|New Item Transaction|New Formula|Sum Up Flag|"+
                "Created{By|Date}|Changed{By|Date}",
                _imagePath);
            int rownum = 0;
            foreach (GlAccountMappingData dx in _list)
            {
                x.Put(++rownum,
                    dx.TRANSACTION_NAME,
                    dx.ITEM_TRANSACTION_DESC,
                    dx.GL_ACCOUNT,
                    dx.NEW_ITEM_TRANSACTION_DESC,
                    dx.NEW_FORMULA,
                    dx.SUM_UP_FLAG,
                    dx.CREATED_BY,
                    dx.CREATED_DT,
                    dx.CHANGED_BY,
                    dx.CHANGED_DT);
            }
            x.PutTail();
            return x.GetBytes();
        }        
        #endregion


    }
}
