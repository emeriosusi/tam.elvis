﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data._20MasterData;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data;
using DataLayer.Model;
using Common.Function;
using Common.Data;
using System.IO;
using BusinessLogic.CommonLogic;
using System.Diagnostics;
using System.Data.Objects.SqlClient;

namespace BusinessLogic._20MasterData
{
    public class TransactionTypeLogic : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();
        #region Search Inquiry
        public List<TransactionTypeData> Search(
            string _transactionCd, 
            string _stdWording, 
            string transactionLevel,
            string groupCd)
        {
            List<TransactionTypeData> result = new List<TransactionTypeData>();
            using (ContextWrap co =  new ContextWrap())
            try
            {
                var _db = co.db;
                //default ignore all searching criteria
                bool ignoreTransactionCd = true;
                bool ignoreStdWording = true;

                #region build searching criteria
                int TrCode = 0;
                if (!string.IsNullOrEmpty(_transactionCd) && Convert.ToInt32(_transactionCd) != 0)
                {
                    ignoreTransactionCd = false;
                    TrCode = Convert.ToInt32(_transactionCd);
                }

                if (!string.IsNullOrEmpty(_stdWording) && _stdWording != "")
                {
                    ignoreStdWording = false;
                }
                #endregion
                bool ignoreTransactionLevel = transactionLevel.isEmpty();
                bool ignoreGroup = groupCd.isEmpty();

                var q = (from p in _db.TB_M_TRANSACTION_TYPE select p);
                if (!ignoreTransactionCd)
                        q= q.Where(p=>p.TRANSACTION_CD == TrCode);
                if (!ignoreStdWording)
                    q = q.Where(p=>p.STD_WORDING.Contains(_stdWording.Trim()));
                int tLevel = transactionLevel.Int(0);
                if (!ignoreTransactionLevel) 
                    q = q.Where(p=> p.TRANSACTION_LEVEL_CD == tLevel);
                int gCd = groupCd.Int(0);
                if (!ignoreGroup)
                    q = q.Where(p => p.GROUP_CD == gCd);
                
                if (q.Any())
                {
                    result = q.OrderBy(a=>a.TRANSACTION_CD)
                        .ThenByDescending(a=>a.CREATED_DT)
                        .Select(p => new TransactionTypeData()
                         {
                             TRANSACTION_CD = p.TRANSACTION_CD,
                             TRANSACTION_NAME = p.TRANSACTION_NAME,
                             TRANSACTION_LEVEL_CD = p.TRANSACTION_LEVEL_CD,
                             STD_WORDING = p.STD_WORDING,
                             GROUP_CD = p.GROUP_CD,
                             CLOSE_FLAG = p.CLOSE_FLAG,
                             SERVICE_FLAG = p.SERVICE_FLAG,
                             BUDGET_FLAG = p.BUDGET_FLAG,
                             ATTACHMENT_FLAG = p.ATTACHMENT_FLAG,
                             PRODUCTION_FLAG = p.PRODUCTION_FLAG,
                             COST_CENTER_FLAG = p.COST_CENTER_FLAG,
                             TEMPLATE_CD = p.TEMPLATE_CD,
                             CREATED_BY = p.CREATED_BY,
                             CREATED_DT = p.CREATED_DT,
                             CHANGED_BY = p.CHANGED_BY,
                             CHANGED_DT = p.CHANGED_DT
                         }).ToList();
                }
            }
            catch (Exception ex)
            {
                Handle(ex);                
            }

            
            return result;
        }


        #endregion

        #region delete transaction type
        public bool Delete(List<string> listOfTransactionCode)
        {
            bool _result = false;

            DbTransaction T1 = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var DB = co.db;
                    T1 = DB.Connection.BeginTransaction();

                    foreach (string TransactionCode in listOfTransactionCode)
                    {
                        int transCd = Convert.ToInt32(TransactionCode);
                        var q = (from i in DB.TB_M_TRANSACTION_TYPE 
                                 where i.TRANSACTION_CD == transCd select i)
                                 .FirstOrDefault();
                        DB.DeleteObject(q);
                        DB.SaveChanges();
                    }
                    _result = true;
                    T1.Commit();
                }
                catch (Exception ex)
                {
                    T1.Rollback();
                    Handle(ex);
                }
            }
            return _result;
        }
        #endregion

        #region Add Transaction Type
        public bool InsertTransactionType(TransactionTypeData t, UserData userData)
        {
            bool result = false;
            try
            {
                using (ContextWrap co = new ContextWrap())
                {
                    var db = co.db;
                    TB_M_TRANSACTION_TYPE _header = new TB_M_TRANSACTION_TYPE()
                    {
                        TRANSACTION_CD = t.TRANSACTION_CD,

                        TRANSACTION_NAME = t.TRANSACTION_NAME,
                        STD_WORDING = t.STD_WORDING,
                        TRANSACTION_LEVEL_CD = t.TRANSACTION_LEVEL_CD,
                        CLOSE_FLAG = t.CLOSE_FLAG,
                        SERVICE_FLAG = t.SERVICE_FLAG,
                        BUDGET_FLAG = t.BUDGET_FLAG,
                        ATTACHMENT_FLAG = t.ATTACHMENT_FLAG,
                        PRODUCTION_FLAG = t.PRODUCTION_FLAG,
                        COST_CENTER_FLAG = t.COST_CENTER_FLAG,
                        TEMPLATE_CD = t.TEMPLATE_CD,
                        GROUP_CD = t.GROUP_CD,
                        CREATED_BY = userData.USERNAME,
                        CREATED_DT = DateTime.Now,
                    };
                    db.AddToTB_M_TRANSACTION_TYPE(_header);
                    db.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return result;
        }

       
        #endregion

        #region Edit Transaction Type
        public bool EditTransactionType(TransactionTypeData t, UserData userData)
        {
            bool result = false;
            try
            {
                using (ContextWrap co = new ContextWrap())
                {
                    var db = co.db;
                    int trCode = t.TRANSACTION_CD;
                    var _header = (from i in db.TB_M_TRANSACTION_TYPE 
                                   where i.TRANSACTION_CD == trCode select i)
                                   .FirstOrDefault();

                    if (_header != null)
                    {
                        _header.TRANSACTION_NAME = t.TRANSACTION_NAME;
                        _header.STD_WORDING = t.STD_WORDING;
                        _header.TRANSACTION_LEVEL_CD = t.TRANSACTION_LEVEL_CD;
                        _header.CLOSE_FLAG = t.CLOSE_FLAG;
                        _header.SERVICE_FLAG = t.SERVICE_FLAG;
                        _header.BUDGET_FLAG = t.BUDGET_FLAG;
                        _header.ATTACHMENT_FLAG = t.ATTACHMENT_FLAG;
                        _header.PRODUCTION_FLAG = t.PRODUCTION_FLAG;
                        _header.COST_CENTER_FLAG = t.COST_CENTER_FLAG;
                        _header.TEMPLATE_CD = t.TEMPLATE_CD;
                        _header.GROUP_CD = t.GROUP_CD;
                        _header.CHANGED_BY = userData.USERNAME;
                        _header.CHANGED_DT = DateTime.Now;
                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return result;
        }
        #endregion

        #region Get String Builder

        public byte[] Get(string _username, string _imagePath, List<TransactionTypeData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("Transaction_Type");
            x.PutHeader("Transaction Type",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") 
                + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Transaction Code|Transaction Name|Std Wording|Transaction Level|Group|Template|"
                + "Close Flag|Service Flag|Budget Flag|Attachment|Production|Cost Center|"
                + "Created{Date|By}|Changed{Date|By}",
                _imagePath);
            int rownum = 0;
            StringMap dProd = logic.Sys.GetDict("PRODUCTION_TRANSACTION");

            foreach (TransactionTypeData dx in _list)
            {
                x.Put(++rownum,
                    dx.TRANSACTION_CD,
                    dx.TRANSACTION_NAME,
                    dx.STD_WORDING,
                    TransactionLevelName(dx.TRANSACTION_LEVEL_CD.str()),
                    GroupName(dx.GROUP_CD.str()),
                    TemplateName(dx.TEMPLATE_CD.str()),                    
                    fValue(dx.CLOSE_FLAG, "closed", "not closed"),
                    fValue(dx.SERVICE_FLAG, "service", "non service"),
                    fValue(dx.BUDGET_FLAG, "budgeted", "non budgeted"),
                    fValue(dx.ATTACHMENT_FLAG, "yes", "no"),
                    dProd.Get(Convert.ToString(dx.PRODUCTION_FLAG??0), ""),
                    fValue(dx.COST_CENTER_FLAG, "yes", "no"),
                    dx.CREATED_DT.fmt(StrTo.TO_MINUTE),
                    dx.CREATED_BY,
                    dx.CHANGED_DT.fmt(StrTo.TO_MINUTE),
                    dx.CHANGED_BY
                    );
            }
            x.PutTail();
            return x.GetBytes();
        }

        #endregion

        private string fValue(int? v, string v1, string v0, string vNull = "")
        {
            if (v == null)
                return vNull;
            else if (v != 0)
                return v1;
            else
                return v0;
        }

        public List<GroupCode> GetGroupCodes()
        {
            List<GroupCode> r = new List<GroupCode>();
            StringMap m = logic.Sys.GetDict("GROUP_CODE_TRANSACTION");
            if ((m != null) && (m.d != null) && (m.d.Count > 0))
            {
                r = m.d.Select(a =>     
                    new GroupCode() 
                        { 
                            GROUP_CD = a.Key.Int(), 
                            GROUP_NAME = a.Value 
                        }
                    ).ToList(); 
            }
            return r;
        }

        public List<TemplateData> GetTemplateCodes()
        {
            List<TemplateData> r = new List<TemplateData>();
            StringMap m = logic.Sys.GetDict("TEMPLATE_CD");
            if ((m != null) && (m.d != null) && (m.d.Count > 0))
            {
                r = m.d.Select(a =>
                        new TemplateData()
                        {
                            TemplateCode = a.Key.Int(),
                            TemplateName = a.Value
                        }
                    ).ToList();
            }
            return r;
        }


        public string TemplateName(string code)
        {
            return logic.Sys.GetText("TEMPLATE_CD", code);
        }

        public string GroupName(string code)
        {
            return logic.Sys.GetText("GROUP_CODE_TRANSACTION", code);
        }

        public string TransactionLevelName(string code)
        {
            string r = null;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from l in db.vw_TransactionLevel
                         where l.DIVISION_ID == code
                         select l);
                if (q.Any())
                {
                    r = q.FirstOrDefault().DIVISION_NAME;
                }
            }
            return r;
        }
        
    }
}
