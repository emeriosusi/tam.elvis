﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic;

using DataLayer;
using DataLayer.Model;
using Common;
using Common.Data;
using Common.Function;

namespace K2.Helper
{
    public class NoOpK2Helper : IWorkflow
    {
        public void Open()
        {
            
        }

        public void Close()
        {
            
        }

        public void Impersonate(string User)
        {
            
        }

        public void Revert()
        {
            
        }

        public int GetProcID(string SN)
        {
            return 0;
        }

        public Dictionary<string, string> GetDatafield(List<string> Keys, int procid)
        {
            return null;
        }

        public void EditDatafield(Dictionary<string, string> datafield, int procid)
        {
            
        }

        public void EditDatafield(Dictionary<string, object> datafield, int procid)
        {
            
        }

        public void deleteprocess(string folio)
        {
            
        }

        public void deleteprocess(List<string> folioLIst)
        {
            
        }

        public void deleteprocess(int ProcID)
        {
            
        }

        public string StartProccessWithID(string Process, string folio, Dictionary<string, string> datafield)
        {
            // fid.Hadid
            // K2 still not running yet in FID environment
            // so simulate this K2 process for the purpose of BCT
            // TODO : Delete this process if K2 is ready to use
            GlobalResourceData resx = new GlobalResourceData();
            if (Process.Equals(resx.K2ProcID("Form_Registration_Submit_New")))
            {
                LogicBase l = new LogicBase();

                Dapper.DynamicParameters d = new Dapper.DynamicParameters();
                d.Add("@REFF_NO", datafield["Reff_No"]);
                d.Add("@RegisterDT", datafield["RegisterDt"]);
                d.Add("@UserName", datafield["CurrentPIC"]);
                d.Add("@ModuleCd", datafield["ModuleCD"]);
                d.Add("@DivCd", datafield["CurrentDivCD"]);
                d.Add("@VendorCd", datafield["VendorCD"]);
                d.Add("@RejectByFinance", datafield["RejectedFinance"]);
                d.Add("@Entertainment", datafield["Entertain"]);
                d.Add("@TotalAmount", datafield["TotalAmount"]);

                l.Exec("sp_InsertDistributionStatus", d);
            }

            return null;
        }

        public string StartProccessWithID(string Process, string folio, Dictionary<string, object> datafield)
        {
            return null;
        }

        public Dictionary<string, string> GetManagedWorklist(string user)
        {
            return new Dictionary<string,string>();
        }

        public List<WorklistHelper> GetWorklistComplete(string user)
        {
            return GetAllWorklistComplete(user);            
        }

        public List<WorklistHelper> GetWorklistFiltered(string user, string key, string value)
        {
            return GetAllWorklistComplete(user);
        }


        public List<WorklistHelper> GetWorklistBy(string aCriteria)
        {
            return GetAllWorklistComplete("");
        }
        

        public List<WorklistHelper> GetAllWorklistComplete(string user)
        {
            List<WorklistHelper> l = new List<WorklistHelper>();
            int seq= 0;
            LogicFactory lo = LogicFactory.Get();

            GlobalResourceData g = new GlobalResourceData();
            string fname = g.K2ProcID("Form_Registration_Submit_New").Split('\\')[0];
            using (ContextWrap co = new ContextWrap())
            {
                var p = co.db.TB_R_PV_H
                    .Where(a => a.NEXT_APPROVER == user 
                             && a.STATUS_CD != 0
                             && a.STATUS_CD != 99                              
                             && a.STATUS_CD != 29).ToList();
                
                
                if (p != null && p.Count > 0)
                {
                    foreach (var d in p)
                    {
                        l.Add(new WorklistHelper() {
                            Folio = d.PV_NO.str() + d.PV_YEAR.str(),
                            SN = (seq++).str(),
                            FullName= fname,
                            StartDate = DateTime.Now
                        });
                    }
                }

                var r = co.db.TB_R_RV_H
                    .Where(a => a.NEXT_APPROVER == user 
                             && a.STATUS_CD != 99 
                             && a.STATUS_CD != 68 
                             && a.STATUS_CD != 0).ToList();
                if (r != null && r.Count > 0)
                {
                    foreach (var d in r)
                    {
                        l.Add(new WorklistHelper()
                        {
                            Folio = d.RV_NO.str() + d.RV_YEAR.str(),
                            SN = (seq++).str(),
                            FullName = fname,
                            StartDate = DateTime.Now
                        });
                    }
                }
            }
            return l;
        }

        public Dictionary<string, string> GetWorklist(string user)
        {
            Dictionary<string, string> dx = new Dictionary<string, string>();
            List<WorklistHelper> l = GetAllWorklistComplete(user);
            foreach (WorklistHelper w in l)
            {
                if (!dx.ContainsKey(w.Folio))
                {
                    dx.Add(w.Folio, w.SN);
                }
            }
            return dx;
        }

        public Dictionary<string, string> GetWorklistByCriteria(string user, string property, string propertyvalue)
        {
            return new Dictionary<string, string>();
        }


        public List<WorklistHelper> GetWorklistCompleteByProcess(string user, string processFullName)
        {
            return new List<WorklistHelper>();
        }

        public void Action(string action, string sn)
        { 
        }

        // fid.Hadid
        // K2 still not running yet in FID environment
        // so simulate this K2 process for the purpose of BCT
        // TODO : Delete this function if K2 is ready to use
        public void Action(string action, Dictionary<string, string> datafield)
        {
            GlobalResourceData resx = new GlobalResourceData();
            LogicBase l = new LogicBase();
            Dapper.DynamicParameters d;

            if (action.Equals(resx.K2ProcID("Form_Registration_Approval")))
            {
                string reffNo = "";
                if (datafield.ContainsKey("REFF_NO")) reffNo = datafield["REFF_NO"];
                else if (datafield.ContainsKey("Reff_No")) reffNo = datafield["Reff_No"];

                d = new Dapper.DynamicParameters();
                d.Add("@REFF_NO", reffNo);
                d.Add("@CurrentPic", datafield["CurrentPIC"]);

                var sts = l.Qx<StatusApprovedRejected>("GetStatusCd", d).FirstOrDefault();

                int moduleCd = datafield["REFF_NO"].StartsWith("5") ? 1 : 2;

                d = new Dapper.DynamicParameters();
                d.Add("@REFF_NO", reffNo);
                d.Add("@STATUS_CD", sts.APPROVE_STATUS_CD);
                d.Add("@MODULE_CD", moduleCd);
                d.Add("@PROCEED_BY", datafield["CurrentPIC"]);

                l.Exec("sp_DoApproval", d);
            }
            else if (action.Equals(resx.K2ProcID("Form_Registration_Rejected")))
            {
                string reffNo = "";
                if (datafield.ContainsKey("REFF_NO")) reffNo = datafield["REFF_NO"];
                else if (datafield.ContainsKey("Reff_No")) reffNo = datafield["Reff_No"];

                d = new Dapper.DynamicParameters();
                d.Add("@REFF_NO", reffNo);
                d.Add("@CurrentPic", datafield["CurrentPIC"]);

                var sts = l.Qx<StatusApprovedRejected>("GetStatusCd", d).FirstOrDefault();

                int moduleCd = datafield["REFF_NO"].StartsWith("5") ? 1 : 2;

                d = new Dapper.DynamicParameters();
                d.Add("@REFF_NO", reffNo);
                d.Add("@STATUS_CD", sts.REJECT_STATUS_CD);
                d.Add("@MODULE_CD", moduleCd);
                d.Add("@PROCEED_BY", datafield["CurrentPIC"]);

                l.Exec("sp_DoReject", d);
            }
        }

        public void Action(string action, string sn, string manageduser, string shareduser)
        {
            
        }

        public List<string> GetAction(string sn)
        {
            return new List<string>();
        }

        public List<string> GetAction(string sn, string UserID)
        {
            return new List<string>();
        }

        public void RedirectItem(string User, string sn)
        {
            
        }

        public void SleepItem(bool sleep, string sn, int time)
        {
            
        }

        public void FinishItem(string sn)
        {
            
        }

        public void SetUserstatus(string status)
        {
            
        }

        public string GetUserstatus(string User)
        {
            return null;
        }

        public List<string> Redirect(List<string> works, string fromUser, string toUser)
        {
            return new List<string>();
        }

        public void ShareWorklist(DateTime Start, string DestinationUser, int userStatus)
        {
            
        }

    }

    public class StatusApprovedRejected
    {
        public int APPROVE_STATUS_CD { get; set; }
        public int REJECT_STATUS_CD { get; set; }
    }
}
