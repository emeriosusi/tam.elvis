﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Model;
using Common.Data;
using System.Diagnostics;
using BusinessLogic._00Administration;
using Common.Function;
using Dapper; 

namespace BusinessLogic.CommonLogic
{
    public class AnnouncementLogic
    {
        public List<AnnouncementData> SearchInquiryHome(UserData userData)
        {
            string positions = string.Join(";", userData.Positions.Where(p => !string.IsNullOrEmpty(p.POSITION_ID)).Select(pi => pi.POSITION_ID).Distinct().ToList());
            string divisions = string.Join(";", userData.Divisions.Distinct());
            return LogicFactory.Get().Base.hQx<AnnouncementData>("GetAnnouncement", new { positions = positions, divisions = divisions }).ToList();
        }
        
    }
}
