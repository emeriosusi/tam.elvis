﻿using Common;
using Common.Data;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using System.Web.UI;
using System.Text;

namespace BusinessLogic.CommonLogic
{
    public class MessageLogic
    {
        private LogicFactory logic = LogicFactory.Get();

        public static string Format(string s, MessageValueData m)
        {
            object[] x = new object[6];
            int j;
            if (m.VALUE1 != null) { j = 0; x[0] = m.VALUE1; } ;
            if (m.VALUE2 != null) { j = 1; x[1] = m.VALUE2; } ;
            if (m.VALUE3 != null) { j = 2; x[2] = m.VALUE3; } ;
            if (m.VALUE4 != null) { j = 3; x[3] = m.VALUE4; } ;
            if (m.VALUE5 != null) { j = 4; x[4] = m.VALUE5; } ;
            if (m.VALUE6 != null) { j = 5; x[5] = m.VALUE6; } ;
            return string.Format(s, x);
        }
        private GlobalResourceData _g = new GlobalResourceData();

        public string Message(string _id, params object[] x)
        {
            string m = _g.GetResxObject(_id);
            if (!string.IsNullOrEmpty(m))
                if (x != null && x.Length > 0)
                    return string.Format(m, x);
                else
                    return m;
            else
            {
                StringBuilder b = new StringBuilder("");
                b.Append(_id);
                b.Append(":");
                if (x != null)
                {
                    b.Append(x.str());
                    b.Append(" ");
                }
                return b.ToString();
            }
        }

        public string WriteMessage(int _pid, string _id, string _location, string _uid, params object[] x)
        {
            string logs = Message(_id, x);
            LoggingLogic.say(_uid, logs);
            logic.Log.Log(_id, logs, _location, _uid, "", _pid);

            return logs;
        }

        private GlobalResourceData _globalResource = new GlobalResourceData();
        public void WriteErrMsg(int _processId, string _strClassGlobalResource, string _msgId, string _msgType, string _errLocation, MessageValueData _msgValue)
        {
            string _errMsg = null;

            _errMsg = _globalResource.GetResxObject(_strClassGlobalResource, _msgId);


            if (_msgValue != null)
            {
                _errMsg = Format(_errMsg, _msgValue);
            }

            logic.Log.Log(_msgId, _errMsg, _errLocation, "", "", _processId);
            
        }

        public string ErrorMessageString(int _processId, string _strClassGlobalResource, string _msgId, string _msgType, string _errLocation, MessageValueData _msgValue)
        {
            string _errMsg = null;
            
            _errMsg = _globalResource.GetResxObject(_strClassGlobalResource, _msgId);

            if (_msgValue != null)
            {
                _errMsg = Format(_errMsg, _msgValue);
            
            }
            return _errMsg;
        }


        public string SetMessage(string Message, string MessageID)
        {
            string _message = "";
            if (Message != "")
            {
                string MessageType = (!string.IsNullOrEmpty(MessageID) && (MessageID.Length>2)) 
                    ? MessageID.Substring(MessageID.Length - 3, 3).ToUpper()
                    : "ERR";
                _message = string.Format("<div class='{0}'><div class='status{0}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><p class='statusMessage'>"
                        + "{1}</p></div>", MessageType, Message);
            } return _message;
        }

        public static string Wrap(string Message, string MessageID)
        {
            if (string.IsNullOrEmpty(Message)) return "";
            bool hasID = !string.IsNullOrEmpty(MessageID) && (MessageID.Length > 2);
            const string ids = " ERR WRN INF ";
            string id = "ERR";
            if (hasID)
                id = MessageID.Substring(MessageID.Length - 3, 3).ToUpper();
            else {
                if ((Message.IndexOf(' ')<0) && (Message.Length > 3) && (Message.Length < 15))
                {
                    string mId = Message.Substring(Message.Length - 3, 3).ToUpper();
                    if (ids.Contains(mId)) id = mId;
                }
            }

            if (!hasID || ids.Contains(id))
            {
                return
                    string.Format(
                       @"<div class=""{0}""><div class=""status{0}"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>&nbsp;&nbsp;"
                       +@"<span class=""statusMessage"">{1}</span>"
                       +"</div>", id, Message);             
            }
            else
                return Message;
        }

    }
}
