﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.EntityClient;
using Common;
using Common.Data;
using Common.Function;
using DataLayer.Model;
using Dapper;
using Common.Data._80Accrued; 

namespace BusinessLogic.CommonLogic
{
    public class LookUpLogic : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();

        public List<VendorCdData> GetVendorCodeData(string code, string desc)
        {
        
            return Qx<VendorCdData>("GetVendorCodeData",
                new { code = code.Replace("*", "%"), desc = "%" + desc + "%" }
                ).ToList();
        }

        public List<OtherAmountData> GetAmounts(int _no, int _yy)
        {
            List<OtherAmountData> l = new List<OtherAmountData>();
            l = Qx<OtherAmountData>("GetAmounts", new { aNo = _no, aYY = _yy }).ToList();

            if (l != null && l.Count > 0)
            {
                foreach (OtherAmountData m in l)
                {
                    m.CURRENCY_AMOUNT = (m.TOTAL_AMOUNT ?? 0).fmt(0);
                }
            }
            return l;

            
        }

        public List<OtherAmountData> GetAmounts(string no, string yy)
        {
            return GetAmounts(no.Int(), yy.Int());
        }

        public decimal GetOutstandingAmountBudget(string budgetNo)
        {
            return Qx<decimal>("GetAmountsOutstanding", new { bNo = budgetNo }).FirstOrDefault();
        }
       

        public List<OtherAmountData> GetOtherInvoiceAmountData(string invoiceNo, string vendorCd, string invoiceDate)
        {
            //List<OtherAmountData> _List = new List<OtherAmountData>();
            
            if (!invoiceDate.isEmpty())
            {
                DateTime d = invoiceDate.Date("dd MMM yyyy");
                if (d.Year == StrTo.NULL_YEAR)
                    invoiceDate = null;
                else
                    invoiceDate = d.fmt("yyyyMMdd");
            }
            return Qx<OtherAmountData>("GetOtherInvoiceAmountData").ToList();
          
        }

    

        public List<AttachmentData> GetAttachmentData(String pvNoParam, String pvYearParam)
        {
            string refNo = String.Concat(pvNoParam, pvYearParam);
            List<AttachmentData> returnData = new List<AttachmentData>();
            returnData = Qx<AttachmentData>("GetAttachmentData", new { refNo = refNo }).ToList();

            foreach (var r in returnData)
            {
                r.URL = Common.Function.CommonFunction.CombineUrl(r.DIRECTORY, r.FILE_NAME);
            }

            return returnData;
        }

        public List<SapDocNoData> GetSapDocNo(String pvNo, String pvYear)
        {
            List<SapDocNoData> returnData = new List<SapDocNoData>();
            int doc_no = 0;
            bool ignoreDocNo = true;
            if (!String.IsNullOrWhiteSpace(pvNo))
            {
                ignoreDocNo = false;
                doc_no = Convert.ToInt32(pvNo);
            }
            int doc_year = 0;
            bool ignoreDocYear = true;
            if (!String.IsNullOrWhiteSpace(pvYear))
            {
                ignoreDocYear = false;
                doc_year = Convert.ToInt32(pvYear);
            }
            return Qx<SapDocNoData>("GetSapDocNo", new { no = doc_no, yy = @doc_year }).ToList();
                
        }

        public List<SapDocNoData> GetSapDocNoByBookNo(String bookNo, String accrNo)
        {
            return Qx<SapDocNoData>("GetSapDocNoByBookNo", new { bookNo = @bookNo, accrNo = @accrNo }).ToList();
        }

        public List<ExchangeRate> getExchangeRates()
        {
            return Qu<ExchangeRate>("getExchangeRates").ToList();
        }
        public Dictionary<int, string> GetDocType(int DocType = 1)
        {
            Dictionary<int, string> r = new Dictionary<int, string>();
            r = Qu<DocTypeData>((DocType == 1) ? "GetDocTypePV" : "GetDocTypeRV").ToDictionary(a => a.DocType, a => a.DocName);   
            return r;
        }


        public bool DistributedToMe(string docNo, string docYear, string uid = "")
        {
            decimal reff_no = (docNo + docYear).Dec(0);
            
            int d = Qx<int>("GetDistributedToMe", new { REFF_NO = reff_no, uid = uid }).FirstOrDefault();
            return (d > 0); 
        }

        public int GetApproveStatus(int status)
        {
            int? r = null;
            r = Qu<int?>("GetApproveStatus", status).FirstOrDefault(); 
            return r ?? 0;
        }


        public int GetStatusPair(int status)
        {
            int? r = null;
            r = Qu<int?>("GetStatusPair", status).FirstOrDefault();
            return r ?? 0;
        }

        public List<ExchangeRate> GetCashierPolicyExchangeRates(int DocType = 1)
        {
            return Qx<ExchangeRate>("GetCashierPolicyExchangeRates", new { docType = DocType }).ToList();
        }

        public List<ExchangeRate> GetExchangeRatesFromOriginal(int _no, int _yy)
        {
            List<ExchangeRate> le = new List<ExchangeRate>();
            
            bool isPV = _no < 600000000;

            return Qx<ExchangeRate>("GetExchangeRatesFromOriginal", new { no = _no, yy = _yy, isPV = isPV}).ToList();
        }

        public int GetRoleClassOf(UserData userData)
        {
            int roleClass = 0;
            if (userData != null)
            {
                string UserLinkToForm = logic.Sys.GetText("PvListLink", "Form");
                string uRole = userData.Roles[0];
                if (("," + UserLinkToForm + ",").Contains("," + uRole + ","))
                    roleClass = 1;

                string UserLinkToApproval = logic.Sys.GetText("PvListLink", "Approval");
                if (("," + UserLinkToApproval + ",").Contains("," + uRole + ","))
                    roleClass = 0;
            }
            return roleClass;
        }

        public string GetLastUser(string docNo, string docYear, string userName, string mode)
        {
            string retVal = "";
            try
            {
                decimal reffNo = Decimal.Parse(String.Concat(docNo, docYear));

                return Qx<string>("GetLastUser", new { reffNo = reffNo, userName = userName, mode = mode }).FirstOrDefault();

                
            }
            catch (Exception ex)
            {
                LoggingLogic.say("", "GetLastUser({0}, {1}, {2}, {3}): {4}", docNo, docYear, userName, mode, ex.Message);
            }

            return retVal;
        }

        public string RemainingBudget(string no, string year, string wbs, ref string err)
        {
            //call SAP BAPI here

            string r = "";
            if (Common.AppSetting.BUDGET_CHECK_GET && !Common.AppSetting.SKIP_BUDGET_CHECK)
            {
                int _no = no.Int();
                int _yy = year.Int();

                string rb = logic.SAP.GetRemainingBudget(_no, _yy, wbs);
                err = logic.SAP.LastMessageText;
                r = rb;
            }
            else
            {
                r = logic.SAP.getRemainingBudget(wbs, year);
            }

            return r;
        }

        public string getBookingNoBySuspense(int suNO, int suYY)
        {
            string bookingNo = null;
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = (from p in _db.TB_R_ACCR_BALANCE
                         where p.SUSPENSE_NO_NEW == suNO
                         select p);
                if (q.Any())
                {
                    var d = q.FirstOrDefault();
                    bookingNo = d.BOOKING_NO;
                }
            }

            return bookingNo;
        }

        public AccruedBalanceData getAccrBalance(Dictionary<String, Object> param)
        {
            AccruedBalanceData balance = null;
            try
            {
                using (ContextWrap co = new ContextWrap())
                {
                    var _db = co.db;
                    var q = (from p in _db.TB_R_ACCR_BALANCE
                             select new AccruedBalanceData
                             {
                                 BOOKING_NO = p.BOOKING_NO
                                 , SUSPENSE_NO_NEW = p.SUSPENSE_NO_NEW
                                 , DIVISION_ID = p.DIVISION_ID
                             });

                    if (param.ContainsKey("suNo"))
                    {
                        int suNo = Convert.ToInt32(param["suNo"]);
                        q = q.Where(x => x.SUSPENSE_NO_NEW == suNo);
                    }
                    if (param.ContainsKey("bookingNo"))
                    {
                        string bookingNo = param["bookingNo"].ToString();
                        q = q.Where(x => x.BOOKING_NO.Equals(bookingNo));
                    }

                    if (q.Any())
                    {
                        balance = q.FirstOrDefault();
                    }
                }
            }
            catch (Exception e)
            {
                Handle(e);
            }

            return balance;
        }

        public string getAccrReffNo(string accrNo)
        {
            var temp = (from a in db.TB_R_ACCR_DOC_NO
                        where a.ACCR_NO == accrNo
                        select a.ACCR_DOC_NO).FirstOrDefault();

            return temp.str();
        }

        // fid.Hadid 20180730 | for sap offline (dummy sap)
        public decimal? getWBSDummy(string wbsNo)
        {
            return Qx<decimal?>("GetWBSDummy", new { WbsNo = wbsNo }).FirstOrDefault();
        }

        public string getSapDocNoDummy()
        {
            return Qx<string>("GetSapDocNoDummy").FirstOrDefault();
        }
        // end dummy sap
    }
}
