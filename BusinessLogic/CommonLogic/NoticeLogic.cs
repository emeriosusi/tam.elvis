﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Model;
using Common;
using Common.Data;
using Common.Function;
using System.Diagnostics;
using System.Data;
using System.Data.EntityClient;
using Dapper;

namespace BusinessLogic.CommonLogic
{
    public class NoticeLogic : LogicBase
    {
        #region Search Inquiry
        public List<NoticeHomeData> Search(string _UserName)
        {
            string k = "NoticeHomeData(" + StrTo.EscapeQuotes(_UserName) + ")";
            List<NoticeHomeData> hd = Heap.Get<List<NoticeHomeData>>(k);
            if (hd == null)
            {
                List<NoticeHomeData> r = new List<NoticeHomeData>();
                List<decimal> docs = new List<decimal>();
                try
                {
                    r = Logic.Cx.DB.Query<NoticeHomeData>("sp_GetNoticeHomeListAccr",
                        new { user = StrTo.EscapeQuotes(_UserName) },
                        null, false, null, CommandType.StoredProcedure).ToList();
                }
                catch (Exception e) { LoggingLogic.err(e); }

                List<NoticeHomeData> d = new List<NoticeHomeData>();
                foreach (var n in r)
                {
                    if (!docs.Contains(n.REFF_NO))
                    {
                        d.Add(n);
                        docs.Add(n.REFF_NO);
                    }
                }
                hd = d;
                Heap.Set(k, d, Heap.Duration * 4);
            }
            return hd;
        }

        public List<NoticeData> SearchNoticeInquiry(string docNo, string docYear)
        {
            return Qu<NoticeData>("GetNoticeData", docNo, docYear).ToList();
        }

        public List<UserRoleHeader> GetNoticeUser(String pvNoParam, String pvYearParam)
        {
            List<UserRoleHeader> r = new List<UserRoleHeader>();
         

                string location = String.Format("GetNoticeUser(No= {0}, Year={1})", pvNoParam ?? "", pvYearParam ?? "");
                try
                {
                    Decimal reffNo;
                    if (pvNoParam + pvYearParam == "")
                    {
                        r = hQu<UserRoleHeader>("GetNoticeUserAll").ToList();
                    }
                    // fid.Hadid. 20180617.
                    // to accomodate Accrued doc no
                    //else if (!pvNoParam.isEmpty() && !pvYearParam.isEmpty())
                    else if (!pvNoParam.isEmpty())
                    {
                        reffNo = Decimal.Parse(pvNoParam + pvYearParam);
                        r = hQu<UserRoleHeader>("GetNoticeUser", reffNo).ToList();
                    }

                }
                catch (Exception ex)
                {
                    (new LoggingLogic()).Log("MSTD00002ERR", ex.Message, location);
                }
          
            return r;
        }
        #endregion

        #region Insert

        public void InsertNotice(
            String _no, String _year,
            String userName, String userRole,
            String noticeTo, String userRoleTo,
            List<string> NoticeTos,
            String comment,
            Boolean isNeedToReply,
            bool hasWorklist = false)
        {
            Exec("sp_PutNotice", new { no = _no, year =_year, username= userName, userRole=userRole,noticeTo= noticeTo, userRoleTo=userRoleTo, 
            noticeTos= string.Join(",", NoticeTos), comment =comment, needReply = ((isNeedToReply)? 1:0), hasWorklist= ((hasWorklist)? 1:0)
            });
        }

        public static void NoticeToFirstInList(string noticeTo, List<string> recipient)
        {
            int noticeLoc = recipient.IndexOf(noticeTo);
            if (noticeLoc > 0)
            {
                recipient.RemoveAt(noticeLoc);
                recipient.Insert(0, noticeTo);
            }
            else if (noticeLoc < 0)
            {
                recipient.Insert(0, noticeTo);
            }
        }

        #endregion

        public List<string> listNoticeRecipient(string pvNumber, string pvYear, string _from, string _to)
        {
            List<string> r = new List<string>();
            try
            {
                r= Qx<string>("GetNoticeRecipient", new { no = pvNumber, year = pvYear, from = _from, to = _to }).ToList();
            }
            catch (Exception e)
            {
                LoggingLogic.err(e); 
            }
            return r;
           
        }


        public List<string> listWorkflowRecipient(string pvNumber, string pvYear, decimal status)
        {
            List<string> r = new List<string>();
            try
            {
                r = Qx<string>("GetWorkflowRecipient", new { no = pvNumber, year = pvYear, status=status }).ToList();
            }
            catch (Exception e)
            {
                LoggingLogic.err(e); 
            }
            return r;
            
        }

        #region Update
        public List<NoticeData> releaseNotice(String pvNoParam,
                                               String pvYearParam,
                                               String noticeNoParam,
                                               String userNameParam,
                                               ref ErrorData err)
        {
            List<NoticeData> l = new List<NoticeData>();
            try
            {
                l = Qx<NoticeData>("SetNoticeRelease", new { no = pvNoParam, year = pvYearParam, noticeNo = noticeNoParam, userName = userNameParam }).ToList();
                if (l.Count < 1)
                {
                     err = new ErrorData();
                        err.ErrID = 1;
                        err.ErrMsg = "release the notice";
                        err.ErrMsgID = "MSTD00022ERR";
                }
            }

            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                err = new ErrorData();
                err.ErrID = 1;
                err.ErrMsg = ex.Message;
                err.ErrMsgID = "MSTD00002ERR";
            }
            return l;
            
        }
        #endregion
    }
}
