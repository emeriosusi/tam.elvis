﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Model;

namespace BusinessLogic.CommonLogic
{
    public class CommonDataRetriever
    {
        protected readonly ELVIS_DBEntities elvisdb;

        public CommonDataRetriever()
        {
            elvisdb = new ELVIS_DBEntities();
        }

        public List<CodeConstant> getDocumentStatus(bool includeNotCreatedStatus, string moduleCode)
        {
            List<CodeConstant> lstResult = new List<CodeConstant>();
            var qAllStatus = from x in elvisdb.TB_M_STATUS 
                             select new { x.STATUS_CD, x.STATUS_NAME, x.MODULE_CD };
            var maxStat = qAllStatus.Select(p => p.STATUS_CD).Max();
            var q = qAllStatus.Where(p => (p.STATUS_CD <= 30));
            if (!includeNotCreatedStatus)
            {   
                q = qAllStatus.Where(p => (p.STATUS_CD >= 0));
            }
            if (!string.IsNullOrEmpty(moduleCode))
            {
                q = qAllStatus.Where(p => (p.MODULE_CD.Equals(moduleCode)));
            }

            q = q.Union(qAllStatus.Where(p => p.STATUS_CD == maxStat));
            if ((q != null) && q.Any())
            {
                var qresult = q.ToList();
                foreach (var d in qresult)
                {
                    lstResult.Add(new CodeConstant()
                    {
                        Code = d.STATUS_CD.ToString(),
                        Description = d.STATUS_NAME
                    });
                }
            }

            return lstResult;
        }
    }
}
