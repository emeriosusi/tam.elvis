﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Model;
using Common.Data;
using System.Web;
using Common.Function;
using Dapper; 
using Common;

namespace BusinessLogic.CommonLogic
{
    public class ComboBoxLogic : LogicBase
    {
        public List<ComboClassData> Data(string id, List<ComboClassData> dx=null)
        {
            List<ComboClassData> d = null;
            
                string idx = "Combo." + id;
                if (dx == null)
                {
                    
                        d = Heap.Get<List<ComboClassData>>(idx);
                        d = d.Clone();
                    
                }
                else
                {
               
                    Heap.Set<List<ComboClassData>>(idx, dx.Clone());
            }
            return d;
        }

        public List<ComboClassData> getComboData(string query, bool needAll, string id = "")
        {
            List<ComboClassData> _retData;

            if (id.isEmpty())
            {
                id = Common.Util.Hash(query);
            }
            _retData = Data(id);
            if (_retData == null)
            {
                IEnumerable<ComboClassData> results = Db.Query<ComboClassData>(query).ToList();
                if (results.Count() > 0)
                    _retData = results.ToList<ComboClassData>();
                else
                    _retData = null;

                if (_retData != null)
                {
                    Data(id, _retData);
                }
            }

            if (needAll)
            {
                ComboClassData dataAll = new ComboClassData();
                dataAll.COMBO_CD = "";
                dataAll.COMBO_NAME = "All";
                if (_retData == null)
                    _retData = new List<ComboClassData>();
                _retData.Insert(0, dataAll);
            }
            else
            {
                ComboClassData dataSelect = new ComboClassData();
                dataSelect.COMBO_CD = "";
                dataSelect.COMBO_NAME = "Select";
                if (_retData == null)
                    _retData = new List<ComboClassData>();
                _retData.Insert(0, dataSelect);
            }
            return _retData;
        }
    }
}
