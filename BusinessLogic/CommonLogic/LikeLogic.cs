﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.CommonLogic
{
    public class LikeLogic
    {
        public const int igFIRST = 4;
        public const int igMIDDLE = 2;
        public const int igLAST = 1;
        public const int igFIRSTLAST = igFIRST | igLAST;
        public const int igALL = igFIRST | igMIDDLE | igLAST;
        public const int igNONE = 0;
        public const string X = "*";

        public static int ignoreWhat(string s, ref string sPre, ref string sPost)
        {
            if (String.IsNullOrEmpty(s))
                return igALL;
            else if (s.Equals(X))
                return igALL;
            else
            {
                int r = igNONE;
                int p = s.IndexOf(X);
                if (p >= 0)
                {
                    if (s.StartsWith(X))
                    {
                        r = r | igFIRST;
                        sPre = "";
                        while (s.Substring(p, 1).Equals(X) && p < s.Length) p++;
                        sPost = s.Substring(p, s.Length - p);                        
                    }
                    if (s.EndsWith(X))
                    {
                        r = r | igLAST;                        
                        sPre = s.Substring(0, p);
                        sPost = "";
                    }
                    if (((r & igFIRST) == 0) && ((r & igLAST) == 0))
                    {
                        r = r | igMIDDLE;
                        sPre = s.Substring(0, p);
                        sPost = s.Substring(p + 1, s.Length - (s.LastIndexOf(X) + 1));

                    }
                    else if ( (r & igFIRST) != 0 && (r & igLAST) != 0)  
                    {
                        sPre = s.Replace(X, " ").Trim();
                        sPost = "";
                    }
                }
                return r;
            }
        }
    }
}
