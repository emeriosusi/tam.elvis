﻿using System;
using System.Collections.Generic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;

namespace BusinessLogic.CommonLogic
{
    public class PVRVWorkFlowLogic
    {
        private FormPersistence formPersistence;
        private LogicFactory logic = LogicFactory.Get();

        public PVRVWorkFlowLogic()
        {
            formPersistence = new FormPersistence();
        }

        #region Approval
        public bool Go(PVFormData data, string _Command, UserData _UserData, string _Comment)
        {
            bool _RetVal = false;
            try
            {
                if (data != null)
                {
                    data.FinanceAccessEnabled = false;
                    _RetVal = formPersistence.postToK2(data, _UserData, true);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("WSACancelBlockingCall")) // added to ignore socket error, assuming operation always successfull
                    _RetVal = true;
                else 
                
                throw;
            }
            return _RetVal;
        }

        public bool Go(string _DocNo, string _Command, UserData _UserData, string _Comment, bool isCancel = false, string folio = null)
        {
            bool _RetVal = false;
            
            logic.Say("PVRVWorkflowLogic", "ApprovalRejectionPVForm('{0}', '{1}', '{2}', '{3}', {4})", _DocNo, _Command, _UserData.USERNAME, _Comment, isCancel);
            
            try
            {
                Dictionary<string, string> _DataField = new Dictionary<string, string>();
                if (string.IsNullOrEmpty(folio))
                    folio = _DocNo;

                _DataField.Add("REFF_NO", _DocNo);
                //_DataField.Add("Comment", _Comment);
                //_DataField.Add("EmailFrom", _UserData.EMAIL);
                //_DataField.Add("PICFullName", _UserData.FIRST_NAME + " " + _UserData.LAST_NAME);
                _DataField.Add("CurrentPIC", _UserData.USERNAME);

                _RetVal = logic.WorkFlow.K2SendCommandWithCheckWorklist(folio, _UserData, _Command, _DataField);
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                logic.Log.Log("MSTD00002ERR", 
                        "ApprovalRejectionPVForm(" + _DocNo + "," + _Command + "," 
                            + _UserData.USERNAME + ',' + _Comment + ")\r\n" 
                            + LoggingLogic.Trace(ex) , "PVRVWorkflowLogic.ApproveRejectionPVForm", _UserData.USERNAME);
                
                if (ex.Message.Contains("WSACancelBlockingCall")) // added to ignore socket error, assuming operation always successfull
                    _RetVal = true;
                else 
                    throw ;
            }
            return _RetVal;
        }
        #endregion

        public bool Redirect(string _DocNo, UserData _UserData, String byPassUserName)
        {
            bool _RetVal = false;
            string loc = string.Format("Redirect({0},'{1}','{2}')", _DocNo, _UserData.USERNAME, byPassUserName);
            Ticker.In(loc);
            try
            {
                Dictionary<string, string> _DataField = new Dictionary<string, string>();
                _DataField.Add("REFF_NO", _DocNo);
                _RetVal = logic.WorkFlow.K2SendRedirectCommand(_DocNo, _UserData, byPassUserName, _DataField);
            }
            catch (Exception ex) 
            {
                logic.Log.Log("MSTD00002ERR",
                        "Redirect(" + _DocNo + "," 
                            + _UserData.USERNAME + ',' + byPassUserName + ")\r\n"
                            + LoggingLogic.Trace(ex), "PVRVWorkflowLogic.RedirectWorkListPVForm", _UserData.USERNAME);
                if (ex.Message.Contains("WSACancelBlockingCall")) // added to ignore socket error, assuming operation always successfull
                    _RetVal = true;
                else 
                    throw ;
            }
            Ticker.Out(loc);
            return _RetVal;
        }

    }
}
