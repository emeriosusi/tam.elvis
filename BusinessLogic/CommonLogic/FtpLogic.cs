﻿using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Text;
using Common;

namespace BusinessLogic.CommonLogic
{
    public class FtpLogic
    {
        int isExisting = 1; //1-Doc Year, 2-module-PV or module-RV, 3-Doc No, 4-file
        public string IP = Common.AppSetting.FTP_SERVER;
        public string UserID = Common.AppSetting.FTP_USERID;
        public string Password = Common.AppSetting.FTP_PASS;
        public string Directory;
        private readonly Common.Function.GlobalResourceData _globalResource = new Common.Function.GlobalResourceData();

        public void createDir(string docYear, string module, string docNo, string dirName)
        {
            FtpWebRequest reqFTP = null;
            Stream ftpStream = null;

            try
            {
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/"));

                switch (isExisting)
                {
                    case 1:
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + dirName));
                        break;
                    case 2:
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + docYear + "/" + dirName));
                        break;
                    case 3:
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + docYear + "/" + module + "/" + dirName));
                        break;
                }
                reqFTP.Proxy = new WebProxy();
                reqFTP.Method = WebRequestMethods.Ftp.MakeDirectory;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(UserID, Password);
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                ftpStream = response.GetResponseStream();
                ftpStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                if (ftpStream != null)
                {
                    ftpStream.Close();
                    ftpStream.Dispose();
                }
                LoggingLogic.err(ex);
                LoggingLogic.say("ftp", "!mkdir " + reqFTP.RequestUri);
            }
        
        }

        public bool ftpUpload(string module, string docNo, string filename, string filepath, ref bool result, ref string errMessage)
        {
            string docYear = DateTime.Now.ToString("yyyy");
            return ftpUpload(docYear, module, docNo, filename, filepath, ref result, ref errMessage);
        }

        /**
         * @Lufty: Customized for PVForm
         */
        public bool ftpUploadBytes(string docYear, string module,
                                      string docNo, string filename,
                                      byte[] fileBytes, ref bool result,
                                      ref string errMessage)
        {
            result = false;

            Directory = "ftp://" + IP + "/" + docYear + "/" + module + "/" + docNo + "/";
            string ftpfullpath = Directory + "/" + filename;
            FtpWorker fw = new FtpWorker();
            result = fw.Upload(fileBytes, filename, Directory, ref result, ref errMessage);

            
            return result;
        }


        public bool Move(string src, string des)
        {
            string result = "";
            FtpWebRequest reqFTP = null;

            FtpWorker fw = new FtpWorker();
            string reqHead = "ftp://" + IP + "/";
            string md = "";
            int lastSlash = des.LastIndexOf("/");
            if (lastSlash > 0)
            {
                md = reqHead + des.Substring(0, lastSlash);
                fw.MakeDirs(md);
            }
            
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + src));
            LoggingLogic.say("ftp", reqFTP.RequestUri.ToString());
            
            LoggingLogic.say(reqFTP.RequestUri.ToString());
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(UserID, Password);
            reqFTP.Method = WebRequestMethods.Ftp.Rename;
            reqFTP.KeepAlive = false;
            reqFTP.UsePassive = false;
            reqFTP.Proxy = new WebProxy();
            string[] a = src.Split('/');
            string target = "";
            for (int i = 0; i < a.Length-1; i++)
            {
                target += "../";
            }
            target = target + des;
            LoggingLogic.say("ftp", "Move '{0}' to '{1}'", src , target);
            reqFTP.RenameTo = target;
            FtpWebResponse response = (FtpWebResponse) reqFTP.GetResponse();
            Stream datastream = response.GetResponseStream();
            StreamReader sr = new StreamReader(datastream);
            result = sr.ReadToEnd();
            sr.Close();
            response.Close();
            LoggingLogic.say("ftp", "Move result={0}", result);
            return true;
        }

        public bool ftpUpload(string docYear, string module, string docNo, string filename, string filepath, ref bool result, ref string errMessage)
        {
            result = false;
            Directory = "ftp://" + IP + "/" + docYear + "/" + module + "/" + docNo + "/";
            string ftpfullpath = Directory + "/" + filename;
            FtpWorker fw = new FtpWorker();
            result = fw.Upload(filename, ftpfullpath, ref result, ref errMessage);
                       
            return result;
        }

        public bool checkDir(string docYear, string module, string docNo, string dir)
        {
            //list directory to Array
            string[] Files = GetFileList(docYear, module, docNo, dir);
            ArrayList arrDirectories = new ArrayList();
            if (Files != null)
            {
                foreach (string i in Files)
                {
                    arrDirectories.Add(i);
                }
            }

            if (arrDirectories.Contains(dir))
                return true;
            else
                return false;
        }

        public string[] GetFileList(string docYear, string module, string docNo, string fileName)
        {
            LoggingLogic.say("ftp", "GetFileList(year={0}, module={1}, no={2}, filename={3})", docYear, module, docNo, fileName);
                
            string[] downloadFiles;
            StringBuilder result = new StringBuilder();
            WebResponse response = null;
            StreamReader reader = null;
            FtpWebRequest reqFTP = null;
            try
            {
                
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/"));

                switch (isExisting)
                {
                    case 1:
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/"));
                        break;
                    case 2:
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + docYear + "/"));
                        break;
                    case 3:
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + docYear + "/" + module + "/"));
                        break;
                    case 4:
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + docYear + "/" + module + "/" + docNo + "/"));
                        break;
                }
                LoggingLogic.say("ftp", "Req="  + reqFTP.RequestUri.ToString());
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(UserID, Password);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                reqFTP.Proxy = new WebProxy();
                reqFTP.KeepAlive = false;
                reqFTP.UsePassive = false;
                response = reqFTP.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                string line = reader.ReadLine();
                while (line != null)
                {
                    result.Append(line);
                    result.Append("\n");
                    line = reader.ReadLine();
                }
                // to remove the trailing '\n'
                string list = result.ToString();
                if (list.LastIndexOf("\n") > 0)
                    result.Remove(result.ToString().LastIndexOf('\n'), 1);
                return list.Split('\n');
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                LoggingLogic.say("ftp", "!ls " + reqFTP.RequestUri);
                if (reader != null)
                {
                    reader.Close();
                }
                if (response != null)
                {
                    response.Close();
                }
                downloadFiles = null;
                return downloadFiles;
            }
        }

        public bool Delete(string directory, string filename)
        {
            try
            {
                directory = "ftp://" + IP + "/" + directory;
                return Delete(directory + "/" + filename);
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw ;
            }
        }

        public bool Rename(string directory, string currentFilename, string newFilename)
        {
            FtpWebRequest reqFTP = null;
            Stream ftpStream = null;
            bool result = false;
            try
            {

                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + IP + "/" + directory + "/" + currentFilename));
                reqFTP.RenameTo = newFilename;
                reqFTP.UseBinary = true;
                reqFTP.Credentials = new NetworkCredential(UserID, Password);
                reqFTP.Method = WebRequestMethods.Ftp.Rename;
                reqFTP.Proxy = new WebProxy();
                reqFTP.KeepAlive = false;
                reqFTP.UsePassive = false;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                ftpStream = response.GetResponseStream();
                ftpStream.Close();
                response.Close();
                result = true;
            }
            catch (Exception ex)
            {
                if (ftpStream != null)
                {
                    ftpStream.Close();
                    ftpStream.Dispose();
                }
                LoggingLogic.err(ex);
                LoggingLogic.say("ftp", "!rename {0}\t{1}", currentFilename, newFilename);
                // throw new Exception(ex.Message.ToString());
            }

            return result;
        }

        public bool RemoveDir(string url)
        {
            try
            {
                url = "ftp://" + IP + "/" + url;
                Uri serverUri = new Uri(url);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return false;
                }
                FtpWebRequest reqFTP = (FtpWebRequest)WebRequest.Create(serverUri);

                reqFTP.Credentials = new NetworkCredential(UserID, Password);
                reqFTP.KeepAlive = false;
                reqFTP.Proxy = new WebProxy();
                reqFTP.Method = WebRequestMethods.Ftp.RemoveDirectory;

                string result = String.Empty;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                long size = response.ContentLength;
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
                return true;
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                LoggingLogic.say("ftp", "!rmdir " + url);
            }
            
            return false;
        }

        public bool Delete(string url)
        {
            try
            {
                //string uri = "ftp://" + IP + "/" + fileName;
                Uri serverUri = new Uri(url);
                if (serverUri.Scheme != Uri.UriSchemeFtp)
                {
                    return false;
                }
                FtpWebRequest reqFTP = (FtpWebRequest)WebRequest.Create(serverUri);

                reqFTP.Credentials = new NetworkCredential(UserID, Password);
                reqFTP.KeepAlive = false;
                reqFTP.Proxy = new WebProxy();
                reqFTP.Method = WebRequestMethods.Ftp.DeleteFile;

                string result = String.Empty;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                long size = response.ContentLength;
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
                return true;
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                LoggingLogic.say("ftp", "!delete " + url);
            }
            return false;
        }
    }
}
