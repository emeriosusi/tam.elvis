﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using Common.Data._70History;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data;
using DataLayer.Model;
using Common.Data;
using System.IO;
using BusinessLogic.CommonLogic;
using System.Diagnostics;


namespace BusinessLogic._70History
{
    public class HistoryLogic
    {
        #region Search Inquiry
        public List<HistoryData> SearchInqury(string _docNo, string _status)
        {
            List<HistoryData> result = new List<HistoryData>();
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var _db = co.db;
                    //default ignore all searching criteria
                    #region initialization ignore variable

                    bool ignoreDocNo = true;
                    bool ignoreStatus = true;
                    decimal docNo = 0;
                    #endregion

                    #region build non-mandatory searching criteria

                    if (!string.IsNullOrEmpty(_docNo))
                    {
                        ignoreDocNo = false;
                        docNo = Convert.ToDecimal(_docNo.Trim());
                    }

                    int status = 0;
                    if (!string.IsNullOrEmpty(_status) && Convert.ToInt32(_status) != -1)
                    {
                        ignoreStatus = false;
                        status = Convert.ToInt32(_status.Trim());
                    }

                    #endregion

                    #region Query
                    var q = (from p in _db.vw_History
                             where (ignoreDocNo || p.REFF_NO == docNo)
                             where (ignoreStatus || p.STATUS_CD == status)
                             orderby p.ACTUAL_DT descending
                             select p).ToList();

                    if (q.Any())
                    {
                        foreach (var data in q)
                        {
                            HistoryData _data = new HistoryData();
                            _data.REFF_NO = data.REFF_NO;
                            // _data.MODULE_DESC = data.MODULE_DESC;
                            _data.STATUS_NAME = data.STATUS_NAME;
                            _data.USERNAME = data.USERNAME;
                            _data.ACTUAL_DT = data.ACTUAL_DT;

                            result.Add(_data);
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);

                    throw;
                }
            return result;
        }
        #endregion
        public byte[] Get(string _username, string _imagePath, List<HistoryData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("History");
            x.PutHeader("History",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Doc No|Status|Username|Process Date",
                _imagePath);
            int rownum = 0;
            foreach (HistoryData dx in _list)
            {
                x.Put(++rownum,
                    dx.REFF_NO,
                    // dx.MODULE_DESC,
                    dx.STATUS_NAME,
                    dx.USERNAME,
                    dx.ACTUAL_DT);
            }
            x.PutTail();
            return x.GetBytes();
        }
    }
}
