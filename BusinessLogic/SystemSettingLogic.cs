﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.EntityClient;
using DataLayer.Model;
using Common;
using Common.Data;
using Common.Function;
using System.Data.Objects.SqlClient;
using System.Web;
using Dapper;

namespace BusinessLogic
{
    public class SystemSettingLogic : LogicBase
    {
        public LogicFactory lo;

        public SystemSettingLogic(ContextWrap co = null)
        {
            if (co != null)
                this.db = co.db;

            lo = LogicFactory.Get();
        }

        private FiscalDate fiscalPeriod = null;
        public int FiscalYear(DateTime d)
        {
            if (fiscalPeriod == null)
            {
                fiscalPeriod = new FiscalDate();
                int? mm, dd;

                mm = GetValue("FISCAL_YEAR_FROM", "MONTH");
                dd = GetValue("FISCAL_YEAR_FROM", "DATE");
                fiscalPeriod.DateFrom = (dd ?? 1);
                fiscalPeriod.MonthFrom = (mm ?? 4);

                mm = GetValue("FISCAL_YEAR_TO", "MONTH");
                dd = GetValue("FISCAL_YEAR_TO", "DATE");
                fiscalPeriod.DateTo = (dd ?? 31);
                fiscalPeriod.MonthTo = (mm ?? 3);
            }
            DateTime dx = new DateTime(d.Year, fiscalPeriod.MonthFrom, fiscalPeriod.DateFrom);
            DateTime dy = new DateTime(d.Year + 1, fiscalPeriod.MonthTo, fiscalPeriod.DateTo);

            if (dx.CompareTo(d) > 0)
            {
                return d.Year - 1;
            }
            else if (dx.CompareTo(d) <= 0 && dy.CompareTo(d) >= 0)
            {
                return d.Year;
            }
            else
            {
                return d.Year;
            }
        }

        public List<SystemMaster> GetSystemMaster(string key)
        {
            return hQu<SystemMaster>("SYS_GETMASTER", key).ToList();
        }

        #region GetSystemSettingValues

        public int? GetValue(string stype, string scode)
        {
            int? r = null;
            try
            {
                r = GetInt(stype, scode);
                if (r == null)
                {
                    r = hQu<int?>("SYS_GETVALUE", stype, scode).FirstOrDefault();
                    if (r != null)
                        SetInt(stype, scode, r.Value);
                }
            }
            catch (Exception ex)
            {
                r = null;
                LoggingLogic.err(ex);
            }
            return r;
        }

        public string[] GetArray(string stype, string scode, string delim = ",")
        {
            string[] r = new string[] { };
            string t = GetText(stype, scode);
            r = (t ?? "").Split(new string[] { delim }, StringSplitOptions.RemoveEmptyEntries);
            if (t != null && r != null && r.Length > 1)
            {
                for (int i = 0; i < r.Length; i++)
                {
                    r[i] = r[i].Trim();
                }
            }
            return r;
        }

        public List<string> GetList(string stype)
        {
            List<string> r = new List<string>();
            try
            {
                r = hQu<string>("SYS_GETLIST", stype).ToList();
                for (int i = 0; i < r.Count; i++)
                {
                    r[i] = r[i].Trim();
                }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return r;
        }

        public StringMap GetDict(string stype)
        {
            StringMap d = new StringMap();
            try
            {
                List<SystemMaster> s = GetSystemMaster(stype);
                if (s != null)
                    foreach (var t in s)
                    {
                        d.Put((t.Code ?? "").Trim(), (t.ValueTxt ?? "").Trim());
                    }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return d;
        }

        public int GetNum(string sKeyCode, int ValueDefault = 0)
        {
            if (sKeyCode.isEmpty()) return ValueDefault;
            string skey = sKeyCode;
            string scode = "";
            if (sKeyCode.Contains("/"))
            {
                string[] skc = sKeyCode.Split(new string[] { "/" }, StringSplitOptions.None);
                skey = skc[0];
                scode = skc[1];
            }
            else
            {
                skey = sKeyCode;
            }
            List<int> li = hQu<int>("SYS_GETNUM", skey, scode).ToList();
            if (li != null && li.Count > 0)
                return li.FirstOrDefault();
            else
                return ValueDefault;            
        }

        public readonly string CASHIER_USERNAME = "ELVIS_CASHIER_USERNAME";
        public string CashierUserName
        {
            get
            {
                string cn = null;
                cn = Heap.Get<string>(CASHIER_USERNAME);
                if (cn.isEmpty())
                {
                    cn = hQu<string>("SYS_GET_CASHIER").FirstOrDefault();
                    Heap.Set(CASHIER_USERNAME, cn, Heap.Duration * 100);
                }
                return cn;
            }
        }

        public static readonly string SSI = "Sys.Int";
        public static int? GetInt(string stype, string scode)
        {
            Dictionary<string, int> d = Heap.Get<Dictionary<string, int>>(SSI);
            if (d != null)
            {
                string k = stype + "." + scode;
                if (d.ContainsKey(k))
                    return d[k] as int?;
            }
            return null;
        }

        public static void SetInt(string stype, string scode, int value)
        {
            Dictionary<string, int> d = Heap.Get<Dictionary<string, int>>(SSI);

            string k = stype + "." + scode;
            if (d == null)
            {
                d = new Dictionary<string, int>();
                d[k] = value;
                Heap.Set<Dictionary<string, int>>(SSI, d, Heap.Duration * 100);
            }
            d[k] = value;

        }

        public static readonly string SSV = "Sys.Text";
        public static string Gets(string stype, string scode)
        {
            Dictionary<string, string> ssv = Heap.Get<Dictionary<string, string>>(SSV);
            if (ssv != null)
            {
                string k = stype + "." + scode;
                if (ssv.ContainsKey(k))
                    return ssv[k];
            }
            
            return null;
        }

        public static void Sets(string stype, string scode, string sValue)
        {
            Dictionary<string, string> d = Heap.Get < Dictionary<string, string>>(SSV);
            string k = stype + "." + scode;
            if (d == null)
            {
                d = new Dictionary<string, string>();
                d[k] = sValue;
                Heap.Set<Dictionary<string, string>>(SSV, d, Heap.Duration *100);
            }
            d[k] = sValue;
        }

        public string GetText(string stype, string scode, string defaultValue = "")
        {
            string r = defaultValue;
            try
            {
                r = Gets(stype, scode);
                if (r.isEmpty())
                {
                    r = hQu<string>("SYS_GETTEXT", stype, scode).FirstOrDefault();

                    if (r == null)
                        r = defaultValue;

                    Sets(stype, scode, r);
                }
            }
            catch (Exception ex)
            {
                r = null;
                LoggingLogic.err(ex);
            }
            return r;
        }

        public DateTime? GetDate(string stype, string scode)
        {
            DateTime? r = null;
            try
            {
                string x = hQu<string>("SYS_GETDATE", new { atype = stype, acd = scode }).FirstOrDefault();
                if (string.IsNullOrEmpty(x))
                {
                    DateTime d;
                    if (!DateTime.TryParseExact(x, "dd/MM/yyyy",
                        System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.AssumeLocal, out d))
                    {
                        d = new DateTime(StrTo.NULL_YEAR, 1, 1);
                    }
                    else
                    {
                        r = d;
                    }
                }
            }
            catch (Exception ex)
            {
                r = null;
                LoggingLogic.err(ex);
            }
            return r;
        }

        #endregion

        string[] RoundCurrency = null;
        public string[] RoundCurrencies()
        {
            if (RoundCurrency == null)
            {
                string roundCurrency = GetText("INVOICE_UPLOAD", "NO_FRACTION_CURRENCY");
                if (string.IsNullOrEmpty(roundCurrency))
                {
                    RoundCurrency = new string[] { "IDR", "JPY" };
                }
                else
                {
                    RoundCurrency = roundCurrency.Split(',');
                }
            }
            return RoundCurrency;
        }


        public void ResetCache()
        {
            Heap.Set<Dictionary<string, int>>(SSI, null);
            Heap.Set<Dictionary<string, string>>(SSV, null);
            Heap.Set<string>(CASHIER_USERNAME, null);
        }
    }
}
