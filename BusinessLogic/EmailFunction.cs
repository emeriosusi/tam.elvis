﻿using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Function;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;

namespace BusinessLogic
{
    public class EmailFunction : LogicBase
    {
        private string SMTPSERVER;

        private string MailFrom;

        private string MailPath;

        private string MailSink;

        public EmailFunction(string mailPath)
        {
            this.MailPath = mailPath;
            this.SMTPSERVER = base.Logic.Sys.GetText(AppSetting.SMTP_SERVER_FIELD_NAME, "", "");
            this.MailSink = base.Logic.Sys.GetText("EMAIL_NOTICE", "EML_TO", "");
            this.MailFrom = base.Logic.Sys.GetText("EML_WORKLIST_REMINDER", "EML_FROM", "noreply.elvis-admin@toyota.astra.co.id");
        }

        public bool ComposeAdminEmailNotification(string DOC_NO, string DOC_YEAR, string link, ref ErrorData Err)
        {
            object[] dOCNO = new object[] { DOC_NO, DOC_YEAR, link };
            LoggingLogic.say("EmailFunction", "ComposeAdminEmailNotification({0}, {1}, {2}, Err)", dOCNO);
            bool result = false;
            try
            {
                long num = Convert.ToInt64(DOC_NO);
                short num1 = Convert.ToInt16(DOC_YEAR);
                string vendorName = "";
                string transactionName = "";
                string toEmail = "";
                string fromEmail = "ELVIS-ADMIN@toyota.co.id";
                IQueryable<string> qpv =
                    from p in base.db.TB_R_PV_H
                    where (long)p.PV_NO == num && p.PV_YEAR == (int)num1
                    select p.PAY_METHOD_CD;
                string paymentMethod = "";
                if (qpv.Any<string>())
                {
                    paymentMethod = qpv.FirstOrDefault<string>();
                }
                if (string.IsNullOrEmpty(paymentMethod) || !paymentMethod.Equals("C") && !paymentMethod.Equals("E"))
                {
                    return false;
                }
                else
                {
                    var data = (
                        from i in base.db.vw_GetUserEmail
                        where (long)i.PV_NO == num && i.PV_YEAR == (int)num1
                        select new { VENDOR_NAME = i.VENDOR_NAME, TRANSACTION_NAME = i.TRANSACTION_NAME, EMAIL = i.EMAIL }).FirstOrDefault();
                    if (data != null)
                    {
                        vendorName = data.VENDOR_NAME;
                        transactionName = data.TRANSACTION_NAME;
                        toEmail = data.EMAIL;
                    }
                    TextReader reader = new StreamReader(this.MailPath);
                    DateTime datum = (new FormPersistence()).GetDocDate(DOC_NO, DOC_YEAR);
                    string MailBody = reader.ReadToEnd();
                    if (MailBody.Contains("[DOC_NO]"))
                    {
                        MailBody = MailBody.Replace("[DOC_NO]", DOC_NO);
                    }
                    if (MailBody.Contains("[VENDOR_NM]"))
                    {
                        MailBody = MailBody.Replace("[VENDOR_NM]", vendorName);
                    }
                    if (MailBody.Contains("[TRANSACTION_TYPE_NM]"))
                    {
                        MailBody = MailBody.Replace("[TRANSACTION_TYPE_NM]", transactionName);
                    }
                    if (MailBody.Contains("[DOC_DATE]"))
                    {
                        MailBody = MailBody.Replace("[DOC_DATE]", datum.ToString("dd-MM-yyyy"));
                    }
                    if (MailBody.Contains("[DOC_LINK]"))
                    {
                        MailBody = MailBody.Replace("[DOC_LINK]", string.Format(link, datum.ToString("yyyy-MM-dd"), DOC_NO));
                    }
                    System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage()
                    {
                        IsBodyHtml = true,
                        Body = MailBody
                    };
                    System.Net.Mail.MailMessage MailMessage = mailMessage;
                    if (toEmail != null && toEmail != "" && toEmail.Contains("@"))
                    {
                        MailMessage.To.Add(toEmail);
                    }
                    string Subject = "[ELVIS]";
                    Subject = (!this.MailPath.Contains("PVCover") ? "[ELVIS] PV Ticket Notification" : "[ELVIS] PV Cover Notification");
                    Subject = string.Format(Subject, DOC_NO);
                    MailMessage.Subject = Subject;
                    MailAddress sender = new MailAddress(fromEmail);
                    MailMessage.Sender = sender;
                    MailMessage.From = sender;
                    if (MailMessage.To == null || MailMessage.To.Count<MailAddress>() <= 0)
                    {
                        result = false;
                        Err = new ErrorData(1, "MSTD00031WRN", "Email to", null);
                    }
                    else
                    {
                        using (SmtpClient client = new SmtpClient(this.SMTPSERVER))
                        {
                            client.Send(MailMessage);
                        }
                        result = true;
                    }
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                result = false;
                Err = new ErrorData(2, "MSTD00002ERR", ex.ToString(), null);
            }
            return result;
        }

        public bool ComposeApprovalNotice_EMAIL(string DOC_NO, string DOC_YEAR, string NOTICE, string NOTICE_LINK, string noticeTo, UserData UserLogin, string noticer, string issDiv, string DOC_TYPE, string op, ref ErrorData Err)
        {
            object[] dOCNO = new object[] { DOC_NO, DOC_YEAR, NOTICE, NOTICE_LINK, noticeTo, null, null, null, null, null };
            dOCNO[5] = (UserLogin != null ? UserLogin.USERNAME : "");
            dOCNO[6] = noticer;
            dOCNO[7] = issDiv;
            dOCNO[8] = DOC_TYPE;
            dOCNO[9] = op;
            LoggingLogic.say("EmailFunction", "ComposeApprovalNotice_EMAIL({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})", dOCNO);
            bool kode = false;
            try
            {
                string MailBody = (new StreamReader(this.MailPath)).ReadToEnd();
                if (MailBody.Contains("[DOCUMENT]"))
                {
                    MailBody = MailBody.Replace("[DOCUMENT]", DOC_TYPE);
                }
                if (MailBody.Contains("[DOC_NO]"))
                {
                    MailBody = MailBody.Replace("[DOC_NO]", DOC_NO);
                }
                if (MailBody.Contains("[NOTICE_COMMENT]"))
                {
                    MailBody = MailBody.Replace("[NOTICE_COMMENT]", NOTICE);
                }
                if (MailBody.Contains("[NOTICE_LINK]"))
                {
                    MailBody = MailBody.Replace("[NOTICE_LINK]", NOTICE_LINK);
                }
                if (MailBody.Contains("[USER_NOTICE]"))
                {
                    MailBody = MailBody.Replace("[USER_NOTICE]", string.Concat(UserLogin.FIRST_NAME ?? "", " ", UserLogin.LAST_NAME ?? ""));
                }
                System.Net.Mail.MailMessage MailMessage = new System.Net.Mail.MailMessage()
                {
                    IsBodyHtml = true,
                    Body = MailBody
                };
                if (noticeTo != null && noticeTo != "")
                {
                    if (!noticeTo.Contains("@"))
                    {
                        IQueryable<vw_User> ntc =
                            from a in base.db.vw_User
                            where a.USERNAME == noticeTo
                            select a;
                        if (ntc.Any<vw_User>())
                        {
                            MailMessage.To.Add(ntc.FirstOrDefault<vw_User>().EMAIL);
                        }
                    }
                    else
                    {
                        MailMessage.To.Add(noticeTo);
                    }
                }
                MailMessage.Subject = string.Format("[ELVIS] {0} {1} {2}", op, DOC_TYPE, DOC_NO);
                MailAddress sender = new MailAddress(UserLogin.EMAIL);
                MailMessage.Sender = sender;
                MailMessage.From = sender;
                if (MailMessage.To == null || MailMessage.To.Count<MailAddress>() <= 0)
                {
                    kode = false;
                    Err = new ErrorData(1, "MSTD00031WRN", "Email to", null);
                }
                else
                {
                    using (SmtpClient client = new SmtpClient(this.SMTPSERVER))
                    {
                        client.Send(MailMessage);
                    }
                    kode = true;
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                kode = false;
                Err = new ErrorData(2, "MSTD00002ERR", ex.ToString(), null);
            }
            return kode;
        }

        public bool ComposeBPH_EMAIL(string DOC_NO, string SUB_NO, DateTime DUE_DT, int DOC_YEAR, UserData UserLogin, ref ErrorData Err)
        {
            bool kode = false;
            try
            {
                string MailBody = (new StreamReader(this.MailPath)).ReadToEnd();
                if (MailBody.Contains("[DOC_NO]"))
                {
                    MailBody = MailBody.Replace("[DOC_NO]", DOC_NO);
                }
                if (MailBody.Contains("[SUB_NO]"))
                {
                    MailBody = MailBody.Replace("[SUB_NO]", SUB_NO);
                }
                if (MailBody.Contains("[DUE_DT]"))
                {
                    MailBody = MailBody.Replace("[DUE_DT]", DUE_DT.ToString("dd/MMM/yyyy"));
                }
                System.Net.Mail.MailMessage MailMessage = new System.Net.Mail.MailMessage()
                {
                    IsBodyHtml = true,
                    Body = MailBody
                };
                List<vw_User_Data> userLIst = base.db.vw_User_Data.ToList<vw_User_Data>();
                if (userLIst != null && userLIst.Count > 0)
                {
                    foreach (vw_User_Data recipient in userLIst)
                    {
                        System.Net.Mail.MailAddress MailAddress = new System.Net.Mail.MailAddress(recipient.EMAIL);
                        MailMessage.To.Add(MailAddress);
                    }
                }
                string Subject = base.Logic.Sys.GetText(AppSetting.BPH_EMAIL_SUBJECT_FIELD_NAME, "", "");
                if (Subject != "")
                {
                    if (Subject.Contains("[DOC_YEAR]"))
                    {
                        Subject = Subject.Replace("[DOC_YEAR]", DOC_YEAR.ToString());
                    }
                    MailMessage.Subject = Subject;
                }
                System.Net.Mail.MailAddress sender = new System.Net.Mail.MailAddress(UserLogin.EMAIL);
                MailMessage.Sender = sender;
                MailMessage.From = sender;
                if (userLIst == null || userLIst.Count <= 0)
                {
                    kode = false;
                    Err = new ErrorData()
                    {
                        ErrID = 1,
                        ErrMsgID = "MsgID0020"
                    };
                }
                else
                {
                    (new SmtpClient(this.SMTPSERVER)).Send(MailMessage);
                    kode = true;
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                kode = false;
                Err = new ErrorData()
                {
                    ErrID = 2,
                    ErrMsg = ex.ToString()
                };
            }
            return kode;
        }

        public bool ComposePoa(string Grantor, string Attorney, string poaNum, DateTime? validFrom, DateTime? validTo, string userName)
        {
            object[] grantor = new object[] { Grantor, Attorney, poaNum, validFrom, validTo, userName };
            LoggingLogic.say("EmailFunction", "ComposePoa({0}, {1}, {2}, {3}, {4}, {5})", grantor);
            bool Ok = false;
            if (!validFrom.HasValue || !validTo.HasValue || Grantor.isEmpty() || Attorney.isEmpty() || poaNum.isEmpty() || userName.isEmpty())
            {
                return false;
            }
            try
            {
                string PoaText = File.ReadAllText(Path.Combine(this.MailPath, "PoA.txt"));
                Templet t = new Templet(PoaText);
                string gName = "";
                string gEmail = "";
                string gPos = "";
                IQueryable<vw_User> qug =
                    from a in base.db.vw_User
                    where a.USERNAME == Grantor
                    select a;
                if (qug.Any<vw_User>())
                {
                    vw_User ug = qug.FirstOrDefault<vw_User>();
                    gName = string.Concat(ug.FIRST_NAME, " ", ug.LAST_NAME);
                    gEmail = ug.EMAIL;
                    gPos = ug.POSITION_NAME;
                }
                string aName = "";
                string aEmail = "";
                string aPos = "";
                IQueryable<vw_User> qua =
                    from a in base.db.vw_User
                    where a.USERNAME == Attorney
                    select a;
                if (qua.Any<vw_User>())
                {
                    vw_User ua = qua.FirstOrDefault<vw_User>();
                    aName = string.Concat(ua.FIRST_NAME, " ", ua.LAST_NAME);
                    aEmail = ua.EMAIL;
                    aPos = ua.POSITION_NAME;
                }
                if (aName.isEmpty())
                {
                    aName = Attorney;
                }
                if (gName.isEmpty())
                {
                    gName = Grantor;
                }
                IQueryable<vw_User> qu =
                    from usr in base.db.vw_User
                    where usr.USERNAME == userName
                    select usr;
                if (qu.Any<vw_User>())
                {
                    vw_User usend = qu.FirstOrDefault<vw_User>();
                    string.Concat(usend.FIRST_NAME, " ", usend.LAST_NAME);
                    string eMAIL = usend.EMAIL;
                }
                Filler f = t.Mark("Name,grantorName,grantorPos,attorneyName,attorneyPos,validFrom,validTo,NameEN,grantorNameEN,grantorPosEN,attorneyNameEN,attorneyPosEN,validFromEN,validToEN");
                using (SmtpClient client = new SmtpClient(this.SMTPSERVER))
                {
                    object[] objArray = new object[] { aName, gName, gPos, aName, aPos, validFrom.DATUM(), validTo.DATUM(), aName, gName, gPos, aName, aPos, validFrom.DATUM(), validTo.DATUM() };
                    f.Set(objArray);
                    MailMessage mailMessage = new MailMessage()
                    {
                        IsBodyHtml = true,
                        Body = t.Get(),
                        Sender = new MailAddress("ELVIS.ADMIN-NOREPLY@toyota.co.id"),
                        From = new MailAddress("ELVIS.ADMIN-NOREPLY@toyota.co.id"),
                        Subject = "ELVIS Power of Attorney"
                    };
                    MailMessage poaMail = mailMessage;
                    poaMail.To.Add(new MailAddress(aEmail));
                    client.Send(poaMail);
                    object[] objArray1 = new object[] { gName, gName, gPos, aName, aPos, validFrom.DATUM(), validTo.DATUM(), gName, gName, gPos, aName, aPos, validFrom.DATUM(), validTo.DATUM() };
                    f.Set(objArray1);
                    MailMessage mailMessage1 = new MailMessage()
                    {
                        IsBodyHtml = true,
                        Body = t.Get(),
                        Sender = new MailAddress("ELVIS.ADMIN-NOREPLY@toyota.co.id"),
                        From = new MailAddress("ELVIS.ADMIN-NOREPLY@toyota.co.id"),
                        Subject = "ELVIS Power of Attorney"
                    };
                    poaMail = mailMessage1;
                    poaMail.To.Add(new MailAddress(gEmail));
                    client.Send(poaMail);
                }
                Ok = true;
            }
            catch (Exception exception)
            {
                LoggingLogic.err(exception);
            }
            return Ok;
        }

        public bool ComposeUploadInvoice_EMAIL(UserData UserLogin, string[] mailTo, ErrorData Err, params object[] x)
        {
            object[] objArray = new object[] { (UserLogin != null ? UserLogin.USERNAME : "") };
            LoggingLogic.say("EmailFunction", "ComposeUploadInvoice_EMAIL({0},[]mailTo, err, []x)", objArray);
            bool kode = false;
            string MailBody = File.ReadAllText(this.MailPath);
            string[] strArrays = new string[] { "VENDOR_CD", "VENDOR_NAME", "INVOICE_DATE", "INVOICE_NO", "TRANSACTION_TYPE_NM", "DOC_LINK" };
            string[] a = strArrays;
            int max = ((int)x.Length > (int)a.Length ? (int)a.Length : (int)x.Length);
            for (int i = 0; i < max; i++)
            {
                string marker = string.Concat("[", a[i], "]");
                if (MailBody.Contains(marker))
                {
                    MailBody = MailBody.Replace(marker, Convert.ToString(x[i]));
                }
            }
            try
            {
                System.Net.Mail.MailMessage MailMessage = new System.Net.Mail.MailMessage()
                {
                    IsBodyHtml = true,
                    Body = MailBody
                };
                string Subject = base.Logic.Sys.GetText("INVOICE_UPLOAD_EMAIL_SUBJECT", "", "");
                if (string.IsNullOrEmpty(Subject))
                {
                    Subject = "[ELVIS] Ready to Convert Invoice Upload from {0}";
                }
                MailMessage.Subject = string.Format(Subject, string.Concat(UserLogin.FIRST_NAME, " ", UserLogin.LAST_NAME));
                MailAddress sender = new MailAddress(UserLogin.EMAIL);
                MailMessage.Sender = sender;
                MailMessage.From = sender;
                for (int i = 0; i < (int)mailTo.Length; i++)
                {
                    MailMessage.To.Add(mailTo[i]);
                }
                if (MailMessage.To == null || MailMessage.To.Count<MailAddress>() <= 0)
                {
                    kode = false;
                    Err = new ErrorData(1, "MSTD00031WRN", "Email to", null);
                }
                else
                {
                    using (SmtpClient client = new SmtpClient(this.SMTPSERVER))
                    {
                        client.Send(MailMessage);
                    }
                    kode = true;
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                kode = false;
                Err = new ErrorData(2, "MSTD00002ERR", ex.ToString(), null);
            }
            return kode;
        }

        public bool ComposeAccruedApproval_EMAIL(int moduleCd, string accrNo, string[] mailTo, string link, ref ErrorData Err)
        {
            object[] objLog = new object[] { moduleCd, accrNo};
            LoggingLogic.say("EmailFunction", "ComposeAccruedApproval_EMAIL({0}, {1}, Err)", objLog);
            bool result = false;
            try
            {
                string approver = "";
                string accrType = "";
                DateTime? accrDate = null;

                switch (moduleCd)
                {
                    case 3:
                        var ac = (from a in db.TB_R_ACCR_LIST_H
                                  where a.ACCRUED_NO == accrNo
                                  select a).FirstOrDefault();
                        approver = ac.NEXT_APPROVER;
                        accrDate = ac.CREATED_DT;
                        accrType = "Accrued";
                        break;
                    case 4:
                        var sn = (from a in db.TB_R_ACCR_SHIFTING_H
                                  where a.SHIFTING_NO == accrNo
                                  select a).FirstOrDefault();
                        approver = sn.NEXT_APPROVER;
                        accrDate = sn.CREATED_DT;
                        accrType = "Shifting";
                        break;
                    case 5:
                        var en = (from a in db.TB_R_ACCR_EXTEND_H
                                  where a.EXTEND_NO == accrNo
                                  select a).FirstOrDefault();
                        approver = en.NEXT_APPROVER;
                        accrDate = en.CREATED_DT;
                        accrType = "Extend";
                        break;
                    default: break;
                }

                TextReader reader = new StreamReader(this.MailPath);

                string MailBody = reader.ReadToEnd();
                if (MailBody.Contains("[DOC_APPROVER]"))
                {
                    MailBody = MailBody.Replace("[DOC_APPROVER]", approver);
                }
                if (MailBody.Contains("[DOC_TYPE]"))
                {
                    MailBody = MailBody.Replace("[DOC_TYPE]", accrType.str());
                }
                if (MailBody.Contains("[DOC_NO]"))
                {
                    MailBody = MailBody.Replace("[DOC_NO]", accrNo);
                }
                if (MailBody.Contains("[DOC_DATE]"))
                {
                    MailBody = MailBody.Replace("[DOC_DATE]", accrDate.Value.ToString("dd-MM-yyyy"));
                }
                if (MailBody.Contains("[DOC_LINK]"))
                {
                    MailBody = MailBody.Replace("[DOC_LINK]", link);
                }

                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage()
                {
                    IsBodyHtml = true,
                    Body = MailBody
                };

                System.Net.Mail.MailMessage MailMessage = mailMessage;
                foreach (string to in mailTo)
                {
                    MailMessage.To.Add(to);
                }

                string Subject = string.Format("[ELVIS] Accrued Appproval {0}", accrNo);
                MailMessage.Subject = Subject;
                MailAddress sender = new MailAddress(MailFrom);
                MailMessage.Sender = sender;
                MailMessage.From = sender;
                if (MailMessage.To == null || MailMessage.To.Count<MailAddress>() <= 0)
                {
                    result = false;
                    Err = new ErrorData(1, "MSTD00031WRN", "Email to", null);
                }
                else
                {
                    using (SmtpClient client = new SmtpClient(this.SMTPSERVER))
                    {
                        client.Send(MailMessage);
                    }
                    result = true;
                }
            }
            catch (Exception exception)
            {
                Exception ex = exception;
                result = false;
                Err = new ErrorData(2, "MSTD00002ERR", ex.ToString(), null);
                Handle(exception);
            }
            return result;
        }
    }
}