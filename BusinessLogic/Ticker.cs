﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Common; 

namespace BusinessLogic
{
    public class Ticker
    {
        public static readonly string TICK = "Ticker";

        public static ITick Get(int TickClass = -1)
        {
            ITick t = Heap.Get<ITick>("Ticker");
            if (t== null) 
            {
                switch (TickClass)
                {
                    case 0:
                        t = new IdleTick();
                        break;
                    case 1:
                        t = new InOutMark();
                        break;
                    case 2:
                        t = new TextTickels();
                        break;
                    default:
                        t = new TextTickels();
                        break;
                }
                t.Level = 0;
                Heap.Set<ITick>("Ticker", t, Heap.Duration * 1000);                    
            }
            return t;
        }

        public static ITick Tick
        {
            get
            {
                return Get(Common.AppSetting.TICK_CLASS);
            }
        }



        public static void In(string w, params object[] x)
        {
            Tick.In(w, x);
        }

        public static int Level
        {
            get
            {
                return Tick.Level;
            }
            set
            {
                Tick.Level = value;
            }
        }

        public static void Out(string w = null, params object[] x)
        {
            Tick.Out(w, x);
        }

        public static void Spin(string w, params object[] x)
        {
            Tick.Spin(w, x);
        }


        public static void Trace(string w)
        {
            Tick.Trace(w);
        }
    }
}
