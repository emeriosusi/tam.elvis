﻿using System;
using System.Collections.Generic;
using BusinessLogic;
using Common;
using Common.Data;
using DataLayer;
using SourceCode.Hosting.Client.BaseAPI;
using SourceCode.Workflow.Client;
using SourceCode.Workflow.Management;
using Action = SourceCode.Workflow.Client.Action;
using Destination = SourceCode.Workflow.Client.Destination;
using DestinationType = SourceCode.Workflow.Client.DestinationType;
using manInstance = SourceCode.Workflow.Management.ProcessInstance;
using ProcessInstance = SourceCode.Workflow.Client.ProcessInstance;
using ShareType = SourceCode.Workflow.Client.ShareType;
using UserStatuses = SourceCode.Workflow.Client.UserStatuses;
using WCCompare = SourceCode.Workflow.Client.WCCompare;
using WCField = SourceCode.Workflow.Client.WCField;
using WorklistCriteria = SourceCode.Workflow.Client.WorklistCriteria;
using WorklistItem = SourceCode.Workflow.Client.WorklistItem;
using WorklistShare = SourceCode.Workflow.Client.WorklistShare;
using WorkType = SourceCode.Workflow.Client.WorkType;
using Common.Function; 

namespace K2.Helper
{

    public class K2Helper : IWorkflow
    {
        #region field
        private uint Integrated;
        private String Server;
        private String User;
        private String Pass;
        private String Domain;
        private String Label;
        private uint WFPort;
        private uint SmOPort;
        private Connection Conn;
        #endregion

        #region Constructor
        public K2Helper(String server, String user, String pass, String domain, String label, uint wfport, uint smoport)
        {
            this.Integrated = Convert.ToUInt32(Common.AppSetting.K2_INTEGRATED.Int(0)); // fid.Hadid 20180528
            this.Server = server;
            this.User = user;
            this.Pass = pass;
            this.Domain = domain;
            this.Label = label;
            this.WFPort = wfport;
            this.SmOPort = smoport;
        }

        public K2Helper()
        {
            this.Integrated = Convert.ToUInt32(Common.AppSetting.K2_INTEGRATED.Int(0)); // fid.Hadid 20180528
            this.Server = Common.AppSetting.K2_SERVER;
            this.User = Common.AppSetting.K2_USER;
            this.Pass = Common.AppSetting.K2_PASS;
            this.Domain = Common.AppSetting.K2_DOMAIN;
            this.Label = Common.AppSetting.K2_LABEL;
            this.WFPort = Convert.ToUInt32(Common.AppSetting.K2_WORKFLOW_PORT);
            this.SmOPort = Convert.ToUInt32(Common.AppSetting.K2_SMO_PORT);
        }
        #endregion

        #region Method
        private String ConnectionSetup()
        {
            SCConnectionStringBuilder StrConn = new SCConnectionStringBuilder();
            StrConn.Authenticate = true;
            StrConn.Integrated = (Integrated != 0) ; // fid.Hadid 20180528
            StrConn.IsPrimaryLogin = true;
            StrConn.Host = Server;
            StrConn.SecurityLabelName = Label;
            StrConn.WindowsDomain = Domain;
            StrConn.Port = WFPort;
            if (User != "")
            {
                StrConn.UserID = User;
                StrConn.Password = Pass;
            }
            return StrConn.ConnectionString;
        }
        private String ConnectionWMSSetup()
        {
            SCConnectionStringBuilder StrConn = new SCConnectionStringBuilder();
            StrConn.Authenticate = true;
            StrConn.Integrated = (Integrated != 0 ); // fid.Hadid 20180528
            StrConn.IsPrimaryLogin = true;
            StrConn.Host = Server;
            StrConn.SecurityLabelName = Label;
            StrConn.WindowsDomain = Domain;
            StrConn.Port = SmOPort;
            if (User != "")
            {
                StrConn.UserID = User;
                StrConn.Password = Pass;
            }
            Ticker.In("k2 WMS open {0}", StrConn.ConnectionString);
            return StrConn.ConnectionString;
        }
        private ProcessInstance GetProcess(String Process)
        {
            try
            {

                ProcessInstance proc = Conn.CreateProcessInstance(Process);
                return proc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Open()
        {
            Conn = new Connection();
            string cose = ConnectionSetup();
            Ticker.In("k2 Open {0}", cose);
            Conn.Open(this.Server, cose);
        }

        public void Close()
        {
            Ticker.Out("k2 Close");
            if (Conn != null)
                Conn.Close();
        }

        public void Impersonate(String User)
        {
            Conn.ImpersonateUser(User);
        }

        public void Revert()
        {
            Conn.RevertUser();
        }

        public int GetProcID(String SN)
        {
            return Convert.ToInt32(SN.Split('_')[0]);
        }

        public Dictionary<String, String> GetDatafield(List<String> Keys, int procid)
        {
            Dictionary<String, String> Datafield = new Dictionary<string, string>();
            ProcessInstance proc = Conn.OpenProcessInstance(procid);
            for (int i = 0; i < Keys.Count; i++)
            {
                Datafield.Add(Keys[i], proc.DataFields[Keys[i]].Value.ToString());
            }
            return Datafield;
        }

        public void EditDatafield(Dictionary<String, String> datafield, int procid)
        {
            ProcessInstance proc = Conn.OpenProcessInstance(procid);
            foreach (String Keys in datafield.Keys)
            {
                proc.DataFields[Keys].Value = datafield[Keys];
            }
            proc.Update();
        }
        public void EditDatafield(Dictionary<String, object> datafield, int procid)
        {
            ProcessInstance proc = Conn.OpenProcessInstance(procid);
            foreach (String Keys in datafield.Keys)
            {
                proc.DataFields[Keys].Value = datafield[Keys];
            }
            proc.Update();
        }

        public void deleteprocess(string folio)
        {

            WorkflowManagementServer wms = new WorkflowManagementServer();
            wms.CreateConnection();
            wms.Open(ConnectionWMSSetup());

            ProcessInstances pi = wms.GetProcessInstances(folio);

            foreach (manInstance var in pi)
            {
                wms.DeleteProcessInstances(var.ID, false);
            }
            wms.Connection.Close();

        }
        public void deleteprocess(List<string> folioLIst)
        {

            WorkflowManagementServer wms = new WorkflowManagementServer();
            wms.CreateConnection();
            wms.Open(ConnectionWMSSetup());
            foreach (string folio in folioLIst)
            {
                ProcessInstances pi = wms.GetProcessInstances(folio);

                foreach (manInstance var in pi)
                {
                    wms.DeleteProcessInstances(var.ID, false);
                }
            }
            wms.Connection.Close();

        }
        public void deleteprocess(int ProcID)
        {

            WorkflowManagementServer wms = new WorkflowManagementServer();

            wms.Open(ConnectionSetup());

            ProcessInstances pi = wms.GetProcessInstances(ProcID);

            foreach (manInstance var in pi)
            {
                wms.DeleteProcessInstances(var.ID, false);
            }
            wms.Connection.Close();

        }

        public string StartProccessWithID(String Process, String folio, Dictionary<String, String> datafield)
        {
            try
            {
                //di comment dulu biar ga ke K2, K2 nya lagi di setting ama orang
                ProcessInstance proc = GetProcess(Process);
                foreach (String Keys in datafield.Keys)
                {
                    proc.DataFields[Keys].Value = datafield[Keys];
                }
                proc.Folio = folio;
                string procid = proc.ID.ToString();
                Conn.StartProcessInstance(proc);
                return procid;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string StartProccessWithID(String Process, String folio, Dictionary<String, object> datafield)
        {
            try
            {
                ProcessInstance proc = GetProcess(Process);
                foreach (String Keys in datafield.Keys)
                {
                    proc.DataFields[Keys].Value = datafield[Keys];
                }
                proc.Folio = folio;
                string procid = proc.ID.ToString();
                Conn.StartProcessInstance(proc);
                return procid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dictionary<string, string> GetManagedWorklist(string user)
        {
            Dictionary<string, string> worklist = new Dictionary<string, string>();

            try
            {
                Worklist K2WList = Conn.OpenWorklist("ASP", user);
                foreach (WorklistItem item in K2WList)
                {
                    if (!worklist.ContainsKey(item.ProcessInstance.Folio))
                        worklist.Add(item.ProcessInstance.Folio, item.SerialNumber);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return worklist;
        }

        public static string PlainUserName(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                int i = s.LastIndexOf("\\");
                if ((i > 0) && (i < s.Length))
                    s = s.Substring(i + 1).ToLower();
            }

            return s;
        }

        public List<WorklistHelper> GetWorklistComplete(string user)
        {
            List<WorklistHelper> _LWrklistHelper = new List<WorklistHelper>();
            try
            {
                Impersonate(user);
                Worklist K2WList = Conn.OpenWorklist();
                foreach (WorklistItem item in K2WList)
                {
                    WorklistHelper _Data = new WorklistHelper();
                    if (item.AllocatedUser.ToUpper() == Label.ToUpper() + ":" + (String.IsNullOrEmpty(Domain) ? "" : Domain.ToUpper() + "\\") + user.ToUpper())
                    {
                        _Data.Folio = item.ProcessInstance.Folio;
                        _Data.SN = item.SerialNumber;
                        _Data.FullName = item.ProcessInstance.FullName;
                        _Data.StartDate = item.EventInstance.StartDate;
                        _Data.UserID = PlainUserName(item.AllocatedUser);
                        _LWrklistHelper.Add(_Data);
                    }
                }
                Revert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _LWrklistHelper;
        }

        public List<WorklistHelper> GetAllWorklistComplete(string user)
        {
            return GetWorklistBy("user=" + user);
        }

        public Dictionary<string, string> GetWorklist(string user)
        {
            Dictionary<string, string> worklist = new Dictionary<string, string>();

            try
            {
                Ticker.In("GetWorkList(" + user + ")");
                Impersonate(user);
                Worklist K2WList = Conn.OpenWorklist();
                foreach (WorklistItem item in K2WList)
                {
                    if (item.AllocatedUser.ToUpper() == Label.ToUpper() + ":" + (String.IsNullOrEmpty(Domain) ? "" : Domain.ToUpper() + "\\") + user.ToUpper())
                    {
                        if (!worklist.ContainsKey(item.ProcessInstance.Folio))
                            worklist.Add(item.ProcessInstance.Folio, item.SerialNumber);
                    }
                }
                Revert();
                Ticker.Out("GetWorkList(" + user + ")");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return worklist;
        }


        public List<WorklistHelper> GetWorklistBy(string aCriteria)
        {
            List<WorklistHelper> w = new List<WorklistHelper>();
            string uid = "";
            Dictionary<string, string> kvdic = new Dictionary<string, string>();

            WorklistCriteria criteria = new WorklistCriteria();
            criteria.Platform = "ASP";
            int cCount = 0;

            string[] kv = aCriteria.Split(new string[] { ";", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < kv.Length; i++)
            {
                WCField field = WCField.None;
                string keyValue = kv[i];
                string[] ax = keyValue.Split(new string[] { "=" }, StringSplitOptions.None);
                string key = ax[0];
                string val = ax[1];
                if (string.IsNullOrEmpty(key))
                        continue;

                key = key.ToLower();
                kvdic.Add(key, val);

                if (key.Equals("user"))
                {
                    Revert();
                    uid = val;
                    Impersonate(uid);
                }
                else
                {
                    if (key.Equals("process"))
                        field = WCField.ProcessFullName;
                    if (key.Equals("folder"))
                        field = WCField.ProcessFolder;
                    else if (key.Equals("folio"))
                        field = WCField.ProcessFolio;
                    else if (key.Equals("startdate"))
                        field = WCField.ProcessStartDate;
                    else if (key.Equals("activityname"))
                        field = WCField.ActivityName;
                    else if (key.Equals("name"))
                        field = WCField.ProcessName;
                    else if (key.Equals("status"))
                        field = WCField.ProcessStatus;
                    else
                        field = WCField.None;

                    if (field != WCField.None)
                    {
                        ++cCount;
                        criteria.AddFilterField(field, WCCompare.Equal, val);
                    }
                }
            }

            if (!kvdic.ContainsKey("process")) // inject process name to prevent mistakes 
            { 
                criteria.AddFilterField(
                        WCField.ProcessFullName, 
                        WCCompare.Equal, 
                        AppSetting.K2_PROCESS_FULLNAME);
                cCount++;
            }
            Worklist K2WList;
            
            if (cCount > 0)
                K2WList = Conn.OpenWorklist(criteria);
            else
                K2WList = Conn.OpenWorklist();
            
            foreach (WorklistItem item in K2WList)
            {
                w.Add(new WorklistHelper()
                {
                    UserID = PlainUserName(item.AllocatedUser),
                    Folio = item.ProcessInstance.Folio,
                    FullName = item.ProcessInstance.FullName,
                    SN = item.SerialNumber,
                    StartDate = item.ProcessInstance.StartDate,
                });
            }
            if (!string.IsNullOrEmpty(uid))
                Revert();

            return w;
        }

        public List<WorklistHelper> GetWorklistFiltered(string user, string key, string value)
        {
            string criteria = "user=" + (user ?? "");
            if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value)) 
                criteria = criteria + ';' + key+ "=" + value ;
            return GetWorklistBy(criteria);
        }



        public Dictionary<string, string> GetWorklistByCriteria(string user, string property, string propertyvalue)
        {
            Dictionary<string, string> worklist = new Dictionary<string, string>();
            List<WorklistHelper> l = GetWorklistFiltered(user, property, propertyvalue);
            foreach (WorklistHelper w in l)
            {
                worklist.Add(w.Folio, w.SN);
            }
            return worklist;
        }

        public List<WorklistHelper> GetWorklistCompleteByProcess(string user, string processFullName)
        {
            List<WorklistHelper> _LWrklistHelper = new List<WorklistHelper>();
            try
            {
                Impersonate(user);
                if (string.IsNullOrEmpty(processFullName))
                    processFullName = AppSetting.K2_PROCESS_FULLNAME;

                // add criteria or filter worklist depend on process full name
                WorklistCriteria k2criteria = new WorklistCriteria();
                k2criteria.AddFilterField(WCField.ProcessFullName, WCCompare.Equal, processFullName);
                // open K2 worklist based on criteria
                Worklist K2WList = Conn.OpenWorklist(k2criteria);

                foreach (WorklistItem item in K2WList)
                {
                    WorklistHelper _Data = new WorklistHelper();
                    if (item.AllocatedUser.ToUpper() == Label.ToUpper() + ":" + (String.IsNullOrEmpty(Domain) ? "" : Domain.ToUpper() + "\\") + user.ToUpper())
                    {
                        _Data.Folio = item.ProcessInstance.Folio;
                        _Data.SN = item.SerialNumber;
                        _Data.FullName = item.ProcessInstance.FullName;
                        _Data.StartDate = item.EventInstance.StartDate;

                        _Data.UserID = PlainUserName(item.AllocatedUser);
                        _LWrklistHelper.Add(_Data);
                    }
                }
                Revert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _LWrklistHelper;
        }

        public void Action(String action, String sn)
        {
            try
            {
                WorklistItem wfItem = Conn.OpenWorklistItem(sn);
                wfItem.Actions[action].Execute();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Action(String action, String sn, String manageduser, String shareduser)
        {
            try
            {
                WorklistItem wfItem;
                if (shareduser == String.Empty && manageduser == String.Empty)
                    wfItem = Conn.OpenWorklistItem(sn, "ASP");
                else if (shareduser == String.Empty && manageduser != String.Empty)
                    // ' Check if the worklist item from a subordinate user
                    wfItem = Conn.OpenManagedWorklistItem(manageduser, sn);
                else
                    // ' Check if the worklist item is for an Out of Office shared user
                    wfItem = Conn.OpenSharedWorklistItem(shareduser, manageduser, sn);
                wfItem.Actions[action].Execute();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<String> GetAction(String sn)
        {
            try
            {
                List<String> actions = new List<string>();
                WorklistItem wfItem = Conn.OpenWorklistItem(sn);
                foreach (Action act in wfItem.Actions)
                {
                    actions.Add(act.Name);
                }
                return actions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<String> GetAction(String sn, string UserID)
        {
            try
            {
                Impersonate(UserID);
                List<String> actions = new List<string>();
                WorklistItem wfItem = Conn.OpenWorklistItem(sn, "ASP");
                foreach (Action act in wfItem.Actions)
                {
                    actions.Add(act.Name);
                }
                return actions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RedirectItem(String User, String sn)
        {
            try
            {
                WorklistItem wfItem = Conn.OpenWorklistItem(sn);
                //wfItem.Release();
                wfItem.Redirect(User);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SleepItem(Boolean sleep, String sn, int time)
        {
            try
            {
                WorklistItem wfItem = Conn.OpenWorklistItem(sn);
                if (time == 0)
                    wfItem.Sleep(sleep);
                else
                    wfItem.Sleep(sleep, time);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FinishItem(String sn)
        {
            try
            {
                ServerItem wfItem = Conn.OpenServerItem(sn);
                wfItem.Finish(); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*    public void RetrieveOOFUser()
            {
                WorkflowManagementServer wfServer = new WorkflowManagementServer();
                wfServer.CreateConnection();
                wfServer.Connection.Open(ConnectionSetup());
                SourceCode.Workflow.Management.OOF.Users OOFUsers = wfServer.GetUsers(ShareType.OOF);

                foreach (SourceCode.Workflow.Management.OOF.User OOFUser in OOFUsers)
                {
                    SourceCode.Workflow.Management.WorklistShares wlss = wfServer.GetCurrentSharingSettings(OOFUser.FQN, ShareType.OOF);
                    foreach (SourceCode.Workflow.Management.WorklistShare wls in wlss)
                    {
                        Console.WriteLine("User Name: " + OOFUser.FQN);
                        Console.WriteLine("Start Date: " + wls.StartDate.ToString());
                        Console.WriteLine("End Date: " + wls.EndDate.ToString());
                    }
                }
                if (wfServer != null)
                    if (!wfServer.Connection.IsConnected)
                        wfServer.Connection.Close();
            }*/

        public void SetUserstatus(String status)
        {
            UserStatuses statususer = new UserStatuses();

            if (status.ToLower() == "available")
                statususer = UserStatuses.Available;
            else
                if (status.ToLower() == "oof")
                    statususer = UserStatuses.OOF;
                else
                    statususer = UserStatuses.None;
            try
            {
                Conn.SetUserStatus(statususer);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String GetUserstatus(String User)
        {
            String status = string.Empty;
            try
            {
                UserStatuses statususer = Conn.GetUserStatus(User);
                switch (statususer)
                {
                    case UserStatuses.Available:
                        status = "Available";
                        break;
                    case UserStatuses.OOF:
                        status = "OOF";
                        break;
                    case UserStatuses.None:
                        status = "None";
                        break;
                }
                return status;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> Redirect(List<string> works, string fromUser, string toUser)
        {
            List<string> r = new List<string>();
            try
            {
                Impersonate(fromUser);
                Worklist w = Conn.OpenWorklist();

                foreach (WorklistItem i in w)
                {
                    if (works.Contains(i.ProcessInstance.Folio))
                    {
                        i.Redirect(toUser);
                        r.Add(i.ProcessInstance.Folio);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }
            finally
            {
                Revert();
            }
            return r;
        }

        /// <summary>
        /// Set Out Of Office status of a user (Out of Office or Available). Do not forget to impersonate before calling this method.
        /// </summary>
        /// <param name="Start">Start date of the status</param>
        /// <param name="DestinationUser">Active Directory user to delegate to</param>
        /// <param name="userStatus">0 - Out of Office; 1 - Available</param>
        public void ShareWorklist(DateTime Start, String DestinationUser, int userStatus)
        {
            UserStatuses uStatus = UserStatuses.Available;

            switch (userStatus)
            {
                case 0:
                    uStatus = UserStatuses.OOF;
                    break;
                case 1:
                    uStatus = UserStatuses.Available;
                    break;
                default:
                    uStatus = UserStatuses.Available;
                    break;
            }


            Destination dest = new Destination();
            WorklistShare wlShare = new WorklistShare();
            WorkType wType = new WorkType("BMSWorkType");

            dest.Name = DestinationUser;
            dest.DestinationType = DestinationType.User;

            wType.WorklistCriteria.Platform = "ASP";
            wType.Destinations.Add(dest);

            wlShare.ShareType = ShareType.OOF;
            wlShare.StartDate = Start;
            //wlShare.EndDate = End;
            wlShare.WorkTypes.Add(wType);

            Conn.ShareWorkList(wlShare);
            Conn.SetUserStatus(uStatus);
        }

        #endregion
    }


}
