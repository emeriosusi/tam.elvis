﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Common.Data;

namespace BusinessLogic.AccruedForm
{
    [Serializable]
    public class AccrFormHistory
    {
        public string ROLE_ID { get; set; }
        public string ROLE_NAME { get; set; }
        public string NAME { get; set; }
        public int STATUS { get; set; }
        public int STATUS_CD { get; set; }
        public string STATUS_NAME { get; set; }
        public byte CLASS { get; set; }
        public DateTime? ACTUAL_DT { get; set; }
    }
}