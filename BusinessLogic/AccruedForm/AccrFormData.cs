﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data;
using Common.Function;
using BusinessLogic.VoucherForm;

namespace BusinessLogic.AccruedForm
{
    [Serializable]
    public class AccrFormData
    {
        public const int EXTERNAL_VENDOR_GROUP_CODE = 3;
        public const string PAYMENT_METHOD_CASH = "C";
        public const string PAYMENT_METHOD_CHEQUE = "E";

        private UTF8Encoding stringEncoder;

        public string AccruedNo { get; set; }
        public string IssuingDiv { get; set; }
        public int? BudgetYear { get; set; }
        public DateTime? ChangedDt { get; set; }
        public DateTime CreatedDt { get; set; }
        public decimal TotalAmount { get; set; }
        public string SubmissionSts { get; set; }
        public string WorkflowSts { get; set; }
        public string NextApprover { get; set; }
        public string CurrApprover { get; set; }
        public int? CANCEL_FLAG { get; set; }


        public AccrFormData()
        {
            reset();
        }

        public void reset()
        {
            stringEncoder = new UTF8Encoding();
            lstNotes = new List<FormNote>();
            formTable = new AccrFormTable();
            //formTable.Data = this;
            attachmentTable = new AttachmentTable();
            PVNumber = null;
            PVYear = null;
            StatusCode = null;
            PaymentMethodCode = null;
            VendorGroupCode = null;
            VendorCode = null;
            PVTypeCode = null;
            TransactionCode = null;
            SuspenseNumber = null;
            BudgetNumber = null;
            ActivityDate = null;
            IssuingDiv = null;
            PVDate = null;
            TaxCode = null;
            TaxInvoiceNumber = null;
            Submitted = false;
            Editable = false;
            Editing = false;
            CanUploadDetail = false;
            // OpenHeaderAnyway = false;
            OpenEmpty = true;
            FormIsValid = true;
            PostK2ByWorklistChecking = false;
            
            AttachmentRequired = false;
            //SubmissionFailed = false;
            Budgeted = false;
            EntryCostCenter = true;
            Saved = false;
            BudgetChecked = false;
            PostedK2 = false;
            isSubmit = false;
            ModuleCd = 3;
        }
        public void copyStatus(AccrFormData source)
        {
            // OpenHeaderAnyway = source.OpenHeaderAnyway;
            OpenEmpty = source.OpenEmpty;
            OpenExisting = source.OpenExisting;
            Submitted = source.Submitted;
            FormIsValid = source.FormIsValid;
            PostK2ByWorklistChecking = source.PostK2ByWorklistChecking;
            Editing = source.Editing;
            Editable = source.Editable;
            AttachmentRequired = source.AttachmentRequired;
            //SubmissionFailed = source.SubmissionFailed;
            Budgeted = source.Budgeted;            
            EntryCostCenter = source.EntryCostCenter;
            //added by Akhmad nuryanto
            isResubmitting = source.isResubmitting;
            Saved = source.Saved;
            BudgetChecked = source.BudgetChecked;
            PostedK2 = source.PostedK2;
            UserName = source.UserName;
        }

        public bool Editable { get; set; }
        public bool Editing { get; set; }
        public bool Saved { get; set; } 
        public bool CanUploadDetail { get; set; }
        //public bool OpenHeaderAnyway { get; set; }
        private bool openEmpty;
        public bool OpenEmpty
        {
            get
            {
                return openEmpty;
            }
            set
            {
                openEmpty = value;
                openExisting = !openEmpty;
                Editable = openExisting;
            }
        }
        private bool openExisting;
        public bool OpenExisting
        {
            get
            {
                return openExisting;
            }
            set
            {
                openExisting = value;
                openEmpty = !openExisting;
                // Editing = !value;
            }
        }
        public bool Submitted { get; set; }
        public bool FormIsValid { get; set; }
        public bool PostedK2 { get; set; }         
        public bool PostK2ByWorklistChecking { get; set; }
        // public List<int?> EntertainmentTransactionTypes { get; set; }

        public string LoggedUserRole { get; set; }
        public string UserName { get; set; }

        private TransactionType _transactionType;
        public TransactionType _TransactionType
        {
          
            get
            {
                return _transactionType;
            }
            set
            {
                _transactionType = value;
                if (_transactionType != null)
                {
                    Budgeted = _transactionType.BudgetFlag;
                    AttachmentRequired = _transactionType.AttachmentFlag;
                    EntryCostCenter = _transactionType.CostCenterFlag;
                }
            }
        }

        private AttachmentTable attachmentTable;
        public AttachmentTable AttachmentTable
        {
            get
            {
                return attachmentTable;
            }
         
        }

        private AccrFormTable formTable;
        public AccrFormTable FormTable
        {
            get
            {
                return formTable;
            }
        }

        public bool OnHold { get; set; }
        public string HoldingUser { get; set; }
        private List<FormNote> lstNotes = new List<FormNote>();
        public List<FormNote> Notes
        {
            get
            {
                return lstNotes;
            }
        }
        public string K2Command { get; set; }
        public List<AccrFormDetail> Details
        {
            get
            {
                return formTable.DataList;
            }
            set
            {
                formTable.DataList = value;
            }
        }
        public int ModuleCd { get; set; }

        public int? _pvNumber = null;
        public int? PVNumber
        {
            get
            {
                return _pvNumber.HasValue ? _pvNumber.Value : 0;
            }
            set
            {
                _pvNumber = value;
            }
        }
        public int? _pvYear = null;
        public int? PVYear
        {
            get
            {
                return _pvYear.HasValue ? _pvYear.Value : 0;
            }
            set
            {
                _pvYear = value;
            }
        }
        public int? StatusCode { get; set; }
        public string PaymentMethodCode { get; set; }
        public int? VendorGroupCode { get; set; }
        public string VendorCode { get; set; }
        public int? PVTypeCode { get; set; }

        private int? transactionCode;
        public int? TransactionCode
        {
          
            get
            {
                return transactionCode;
            }
            set
            {
                transactionCode = value;
            }
        }
        public int? SuspenseNumber { get; set; }
        public int? SuspenseYear { get; set; } 

        private string _BudgetNumber = "";

        public string BudgetNumber 
        {
            get
            {
                return _BudgetNumber;
            }
            set
            {
                _BudgetNumber = value;
                if (wbsDetailCount == 1)
                {
                    for (int i = 0; i < Details.Count; i++)
                    {
                        Details[i].WbsNumber = _BudgetNumber;
                    }
                }
            }
        }
        //fid.Hadid 20180406
        public string BookingNo { get; set; }
        public string ReffNo { get; set; }
        //fid.Hadid
        public DateTime? ActivityDate { get; set; }
        public DateTime? ActivityDateTo { get; set; }
        public DateTime? PVDate { get; set; }
        public string TaxCode { get; set; }
        public string TaxInvoiceNumber { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? PlanningPaymentDate { get; set; }
        public int? BankType { get; set; }
        public bool Canceled { get; set; }
        public bool Deleted { get; set; } 
        public decimal? ReferenceNo { get; set; }
        public DateTime? SubmitHC { get; set; }
        public int? WorkflowStatus { get; set; } 

        public bool SingleWbsUsed { get; set; }
        public bool TaxCalculated { get; set; }
        public bool AttachmentRequired { get; set; }
        //public bool SubmissionFailed { get; set; }
        public bool Budgeted { get; set; }
        public bool BudgetChecked { get; set; } 
        public bool EntryCostCenter { get; set; }
        public bool InWorklist { get; set; }
        public bool isSubmit { get; set; } 
        //addded by Akhmad Nuryanto, flag for resubmitting (after rejection)
        public bool isResubmitting { get; set; }       
        //end of addition by Akhmad Nuryanto

        public int wbsDetailCount
        {
            get
            {
                List<string> wbes = (from d in Details
                                     select d.WbsNumber).Distinct().ToList();
                return (wbes != null) ? wbes.Count : 0;                
            }
        }

        public int? HOLD_FLAG { get; set; }
        public int? COUNTER_FLAG { get; set; } 
        //public int DOC_CD { get { return DocCd; } }
        //public string ScreenType { get { return (DocCd == 1) ? "PV" : "RV"; } }

        public static string check(bool b, string n = "")
        {
            return "[" + (b ? "v" : " ") + "]" 
                    + (!n.isEmpty()? " " + n :"");
        }
        public static string limit(string s, int len)
        {
            if (s.isEmpty()) return s;

            if (s.Length <= len)
            {
                return s;
            }
            else
            {
                return s.Substring(0, len-3) + ">>>";
            }
        }

        public string sColBox(string separator, int[] len)
        {
            StringBuilder a = new StringBuilder();
            for (int i = 0; i < len.Length; i++)
            {
                if (i > 0) a.Append(separator);
                a.Append("{" + i.ToString() + 
                    ((len[i]!=0)? ":" + len[i].ToString(): "") + 
                    "}");
            }
            return a.ToString();
        }

        public string dump()
        {
            StringBuilder s = new StringBuilder("FormData " 
                + "\r\n\r\n");

            string title = "  #|From|Seq |ItemTRN|cc________|cc name_________________________________|" +
                           "FROM|CURR|              Amount|  GL Account|" +
                           "Description_______________________________________|" +
                           "Wording___________________________________________|" +
                           "invoice        |" +
                           "SAP item|Saved  ";
            string[] titles = title.Split('|');

            
            StringBuilder xx = new StringBuilder("");
            for (int x = 0; x < titles.Length; x++)
            {
                if (x > 0) xx.Append("|");
                int width = titles[x].Length;
                if (titles[x].Contains("_")) {
                    width = width * -1;
                }
                xx.Append("{" + x.str() + "," + width.str() + "}");
            }
            xx.Append("\r\n");
            title = title.Replace("_", " ");
            string dform = xx.ToString();

            s.AppendFormat("{0,2} # {1,10} {2,4}\r\n", "ACCR", (PVNumber != null) ? PVNumber.str() : "<null>", (PVYear != null) ? PVYear.str() : "??");
            s.AppendFormat("Date : {0} Div: {1} Status: {2}\r\n"
                , (PVDate != null) ? PVDate.ISOdate(): "<null>"
                , IssuingDiv ?? "??"
                , (StatusCode != null) ? StatusCode.str() : "??");

            s.AppendFormat("Type: {0} ActivityDate: {1} to {2}  Payment: {3}\r\n"
                , (PVTypeCode != null) ? PVTypeCode.str() : "?"
                , (ActivityDate != null) ? ActivityDate. ISOdate(): "??"
                , (ActivityDateTo != null) ? ActivityDateTo.ISOdate() : "??"
                , PaymentMethodCode ?? "??");
            s.AppendFormat("Outbound: {0} Group: {1} Reference#: {2}\r\n"
                , VendorCode ?? "?"
                , (VendorGroupCode != null) ? VendorGroupCode.str() : "?"
                , ReferenceNo);
            s.AppendFormat("TType: {0} BudgetNumber: [{1}]\r\n"
                , (TransactionCode != null) ? TransactionCode.str() : "?"
                , BudgetNumber ?? "?"
                );
            s.AppendFormat("BankType: {0}\r\n", BankType);
            
            int di = 0;
            s.AppendLine("\r\n" + title);

            foreach (AccrFormDetail d in Details)
            {
                s.AppendFormat(dform,
                    ++di                             
                    , d.FromSeq                      
                    , d.SequenceNumber               
                    , d.ItemTransaction              
                    , d.CostCenterCode ?? "?"        
                    , limit(d.CostCenterName,40)     
                    , d.FromCurr                     
                    , d.CurrencyCode                 
                    , CommonFunction.Eval_Curr(d.CurrencyCode, d.Amount)                       
                    , d.GlAccount                    
                    , limit(d.Description, 50)       
                    , limit(d.StandardDescriptionWording, 50)   
                    , d.InvoiceNumber                
                    , d.ItemNo                       
                    , d.Persisted.box(" ")           
                    );              
            }
            s.AppendLine("---");
            s.AppendFormat("K2 Command: {0} \r\n", K2Command);
            
            s.AppendLine(("|"
                + InWorklist            .box("InWorklist          ") + " "
                + OnHold                .box("OnHold              ") + "|"

                + TaxCalculated         .box("TaxCalculated       ") + " "
                + SingleWbsUsed         .box("SingleWbsUsed       ") + " "
                
                + Budgeted              .box("Budgeted            ") + " "
                + BudgetChecked         .box("BudgetChecked       ") + "|"                  
                

                + CanUploadDetail       .box("CanUploadDetail     ") + " "
                + AttachmentRequired    .box("AttachmentRequired  ") + " " 
                + EntryCostCenter       .box("EntryCostCenter     ") + " "
                + FormIsValid           .box("FormIsValid         ") + "|"

                //+ OpenHeaderAnyway      .box("OpenHeaderAnyway    ") + " "
                + OpenEmpty             .box("OpenEmpty           ") + " "
                + OpenExisting          .box("OpenExisting        ") + "|" 

                + Editable              .box("Editable            ") + " " 
                + Editing               .box("Editing             ") + " "                
                + Saved                 .box("Saved               ") + "|"

                + isSubmit              .box("isSubmit            ") + " "
                + Submitted             .box("Submitted           ") + "|"

                + PostedK2              .box("PostedK2            ") + " "  
                + PostK2ByWorklistChecking
                                        .box("PostK2ByWorklistChecking")  
                ).Replace("|", "\r\n\t"));

            s.AppendLine("\r\n{" + UserName + "}\r\n");
            return s.ToString();
        }

    }
}