﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using Common.Function;

namespace BusinessLogic.AccruedForm
{
    public class AccruedSearchCriteria
    {
        public string issuingDivision, accrNo;
        public int submissionSts, budgetYear;
        public short workflowSts;
        public decimal totAmtFrom, totAmtTo;
        public DateTime createdDtFrom, createdDtTo;
        public bool ignoreAccruedNo, ignoreIssuingDivision
            , ignoreDateFrom, ignoreDateTo, ignoreAmt
            , ignoreNumEndsWith, ignoreNumStartsWith, ignoreNumContains;
        public string[] divs;

        public string sItemPre, sItemPost, sAccrPre, sAccrPost;
        public int iAno;

        private void init()
        {
            issuingDivision = "";
            accrNo = "";

            submissionSts = -1;
            budgetYear = -1;
            workflowSts = -1;
            
            totAmtFrom = 0;
            totAmtTo = 0;

            createdDtFrom = new DateTime();
            createdDtTo = new DateTime();
            
            ignoreAccruedNo = true;
            ignoreAccruedNo = true;
            ignoreIssuingDivision = true;
            ignoreDateFrom = true;
            ignoreDateTo = true;
            ignoreAmt = true;
            ignoreNumEndsWith = true;
            ignoreNumStartsWith = true;
            ignoreNumContains = true;
        }

        public AccruedSearchCriteria()
        {
            init();
        }
        public AccruedSearchCriteria(string _accrNo)
        {
            init();

            ignoreAccruedNo = _accrNo.isEmpty();
            accrNo = _accrNo;

            if (!_accrNo.isEmpty())
            {
                iAno = LikeLogic.ignoreWhat(_accrNo, ref sAccrPre, ref sAccrPost);
            }
        }

        public AccruedSearchCriteria(
            string _issuingDivision, string _accrNo,
            string _submissionSts, string _workflowSts,
            string _budgetYear, string _totAmtFrom, string _totAmtTo,
            string _createdDtFrom, string _createdDtTo)
        {
            //default ignore all searching criteria
            init();

            #region build searching criteria

            if (!_issuingDivision.isEmpty()) //   !string.IsNullOrEmpty(_issuingDivision) && Convert.ToInt16(_issuingDivision) != 0)
            {
                ignoreIssuingDivision = false;

                issuingDivision = _issuingDivision;

                if (!issuingDivision.isEmpty() && issuingDivision.Contains(";"))
                {
                    divs = issuingDivision.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                }
            }

            if (!string.IsNullOrEmpty(_createdDtFrom))
            {
                ignoreDateFrom = false;
                createdDtFrom = DateTime.ParseExact(_createdDtFrom, "dd/MM/yyyy", null);
            }
            if (!string.IsNullOrEmpty(_createdDtTo))
            {
                ignoreDateTo = false;
                createdDtTo = DateTime.ParseExact(_createdDtTo + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
            }

            if (!_totAmtFrom.isEmpty() || !_totAmtTo.isEmpty())
            {
                if (_totAmtFrom.IndexOf("*") < 0 && _totAmtTo.IndexOf("*") < 0)
                {
                    ignoreAmt = false;
                    if (!_totAmtFrom.isEmpty()) totAmtFrom = _totAmtFrom.Int(-1); else totAmtFrom = -1;
                    if (!_totAmtTo.isEmpty()) totAmtTo = _totAmtTo.Int(-1); else totAmtTo = -1;
                    if (totAmtTo < 0 && totAmtFrom > 0)
                        totAmtTo = totAmtFrom;
                    else if (totAmtFrom < 0 && totAmtTo > 0)
                        totAmtFrom = totAmtTo;
                    else if (totAmtFrom < 0 && totAmtTo < 0)
                        ignoreAmt = true;
                }
                else
                {
                    string sNo = (_totAmtFrom.isEmpty()) ? _totAmtTo : _totAmtFrom;

                    int iItem = LikeLogic.ignoreWhat(_totAmtFrom, ref sItemPre, ref sItemPost);
                    switch (iItem)
                    {
                        case LikeLogic.igFIRST:
                            ignoreNumEndsWith = false;
                            break;
                        case LikeLogic.igLAST:
                            ignoreNumStartsWith = false;
                            break;
                        case LikeLogic.igFIRSTLAST:
                            ignoreNumContains = false;
                            break;
                        case LikeLogic.igMIDDLE:
                            ignoreNumEndsWith = false;
                            ignoreNumStartsWith = false;
                            break;
                        default: break;
                    }
                }
            }

            if (string.IsNullOrEmpty(_workflowSts) || !Int16.TryParse(_workflowSts, out workflowSts))
            {
                workflowSts = -1;
            }

            if (string.IsNullOrEmpty(_submissionSts) || !Int32.TryParse(_submissionSts, out submissionSts))
            {
                submissionSts = -1;
            }

            if (string.IsNullOrEmpty(_budgetYear) || !Int32.TryParse(_budgetYear, out budgetYear))
            {
                budgetYear = -1;
            }

            ignoreAccruedNo = _accrNo.isEmpty();
            accrNo = _accrNo;

            if (!_accrNo.isEmpty())
            {
                iAno = LikeLogic.ignoreWhat(_accrNo, ref sAccrPre, ref sAccrPost);                    
            }

            #endregion

        }
    }
}
