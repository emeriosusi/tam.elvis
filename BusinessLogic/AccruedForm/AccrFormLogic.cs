﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.IO;
using System.Linq;
using BusinessLogic.CommonLogic;
using Common;
using Common.Enum;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;
using BusinessLogic.VoucherForm;
using Common.Data._80Accrued;
using System.Data.SqlClient;
using System.Data.Objects.SqlClient;
using Dapper;

namespace BusinessLogic.AccruedForm
{
    public class AccrFormLogic : FormPersistence
    {
        private const string ACCR_NO_SEP = "-";
        private const char SAP_DOC_SEP = ';';

        readonly List<CodeConstant> docTypes = CommonData.pvTypeCodes;
        public override List<CodeConstant> getDocTypes()
        {
            return CommonData.AccrPVTypes;
        }

        public string getAccrNo(string divisionId, int budgetYear)
        {
            string newAccrNo = "";
            try {
                if (!String.IsNullOrEmpty(divisionId) && budgetYear > 0)
                {
                    var q = (from h in db.TB_R_ACCR_LIST_H
                             orderby h.ACCRUED_NO.Substring(h.ACCRUED_NO.Length - 2) descending
                             where h.DIVISION_ID == divisionId
                                && h.BUDGET_YEAR == budgetYear
                             select new
                             {
                                 h.ACCRUED_NO
                                 , h.BUDGET_YEAR
                                 , SEQUENCE = h.ACCRUED_NO.Substring(h.ACCRUED_NO.Length - 2)
                             }).FirstOrDefault();

                    var divName = logic.Vendor.GetDivisionName(divisionId);

                    int newSeq = 1;
                    if (q != null)
                    {
                        newSeq = (Convert.ToInt32(q.SEQUENCE) + 1);
                    }

                    newAccrNo = String.Format("LA{0}{1}{2}{3}{4}{5}"
                            , ACCR_NO_SEP
                            , divName
                            , ACCR_NO_SEP
                            , budgetYear
                            , ACCR_NO_SEP
                            , newSeq.str().PadLeft(2, '0'));
                }
            }
            catch(Exception e)
            {
                Handle(e);
            }

            return newAccrNo;
        }

        public override string getDocType(int? code)
        {
            string coda = (code ?? 0).str();
            return CommonData.AccrPVTypes
                .Where(a => a.Code == coda)
                .Select(b => b.Description)
                .FirstOrDefault();
        }

        public int getDocumentNo(string uid = "")
        {
            return ((int)GetDocNo(1, uid));
        }

        protected List<AccrFormDetail> GetDetail(AccrFormData f)
        {
            return GetListDetail(f);
        }

        public List<AccrFormDetail> GetListDetail(AccrFormData f)
        {
            decimal div = Convert.ToDecimal(f.IssuingDiv);
            var list = (from d in db.TB_R_ACCR_LIST_D
                     join b in db.TB_R_ACCR_BALANCE
                     on d.WBS_NO_OLD equals b.WBS_NO_OLD into dbal
                     from b in dbal.DefaultIfEmpty()
                     join w in db.vw_WBS
                     on d.WBS_NO_OLD equals w.WbsNumber into dw
                     from w in dw.DefaultIfEmpty()
                     join t in db.TB_M_ACCR_PV_TYPE
                     on d.PV_TYPE_CD equals t.PV_TYPE_CD
                     where d.ACCRUED_NO == f.AccruedNo
                        && w.Division == div
                     orderby d.WBS_NO_OLD, d.PV_TYPE_CD, d.ACTIVITY_DES
                     select new AccrFormDetail {
                            BookingNo = d.BOOKING_NO,
                            //BookingNo = d.BOOKING_NO ?? b.BOOKING_NO,
                            WbsNumber = d.WBS_NO_OLD,
                            WbsDesc = w.Description,
                            SuspenseNo = SqlFunctions.StringConvert((decimal)d.SUSPENSE_NO_OLD).Trim(),
                            PVType = t.PV_TYPE_NAME,
                            PVTypeCd = d.PV_TYPE_CD,
                            ActivityDescription = d.ACTIVITY_DES,
                            CostCenterCode = d.COST_CENTER,
                            CurrencyCode = d.CURRENCY_CD,
                            Amount = d.AMOUNT ?? 0,
                            AmountIdr = d.AMOUNT_IDR ?? 0,
                            Persisted = true
                        }
                     ).Distinct().ToList();

            list.Sort(AccrFormDetail.RuleOrder);

            // set sequence number
            list = list.Select((x, seq) => {
                x.SequenceNumber = seq;
                x.DisplaySequenceNumber = seq + 1;
                return x;
            }).ToList();

            if (f.StatusCode != 104 && f.StatusCode != 105)
                FillSAPAmount(list);

            return list;
        }

        public void FillSAPAmount(List<AccrFormDetail> Details, string UserName = "")
        {
            if (Details == null || Details.Count == 0)
                return;

            List<string> budgetNo = Details
                                    .Where(x => !x.WbsNumber.isEmpty())
                                    .Select(x => x.WbsNumber)
                                    .Distinct()
                                    .ToList();

            if (budgetNo == null || budgetNo.Count <= 0) return;

            var rem = logic.SAP.BudgetRemaining(budgetNo, UserName);

            //if (rem == null || rem.Count <= 0) return;

            for (int i = 0; i < Details.Count; i++)
            {
                var d = Details[i];

                if (i == 0)
                {
                    decimal? sapAmt = rem
                                    .Where(x => x.BUDGET_NO.Equals(d.WbsNumber))
                                    .Select(x => x.REMAINING_AMT).FirstOrDefault();

                    d.SapAmtAvailable = sapAmt ?? 0;
                }
                else
                {
                    var p = Details[i - 1];
                    if (!d.WbsNumber.Equals(p.WbsNumber))
                    {
                        decimal? sapAmt = rem
                                            .Where(x => x.BUDGET_NO.Equals(d.WbsNumber))
                                            .Select(x => x.REMAINING_AMT).FirstOrDefault();

                        d.SapAmtAvailable = sapAmt ?? 0;
                    }
                    else
                        d.SapAmtAvailable = p.SapAmtRemaining;
                }

                d.SapAmtRemaining = d.SapAmtAvailable - d.AmountIdr;
            }
        }

        public string GetStatusName(int statusCd)
        {
            return (from h in db.TB_M_STATUS
                   where h.STATUS_CD == statusCd
                   select h.STATUS_NAME).FirstOrDefault();
        }

        public override AccrFormData search(string refNo)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var query = db.TB_R_ACCR_LIST_H
                            .Where(t => t.ACCRUED_NO == refNo);

                var result = query.ToList();
                if (result.Any())
                {
                    AccrFormData data = new AccrFormData();
                    var d = result[0];

                    var cnapp = (from c in db.vw_CurNextApprover
                                 join a in db.TB_R_ACCR_DOC_NO on c.REFF_NO equals a.ACCR_DOC_NO
                                 where a.ACCR_NO == d.ACCRUED_NO
                                 select c).FirstOrDefault();

                    data.AccruedNo = d.ACCRUED_NO;
                    data.ReffNo = getAccrReffNo(d.ACCRUED_NO);
                    data.TotalAmount = d.TOTAL_AMT != null ? d.TOTAL_AMT.Value : 0;
                    data.ChangedDt = d.CHANGED_DT ?? d.CREATED_DT;
                    data.CreatedDt = d.CREATED_DT;
                    data.SubmissionSts = GetStatusName(d.STATUS_CD.Value);
                    data.CurrApprover = (cnapp != null ? cnapp.CURRENT_APPROVER : "");
                    data.NextApprover = (cnapp != null ? cnapp.NEXT_APPROVER : "");
                    data.StatusCode = d.STATUS_CD ?? 0;
                    data.IssuingDiv = d.DIVISION_ID;
                    data.BudgetYear = d.BUDGET_YEAR;
                    data.FormTable.DataList = GetDetail(data);
                    data.CANCEL_FLAG = d.CANCEL_FLAG != null ? d.CANCEL_FLAG.Value : 0;

                    data.UserName = UserName;
                    return data;
                }
            }

            return null;
        }
        public string getAccrReffNo(string accrNo)
        {
            var r = (from d in db.TB_R_ACCR_DOC_NO
                    where d.ACCR_NO == accrNo
                    select d).FirstOrDefault();

            if (r == null) return "";
            return r.ACCR_DOC_NO.str();
        }
        public bool AssignBookingNo(AccrFormData formData, UserData u)
        {
            bool isSuccess = true;
            try
            {
                //reset booking no first
                formData.Details = formData.Details.Select(x => { x.BookingNo = null; return x; }).ToList();

                //Ambil booking no susulan. sorry me english is not gud
                formData.Details = formData.Details
                                    .Select(x => {
                                        if(x.BookingNo.isEmpty())
                                            x.BookingNo = getAppendixBookingNo(x, u.DIV_CD.str());
                                        return x;
                                    }).ToList();

                //Create new Booking no
                var grouping = formData.Details
                                .Where(x => x.BookingNo.isEmpty() && x.PVTypeCd != 2)
                                .Select(x => new { WBS_NO = x.WbsNumber, PV_TYPE = x.PVTypeCd })
                                .Distinct()
                                .ToList();

                string groupBookingNo = "";
                int seqBookingNo = -1;

                if (grouping.Any())
                {
                    foreach (var g in grouping)
                    {
                        groupBookingNo = createNewBookingNo(ref seqBookingNo, u);
                        formData.Details = formData.Details
                                        .Select(x =>
                                            {
                                                if (x.BookingNo.isEmpty())
                                                    if (x.WbsNumber.Equals(g.WBS_NO) && x.PVTypeCd == g.PV_TYPE)
                                                        x.BookingNo = groupBookingNo;
                                                    else if (x.PVTypeCd == 2)
                                                        x.BookingNo = createNewBookingNo(ref seqBookingNo, u);

                                                return x;
                                            }).ToList();
                    }
                }
                else
                {
                    // if only suspense
                    formData.Details = formData.Details
                                    .Select(x =>
                                    {
                                        if (x.BookingNo.isEmpty())
                                            x.BookingNo = createNewBookingNo(ref seqBookingNo, u);

                                        return x;
                                    }).ToList();
                }

            }
            catch (Exception e)
            {
                isSuccess = false;
                LoggingLogic.err(e);
            }

            return isSuccess;
        }

        private string getAppendixBookingNo(AccrFormDetail det, string divId)
        {
            // if suspense, no need to get appendix booking no
            if (det.PVTypeCd == 2)
                return null;

            var bal = (from b in db.TB_R_ACCR_BALANCE
                     join d in db.TB_R_ACCR_LIST_D
                     on b.WBS_NO_OLD equals d.WBS_NO_OLD
                     join h in db.TB_R_ACCR_LIST_H
                     on d.ACCRUED_NO equals h.ACCRUED_NO
                     where h.STATUS_CD == 105
                        && b.DIVISION_ID == divId
                        && b.WBS_NO_OLD.Equals(det.WbsNumber)
                        && b.PV_TYPE_CD == det.PVTypeCd
                     select b).FirstOrDefault();

            if (bal != null)
            {
                return bal.BOOKING_NO;
            }

            return null;
        }
        private string createNewBookingNo(ref int seq, UserData u)
        {
            string prefix = string.Format("BN-{0}-{1}-"
                                            , u.DIV_NAME
                                            //, DateTime.Now.Year - 1); // cuman buat testing tar ilangin -1 nya
                                            , DateTime.Now.Year);

            if (seq == -1)
            {
                seq = 0;
                var maxSeqStr = db.TB_R_ACCR_BALANCE
                                .Where(x => x.BOOKING_NO.StartsWith(prefix))
                                .Select(x => new { SEQ = x.BOOKING_NO.Replace(prefix, "") })
                                .ToList();

                if (maxSeqStr != null && maxSeqStr.Count() > 0)
                {
                    seq = maxSeqStr.Select(x => Int32.Parse(x.SEQ)).Max();
                    //Int32.TryParse(maxSeqStr, out seq);
                }
            }

            seq++;
            return string.Format("{0}{1}", prefix, seq.str().PadLeft(3, '0'));
        }

        private int CreateAccrReffNo(string AccrNo, int ModuleCd, ELVIS_DBEntities DB)
        {
            if (DB == null)
                DB = db;

            int ct = logic.Sys.GetNum("COUNTER_ID_ACCRUED/" + ModuleCd, 0);
            ct++;

            string mcstr = ModuleCd.str();
            var sys = (from m in DB.TB_M_SYSTEM
                      where m.SYSTEM_TYPE == "COUNTER_ID_ACCRUED"
                        && m.SYSTEM_CD == mcstr
                      select m).FirstOrDefault();

            if (sys != null)
            {
                sys.SYSTEM_VALUE_NUM = ct;
                sys.CHANGED_BY = "SYSTEM";
                sys.CHANGED_DT = DateTime.Now;
            }

            DB.SaveChanges();

            int pref = 0;
            switch (ModuleCd)
            {
                case 3: pref = 1; break;
                case 4: pref = 2; break;
                case 5: pref = 3; break;
                case 6: pref = 4; break;
                default: pref = 0; break;
            }

            string ReffNo = string.Format("{0}{1}", pref, ct.str().PadLeft(7, '0'));

            return ReffNo.Int();
        }
        public void InsertAccrReffNo(string AccruedNo, int ModuleCd, ELVIS_DBEntities DB = null)
        {
            if (DB == null)
                DB = db;

            var existing = from doc in DB.TB_R_ACCR_DOC_NO 
                           where doc.ACCR_NO == AccruedNo
                           select doc;

            if (existing.Any())
                return;

            int ReffNo = CreateAccrReffNo(AccruedNo, ModuleCd, DB);

            TB_R_ACCR_DOC_NO d = new TB_R_ACCR_DOC_NO();
            d.ACCR_NO = AccruedNo;
            d.ACCR_DOC_NO = ReffNo;
            d.DOC_CD = ModuleCd;
            DB.TB_R_ACCR_DOC_NO.AddObject(d);

            DB.SaveChanges();
        }
        public void DeleteAccrReffNo(string AccruedNo, ELVIS_DBEntities DB = null)
        {
            if (DB == null)
                DB = db;

            var re = (from r in DB.TB_R_ACCR_DOC_NO
                      where r.ACCR_NO == AccruedNo
                      select r).ToList();
            if (re.Any())
            {
                foreach (var r in re)
                {
                    DB.DeleteObject(r);
                    DB.SaveChanges();
                }
            }
        }

        public bool saveHeader(AccrFormData formData, UserData userData, ELVIS_DBEntities DB)
        {
            bool headerSaved = false;
            TB_R_ACCR_LIST_H h = null;
            try
            {
                bool added = false;
                string _accrNo = formData.AccruedNo;

                var existingHeaders = (from t in DB.TB_R_ACCR_LIST_H
                                       where t.ACCRUED_NO == _accrNo
                                       select t).ToList();


                if (existingHeaders != null && existingHeaders.Any())
                {
                    h = existingHeaders[0];
                }
                else
                {
                    added = true;
                    h = new TB_R_ACCR_LIST_H();
                    h.ACCRUED_NO = formData.AccruedNo;
                    //h.BUDGET_YEAR = DateTime.Now.Year - 1; // cuman buat testing tar ilangin -1 nyaa
                    h.BUDGET_YEAR = DateTime.Now.Year;
                    h.DIVISION_ID = userData.DIV_CD.ToString();
                    DB.TB_R_ACCR_LIST_H.AddObject(h);
                }

                h.TOTAL_AMT = formData.Details.Sum(x => x.AmountIdr);
                h.STATUS_CD = formData.StatusCode ?? 0;

                if (added)
                {
                    h.CREATED_DT = today;
                    h.CREATED_BY = userData.USERNAME;
                }
                else
                {
                    h.CHANGED_DT = today;
                    h.CHANGED_BY = userData.USERNAME;
                }

                if (formData.isSubmit)
                {
                    //h.CANCEL_FLAG = 0;
                }

                DB.SaveChanges();

                headerSaved = true;
            }
            catch (Exception ex)
            {
                headerSaved = false;
                LoggingLogic.err(ex);
                throw;
            }
            return headerSaved;
        }

        public void saveDetail(AccrFormData formData, UserData u, ELVIS_DBEntities DB, DbTransaction TX)
        {
            var existingDetails =
                   (from t in DB.TB_R_ACCR_LIST_D
                    where (t.ACCRUED_NO == formData.AccruedNo)
                    select t).ToList();

            /* Delete record if none matched in the memory */
            existingDetails.Where(x => !formData.Details.Any(y => x.SEQ_NO == y.SequenceNumber));
            foreach (var toBeDel in existingDetails)
            {
                DB.TB_R_ACCR_LIST_D.DeleteObject(toBeDel);
                DB.SaveChanges();
            }

            /* Insert & Update Form Detail */
            foreach (AccrFormDetail detail in formData.Details)
            {
                var q = (from trald in db.TB_R_ACCR_LIST_D
                         where trald.SEQ_NO == detail.SequenceNumber && trald.ACCRUED_NO == detail.AccruedNo
                         select new AccrFormDetail()
                         {
                             AccruedNo = trald.ACCRUED_NO,
                             SequenceNumber = trald.SEQ_NO != null ? trald.SEQ_NO.Value : -1,
                             WbsNumber = trald.WBS_NO_OLD,
                             ActivityDescription = trald.ACTIVITY_DES,
                             PVTypeCd = trald.PV_TYPE_CD,
                             CostCenterCode = trald.COST_CENTER,
                             CurrencyCode = trald.CURRENCY_CD,
                             Amount = trald.AMOUNT != null ? trald.AMOUNT.Value : 0,
                             AmountIdr = trald.AMOUNT_IDR != null ? trald.AMOUNT.Value : 0,
                             SuspenseNoOld = trald.SUSPENSE_NO_OLD != null ? trald.SUSPENSE_NO_OLD.Value : 0,
                             ChangedBy = u.USERNAME,
                             ChangedDt = DateTime.Now
                         }).Distinct().ToList();

                if (q != null)
                {
                    TB_R_ACCR_LIST_D d = new TB_R_ACCR_LIST_D();
                    d.ACCRUED_NO = formData.AccruedNo;
                    d.SEQ_NO = detail.SequenceNumber;
                    d.BOOKING_NO = detail.BookingNo;
                    d.WBS_NO_OLD = detail.WbsNumber;
                    d.ACTIVITY_DES = detail.ActivityDescription;
                    d.PV_TYPE_CD = detail.PVTypeCd != null ? detail.PVTypeCd.Value : 0;
                    d.COST_CENTER = detail.CostCenterCode;
                    d.CURRENCY_CD = detail.CurrencyCode;
                    d.AMOUNT = detail.Amount;
                    d.AMOUNT_IDR = detail.AmountIdr;
                    d.SUSPENSE_NO_OLD = !detail.SuspenseNo.isEmpty() ? Convert.ToInt32(detail.SuspenseNo) : (int?)null;
                    d.CREATED_BY = u.USERNAME;
                    d.CREATED_DT = DateTime.Now;

                    DB.TB_R_ACCR_LIST_D.AddObject(d);
                    DB.SaveChanges();
                }
                else
                {
                    foreach (var qq in q)
                    {
                        qq.AccruedNo = formData.AccruedNo;
                        qq.SequenceNumber = detail.SequenceNumber;
                        qq.WbsNumber = detail.WbsNumber;
                        qq.ActivityDescription = detail.ActivityDescription;
                        qq.PVTypeCd = detail.PVTypeCd;
                        qq.CostCenterCode = detail.CostCenterCode;
                        qq.CurrencyCode = detail.CurrencyCode;
                        qq.Amount = detail.Amount;
                        qq.AmountIdr = detail.AmountIdr;
                        qq.SuspenseNoOld = detail.SuspenseNoOld;
                        qq.ChangedBy = u.USERNAME;
                        qq.ChangedDt = DateTime.Now;
                    }

                    DB.SaveChanges();
                }

                //Qx<string>("InputUpdateAccrForm", new
                //{
                //    AccruedNo = formData.AccruedNo,
                //    SequenceNumber = detail.SequenceNumber,
                //    BookingNo = detail.BookingNo,
                //    WbsNumber = detail.WbsNumber,
                //    ActivityDescription = detail.ActivityDescription,
                //    PVTypeCd = detail.PVTypeCd,
                //    CostCenterCode = detail.CostCenterCode,
                //    CurrencyCode = detail.CurrencyCode,
                //    Amount = (detail.CurrencyCode == "IDR" ? Math.Round(detail.Amount) : detail.Amount),
                //    AmountIdr = Math.Round(detail.AmountIdr),
                //    SuspenseNo = !detail.SuspenseNo.isEmpty() ? Convert.ToInt32(detail.SuspenseNo) : (int?)null,
                //    Username = u.USERNAME
                //});
            }
            DB.SaveChanges();
        }

        //public void delExistingDetail(AccrFormData formData, UserData u, ELVIS_DBEntities DB)
        //{
        //    var existingDetails =
        //           (from t in DB.TB_R_ACCR_LIST_D
        //            where (t.ACCRUED_NO == formData.AccruedNo)
        //            select t).ToList();

        //    if (!existingDetails.Any()) return;
        //    var lstDeleted = new List<TB_R_ACCR_LIST_D>();

        //    /* Update existing records */
        //    bool matched = false;
        //    foreach (TB_R_ACCR_LIST_D d in existingDetails)
        //    {
        //        matched = false;
        //        foreach (AccrFormDetail detail in formData.Details)
        //        {
        //            if (detail.SequenceNumber == d.SEQ_NO)
        //            {
        //                d.WBS_NO_OLD = detail.WbsNumber;
        //                d.ACTIVITY_DES = detail.ActivityDescription;
        //                d.PV_TYPE_CD = detail.PVTypeCd.HasValue ? detail.PVTypeCd.Value : 0;
        //                d.COST_CENTER = detail.CostCenterCode;
        //                d.CURRENCY_CD = detail.CurrencyCode;
        //                d.AMOUNT = (detail.CurrencyCode == "IDR" ? Math.Round(detail.Amount) : detail.Amount);
        //                d.AMOUNT_IDR = Math.Round(detail.AmountIdr);
        //                d.SUSPENSE_NO_OLD = !detail.SuspenseNo.isEmpty() ? Convert.ToInt32(detail.SuspenseNo) : (int?)null;
        //                d.CHANGED_DT = today;
        //                d.CHANGED_BY = u.USERNAME;

        //                detail.Persisted = true;
        //                matched = true;

        //                break;
        //            }
        //        }

        //        /* Delete record if none matched in the memory */
        //        if (!matched)
        //        {
        //            DB.TB_R_ACCR_LIST_D.DeleteObject(d);
        //            DB.SaveChanges();
        //        }
        //    }
        //}

        //public void saveDetail(AccrFormData formData, UserData userData, ELVIS_DBEntities DB)
        //{
        //    foreach (AccrFormDetail d in formData.Details)
        //    {
        //        if (!d.Persisted)
        //        {
        //            TB_R_ACCR_LIST_D x = db.TB_R_ACCR_LIST_D
        //                .Where(a => a.ACCRUED_NO == formData.AccruedNo
        //                    && a.SEQ_NO == d.SequenceNumber)
        //                .FirstOrDefault();
        //            if (x == null)
        //            {
        //                x = new TB_R_ACCR_LIST_D()
        //                {
        //                    ACCRUED_NO = formData.AccruedNo,
        //                    SEQ_NO = d.SequenceNumber,
        //                    WBS_NO_OLD = d.WbsNumber,
        //                    ACTIVITY_DES = d.ActivityDescription,
        //                    PV_TYPE_CD = d.PVTypeCd.HasValue ? d.PVTypeCd.Value : 0,
        //                    COST_CENTER = d.CostCenterCode,
        //                    CURRENCY_CD = d.CurrencyCode,
        //                    AMOUNT = (d.CurrencyCode == "IDR" ? Math.Round(d.Amount) : d.Amount),
        //                    AMOUNT_IDR = Math.Round(d.AmountIdr),
        //                    SUSPENSE_NO_OLD = !d.SuspenseNo.isEmpty() ? Convert.ToInt32(d.SuspenseNo) : (int?)null,
        //                    CREATED_BY = userData.USERNAME,
        //                    CREATED_DT = today
        //                };
        //                DB.TB_R_ACCR_LIST_D.AddObject(x);
        //            }
        //            else
        //            {
                        
        //                x.WBS_NO_OLD = d.WbsNumber;
        //                x.ACTIVITY_DES = d.ActivityDescription;
        //                x.PV_TYPE_CD = d.PVTypeCd.HasValue ? d.PVTypeCd.Value : 0;
        //                x.COST_CENTER = d.CostCenterCode;
        //                x.CURRENCY_CD = d.CurrencyCode;
        //                x.AMOUNT = (d.CurrencyCode == "IDR" ? Math.Round(d.Amount) : d.Amount);
        //                x.AMOUNT_IDR = Math.Round(d.AmountIdr);
        //                x.SUSPENSE_NO_OLD = d.SuspenseNo.isEmpty() ? Convert.ToInt32(d.SuspenseNo) : (int?)null;
        //                x.CHANGED_BY = userData.USERNAME;
        //                x.CHANGED_DT = today;
        //            }

        //            DB.SaveChanges();
        //            d.Persisted = true;
        //        }
        //    }

        //    /* Detail History */
        //    if (formData.isSubmit)
        //    {
        //        //saveHistoryDetail(formData, userData, version, DB);
        //        DB.SaveChanges();
        //    }
        //}

        public override bool save(AccrFormData formData, UserData userData)
        {
            bool Ok = true;
            today = DateTime.Now;

            base.save(formData, userData);
            /* Header */
            DbTransaction TX = null;
            using (ContextWrap eco = new ContextWrap())
            {
                ELVIS_DBEntities db = eco.db;

                try
                {
                    TX = db.Connection.BeginTransaction();

                    AssignBookingNo(formData, userData);
                    Ok = SaveData(db, TX, formData, userData);

                    if (Ok)
                        TX.Commit();
                }
                catch (Exception ex)
                {
                    TX.Rollback();
                    LoggingLogic.err(ex);
                    Ok = false;
                }
            }
            return Ok;
        }

        public bool SaveData(
            ELVIS_DBEntities DB, DbTransaction TX,
            AccrFormData formData, UserData userData)
        {
            bool Ok = false;

            Ok = saveHeader(formData, userData, DB);

            logi = string.Format("{0}", formData.AccruedNo);
            Util.ReMoveFile(Path.Combine(LoggingLogic.GetPath(), logi + ".log"));

            /* Detail */
            //delExistingDetail(formData, userData, DB);

            saveDetail(formData, userData, DB, TX);

            InsertAccrReffNo(formData.AccruedNo, 3, DB);

            DB.SaveChanges();

            return Ok;
        }

        public void syncBudgetNo(PVFormData f, List<PVFormDetail> lstDetail)
        {
            bool syncBudgetNo = (f.BudgetNumber != null) && (!f.BudgetNumber.Equals(""));
            List<string> lstCheckedWbsNo = new List<string>();
            foreach (PVFormDetail d in lstDetail)
            {
                if (!string.IsNullOrEmpty(d.WbsNumber))
                {
                    if (!lstCheckedWbsNo.Contains(d.WbsNumber))
                    {
                        syncBudgetNo = true;
                        break;
                    }
                }
            }
        }

        #region Simulation
        private AccrFormSimulationData GetHeaderSimulationData(String refNo)
        {
            return (from h in db.TB_R_ACCR_LIST_H
                    where h.ACCRUED_NO == refNo
                    select new AccrFormSimulationData
                    {
                        KEYS = h.ACCRUED_NO
                        , ACCRUED_NO = h.ACCRUED_NO
                        , TRANSACTION_NAME = "Accrued Year End"
                        , DOC_DATE = h.CREATED_DT
                        , REF_DOC = h.ACCRUED_NO
                        , CURRENCY_CODE = "IDR"
                        , COMPANY_CODE = "TAM"
                        , POSTING_DATE = DateTime.Now
                        , WITHOLDING_TAX = "0"
                        , FISCAL_YEAR = h.BUDGET_YEAR.HasValue ? h.BUDGET_YEAR.Value : 0
                        , PERIOD = DateTime.Now.Month
                    }).FirstOrDefault();
        }

        private List<AccrFormSimulationData> GetSuspenseHeaderSimulationData(String refNo, bool isPaid)
        { 
            var ret = (from h in db.TB_R_ACCR_LIST_H
                        join d in db.TB_R_ACCR_LIST_D on h.ACCRUED_NO equals d.ACCRUED_NO
                        join p in db.TB_R_PV_H on d.SUSPENSE_NO_OLD equals p.PV_NO
                        join s in db.TB_R_SAP_DOC_NO on new { NO = p.PV_NO, YY = p.PV_YEAR } equals new { NO = s.DOC_NO, YY = s.DOC_YEAR } into sp
                        from s in sp.DefaultIfEmpty()
                        where h.ACCRUED_NO == refNo 
                            && d.PV_TYPE_CD == 2
                            && s.ITEM_NO == 1 //only retrieve 1 item
                            //&& ((isPaid && p.STATUS_CD == 28)
                            //    || (!isPaid && p.STATUS_CD >= 10 && p.STATUS_CD <= 27))
                        select new AccrFormSimulationData
                        {
                            KEYS = h.ACCRUED_NO + "|" + ( d.SUSPENSE_NO_OLD.HasValue ? SqlFunctions.StringConvert((decimal)d.SUSPENSE_NO_OLD.Value).Trim() : "")
                            , ACCRUED_NO = h.ACCRUED_NO
                            , TRANSACTION_NAME = "Accrued Year End"
                            , DOC_DATE = h.CREATED_DT
                            //, REF_DOC = d.SUSPENSE_NO_OLD.HasValue ? SqlFunctions.StringConvert((decimal)d.SUSPENSE_NO_OLD.Value).Trim() : ""
                            , REF_DOC = "Suspense " + (isPaid ? "Paid" : "Unpaid")
                            , REF_DOC_YEAR = SqlFunctions.StringConvert((decimal)p.PV_YEAR).Trim()
                            , CURRENCY_CODE = "IDR"
                            , COMPANY_CODE = "TAM"
                            , POSTING_DATE = DateTime.Now
                            , WITHOLDING_TAX = "0"
                            , FISCAL_YEAR = h.BUDGET_YEAR.HasValue ? h.BUDGET_YEAR.Value : 0
                            , PERIOD = DateTime.Now.Month
                            , BOOKING_NO = d.BOOKING_NO
                            , SUSPENSE_NO = d.SUSPENSE_NO_OLD
                            , WBS_NO = d.WBS_NO_OLD
                            , COST_CENTER = d.COST_CENTER
                            , SAP_DOC_NO = s.SAP_DOC_NO
                            , SAP_DOC_YEAR = s.SAP_DOC_YEAR
                        }).ToList();

            if (ret == null)
                return null;

            var comp = logic.Sys.GetSystemMaster("COMPANY_INFO").FirstOrDefault();
            if (comp != null)
            {
                ret = ret.Select(x => 
                    {
                        x.COMPANY_CD = comp.ValueNum.str();
                        x.COMPANY_CODE = comp.ValueTxt;
                        return x; 
                    }).ToList();
            }

            logic.SAP.SuspenseCheckPaid(ret);

            string chkPaid = isPaid ? AccrFormSimulationData.SUSPAID : AccrFormSimulationData.SUSUNPAID;

            return ret.Where(x => (x.PAID_STS ?? "").Equals(chkPaid)).ToList();
        }

        private List<AccrFormSimulationData> GetBalHeaderSimulationData(List<string> refNo, ELVIS_DBEntities DB)
        {
            return (from bal in DB.TB_R_ACCR_BALANCE
                    join div in DB.vw_Division on bal.DIVISION_ID equals div.DIVISION_ID
                    where refNo.Contains(bal.BOOKING_NO)
                    group div by div.DIVISION_NAME into g
                    select new AccrFormSimulationData
                    {   KEYS = g.Max(x => x.DIVISION_ID)
                        , TRANS_TYPE = "Closing Accrued"
                        , DOC_DATE = DateTime.Now
                        , REF_DOC = g.Key
                        , DIVISION_NAME = g.Key
                        , DOC_CURRENCY = "IDR"
                        , COMPANY_CD = "TAM"
                        , POSTED_DATE = DateTime.Now
                        , WITHOLDING_TAX = "0"
                        , FISCAL_YEAR = DateTime.Now.Year
                        , PERIOD = DateTime.Now.Month
                    }).ToList();
        }

        public List<AccrFormSimulationData> GetPRSimulationData(String refNo)
        {
            AccrFormSimulationData simulationData = null;

            try
            {
                // header
                simulationData = GetHeaderSimulationData(refNo);
                simulationData.REF_DOC = "PR";

                if (simulationData == null) return null;

                // detail transaction
                var q = (from d in db.TB_R_ACCR_LIST_D
                                join a in db.vw_WBS_ACCOUNT on d.WBS_NO_OLD equals a.WBS_NO into da
                                from a in da.DefaultIfEmpty()
                                where d.ACCRUED_NO == refNo && d.PV_TYPE_CD == 4
                                select new 
                                {
                                    ACCOUNT = a.GLACCOUNT.HasValue ? SqlFunctions.StringConvert((decimal)a.GLACCOUNT.Value).Trim() : ""
                                    , ACCOUNT_SHORT = a.SHORT_TEXT
                                    , AMOUNT = d.AMOUNT_IDR
                                    , TX = "V0"
                                    , TEXT = d.BOOKING_NO
                                    , WBS_NO = d.WBS_NO_OLD
                                    , COST_CENTER = d.COST_CENTER
                                });

                if (!q.Any()) return null;

                int i = 1;
                decimal totAmount = 0;
                simulationData.Details = new List<AccrFormSimulationDetail>();

                foreach(var d in q)
                {
                    simulationData.Details.Add(new AccrFormSimulationDetail()
                    {
                        SEQ_NO = i++
                        , ACCOUNT = d.ACCOUNT ?? ""
                        , ACCOUNT_SHORT = d.ACCOUNT_SHORT ?? ""
                        , TX = d.TX ?? ""
                        , AMOUNT = string.Format("{0:#,#}", d.AMOUNT) ?? ""
                        , AMOUNT_DEC = d.AMOUNT ?? 0
                        , TEXT = d.TEXT ?? ""
                        , WBS_NO = d.WBS_NO ?? ""
                        , COST_CENTER = d.COST_CENTER ?? ""
                    });

                    totAmount += d.AMOUNT ?? 0;
                }

                // counter transaction
                simulationData.Details.Add(new AccrFormSimulationDetail()
                {
                    SEQ_NO = i++
                    , ACCOUNT = logic.Sys.GetNum("ACCR.POST.LA.DIRECT/CREDIT", 0).str() ?? ""
                    , ACCOUNT_SHORT = logic.Sys.GetText("ACCR.POST.LA.DIRECT", "CREDIT", "") ?? ""
                    , AMOUNT = string.Format("{0:#,#}", totAmount * -1) ?? ""
                    , AMOUNT_DEC = totAmount * -1
                    , TX = ""
                    , TEXT = ""
                    , WBS_NO = ""
                    , COST_CENTER = ""
                });
            }
            catch (Exception e)
            {
                Handle(e);
                return null;
            }
            
            return new List<AccrFormSimulationData>() { simulationData };
        }
        public List<AccrFormSimulationData> GetDirectSimulationData(String refNo)
        {
            AccrFormSimulationData simulationData = null;

            try
            {
                // header
                simulationData = GetHeaderSimulationData(refNo);
                simulationData.REF_DOC = "Direct";

                if (simulationData == null) return null;

                // detail transaction
                var q = (from d in db.TB_R_ACCR_LIST_D
                                join a in db.vw_WBS_ACCOUNT on d.WBS_NO_OLD equals a.WBS_NO into da
                                from a in da.DefaultIfEmpty()
                                where d.ACCRUED_NO == refNo && d.PV_TYPE_CD == 1
                                select new 
                                {
                                    ACCOUNT = a.GLACCOUNT.HasValue ? SqlFunctions.StringConvert((decimal)a.GLACCOUNT).Trim() : ""
                                    , ACCOUNT_SHORT = a.SHORT_TEXT
                                    , AMOUNT = d.AMOUNT_IDR
                                    , TX = "V0"
                                    , TEXT = d.BOOKING_NO
                                    , WBS_NO = d.WBS_NO_OLD
                                    , COST_CENTER = d.COST_CENTER
                                });
                    
                if (!q.Any()) return null;

                int i = 1;
                decimal totAmount = 0;
                simulationData.Details = new List<AccrFormSimulationDetail>();

                foreach(var d in q)
                {
                    simulationData.Details.Add(new AccrFormSimulationDetail()
                    {
                        SEQ_NO = i++
                        , ACCOUNT = d.ACCOUNT ?? ""
                        , ACCOUNT_SHORT = d.ACCOUNT_SHORT ?? ""
                        , TX = d.TX ?? ""
                        , AMOUNT = string.Format("{0:#,#}", d.AMOUNT) ?? ""
                        , AMOUNT_DEC = d.AMOUNT ?? 0
                        , TEXT = d.TEXT ?? ""
                        , WBS_NO = d.WBS_NO ?? ""
                        , COST_CENTER = d.COST_CENTER ?? ""
                    });

                    totAmount += d.AMOUNT ?? 0;
                }

                // counter transaction
                simulationData.Details.Add(new AccrFormSimulationDetail()
                {
                    SEQ_NO = i++
                    , ACCOUNT = logic.Sys.GetNum("ACCR.POST.LA.DIRECT/CREDIT", 0).str() ?? ""
                    , ACCOUNT_SHORT = logic.Sys.GetText("ACCR.POST.LA.DIRECT", "CREDIT", "") ?? ""
                    , AMOUNT = string.Format("{0:#,#}", totAmount * -1) ?? ""
                    , AMOUNT_DEC = totAmount * -1
                    , TX = ""
                    , TEXT = ""
                    , WBS_NO = ""
                    , COST_CENTER = ""
                });
            }
            catch (Exception e)
            {
                Handle(e);
                return null;
            }

            return new List<AccrFormSimulationData>() { simulationData };
        }
        public List<AccrFormSimulationData> GetSuspenseSimulationData(String refNo, bool isPaid)
        {
            List<AccrFormSimulationData> listSimulation = null;

            try
            {
                // header
                listSimulation = GetSuspenseHeaderSimulationData(refNo, isPaid);

                if (listSimulation == null || listSimulation.Count <= 0) return null;

                // detail transaction
                foreach (var simulationData in listSimulation)
                {
                    var q = (from d in db.TB_R_ACCR_LIST_D
                                join ph in db.TB_R_PV_H on d.SUSPENSE_NO_OLD equals (int?)ph.PV_NO
                                join v in db.vw_Vendor on ph.VENDOR_CD equals v.VENDOR_CD into vw
                                from v in vw.DefaultIfEmpty()
                                where d.ACCRUED_NO == simulationData.ACCRUED_NO 
                                    && d.BOOKING_NO == simulationData.BOOKING_NO
                                    && d.PV_TYPE_CD == 2
                                    select new 
                                    {
                                        ACCOUNT = ph.VENDOR_CD
                                        , ACCOUNT_SHORT = v.VENDOR_NAME
                                        , AMOUNT = d.AMOUNT_IDR
                                        , TX = "V0"
                                        , TEXT = d.BOOKING_NO
                                        , WBS_NO = d.WBS_NO_OLD
                                        , COST_CENTER = d.COST_CENTER
                                    });

                    if (!q.Any()) continue;

                    decimal totAmount = q.Sum(x => x.AMOUNT ?? 0);

                    simulationData.Details = new List<AccrFormSimulationDetail>();

                    // get first row only
                    //q = q.Where(x => x.SEQ_NO == 1);

                    string AccountF = "", AccountShortF = "", AccountS = "", AccountShortS = "", AccountT = "", AccountShortT = "";
                    if (isPaid)
                    {
                        #region Detail Suspense Paid
                        AccountF = logic.Sys.GetNum("ACCR.POST.LA.SUSPENSE.PAID/DEBIT", 0).str();
                        AccountShortF = logic.Sys.GetText("ACCR.POST.LA.SUSPENSE.PAID", "DEBIT", "");
                        AccountS = logic.Sys.GetNum("ACCR.POST.LA.SUSPENSE.PAID/CREDIT", 0).str();
                        AccountShortS = logic.Sys.GetText("ACCR.POST.LA.SUSPENSE.PAID", "CREDIT", "");

                        // first block
                        int i = 1;
                        simulationData.Details.Add(new AccrFormSimulationDetail()
                        {
                            SEQ_NO = i++
                            , ACCOUNT = AccountF ?? ""
                            , ACCOUNT_SHORT = AccountShortF ?? ""
                            , TX = ""
                            , AMOUNT = string.Format("{0:#,#}", totAmount) ?? ""
                            , AMOUNT_DEC = totAmount
                            , TEXT = ""
                            , dataMode = DataSAPMode.GL
                        });

                        foreach (var d in q)
                        { 
                            simulationData.Details.Add(new AccrFormSimulationDetail()
                            {
                                SEQ_NO = i++
                                , ACCOUNT = d.ACCOUNT ?? ""
                                , ACCOUNT_SHORT = d.ACCOUNT_SHORT ?? ""
                                , TX = "V0"
                                , AMOUNT = string.Format("{0:#,#}", d.AMOUNT * -1) ?? ""
                                , AMOUNT_DEC = d.AMOUNT.HasValue ? d.AMOUNT.Value * -1 : 0
                                , TEXT = d.TEXT ?? ""
                                , dataMode = DataSAPMode.Vendor
                            });
                        }

                        // second block
                        i = 1;
                        foreach (var d in q)
                        { 
                            simulationData.Details.Add(new AccrFormSimulationDetail()
                            {
                                SEQ_NO = i++
                                , ACCOUNT = d.ACCOUNT ?? ""
                                , ACCOUNT_SHORT = d.ACCOUNT_SHORT ?? ""
                                , TX = "V0"
                                , AMOUNT = string.Format("{0:#,#}", d.AMOUNT) ?? ""
                                , AMOUNT_DEC = d.AMOUNT.HasValue ? d.AMOUNT.Value : 0
                                , TEXT = d.TEXT ?? ""
                                , dataMode = DataSAPMode.Vendor
                                , WBS_NO = d.WBS_NO ?? ""
                                , COST_CENTER = d.COST_CENTER ?? ""
                            });
                        }

                        simulationData.Details.Add(new AccrFormSimulationDetail()
                        {
                            SEQ_NO = i++
                            , ACCOUNT = AccountS ?? ""
                            , ACCOUNT_SHORT = AccountShortS ?? ""
                            , TX = ""
                            , AMOUNT = string.Format("{0:#,#}", totAmount * -1) ?? ""
                            , AMOUNT_DEC = totAmount * -1
                            , TEXT = ""
                            , dataMode = DataSAPMode.GL
                        });
                        #endregion
                    }
                    else
                    {
                        #region Detail Suspense Not Paid

                        AccountF = logic.Sys.GetNum("ACCR.POST.LA.SUSPENSE.UNPAID2/CREDIT", 0).str();
                        AccountShortF = logic.Sys.GetText("ACCR.POST.LA.SUSPENSE.UNPAID2", "CREDIT", "");
                        AccountS = logic.Sys.GetNum("ACCR.POST.LA.SUSPENSE.UNPAID/CREDIT", 0).str();
                        AccountShortS = logic.Sys.GetText("ACCR.POST.LA.SUSPENSE.UNPAID", "CREDIT", "");
                        AccountT = logic.Sys.GetNum("ACCR.POST.LA.SUSPENSE.UNPAID/DEBIT", 0).str();
                        AccountShortT = logic.Sys.GetText("ACCR.POST.LA.SUSPENSE.UNPAID", "DEBIT", "");

                        // first block
                        int i = 1;
                        foreach (var d in q)
                        { 
                            simulationData.Details.Add(new AccrFormSimulationDetail()
                            {
                                SEQ_NO = i++
                                , ACCOUNT = d.ACCOUNT ?? ""
                                , ACCOUNT_SHORT = d.ACCOUNT_SHORT ?? ""
                                , TX = "V0"
                                , AMOUNT = string.Format("{0:#,#}", d.AMOUNT * -1) ?? ""
                                , AMOUNT_DEC = d.AMOUNT.HasValue ? d.AMOUNT.Value * -1 : 0
                                , TEXT = d.TEXT ?? ""
                                , dataMode = DataSAPMode.Vendor
                            });
                        }

                        simulationData.Details.Add(new AccrFormSimulationDetail()
                        {
                            SEQ_NO = i++
                            , ACCOUNT = AccountF ?? ""
                            , ACCOUNT_SHORT = AccountShortF ?? ""
                            , TX = ""
                            , AMOUNT = string.Format("{0:#,#}", totAmount) ?? ""
                            , AMOUNT_DEC = totAmount
                            , TEXT = ""
                            , dataMode = DataSAPMode.GL
                        });

                        // second block
                        i = 1;
                        foreach (var d in q)
                        { 
                            simulationData.Details.Add(new AccrFormSimulationDetail()
                            {
                                SEQ_NO = i++
                                , ACCOUNT = d.ACCOUNT ?? ""
                                , ACCOUNT_SHORT = d.ACCOUNT_SHORT ?? ""
                                , TX = "V0"
                                , AMOUNT = string.Format("{0:#,#}", d.AMOUNT) ?? ""
                                , AMOUNT_DEC = d.AMOUNT.HasValue ? d.AMOUNT.Value : 0
                                , TEXT = d.TEXT ?? ""
                                , dataMode = DataSAPMode.Vendor
                                , WBS_NO = d.WBS_NO ?? ""
                                , COST_CENTER = d.COST_CENTER ?? ""
                            });
                        }

                        simulationData.Details.Add(new AccrFormSimulationDetail()
                        {
                            SEQ_NO = i++
                            , ACCOUNT = AccountS ?? ""
                            , ACCOUNT_SHORT = AccountShortS ?? ""
                            , TX = ""
                            , AMOUNT = string.Format("{0:#,#}", totAmount * -1) ?? ""
                            , AMOUNT_DEC = totAmount * -1
                            , TEXT = ""
                            , dataMode = DataSAPMode.GL
                        });

                        // third block
                        i = 1;
                        simulationData.Details.Add(new AccrFormSimulationDetail()
                        {
                            SEQ_NO = i++
                            , ACCOUNT = AccountT ?? ""
                            , ACCOUNT_SHORT = AccountShortT ?? ""
                            , TX = ""
                            , AMOUNT = string.Format("{0:#,#}", totAmount) ?? ""
                            , AMOUNT_DEC = totAmount
                            , TEXT = ""
                            , dataMode = DataSAPMode.GL
                        });

                        foreach (var d in q)
                        { 
                            simulationData.Details.Add(new AccrFormSimulationDetail()
                            {
                                SEQ_NO = i++
                                , ACCOUNT = d.ACCOUNT ?? ""
                                , ACCOUNT_SHORT = d.ACCOUNT_SHORT ?? ""
                                , TX = "V0"
                                , AMOUNT = string.Format("{0:#,#}", d.AMOUNT * -1) ?? ""
                                , AMOUNT_DEC = d.AMOUNT.HasValue ? d.AMOUNT.Value * -1 : 0
                                , TEXT = d.TEXT ?? ""
                                , dataMode = DataSAPMode.Vendor
                                , WBS_NO = d.WBS_NO ?? ""
                                , COST_CENTER = d.COST_CENTER ?? ""
                            });
                        }
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                Handle(e);
                return null;
            }

            return listSimulation;
        }

        public List<AccrFormSimulationData> GetBalSimulationData(String refNo)
        {
            List<AccrFormSimulationData> listSimulation = null;

            try
            {
                using (ContextWrap co = new ContextWrap())
                {
                    ELVIS_DBEntities DB = co.db;

                    // split
                    List<string> refNoSplitted = new List<string>();
                    refNoSplitted = refNo.Split(',').ToList();

                    // header
                    listSimulation = GetBalHeaderSimulationData(refNoSplitted, DB);

                    if (listSimulation == null || listSimulation.Count <= 0) return null;

                    // detail transaction
                    foreach (var simulationData in listSimulation)
                    {
                        var q = (from bal in DB.TB_R_ACCR_BALANCE
                                 where bal.DIVISION_ID == simulationData.KEYS
                                    && refNoSplitted.Contains(bal.BOOKING_NO)
                                 select new
                                 {
                                     AVAILABLE_AMT = bal.AVAILABLE_AMT
                                     , DIVISION_ID = bal.DIVISION_ID
                                 });

                        if (!q.Any()) continue;

                        decimal totAmount = q.Sum(x => x.AVAILABLE_AMT ?? 0);

                        simulationData.Details = new List<AccrFormSimulationDetail>();

                        string closingDbTxt = logic.Sys.GetText("ACCR.POST.CLOSING", "DEBIT", "Rsv-Exp-Others");
                        string closingDbAcc = logic.Sys.GetNum("ACCR.POST.CLOSING/DEBIT", 0).ToString();
                        string closingCrTxt = logic.Sys.GetText("ACCR.POST.CLOSING", "CREDIT", "RsvMiscellaneous");
                        string closingCrAcc = logic.Sys.GetNum("ACCR.POST.CLOSING/CREDIT", 0).ToString();

                        simulationData.Details.Add(new AccrFormSimulationDetail()
                        {
                            SEQ_NO = 1,
                            ACCOUNT = closingDbAcc,
                            ACCOUNT_SHORT = closingDbTxt,
                            AMOUNT = string.Format("{0:#,#}", totAmount),
                            AMOUNT_DEC = totAmount,
                            TX = "",
                            TEXT = "",
                            dataMode = DataSAPMode.GL
                        });

                        simulationData.Details.Add(new AccrFormSimulationDetail()
                        {
                            SEQ_NO = 2,
                            ACCOUNT = closingCrAcc,
                            ACCOUNT_SHORT = closingCrTxt,
                            AMOUNT = string.Format("{0:#,#}", totAmount * -1),
                            AMOUNT_DEC = totAmount * -1,
                            TX = "",
                            TEXT = "",
                            dataMode = DataSAPMode.GL
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Handle(e);
                return null;
            }

            return listSimulation;
        }


        #endregion

        public bool doSuspenseGetDocNo(string reffNo, UserData user)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@REFF_NO", reffNo);
            d.Add("@UserName", user.USERNAME);
            d.Add("@Status", null, DbType.Int32, ParameterDirection.Output);

            Exec("sp_SuspenseGetDocNo", d);

            int sts = d.Get<int>("@Status");

            bool isSuccess = sts == 0;
            return isSuccess;
        }
        public bool doSuspenseMigration(string AccruedNo, UserData user)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@REFF_NO", AccruedNo);
            d.Add("@UserName", user.USERNAME);
            d.Add("@Status", null, DbType.Int32, ParameterDirection.Output);

            Exec("sp_SuspenseMigration", d);

            int sts = d.Get<int>("@Status");

            bool isSuccess = sts == 0;
            return isSuccess;
        }

        public string GetDescWBS(string WBSNo)
        {
            return Qx<string>("GetWBSDesc", new { WbsNo = WBSNo }).FirstOrDefault();
        }

        #region Upload Accrued List
        /*
         * Created By : fid.Taufik
         * 
         * 
         */


        public void DeleteTempAccrued(int pid)
        {

            var q = Qx<string>("DeleteStagingAccr", new { pid = pid });
           
        }

        public void GenerateLogAccr(int pid, UserData ux)
        {
            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();
                    string sErr = "";

                    var dis = (from d in DB.TB_T_ACCR_UPLOAD_LIST 
                               where d.PROCESS_ID == pid && d.ERRMSG != null
                               select d).ToList();
                    if (dis.Any())
                    {
                        foreach (var tmp in dis)
                        {
                            sErr = tmp.ERRMSG;
                            string[] words = sErr.Split(';');
                            foreach (string word in words)
                            {
                                if (!String.IsNullOrEmpty(word))
                                {
                                    sErr = logic.Msg.WriteMessage(pid, "MSTD00002ERR", "Business Validation", ux.USERNAME, word);
                                }
                                
                            }
                            
                        }
                    }

                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }

        }

        public void CopyTempToTransaction(int pid)
        {
            var q = Qx<string>("MoveTempAccr", new { pid = pid });
        }


        public bool UploadAccr(string xlsFilename, string metaFile,
            ref List<AccrFormDetail> dd,
            ref string[][] derr,
            int TransactionCd,
            UserData ux, int pid)
        {
            bool Ok = false;
            me = "FormPersistence.Upload";

            _errs.Clear();

            if (!System.IO.File.Exists(xlsFilename))
            {
                _errs.Add("Uploaded File not found");
            }

            string _meta = System.IO.File.ReadAllText(metaFile);
            if (string.IsNullOrEmpty(_meta))
            {
                _errs.Add("Metadata not found");
            }
            if (_errs.Count > 0) return false;

            ceu = new CommonExcelUpload();
            ceu.UserName = ux.USERNAME;
            ceu.PID = pid;

            ceu.LogWriter = logic.Msg.WriteMessage;
            Dictionary<string, object> _data;
            _data = ceu.Read(_meta, xlsFilename);

            if (_data.Keys.Count < 1)
            {
                _errs.Add("Empty file");
                return false;
            }

            // validate mandatory field and field length  
            // => this is *very* critical because process will fail 
            // when inserted data is not valid
            Ok = ceu.Check();

            // validate Detail 
            DataTable x = ceu.Rows;

            if (!Ok)
            {
                ShowFeedback(ceu, x, ref derr);
                return false;

                /// FOR TESTING OVERRIDE 
                /// Ok = true;
            }

            PutUploadAccr(x, TransactionCd, pid, xlsFilename, ux, ref dd, ref derr); /// formerly use steps in PutData

            Ok = (_errs.Count < 1);

            if (Ok)
            {
                Ok = ceu.ReadDataError();
            }

            return Ok;
        }

        private void PutDataAccr(DataTable x, int TransactionCd, int pid, string xlsFilename, UserData ux, ref List<AccrFormDetail> dd, ref string[][] derr)
        {
            #region Put Data

            if (x != null)
            {
                string[] RoundCurrency = logic.Sys.RoundCurrencies();
                string roundCurrencies = string.Join(" and ", RoundCurrency, 0, RoundCurrency.Length);
                derr = new string[x.Rows.Count][];
                int ri = 0;

                string stdWord = "";
                var word = (from q in db.TB_M_TRANSACTION_TYPE
                            where q.TRANSACTION_CD == TransactionCd
                            select q.STD_WORDING).FirstOrDefault();
                if (word != null)
                    stdWord = word.ToString();


                string wbsLogName = Util.ExpandTimeVars("WBS%MS%");


                int SEQ = 0;
                for (int n = 0; n < x.Rows.Count; n++)
                {
                    DataRow r = x.Rows[n];
                    int errCount = _errs.Count;
                    List<string> lineErr = new List<string>();
                    AccrFormDetail d = new AccrFormDetail()
                    {
                        SequenceNumber = SEQ,
                        PVType = r[4].str().Trim(),
                        ActivityDescription = r[5].str().Trim(),
                        CostCenterCode = r[6].str().Trim(),
                        CurrencyCode = r[7].str().Trim(),
                        Amount = r[8].Int(),
                        SuspenseNo = r[3].str().Trim(),
                        WbsNumber = r[1].str().Trim()
                    };

                    //todo - validation


                    derr[ri] = new string[9];
                    for (int i = 0; i < 9; i++)
                    {
                        derr[ri][i] = Convert.ToString(r[i]);
                    }

                    if (errCount < _errs.Count)
                    {
                        derr[ri][9] = CommonFunction.CommaJoin(lineErr, ",\r\n");
                    }
                    else
                    {
                        if (d.Amount > 0) dd.Add(d);
                    }
                    ri++;
                }
            }

            #endregion PutData
        }

        public bool PutUploadAccr(DataTable x
                , int TransactionCd
                , int pid
                , string xlsFilename
                , UserData ux
            // output
                , ref List<AccrFormDetail> dd
                , ref string[][] derr)
        {
            //string spName = Qx<string>("GetSpFromTx",
            //        new { tt = TransactionCd.str() }).FirstOrDefault();
            string vendorCd = null;
            Exec("sp_PutUploadH",
                    new
                    {
                        pid = pid,
                        transactionCd = TransactionCd,
                        vendorCd = vendorCd,
                        uploadFileName = Path.GetFileName(xlsFilename),
                        uid = ux.USERNAME
                    },
                        null, null, CommandType.StoredProcedure);

            //if (!spName.isEmpty() && x != null)
            if(x != null)
            {
                SqlBulkCopy bulk = new SqlBulkCopy(Db as SqlConnection);

                DecorateUpload(x, pid, ux.USERNAME);
                bulk.DestinationTableName = "TB_T_ACCR_UPLOAD_LIST";
                bulk.ColumnMappings.Clear();
                for (int i = 0; i < x.Columns.Count; i++)
                {
                    bulk.ColumnMappings.Add(x.Columns[i].ColumnName, x.Columns[i].ColumnName);
                }
                PreExecuteTest(x, ux, pid);

                bulk.WriteToServer(x);

                //Exec(spName, new { pid = pid, uid = ux.USERNAME });

                //int fiscalYear = logic.Sys.FiscalYear(DateTime.Now);
                //int fiscalYear = DateTime.Now.Year - 1; // cuman buat testing tar ilangin -1 nya
                int fiscalYear = DateTime.Now.Year;
                var q = Qx<string>("ValidateUploadAccrList", new { divId = ux.DIV_CD.ToString(), processId = pid, fiscalYear = fiscalYear }).ToList();
            }

            
            List<ErrorUploadData> ErrUploads = Qx<ErrorUploadData>("GetErrorUploadAccr", new { pid = pid }).ToList();
            if (ErrUploads != null && ErrUploads.Count > 0)
                _errs.AddRange(ErrUploads.Select(e => e.ERRMSG));
            else
            {
                CopyTempToTransaction(pid);
                dd.AddRange(Qx<AccrFormDetail>("GetLastUploadAccr", new { pid = pid }).ToList());
            }
            return true;
        }

        public void DecorateUpload(DataTable x, int pid, string createdBy)
        {
            if (x.Columns.IndexOf("PROCESS_ID") < 0)
            {
                x.Columns.Add("PROCESS_ID", typeof(int));
            }
            if (x.Columns.IndexOf("CREATED_BY") < 0)
            {
                x.Columns.Add("CREATED_BY", typeof(string));
            }
            if (x.Columns.IndexOf("CREATED_DT") < 0)
            {
                x.Columns.Add("CREATED_DT", typeof(DateTime));
            }
            if (x.Columns.IndexOf("SEQ_NO") < 0)
            {
                x.Columns.Add("SEQ_NO", typeof(int));
            }
            List<int> existingNum = new List<int>();
            List<int> d = new List<int>();
            int maxseq = 0;
            for (int i = 0; i < x.Rows.Count; i++)
            {
                DataRow r = x.Rows[i];
                int seq = (r["SEQ_NO"] as int?) ?? 0;
                if (seq > maxseq) maxseq = seq;
                if (seq <= 0)
                    d.Add(i);
                else
                {
                    if (existingNum.Contains(seq))
                    {
                        d.Add(i);
                    }
                    else
                        existingNum.Add(seq);
                }

            }

            if (d.Count > 0)
            {
                for (int i = 0; i < d.Count; i++)
                {
                    DataRow r = x.Rows[d[i]];
                    maxseq++;
                    r["SEQ_NO"] = maxseq;
                }
            }

            for (int i = 0; i < x.Rows.Count; i++)
            {
                DataRow r = x.Rows[i];

                r["PROCESS_ID"] = pid;
                r["CREATED_BY"] = createdBy;
                r["CREATED_DT"] = DateTime.Now;
            }
        }

        #endregion

        #region Accrued Posting
        public bool SavePostingDate(string AccruedNo, DateTime postingDate)
        {
            bool result = true;
            try
            {
                var accrH = (from h in db.TB_R_ACCR_LIST_H
                             where h.ACCRUED_NO == AccruedNo
                             select h).FirstOrDefault();

                if (accrH != null)
                {
                    accrH.POSTING_DATE = postingDate;
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Handle(ex);
                result = false;
            }

            return result;
        }

        public bool AccruedPosting(AccrFormData FormData, UserData u, ref List<AccrSAPError> sapErr)
        {
            //if (Common.AppSetting.NOOP_SAP)
            //    return true;

            bool isSuccessAll = true;

            isSuccessAll &= PostingPR(FormData, u, ref sapErr);
            isSuccessAll &= PostingDirect(FormData, u, ref sapErr);
            isSuccessAll &= PostingSusPaid(FormData, u, ref sapErr);
            isSuccessAll &= PostingSusUnpaid(FormData, u, ref sapErr);

            return isSuccessAll;
        }

        private bool PostingPR(AccrFormData FormData, UserData u, ref List<AccrSAPError> sapErr)
        {
            string fid = resx.FunctionId("FCN_ACCRUED_POST_PR");
            string _loc = "PostingPR()";
            bool isSuccess = true;

            logic.Say(_loc, "Start");

            DbTransaction trans = null;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                trans = db.Connection.BeginTransaction();

                if (!FormData.Details.Any(x => x.PVTypeCd == 4))
                {
                    logic.Say(_loc, "no PR data to be Posted");
                    return true;
                }

                var lstBnPR = FormData.Details.Where(x => x.PVTypeCd == 4).Select(x => x.BookingNo).Distinct().ToList();
                string bnpr = lstBnPR[0];

                var postingFlag = (from b in db.TB_R_ACCR_BALANCE
                                   where bnpr.Contains(b.BOOKING_NO)
                                   select b.POSTING_FLAG).ToList();

                if (!postingFlag.Any(p => (p ?? 0) == 0))
                {
                    logic.Say(_loc, "this Accrued already posted successfully");
                    return true;
                }

                // retrieve data that will be sent to SAP
                var dataPR = GetPRSimulationData(FormData.AccruedNo).FirstOrDefault();
                string comp = logic.Sys.GetNum("COMPANY_INFO/COMPANY_CODE", 200).str();

                AccrPostData posting = new AccrPostData();

                #region Header
                posting.USERNAME = u.USERNAME;
                posting.HEADER_TXT = dataPR.REF_DOC;
                posting.COMP_CODE = comp;
                posting.DOC_DATE = dataPR.DOC_DATE.ToString("yyyyMMdd");
                posting.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                posting.TRANS_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                posting.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                posting.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                posting.REF_DOC_NO = FormData.AccruedNo;
                posting.DOC_TYPE = "SA";
                posting.GROUP_DOC = fid;
                #endregion

                #region GL - Amount

                AccrPostGL accountGL;
                AccrPostCurr accountCurr;
                foreach (var d in dataPR.Details)
                {
                    accountGL = new AccrPostGL();
                    accountGL.ITEMNO_ACC = d.SEQ_NO.str();
                    accountGL.GL_ACCOUNT = d.ACCOUNT;
                    accountGL.ITEM_TEXT = d.TEXT;
                    accountGL.COMP_CODE = "";
                    accountGL.BUS_AREA = "";
                    accountGL.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                    accountGL.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                    accountGL.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                    accountGL.VALUE_DATE = "";
                    accountGL.WBS_ELEMENT = d.WBS_NO;
                    accountGL.ORDERID = "";
                    accountGL.FUNDS_CTR = "";
                    accountGL.COSTCENTER = d.COST_CENTER;
                    accountGL.TAX_CODE = d.TX;

                    posting.ACCOUNT_GL.Add(accountGL);

                    accountCurr = new AccrPostCurr();
                    accountCurr.ITEMNO_ACC = d.SEQ_NO.str();
                    accountCurr.CURRENCY = dataPR.CURRENCY_CODE;
                    accountCurr.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                    accountCurr.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                    posting.CURRENCYAMOUNT.Add(accountCurr);
                }
                #endregion

                object sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                #region Process Result
                if (sapRet != null)
                {
                    if (sapRet is AccrPostResult)
                    {
                        isSuccess = true;
                        var ret = sapRet as AccrPostResult;

                        try
                        {
                            foreach (var bn in lstBnPR)
                            {
                                var bal = logic.AccrBalance.GetBalanceByBookingNo(bn);

                                if (bal != null)
                                {
                                    logic.Say(_loc, "insert doc no : {0}||{1}", bn, ret.DOC_NUMSAP);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), bal.CREATED_DT.Year, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 1, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), bal.CREATED_DT.Year, ret, FormData);
                                    UpdatePostingFlag(bal.BOOKING_NO, 1, u);
                                }
                                else
                                    logic.Say(_loc, "Failed to insert doc no because no {0} balance data", bn);
                            }

                            trans.Commit();
                        }
                        catch (Exception e)
                        {
                            if (trans != null)
                                trans.Rollback();

                            LoggingLogic.err(e);
                        }

                        LogSuccessSAP(_loc, ret);
                    }
                    else
                    {
                        isSuccess = false;
                        var ret = sapRet as List<AccrSAPError>;

                        LogErrorSAP(_loc, ret);

                        sapErr.AddRange(
                            ret.Select(x => { x.PROCESS_NAME = "Posting PR"; return x; })
                            .ToList()
                            );
                    }
                }
                else
                {
                    isSuccess = false;
                    LogErrorSAP(_loc, null);
                    sapErr.Add(new AccrSAPError
                    {
                        TYPE = "00",
                        NUMBER = "",
                        MESSAGE = "SAP return null;",
                        PROCESS_NAME = "Posting PR"
                    });
                }
                #endregion Process Result
            }

            return isSuccess;
        }

        private bool PostingDirect(AccrFormData FormData, UserData u, ref List<AccrSAPError> sapErr)
        {
            string fid = resx.FunctionId("FCN_ACCRUED_POST_DIRECT");
            string _loc = "PostingDirect()";
            bool isSuccess = true;

            logic.Say(_loc, "Start");
            
            DbTransaction trans = null;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                trans = db.Connection.BeginTransaction();

                if (!FormData.Details.Any(x => x.PVTypeCd == 1))
                {
                    logic.Say(_loc, "no Direct data to be Posted");
                    return true;
                }

                var lstBnDr = FormData.Details.Where(x => x.PVTypeCd == 1).Select(x => x.BookingNo).Distinct().ToList();
                //string bndr = lstBnDr[0];

                var postingFlag = (from b in db.TB_R_ACCR_BALANCE
                                   where lstBnDr.Contains(b.BOOKING_NO)
                                   select b.POSTING_FLAG).ToList();

                if (!postingFlag.Any(p => (p ?? 0) == 0))
                {
                    logic.Say(_loc, "this Accrued already posted successfully");
                    return true;
                }

                var dataDirect = GetDirectSimulationData(FormData.AccruedNo).FirstOrDefault();
                string comp = logic.Sys.GetNum("COMPANY_INFO/COMPANY_CODE", 200).str();

                AccrPostData posting = new AccrPostData();

                #region Header
                posting.USERNAME = u.USERNAME;
                posting.HEADER_TXT = dataDirect.REF_DOC;
                posting.COMP_CODE = comp;
                posting.DOC_DATE = dataDirect.DOC_DATE.ToString("yyyyMMdd");
                posting.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                posting.TRANS_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                posting.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                posting.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                posting.REF_DOC_NO = FormData.AccruedNo;
                posting.DOC_TYPE = "SA";
                posting.GROUP_DOC = fid;
                #endregion

                #region GL - Amount

                AccrPostGL accountGL;
                AccrPostCurr accountCurr;
                foreach (var d in dataDirect.Details)
                {
                    accountGL = new AccrPostGL();
                    accountGL.ITEMNO_ACC = d.SEQ_NO.str();
                    accountGL.GL_ACCOUNT = d.ACCOUNT;
                    accountGL.ITEM_TEXT = d.TEXT;
                    accountGL.COMP_CODE = "";
                    accountGL.BUS_AREA = "";
                    accountGL.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                    accountGL.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                    accountGL.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                    accountGL.VALUE_DATE = "";
                    accountGL.WBS_ELEMENT = d.WBS_NO;
                    accountGL.ORDERID = "";
                    accountGL.FUNDS_CTR = "";
                    accountGL.COSTCENTER = d.COST_CENTER;
                    accountGL.TAX_CODE = d.TX;

                    posting.ACCOUNT_GL.Add(accountGL);

                    accountCurr = new AccrPostCurr();
                    accountCurr.ITEMNO_ACC = d.SEQ_NO.str();
                    accountCurr.CURRENCY = dataDirect.CURRENCY_CODE;
                    accountCurr.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                    accountCurr.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                    posting.CURRENCYAMOUNT.Add(accountCurr);
                }
                #endregion

                object sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                #region Process Result
                if (sapRet != null)
                {
                    if (sapRet is AccrPostResult)
                    {
                        isSuccess = true;
                        var ret = sapRet as AccrPostResult;

                        try
                        {
                            foreach (var bn in lstBnDr)
                            {
                                var bal = logic.AccrBalance.GetBalanceByBookingNo(bn);

                                if (bal != null)
                                {
                                    logic.Say(_loc, "insert doc no : {0}||{1}", bn, ret.DOC_NUMSAP);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), bal.CREATED_DT.Year, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 1, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), bal.CREATED_DT.Year, ret, FormData);
                                    UpdatePostingFlag(bal.BOOKING_NO, 1, u);
                                }
                                else
                                    logic.Say(_loc, "Failed to insert doc no because no {0} balance data", bn);
                            }

                            trans.Commit();
                        }
                        catch (Exception e)
                        {
                            if (trans != null)
                                trans.Rollback();

                            LoggingLogic.err(e);
                        }

                        LogSuccessSAP(_loc, ret);
                    }
                    else
                    {
                        isSuccess = false;
                        var ret = sapRet as List<AccrSAPError>;

                        LogErrorSAP(_loc, ret);

                        sapErr.AddRange(
                            ret.Select(x => { x.PROCESS_NAME = "Posting Direct"; return x; })
                            .ToList()
                            );
                    }
                }
                else
                {
                    isSuccess = false;
                    LogErrorSAP(_loc, null);
                    sapErr.Add(new AccrSAPError
                    {
                        TYPE = "00",
                        NUMBER = "",
                        MESSAGE = "SAP return null;",
                        PROCESS_NAME = "Posting Direct"
                    });
                }
                #endregion
            }

            return isSuccess;
        }

        private bool PostingSusPaid(AccrFormData FormData, UserData u, ref List<AccrSAPError> sapErr)
        {
            bool isSuccess = true;
            string fid = resx.FunctionId("FCN_ACCRUED_POST_SUS_PAID");
            string loc = "PostingSusPaid()";

            logic.Say(loc, "Start");

            bool isPaid = true;
            var susPaidData = GetSuspenseSimulationData(FormData.AccruedNo, isPaid);
            string comp = logic.Sys.GetNum("COMPANY_INFO/COMPANY_CODE", 200).str();

            if (susPaidData == null || !susPaidData.Any())
            {
                logic.Say(loc, "no Suspense data to be Posted");
                return true;
            }

            DbTransaction trans = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    trans = db.Connection.BeginTransaction();

                    foreach (var data in susPaidData)
                    {
                        string _loc = "";
                        AccrPostData posting = null;
                        object sapRet = null;

                        var bal = logic.AccrBalance.GetBalanceByBookingNo(data.BOOKING_NO);

                        if (bal == null)
                        {
                            logic.Say(loc, string.Format("There is no balance of Booking No {0}", data.BOOKING_NO));
                            continue;
                        }
                        if ((bal.POSTING_FLAG ?? 0) == 3)
                        {
                            logic.Say(loc, string.Format("Suspense {0} already posted successfully", data.SUSPENSE_NO));
                            continue;
                        }
                        int budgetYear = bal.CREATED_DT.Year;

                        bool doFirstPosting = (bal.POSTING_FLAG ?? 0) < 1;
                        bool doSecondPosting = (bal.POSTING_FLAG ?? 0) < 2;
                        bool doThirdPosting = (bal.POSTING_FLAG ?? 0) < 3;

                        if (doFirstPosting)
                        {
                            #region ZFM_FI_ACCRUED_POSTING
                            logic.Say(loc, "ZFM_FI_ACCRUED_POSTING: {0}", data.KEYS);

                            _loc = "PostingSusPaid() - ZFM_FI_ACCRUED_POSTING - " + data.KEYS;

                            posting = new AccrPostData();

                            #region Header
                            posting.USERNAME = u.USERNAME;
                            posting.HEADER_TXT = data.REF_DOC;
                            posting.COMP_CODE = comp;
                            posting.DOC_DATE = data.DOC_DATE.ToString("yyyyMMdd");
                            posting.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.TRANS_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                            posting.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                            posting.REF_DOC_NO = FormData.AccruedNo;
                            posting.DOC_TYPE = "SA";
                            posting.GROUP_DOC = fid;
                            #endregion

                            #region Detail
                            AccrPostGL gl;
                            AccrPostVendor vd;
                            AccrPostCurr cur;
                            foreach (var d in data.Details.GetRange(0, 2))
                            {
                                if (d.dataMode == DataSAPMode.GL)
                                {
                                    gl = new AccrPostGL();
                                    gl.ITEMNO_ACC = d.SEQ_NO.str();
                                    gl.GL_ACCOUNT = d.ACCOUNT;
                                    gl.ITEM_TEXT = d.ACCOUNT_SHORT;
                                    gl.COMP_CODE = "";
                                    gl.BUS_AREA = "";
                                    gl.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                                    gl.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                                    gl.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    gl.VALUE_DATE = "";
                                    gl.WBS_ELEMENT = data.WBS_NO;
                                    gl.ORDERID = "";
                                    gl.FUNDS_CTR = "";
                                    gl.COSTCENTER = data.COST_CENTER;
                                    gl.TAX_CODE = d.TX;

                                    posting.ACCOUNT_GL.Add(gl);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                                else
                                {
                                    vd = new AccrPostVendor();
                                    vd.ITEMNO_ACC = d.SEQ_NO.str();
                                    vd.VENDOR_NO = d.ACCOUNT;
                                    vd.BUS_AREA = "";
                                    vd.ITEM_TEXT = d.TEXT;
                                    vd.BLINE_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    vd.PYMT_CUR = "";
                                    vd.PYMT_AMT = "0";
                                    vd.SP_GL_IND = "U";
                                    vd.TAX_CODE = d.TX;

                                    posting.ACCOUNT_VENDOR.Add(vd);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                            }

                            #endregion

                            sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                            #region Process Result
                            if (sapRet != null)
                            {
                                if (sapRet is AccrPostResult)
                                {
                                    var ret = sapRet as AccrPostResult;

                                    logic.Say(_loc, "insert doc no : {0}-{1}", bal.BOOKING_NO, ret.DOC_NUMSAP);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 1, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret);
                                    UpdatePostingFlag(bal.BOOKING_NO, 1, u);

                                    LogSuccessSAP(_loc, ret);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var ret = sapRet as List<AccrSAPError>;

                                    LogErrorSAP(_loc, ret);

                                    sapErr.AddRange(
                                        ret.Select(x => { x.PROCESS_NAME = "Posting Suspense Paid"; return x; })
                                        .ToList()
                                        );
                                    // if post failed, continue to next suspense
                                    continue;
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                LogErrorSAP(_loc, null);
                                sapErr.Add(new AccrSAPError
                                {
                                    TYPE = "00",
                                    NUMBER = "",
                                    MESSAGE = "SAP return null;",
                                    PROCESS_NAME = "Posting Suspense Paid"
                                });

                                continue;
                            }
                            #endregion Process Result

                            #endregion ZFM_FI_ACCRUED_POSTING
                        }

                        if (doSecondPosting)
                        {
                            #region ZFM_FI_ACCRUED_POSTING
                            logic.Say(loc, "ZFM_FI_ACCRUED_POSTING: {0}", data.KEYS);

                            _loc = "PostingSusPaid() - ZFM_FI_ACCRUED_POSTING | 1 - " + data.KEYS;
                            posting = new AccrPostData();

                            #region Header
                            posting.USERNAME = u.USERNAME;
                            posting.HEADER_TXT = data.REF_DOC;
                            posting.COMP_CODE = comp;
                            posting.DOC_DATE = data.DOC_DATE.ToString("yyyyMMdd");
                            posting.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.TRANS_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                            posting.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                            posting.REF_DOC_NO = FormData.AccruedNo;
                            posting.DOC_TYPE = "SA";
                            posting.GROUP_DOC = fid;
                            #endregion

                            #region Detail
                            AccrPostGL gl;
                            AccrPostVendor vd;
                            AccrPostCurr cur;
                            foreach (var d in data.Details.GetRange(2, 2))
                            {
                                if (d.dataMode == DataSAPMode.GL)
                                {
                                    gl = new AccrPostGL();
                                    gl.ITEMNO_ACC = d.SEQ_NO.str();
                                    gl.GL_ACCOUNT = d.ACCOUNT;
                                    gl.ITEM_TEXT = d.ACCOUNT_SHORT;
                                    gl.COMP_CODE = "";
                                    gl.BUS_AREA = "";
                                    gl.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                                    gl.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                                    gl.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    gl.VALUE_DATE = "";
                                    gl.WBS_ELEMENT = data.WBS_NO;
                                    gl.ORDERID = "";
                                    gl.FUNDS_CTR = "";
                                    gl.COSTCENTER = data.COST_CENTER;
                                    gl.TAX_CODE = d.TX;

                                    posting.ACCOUNT_GL.Add(gl);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                                else
                                {
                                    vd = new AccrPostVendor();
                                    vd.ITEMNO_ACC = d.SEQ_NO.str();
                                    vd.VENDOR_NO = d.ACCOUNT;
                                    vd.BUS_AREA = "";
                                    vd.ITEM_TEXT = d.TEXT;
                                    vd.BLINE_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    vd.PYMT_CUR = "";
                                    vd.PYMT_AMT = "0";
                                    vd.SP_GL_IND = "U";
                                    vd.TAX_CODE = d.TX;

                                    posting.ACCOUNT_VENDOR.Add(vd);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                            }

                            #endregion

                            sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                            #region Process Result
                            if (sapRet != null)
                            {
                                if (sapRet is AccrPostResult)
                                {
                                    var ret = sapRet as AccrPostResult;

                                    logic.Say(_loc, "insert doc no : {0}-{1}", bal.BOOKING_NO, ret.DOC_NUMSAP);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 2, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret);
                                    UpdatePostingFlag(bal.BOOKING_NO, 2, u);

                                    LogSuccessSAP(_loc, ret);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var ret = sapRet as List<AccrSAPError>;

                                    LogErrorSAP(_loc, ret);

                                    sapErr.AddRange(
                                        ret.Select(x => { x.PROCESS_NAME = "Posting Suspense Paid"; return x; })
                                        .ToList()
                                        );
                                    // if post failed, continue to next suspense
                                    continue;
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                LogErrorSAP(_loc, null);
                                sapErr.Add(new AccrSAPError
                                {
                                    TYPE = "00",
                                    NUMBER = "",
                                    MESSAGE = "SAP return null;",
                                    PROCESS_NAME = "Posting Suspense Paid"
                                });
                                continue;
                            }
                            #endregion Process Result

                            #endregion ZFM_FI_ACCRUED_POSTING
                        }
                        
                        if (doThirdPosting)
                        {
                            #region ZFM_FI_CLEARING
                            logic.Say(loc, "ZFM_FI_CLEARING: {0}", data.KEYS);

                            _loc = "PostingSusPaid() - ZFM_FI_CLEARING - " + data.KEYS;
                            AccrClearingData clearing = new AccrClearingData();

                            string vendorAccount = data.Details
                                                    .Where(x => x.dataMode == DataSAPMode.Vendor)
                                                    .Select(x => x.ACCOUNT)
                                                    .FirstOrDefault();

                            var listSapDoc = GetSAPDoc(bal.REFF_NO.Int(), budgetYear);

                            string docPost1 = "", docPost2 = "";
                            if (listSapDoc != null && listSapDoc.Count() > 0)
                                docPost1 = listSapDoc[0].SAP_DOC_NO;
                            if (listSapDoc != null && listSapDoc.Count() > 1)
                                docPost2 = listSapDoc[1].SAP_DOC_NO;

                            clearing.AGKON = vendorAccount;
                            clearing.BUDAT = FormData.PostingDate.Value.ToString("dd.MM.yyyy");
                            clearing.MONAT = FormData.PostingDate.Value.ToString("MM");
                            clearing.BUKRS = comp;
                            clearing.WAERS = data.CURRENCY_CODE;
                            clearing.AGUMS = "U";
                            clearing.XNOPS = "X";
                            clearing.XPOS1 = "";
                            clearing.XPOS2 = "X";
                            clearing.SEL01 = docPost1;
                            clearing.SEL02 = docPost2;
                            clearing.ABPOS = "0000001";

                            sapRet = logic.SAP.AccruedClearing(clearing, u.USERNAME, fid, _loc);

                            if (sapRet != null)
                            {
                                if (sapRet is AccrClearingResult)
                                {
                                    var ret = sapRet as AccrClearingResult;

                                    logic.Say(_loc, "insert doc no : {0}-{1}", bal.BOOKING_NO, ret.NOCLR);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 3, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret);
                                    UpdatePostingFlag(bal.BOOKING_NO, 3, u);

                                    LogSuccessSAP(_loc, ret);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var ret = sapRet as List<AccrSAPError>;

                                    LogErrorSAP(_loc, ret);
                                    sapErr.AddRange(
                                        ret.Select(x => { x.PROCESS_NAME = "Posting Suspense Paid"; return x; })
                                        .ToList()
                                        );

                                    continue;
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                LogErrorSAP(_loc, null);
                                sapErr.Add(new AccrSAPError
                                {
                                    TYPE = "00",
                                    NUMBER = "",
                                    MESSAGE = "SAP return null;",
                                    PROCESS_NAME = "Posting Suspense Paid"
                                });

                                continue;
                            }
                            #endregion
                        }
                    }

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    if (trans != null)
                        trans.Rollback();

                    isSuccess = false;
                    LoggingLogic.err(ex);
                }
            }
            return isSuccess;
        }

        private bool PostingSusUnpaid(AccrFormData FormData, UserData u, ref List<AccrSAPError> sapErr)
        {
            bool isSuccess = true;
            string fid = resx.FunctionId("FCN_ACCRUED_POST_SUS_UNPAID");
            string loc = "PostingSusUnpaid()";

            logic.Say(loc, "Start");

            bool isPaid = false;
            var susUnpaidData = GetSuspenseSimulationData(FormData.AccruedNo, isPaid);
            string comp = logic.Sys.GetNum("COMPANY_INFO/COMPANY_CODE", 200).str();

            if (susUnpaidData == null || !susUnpaidData.Any())
            {
                logic.Say(loc, "no Suspense data to be Posted");
                return true;
            }
            
            DbTransaction trans = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    trans = db.Connection.BeginTransaction();

                    foreach (var data in susUnpaidData)
                    {
                        string _loc = "";
                        AccrPostData posting = null;
                        object sapRet = null;

                        var bal = logic.AccrBalance.GetBalanceByBookingNo(data.BOOKING_NO);

                        if (bal == null)
                        {
                            logic.Say(loc, string.Format("There is no balance of Booking No {0}", data.BOOKING_NO));
                            continue;
                        }
                        if ((bal.POSTING_FLAG ?? 0) == 4)
                        {
                            logic.Say(loc, string.Format("Suspense {0} already posted successfully", data.SUSPENSE_NO));
                            continue;
                        }
                        int budgetYear = bal.CREATED_DT.Year;

                        bool doFirstPosting = (bal.POSTING_FLAG ?? 0) < 1;
                        bool doSecondPosting = (bal.POSTING_FLAG ?? 0) < 2;
                        bool doThirdPosting = (bal.POSTING_FLAG ?? 0) < 3;
                        bool doLastPosting = (bal.POSTING_FLAG ?? 0) < 4;

                        if (doFirstPosting)
                        {
                            #region ZFM_FI_ACCRUED_POSTING2
                            logic.Say(loc, "ZFM_FI_ACCRUED_POSTING2: {0}", data.KEYS);

                            _loc = "PostingSusUnpaid() - ZFM_FI_ACCRUED_POSTING2 - " + data.KEYS;
                            var sapDoc = GetSAPDoc(data.SUSPENSE_NO ?? 0, data.REF_DOC_YEAR.Int())
                                            .FirstOrDefault();

                            posting = new AccrPostData();

                            #region Header
                            posting.USERNAME = u.USERNAME;
                            posting.HEADER_TXT = data.REF_DOC;
                            posting.COMP_CODE = comp;
                            posting.DOC_DATE = data.DOC_DATE.ToString("yyyyMMdd");
                            posting.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.TRANS_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                            posting.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                            posting.REF_DOC_NO = FormData.AccruedNo;
                            posting.DOC_TYPE = "KZ";
                            posting.GROUP_DOC = fid;
                            posting.DOC_REQUEST = sapDoc != null ? sapDoc.SAP_DOC_NO : "";
                            #endregion

                            #region Detail
                            AccrPostGL gl;
                            AccrPostVendor vd;
                            AccrPostCurr cur;
                            foreach (var d in data.Details.GetRange(0, 2))
                            {
                                if (d.dataMode == DataSAPMode.GL)
                                {
                                    gl = new AccrPostGL();
                                    gl.ITEMNO_ACC = d.SEQ_NO.str();
                                    gl.GL_ACCOUNT = d.ACCOUNT;
                                    gl.ITEM_TEXT = d.ACCOUNT_SHORT;
                                    gl.COMP_CODE = "";
                                    gl.BUS_AREA = "";
                                    gl.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                                    gl.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                                    gl.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    gl.VALUE_DATE = "";
                                    gl.WBS_ELEMENT = d.WBS_NO;
                                    gl.ORDERID = "";
                                    gl.FUNDS_CTR = "";
                                    gl.COSTCENTER = d.COST_CENTER;
                                    gl.TAX_CODE = d.TX;

                                    posting.ACCOUNT_GL.Add(gl);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                                else
                                {
                                    vd = new AccrPostVendor();
                                    vd.ITEMNO_ACC = d.SEQ_NO.str();
                                    vd.VENDOR_NO = d.ACCOUNT;
                                    vd.BUS_AREA = "";
                                    vd.ITEM_TEXT = d.TEXT;
                                    vd.BLINE_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    vd.PYMT_CUR = "";
                                    vd.PYMT_AMT = "0";
                                    vd.SP_GL_IND = "U";
                                    vd.TAX_CODE = d.TX;

                                    posting.ACCOUNT_VENDOR.Add(vd);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                            }

                            #endregion

                            sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                            #region Process Result
                            if (sapRet != null)
                            {
                                if (sapRet is AccrPostResult)
                                {
                                    var ret = sapRet as AccrPostResult;

                                    logic.Say(_loc, "insert doc no : {0}-{1}", bal.BOOKING_NO, ret.DOC_NUMSAP);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 1, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret);
                                    UpdatePostingFlag(bal.BOOKING_NO, 1, u);

                                    LogSuccessSAP(_loc, ret);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var ret = sapRet as List<AccrSAPError>;

                                    LogErrorSAP(_loc, ret);
                                    sapErr.AddRange(
                                        ret.Select(x => { x.PROCESS_NAME = "Posting Suspense Unpaid"; return x; })
                                        .ToList()
                                        );

                                    // if post failed, continue to next suspense
                                    continue;
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                LogErrorSAP(_loc, null);
                                sapErr.Add(new AccrSAPError
                                {
                                    TYPE = "00",
                                    NUMBER = "",
                                    MESSAGE = "SAP return null;",
                                    PROCESS_NAME = "Posting Suspense Unpaid"
                                });

                                continue;
                            }
                            #endregion Process Result

                            #endregion ZFM_FI_ACCRUED_POSTING2
                        }

                        if (doSecondPosting)
                        {
                            #region ZFM_FI_ACCRUED_POSTING
                            logic.Say(loc, "ZFM_FI_ACCRUED_POSTING: {0}", data.KEYS);

                            _loc = "PostingSusUnpaid() - ZFM_FI_ACCRUED_POSTING | 1 - " + data.KEYS;
                            posting = new AccrPostData();

                            #region Header
                            posting.USERNAME = u.USERNAME;
                            posting.HEADER_TXT = data.REF_DOC;
                            posting.COMP_CODE = comp;
                            posting.DOC_DATE = data.DOC_DATE.ToString("yyyyMMdd");
                            posting.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.TRANS_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                            posting.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                            posting.REF_DOC_NO = FormData.AccruedNo;
                            posting.DOC_TYPE = "SA";
                            posting.GROUP_DOC = fid;
                            #endregion

                            #region Detail
                            AccrPostGL gl;
                            AccrPostVendor vd;
                            AccrPostCurr cur;
                            foreach (var d in data.Details.GetRange(2, 2))
                            {
                                if (d.dataMode == DataSAPMode.GL)
                                {
                                    gl = new AccrPostGL();
                                    gl.ITEMNO_ACC = d.SEQ_NO.str();
                                    gl.GL_ACCOUNT = d.ACCOUNT;
                                    gl.ITEM_TEXT = d.ACCOUNT_SHORT;
                                    gl.COMP_CODE = "";
                                    gl.BUS_AREA = "";
                                    gl.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                                    gl.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                                    gl.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    gl.VALUE_DATE = "";
                                    gl.WBS_ELEMENT = d.WBS_NO;
                                    gl.ORDERID = "";
                                    gl.FUNDS_CTR = "";
                                    gl.COSTCENTER = d.COST_CENTER;
                                    gl.TAX_CODE = d.TX;

                                    posting.ACCOUNT_GL.Add(gl);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                                else
                                {
                                    vd = new AccrPostVendor();
                                    vd.ITEMNO_ACC = d.SEQ_NO.str();
                                    vd.VENDOR_NO = d.ACCOUNT;
                                    vd.BUS_AREA = "";
                                    vd.ITEM_TEXT = d.TEXT;
                                    vd.BLINE_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    vd.PYMT_CUR = "";
                                    vd.PYMT_AMT = "0";
                                    vd.SP_GL_IND = "U";
                                    vd.TAX_CODE = d.TX;

                                    posting.ACCOUNT_VENDOR.Add(vd);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                            }

                            #endregion

                            sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                            #region Process Result
                            if (sapRet != null)
                            {
                                if (sapRet is AccrPostResult)
                                {
                                    var ret = sapRet as AccrPostResult;

                                    logic.Say(_loc, "insert doc no : {0}-{1}", bal.BOOKING_NO, ret.DOC_NUMSAP);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 2, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret);
                                    UpdatePostingFlag(bal.BOOKING_NO, 2, u);

                                    LogSuccessSAP(_loc, ret);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var ret = sapRet as List<AccrSAPError>;

                                    LogErrorSAP(_loc, ret);
                                    sapErr.AddRange(
                                        ret.Select(x => { x.PROCESS_NAME = "Posting Suspense Unpaid"; return x; })
                                        .ToList()
                                        );

                                    // if post failed, continue to next suspense
                                    continue;
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                LogErrorSAP(_loc, null);
                                sapErr.Add(new AccrSAPError
                                {
                                    TYPE = "00",
                                    NUMBER = "",
                                    MESSAGE = "SAP return null;",
                                    PROCESS_NAME = "Posting Suspense Unpaid"
                                });

                                continue;
                            }
                            #endregion Process Result

                            #endregion ZFM_FI_ACCRUED_POSTING
                        }

                        if (doThirdPosting)
                        {
                            #region ZFM_FI_CLEARING
                            logic.Say(loc, "ZFM_FI_CLEARING: {0}", data.KEYS);

                            _loc = "PostingSusUnpaid() - ZFM_FI_CLEARING - " + data.KEYS;
                            AccrClearingData clearing = new AccrClearingData();

                            var listSapDoc = GetSAPDoc(bal.REFF_NO.Int(), budgetYear);

                            string docPost1 = "", docPost2 = "";
                            if (listSapDoc != null && listSapDoc.Count() > 0)
                                docPost1 = listSapDoc[0].SAP_DOC_NO;
                            if (listSapDoc != null && listSapDoc.Count() > 1)
                                docPost2 = listSapDoc[1].SAP_DOC_NO;

                            string vendorAccount = data.Details
                                                    .Where(x => x.dataMode == DataSAPMode.Vendor)
                                                    .Select(x => x.ACCOUNT)
                                                    .FirstOrDefault();

                            clearing.AGKON = vendorAccount;
                            clearing.BUDAT = FormData.PostingDate.Value.ToString("dd.MM.yyyy");
                            clearing.MONAT = FormData.PostingDate.Value.ToString("MM");
                            clearing.BUKRS = comp;
                            clearing.WAERS = data.CURRENCY_CODE;
                            clearing.AGUMS = "U";
                            clearing.XNOPS = "X";
                            clearing.XPOS1 = "";
                            clearing.XPOS2 = "X";
                            clearing.SEL01 = docPost1;
                            clearing.SEL02 = docPost2;
                            clearing.ABPOS = "0000001";

                            sapRet = logic.SAP.AccruedClearing(clearing, u.USERNAME, fid, _loc);

                            if (sapRet != null)
                            {
                                if (sapRet is AccrClearingResult)
                                {
                                    var ret = sapRet as AccrClearingResult;

                                    logic.Say(_loc, "insert doc no : {0}-{1}", bal.BOOKING_NO, ret.NOCLR);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 3, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret);
                                    UpdatePostingFlag(bal.BOOKING_NO, 3, u);

                                    LogSuccessSAP(_loc, ret);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var ret = sapRet as List<AccrSAPError>;

                                    LogErrorSAP(_loc, ret);
                                    sapErr.AddRange(
                                        ret.Select(x => { x.PROCESS_NAME = "Posting Suspense Unpaid"; return x; })
                                        .ToList()
                                        );

                                    continue;
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                LogErrorSAP(_loc, null);
                                sapErr.Add(new AccrSAPError
                                {
                                    TYPE = "00",
                                    NUMBER = "",
                                    MESSAGE = "SAP return null;",
                                    PROCESS_NAME = "Posting Suspense Unpaid"
                                });

                                continue;
                            }
                            #endregion
                        }

                        if (doLastPosting)
                        {
                            #region ZFM_FI_ACCRUED_POSTING
                            logic.Say(loc, "ZFM_FI_ACCRUED_POSTING: {0}", data.KEYS);

                            _loc = "PostingSusUnpaid() - ZFM_FI_ACCRUED_POSTING | 2 - " + data.KEYS;
                            posting = new AccrPostData();

                            #region Header
                            posting.USERNAME = u.USERNAME;
                            posting.HEADER_TXT = data.REF_DOC;
                            posting.COMP_CODE = comp;
                            posting.DOC_DATE = data.DOC_DATE.ToString("yyyyMMdd");
                            posting.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.TRANS_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                            posting.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                            posting.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                            posting.REF_DOC_NO = FormData.AccruedNo;
                            posting.DOC_TYPE = "SA";
                            posting.GROUP_DOC = fid;
                            #endregion

                            #region Detail
                            AccrPostGL gl;
                            AccrPostVendor vd;
                            AccrPostCurr cur;
                            foreach (var d in data.Details.GetRange(4, 2))
                            {
                                if (d.dataMode == DataSAPMode.GL)
                                {
                                    gl = new AccrPostGL();
                                    gl.ITEMNO_ACC = d.SEQ_NO.str();
                                    gl.GL_ACCOUNT = d.ACCOUNT;
                                    gl.ITEM_TEXT = d.ACCOUNT_SHORT;
                                    gl.COMP_CODE = "";
                                    gl.BUS_AREA = "";
                                    gl.FIS_PERIOD = FormData.PostingDate.Value.ToString("MM");
                                    gl.FISC_YEAR = FormData.PostingDate.Value.ToString("yyyy");
                                    gl.PSTNG_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    gl.VALUE_DATE = "";
                                    gl.WBS_ELEMENT = data.WBS_NO;
                                    gl.ORDERID = "";
                                    gl.FUNDS_CTR = "";
                                    gl.COSTCENTER = data.COST_CENTER;
                                    gl.TAX_CODE = d.TX;

                                    posting.ACCOUNT_GL.Add(gl);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                                else
                                {
                                    vd = new AccrPostVendor();
                                    vd.ITEMNO_ACC = d.SEQ_NO.str();
                                    vd.VENDOR_NO = d.ACCOUNT;
                                    vd.BUS_AREA = "";
                                    vd.ITEM_TEXT = d.TEXT;
                                    vd.BLINE_DATE = FormData.PostingDate.Value.ToString("yyyyMMdd");
                                    vd.PYMT_CUR = "";
                                    vd.PYMT_AMT = "0";
                                    vd.SP_GL_IND = "U";
                                    vd.TAX_CODE = d.TX;

                                    posting.ACCOUNT_VENDOR.Add(vd);

                                    cur = new AccrPostCurr();
                                    cur.ITEMNO_ACC = d.SEQ_NO.str();
                                    cur.CURRENCY = data.CURRENCY_CODE;
                                    cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                    cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                    posting.CURRENCYAMOUNT.Add(cur);
                                }
                            }

                            #endregion

                            sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                            #region Process Result
                            if (sapRet != null)
                            {
                                if (sapRet is AccrPostResult)
                                {
                                    var ret = sapRet as AccrPostResult;

                                    logic.Say(_loc, "insert doc no : {0}-{1}", bal.BOOKING_NO, ret.DOC_NUMSAP);
                                    //InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret, db);
                                    //UpdatePostingFlag(bal.BOOKING_NO, 4, u, db);
                                    InsertSAPDoc(bal.REFF_NO.Int(), budgetYear, ret);
                                    UpdatePostingFlag(bal.BOOKING_NO, 4, u);

                                    LogSuccessSAP(_loc, ret);
                                }
                                else
                                {
                                    isSuccess = false;
                                    var ret = sapRet as List<AccrSAPError>;

                                    LogErrorSAP(_loc, ret);
                                    sapErr.AddRange(
                                        ret.Select(x => { x.PROCESS_NAME = "Posting Suspense Unpaid"; return x; })
                                        .ToList()
                                        );

                                    // if post failed, continue to next suspense
                                    continue;
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                LogErrorSAP(_loc, null);
                                sapErr.Add(new AccrSAPError
                                {
                                    TYPE = "00",
                                    NUMBER = "",
                                    MESSAGE = "SAP return null;",
                                    PROCESS_NAME = "Posting Suspense Unpaid"
                                });

                                continue;
                            }
                            #endregion Process Result

                            #endregion ZFM_FI_ACCRUED_POSTING
                        }
                    }

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    if (trans != null)
                        trans.Rollback();

                    isSuccess = false;
                    LoggingLogic.err(ex);
                }
            }
            return isSuccess;
        }

        public List<TB_R_SAP_DOC_NO> GetSAPDoc(int docNo, int docYear)
        {
            return (from r in db.TB_R_SAP_DOC_NO
                    where r.DOC_NO == docNo
                        && r.DOC_YEAR == docYear
                    select r).ToList();
        }

        private void LogSuccessSAP(string _loc, AccrPostResult ret)
        {
            logic.Say(_loc, "Success. {0}|{1}. Msg: {2}"
                , ret.DOC_NUMSAP
                , ret.FISC_YEAR
                , ret.MESSAGE);
        }
        private void LogSuccessSAP(string _loc, AccrClearingResult ret)
        {
            logic.Say(_loc, "Success. {0}|{1}. "
                , ret.BELNR
                , ret.NOCLR);
        }
        private void LogErrorSAP(string _loc, List<AccrSAPError> ret)
        {
            if (ret == null)
            {
                logic.Say(_loc, "return NULL");
            }
            else
            {
                foreach(var err in ret)
                {
                    logic.Say(_loc, "Failed. {0}. {1}: {2}"
                        , err.TYPE
                        , err.NUMBER
                        , err.MESSAGE);
                }
            }
        }

        private void InsertSAPDoc(int docNo, int docYear, AccrPostResult sapRet, AccrFormData formData = null, ELVIS_DBEntities DB = null)
        {
            if (DB == null)
                DB = db;

            var listSAPDoc = GetSAPDoc(docNo, docYear);
            int maxItemNo = 0; 
            if(listSAPDoc != null && listSAPDoc.Count > 0)
                maxItemNo = listSAPDoc.Max(x => x.ITEM_NO);
            maxItemNo++;

            int sapDocYear = sapRet.FISC_YEAR.Int();

            TB_R_SAP_DOC_NO sapDoc = new TB_R_SAP_DOC_NO();
            sapDoc.DOC_NO = docNo;
            sapDoc.DOC_YEAR = docYear;
            sapDoc.ITEM_NO = maxItemNo;
            sapDoc.SAP_DOC_NO = sapRet.DOC_NUMSAP;
            sapDoc.SAP_DOC_YEAR = sapDocYear;

            DB.TB_R_SAP_DOC_NO.AddObject(sapDoc);
            DB.SaveChanges();

            if (formData != null && formData.PVTypeCode != 2)
            {
                var d = (from det in DB.TB_R_ACCR_LIST_D
                         join doc in DB.TB_R_ACCR_DOC_NO on det.BOOKING_NO equals doc.ACCR_NO
                         where det.ACCRUED_NO == formData.AccruedNo
                             && doc.ACCR_DOC_NO == docNo
                         select det).ToList();

                if (d != null && d.Count > 0)
                {
                    foreach (var a in d)
                    {
                        a.SAP_ITEM_NO = maxItemNo;
                    }
                    DB.SaveChanges(); 
                }
            }
        }
        private void InsertSAPDoc(int docNo, int docYear, AccrClearingResult sapRet, ELVIS_DBEntities DB = null)
        {
            if (DB == null)
                DB = db;

            var listSAPDoc = GetSAPDoc(docNo, docYear);
            int maxItemNo = listSAPDoc.Max(x => x.ITEM_NO);

            TB_R_SAP_DOC_NO sapDoc = new TB_R_SAP_DOC_NO();
            sapDoc.DOC_NO = docNo;
            sapDoc.DOC_YEAR = docYear;
            sapDoc.ITEM_NO = maxItemNo + 1;
            sapDoc.SAP_DOC_NO = sapRet.NOCLR;
            //sapDoc.SAP_DOC_YEAR = sapRet.FISC_YEAR.Int();

            DB.TB_R_SAP_DOC_NO.AddObject(sapDoc);
            DB.SaveChanges();
        }
        private void UpdatePostingFlag(string bookingNo, int postingFlag, UserData userData, ELVIS_DBEntities DB = null)
        {
            if (DB == null)
                DB = db;

            var bal = (from b in DB.TB_R_ACCR_BALANCE
                       where b.BOOKING_NO == bookingNo
                       select b).FirstOrDefault();

            if (bal != null)
            {
                bal.POSTING_FLAG = postingFlag;
                bal.CHANGED_BY = userData.USERNAME;
                bal.CHANGED_DT = DateTime.Now;

                DB.SaveChanges();
            }
        }
        #endregion

        #region Close Balance to SAP
        public bool CloseBalanceToSAP(string BookingNo, DateTime PostingDate, UserData u, ref List<AccrSAPError> sapErr)
        {
            bool isSuccess = true;
            string fid = resx.FunctionId("FCN_ACCRUED_CLOSE_BAL");
            string loc = "CloseBalanceToSAP()";

            logic.Say(loc, "Start({0})", BookingNo);

            var closing = logic.AccrForm.GetBalSimulationData(BookingNo);
            string comp = logic.Sys.GetNum("COMPANY_INFO/COMPANY_CODE", 200).str();

            if (closing == null || !closing.Any())
            {
                logic.Say(loc, "no Balance data to be closed");
                return true;
            }

            DbTransaction trans = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    trans = db.Connection.BeginTransaction();

                    List<string> listBooking = BookingNo.Split(',').ToList();

                    var balance = (from b in db.TB_R_ACCR_BALANCE
                                   where listBooking.Contains(b.BOOKING_NO)
                                   select b).ToList();

                    if (balance.Any(x => !x.SAP_DOC_NO_CLOSE.isEmpty()))
                    {
                        logic.Say(loc, "Any booking already closed");
                        return false;
                    }

                    foreach (var data in closing)
                    {
                        AccrPostData posting = null;
                        object sapRet = null;

                        #region ZFM_FI_ACCRUED_POSTING
                        logic.Say(loc, "ZFM_FI_ACCRUED_POSTING: {0}", data.KEYS);

                        posting = new AccrPostData();

                        #region Header
                        posting.USERNAME = u.USERNAME;
                        posting.HEADER_TXT = data.REF_DOC;
                        posting.COMP_CODE = comp;
                        posting.DOC_DATE = data.DOC_DATE.ToString("yyyyMMdd");
                        posting.PSTNG_DATE = PostingDate.ToString("yyyyMMdd");
                        posting.TRANS_DATE = PostingDate.ToString("yyyyMMdd");
                        posting.FISC_YEAR = PostingDate.ToString("yyyy");
                        posting.FIS_PERIOD = PostingDate.ToString("MM");
                        posting.REF_DOC_NO = data.REF_DOC;
                        posting.DOC_TYPE = "SA";
                        posting.GROUP_DOC = fid;
                        #endregion

                        #region Detail
                        AccrPostGL gl;
                        AccrPostVendor vd;
                        AccrPostCurr cur;
                        foreach (var d in data.Details)
                        {
                            if (d.dataMode == DataSAPMode.GL)
                            {
                                gl = new AccrPostGL();
                                gl.ITEMNO_ACC = d.SEQ_NO.str();
                                gl.GL_ACCOUNT = d.ACCOUNT;
                                gl.ITEM_TEXT = d.ACCOUNT_SHORT;
                                gl.COMP_CODE = "";
                                gl.BUS_AREA = "";
                                gl.FIS_PERIOD = PostingDate.ToString("MM");
                                gl.FISC_YEAR = PostingDate.ToString("yyyy");
                                gl.PSTNG_DATE = PostingDate.ToString("yyyyMMdd");
                                gl.VALUE_DATE = "";
                                gl.WBS_ELEMENT = d.WBS_NO;
                                gl.ORDERID = "";
                                gl.FUNDS_CTR = "";
                                gl.COSTCENTER = d.COST_CENTER;
                                gl.TAX_CODE = d.TX;

                                posting.ACCOUNT_GL.Add(gl);

                                cur = new AccrPostCurr();
                                cur.ITEMNO_ACC = d.SEQ_NO.str();
                                cur.CURRENCY = data.CURRENCY_CODE;
                                cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                posting.CURRENCYAMOUNT.Add(cur);
                            }
                            else
                            {
                                vd = new AccrPostVendor();
                                vd.ITEMNO_ACC = d.SEQ_NO.str();
                                vd.VENDOR_NO = d.ACCOUNT;
                                vd.BUS_AREA = "";
                                vd.ITEM_TEXT = d.ACCOUNT_SHORT;
                                vd.BLINE_DATE = PostingDate.ToString("yyyyMMdd");
                                vd.PYMT_CUR = "";
                                vd.PYMT_AMT = "0";
                                vd.SP_GL_IND = "U";
                                vd.TAX_CODE = d.TX;

                                posting.ACCOUNT_VENDOR.Add(vd);

                                cur = new AccrPostCurr();
                                cur.ITEMNO_ACC = d.SEQ_NO.str();
                                cur.CURRENCY = data.CURRENCY_CODE;
                                cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                                cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                                posting.CURRENCYAMOUNT.Add(cur);
                            }
                        }

                        #endregion

                        sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, loc);

                        #region Process Result
                        if (sapRet != null)
                        {
                            if (sapRet is AccrPostResult)
                            {
                                var ret = sapRet as AccrPostResult;

                                var bals = balance.Where(x => x.DIVISION_ID.Equals(data.KEYS));

                                foreach (var bal in bals)
                                {
                                    // update doc no
                                    bal.SAP_DOC_NO_CLOSE = ret.DOC_NUMSAP;
                                    bal.SAP_DOC_YEAR_CLOSE = ret.FISC_YEAR.Int();
                                    bal.BOOKING_STS = 1;
                                    bal.CHANGED_DT = DateTime.Now;
                                    bal.CHANGED_BY = u.USERNAME;

                                    var a = logic.AccrBalance.GetBalanceByBookingNo(bal.BOOKING_NO);
                                    //InsertSAPDoc(a.REFF_NO.Int(), a.CREATED_DT.Year, ret, db);
                                    InsertSAPDoc(a.REFF_NO.Int(), a.CREATED_DT.Year, ret);
                                }

                                db.SaveChanges();

                                LogSuccessSAP(loc, ret);
                            }
                            else
                            {
                                isSuccess = false;
                                var ret = sapRet as List<AccrSAPError>;

                                LogErrorSAP(loc, ret);
                                sapErr.AddRange(
                                    ret.Select(x => { x.PROCESS_NAME = "Closing Balance"; return x; })
                                    .ToList()
                                    );
                            }
                        }
                        else
                        {
                            isSuccess = false;
                            LogErrorSAP(loc, null);
                            sapErr.Add(new AccrSAPError
                            {
                                TYPE = "00",
                                NUMBER = "",
                                MESSAGE = "SAP return null;",
                                PROCESS_NAME = "Closing Balance"
                            });
                        }
                        #endregion Process Result

                        #endregion ZFM_FI_ACCRUED_POSTING
                    }

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    if (trans != null)
                        trans.Rollback();
                    LoggingLogic.err(ex);
                }
            }
            return isSuccess;
        }
        #endregion
    
        public List<AccrFormDetail> getDescription (List<AccrFormDetail> listAccr)
        {
            var q = (from la in listAccr
                         join vw in db.vw_WBS on la.WbsNumber equals vw.WbsNumber into ds
                         from vw in ds.DefaultIfEmpty()
                     select new AccrFormDetail()
                     {
                         BookingNo = la.BookingNo,
                         WbsNumber = la.WbsNumber,
                         WbsDesc = vw.Description,
                         SuspenseNo = la.SuspenseNo,
                         PVType = la.PVType,
                         PVTypeCd = la.PVTypeCd,
                         ActivityDescription = la.ActivityDescription,
                         CostCenterCode = la.CostCenterCode,
                         CurrencyCode = la.CurrencyCode,
                         Amount = la.Amount,
                         AmountIdr = la.AmountIdr
                     }).Distinct();
            IEnumerable<AccrFormDetail> filteredList = q
              .GroupBy(lst => new { lst.WbsNumber, lst.PVTypeCd, lst.CostCenterCode,lst.CurrencyCode,lst.Amount,lst.SuspenseNo})
              .Select(group => group.First());

            if (filteredList.Any())
            {
                return filteredList.ToList();
            }
            else
            {
                return null;
            }
        }
        public int GetLatestStatus(string AccruedNo)
        {
            return Qu<int>("GetAccruedLastStatus", AccruedNo).FirstOrDefault();
        }

        public bool PVAccrPostingDirect(List<string> keys, UserData u, ref List<AccrSAPError> sapErr)
        {
            string fid = resx.FunctionId("FCN_ACCRUED_POST_PV_DIRECT");
            string _loc = "PVAccrPostingDirect()";
            bool isSuccess = true;

            logic.Say(_loc, "Start");

            foreach (string pvNoYear in keys)
            { 
                var ny = pvNoYear.Split(':');
                string pvNo = ny[0];
                string pvYear = ny[1];

                PVFormSimulationData dataDirect =
                    logic.PVForm.GetSimulationData(pvNo.ToString(), pvYear.ToString())
                    .FirstOrDefault();
                string comp = logic.Sys.GetNum("COMPANY_INFO/COMPANY_CODE", 200).str();

                DateTime postingDate = dataDirect.POSTING_DATE.Value;

                AccrPostData posting = new AccrPostData();

                #region Header
                posting.USERNAME = u.USERNAME;
                posting.HEADER_TXT = dataDirect.KEYS;
                posting.COMP_CODE = comp;
                posting.DOC_DATE = dataDirect.PV_DATE.ToString("yyyyMMdd");
                posting.PSTNG_DATE = dataDirect.POSTING_DATE.Value.ToString("yyyyMMdd");
                posting.TRANS_DATE = dataDirect.POSTING_DATE.Value.ToString("yyyyMMdd");
                posting.FISC_YEAR = dataDirect.POSTING_DATE.Value.ToString("yyyy");
                posting.FIS_PERIOD = dataDirect.POSTING_DATE.Value.ToString("MM");
                posting.REF_DOC_NO = "";
                posting.DOC_TYPE = "SA";
                posting.GROUP_DOC = fid;
                #endregion

                #region GL - Amount

                AccrPostGL gl;
                AccrPostVendor vd;
                AccrPostCurr cur;
                foreach (var d in dataDirect.Details)
                {
                    if (d.dataMode == DataSAPMode.GL)
                    {
                        gl = new AccrPostGL();
                        gl.ITEMNO_ACC = d.SEQ_NO.str();
                        gl.GL_ACCOUNT = d.ACCOUNT;
                        gl.ITEM_TEXT = d.ACCOUNT_SHORT;
                        gl.COMP_CODE = "";
                        gl.BUS_AREA = "";
                        gl.FIS_PERIOD = postingDate.ToString("MM");
                        gl.FISC_YEAR = postingDate.ToString("yyyy");
                        gl.PSTNG_DATE = postingDate.ToString("yyyyMMdd");
                        gl.VALUE_DATE = "";
                        gl.WBS_ELEMENT = "";
                        gl.ORDERID = "";
                        gl.FUNDS_CTR = "";
                        gl.COSTCENTER = "";
                        gl.TAX_CODE = d.TX;

                        posting.ACCOUNT_GL.Add(gl);

                        cur = new AccrPostCurr();
                        cur.ITEMNO_ACC = d.SEQ_NO.str();
                        cur.CURRENCY = dataDirect.CURRENCY_CODE;
                        cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                        cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                        posting.CURRENCYAMOUNT.Add(cur);
                    }
                    else
                    {
                        vd = new AccrPostVendor();
                        vd.ITEMNO_ACC = d.SEQ_NO.str();
                        vd.VENDOR_NO = d.ACCOUNT;
                        vd.BUS_AREA = "";
                        vd.ITEM_TEXT = d.ACCOUNT_SHORT;
                        vd.BLINE_DATE = postingDate.ToString("yyyyMMdd");
                        vd.PYMT_CUR = "";
                        vd.PYMT_AMT = "0";
                        vd.SP_GL_IND = "U";
                        vd.TAX_CODE = d.TX;

                        posting.ACCOUNT_VENDOR.Add(vd);

                        cur = new AccrPostCurr();
                        cur.ITEMNO_ACC = d.SEQ_NO.str();
                        cur.CURRENCY = dataDirect.CURRENCY_CODE;
                        cur.AMT_DOCCUR = Math.Abs(d.AMOUNT_DEC).ToString("F0");
                        cur.DEBIT_CREDIT = d.AMOUNT_DEC >= 0 ? "S" : "H";

                        posting.CURRENCYAMOUNT.Add(cur);
                    }
                }
                #endregion

                object sapRet = logic.SAP.AccruedPosting(posting, u.USERNAME, fid, _loc);

                #region Process Result
                if (sapRet != null)
                {
                    if (sapRet is AccrPostResult)
                    {
                        isSuccess = true;
                        var ret = sapRet as AccrPostResult;

                        DbTransaction trans = null;
                        using (ContextWrap co = new ContextWrap())
                        {
                            try
                            {
                                var db = co.db;
                                trans = db.Connection.BeginTransaction();

                                int no = pvNo.Int();
                                int year = pvYear.Int();

                                var listSAPDoc = GetSAPDoc(no, year);
                                int maxItemNo = 0;
                                if (listSAPDoc != null && listSAPDoc.Count > 0)
                                    maxItemNo = listSAPDoc.Max(x => x.ITEM_NO);
                                maxItemNo++;

                                TB_R_SAP_DOC_NO doc = new TB_R_SAP_DOC_NO();
                                doc.DOC_NO = no;
                                doc.DOC_YEAR = year;
                                doc.ITEM_NO = maxItemNo;
                                doc.SAP_DOC_NO = ret.DOC_NUMSAP;
                                doc.SAP_DOC_YEAR = ret.FISC_YEAR.Int();

                                db.TB_R_SAP_DOC_NO.AddObject(doc);
                                db.SaveChanges();

                                trans.Commit();
                            }
                            catch (Exception e)
                            {
                                if (trans != null)
                                    trans.Rollback();

                                LoggingLogic.err(e);
                            }
                        }
                        LogSuccessSAP(_loc, ret);
                    }
                    else
                    {
                        isSuccess = false;
                        var ret = sapRet as List<AccrSAPError>;

                        LogErrorSAP(_loc, ret);
                        sapErr.AddRange(
                            ret.Select(x => { x.PROCESS_NAME = "Posting PV"; return x; })
                            .ToList()
                            );
                    }
                }
                else
                {
                    isSuccess = false;
                    LogErrorSAP(_loc, null);
                    sapErr.Add(new AccrSAPError
                    {
                        TYPE = "00",
                        NUMBER = "",
                        MESSAGE = "SAP return null;",
                        PROCESS_NAME = "Posting PV"
                    });
                }
                #endregion

            }

            return isSuccess;
        }

        public List<AccrFormHistory> getApprovalHistory(string accrNo, int stsCode, UserData userData)
        {
            var q = (from tradn in db.TB_R_ACCR_DOC_NO
                     join trds in db.TB_R_DISTRIBUTION_STATUS on tradn.ACCR_DOC_NO equals trds.REFF_NO
                     join tms in db.TB_M_STATUS on trds.STATUS_CD equals tms.STATUS_CD
                     join vu in db.vw_User on trds.PROCCED_BY equals vu.USERNAME
                     where tradn.ACCR_NO == accrNo
                     select new AccrFormHistory
                     {
                         ROLE_ID = vu.ROLE_ID,
                         ROLE_NAME = vu.ROLE_NAME,
                         NAME = vu.FULL_NAME,
                         STATUS = 0,
                         STATUS_NAME = tms.STATUS_NAME,
                         STATUS_CD = trds.STATUS_CD,
                         ACTUAL_DT = trds.ACTUAL_DT,
                         CLASS = trds.CLASS
                     }).Distinct().ToList();

            int StatusX = stsCode;
            if (stsCode > 0)
            {
                StatusX = logic.Look.GetApproveStatus(stsCode);
            }

            foreach (var al in q)
            {
                int status = 0;
                if (al.ACTUAL_DT != null)
                    status = 1;

                string pos = al.CLASS.str();

                DateTime? actual = al.ACTUAL_DT;
                if (StatusX > 0)
                {
                    if (actual == null)
                    {
                        if (al.STATUS_CD <= StatusX && stsCode == StatusX)
                        {
                            
                            status = 1;
                            actual = DateTime.Now;
                        }
                        else if (al.STATUS_CD == StatusX && stsCode != StatusX)
                        {
                            status = 2;
                            
                        }
                    }
                }

                al.STATUS = status;
                al.ACTUAL_DT = actual;
            }

            return q;
        }
    }

}