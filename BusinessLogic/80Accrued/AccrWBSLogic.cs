﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.Objects;
using Common;
using Common.Data;
using BusinessLogic.CommonLogic;
using DataLayer.Model;
using Common.Data._80Accrued;
using BusinessLogic.VoucherForm;
using Common.Function;
using Dapper;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace BusinessLogic._80Accrued
{
    public class AccrWBSLogic : FormPersistence
    {
        //protected List<string> _errs;

        public CommonExcelUpload ceu = null;
        protected string me = "";
        protected LogicFactory logic = LogicFactory.Get();
        protected LoggingLogic log = new LoggingLogic();
        protected readonly string _INF = "MSTD00001INF";
        protected List<string> errs = new List<string>();

        public IQueryable<AccruedListDetail> Search(string accruedNo)
        {
            db.CommandTimeout = 3600;
            decimal dummy = 0; //default before implement SAP
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     //join vw in db.vw_WBS on trab.WBS_NO_OLD equals vw.WbsNumber into a
                     //from vw in a.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO
                     select new AccruedListDetail()
                     {
                         BOOKING_NO = trab.BOOKING_NO,
                         PV_TYPE_CD = trald.PV_TYPE_CD,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC = (from vw in db.vw_WBS
                                     where vw.WbsNumber == trab.WBS_NO_OLD
                                     select vw.Description).FirstOrDefault(),
                         INIT_AMT = trab.INIT_AMT,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         ACCRUED_NO = trald.ACCRUED_NO,
                         SAP_AVAILABLE = dummy,
                         SAP_REMAINING = trab.INIT_AMT - dummy
                     }).Distinct(); // distinct wbs no old di vw_WBS
            //get data only when pv type = PR
            q = q.Where(p => p.PV_TYPE_CD == 4);

            if (!String.IsNullOrEmpty(accruedNo))
            {
                q = q.Where(p => p.ACCRUED_NO == accruedNo);
            }

            return q;
        }

        public IQueryable<AccruedWBSListDetail> GetHeader(string accruedNo)
        {
            db.CommandTimeout = 3600;
            var q = (from tralh in db.TB_R_ACCR_LIST_H
                     join tms in db.TB_M_STATUS on tralh.STATUS_CD equals tms.STATUS_CD
                     join trald in db.TB_R_ACCR_LIST_D on tralh.ACCRUED_NO equals trald.ACCRUED_NO into a
                     from trald in a.DefaultIfEmpty()
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trald.PV_TYPE_CD equals tmapt.PV_TYPE_CD into b
                     from tmapt in b.DefaultIfEmpty()
                     join vd in db.vw_Division on tralh.DIVISION_ID equals vd.DIVISION_ID into e
                     from vd in e.DefaultIfEmpty()
                     select new AccruedWBSListDetail()
                     {
                         ACCRUED_NO = tralh.ACCRUED_NO,
                         DIVISION_NAME = vd.DIVISION_NAME,
                         UPDATED_DT = tralh.CHANGED_DT,
                         PV_TYPE_CD = trald.PV_TYPE_CD,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME,
                         STATUS_NAME = tms.STATUS_NAME
                     });

            //q = q.OrderBy(p => p.WBS_NO);
            q = q.Where(p => p.PV_TYPE_CD == 4);
            if (!String.IsNullOrEmpty(accruedNo))
            {
                q = q.Where(p => p.ACCRUED_NO == accruedNo);
            }

            return q;
        }

        public List<WBSStructure> getWbsNumbers()
        {

            List<WBSStructure> lstWbsResult = new List<WBSStructure>();

            try
            {
                //string fWbs = "wbs_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
                StringBuilder sql = new StringBuilder(
                            "SELECT DISTINCT v.WbsNumber, v.[Description] \r\n" +
                            "  FROM dbo.vw_WBS v \r\n" +
                            " "
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );

               
                lstWbsResult = Db.Query<WBSStructure>(sql.ToString()).ToList();
                //LoggingLogic.say(fWbs, sql.ToString());

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }

        public bool UpdateDetail(List<AccruedListDetail> ListItem, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();


                    foreach (AccruedListDetail d in ListItem)
                    {
                        if (d.BOOKING_NO != null)
                        {
                            var set_d = (from i in db.TB_R_ACCR_BALANCE
                                         where i.BOOKING_NO == d.BOOKING_NO
                                         select i).FirstOrDefault();
                            if (set_d != null)
                            {
                                if (set_d.WBS_NO_PR != d.WBS_NO_PR)
                                {
                                    set_d.WBS_NO_PR = d.WBS_NO_PR;
                                    set_d.CHANGED_BY = userData.USERNAME;
                                    set_d.CHANGED_DT = DateTime.Now;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;

        }

        public decimal GetAmountWBSSAP(string WBSNo)
        {
            return Qx<decimal>("GetAmountWbsSap", new { WbsNo = WBSNo }).FirstOrDefault();
        }

        public void CompleteAccrWBS(string accrNo)
        {
            var q = Qx<string>("CompleteAccrWBS", new { accrNo = accrNo });
        }
		
		public string GenerateListAccrWBSResult(
            string _username,
            string templateFile,
            string newFile,
            string c,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "WBS_Update";

            x.MarkPage(_sheetName);

            //Get data from search criteria
            var qh = GetHeader(c);
            AccruedWBSListDetail dataHeader = qh.FirstOrDefault();

            //Set value Header
            string divisionName = "";
            string accrNo = "";
            string updatedDt = "";
            string pvType = "";
            string printedBy = "";
            string printedDt = "";

            printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            printedDt = DateTime.Now.ToString("dd-MM-yyyy");
            divisionName = dataHeader.DIVISION_NAME;
            accrNo = dataHeader.ACCRUED_NO;
            updatedDt = dataHeader.UPDATED_DT.HasValue
                            ? dataHeader.UPDATED_DT.Value.ToString("dd-MM-yyyy")
                            : "";
            pvType = dataHeader.PV_TYPE_NAME;

            x.MarkCells("[DIVISION],[ACCRUED_NO],[UPDATED_DT],[PV_TYPE],[PRINTED_BY],[PRINTED_DT]");
            x.PutCells(divisionName, accrNo, updatedDt, pvType, printedBy, printedDt);
            //End set value Header

            //Get Data Detail
            var qq = Search(c);
            List<AccruedListDetail> _list =
                qq.Distinct().ToList();

            //Set Value Detail
            x.MarkRow("[BOOKING_NO],[WBS_NO_OLD],[WBS_DESC],"
                + "[INIT_AMT],[WBS_NO_PR],"
                + "[SAP_AVAILABLE],[SAP_REMAINING]");

            int j = 0;

            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedListDetail dx in _list)
            {
                //Input Data to list detail
                decimal sapAvaiable = GetAmountWBSSAP(dx.WBS_NO_PR);
                decimal? sapRemaining = sapAvaiable - dx.INIT_AMT;
                if (sapAvaiable == 0)
                {
                    x.PutRowInsert(dx.BOOKING_NO, dx.WBS_NO_OLD, dx.WBS_DESC,
                    dx.INIT_AMT, dx.WBS_NO_PR, "", "");
                }
                else
                {
                    x.PutRowInsert(dx.BOOKING_NO, dx.WBS_NO_OLD, dx.WBS_DESC,
                    dx.INIT_AMT, dx.WBS_NO_PR, sapAvaiable, sapRemaining);
                }

                

                j++;
            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "", "");
            }
            //End set value Detail

            x.PutTail();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            return filePath;
            //if (String.IsNullOrEmpty(pdfExport))
            //{
            //    return filePath;
            //}
            //else
            //{
            //    string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
            //    bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
            //    if (rslt)
            //    {
            //        return pdfPath;
            //    }
            //    else
            //    {
            //        return filePath;
            //    }

            //}
        }

        public void DeleteTempAccruedWBS(int pid)
        {

            var q = Qx<string>("DeleteStagingAccrWBS", new { pid = pid });
        }


        public bool Upload(string accrNo, string xlsFilename, string metaFile,
            ref string[][] derr,
            int TransactionCd,
            UserData ux, int pid)
        {
            bool Ok = false;
            me = "AccrWBSLogic.Upload";

            _errs.Clear();

            if (!System.IO.File.Exists(xlsFilename))
            {
                _errs.Add("Uploaded File not found");
            }

            string _meta = System.IO.File.ReadAllText(metaFile);
            if (string.IsNullOrEmpty(_meta))
            {
                _errs.Add("Metadata not found");
            }
            if (_errs.Count > 0) return false;

            ceu = new CommonExcelUpload();
            ceu.UserName = ux.USERNAME;
            ceu.PID = pid;

            ceu.LogWriter = logic.Msg.WriteMessage;
            Dictionary<string, object> _data;
            _data = ceu.Read(_meta, xlsFilename);

            if (_data.Keys.Count < 1)
            {
                _errs.Add("Empty file");
                return false;
            }

            // validate mandatory field and field length  
            // => this is *very* critical because process will fail 
            // when inserted data is not valid
            Ok = ceu.Check();

            // validate Detail 
            DataTable x = ceu.Rows;

            if (!Ok)
            {
                ShowFeedback(ceu, x, ref derr);
                return false;

                /// FOR TESTING OVERRIDE 
                /// Ok = true;
            }

            PutUploadAccr(accrNo, x, TransactionCd, pid, xlsFilename, ux, ref derr); /// formerly use steps in PutData

            Ok = (_errs.Count < 1);

            return Ok;
        }

        public bool PutUploadAccr(string accrNo, DataTable x
                , int TransactionCd
                , int pid
                , string xlsFilename
                , UserData ux
            // output
                , ref string[][] derr)
        {
            //string spName = Qx<string>("GetSpFromTx",
            //        new { tt = TransactionCd.str() }).FirstOrDefault();
            string vendorCd = null;
            Exec("sp_PutUploadH",
                    new
                    {
                        pid = pid,
                        transactionCd = TransactionCd,
                        vendorCd = vendorCd,
                        uploadFileName = Path.GetFileName(xlsFilename),
                        uid = ux.USERNAME
                    },
                        null, null, CommandType.StoredProcedure);

            //if (!spName.isEmpty() && x != null)
            if(x != null)
            {
                SqlBulkCopy bulk = new SqlBulkCopy(Db as SqlConnection);

                DecorateUpload(x, pid, ux.USERNAME);
                bulk.DestinationTableName = "TB_T_ACCR_UPLOAD_WBS";
                bulk.ColumnMappings.Clear();
                for (int i = 0; i < x.Columns.Count; i++)
                {
                    bulk.ColumnMappings.Add(x.Columns[i].ColumnName, x.Columns[i].ColumnName);
                }
                PreExecuteTest(x, ux, pid);

                bulk.WriteToServer(x);

                var q = Qx<string>("ValidateUploadAccrWBSList", new { accrNo = accrNo, processId = pid }).ToList();
            }


            List<ErrorUploadData> ErrUploads = Qx<ErrorUploadData>("GetErrorUploadAccrWBS", new { pid = pid }).ToList();
            if (ErrUploads != null && ErrUploads.Count > 0)
                _errs.AddRange(ErrUploads.Select(e => e.ERRMSG));
            else
            {
                CopyTempToTransaction(pid);
            }
            return true;
        }

        public void CopyTempToTransaction(int pid)
        {
            var q = Qx<string>("MoveTempAccrWBS", new { pid = pid });
        }

        public void GenerateLogAccr(int pid, UserData ux)
        {
            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();
                    string sErr = "";

                    var dis = (from d in DB.TB_T_ACCR_UPLOAD_WBS
                               select d).ToList();
                    if (dis.Any())
                    {
                        foreach (var tmp in dis)
                        {
                            sErr = tmp.ERRMSG;
                            string[] words = sErr.Split(';');
                            foreach (string word in words)
                            {
                                if (!String.IsNullOrEmpty(word))
                                {
                                    sErr = logic.Msg.WriteMessage(pid, "MSTD00002ERR", "Business Validation", ux.USERNAME, word);
                                }

                            }

                        }
                    }

                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }

        }

        public string DownloadError(int _processId, string uploadedFile, string sourceFile, string targetDirectory, string[][] e)
        {
            string fileName =
                    Util.GetTmp(targetDirectory,
                    Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(uploadedFile))
                            + "_" + _processId.str() + "_error", ".txt");

            File.WriteAllText(fileName, string.Join("\r\n", _errs));

            return Path.GetFileName(fileName);
        }

        private decimal GetSapAvailable(string WBSNo)
        {
            return logic.AccrWBS.GetAmountWBSSAP(WBSNo);
        }

        protected bool checkCompleteDetail(List<AccruedListDetail> dataDetail)
        {
            bool result = true;

            foreach (var p in dataDetail)
            {
                if (p.INIT_AMT != GetSapAvailable(p.WBS_NO_PR))
                {
                    result = false;
                }
            }

            return result;
        }

        public bool updateComplete(string accr_no, List<AccruedListDetail> dataDetail, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                bool isValid = checkCompleteDetail(dataDetail);
                if (isValid)
                {
                    try
                    {
                        var db = co.db;
                        TX = db.Connection.BeginTransaction();

                        #region update no pr
                        //update wbs no pr goes here
                        //foreach (AccruedListDetail d in dataDetail)
                        //{
                        //if (d.BOOKING_NO != null && d.WBS_NO_PR != null)
                        //{
                        //    var set_d = (from i in db.TB_R_ACCR_BALANCE
                        //                 where i.BOOKING_NO == d.BOOKING_NO
                        //                 select i).FirstOrDefault();
                        //    if (set_d != null)
                        //    {
                        //        set_d.WBS_NO_PR = d.WBS_NO_PR;
                        //        set_d.CHANGED_BY = userData.USERNAME;
                        //        set_d.CHANGED_DT = DateTime.Now;

                        //        db.SaveChanges();
                        //    }
                        //}
                        //}
                        #endregion update no pr

                        #region update status cd  accrued no
                        var q = (from h in db.TB_R_ACCR_LIST_H
                                 where h.ACCRUED_NO == accr_no
                                 select h).FirstOrDefault();

                        if (q != null)
                        {
                            q.STATUS_CD = 105;
                            q.CHANGED_DT = DateTime.Now;
                            q.CHANGED_BY = userData.USERNAME;
                        }

                        db.SaveChanges();

                        #endregion update status cd accrued no

                        TX.Commit();
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        LoggingLogic.err(ex);
                        if (TX != null)
                            TX.Rollback();
                        result = false;
                    }
                }
                
            }

            return result;
        }
    }
}
