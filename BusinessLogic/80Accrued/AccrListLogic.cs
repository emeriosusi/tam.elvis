﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using BusinessLogic.AccruedForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data._80Accrued;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;
using Dapper;
using System.Data.Objects.SqlClient;

namespace BusinessLogic._80Accrued
{
    public class AccrListLogic : LogicBase
    {
        public const byte SAP_PAID_BY_CASHIER = 1;
        public const byte SAP_CHECK_BY_ACCOUNTING = 2;
        private LogicFactory logic = LogicFactory.Get();
        protected LoggingLogic log = new LoggingLogic();
        protected readonly string _INF = "MSTD00001INF";
        protected List<string> errs = new List<string>();

        public IQueryable<AccruedListData> Search(AccruedSearchCriteria crit)
        {
            db.CommandTimeout = 3600;

            var q = (from h in db.TB_R_ACCR_LIST_H
                     join d in db.vw_Division on h.DIVISION_ID equals d.DIVISION_ID into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.TB_M_STATUS on h.STATUS_CD equals s.STATUS_CD into ss
                     from s in ss.DefaultIfEmpty()
                     join a in db.TB_R_ACCR_DOC_NO on h.ACCRUED_NO equals a.ACCR_NO into da
                     from a in da.DefaultIfEmpty()
                     join c in db.vw_CurNextApprover on a.ACCR_DOC_NO equals c.REFF_NO into ac
                     from c in ac.DefaultIfEmpty()

                     select new AccruedListData()
                     {
                         ACCRUED_NO = h.ACCRUED_NO,
                         DIVISION_NAME = d.DIVISION_NAME,
                         DIVISION_ID = h.DIVISION_ID,
                         CREATED_BY = h.CREATED_BY,
                         CREATED_DT = h.CREATED_DT,
                         CHANGED_BY = h.CREATED_BY,
                         CHANGED_DT = h.CHANGED_DT,
                         SUBMISSION_STATUS = s.STATUS_NAME,
                         STATUS_CD = s.STATUS_CD != null ? s.STATUS_CD : 0,
                         PIC_CURRENT = c.CURRENT_APPROVER,
                         PIC_NEXT = c.NEXT_APPROVER,
                         TOT_AMOUNT = h.TOTAL_AMT != null ? h.TOTAL_AMT.Value : 0,
                         MODULE_CD = s.MODULE_CD,
                         BUDGET_YEAR = h.BUDGET_YEAR != null ? h.BUDGET_YEAR.Value : 0,
                         WORKFLOW_STATUS = SqlFunctions.StringConvert((decimal?)h.WORKFLOW_STATUS).Trim()
                     });

            if (!crit.ignoreIssuingDivision)
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (!crit.ignoreAccruedNo)
            {
                switch (crit.iAno)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.ACCRUED_NO.EndsWith(crit.sAccrPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.ACCRUED_NO.StartsWith(crit.sAccrPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.ACCRUED_NO.Contains(crit.sAccrPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.ACCRUED_NO.StartsWith(crit.sAccrPre) && p.ACCRUED_NO.EndsWith(crit.sAccrPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.ACCRUED_NO.Contains(crit.accrNo));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!crit.ignoreDateFrom)
            {

                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT >= crit.createdDtFrom);
            }

            if (!crit.ignoreDateTo)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT <= crit.createdDtTo);
            }
            if (crit.budgetYear > 0)
            {
                q = q.Where(p => p.BUDGET_YEAR == crit.budgetYear);
            }
            if (crit.submissionSts >= 0)
            {
                q = q.Where(p => p.STATUS_CD == crit.submissionSts);
            }
            if (!crit.ignoreAmt)
            {
                q = q.Where(p => p.TOT_AMOUNT >= crit.totAmtFrom && p.TOT_AMOUNT <= crit.totAmtTo);
            }
            if (crit.workflowSts == 0 )
            { 
                q = q.Where(p => p.WORKFLOW_STATUS =="0" ||  p.WORKFLOW_STATUS =="");
            }
            if (crit.workflowSts == 1)
            {
                q = q.Where(p => p.WORKFLOW_STATUS == "1");
            }

            return q;
        }

        public IQueryable<AccruedShiftingListData> SearchByDiv(AccruedSearchCriteria crit)
        {
            db.CommandTimeout = 3600;

            var q = (from h in db.TB_R_ACCR_SHIFTING_H
                     join d in db.vw_Division on h.DIVISION_ID equals d.DIVISION_ID into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.TB_M_STATUS on h.STATUS_CD equals s.STATUS_CD into ss
                     from s in ss.DefaultIfEmpty()

                     select new AccruedShiftingListData()
                     {
                         SHIFTING_NO = h.SHIFTING_NO,
                         DIVISION_NAME = d.DIVISION_NAME,
                         DIVISION_ID = h.DIVISION_ID,
                         CREATED_BY = h.CREATED_BY,
                         CREATED_DT = h.CREATED_DT,
                         CHANGED_BY = h.CREATED_BY,
                         CHANGED_DT = h.CHANGED_DT,
                         SUBMISSION_STATUS = s.STATUS_NAME,
                         STATUS_CD = s.STATUS_CD != null ? s.STATUS_CD : 0,
                         PIC_CURRENT = "",
                         PIC_NEXT = "",
                         TOT_AMOUNT = h.TOTAL_AMT != null ? h.TOTAL_AMT.Value : 0,
                         MODULE_CD = s.MODULE_CD,
                         BUDGET_YEAR = h.BUDGET_YEAR != null ? h.BUDGET_YEAR.Value : 0,
                         WORKFLOW_STATUS = SqlFunctions.StringConvert((decimal)(h.WORKFLOW_STATUS != null ? h.WORKFLOW_STATUS.Value : 0)).Trim()
                     });

            //var allowedModule = new[] { 0, 3 };
            //q = q.Where(x => (allowedModule.Contains(x.MODULE_CD)));

            if (!crit.ignoreIssuingDivision)
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (!crit.ignoreAccruedNo)
            {
                switch (crit.iAno)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.ACCRUED_NO.EndsWith(crit.sAccrPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.ACCRUED_NO.StartsWith(crit.sAccrPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.ACCRUED_NO.Contains(crit.sAccrPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.ACCRUED_NO.StartsWith(crit.sAccrPre) && p.ACCRUED_NO.EndsWith(crit.sAccrPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.ACCRUED_NO.Contains(crit.accrNo));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!crit.ignoreDateFrom)
            {

                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT >= crit.createdDtFrom);
            }

            if (!crit.ignoreDateTo)
            {
                var _createdDtTo = (crit.createdDtTo).AddDays(1);
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT <= _createdDtTo);
            }
            if (crit.budgetYear > 0)
            {
                q = q.Where(p => p.BUDGET_YEAR == crit.budgetYear);
            }
            if (crit.submissionSts > 0)
            {
                q = q.Where(p => p.STATUS_CD == crit.submissionSts);
            }
            if (!crit.ignoreAmt)
            {
                q = q.Where(p => p.TOT_AMOUNT >= crit.totAmtFrom && p.TOT_AMOUNT <= crit.totAmtTo);
            }
            if (crit.workflowSts == 0)
            {
                q = q.Where(p => p.WORKFLOW_STATUS == "0" || p.WORKFLOW_STATUS == "");
            }
            if (crit.workflowSts == 1)
            {
                q = q.Where(p => p.WORKFLOW_STATUS == "1");
            }

            return q;
        }

        public bool isDraft(string accrNo)
        {
            var q = (from h in db.TB_R_ACCR_LIST_H
                     where h.ACCRUED_NO == accrNo
                     select h).FirstOrDefault();

            return q != null && q.STATUS_CD == 0;
        }

        public bool Delete(string accruedNo, string username)
        {
            if (String.IsNullOrEmpty(accruedNo)) return false;

            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    logic.AccrForm.DeleteAccrReffNo(accruedNo, DB);

                    var det = (from d in DB.TB_R_ACCR_LIST_D
                               where d.ACCRUED_NO == accruedNo
                               select d).ToList();
                    if (det.Any())
                    {
                        foreach (var d in det)
                        {
                            DB.DeleteObject(d);
                            DB.SaveChanges();
                        }
                    }

                    var head = (from h in DB.TB_R_ACCR_LIST_H
                                where h.ACCRUED_NO == accruedNo
                                select h).ToList();
                    if (head.Any())
                    {
                        foreach (var h in head)
                        {
                            DB.DeleteObject(h);
                            DB.SaveChanges();
                        }
                    }
                    _result = true;
                    tx.Commit();

                    logic.WorkFlow.K2DeleteProcess(accruedNo);
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public List<AccruedListDetail> GetActivityBooking(Dictionary<string, object> param)
        {
            string bookingNo = param["BOOKING_NO"].ToString();



            var q = (from d in db.TB_R_ACCR_LIST_D
                     where d.BOOKING_NO == bookingNo
                     select new AccruedListDetail
                     {
                         ACCRUED_NO = d.ACCRUED_NO,
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            if (q.Any()) return q.ToList();

            return null;
        }


        public List<AccruedListDetail> GetActivityAccrued(Dictionary<string, object> param)
        {
            string accruedNo = "";
            if (param.ContainsKey("ACCRUED_NO"))
                accruedNo = param["ACCRUED_NO"].ToString();


            var q = (from d in db.TB_R_ACCR_LIST_D
                     where d.ACCRUED_NO == accruedNo
                     select new AccruedListDetail
                     {
                         ACCRUED_NO = d.ACCRUED_NO,
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            if (param.ContainsKey("BOOKING_NO"))
            {
                string bookingNo = param["BOOKING_NO"].ToString();
                q = q.Where(p => p.BOOKING_NO == bookingNo);
            }

            if (q.Any()) return q.ToList();

            return null;
        }

        public string GetDivNameFromFolio(string code, string no, UserData userData)
        {
            string divName = userData.DIV_NAME;
            StringBuilder sql = new StringBuilder();
            List<AccruedListDetail> lst = new List<AccruedListDetail>();

            switch (code)
            {
                case "LA": // list accrued
                    sql = new StringBuilder("SELECT DISTINCT a.DIVISION_ID, DIVISION_NAME FROM TB_R_ACCR_LIST_H a LEFT JOIN vw_Division b ON a.DIVISION_ID = b.DIVISION_ID WHERE ACCRUED_NO = '" + no + "'");
                    break;
                case "BS": // shifting
                    sql = new StringBuilder("SELECT DISTINCT a.DIVISION_ID, DIVISION_NAME FROM TB_R_ACCR_SHIFTING_H a LEFT JOIN vw_Division b ON a.DIVISION_ID = b.DIVISION_ID WHERE SHIFTING_NO = '" + no + "'");
                    break;
                case "AE": // extend
                    sql = new StringBuilder("SELECT DISTINCT a.DIVISION_ID, DIVISION_NAME FROM TB_R_ACCR_EXTEND_H a LEFT JOIN vw_Division b ON a.DIVISION_ID = b.DIVISION_ID WHERE EXTEND_NO = '" + no + "'");
                    break;
                default:
                    break;
            }

            try
            {
                if (sql.ToString().isNotEmpty())
                    lst = Db.Query<AccruedListDetail>(sql.ToString()).ToList();
            }
            catch (Exception ex)
            {
                Handle(ex);
            }

            if (lst.Count > 0)
            {
                divName = lst[0].DIVISION_NAME;
            }

            return divName;
        }

        #region Download Accrued List
        //Added by fid.Taufik

        public IQueryable<AccrFormDetail> SearchDataDownload(AccruedSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from h in db.TB_R_ACCR_LIST_D
                     join d in db.TB_M_ACCR_PV_TYPE on h.PV_TYPE_CD equals d.PV_TYPE_CD into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.vw_WBS on h.WBS_NO_OLD equals s.WbsNumber into ss
                     from s in ss.DefaultIfEmpty()
                     join m in db.TB_R_ACCR_LIST_H on h.ACCRUED_NO equals m.ACCRUED_NO into mm
                     from m in mm.DefaultIfEmpty()
                     join div in db.vw_Division on m.DIVISION_ID equals div.DIVISION_ID into divl
                     from div in divl.DefaultIfEmpty()

                     select new AccrFormDetail()
                     {
                         AccruedNo = h.ACCRUED_NO,
                         BookingNo = h.BOOKING_NO,
                         WbsNumber = h.WBS_NO_OLD,
                         WbsDesc = s.Description,
                         SuspenseNo = h.SUSPENSE_NO_OLD.HasValue ? SqlFunctions.StringConvert((double)h.SUSPENSE_NO_OLD.Value) : "",
                         PVType = d.PV_TYPE_NAME,
                         ActivityDescription = h.ACTIVITY_DES,
                         CostCenterCode = h.COST_CENTER,
                         Amount = h.AMOUNT != null ? h.AMOUNT.Value : 0,
                         CurrencyCode = h.CURRENCY_CD,
                         DivisionName = div.DIVISION_NAME,
                         DivisionId = m.DIVISION_ID,
                         DivisionFulName = div.FULL_NAME,
                         AmountIdr = h.AMOUNT_IDR != null ? h.AMOUNT_IDR.Value : 0,
                         ChangedDt = m.CHANGED_DT,
                         CreatedBy = m.CREATED_BY,
                         CreatedDt = m.CREATED_DT,
                         BudgetYear = m.BUDGET_YEAR
                     }).Distinct();

            q = q.OrderBy(p => p.WbsNumber);
            if (!String.IsNullOrEmpty(crit.accrNo))
            {

                q = q.Where(p => p.AccruedNo == crit.accrNo);

            }

            return q;
        }

        //public byte[] GetExcelDownload(
        //    string _username,
        //    string templateFile,
        //    AccruedSearchCriteria c)
        //{

        //    if (!File.Exists(templateFile))
        //    {
        //        log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
        //        return null;
        //    }

        //    ExcelWriter x = new ExcelWriter(templateFile);
        //    string _sheetName = "ACCR";

        //    x.MarkPage(_sheetName);
        //    x.MarkRow("[BOOKING_NO],[WBS_NO_OLD],[WBS_DESC],[SUSPENSE_NO_OLD],"
        //        + "[PV_TYPE],[ACTIVITY_DES],[COST_CENTER],[CURRENCY_CD],[AMOUNT],[AMOUNT_IN_RP],"
        //        + "[SAP_AVAILABLE],[SAP_REMAINING]");

        //    int j = 0;


        //    var qq = SearchDataDownload(c);

        //    List<AccruedListDetail> _list =
        //        qq.ToList();
        //    Dictionary<int, string> doctype = logic.Look.GetDocType();
        //    foreach (AccruedListDetail dx in _list)
        //    {
        //        x.PutRow(dx.BOOKING_NO,dx.WBS_NO_OLD,dx.WBS_DESC,
        //                    dx.SUSPENSE_NO_OLD,dx.PV_TYPE_NAME,dx.ACTIVITY_DES,dx.COST_CENTER,
        //                    dx.CURRENCY_CD, dx.AMOUNT, dx.AMOUNT_IN_RP, 0, 0);

        //    }

        //    if (j < 1)
        //    {
        //        x.PutRow("", "", "", "", "",
        //                 "", "", "", "", "",
        //                 "", "");
        //    }

        //    x.PutTail();
        //    return x.GetBytes();
        //}

        public string GetLinkExcelDownload(
            string _username,
            string templateFile,
            string newFile,
            AccruedSearchCriteria c,
            int StatusCode,
            UserData userData,
            string pdfExport = "")
        {
            try
            {
                if (!File.Exists(templateFile))
                {
                    log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                    return null;
                }

                ExcelWriter x = new ExcelWriter(templateFile);
                string _sheetName = "ACCR";

                x.MarkPage(_sheetName);

                //Get data from search criteria
                var qq = SearchDataDownload(c);
                List<AccrFormDetail> _list = qq.ToList();

                if (StatusCode != 104 && StatusCode != 105)
                    logic.AccrForm.FillSAPAmount(_list);

                _list.Sort(AccrFormDetail.RuleOrder);

                //Set value Header
                string title = "";
                string divisionName = "";
                string divisionFullName = "";
                string accrNo = "";
                string updatedDt = "";
                string createdDt = "";
                decimal usdToIdr = 0;
                decimal jpyToIdr = 0;
                string printedBy = "";
                string printedDt = "";
                decimal totalAmount = 0;

                int budgetYear = qq.Select(xx => xx.BudgetYear ?? DateTime.Now.Year).FirstOrDefault();
                title = string.Format("LIST ACCRUED TAHUN {0}", budgetYear.str());
                printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
                printedDt = DateTime.Now.ToString("dd-MM-yyyy");
                foreach (AccrFormDetail hAccr in _list)
                {
                    divisionName = hAccr.DivisionName;
                    divisionFullName = hAccr.DivisionFulName;
                    accrNo = hAccr.AccruedNo;
                    updatedDt = hAccr.ChangedDt.HasValue
                                ? hAccr.ChangedDt.Value.ToString("dd-MM-yyyy")
                                : "";
                    createdDt = hAccr.CreatedDt.HasValue
                                ? hAccr.CreatedDt.Value.ToString("dd-MM-yyyy")
                                : "";
                    totalAmount = totalAmount + hAccr.AmountIdr;
                }

                var q = db.vw_ExchangeRate.ToList();
                if (q.Any())
                {
                    var usd = q.Where(a => a.CURRENCY_CD == "USD").FirstOrDefault();
                    if (usd != null)
                    {
                        usdToIdr = usd.EXCHANGE_RATE;
                    }

                    var jpy = q.Where(a => a.CURRENCY_CD == "JPY").FirstOrDefault();
                    if (jpy != null)
                    {
                        jpyToIdr = jpy.EXCHANGE_RATE;
                    }
                }

                x.MarkCells("[TITLE],[DIVISION],[ACCRUED_NO],[UPDATED_DT],[USD_RATE],[JPY_RATE],[PRINTED_BY],[PRINTED_DT],[TOTAL_AMOUNT]");
                x.PutCells(title, divisionFullName, accrNo, createdDt, usdToIdr.ToString("G"), jpyToIdr.ToString("G"), printedBy, printedDt, totalAmount.ToString("N0"));
                //End set value Header

                //Set Value Detail
                x.MarkRow("[WBS_NO_OLD],[WBS_DESC],[SUSPENSE_NO_OLD],"
                    + "[PV_TYPE],[ACTIVITY_DES],[COST_CENTER],[CURRENCY_CD],[AMOUNT],[AMOUNT_IN_RP],[SAP_AVAILABLE],[SAP_REMAINING]");

                int j = 0;

                Dictionary<int, string> doctype = logic.Look.GetDocType();
                foreach (AccrFormDetail dx in _list)
                {
                    //Input Data to list detail
                    x.PutRowInsert(dx.WbsNumber, dx.WbsDesc,
                            dx.SuspenseNo, dx.PVType, dx.ActivityDescription, dx.CostCenterCode,
                            dx.CurrencyCode, dx.Amount, dx.AmountIdr, dx.SapAmtAvailable, dx.SapAmtRemaining);
                    j++;
                }

                if (j < 1)
                {
                    x.PutRow("", "", "", "", "",
                             "", "", "", "", "",
                             "", "");
                }
                //End set value Detail

                x.PutTail();
                string _strTempDir = LoggingLogic.GetTempPath("");
                string filePath = System.IO.Path.Combine(_strTempDir, newFile);
                if (!Directory.Exists(_strTempDir))
                    Directory.CreateDirectory(_strTempDir);
                x.Write(filePath);

                if (String.IsNullOrEmpty(pdfExport))
                {
                    return filePath;
                }
                else
                {
                    string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                    bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                    if (rslt)
                    {
                        return pdfPath;
                    }
                    else
                    {
                        return filePath;
                    }
                }
            }
            catch (Exception e)
            {
                Handle(e);
                throw;
            }
        }
        #endregion

        #region Download Accrued WBS
        //Added by fid.Taufik
        public IQueryable<AccruedListDetail> SearchDataDownloadWBS(AccruedSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join ttsw in db.vw_WBS on trab.WBS_NO_OLD equals ttsw.WbsNumber into a
                     from ttsw in a.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO into b
                     from trald in b.DefaultIfEmpty()
                     join tralh in db.TB_R_ACCR_LIST_H on trald.ACCRUED_NO equals tralh.ACCRUED_NO into c
                     from tralh in c.DefaultIfEmpty()
                     join tmpt in db.TB_M_PV_TYPE on trald.PV_TYPE_CD equals tmpt.PV_TYPE_CD into d
                     from tmpt in d.DefaultIfEmpty()


                     select new AccruedListDetail()
                     {
                         ACCRUED_NO = trald.ACCRUED_NO,
                         BOOKING_NO = trab.BOOKING_NO,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC = ttsw.Description,
                         PV_TYPE_NAME = tmpt.PV_TYPE_NAME,
                         INIT_AMT = trab.INIT_AMT != null ? trab.INIT_AMT : 0,
                         SAP_AVAILABLE = 0, // retrieve from SAP
                         SAP_REMAINING = trab.INIT_AMT - trab.SPENT_AMT,
                         UPDATED_DT = tralh.CHANGED_DT

                     }).Distinct();

            var allowedModule = new[] { 0, 3 };

            q = q.OrderBy(p => p.WBS_NO);

            if (!String.IsNullOrEmpty(crit.issuingDivision))
            {

                //q = q.Where(p => p.DIVISION_ID == crit.issuingDivision);

            }

            if (!String.IsNullOrEmpty(crit.accrNo))
            {

                q = q.Where(p => p.ACCRUED_NO == crit.accrNo);

            }

            return q;
        }

        public IQueryable<AccruedListDetail> SearchDataDownloadPDF(AccruedSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join ttsw in db.vw_WBS on trab.WBS_NO_OLD equals ttsw.WbsNumber into a
                     from ttsw in a.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO into b
                     from trald in b.DefaultIfEmpty()
                     join tralh in db.TB_R_ACCR_LIST_H on trald.ACCRUED_NO equals tralh.ACCRUED_NO into c
                     from tralh in c.DefaultIfEmpty()
                     join tmpt in db.TB_M_PV_TYPE on trald.PV_TYPE_CD equals tmpt.PV_TYPE_CD into d
                     from tmpt in d.DefaultIfEmpty()


                     select new AccruedListDetail()
                     {
                         ACCRUED_NO = trald.ACCRUED_NO,
                         BOOKING_NO = trab.BOOKING_NO,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC = ttsw.Description,
                         PV_TYPE_NAME = tmpt.PV_TYPE_NAME,
                         INIT_AMT = trab.INIT_AMT != null ? trab.INIT_AMT : 0,
                         SAP_AVAILABLE = 0, // Retrieve SAP Amount
                         SAP_REMAINING = trab.INIT_AMT - trab.SPENT_AMT,
                         UPDATED_DT = tralh.CHANGED_DT

                     }).Distinct();

            var allowedModule = new[] { 0, 3 };
            if (!String.IsNullOrEmpty(crit.issuingDivision))
            {

                //q = q.Where(p => p.DIVISION_ID == crit.issuingDivision);

            }

            if (!String.IsNullOrEmpty(crit.accrNo))
            {

                q = q.Where(p => p.ACCRUED_NO == crit.accrNo);

            }

            return q;
        }


        public string DownloadListAccrWBS(
            string _username,
            string templateFile,
            string newFile,
            AccruedSearchCriteria c,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "ACCR";

            x.MarkPage(_sheetName);

            //Get data from search criteria
            var qq = SearchDataDownloadWBS(c);
            List<AccruedListDetail> _list =
                qq.ToList();

            //Set value Header
            string divisionName = "";
            string accrNo = "";
            string updatedDt = "";
            string pvType = "";
            string printedBy = "";
            string printedDt = "";

            printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            printedDt = DateTime.Now.ToString("dd-MM-yyyy");
            divisionName = userData.DIV_NAME;
            foreach (AccruedListDetail hAccr in _list)
            {
                accrNo = hAccr.ACCRUED_NO;
                updatedDt = hAccr.UPDATED_DT.HasValue
                            ? hAccr.UPDATED_DT.Value.ToString("dd-MM-yyyy")
                            : "";
                pvType = hAccr.PV_TYPE_NAME;

            }

            x.MarkCells("[DIVISION],[ACCRUED_NO],[UPDATED_DT],[PV_TYPE],[PRINTED_BY],[PRINTED_DT]");
            x.PutCells(divisionName, accrNo, updatedDt, pvType, printedBy, printedDt);
            //End set value Header

            //Set Value Detail
            x.MarkRow("[BOOKING_NO],[WBS_NO_OLD],[WBS_DESC],"
                + "[AMOUNT_IN_RP],[WBS_NO_NEW],"
                + "[SAP_AVAILABLE],[SAP_REMAINING]");

            int j = 0;

            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedListDetail dx in _list)
            {
                //Input Data to list detail
                x.PutRow(dx.BOOKING_NO, dx.WBS_NO_OLD, dx.WBS_DESC,
                        dx.AMOUNT_IN_RP, dx.WBS_NO, dx.SAP_AVAILABLE, 0);
                j++;

            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "");
            }
            //End set value Detail

            x.PutTail();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            if (String.IsNullOrEmpty(pdfExport))
            {
                return filePath;
            }
            else
            {
                string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                if (rslt)
                {
                    return pdfPath;
                }
                else
                {
                    return filePath;
                }

            }
        }

        public string DownloadListAccrPDF(
            string _username,
            string templateFile,
            string newFile,
            AccruedSearchCriteria c,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "General Info";

            x.MarkPage(_sheetName);

            //Get data from search criteria
            var qq = Search(c);
            int sizeForAll = qq.Count();
            List<AccruedListData> _list =
                qq.ToList();

            //Set value Header
            string downloadedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            string downloadedDate = DateTime.Now.ToString("dd MMM yyyy");
            string totalRecord = sizeForAll.ToString();

            x.MarkCells("[DOWNLOADED_BY],[DOWNLOADED_DATE],[TOTAL_RECORD]");
            x.PutCells(": " + downloadedBy, ": " + downloadedDate, ": " + totalRecord);
            //End set value Header

            //Set Value Detail
            x.MarkRow("[WORKFLOW_STATUS],[ACCRUED_NO],[ISSUING_DIVISION],[CREATED_DATE],[CREATED_BY],[UPDATED_DATE],[UPDATED_BY],[SUBMISSION_STATUS],[PIC_CURRENT],[PIC_NEXT],[TOTAL_AMOUNT]");
            int j = 1;
            Dictionary<int, string> doctype = logic.Look.GetDocType();

            foreach (AccruedListData dx in _list)
            {
                //Input Data to list detail
                x.PutRow(dx.WORKFLOW_STATUS.isNotEmpty() ? logic.Sys.GetText("WORKFLOW_STATUS", dx.WORKFLOW_STATUS) : "",
                         dx.ACCRUED_NO,
                         dx.DIVISION_NAME,
                         dx.CREATED_DT.ToString("dd.MM.yyyy"),
                         dx.CREATED_BY,
                         dx.CHANGED_DT.HasValue ? dx.CHANGED_DT.Value.ToString("dd.MM.yyyy") : "",
                         dx.CHANGED_BY,
                         dx.SUBMISSION_STATUS,
                         dx.PIC_CURRENT,
                         dx.PIC_NEXT,
                         dx.TOT_AMOUNT.ToString("N0")
                    //String.Format("Rp. {0:#.00}", dx.TOT_AMOUNT)
                    );
                j++;
            }

            if (j < 2)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "", "", "", "");
            }
            //End set value Detail

            x.PutTail();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            return filePath;

            //if (String.IsNullOrEmpty(pdfExport))
            //{
            //    return filePath;
            //}
            //else
            //{
            //    string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
            //    bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
            //    if (rslt)
            //    {
            //        return pdfPath;
            //    }
            //    else
            //    {
            //        return filePath;
            //    }

            //}
        }
        #endregion

        #region Download List Accrued Result
        //Added by fid.Taufik

        public IQueryable<AccruedListDetail> DataListAccrResult(AccruedResultSearch crit)
        {
            db.CommandTimeout = 3600;
            /*
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trab.PV_TYPE_CD equals tmapt.PV_TYPE_CD into d
                     from tmapt in d.DefaultIfEmpty()
                     join ttsw in db.vw_WBS on trab.WBS_NO_OLD equals ttsw.WbsNumber into a
                     from ttsw in a.DefaultIfEmpty()
                     join ttsw1 in db.vw_WBS on trab.WBS_NO_PR equals ttsw1.WbsNumber into a1
                     from ttsw1 in a1.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO into b
                     from trald in b.DefaultIfEmpty()
                     join tralh in db.TB_R_ACCR_LIST_H on trald.ACCRUED_NO equals tralh.ACCRUED_NO into c
                     from tralh in c.DefaultIfEmpty()
                     join vd in db.vw_Division on tralh.DIVISION_ID equals vd.DIVISION_ID into e
                     from vd in e.DefaultIfEmpty()
                     where trald.ACCRUED_NO == crit.AccruedNo 
                     //&& (trab.WBS_NO_OLD != null || trab.WBS_NO_OLD != "") && (trab.WBS_NO_PR != null || trab.WBS_NO_PR != "")
                     select new AccruedListDetail()
                     {
                         ACCRUED_NO = trald.ACCRUED_NO,
                         BOOKING_NO = trab.BOOKING_NO,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_NO = trab.WBS_NO_PR,
                         WBS_DESC = ttsw.Description.Trim(),
                         WBS_DESC_PR = ttsw1.Description.Trim(),
                         SUSPENSE_NO_OLD = trab.SUSPENSE_NO_OLD,
                         SUSPENSE_NO_PR = trab.SUSPENSE_NO_NEW,
                         INIT_AMT = trald.AMOUNT_IDR,
                         SAP_SPENT = trab.SPENT_AMT,
                         SAP_REMAINING = trab.INIT_AMT - trab.SPENT_AMT,
                         UPDATED_DT = tralh.CHANGED_DT,
                         DIVISION_NAME = vd.DIVISION_NAME,
                         PV_TYPE_CD = trab.PV_TYPE_CD,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME,
                         ACTIVITY_DES = trald.ACTIVITY_DES,
                         SAP_DOC_NO = trab.BOOKING_STS == 0 ? !string.IsNullOrEmpty(trab.SAP_DOC_NO_OPEN) ? trab.SAP_DOC_NO_OPEN.Replace(";", ";<br/>") : trab.SAP_DOC_NO_OPEN :
                                !string.IsNullOrEmpty(trab.SAP_DOC_NO_CLOSE) ? trab.SAP_DOC_NO_CLOSE.Replace(";", ";<br/>") : trab.SAP_DOC_NO_CLOSE 
                         

                     }).Distinct();
             */
            var q = (from v in db.vw_DataDetailResultList
                     join tralh in db.TB_R_ACCR_LIST_H on v.ACCRUED_NO equals tralh.ACCRUED_NO
                     join vd in db.vw_Division on tralh.DIVISION_ID equals vd.DIVISION_ID
                     join trab in db.TB_R_ACCR_BALANCE on v.BOOKING_NO equals trab.BOOKING_NO
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trab.PV_TYPE_CD equals tmapt.PV_TYPE_CD
                     where v.ACCRUED_NO == crit.AccruedNo
                     select new AccruedListDetail()
                     {
                         ACCRUED_NO = v.ACCRUED_NO,
                         BOOKING_NO = v.BOOKING_NO,
                         WBS_NO_OLD = v.WBS_NO_OLD,
                         WBS_NO = v.WBS_NO_PR,
                         WBS_DESC = v.WBS_NO_OLD_DESC.Trim(),
                         WBS_DESC_PR = v.WBS_NO_PR_DESC.Trim(),
                         SUSPENSE_NO_OLD = v.SUSPENSE_NO_OLD,
                         SUSPENSE_NO_PR = v.SUSPENSE_NO_NEW,
                         INIT_AMT = v.AMOUNT_IDR,
                         SAP_SPENT = trab.SPENT_AMT,
                         SAP_REMAINING = trab.INIT_AMT - trab.SPENT_AMT,
                         UPDATED_DT = tralh.CHANGED_DT,
                         DIVISION_NAME = vd.DIVISION_NAME,
                         PV_TYPE_CD = trab.PV_TYPE_CD,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME,
                         ACTIVITY_DES = "",
                         SAP_DOC_NO = "",
                         SAP_ITEM_NO = v.PV_TYPE_CD != 2 ? v.SAP_ITEM_NO : 1
                     }).Distinct();

            #region Criteria

            switch (crit.iBookingNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.BOOKING_NO.EndsWith(crit.sBookingNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.BOOKING_NO.StartsWith(crit.sBookingNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.BOOKING_NO.Contains(crit.sBookingNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.BOOKING_NO.StartsWith(crit.sBookingNoPre) && p.BOOKING_NO.EndsWith(crit.sBookingNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.BOOKING_NO.Equals(crit.BookingNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iActivity)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.ACTIVITY_DES.ToLower().EndsWith(crit.sActivityPost.ToLower()));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.ACTIVITY_DES.ToLower().StartsWith(crit.sActivityPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.ACTIVITY_DES.ToLower().Contains(crit.sActivityPre.ToLower()));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.ACTIVITY_DES.ToLower().StartsWith(crit.sActivityPre.ToLower()) && p.ACTIVITY_DES.ToLower().EndsWith(crit.sActivityPost.ToLower()));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.ACTIVITY_DES.Equals(crit.Activity));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iOldWbsNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_NO_OLD.EndsWith(crit.sOldWbsNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_NO_OLD.StartsWith(crit.sOldWbsNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_NO_OLD.Contains(crit.sOldWbsNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_NO_OLD.StartsWith(crit.sOldWbsNoPre) && p.WBS_NO_OLD.EndsWith(crit.sOldWbsNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_NO_OLD.Equals(crit.OldWbsNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iOldWbsDesc)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_DESC.EndsWith(crit.sOldWbsDescPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_DESC.StartsWith(crit.sOldWbsDescPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_DESC.Contains(crit.sOldWbsDescPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_DESC.StartsWith(crit.sOldWbsDescPre) && p.WBS_DESC.EndsWith(crit.sOldWbsDescPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_DESC.Equals(crit.OldWbsDesc));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iOldSuspenseNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_OLD).EndsWith(crit.sOldSuspenseNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_OLD).StartsWith(crit.sOldSuspenseNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_OLD).Contains(crit.sOldSuspenseNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_OLD).StartsWith(crit.sOldSuspenseNoPre)
                                    && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_OLD).EndsWith(crit.sOldSuspenseNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && SqlFunctions.StringConvert((decimal)p.SUSPENSE_NO_OLD).Trim().Equals(crit.OldSuspenseNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iNewWbsNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_NO.EndsWith(crit.sNewWbsNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_NO.StartsWith(crit.sNewWbsNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_NO.Contains(crit.sNewWbsNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_NO.StartsWith(crit.sNewWbsNoPre) && p.WBS_NO.EndsWith(crit.sNewWbsNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_NO.Equals(crit.NewWbsNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iNewWbsDesc)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_DESC_PR.EndsWith(crit.sNewWbsDescPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_DESC_PR.StartsWith(crit.sNewWbsDescPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_DESC_PR.Contains(crit.sNewWbsDescPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_DESC_PR.StartsWith(crit.sNewWbsDescPre) && p.WBS_NO.EndsWith(crit.sNewWbsDescPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_DESC_PR.Equals(crit.NewWbsDesc));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iNewSuspenseNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_PR).EndsWith(crit.sNewSuspenseNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_PR).StartsWith(crit.sNewSuspenseNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_PR).Contains(crit.sNewSuspenseNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_PR).StartsWith(crit.sNewSuspenseNoPre)
                        && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_PR).EndsWith(crit.sNewSuspenseNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && SqlFunctions.StringConvert((double)p.SUSPENSE_NO_PR).Trim().Equals(crit.NewSuspenseNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            if (crit.PVType > 0)
            {
                q = q.Where(p => p.PV_TYPE_CD == crit.PVType);
            }

            if (crit.AmountAccrFrom > 0)
            {
                q = q.Where(p => p.INIT_AMT >= crit.AmountAccrFrom);
            }

            if (crit.AmountAccrTo > 0)
            {
                q = q.Where(p => p.INIT_AMT <= crit.AmountAccrTo);
            }

            if (crit.AmountSpentFrom > 0)
            {
                q = q.Where(p => p.INIT_AMT >= crit.AmountRemainFrom);
            }

            if (crit.AmountSpentTo > 0)
            {
                q = q.Where(p => p.INIT_AMT <= crit.AmountRemainTo);
            }

            if (crit.AmountRemainFrom > 0)
            {
                q = q.Where(p => p.INIT_AMT >= crit.AmountRemainFrom);
            }

            if (crit.AmountRemainTo > 0)
            {
                q = q.Where(p => p.INIT_AMT <= crit.AmountRemainTo);
            }

            if (crit.caller != 2)
            {
                q = (from a in q
                     join tradn in db.TB_R_ACCR_DOC_NO on a.BOOKING_NO equals tradn.ACCR_NO
                     join trsdn in db.TB_R_SAP_DOC_NO 
                     on new { DOCNO = (int)(tradn.ACCR_DOC_NO ?? -1), ITEMNO = a.SAP_ITEM_NO ?? -1 }
                        equals new { DOCNO = trsdn.DOC_NO, ITEMNO = trsdn.ITEM_NO } into trsdn_
                     from trsdn in trsdn_.DefaultIfEmpty()
                     select new AccruedListDetail()
                      {
                          ACCRUED_NO = a.ACCRUED_NO,
                          BOOKING_NO = a.BOOKING_NO,
                          WBS_NO_OLD = a.WBS_NO_OLD,
                          WBS_NO = a.WBS_NO,
                          WBS_DESC = a.WBS_DESC,
                          WBS_DESC_PR = a.WBS_DESC_PR,
                          SUSPENSE_NO_OLD = a.SUSPENSE_NO_OLD,
                          SUSPENSE_NO_PR = a.SUSPENSE_NO_PR,
                          INIT_AMT = a.INIT_AMT,
                          SAP_SPENT = null,
                          SAP_REMAINING = null,
                          UPDATED_DT = null,
                          DIVISION_NAME = null,
                          PV_TYPE_CD = a.PV_TYPE_CD,
                          PV_TYPE_NAME = a.PV_TYPE_NAME,
                          ACTIVITY_DES = null,
                          SAP_DOC_NO = trsdn.SAP_DOC_NO,
                          SAP_ITEM_NO = a.SAP_ITEM_NO

                      }).Distinct();
            }

            #endregion

            return q;
        }

        public string GenerateListAccrResult(
            string _username,
            string templateFile,
            string newFile,
            AccruedResultSearch c,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "LA14";

            x.MarkPage(_sheetName);

            //Get data from search criteria
            c.caller = 1;
            var qq = DataListAccrResult(c);
            List<AccruedListDetail> _list =
                qq.ToList();
            _list.Sort(AccruedListDetail.RuleOrder);

            //Set value Header

            string printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            string printedDt = DateTime.Now.ToString("dd-MM-yyyy");
            int totalRow = _list.Count;

            x.MarkCells("[DOWNLOAD_BY],[DOWNLOAD_DT],[TOTAL_ROW]");
            x.PutCells(": " + printedBy, ": " + printedDt, ": " + totalRow.ToString());
            //End set value Header

            //Set Value Detail WBS_DESC
            x.MarkRow("[BOOKING_NO],[PV_TYPE],[ACTIVITY_DES],[WBS_NO_OLD],"
                + "[WBS_DESC],[SUSPENSE_NO_OLD],[WBS_NO],[WBS_DESC_PR],[SUSPENSE_NO_PR],[INIT_AMT],"
                + "[SAP_DOC_NO]");

            int j = 0;

            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedListDetail dx in _list)
            {
                //Get the Activities
                var param = new Dictionary<string, object>();
                param.Add("ACCRUED_NO", dx.ACCRUED_NO);
                param.Add("BOOKING_NO", dx.BOOKING_NO);

                List<AccruedListDetail> _Activities = logic.AccrList.GetActivityAccrued(param);

                var act = (from a in _Activities
                           select a.ACTIVITY_DES
                            ).ToList();
                x.PutRow(dx.BOOKING_NO, dx.PV_TYPE_NAME, String.Join(",", act),
                        dx.WBS_NO_OLD, dx.WBS_DESC, dx.SUSPENSE_NO_OLD, dx.WBS_NO,
                        dx.WBS_DESC_PR, dx.SUSPENSE_NO_PR, dx.INIT_AMT, dx.SAP_DOC_NO);
                j++;

            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "", "", "",
                         "");
            }
            //End set value Detail

            x.PutTail();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            if (String.IsNullOrEmpty(pdfExport))
            {
                return filePath;
            }
            else
            {
                string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                if (rslt)
                {
                    return pdfPath;
                }
                else
                {
                    return filePath;
                }

            }



        }

        public byte[] GetDataReport(
            string _username,
            string _imagePath,
            AccruedResultSearch c)
        {
            ExcelWriter x = new ExcelWriter();

            c.caller = 1;
            var qq = DataListAccrResult(c);

            int sizeForAll = qq.Count();

            string header1 = "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                            "Downloaded Date : " + DateTime.Now.date() + ExcelWriter.COL_SEP +
                            "Total Record Download : " + sizeForAll;
            string header2 = "Booking No|PV Type|Activity|Legacy Budget{WBS No.|WBS Description|Suspense No.}|" +
                            "Accrued Budget{WBS No. (PR)|WBS Description|Suspense No.}|Accrued Amount|SAP Doc. No.";
            x.PutPage("List Accrued Result");
            x.PutHeader("List Accrued Result", header1, header2, _imagePath);
            int pageSize = ExcelWriter.xlRowLimit;
            // the first eleven rows is used as header
            pageSize = pageSize - 11;
            int isNewPage = 1;
            int counter = 1;
            List<AccruedListDetail> _list =
                qq.ToList();
            Dictionary<int, string> doctype = logic.Look.GetDocType();

            List<string> lStyle = new List<string>() { 
                "left", //Booking No
                "left", //PV Type
                "left", //Activity
                "left", //Legacy Budget | WBS No.
                "left", //Legacy Budget | WBS Description
                "left", //Legacy Budget | Suspense No.
                "left", //WBS No. (PR)
                "left", //WBS Description
                "left", //Suspense No.
                "right", //Accrued Amount
                "left"  //SAP Doc. No.
            };

            //x.SetListAlignment(lStyle);

            foreach (AccruedListDetail dx in _list)
            {
                if (counter >= pageSize)
                {
                    counter = 1;
                    x.PutTail();
                    x.PutPage("List Accrued Result " + ++isNewPage);
                    x.PutHeader("List Accrued Result", header1, header2, _imagePath);
                }

                var param = new Dictionary<string, object>();
                param.Add("ACCRUED_NO", dx.ACCRUED_NO);
                param.Add("BOOKING_NO", dx.BOOKING_NO);

                List<AccruedListDetail> _Activities = logic.AccrList.GetActivityAccrued(param);

                var act = (from a in _Activities
                           select a.ACTIVITY_DES
                            ).ToList();

                x.Put(
                    dx.BOOKING_NO, dx.PV_TYPE_NAME, String.Join(",", act),
                        dx.WBS_NO_OLD, dx.WBS_DESC, dx.SUSPENSE_NO_OLD, dx.WBS_NO,
                        dx.WBS_DESC_PR, dx.SUSPENSE_NO_PR, dx.INIT_AMT, dx.SAP_DOC_NO);
                counter++;
            }
            //x.ChangeStyleGrid(_list.Count);
            x.PutTail();



            return x.GetBytes();
        }

        #endregion

        #region Upload Accrued Template


        #endregion

    }
}