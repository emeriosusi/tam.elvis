﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using BusinessLogic.AccruedForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data._80Accrued;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;
using Dapper;
using System.Data.Objects.SqlClient;

namespace BusinessLogic._80Accrued
{
    public class AccrShiftingListLogic : LogicBase
    {
        public const byte SAP_PAID_BY_CASHIER = 1;
        public const byte SAP_CHECK_BY_ACCOUNTING = 2;
        private LogicFactory logic = LogicFactory.Get();
        protected LoggingLogic log = new LoggingLogic();
        protected readonly string _INF = "MSTD00001INF";
        protected List<string> errs = new List<string>();

        public IQueryable<AccruedShiftingListData> Search(AccruedShiftingSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from trash in db.TB_R_ACCR_SHIFTING_H
                     join a in db.vw_Division on trash.DIVISION_ID equals a.DIVISION_ID
                     join b in db.TB_M_STATUS on trash.STATUS_CD equals b.STATUS_CD
                     join c in db.TB_R_ACCR_DOC_NO on trash.SHIFTING_NO equals c.ACCR_NO into cc
                     from c in cc.DefaultIfEmpty()
                     join d in db.vw_CurNextApprover on c.ACCR_DOC_NO equals d.REFF_NO into dd
                     from d in dd.DefaultIfEmpty()
                     select new AccruedShiftingListData()
                     {
                         WORKFLOW_STATUS = SqlFunctions.StringConvert((decimal)trash.WORKFLOW_STATUS).Trim(),
                         SHIFTING_NO = trash.SHIFTING_NO,
                         DIVISION_ID = trash.DIVISION_ID,
                         CREATED_BY = trash.CREATED_BY,
                         CREATED_DT = trash.CREATED_DT,
                         STATUS_CD = trash.STATUS_CD,
                         DIVISION_NAME = a.DIVISION_NAME,
                         STATUS_NAME = b.STATUS_NAME,
                         MODULE_CD = b.MODULE_CD,
                         BUDGET_YEAR = trash.BUDGET_YEAR,
                         PIC_CURRENT = d.CURRENT_APPROVER,
                         PIC_NEXT = d.NEXT_APPROVER,
                         TOT_AMOUNT = trash.TOTAL_AMT != null ? trash.TOTAL_AMT.Value : 0,
                     });

            if (!string.IsNullOrEmpty(crit.issuingDivision))
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (crit.workflowSts > -1)
            {
                q = q.Where(p => p.WORKFLOW_STATUS == SqlFunctions.StringConvert((double)crit.workflowSts));
            }

            if (!crit.ignoreAccruedNo)
            {
                switch (crit.iAno)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.SHIFTING_NO.EndsWith(crit.sAccrPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.SHIFTING_NO.StartsWith(crit.sAccrPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.SHIFTING_NO.Contains(crit.sAccrPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.SHIFTING_NO.StartsWith(crit.sAccrPre) && p.SHIFTING_NO.EndsWith(crit.sAccrPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.SHIFTING_NO.Contains(crit.accrNo));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!crit.ignoreDateFrom)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT >= crit.createdDtFrom);
            }
            if (!crit.ignoreDateTo)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT <= crit.createdDtTo);
            }

            if (crit.budgetYear > 0)
            {
                q = q.Where(p => p.BUDGET_YEAR == crit.budgetYear);
            }

            if (crit.submissionSts >= 0)
            {
                q = q.Where(p => p.STATUS_CD == crit.submissionSts);
            }

            if (!crit.ignoreAmt)
            {
                q = q.Where(p => p.TOT_AMOUNT >= crit.totAmtFrom && p.TOT_AMOUNT <= crit.totAmtTo);
            }

            return q;
        }

        public bool isDraft(string shiftingNo)
        {
            var q = (from h in db.TB_R_ACCR_SHIFTING_H
                    where h.SHIFTING_NO == shiftingNo
                    select h).FirstOrDefault();

            return q != null && q.STATUS_CD == 0;
        }

        public bool isSubmit(string shiftingNo)
        {
            var q = (from h in db.TB_R_ACCR_SHIFTING_H
                     where h.SHIFTING_NO == shiftingNo
                     select h).FirstOrDefault();

            return q != null && q.STATUS_CD == 401;
        }

        public bool isPosted(string shiftingNo)
        {
            var q = (from h in db.TB_R_ACCR_SHIFTING_H
                     where h.SHIFTING_NO == shiftingNo
                     select h).FirstOrDefault();

            return q != null && q.STATUS_CD == 403;
        }

        public bool isApproved(string shiftingNo)
        {
            var q = (from h in db.TB_R_ACCR_SHIFTING_H
                     where h.SHIFTING_NO == shiftingNo
                     select h).FirstOrDefault();

            return q != null && (q.STATUS_CD == 402);
        }

        public bool isRejected(string shiftingNo)
        {
            var q = (from h in db.TB_R_ACCR_SHIFTING_H
                     where h.SHIFTING_NO == shiftingNo
                     select h).FirstOrDefault();

            return q != null && (q.STATUS_CD == 411);
        }

        public bool Delete(string shiftingNo, string username)
        {
            if (String.IsNullOrEmpty(shiftingNo)) return false;

            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    logic.AccrForm.DeleteAccrReffNo(shiftingNo, DB);

                    var det = (from d in DB.TB_R_ACCR_SHIFTING_D
                               where d.SHIFTING_NO == shiftingNo
                              select d).ToList();
                    if (det.Any())
                    {
                        foreach (var d in det)
                        {
                            DB.DeleteObject(d);
                            DB.SaveChanges();
                        }
                    }

                    var head = (from h in DB.TB_R_ACCR_SHIFTING_H
                                where h.SHIFTING_NO == shiftingNo
                                select h).ToList();
                    if (head.Any())
                    {
                        foreach (var h in head)
                        {
                            DB.DeleteObject(h);
                            DB.SaveChanges();
                        }
                    }
                    _result = true;
                    tx.Commit();
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public bool Delete(List<AccrFormData> listOfPVNo, string username)
        {
            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    foreach (AccrFormData PVNo in listOfPVNo)
                    {
                        int pvNo = PVNo.PVNumber.Value;
                        int pvYear = PVNo.PVYear.Value;
                        // Get TB_R_PV_H
                        var q = (from i in DB.TB_R_PV_H
                                 where i.PV_NO == pvNo
                                 where i.PV_YEAR == pvYear
                                 select i).FirstOrDefault();
                        // decimal reffNo = Convert.ToDecimal((q.REFFERENCE_NO ?? 0));
                        string reff = pvNo.str() + pvYear.str();
                        decimal REFF_NO = reff.Dec();

                        /* deletion by set DELETED=1 */
                        /* 2012-11-24 dan -
                        if (q.REFFERENCE_NO != null)
                        {
                            string reff = Convert.ToString(q.REFFERENCE_NO);
                            // Delete TB_R_ATTACHMENT
                            var att = (from a in DB.TB_R_ATTACHMENT
                                       where a.REFERENCE_NO == reff
                                       select a).ToList();
                            if (att.Any())
                            {
                                foreach (var tmp in att)
                                {
                                    DB.DeleteObject(tmp);
                                    DB.SaveChanges();
                                }
                            }
                        }
                        
                        // Delete TB_R_NOTICE
                        var not = (from n in DB.TB_R_NOTICE
                                   where n.DOC_NO == pvNo
                                   where n.DOC_YEAR == pvYear
                                   select n).ToList();
                        if (not.Any())
                        {
                            foreach (var tmp in not)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        */
                        // Delete TB_R_DISTRIBUTION_STATUS
                        var dis = (from d in DB.TB_R_DISTRIBUTION_STATUS
                                   where d.REFF_NO == REFF_NO
                                   select d).ToList();
                        if (dis.Any())
                        {
                            foreach (var tmp in dis)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Delete TB_R_PVRV_AMOUNT
                        /* not necessary 2012-11-24 dan -
                        var pvrv = (from r in DB.TB_R_PVRV_AMOUNT
                                    where r.DOC_NO == pvNo
                                    where r.DOC_YEAR == pvYear
                                    select r).ToList();
                        if (pvrv.Any())
                        {
                            foreach (var tmp in pvrv)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        */
                        // Get TB_R_PV_DETAIL for Invoice checking and re-enable conversion
                        var pvdI = (from pi in DB.TB_R_PV_D
                                    where pi.PV_NO == pvNo
                                    where pi.PV_YEAR == pvYear
                                    select pi).ToList();
                        if (pvdI.Any())
                        {
                            foreach (var pInv in pvdI)
                            {
                                var v = (from inv in DB.TB_R_INVOICE_H
                                         where inv.PV_NO == pInv.PV_NO
                                         where inv.PV_YEAR == pInv.PV_YEAR
                                         select inv).FirstOrDefault();
                                if (v != null)
                                {
                                    v.PV_NO = null;
                                    v.PV_YEAR = null;
                                    v.STATUS_CD = -1;
                                    v.CONVERT_FLAG = 0;
                                    v.TRANSACTION_CD = null;
                                    v.CHANGED_BY = username;
                                    v.CHANGED_DT = DateTime.Now;
                                }
                            }
                        }

                        // Delete TB_R_PV_DETAIL
                        /* 2012-11-24 dan -
                        var pvd = (from p in DB.TB_R_PV_D
                                   where p.PV_NO == pvNo
                                   where p.PV_YEAR == pvYear
                                   select p).ToList();
                        if (pvd.Any())
                        {
                            foreach (var tmp in pvd)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        */

                        // Get the previous PV Form if any

                        var preP = (from v in DB.TB_R_PV_H
                                    where v.SET_NO_PV.HasValue && v.SET_NO_PV == pvNo
                                    // where v.SET_NO_PV.HasValue
                                    select v).FirstOrDefault();
                        if (preP != null)
                        {
                            int rvSettle = preP.SET_NO_RV ?? 0;
                            // Get the RV (if any)
                            var newR = (from r in DB.TB_R_RV_H
                                        where r.RV_NO == rvSettle
                                        where r.RV_YEAR == pvYear
                                        select r).FirstOrDefault();
                            if (newR != null)
                            {
                                // Delete the PVRV Amount of the particular RV
                                var newrv = (from n in DB.TB_R_PVRV_AMOUNT
                                             where n.DOC_NO == newR.RV_NO
                                             where n.DOC_YEAR == newR.RV_YEAR
                                             select n).ToList();
                                if (newrv.Any())
                                {
                                    foreach (var tmp in newrv)
                                    {
                                        DB.DeleteObject(tmp);
                                        DB.SaveChanges();
                                    }
                                }
                                // Delete RV Detail
                                var newRD = (from rd in DB.TB_R_RV_D
                                             where rd.RV_NO == newR.RV_NO
                                             where rd.RV_YEAR == newR.RV_YEAR
                                             select rd).ToList();
                                if (newRD.Any())
                                {
                                    foreach (var tmp in newRD)
                                    {
                                        DB.DeleteObject(tmp);
                                        DB.SaveChanges();
                                    }
                                }
                                newR.DELETED = 1;
                                // DB.DeleteObject(newR);
                                DB.SaveChanges();
                            }

                            // Get the settlement of the previous PV and re-enable conversion
                            var prevSetH = (from ps in DB.TB_R_SET_FORM_H
                                            where ps.PV_SUS_NO == preP.PV_NO
                                            where ps.PV_YEAR == preP.PV_YEAR
                                            select ps).FirstOrDefault();
                            if (prevSetH != null)
                            {
                                prevSetH.CONVERT_FLAG = 0;
                                prevSetH.CHANGED_BY = username;
                                prevSetH.CHANGED_DT = DateTime.Now;
                                DB.SaveChanges();
                            }

                            preP.SET_NO_PV = null;
                            preP.SET_NO_RV = null;
                            preP.CHANGED_BY = username;
                            preP.CHANGED_DATE = DateTime.Now;
                            DB.SaveChanges();
                        }

                        // Delete TB_R_SET_FORM_D
                        var setD = (from d in DB.TB_R_SET_FORM_D
                                    where d.PV_SUS_NO == pvNo
                                    where d.PV_YEAR == pvYear
                                    select d).ToList();
                        if (setD.Any())
                        {
                            foreach (var tmp in setD)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Delete TB_R_SET_FORM_H
                        var setH = (from h in DB.TB_R_SET_FORM_H
                                    where h.PV_SUS_NO == pvNo
                                    where h.PV_YEAR == pvYear
                                    select h).ToList();
                        if (setH.Any())
                        {
                            foreach (var tmp in setH)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        q.CANCEL_FLAG = 0;
                        q.DELETED = 1;
                        q.NEXT_APPROVER = "";
                        q.CHANGED_DATE = DateTime.Now;
                        q.CHANGED_BY = username;

                        BudgetCheckResult o;

                        // DB.DeleteObject(q); // 2012-11-24 dan - : delete logically not physically 
                        logic.k2.deleteprocess(q.PV_NO.ToString() + q.PV_YEAR.ToString());

                        BudgetCheckInput iBudget = new BudgetCheckInput()
                        {
                            DOC_STATUS = (int)BudgetCheckStatus.Delete,
                            DOC_NO = q.PV_NO,
                            DOC_YEAR = logic.Sys.FiscalYear(q.PV_DATE), //  q.PV_YEAR,
                            WBS_NO = q.BUDGET_NO,
                            AMOUNT = q.TOTAL_AMOUNT ?? 0
                        };

                        if (Common.AppSetting.SKIP_BUDGET_CHECK || !Common.AppSetting.BUDGET_CHECK_DELETE)
                        {
                            o = logic.SAP._BudgetCheck_(iBudget);
                        }
                        else
                        {
                            o = logic.SAP.BudgetCheck(iBudget);
                        }

                        DB.SaveChanges();
                    }
                    _result = true;
                    tx.Commit();
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public IQueryable<vw_PV_List> SearchListIqueryable(
            VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;
            var q = (from p in db.vw_PV_List
                     select p);
            if (!c.ignoreDate)
                q = q.Where(x => (x.PV_DATE >= c.docDateFrom && x.PV_DATE <= c.docDateTo));
            if (!c.ignoreNum)
                q = q.Where(p => (p.PV_NO >= c.numFrom && p.PV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.PV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));
            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignorePlanningPaymentDate)
                q = q.Where(p => ((p.PLANNING_PAYMENT_DATE != null && p.PLANNING_PAYMENT_DATE >= c.planPaymentDateFrom && p.PLANNING_PAYMENT_DATE <= c.planPaymentDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => (p.PAID_DATE != null && p.PAID_DATE >= c.transDateFrom && p.PAID_DATE <= c.transDateTo));
            if (!c.ignorePostingDate)
                q = q.Where(p => (p.POSTING_DATE != null && p.POSTING_DATE >= c.postingDateFrom && p.POSTING_DATE <= c.postingDateTo));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => (p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.POSTING_DATE <= c.verifiedDateTo));
            if (c.settlementStatus > 0)
                q = q.Where(p => p.SETTLEMENT_STATUS == c.settlementStatus);
            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.PV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.PV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.PV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);
            /* 
            if (!c.ignoreUnknownEdit)
                q = q.Where(p => ((
                        (p.HOLD_FLAG != 0) && (p.VERSION > 1) &&
                        db.TB_R_DISTRIBUTION_STATUS.Any(d =>
                                d.ACTUAL_DT.HasValue &&
                                (c.unknownEdit == d.PROCCED_BY) &&
                                (p.REFF.HasValue && d.REFF_NO == p.REFF)))));
             */
            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);

            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));

            return q;
        }

        public IQueryable<vw_PV_List_Status_User> SearchListStatusUserIqueryable(
                VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;
            var q = (from p in db.vw_PV_List_Status_User
                     where (c.ignoreDate || (p.PV_DATE >= c.docDateFrom && p.PV_DATE <= c.docDateTo))
                     select p);

            if (!c.ignoreNum)
                q = q.Where(p => (p.PV_NO >= c.numFrom && p.PV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.PV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));

            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignorePlanningPaymentDate)
                q = q.Where(p => ((p.PLANNING_PAYMENT_DATE != null && p.PLANNING_PAYMENT_DATE >= c.planPaymentDateFrom && p.PLANNING_PAYMENT_DATE <= c.planPaymentDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => ((p.PAID_DATE != null && p.PAID_DATE >= c.transDateFrom && p.PAID_DATE <= c.transDateTo)));
            if (!c.ignorePostingDate)
                q = q.Where(p => (p.POSTING_DATE != null && p.POSTING_DATE >= c.postingDateFrom && p.POSTING_DATE <= c.postingDateTo));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => (p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.POSTING_DATE <= c.verifiedDateTo));
            if (c.settlementStatus > 0)
                q = q.Where(p => p.SETTLEMENT_STATUS == c.settlementStatus);
            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.PV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.PV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.PV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);

            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);

            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));
            return q;
        }

        public IQueryable<vw_PV_List_Status_Finance> SearchListStatusFinanceIqueryable(
            VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;
            var q = (from p in db.vw_PV_List_Status_Finance
                     where (c.ignoreDate || (p.PV_DATE >= c.docDateFrom && p.PV_DATE <= c.docDateTo))
                     select p);

            if (!c.ignoreNum)
                q = q.Where(p => (p.PV_NO >= c.numFrom && p.PV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.PV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));
            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignorePlanningPaymentDate)
                q = q.Where(p => ((p.PLANNING_PAYMENT_DATE != null && p.PLANNING_PAYMENT_DATE >= c.planPaymentDateFrom && p.PLANNING_PAYMENT_DATE <= c.planPaymentDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => ((p.PAID_DATE != null && p.PAID_DATE >= c.transDateFrom && p.PAID_DATE <= c.transDateTo)));
            if (!c.ignorePostingDate)
                q = q.Where(p => (p.POSTING_DATE != null && p.POSTING_DATE >= c.postingDateFrom && p.POSTING_DATE <= c.postingDateTo));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => (p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.POSTING_DATE <= c.verifiedDateTo));
            if (c.settlementStatus > 0)
                q = q.Where(p => p.SETTLEMENT_STATUS == c.settlementStatus);
            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.PV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.PV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.PV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);
            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);

            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));
            return q;
        }

        public PVHeaderData GetPVH(int no, int year)
        {
            return (from p in db.TB_R_PV_H
                    where p.PV_NO == no && p.PV_YEAR == year
                    select new PVHeaderData()
                    {
                        PV_NO = p.PV_NO,
                        PV_YEAR = p.PV_YEAR,
                        STATUS_CD = p.STATUS_CD,
                        PV_TYPE_CD = p.PV_TYPE_CD ?? 0,
                        PAY_METHOD_CD = p.PAY_METHOD_CD,
                        ACTIVITY_DATE_FROM = p.ACTIVITY_DATE_FROM,
                        ACTIVITY_DATE_TO = p.ACTIVITY_DATE_TO,
                        PLANNING_PAYMENT_DATE = p.PLANNING_PAYMENT_DATE,
                        VENDOR_CD = p.VENDOR_CD,
                        VENDOR_GROUP_CD = p.VENDOR_GROUP_CD,
                        BANK_TYPE = p.BANK_TYPE
                    }).FirstOrDefault();
        }

        private bool ValidateActualOnTime(DateTime? planDate, DateTime? actualDate)
        {
            bool isValid = true;
            if (planDate != null)
            {
                if (actualDate == null)
                {
                    DateTime now = DateTime.Now;
                    if (now.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
                else
                {
                    if (actualDate.Value.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
            }
            return isValid;
        }

        private bool ValidateStatusCheckingOnTime(bool isUser, PVListData pv)
        {
            bool valid = true;

            if (isUser)
            {
                valid = ValidateActualOnTime(pv.PLAN_DT_SH, pv.ACTUAL_DT_SH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_DPH, pv.ACTUAL_DT_DPH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_DH, pv.ACTUAL_DT_DH);

                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_DIRECTOR, pv.ACTUAL_DT_DIRECTOR);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_VP, pv.ACTUAL_DT_VP);
            }
            else
            {
                valid = ValidateActualOnTime(pv.PLAN_DT_VERIFIED_COUNTER, pv.ACTUAL_DT_VERIFIED_COUNTER);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_VERIFIED_FINANCE, pv.ACTUAL_DT_VERIFIED_FINANCE);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_SH, pv.ACTUAL_DT_APPROVED_FINANCE_SH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_DPH, pv.ACTUAL_DT_APPROVED_FINANCE_DPH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_DH, pv.ACTUAL_DT_APPROVED_FINANCE_DH);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_APPROVED_FINANCE_DIRECTOR, pv.ACTUAL_DT_APPROVED_FINANCE_DIRECTOR);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_POSTED_TO_SAP, pv.ACTUAL_DT_POSTED_TO_SAP);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_PAID, pv.ACTUAL_DT_PAID);
                if (valid)
                    valid = ValidateActualOnTime(pv.PLAN_DT_CHECKED, pv.ACTUAL_DT_CHECKED);
            }

            return valid;
        }

        public bool Delete(List<PVFormData> listOfPVNo, string username)
        {
            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    foreach (PVFormData PVNo in listOfPVNo)
                    {
                        int pvNo = PVNo.PVNumber.Value;
                        int pvYear = PVNo.PVYear.Value;
                        // Get TB_R_PV_H
                        var q = (from i in DB.TB_R_PV_H
                                 where i.PV_NO == pvNo
                                 where i.PV_YEAR == pvYear
                                 select i).FirstOrDefault();
                        // decimal reffNo = Convert.ToDecimal((q.REFFERENCE_NO ?? 0));
                        string reff = pvNo.str() + pvYear.str();
                        decimal REFF_NO = reff.Dec();

                        /* deletion by set DELETED=1 */
                        /* 2012-11-24 dan -
                        if (q.REFFERENCE_NO != null)
                        {
                            string reff = Convert.ToString(q.REFFERENCE_NO);
                            // Delete TB_R_ATTACHMENT
                            var att = (from a in DB.TB_R_ATTACHMENT
                                       where a.REFERENCE_NO == reff
                                       select a).ToList();
                            if (att.Any())
                            {
                                foreach (var tmp in att)
                                {
                                    DB.DeleteObject(tmp);
                                    DB.SaveChanges();
                                }
                            }
                        }
                        
                        // Delete TB_R_NOTICE
                        var not = (from n in DB.TB_R_NOTICE
                                   where n.DOC_NO == pvNo
                                   where n.DOC_YEAR == pvYear
                                   select n).ToList();
                        if (not.Any())
                        {
                            foreach (var tmp in not)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        */
                        // Delete TB_R_DISTRIBUTION_STATUS
                        var dis = (from d in DB.TB_R_DISTRIBUTION_STATUS
                                   where d.REFF_NO == REFF_NO
                                   select d).ToList();
                        if (dis.Any())
                        {
                            foreach (var tmp in dis)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Delete TB_R_PVRV_AMOUNT
                        /* not necessary 2012-11-24 dan -
                        var pvrv = (from r in DB.TB_R_PVRV_AMOUNT
                                    where r.DOC_NO == pvNo
                                    where r.DOC_YEAR == pvYear
                                    select r).ToList();
                        if (pvrv.Any())
                        {
                            foreach (var tmp in pvrv)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        */
                        // Get TB_R_PV_DETAIL for Invoice checking and re-enable conversion
                        var pvdI = (from pi in DB.TB_R_PV_D
                                    where pi.PV_NO == pvNo
                                    where pi.PV_YEAR == pvYear
                                    select pi).ToList();
                        if (pvdI.Any())
                        {
                            foreach (var pInv in pvdI)
                            {
                                var v = (from inv in DB.TB_R_INVOICE_H
                                         where inv.PV_NO == pInv.PV_NO
                                         where inv.PV_YEAR == pInv.PV_YEAR
                                         select inv).FirstOrDefault();
                                if (v != null)
                                {
                                    v.PV_NO = null;
                                    v.PV_YEAR = null;
                                    v.STATUS_CD = -1;
                                    v.CONVERT_FLAG = 0;
                                    v.TRANSACTION_CD = null;
                                    v.CHANGED_BY = username;
                                    v.CHANGED_DT = DateTime.Now;
                                }
                            }
                        }

                        // Delete TB_R_PV_DETAIL
                        /* 2012-11-24 dan -
                        var pvd = (from p in DB.TB_R_PV_D
                                   where p.PV_NO == pvNo
                                   where p.PV_YEAR == pvYear
                                   select p).ToList();
                        if (pvd.Any())
                        {
                            foreach (var tmp in pvd)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        */

                        // Get the previous PV Form if any

                        var preP = (from v in DB.TB_R_PV_H
                                    where v.SET_NO_PV.HasValue && v.SET_NO_PV == pvNo
                                    // where v.SET_NO_PV.HasValue
                                    select v).FirstOrDefault();
                        if (preP != null)
                        {
                            int rvSettle = preP.SET_NO_RV ?? 0;
                            // Get the RV (if any)
                            var newR = (from r in DB.TB_R_RV_H
                                        where r.RV_NO == rvSettle
                                        where r.RV_YEAR == pvYear
                                        select r).FirstOrDefault();
                            if (newR != null)
                            {
                                // Delete the PVRV Amount of the particular RV
                                var newrv = (from n in DB.TB_R_PVRV_AMOUNT
                                             where n.DOC_NO == newR.RV_NO
                                             where n.DOC_YEAR == newR.RV_YEAR
                                             select n).ToList();
                                if (newrv.Any())
                                {
                                    foreach (var tmp in newrv)
                                    {
                                        DB.DeleteObject(tmp);
                                        DB.SaveChanges();
                                    }
                                }
                                // Delete RV Detail
                                var newRD = (from rd in DB.TB_R_RV_D
                                             where rd.RV_NO == newR.RV_NO
                                             where rd.RV_YEAR == newR.RV_YEAR
                                             select rd).ToList();
                                if (newRD.Any())
                                {
                                    foreach (var tmp in newRD)
                                    {
                                        DB.DeleteObject(tmp);
                                        DB.SaveChanges();
                                    }
                                }
                                newR.DELETED = 1;
                                // DB.DeleteObject(newR);
                                DB.SaveChanges();
                            }

                            // Get the settlement of the previous PV and re-enable conversion
                            var prevSetH = (from ps in DB.TB_R_SET_FORM_H
                                            where ps.PV_SUS_NO == preP.PV_NO
                                            where ps.PV_YEAR == preP.PV_YEAR
                                            select ps).FirstOrDefault();
                            if (prevSetH != null)
                            {
                                prevSetH.CONVERT_FLAG = 0;
                                prevSetH.CHANGED_BY = username;
                                prevSetH.CHANGED_DT = DateTime.Now;
                                DB.SaveChanges();
                            }

                            preP.SET_NO_PV = null;
                            preP.SET_NO_RV = null;
                            preP.CHANGED_BY = username;
                            preP.CHANGED_DATE = DateTime.Now;
                            DB.SaveChanges();
                        }

                        // Delete TB_R_SET_FORM_D
                        var setD = (from d in DB.TB_R_SET_FORM_D
                                    where d.PV_SUS_NO == pvNo
                                    where d.PV_YEAR == pvYear
                                    select d).ToList();
                        if (setD.Any())
                        {
                            foreach (var tmp in setD)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Delete TB_R_SET_FORM_H
                        var setH = (from h in DB.TB_R_SET_FORM_H
                                    where h.PV_SUS_NO == pvNo
                                    where h.PV_YEAR == pvYear
                                    select h).ToList();
                        if (setH.Any())
                        {
                            foreach (var tmp in setH)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }
                        q.CANCEL_FLAG = 0;
                        q.DELETED = 1;
                        q.NEXT_APPROVER = "";
                        q.CHANGED_DATE = DateTime.Now;
                        q.CHANGED_BY = username;

                        BudgetCheckResult o;

                        // DB.DeleteObject(q); // 2012-11-24 dan - : delete logically not physically 
                        logic.k2.deleteprocess(q.PV_NO.ToString() + q.PV_YEAR.ToString());

                        BudgetCheckInput iBudget = new BudgetCheckInput()
                        {
                            DOC_STATUS = (int)BudgetCheckStatus.Delete,
                            DOC_NO = q.PV_NO,
                            DOC_YEAR = logic.Sys.FiscalYear(q.PV_DATE), //  q.PV_YEAR,
                            WBS_NO = q.BUDGET_NO,
                            AMOUNT = q.TOTAL_AMOUNT ?? 0
                        };

                        if (Common.AppSetting.SKIP_BUDGET_CHECK || !Common.AppSetting.BUDGET_CHECK_DELETE)
                        {
                            o = logic.SAP._BudgetCheck_(iBudget);
                        }
                        else
                        {
                            o = logic.SAP.BudgetCheck(iBudget);
                        }

                        DB.SaveChanges();
                    }
                    _result = true;
                    tx.Commit();
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public bool HasSelectAll(string userName)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                int i = (from p in db.vw_User_Role_Header
                         where p.USERNAME == userName
                           && (p.ROLE_ID == "ELVIS_ACC" || p.ROLE_ID == "ELVIS_FD_STAFF")
                         select p.USERNAME).Count();
                return i > 0;
            }
        }

        public String getCashierName()
        {
            String cashierName = "";

            var q = (from p in db.vw_User_Role_Header
                     where p.ROLE_ID == "ELVIS_CASHIER"
                     //where p.ROLE_NAME == "ELVIS CASHIER"
                     select new
                     {
                         p.FIRST_NAME,
                         p.LAST_NAME
                     }).FirstOrDefault();

            if (q != null)
            {
                cashierName = String.Format("{0} {1}", q.FIRST_NAME, q.LAST_NAME);
            }
            return cashierName;
        }

        #region Get String Builder

        private String DateTimeToString(DateTime? dateTimeParam, String format)
        {
            if (dateTimeParam != null)
            {
                DateTime dateTimeParam1 = (DateTime)dateTimeParam;
                return dateTimeParam1.ToString(format);
            }
            else
            {
                return null;
            }
        }

        public byte[] WorklistReport(
            int ProcessID, UserData userData,
            string serverPath,
            int docType,
            DateTime dFrom, DateTime dTo, string issuingDivision, int DelayTolerance)
        {
            string addi = logic.Sys.GetText("EXCEL_TEMPLATE", "");
            addi = addi.Replace("~", "").Replace("/", "\\");
            string template = Path.Combine(serverPath , addi);
            string sqlPath = Path.Combine(serverPath, "ref");
            string uPath = Path.Combine(serverPath, AppSetting.TempFileUpload.Substring(2, AppSetting.TempFileUpload.Length - 3));
            string doctype = (docType == 1) ? "PV" : "RV";
            string fn = Path.Combine(template, "WorklistDelayReportTemplate.xls");
            log.Log(_INF,
                string.Format("{0} WorklistReport(\r\n serverPath={1}, \r\n From={2}, \r\n To={3}, \r\n issuingDivision={4}, \r\n DelayTolerance={5})"
                , doctype
                , serverPath
                , dFrom.fmt(StrTo.SQL_DATUM)
                , dTo.fmt(StrTo.SQL_DATUM)
                , issuingDivision
                , DelayTolerance),
                  "WorklistReport", userData.USERNAME, "", ProcessID);

            Ini ini = new Ini(Path.Combine(sqlPath, "WorklistDelay.ini"));

            string sql1 = doctype + "WorklistReport";
            string sql2 = "Summary" + sql1;

            string sql = logic.SQL[sql1];
            sql = sql.Replace("@from", dFrom.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@to", dTo.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@issuingDivision", issuingDivision);
            sql = sql.Replace("@delay", DelayTolerance.str());
            
            var q = Db.Query<WorklistReportData>(sql).ToList();
            int rows = 0;
            if ((q != null) && (q.Count > 0))
            {
                rows = q.Count;
            }

            if (!File.Exists(fn))
            {
                errs.Add("no template file");
                log.Log("MSTD00002ERR", "Template file : '" + fn + "' not found");
                return null;
            }
            string filename = "";

            // open template 
            ExcelWriter o = new ExcelWriter(fn);
            string s1 = ini.Sections[1];
            o.MarkPage(s1);
            int seq = 0;
            o.ClearCells();
            bool oMarked = o.MarkColumns(ini[s1 + ".Column"]);

            if (!oMarked)
            {
                log.Log("MSTD00036WRN", "Marking Worklist column failed");
            }

            if (oMarked)
                foreach (var d in q)
                {
                    o.PutRow(
                        ++seq, d.DivNamePic, d.PIC, d.POSITION_NAME,
                        d.DivNameDoc, d.DOC_AGE,
                        d.MODIFIED, d.MODIFIED_BY, d.DOC_NO, d.STATUS_NAME,
                        d.Description, d.TOTAL_AMOUNT, d.TRANSACTION_NAME,
                        d.VENDOR_NAME);
                }

            o.ClearCells();

            bool oMarkHead = o.MarkLabels(ini[s1 + ".Header"]); //  "DOWNLOADED_BY,DATUM,DOC_DATE,DELAY_TOLERANCE,ROWS"

            if (!oMarkHead)
            {
                log.Log("MSTD00036WRN", "Marking template header failed");
            }
            else
            {
                o.PutCells(
                    userData.FIRST_NAME + " " + userData.LAST_NAME
                    , DateTime.Now.fmt(StrTo.XLS_DATE)
                    , dFrom.fmt(StrTo.XLS_DATE) + " to " + dTo.fmt(StrTo.XLS_DATE)
                    , DelayTolerance
                    , rows);
            }
            o.SetCalc();


            string aPath = Path.Combine(uPath,
                DateTime.Now.ToString("yyyy"));

            if (!Directory.Exists(aPath)) Directory.CreateDirectory(aPath);
            filename = Util.GetTmp(aPath, "WORKLIST_" + DateTime.Now.ToString("yyyyMMdd_hhmm"), ".xls");

            sql = logic.SQL[sql2];
            sql = sql.Replace("@from", dFrom.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@to", dTo.fmt(StrTo.SQL_DATUM));
            sql = sql.Replace("@issuingDivision", StrTo.RemoveQuotes(issuingDivision));
            sql = sql.Replace("@delay", DelayTolerance.str());


            var qs = Db.Query<DocumentCountSummaryData>(sql).ToList();
            if (qs != null && qs.Count > 0)
            {
                string s0 = ini.Sections[0];
                o.MarkPage(s0);
                o.ClearCells();

                bool isMarked = o.MarkColumns(ini[s0 + ".Column"]);

                if (!isMarked)
                {
                    log.Log("MSTD00036WRN", "Marking template Summary column failed");
                }
                string lastDiv = "";
                string lastPic = "";
                int gtot = 0;

                List<DocumentCountSummaryData> l = new List<DocumentCountSummaryData>();
                int firstOne = -1;
                seq = 0;

                StringBuilder sb = new StringBuilder("");

                foreach (var s in qs)
                {
                    l.Add(s.Clone());
                    l[l.Count - 1].id = ++seq;

                    if (string.Compare(lastPic, s.PIC) == 0)
                    {
                        gtot += s.DOCUMENT_COUNT;

                    }
                    else
                    {
                        /// write gtot to first line per person
                        /// 
                        if (firstOne >= 0 && firstOne < l.Count)
                        {
                            l[firstOne].PERSON_TOTAL = gtot;
                        }

                        firstOne = seq - 1;
                        lastPic = s.PIC;

                        gtot = l[l.Count - 1].DOCUMENT_COUNT;

                    }

                }
                if (firstOne >= 0)
                {
                    l[firstOne].PERSON_TOTAL = gtot;

                }

                gtot = 0;
                int atot = 0;
                lastPic = "";
                foreach (var d in l)
                {
                    string sDiv = "";
                    string sPic = "";
                    string sPos = "";
                    if (string.Compare(lastDiv, d.DIV_PIC) != 0)
                    {
                        sDiv = d.DIV_PIC;
                        lastDiv = d.DIV_PIC;
                    }
                    if (string.Compare(lastPic, d.PIC) != 0)
                    {
                        sPos = d.POSITION_NAME;
                        sPic = d.PIC;
                        lastPic = d.PIC;
                    }
                    o.PutRow(
                        sDiv
                        , sPic
                        , sPos
                        , d.STATUS_NAME
                        , d.DOCUMENT_COUNT
                        , d.PERSON_TOTAL
                        , d.TOTAL_DOC_AGE
                        );

                    gtot += d.DOCUMENT_COUNT;
                    atot += (d.TOTAL_DOC_AGE ?? 0);

                }

                string[] ColNames = ini[s0 + ".Column"].Split(',');
                o.ColumnarGroup(ColNames[0]);
                o.ColumnarGroup(ColNames[1]);
                o.ColumnarGroup(ColNames[2]);
                o.ColumnarGroup(ColNames[ColNames.Length - 2]);

                o.PutRow("Grand Total", null, null, null, gtot, gtot, atot);

                o.RowGroup();

                o.ClearCells();
                bool isHeadMarked = o.MarkLabels(ini[s0 + ".Header"]); // "DOWNLOADED_BY,DATUM,DOC_DATE,DELAY_TOLERANCE"
                if (!isHeadMarked)
                {
                    log.Log("MSTD00036WRN", "Marking template header failed");
                }
                else
                {
                    o.PutCells(
                        userData.FIRST_NAME + " " + userData.LAST_NAME
                        , DateTime.Now.fmt(StrTo.XLS_DATE)
                        , dFrom.fmt(StrTo.XLS_DATE) + " to " + dTo.fmt(StrTo.XLS_DATE)
                        , DelayTolerance);
                }
                o.SetCalc();
            }

            return o.GetBytes();
        }

        public byte[] Get(
            string _username,
            string _imagePath,
            AccruedShiftingSearchCriteria c)
        {
            ExcelWriter x = new ExcelWriter();

            var qq = Search(c);

            int sizeForAll = qq.Count();

            string header1 = "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                            "Downloaded Date : " + DateTime.Now.date() + ExcelWriter.COL_SEP +
                            "Total Record Download : " + sizeForAll;
            string header2 = "No|Work Flow Status|Accrued Shifting No.|Issuing Division|" +
                            "Created Date|Created By|Updated Date|Updated By|" +
                            "Submission Status|PIC{Current|Next}|Total Amount";
            x.PutPage("General Info");
            x.PutHeader("Accrued Shifting General Info", header1, header2, _imagePath);
            int rownum = 0;
            int pageSize = ExcelWriter.xlRowLimit;
            // the first eleven rows is used as header
            pageSize = pageSize - 11;
            int isNewPage = 1;
            int counter = 1;
            List<AccruedShiftingListData> _list =
                qq.ToList();
            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedShiftingListData dx in _list)
            {
                if (counter >= pageSize)
                {
                    counter = 1;
                    x.PutTail();
                    x.PutPage("General Info " + ++isNewPage);
                    x.PutHeader("Accrued Shifting General Info", header1, header2, _imagePath);
                }

                string wsText = "";
                if (dx.WORKFLOW_STATUS != null)
                {
                    wsText = logic.Sys.GetText("WORKFLOW_STATUS", dx.WORKFLOW_STATUS);
                }

                x.Put(
                    ++rownum,
                    wsText, dx.SHIFTING_NO,
                    dx.DIVISION_NAME, dx.CREATED_DT.ToString("dd MMM yyyy"), dx.CREATED_BY,
                    dx.CHANGED_DT.HasValue?dx.CHANGED_DT.Value.ToString("dd MMM yyyy"):"", dx.CHANGED_BY, dx.STATUS_NAME,
                    dx.PIC_CURRENT, dx.PIC_NEXT, dx.TOT_AMOUNT.ToString("0,00", System.Globalization.CultureInfo.InvariantCulture));
                counter++;
            }

            x.PutTail();

            return x.GetBytes();
        }
        #endregion

        public PVFormHeaderData GetPVHeader(int _no, int _year)
        {
            using (ContextWrap co = new ContextWrap())
            {
                return (from pv in co.db.TB_R_PV_H
                        where pv.PV_NO == _no && pv.PV_YEAR == _year
                        select new PVFormHeaderData()
                        {
                            PV_NO = pv.PV_NO,
                            PV_YEAR = pv.PV_YEAR,
                            PV_TYPE_CD = pv.PV_TYPE_CD ?? -1,
                            VENDOR_GROUP_CD = pv.VENDOR_GROUP_CD ?? 0,
                            STATUS_CD = pv.STATUS_CD,

                        }).FirstOrDefault();
            }
        }

        public List<PVFormHeaderData> GetEntertainmentList(DateTime postingFrom, DateTime postingTo)
        {
            List<PVFormHeaderData> l = null;
            using (ContextWrap CO = new ContextWrap())
            {
                var db = CO.db;

                string spList = logic.Sys.GetText("LIST_STATUS", "PV_ENTERTAINMENT_STATUS");

                var st = from s in db.TB_M_SYSTEM
                         where s.SYSTEM_TYPE.Trim().Equals("K2_POSTING")
                         && s.SYSTEM_CD.Trim().Equals("ENTERTAINMENT_TRANSACTION")
                         select s.SYSTEM_VALUE_NUM ?? -1;

                int[] transactionTypes = st.ToArray();

                string[] aList = spList.Split(',');
                int[] EntertainStatus = new int[aList.Length];
                for (int i = 0; i < aList.Length; i++)
                {
                    EntertainStatus[i] = Convert.ToInt32(aList[i]);
                }

                DateTime nullDatum = new DateTime(StrTo.NULL_YEAR, 01, 01);

                var q = from p in db.TB_R_PV_H
                        where EntertainStatus.Contains(p.STATUS_CD)
                        && transactionTypes.Contains(p.TRANSACTION_CD ?? 0)
                        && (p.POSTING_DATE.HasValue
                            && p.POSTING_DATE >= postingFrom
                            && p.POSTING_DATE <= postingTo)
                        select new PVFormHeaderData
                        {
                            PV_NO = p.PV_NO,
                            PV_YEAR = p.PV_YEAR,
                            STATUS_CD = p.STATUS_CD,
                            POSTING_DATE = p.POSTING_DATE ?? nullDatum,
                            TRANSACTION_CD = p.TRANSACTION_CD ?? -1,
                        };

                if (q.Any())
                    l = q.ToList();
            }
            return l;
        }


        public byte[] GetCashierReport(
            IQueryable<vw_PV_List> q,
            string templateFile,
            string attachPath,
            int ProcessID,
            UserData by)
        {
            int rows = q.Count();

            log.Log(_INF,
              String.Format("GetCashierReport(\r\n pv count={0},\r\n template={1},\r\n attachPath={2})",
                  (q != null) ? rows.str() : "?", templateFile, attachPath),
                  "GetEntertain", (by != null) ? by.USERNAME : "", "", ProcessID);
            if (rows > 65500) // max rows by excel 2003 is 2 ^16
            {
                string err = "Too many rows, add more filter";
                errs.Add(err);
                log.Log("MSTD00002ERR", err);
                return null;
            }
            else if (rows < 1)
            {
                errs.Add("no Data");
                return null;
            }

            List<vw_PV_List> docs = q.ToList();
            if (!File.Exists(templateFile))
            {
                errs.Add("no template file");
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }
            string filename = "";

            // open template 
            ExcelWriter o = new ExcelWriter(templateFile);

            bool oMarked = o.MarkColumns(
                "No,PV No,Issuing Division,Vendor Name,Transaction Type,Planning Payment Date,"
                + "IDR,USD,JPY,PV Type,From,To,PV Date,Submit Date,Posting Date");
            if (!oMarked)
            {
                log.Log("MSTD00036WRN", "Marking template column failed");
            }

            int seq = 0;
            decimal total = 0;
            decimal totalIDR = 0;
            decimal totalUSD = 0;
            decimal totalJPY = 0;
            foreach (var pv in docs)
            {
                string reffNo = Convert.ToString(pv.PV_NO) + Convert.ToString(pv.PV_YEAR);
                string IDR = "";
                string JPY = "";
                string USD = "";
                List<OtherAmountData> oad = logic.Look.GetAmounts(pv.PV_NO, pv.PV_YEAR);
                OtherAmountData oa = null;
                oa = oad.Where(c => c.CURRENCY_CD == "IDR").FirstOrDefault();
                if (oa != null)
                {
                    IDR = CommonFunction.Eval_Curr("IDR", oa.TOTAL_AMOUNT);
                    totalIDR += (oa.TOTAL_AMOUNT ?? 0);
                }
                oa = oad.Where(c => c.CURRENCY_CD == "JPY").FirstOrDefault();
                if (oa != null)
                {
                    JPY = CommonFunction.Eval_Curr("JPY", oa.TOTAL_AMOUNT);
                    totalJPY += (oa.TOTAL_AMOUNT ?? 0);
                }
                oa = oad.Where(c => c.CURRENCY_CD == "USD").FirstOrDefault();
                if (oa != null)
                {
                    USD = CommonFunction.Eval_Curr("USD", oa.TOTAL_AMOUNT);
                    totalUSD += (oa.TOTAL_AMOUNT ?? 0);
                }

                o.PutRow(
                    ++seq, pv.PV_NO, pv.DIVISION_NAME,
                    pv.VENDOR_NAME,
                    pv.TRANSACTION_NAME,
                    pv.PLANNING_PAYMENT_DATE,
                    IDR,
                    USD,
                    JPY,
                    pv.PV_TYPE_NAME,
                    pv.ACTIVITY_DATE_FROM,
                    pv.ACTIVITY_DATE_TO,
                    pv.PV_DATE,
                    pv.SUBMIT_DATE,
                    pv.POSTING_DATE);
                total += (pv.TOTAL_AMOUNT_IDR ?? 0);
            }
            o.PutRow(
                    null, null, null,
                    null,
                    null,
                    null,
                    totalIDR,
                    totalUSD,
                    totalJPY,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "");
            o.ClearCells();
            bool oMarkHead = o.MarkLabels("BY,DATUM,ROWS");

            if (!oMarkHead)
            {
                log.Log("MSTD00036WRN", "Marking template header failed");
            }

            o.PutCells(
                by.FIRST_NAME + " " + by.LAST_NAME
                , DateTime.Now
                , docs.Count);
            o.SetCalc();

            // write template to temp download 
            string aPath = Path.Combine(attachPath,
                DateTime.Now.ToString("yyyy"));

            if (!Directory.Exists(aPath)) Directory.CreateDirectory(aPath);
            filename = Path.Combine(aPath, "PV_LIST_" + DateTime.Now.ToString("MMdd_hhmm") + ".xls");

            // o.Write(filename);

            // return temp download filename
            return o.GetBytes();
        }


        public byte[] GetEntertain(
            List<PVFormHeaderData> entertainmentPV,
            string templateFile,
            string attachPath,
            int ProcessID,
            UserData by,
            List<PVFormHeaderData> noAttachment)
        {
            using (ContextWrap CO = new ContextWrap())
            {
                var db = CO.db;
                log.Log(_INF,
                    String.Format("GetEntertain(\r\n pv count={0},\r\n template={1},\r\n attachPath={2})",
                        (entertainmentPV != null) ? Convert.ToString(entertainmentPV.Count) : "?", templateFile, attachPath),
                        "GetEntertain", (by != null) ? by.USERNAME : "", "", ProcessID);

                if (!File.Exists(templateFile))
                {
                    log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                    return null;
                }
                string filename = "";

                string attachmentType = logic.Sys.GetText("ATTACH_CATEGORY", "ENTERTAINMENT");
                if (attachmentType.isEmpty()) attachmentType = "5";


                List<TB_R_ATTACHMENT> allAttached = new List<TB_R_ATTACHMENT>();

                // open template 
                ExcelWriter o = new ExcelWriter(templateFile);
                bool oMarked = o.MarkColumns(
                    "NO,TANGGAL,TEMPAT DAN ALAMAT,JENIS, JUMLAH ,NAMA,POSISI,"
                    + "NAMA PERUSAHAAN,JENIS USAHA,KETERANGAN,PV,PIC,Referensi");
                if (!oMarked)
                {
                    log.Log("MSTD00036WRN", "Marking template column failed");
                }

                int seq = 0;
                foreach (PVFormHeaderData pv in entertainmentPV)
                {
                    string reffNo = Convert.ToString(pv.PV_NO) + Convert.ToString(pv.PV_YEAR);
                    var qa = from q in db.TB_R_ATTACHMENT
                             where reffNo.Equals(q.REFERENCE_NO)
                                && q.ATTACH_CD.Equals(attachmentType)
                                && q.FILE_NAME.Contains(".xls")
                             select q;
                    if (!qa.Any())
                    {
                        ++seq;
                        noAttachment.Add(pv);
                        object[] xx = new object[13];
                        xx[0] = seq;
                        xx[9] = "No file";
                        xx[10] = pv.PV_NO;
                        o.SetRow(xx);
                        // make destination row red 
                        o.SetStyleRed();
                        log.Log("MSTD00002ERR", "No file for PV: [" + pv.PV_NO + "]");
                    }
                    else
                    {
                        foreach (TB_R_ATTACHMENT x in qa)
                            allAttached.Add(x);
                    }
                }

                string lastReffNo = "";

                foreach (TB_R_ATTACHMENT a in allAttached)
                {
                    string attFile = attachPath
                            + Path.DirectorySeparatorChar
                            + a.DIRECTORY
                            + Path.DirectorySeparatorChar
                            + a.FILE_NAME;

                    if (File.Exists(attFile))
                        log.Log(_INF,
                                "READ Reff No:" + a.REFERENCE_NO +
                                " Seq:" + a.REF_SEQ_NO +
                                " File: " + a.FILE_NAME);
                    else
                    {
                        log.Log("MSTD00002ERR",
                            "No file Reff No :" + a.REFERENCE_NO +
                            " Seq:" + a.REF_SEQ_NO +
                            " File: " + a.FILE_NAME);

                        ++seq;

                        object[] z = new object[13];
                        z[0] = seq;
                        z[9] = "No File: '" + (a.FILE_NAME ?? "?")
                                + "' in \"" + (a.DIRECTORY ?? "?") + "\"";
                        if (a.REFERENCE_NO != null && a.REFERENCE_NO.Length > 4)
                            z[10] = a.REFERENCE_NO.Substring(0, a.REFERENCE_NO.Length - 4);
                        else
                            z[10] = "?";
                        o.SetRow(z);
                        // make destination row red 
                        o.SetStyleRed();

                        continue;
                    }
                    ExcelWriter ax = new ExcelWriter(attachPath
                            + Path.DirectorySeparatorChar
                            + a.DIRECTORY
                            + Path.DirectorySeparatorChar
                            + a.FILE_NAME);

                    oMarked = ax.MarkLabels("Nama:");
                    if (!oMarked)
                    {
                        log.Log(
                            "MSTD000036WRN",
                            "Marking input Template " + a.FILE_NAME
                            + " failed for Ref No :" + a.REFERENCE_NO);
                        break;
                    }
                    object[] nm = ax.ReadRow();
                    ax.ClearCells();
                    oMarked = ax.MarkColumns("No.,Tanggal,Tempat dan Alamat,Jenis,"
                        + "Jumlah (Rp),Nama,Posisi,Nama Perusahaan,Jenis Usaha,Keterangan");
                    if (!oMarked)
                    {
                        log.Log(
                            "MSTD000036WRN",
                            "Marking input Template column " + a.FILE_NAME
                            + " failed for Ref No: " + a.REFERENCE_NO);
                        continue;
                    }
                    // insert "a" contents into template 
                    object[] xx;
                    do
                    {
                        xx = ax.ReadRow();
                        if (xx == null) break;

                        int j = xx.Length;
                        if (!lastReffNo.Equals(a.REFERENCE_NO))
                        {
                            seq++;
                            xx[0] = seq;
                            lastReffNo = a.REFERENCE_NO;
                        }
                        else
                        {
                            xx[0] = null;
                        }

                        Array.Resize(ref xx, j + 3);
                        if (a.REFERENCE_NO != null && a.REFERENCE_NO.Length > 4)
                            xx[j] = a.REFERENCE_NO.Substring(0, a.REFERENCE_NO.Length - 4);
                        else
                            xx[j] = "?";
                        if (nm != null && nm[0] != null)
                            xx[j + 1] = nm[0];
                        else
                            log.Log("MSTD00002ERR", "Cannot read name cell");

                        o.SetRow(xx);

                    } while (xx.Length > 0 && xx[0] != null);
                }
                // write template to temp download 
                string aPath = attachPath
                    + Path.DirectorySeparatorChar
                    + DateTime.Now.ToString("yyyy")
                    + Path.DirectorySeparatorChar
                    + "EntertainmentList";

                return o.GetBytes();
            }
        }

        public void UpdateDetailItemNo(ELVIS_DBEntities db, List<PostItemData> l)
        {
            if (l == null || l.Count < 1) return;
            string lastreff = "";
            foreach (PostItemData d in l)
            {
                string reff = string.Format("{0}{1}", d.DOC_NO, d.DOC_YEAR);
                if (!lastreff.Equals(reff))
                {
                    //LoggingLogic.say(reff, "{0,10} {1,4} {2,3}{3,4}", "#", "", "seq", "itm");
                    lastreff = reff;
                }
                //LoggingLogic.say(reff, "{0,10} {1,4} {2,3}{3,4}", d.DOC_NO, d.DOC_YEAR, d.SEQ_NO, d.ITEM_NO);

                var q = db.TB_R_PV_D
                        .Where(p => p.PV_NO == d.DOC_NO && p.PV_YEAR == d.DOC_YEAR && p.SEQ_NO == d.SEQ_NO);

                if (q.Any())
                {
                    foreach (var x in q)
                    {
                        x.ITEM_NO = d.ITEM_NO;
                    }
                    db.SaveChanges();
                }
            }

        }

        public string getInvalidPrintStatusCover(string pv_no, string pv_year)
        {
            string status = null;
            decimal reffNo = Convert.ToDecimal(pv_no + pv_year);
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from p in db.TB_R_DISTRIBUTION_STATUS
                         where p.REFF_NO == reffNo
                         select p).FirstOrDefault();
                int statusCd = 0;
                if (q != null)
                {
                    var r = (from p in db.TB_R_DISTRIBUTION_STATUS.AsEnumerable()
                             where p.REFF_NO == reffNo
                             where Convert.ToString(p.STATUS_CD).StartsWith("1")
                             where !p.ACTUAL_DT.HasValue
                             select p.STATUS_CD);

                    if (r.Any())
                    {
                        statusCd = r.Max();
                    }
                }
                else
                {
                    statusCd = 10;
                }
                if (statusCd > 0)
                {
                    var s = (from p in db.TB_M_STATUS
                             where p.STATUS_CD == statusCd
                             select p).FirstOrDefault();
                    if (s != null)
                    {
                        status = s.STATUS_NAME;
                    }
                }
            }
            return status;
        }


        #region Added by Akhmad Nuryanto, 6/6/2012
        ///Post to SAP

        private bool PostAndSaveDocNo(
            int ProcessId,
            string uid,
            List<PVPostInputHeader> Headers,
            List<PVPostInputDetail> Details,
            List<OneTimeVendorData> Vendors,
            List<string> fails,
            List<string> succeeds,
            List<string> errs,
            Dictionary<string, bool> results)
        {
            string _loc = "PostAndSaveDocNo()";
            int postToSAPStatus = 27;
            List<PVPostSAPResult> _PostResults = new List<PVPostSAPResult>();
            if (Headers.Count < 1 && Details.Count < 1)
            {
                errs.Add("empty Input:");
                return false;
            }

            string errorMessage = "";
            _PostResults = logic.SAP.GetResultPostPVToSAP(
                    Headers,
                    Details,
                    Vendors,
                    ProcessId,
                    uid,
                    ref errorMessage);
            errs.Add(errorMessage);
            if (_PostResults.Count < 1)
            {
                log.Log(_INF, "PostToSAP Result Empty: ", _loc, uid, "", ProcessId);
                return false;
            }

            PVPostSAPResult[] ret = _PostResults
                .OrderBy(a => a.PV_YEAR)
                .ThenBy(a => a.PV_NO)
                .ThenBy(a => a.ITEM_NO)
                .ToArray();

            Ticker.In("PostToSapResult");
            Ticker.Spin("PV_NO	PV_YEAR	ITEM_NO	CURRENCY_CD	INVOICE_NO	SAP_DOC_NO	SAP_DOC_YEAR	SAP_CLEARING_DOC_NO	SAP_CLEARING_DOC_YEAR	MESSAGE	STATUS_MESSAGE	MESSAGE_TEXT");
            for (int pi = 0; pi < ret.Length; pi++)
            {
                PVPostSAPResult x = ret[pi];
                Ticker.Spin("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}"
                    ,
                    x.PV_NO, x.PV_YEAR, x.ITEM_NO, x.CURRENCY_CD, x.INVOICE_NO, x.SAP_DOC_NO, x.SAP_DOC_YEAR, x.SAP_CLEARING_DOC_NO, x.SAP_CLEARING_DOC_YEAR, x.MESSAGE, x.STATUS_MESSAGE, x.MESSAGE_TEXT
                    );
            }
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                int j = ret.Length;
                int i = 0;
                while (i < ret.Length)
                {
                    string _no = ret[i].PV_NO;
                    string _yy = ret[i].PV_YEAR;
                    string k = ret[i].PV_NO + ":" + ret[i].PV_YEAR;
                    bool posted = true;
                    while (i < ret.Length && k == ret[i].PV_NO + ":" + ret[i].PV_YEAR)
                    {
                        if (ret[i].SAP_DOC_NO.isEmpty() || (string.Compare(ret[i].MESSAGE, "E") == 0))
                            posted = false;
                        i++;
                    }

                    if (posted)
                    {
                        if (!succeeds.Contains(k))
                        {
                            succeeds.Add(k);
                            results[k] = true;
                        }
                        PVPostSAPResult[] sdoc = _PostResults.Where(x => x.PV_NO == _no && x.PV_YEAR == _yy).ToArray();
                        if (sdoc != null && sdoc.Length > 0)
                        {
                            List<PostItemData> postItems = new List<PostItemData>();

                            foreach (PVPostSAPResult item in sdoc)
                            {
                                postItems.Add(new PostItemData()
                                {
                                    DOC_NO = item.PV_NO.Int(),
                                    DOC_YEAR = item.PV_YEAR.Int(),
                                    ITEM_NO = item.ITEM_NO.Int(),
                                    INVOICE_NO = item.INVOICE_NO,
                                    CURRENCY_CD = item.CURRENCY_CD,
                                    SAP_DOC_NO = item.SAP_DOC_NO.isEmpty() ? (int?)null : (int?)item.SAP_DOC_NO.Int(),
                                    SAP_DOC_YEAR = item.SAP_DOC_YEAR.isEmpty() ? (int?)null : (int?)item.SAP_DOC_YEAR.Int()
                                });
                            }
                            //UpdateSapDocNo(db, postItems);
                        }
                        int iNo = _no.Int();
                        int iYY = _yy.Int();

                        TB_R_PV_H _TB_R_PV_H = (from p in db.TB_R_PV_H
                                                where p.PV_NO == iNo && p.PV_YEAR == iYY
                                                select p).FirstOrDefault();
                        if (_TB_R_PV_H != null)
                        {
                            _TB_R_PV_H.STATUS_CD = postToSAPStatus;
                            if (_TB_R_PV_H.POSTING_DATE == null)
                                _TB_R_PV_H.POSTING_DATE = DateTime.Now;
                            db.SaveChanges();
                        }

                    }
                    else
                    {
                        if (!fails.Contains(k))
                        {
                            fails.Add(k);
                        }
                        results[k] = false;
                    }
                }

            }


            Ticker.Out();
            bool r = true;
            foreach (string k in results.Keys)
            {
                r = r && results[k];
            }
            return r;
        }

        private bool UpdateSuccessDoc(PVPostSAPResult p)
        {
            int _no = p.PV_NO.Int();
            int _yy = p.PV_YEAR.Int();
            int _it = p.ITEM_NO.Int();

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var r = (from s in db.TB_R_SAP_DOC_NO
                         where s.DOC_NO == _no && s.DOC_YEAR == _yy && s.ITEM_NO == _it
                         select s).FirstOrDefault();
                int u = 0;
                if (r != null)
                {
                    if (!p.SAP_CLEARING_DOC_NO.isEmpty())
                    {
                        r.SAP_CLEARING_DOC_NO = p.SAP_CLEARING_DOC_NO;
                        r.SAP_CLEARING_DOC_YEAR = p.SAP_CLEARING_DOC_YEAR.isEmpty() ? null : (int?)p.SAP_CLEARING_DOC_YEAR.Int();
                        u++;
                    }
                    if (p.SAP_DOC_NO != null)
                    {
                        r.SAP_DOC_NO = p.SAP_DOC_NO;
                        r.SAP_DOC_YEAR = p.SAP_DOC_YEAR.Int();
                        u++;
                    }
                }
                else
                {
                    db.AddToTB_R_SAP_DOC_NO(new TB_R_SAP_DOC_NO()
                    {
                        DOC_NO = p.PV_NO.Int(),
                        DOC_YEAR = p.PV_YEAR.Int(),
                        ITEM_NO = p.ITEM_NO.Int(),
                        SAP_DOC_NO = p.SAP_DOC_NO,
                        SAP_DOC_YEAR = p.SAP_DOC_YEAR.Int(),
                        SAP_CLEARING_DOC_NO = p.SAP_CLEARING_DOC_NO,
                        SAP_CLEARING_DOC_YEAR = p.SAP_CLEARING_DOC_YEAR.Int()
                    });
                    u++;
                }
                if (u > 0)
                {
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region 2013-04-17 dan -
        //private bool UpdateSuccessDoc(string no, string year, string curr, string clearingNo, string clearingYear, int status = 29)
        //{
        //    int DocNo = Convert.ToInt32(no);
        //    int DocYear = Convert.ToInt32(year);
        //    bool updated = false;
        //    var r = (from p in db.TB_R_SAP_DOC_NO
        //             where p.DOC_NO == DocNo
        //             && p.DOC_YEAR == DocYear
        //                 //&& p.INVOICE_NO == item.INVOICE_NO
        //             && p.CURRENCY_CD == curr
        //             select p).ToList();

        //    if (r.Any())
        //    {
        //        foreach (TB_R_SAP_DOC_NO _TB_R_SAP_DOC_NO in r)
        //        {
        //            _TB_R_SAP_DOC_NO.SAP_CLEARING_DOC_NO = clearingNo;
        //            _TB_R_SAP_DOC_NO.SAP_CLEARING_DOC_YEAR = clearingYear == "" ? 0 : Convert.ToInt32(clearingYear);

        //            db.SaveChanges();


        //            updated = true;
        //        }
        //    }
        //    LoggingLogic.say("UpdateSuccessDoc", "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}", no, year, curr, clearingNo, clearingYear, status, updated ? "Ok" : "NG");
        //    return updated;
        //}
        #endregion 2013-04-17 dan -
        #endregion of addition by Akhmad Nuryanto

        public bool GetPVListData(PVListData z)
        {
            int _no = z.PV_NO.Int();
            int _yy = z.PV_YEAR.Int();

            var q = db.TB_R_PV_H
                .Where(p => p.PV_NO == _no && p.PV_YEAR == _yy)
                .FirstOrDefault();


            if (q != null)
            {
                z.PV_NO = q.PV_NO.str();
                z.PV_YEAR = q.PV_YEAR.str();
                z.PV_TYPE_CD = q.PV_TYPE_CD.str();
                z.PAY_METHOD_CD = q.PAY_METHOD_CD;
                z.STATUS_CD = q.STATUS_CD.str();
                z.ACTIVITY_DATE_FROM = q.ACTIVITY_DATE_FROM;
                z.ACTIVITY_DATE_TO = q.ACTIVITY_DATE_TO;
                z.PLANNING_PAYMENT_DATE = q.PLANNING_PAYMENT_DATE;
                z.VENDOR_CD = q.VENDOR_CD;
                z.BANK_TYPE = q.BANK_TYPE;
                z.HOLD_FLAG = q.HOLD_FLAG;
                z.HOLD_BY_NAME = q.HOLD_BY;
                z.PRINT_TICKET_FLAG = q.PRINT_TICKET_FLAG;
                z.PRINT_TICKET_BY = q.PRINT_TICKET_BY;
                z.PAID_DATE = q.PAID_DATE;
                z.SUBMIT_DATE = q.SUBMIT_DATE;
                z.DIVISION_ID = q.DIVISION_ID;
                z.SET_NO_PV = q.SET_NO_PV.str();
                z.SET_NO_RV = q.SET_NO_RV.str();
                z.TRANSACTION_CD = q.TRANSACTION_CD.str();
                z.SUSPENSE_NO = q.SUSPENSE_NO.str();
                z.TOTAL_AMOUNT = q.TOTAL_AMOUNT.str();
                z.BUDGET_NO = q.BUDGET_NO;
                z.NEXT_APPROVER = q.NEXT_APPROVER;
                return true;
            }
            return false;
        }


        public bool Fill(PVListData z)
        {
            int _no = z.PV_NO.Int();
            int _yy = z.PV_YEAR.Int();

            bool filled = false;

            using (ContextWrap CO = new ContextWrap())
            {
                var db = CO.db;
                var q = (from v in db.vw_PV_List select v).
                    Where(l => l.PV_NO == _no && l.PV_YEAR == _yy)
                    .FirstOrDefault();

                if (q != null)
                {
                    z.PV_DATE = (DateTime)q.PV_DATE;

                    z.PV_TYPE_CD = q.PV_TYPE_CD.str();

                    z.DIVISION_NAME = q.DIVISION_NAME;

                    z.DIVISION_ID = q.DIVISION_ID;

                    z.VENDOR_CD = q.VENDOR_CD;

                    z.VENDOR_NAME = q.VENDOR_NAME;

                    z.STATUS_CD = q.STATUS_CD.str();
                    z.TRANSACTION_CD = q.TRANSACTION_CD.str();

                    z.TRANSACTION_NAME = q.TRANSACTION_NAME;

                    z.TOTAL_AMOUNT_IDR = q.TOTAL_AMOUNT_IDR;

                    z.PAY_METHOD_CD = q.PAY_METHOD_CD;

                    z.PAY_METHOD_NAME = q.PAY_METHOD_NAME;

                    z.PLANNING_PAYMENT_DATE = q.PLANNING_PAYMENT_DATE;
                    z.PRINT_TICKET_BY = q.PRINT_TICKET_BY;
                    z.PRINT_TICKET_FLAG = q.PRINT_TICKET_FLAG;
                    z.BUDGET_NO = q.BUDGET_NO;
                    z.NEXT_APPROVER = q.NEXT_APPROVER;

                    z.BUDGET_DESCRIPTION = (from b in db.vw_WBS
                                            where b.WbsNumber == q.BUDGET_NO
                                            select b.Description).FirstOrDefault();

                    List<OtherAmountData> oad = logic.Look.GetAmounts(_no, _yy);
                    OtherAmountData oa = null;

                    oa = oad.Where(c => c.CURRENCY_CD == "IDR").FirstOrDefault();
                    z.AMOUNT_IDR = (oa != null) ? CommonFunction.Eval_Curr("IDR", oa.TOTAL_AMOUNT) : "";
                    oa = oad.Where(c => c.CURRENCY_CD == "JPY").FirstOrDefault();
                    z.AMOUNT_JPY = (oa != null) ? CommonFunction.Eval_Curr("JPY", oa.TOTAL_AMOUNT) : "";
                    oa = oad.Where(c => c.CURRENCY_CD == "USD").FirstOrDefault();
                    z.AMOUNT_USD = (oa != null) ? CommonFunction.Eval_Curr("USD", oa.TOTAL_AMOUNT) : "";

                    var qd = (from d in db.TB_R_PV_D
                              orderby d.SEQ_NO
                              where d.PV_NO == q.PV_NO && d.PV_YEAR == q.PV_YEAR
                              select d).FirstOrDefault();
                    if (qd != null)
                    {
                        z.DESCRIPTION = qd.DESCRIPTION;
                    }
                    z.DELETED = (q.DELETED ?? 0) == 1;
                    filled = true;
                }

                return filled;
            }
        }

        private void BudgetRelease(string key)
        {
            string[] k = key.Split(':');
            if (k.Length > 1)
            {
                int docno, docyear;
                Int32.TryParse(k[0], out docno);
                Int32.TryParse(k[1], out docyear);
                using (ContextWrap co = new ContextWrap())
                {
                    var db = co.db;
                    var q = from pv in db.TB_R_PV_H
                            where pv.PV_NO == docno // && pv.PV_YEAR == docyear 
                            orderby pv.PV_YEAR descending
                            select new
                            {
                                pv.BUDGET_NO
                                ,
                                pv.TOTAL_AMOUNT
                                ,
                                pv.PV_DATE
                            };

                    var x = q.FirstOrDefault();
                    int fiscalYear = logic.Sys.FiscalYear(x.PV_DATE);

                    BudgetCheckInput iBudget = new BudgetCheckInput()
                            {
                                DOC_STATUS = (int)BudgetCheckStatus.Release,
                                DOC_NO = docno,
                                //DOC_YEAR = docyear,
                                DOC_YEAR = fiscalYear,
                                WBS_NO = x.BUDGET_NO,
                                AMOUNT = x.TOTAL_AMOUNT ?? 0
                            };

                    BudgetCheckResult o;
                    if (!iBudget.WBS_NO.isEmpty())
                        if (Common.AppSetting.SKIP_BUDGET_CHECK)
                        {
                            o = logic.SAP._BudgetCheck_(iBudget);
                        }
                        else
                        {
                            o = logic.SAP.BudgetCheck(iBudget);
                        }

                }
            }
        }

        private void SetHistoryData(PVFormHistoryData d, PVFormHistoryData x)
        {
            d.PVNumber = x.PVNumber;
            d.PVYear = x.PVYear;
            d.Version = x.Version;
            d.StatusCode = x.StatusCode;
            d.PaymentMethodCode = x.PaymentMethodCode;
            d.VendorGroupCode = x.VendorGroupCode;
            d.VendorCode = x.VendorCode;
            d.PVTypeCode = x.PVTypeCode;
            d.TransactionCode = x.TransactionCode;
            d.SuspenseNumber = x.SuspenseNumber;
            d.BudgetNumber = x.BudgetNumber;
            d.ActivityDate = x.ActivityDate;
            d.ActivityDateTo = x.ActivityDateTo;
            d.DivisionID = x.DivisionID;
            d.PVDate = x.PVDate;
            d.TaxCode = x.TaxCode;
            d.TaxInvoiceNumber = x.TaxInvoiceNumber;
            d.PostingDate = x.PostingDate;
            d.PlanningPaymentDate = x.PlanningPaymentDate;
            d.BankType = x.BankType;
            d.TaxCalculated = x.TaxCalculated;
            d.SingleWbsUsed = x.SingleWbsUsed;
            d.Version = x.Version;
            d.UpdatedDt = x.UpdatedDt;
            d.UpdatedBy = x.UpdatedBy;
            d.PVTypeName = x.PVTypeName;
            d.VendorGroupName = x.VendorGroupName;
            d.PaymentMethodName = x.PaymentMethodName;
            d.StatusName = x.StatusName;
            d.VendorName = x.VendorName;
            d.TransactionName = x.TransactionName;
            d.SetNoPv = x.SetNoPv;
            d.SetNoRv = x.SetNoRv;
            d.SubmitDate = x.SubmitDate;
            d.PaidDate = x.PaidDate;
            d.WorkflowStatus = x.WorkflowStatus;
            d.SettlementStatus = x.SettlementStatus;
            d.ReferenceNo = x.ReferenceNo;
            //d.LocationCode = x.LocationCode;
            d.SubmitHcDocDate = x.SubmitHcDocDate;
            d.HoldFlag = x.HoldFlag;
            d.HoldBy = x.HoldBy;
            d.ModifiedBy = x.ModifiedBy;
            d.TotalAmount = x.TotalAmount;
            d.Details = GetDetailHistory(x.Details, false);
        }

        private static List<PVFormHistoryDetail> GetDetailHistory(List<PVFormHistoryDetail> x, bool forDelete)
        {
            if (x != null)
            {
                List<PVFormHistoryDetail> y = new List<PVFormHistoryDetail>();
                foreach (var z in x)
                {
                    PVFormHistoryDetail dt = new PVFormHistoryDetail()
                    {
                        SequenceNumber = z.SequenceNumber,
                        PVNumber = z.PVNumber,
                        PVYear = z.PVYear,
                        Version = z.Version,
                        CostCenterCode = z.CostCenterCode,
                        CostCenterCodeChange = z.CostCenterCodeChange,
                        CostCenterName = z.CostCenterName,
                        CostCenterNameChange = z.CostCenterNameChange,
                        Description = z.Description,
                        DescriptionChange = z.DescriptionChange,
                        CurrencyCode = z.CurrencyCode,
                        CurrencyCodeChange = z.CurrencyCodeChange,
                        Amount = z.Amount,
                        AmountChange = z.AmountChange,
                        TaxCode = z.TaxCode,
                        TaxCodeChange = z.TaxCodeChange,
                        TaxNumber = z.TaxNumber,
                        TaxNumberChange = z.TaxNumberChange,
                        ItemTransactionCode = z.ItemTransactionCode,
                        ItemTransactionCodeChange = z.ItemTransactionCodeChange,
                        ItemTransactionName = z.ItemTransactionName,
                        ItemTransactionNameChange = z.ItemTransactionNameChange,
                        //WbsNumber = z.WbsNumber,
                        //WbsNumberChange = z.WbsNumberChange,
                        InvoiceNumber = z.InvoiceNumber,
                        InvoiceNumberChange = z.InvoiceNumberChange,
                        IsDeleted = forDelete
                    };

                    y.Add(dt);
                }

                return y;
            }
            return null;
        }

        private static PVFormHistoryDetail GetDetailHistoryObj(PVFormHistoryDetail z)
        {
            if (z != null)
            {
                PVFormHistoryDetail dt = new PVFormHistoryDetail()
                {
                    SequenceNumber = z.SequenceNumber,
                    PVNumber = z.PVNumber,
                    PVYear = z.PVYear,
                    Version = z.Version,
                    CostCenterCode = z.CostCenterCode,
                    CostCenterCodeChange = z.CostCenterCodeChange,
                    CostCenterName = z.CostCenterName,
                    CostCenterNameChange = z.CostCenterNameChange,
                    Description = z.Description,
                    DescriptionChange = z.DescriptionChange,
                    CurrencyCode = z.CurrencyCode,
                    CurrencyCodeChange = z.CurrencyCodeChange,
                    Amount = z.Amount,
                    AmountChange = z.AmountChange,
                    TaxCode = z.TaxCode,
                    TaxCodeChange = z.TaxCodeChange,
                    TaxNumber = z.TaxNumber,
                    TaxNumberChange = z.TaxNumberChange,
                    ItemTransactionCode = z.ItemTransactionCode,
                    ItemTransactionCodeChange = z.ItemTransactionCodeChange,
                    ItemTransactionName = z.ItemTransactionName,
                    ItemTransactionNameChange = z.ItemTransactionNameChange,
                    //WbsNumber = z.WbsNumber,
                    //WbsNumberChange = z.WbsNumberChange,
                    InvoiceNumber = z.InvoiceNumber,
                    InvoiceNumberChange = z.InvoiceNumberChange,
                    IsDeleted = z.IsDeleted
                };

                return dt;
            }
            return null;
        }

        private static List<PVFormHistoryDetail> GetChangedDetailValue(List<PVFormHistoryDetail> det)
        {
            List<PVFormHistoryDetail> tmp = null;
            if (det != null && det.Any())
            {
                tmp = new List<PVFormHistoryDetail>();
                foreach (var d in det)
                {
                    d.CostCenterCodeChange = true;
                    d.CostCenterNameChange = true;
                    d.DescriptionChange = true;
                    d.CurrencyCodeChange = true;
                    d.AmountChange = true;
                    d.TaxCodeChange = true;
                    d.TaxNumberChange = true;
                    d.ItemTransactionCodeChange = true;
                    d.ItemTransactionNameChange = true;
                    //d.WbsNumberChange = true;
                    d.InvoiceNumberChange = true;
                    tmp.Add(d);
                }
            }
            return tmp;
        }

        private static PVFormHistoryDetail GetChangedDetailValue(PVFormHistoryDetail d)
        {
            if (d != null)
            {
                d.CostCenterCodeChange = true;
                d.CostCenterNameChange = true;
                d.DescriptionChange = true;
                d.CurrencyCodeChange = true;
                d.AmountChange = true;
                d.TaxCodeChange = true;
                d.TaxNumberChange = true;
                d.ItemTransactionCodeChange = true;
                d.ItemTransactionNameChange = true;
                //d.WbsNumberChange = true;
                d.InvoiceNumberChange = true;
            }
            return d;
        }

        public List<PVFormHistoryData> GetHistoryData(String pvNoParam, String pvYearParam)
        {
            List<PVFormHistoryData> returnData = new List<PVFormHistoryData>();


            int pvNo = Convert.ToInt32(pvNoParam);
            int pvYear = Convert.ToInt16(pvYearParam);

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var qq = from p in db.TB_H_PV_H

                         join ve in db.vw_Vendor on p.VENDOR_CD equals ve.VENDOR_CD into vc
                         from vcItem in vc.DefaultIfEmpty()

                         join vg in db.TB_M_VENDOR_GROUP on p.VENDOR_GROUP_CD equals vg.VENDOR_GROUP_CD into vgGroup
                         from vgItem in vgGroup.DefaultIfEmpty()

                         join t in db.TB_M_TRANSACTION_TYPE on p.TRANSACTION_CD equals t.TRANSACTION_CD into tty
                         from ttyItem in tty.DefaultIfEmpty()

                         join pm in db.TB_M_PAYMENT_METHOD on p.PAY_METHOD_CD equals pm.PAY_METHOD_CD into pmtd
                         from pmtdItem in pmtd.DefaultIfEmpty()

                         join s in db.TB_M_STATUS on p.STATUS_CD equals s.STATUS_CD into sts
                         from stsItem in sts.DefaultIfEmpty()

                         join pvt in db.TB_M_PV_TYPE on p.PV_TYPE_CD equals pvt.PV_TYPE_CD into pty
                         from ptyItem in pty.DefaultIfEmpty()

                         join u in db.vw_User on p.CHANGED_BY equals u.USERNAME into vwUser
                         from vu in vwUser.DefaultIfEmpty()

                         where (p.PV_NO.Equals(pvNo) && p.PV_YEAR.Equals(pvYear))
                         orderby p.VERSION
                         select new
                         {
                             p.VERSION,
                             p.CHANGED_DATE,
                             p.CHANGED_BY,
                             vu.LAST_NAME,
                             p.PV_DATE,
                             p.PV_YEAR,
                             p.PV_NO,
                             p.REFFERENCE_NO,
                             p.ACTIVITY_DATE_FROM,
                             p.ACTIVITY_DATE_TO,
                             p.POSTING_DATE,
                             p.PV_TYPE_CD,
                             ptyItem.PV_TYPE_NAME,
                             p.DIVISION_ID,
                             p.VENDOR_GROUP_CD,
                             vgItem.VENDOR_GROUP_NAME,
                             p.BANK_TYPE,
                             p.PLANNING_PAYMENT_DATE,
                             p.STATUS_CD,
                             stsItem.STATUS_NAME,
                             p.TRANSACTION_CD,
                             ttyItem.TRANSACTION_NAME,
                             p.VENDOR_CD,
                             vcItem.VENDOR_NAME,
                             p.PAY_METHOD_CD,
                             pmtdItem.PAY_METHOD_NAME,
                             p.BUDGET_NO,
                             p.SUSPENSE_NO,
                             p.SET_NO_PV,
                             p.SET_NO_RV,
                             p.SUBMIT_DATE,
                             p.PAID_DATE,
                             p.WORKFLOW_STATUS,
                             p.SETTLEMENT_STATUS,
                             //p.LOCATION_CD,
                             p.SUBMIT_HC_DOC_DATE,
                             p.HOLD_BY,
                             p.HOLD_FLAG,
                             p.TOTAL_AMOUNT
                         };

                PVFormHistoryData old = null;
                foreach (var q in qq)
                {
                    PVFormHistoryData _new = new PVFormHistoryData();
                    PVFormHistoryData x = new PVFormHistoryData();

                    int TransactionCd = q.TRANSACTION_CD ?? -1;

                    _new.Version = q.VERSION;
                    _new.PVNumber = q.PV_NO;
                    _new.PVYear = q.PV_YEAR;
                    _new.UpdatedDt = q.CHANGED_DATE;
                    _new.UpdatedBy = q.CHANGED_BY;
                    _new.PVDate = q.PV_DATE;
                    _new.DivisionID = q.DIVISION_ID;

                    _new.PVTypeCode = q.PV_TYPE_CD;
                    _new.PVTypeName = q.PV_TYPE_NAME;
                    _new.VendorGroupCode = q.VENDOR_GROUP_CD;
                    _new.VendorGroupName = q.VENDOR_GROUP_NAME;
                    _new.PaymentMethodCode = q.PAY_METHOD_CD;
                    _new.PaymentMethodName = q.PAY_METHOD_NAME;
                    _new.StatusCode = q.STATUS_CD;
                    _new.StatusName = q.STATUS_NAME;
                    _new.VendorCode = q.VENDOR_CD;
                    _new.VendorName = q.VENDOR_NAME;
                    _new.TransactionCode = q.TRANSACTION_CD;
                    _new.TransactionName = q.TRANSACTION_NAME;
                    _new.ActivityDate = q.ACTIVITY_DATE_FROM;
                    _new.ActivityDateTo = q.ACTIVITY_DATE_TO;
                    _new.PostingDate = q.POSTING_DATE;
                    _new.PlanningPaymentDate = q.PLANNING_PAYMENT_DATE;
                    _new.BankType = q.BANK_TYPE;
                    _new.BudgetNumber = q.BUDGET_NO;
                    _new.SetNoPv = q.SET_NO_PV;
                    _new.SetNoRv = q.SET_NO_RV;
                    _new.SubmitDate = q.SUBMIT_DATE;
                    _new.PaidDate = q.PAID_DATE;
                    _new.WorkflowStatus = q.WORKFLOW_STATUS;
                    _new.SettlementStatus = q.SETTLEMENT_STATUS;
                    _new.ReferenceNo = q.REFFERENCE_NO;
                    //_new.LocationCode = q.LOCATION_CD;
                    _new.SubmitHcDocDate = q.SUBMIT_HC_DOC_DATE;
                    _new.HoldBy = q.HOLD_BY;
                    _new.HoldFlag = q.HOLD_FLAG;
                    _new.ModifiedBy = q.LAST_NAME;
                    _new.TotalAmount = q.TOTAL_AMOUNT;

                    // get detail
                    List<PVFormHistoryDetail> historyDetail = null;
                    var dd = from d in db.TB_H_PV_D
                             join c in db.vw_CostCenter on d.COST_CENTER_CD equals c.COST_CENTER into cctr
                             from subCctr in cctr.DefaultIfEmpty()
                             join it in db.TB_M_ITEM_TRANSACTION
                                on new { tcode = TransactionCd, titem = d.ITEM_TRANSACTION_CD ?? -1 }
                                equals new { tcode = it.TRANSACTION_CD, titem = it.ITEM_TRANSACTION_CD }
                             into itTrans
                             from subIt in itTrans.DefaultIfEmpty()
                             where (d.PV_NO.Equals(pvNo) && d.PV_YEAR.Equals(pvYear) && d.VERSION.Equals(_new.Version))
                             orderby d.SEQ_NO
                             select new
                             {
                                 d.SEQ_NO,
                                 d.VERSION,
                                 d.PV_YEAR,
                                 d.PV_NO,
                                 d.COST_CENTER_CD,
                                 // COST_CENTER_NAME = d.COST_CENTER_CD,
                                 COST_CENTER_NAME = subCctr.DESCRIPTION,
                                 d.CURRENCY_CD,
                                 d.DESCRIPTION,
                                 d.AMOUNT,
                                 // d.WBS_NO,
                                 d.INVOICE_NO,
                                 d.ITEM_TRANSACTION_CD,
                                 subIt.ITEM_TRANSACTION_DESC,
                                 d.TAX_CD,
                                 d.TAX_NO
                             };

                    if (dd.Any())
                    {
                        historyDetail = new List<PVFormHistoryDetail>();
                        foreach (var d in dd)
                        {
                            PVFormHistoryDetail dt = new PVFormHistoryDetail()
                            {
                                SequenceNumber = d.SEQ_NO,
                                PVNumber = d.PV_NO,
                                PVYear = d.PV_YEAR,
                                Version = d.VERSION,
                                CostCenterCode = d.COST_CENTER_CD,
                                CostCenterCodeChange = false,
                                CostCenterName = d.COST_CENTER_NAME,
                                CostCenterNameChange = false,
                                Description = d.DESCRIPTION,
                                DescriptionChange = false,
                                CurrencyCode = d.CURRENCY_CD,
                                CurrencyCodeChange = false,
                                Amount = d.AMOUNT,
                                AmountChange = false,
                                TaxCode = d.TAX_CD,
                                TaxCodeChange = false,
                                TaxNumber = d.TAX_NO,
                                TaxNumberChange = false,
                                ItemTransactionCode = d.ITEM_TRANSACTION_CD,
                                ItemTransactionCodeChange = false,
                                ItemTransactionName = d.ITEM_TRANSACTION_DESC,
                                ItemTransactionNameChange = false,
                                //WbsNumber = d.WBS_NO,
                                //WbsNumberChange = false,
                                InvoiceNumber = d.INVOICE_NO,
                                InvoiceNumberChange = false,
                                IsDeleted = false
                            };
                            historyDetail.Add(dt);
                        }

                        _new.Details = historyDetail;
                    }

                    SetHistoryData(x, _new);

                    if (old != null)
                    {
                        x.Version = _new.Version;
                        x.PVNumber = _new.PVNumber;
                        x.PVYear = _new.PVYear;

                        if (_new.PVDate.Equals(old.PVDate))
                            x.PVDate = null;

                        if (_new.DivisionID == null || _new.DivisionID.Equals(old.DivisionID))
                            x.DivisionID = null;

                        if (_new.PVTypeCode.Equals(old.PVTypeCode))
                        {
                            x.PVTypeCode = null;
                            x.PVTypeName = null;
                        }

                        if (_new.VendorGroupCode.Equals(old.VendorGroupCode))
                        {
                            x.VendorGroupCode = null;
                            x.VendorGroupName = null;
                        }

                        if (_new.PaymentMethodCode == null || _new.PaymentMethodCode.Equals(old.PaymentMethodCode))
                        {
                            x.PaymentMethodCode = null;
                            x.PaymentMethodName = null;
                        }

                        if (_new.StatusCode.Equals(old.StatusCode))
                        {
                            x.StatusCode = null;
                            x.StatusName = null;
                        }

                        if (_new.VendorCode == null || _new.VendorCode.Equals(old.VendorCode))
                        {
                            x.VendorCode = null;
                            x.VendorName = null;
                        }

                        if (_new.TransactionCode.Equals(old.TransactionCode))
                        {
                            x.TransactionCode = null;
                            x.TransactionName = null;
                        }

                        if (_new.ActivityDate.Equals(old.ActivityDate)) x.ActivityDate = null;
                        if (_new.ActivityDateTo.Equals(old.ActivityDateTo)) x.ActivityDateTo = null;
                        if (_new.PostingDate.Equals(old.PostingDate)) x.PostingDate = null;
                        if (_new.PlanningPaymentDate.Equals(old.PlanningPaymentDate)) x.PlanningPaymentDate = null;
                        if (_new.BankType.Equals(old.BankType)) x.BankType = null;
                        if (_new.BudgetNumber == null || _new.BudgetNumber.Equals(old.BudgetNumber)) x.BudgetNumber = null;
                        if (_new.SuspenseNumber.Equals(old.SuspenseNumber)) x.SuspenseNumber = null;
                        if (_new.SetNoPv.Equals(old.SetNoPv)) x.SetNoPv = null;
                        if (_new.SetNoRv.Equals(old.SetNoRv)) x.SetNoRv = null;
                        if (_new.SubmitDate.Equals(old.SubmitDate)) x.SubmitDate = null;
                        if (_new.PaidDate.Equals(old.PaidDate)) x.PaidDate = null;
                        if (_new.WorkflowStatus.Equals(old.WorkflowStatus)) x.WorkflowStatus = null;
                        if (_new.SettlementStatus.Equals(old.SettlementStatus)) x.SettlementStatus = null;
                        if (_new.ReferenceNo.Equals(old.ReferenceNo)) x.ReferenceNo = null;
                        //if (_new.LocationCode.Equals(old.LocationCode))
                        //{
                        //    x.LocationCode = null;
                        //}
                        if (_new.SubmitHcDocDate.Equals(old.SubmitHcDocDate)) x.SubmitHcDocDate = null;
                        if (_new.HoldBy == null || _new.HoldBy.Equals(old.HoldBy)) x.HoldBy = null;
                        if (_new.HoldFlag.Equals(old.HoldFlag)) x.HoldFlag = null;
                        if (_new.TotalAmount.Equals(old.TotalAmount)) x.TotalAmount = null;

                        // Compare details
                        if (_new.Details == null && old.Details != null)
                            x.Details = GetDetailHistory(old.Details, true);
                        else if (_new.Details != null && old.Details == null)
                            x.Details = GetChangedDetailValue(_new.Details);
                        else if (_new.Details != null && old.Details != null)
                        {
                            List<PVFormHistoryDetail> tmp = new List<PVFormHistoryDetail>();
                            PVFormHistoryDetail detTmp = new PVFormHistoryDetail();
                            foreach (var n in _new.Details)
                            {
                                bool isAdded = true;
                                foreach (var o in old.Details)
                                {
                                    if (o.SequenceNumber.Equals(n.SequenceNumber))
                                    {
                                        isAdded = false;
                                        bool removeFromList = true;
                                        detTmp = GetDetailHistoryObj(n);
                                        if ((n.CostCenterCode == null && o.CostCenterCode != null) ||
                                            (n.CostCenterCode != null && !n.CostCenterCode.Equals(o.CostCenterCode)))
                                        {
                                            detTmp.CostCenterCodeChange = true;
                                            detTmp.CostCenterNameChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.Description == null && o.Description != null) ||
                                            (n.Description != null && !n.Description.Equals(o.Description)))
                                        {
                                            detTmp.DescriptionChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.StandardDescriptionWording == null && o.StandardDescriptionWording != null) ||
                                            (n.StandardDescriptionWording != null && !n.StandardDescriptionWording.Equals(o.StandardDescriptionWording)))
                                        {
                                            detTmp.StandardDescriptionWordingChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.CurrencyCode == null && o.CurrencyCode != null) ||
                                            (n.CurrencyCode != null && !n.CurrencyCode.Equals(o.CurrencyCode)))
                                        {
                                            detTmp.CurrencyCodeChange = true;
                                            removeFromList = false;
                                        }

                                        if (!n.Amount.Equals(o.Amount))
                                        {
                                            detTmp.AmountChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.TaxCode == null && o.TaxCode != null) ||
                                            (n.TaxCode != null && !n.TaxCode.Equals(o.TaxCode)))
                                        {
                                            detTmp.TaxCodeChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.TaxNumber == null && o.TaxNumber != null) ||
                                            (n.TaxNumber != null && !n.TaxNumber.Equals(o.TaxNumber)))
                                        {
                                            detTmp.TaxNumberChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.ItemTransactionCode == null && o.ItemTransactionCode != null) ||
                                            (n.ItemTransactionCode != null && !n.ItemTransactionCode.Equals(o.ItemTransactionCode)))
                                        {
                                            detTmp.ItemTransactionCodeChange = true;
                                            detTmp.ItemTransactionNameChange = true;
                                            removeFromList = false;
                                        }

                                        //if ((n.WbsNumber == null && o.WbsNumber != null) ||
                                        //    (n.WbsNumber != null && !n.WbsNumber.Equals(o.WbsNumber)))
                                        //{
                                        //    detTmp.WbsNumberChange = true;
                                        //    removeFromList = false;
                                        //}

                                        if ((n.InvoiceNumber == null && o.InvoiceNumber != null) ||
                                            (n.InvoiceNumber != null && !n.InvoiceNumber.Equals(o.InvoiceNumber)))
                                        {
                                            detTmp.InvoiceNumberChange = true;
                                            removeFromList = false;
                                        }

                                        if ((n.InvoiceNumberUrl == null && o.InvoiceNumberUrl != null) ||
                                            (n.InvoiceNumberUrl != null && !n.InvoiceNumberUrl.Equals(o.InvoiceNumberUrl)))
                                        {
                                            detTmp.InvoiceNumberUrlChange = true;
                                            removeFromList = false;
                                        }

                                        if (!removeFromList)
                                            tmp.Add(detTmp);
                                    }
                                }
                                if (isAdded)
                                {
                                    detTmp = GetDetailHistoryObj(n);
                                    tmp.Add(GetChangedDetailValue(detTmp));
                                }
                            }
                            foreach (var o in old.Details)
                            {
                                bool isDeleted = true;
                                foreach (var n in _new.Details)
                                {
                                    if (o.SequenceNumber.Equals(n.SequenceNumber))
                                    {
                                        isDeleted = false;
                                        break;
                                    }
                                }
                                if (isDeleted)
                                {
                                    detTmp = GetDetailHistoryObj(o);
                                    detTmp.IsDeleted = true;
                                    tmp.Add(detTmp);
                                }
                            }

                            x.Details = tmp;
                        }
                    }
                    else
                    {
                        old = new PVFormHistoryData();
                    }
                    returnData.Add(x);
                    SetHistoryData(old, _new);
                }
            }
            returnData.Reverse();

            return returnData;
        }


        public bool CanPrintTicketOn(DateTime Plan, int pvTypeCd)
        {
            // Start Rinda Rahayu 20160622
            bool resultTest = false;
            int intDate = ((int)DateTime.Now.Date.Subtract(Plan).TotalDays);
            if (intDate == -1 || intDate == -2 || intDate >= 0)
            {
                resultTest = true;
            }
            // End Rinda Rahayu 20160622

            return resultTest;

           /* return (
                ((int)Plan.Subtract(DateTime.Now.Date).TotalDays) <= 0
                );*/

        }

        public bool isCanDirect(string pvNoParam, string pvYearParam, string userName, decimal div)
        {
            bool retVal = false;
            decimal reffNo = Decimal.Parse(pvNoParam + pvYearParam);
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = db.TB_R_DISTRIBUTION_STATUS
                        .Where(a => a.REFF_NO == reffNo && a.ACTUAL_DT == null)
                        .OrderBy(a => a.STATUS_CD).ToList();
                if (q.Any())
                {
                    var s = q.Where(a => a.PROCCED_BY == userName).FirstOrDefault();
                    if (s != null)
                    {
                        if (s.STATUS_CD > 19 && q.FirstOrDefault().STATUS_CD > 19)
                        {
                            retVal = true;
                        }
                        else if (s.STATUS_CD < 20 && q.FirstOrDefault().STATUS_CD < 20)
                        {
                            retVal = true;
                        }
                    }
                }
            }
            return retVal;
        }

        public List<AccruedListDetail> GetActivityAccrued(Dictionary<string, object> param)
        {
            string accruedNo = "";
            if(param.ContainsKey("ACCRUED_NO"))
                accruedNo = param["ACCRUED_NO"].ToString();


            var q = (from d in db.TB_R_ACCR_LIST_D
                     where d.ACCRUED_NO == accruedNo
                     select new AccruedListDetail 
                     { 
                         ACCRUED_NO = d.ACCRUED_NO,
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            if (param.ContainsKey("BOOKING_NO"))
            { 
                string bookingNo = param["BOOKING_NO"].ToString();
                q = q.Where(p => p.BOOKING_NO == bookingNo);
            }

            if (q.Any()) return q.ToList();

            return null;
        }

        #region Download Accrued List
        //Added by fid.Taufik

        public IQueryable<AccrFormDetail> SearchDataDownload(AccruedShiftingSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from h in db.TB_R_ACCR_LIST_D
                     join d in db.TB_M_ACCR_PV_TYPE on h.PV_TYPE_CD equals d.PV_TYPE_CD into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.vw_WBS on h.WBS_NO_OLD equals s.WbsNumber into ss
                     from s in ss.DefaultIfEmpty()
                     join m in db.TB_R_ACCR_LIST_H on h.ACCRUED_NO equals m.ACCRUED_NO into mm
                     from m in mm.DefaultIfEmpty()
                     join div in db.vw_Division on m.DIVISION_ID equals div.DIVISION_ID into divl
                     from div in divl.DefaultIfEmpty()

                     select new AccrFormDetail()
                     {
                         AccruedNo = h.ACCRUED_NO,
                         BookingNo = h.BOOKING_NO,
                         WbsNumber = h.WBS_NO_OLD,
                         WbsDesc = s.Description,
                         SuspenseNo = h.SUSPENSE_NO_OLD.HasValue ? SqlFunctions.StringConvert((double)h.SUSPENSE_NO_OLD.Value) : "",
                         PVType = d.PV_TYPE_NAME,
                         ActivityDescription = h.ACTIVITY_DES,
                         CostCenterCode = h.COST_CENTER,
                         Amount = h.AMOUNT != null ? h.AMOUNT.Value : 0,
                         CurrencyCode = h.CURRENCY_CD,
                         DivisionName = div.DIVISION_NAME,
                         DivisionId = m.DIVISION_ID,
                         AmountIdr = h.AMOUNT_IDR != null ? h.AMOUNT_IDR.Value : 0,
                         ChangedDt = m.CHANGED_DT

                     }).Distinct();

            if (!String.IsNullOrEmpty(crit.accrNo))
            {

                q = q.Where(p => p.AccruedNo == crit.accrNo);

            }

            return q;
        }

        //public byte[] GetExcelDownload(
        //    string _username,
        //    string templateFile,
        //    AccruedSearchCriteria c)
        //{

        //    if (!File.Exists(templateFile))
        //    {
        //        log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
        //        return null;
        //    }

        //    ExcelWriter x = new ExcelWriter(templateFile);
        //    string _sheetName = "ACCR";

        //    x.MarkPage(_sheetName);
        //    x.MarkRow("[BOOKING_NO],[WBS_NO_OLD],[WBS_DESC],[SUSPENSE_NO_OLD],"
        //        + "[PV_TYPE],[ACTIVITY_DES],[COST_CENTER],[CURRENCY_CD],[AMOUNT],[AMOUNT_IN_RP],"
        //        + "[SAP_AVAILABLE],[SAP_REMAINING]");

        //    int j = 0;


        //    var qq = SearchDataDownload(c);

        //    List<AccruedListDetail> _list =
        //        qq.ToList();
        //    Dictionary<int, string> doctype = logic.Look.GetDocType();
        //    foreach (AccruedListDetail dx in _list)
        //    {
        //        x.PutRow(dx.BOOKING_NO,dx.WBS_NO_OLD,dx.WBS_DESC,
        //                    dx.SUSPENSE_NO_OLD,dx.PV_TYPE_NAME,dx.ACTIVITY_DES,dx.COST_CENTER,
        //                    dx.CURRENCY_CD, dx.AMOUNT, dx.AMOUNT_IN_RP, 0, 0);
                
        //    }

        //    if (j < 1)
        //    {
        //        x.PutRow("", "", "", "", "",
        //                 "", "", "", "", "",
        //                 "", "");
        //    }

        //    x.PutTail();
        //    return x.GetBytes();
        //}

        public string GetLinkExcelDownload(
            string _username,
            string templateFile,
            string newFile,
            AccruedShiftingSearchCriteria c,
            UserData userData,
            string pdfExport = "")
        {
            try
            {
                if (!File.Exists(templateFile))
                {
                    log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                    return null;
                }

                ExcelWriter x = new ExcelWriter(templateFile);
                string _sheetName = "ACCR";

                x.MarkPage(_sheetName);

                //Get data from search criteria
                var qq = SearchDataDownload(c);
                List<AccrFormDetail> _list = qq.ToList();
                _list.Sort(AccrFormDetail.RuleOrder);

                //Set value Header
                string divisionName = "";
                string accrNo = "";
                string updatedDt = "";
                decimal usdToIdr = 0;
                decimal jpyToIdr = 0;
                string printedBy = "";
                string printedDt = "";
                decimal totalAmount = 0;

                printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
                printedDt = DateTime.Now.ToString("dd-MM-yyyy");
                foreach (AccrFormDetail hAccr in _list)
                {
                    divisionName = hAccr.DivisionName;
                    accrNo = hAccr.AccruedNo;
                    updatedDt = hAccr.ChangedDt.HasValue
                                ? hAccr.ChangedDt.Value.ToString("dd-MM-yyyy")
                                : "";
                    totalAmount = totalAmount + hAccr.AmountIdr;


                }

                var q = db.vw_ExchangeRate.ToList();
                if (q.Any())
                {
                    var usd = q.Where(a => a.CURRENCY_CD == "USD").FirstOrDefault();
                    if (usd != null)
                    {
                        usdToIdr = usd.EXCHANGE_RATE;
                    }

                    var jpy = q.Where(a => a.CURRENCY_CD == "JPY").FirstOrDefault();
                    if (jpy != null)
                    {
                        jpyToIdr = jpy.EXCHANGE_RATE;
                    }
                }

                x.MarkCells("[DIVISION],[ACCRUED_NO],[UPDATED_DT],[USD_RATE],[JPY_RATE],[PRINTED_BY],[PRINTED_DT],[TOTAL_AMOUNT]");
                x.PutCells(divisionName, accrNo, updatedDt, usdToIdr.ToString("G"), jpyToIdr.ToString("G"), printedBy, printedDt, totalAmount.ToString("G"));
                //End set value Header

                //Set Value Detail
                x.MarkRow("[BOOKING_NO],[WBS_NO_OLD],[WBS_DESC],[SUSPENSE_NO_OLD],"
                    + "[PV_TYPE],[ACTIVITY_DES],[COST_CENTER],[CURRENCY_CD],[AMOUNT],[AMOUNT_IN_RP],"
                    + "[SAP_AVAILABLE],[SAP_REMAINING]");

                int j = 0;

                Dictionary<int, string> doctype = logic.Look.GetDocType();
                foreach (AccrFormDetail dx in _list)
                {
                    //Input Data to list detail
                    x.PutRowInsert(dx.BookingNo, dx.WbsNumber, dx.WbsDesc,
                            dx.SuspenseNo, dx.PVType, dx.ActivityDescription, dx.CostCenterCode,
                            dx.CurrencyCode, dx.Amount, dx.AmountIdr, 0, 0);


                    j++;

                }

                if (j < 1)
                {
                    x.PutRow("", "", "", "", "",
                             "", "", "", "", "",
                             "", "");
                }
                //End set value Detail

                x.PutTail();
                string _strTempDir = LoggingLogic.GetTempPath("");
                string filePath = System.IO.Path.Combine(_strTempDir, newFile);
                if (!Directory.Exists(_strTempDir))
                    Directory.CreateDirectory(_strTempDir);
                x.Write(filePath);

                if (String.IsNullOrEmpty(pdfExport))
                {
                    return filePath;
                }
                else
                {
                    string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                    bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                    if (rslt)
                    {
                        return pdfPath;
                    }
                    else
                    {
                        return filePath;
                    }

                }
            }
            catch (Exception e)
            {
                Handle(e);
                throw;
            }
        }
        #endregion

        #region Download Accrued WBS
        //Added by fid.Taufik
        public IQueryable<AccruedListDetail> SearchDataDownloadWBS(AccruedShiftingSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE 
                     join ttsw in db.vw_WBS on trab.WBS_NO_OLD equals ttsw.WbsNumber into a
                     from ttsw in a.DefaultIfEmpty() 
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO into b
                     from trald in b.DefaultIfEmpty()
                     join tralh in db.TB_R_ACCR_LIST_H on trald.ACCRUED_NO equals tralh.ACCRUED_NO into c
                     from tralh in c.DefaultIfEmpty()
                     join tmpt in db.TB_M_PV_TYPE on trald.PV_TYPE_CD equals tmpt.PV_TYPE_CD into d
                     from tmpt in d.DefaultIfEmpty()
                     

                     select new AccruedListDetail()
                     {
                         ACCRUED_NO = trald.ACCRUED_NO,
                         BOOKING_NO = trab.BOOKING_NO,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC = ttsw.Description,
                         PV_TYPE_NAME = tmpt.PV_TYPE_NAME,
                         INIT_AMT = trab.INIT_AMT != null ? trab.INIT_AMT : 0,
                         SAP_AVAILABLE = 0, // Retrieve from SAP
                         SAP_REMAINING = trab.INIT_AMT - trab.SPENT_AMT,
                         UPDATED_DT = tralh.CHANGED_DT

                     }).Distinct();

            var allowedModule = new[] { 0, 3 };

            if (!String.IsNullOrEmpty(crit.issuingDivision))
            {

                //q = q.Where(p => p.DIVISION_ID == crit.issuingDivision);

            }

            if (!String.IsNullOrEmpty(crit.accrNo))
            {

                q = q.Where(p => p.ACCRUED_NO == crit.accrNo);

            }

            return q;
        }


        public string DownloadListAccrWBS(
            string _username,
            string templateFile,
            string newFile,
            AccruedShiftingSearchCriteria c,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "ACCR";

            x.MarkPage(_sheetName);

            //Get data from search criteria
            var qq = SearchDataDownloadWBS(c);
            List<AccruedListDetail> _list =
                qq.ToList();

            //Set value Header
            string divisionName = "";
            string accrNo = "";
            string updatedDt = "";
            string pvType = "";
            string printedBy = "";
            string printedDt = "";

            printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            printedDt = DateTime.Now.ToString("dd-MM-yyyy");
            divisionName = userData.DIV_NAME;
            foreach (AccruedListDetail hAccr in _list)
            {
                accrNo = hAccr.ACCRUED_NO;
                updatedDt = hAccr.UPDATED_DT.HasValue
                            ? hAccr.UPDATED_DT.Value.ToString("dd-MM-yyyy")
                            : "";
                pvType = hAccr.PV_TYPE_NAME;

            }

            x.MarkCells("[DIVISION],[ACCRUED_NO],[UPDATED_DT],[PV_TYPE],[PRINTED_BY],[PRINTED_DT]");
            x.PutCells(divisionName, accrNo, updatedDt, pvType, printedBy, printedDt);
            //End set value Header

            //Set Value Detail
            x.MarkRow("[BOOKING_NO],[WBS_NO_OLD],[WBS_DESC],"
                + "[AMOUNT_IN_RP],[WBS_NO_NEW],"
                + "[SAP_AVAILABLE],[SAP_REMAINING]");

            int j = 0;

            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedListDetail dx in _list)
            {
                //Input Data to list detail
                x.PutRow(dx.BOOKING_NO, dx.WBS_NO_OLD, dx.WBS_DESC,
                        dx.AMOUNT_IN_RP, dx.WBS_NO, dx.SAP_AVAILABLE, 0);
                j++;

            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "");
            }
            //End set value Detail

            x.PutTail();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            if (String.IsNullOrEmpty(pdfExport))
            {
                return filePath;
            }
            else
            {
                string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                if (rslt)
                {
                    return pdfPath;
                }
                else
                {
                    return filePath;
                }

            }
        }
        #endregion

        #region Download List Accrued Result
        //Added by fid.Taufik

        public IQueryable<AccruedListDetail> DataListAccrResult(AccruedResultSearch crit)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trab.PV_TYPE_CD equals tmapt.PV_TYPE_CD into d
                     from tmapt in d.DefaultIfEmpty()
                     join ttsw in db.vw_WBS on trab.WBS_NO_OLD equals ttsw.WbsNumber into a
                     from ttsw in a.DefaultIfEmpty()
                     join ttsw1 in db.vw_WBS on trab.WBS_NO_PR equals ttsw1.WbsNumber into a1
                     from ttsw1 in a1.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO into b
                     from trald in b.DefaultIfEmpty()
                     join tralh in db.TB_R_ACCR_LIST_H on trald.ACCRUED_NO equals tralh.ACCRUED_NO into c
                     from tralh in c.DefaultIfEmpty()
                     join vd in db.vw_Division on tralh.DIVISION_ID equals vd.DIVISION_ID into e
                     where trald.ACCRUED_NO == crit.AccruedNo
                     from vd in e.DefaultIfEmpty()

                     select new AccruedListDetail()
                     {
                         ACCRUED_NO = trald.ACCRUED_NO,
                         BOOKING_NO = trab.BOOKING_NO,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_NO = trab.WBS_NO_PR,
                         WBS_DESC = ttsw.Description,
                         WBS_DESC_PR = ttsw1.Description,
                         SUSPENSE_NO_OLD = trab.SUSPENSE_NO_OLD,
                         SUSPENSE_NO_PR = trab.SUSPENSE_NO_NEW,
                         INIT_AMT = trab.INIT_AMT,
                         SAP_SPENT = trab.SPENT_AMT,
                         SAP_REMAINING = trab.INIT_AMT - trab.SPENT_AMT,
                         UPDATED_DT = tralh.CHANGED_DT,
                         DIVISION_NAME = vd.DIVISION_NAME,
                         PV_TYPE_CD = trab.PV_TYPE_CD,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME,
                         ACTIVITY_DES = crit.caller == 2 ? trald.ACTIVITY_DES : null

                     }).Distinct();

            #region Criteria

            switch (crit.iBookingNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.BOOKING_NO.EndsWith(crit.sBookingNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.BOOKING_NO.StartsWith(crit.sBookingNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.BOOKING_NO.Contains(crit.sBookingNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.BOOKING_NO.StartsWith(crit.sBookingNoPre) && p.BOOKING_NO.EndsWith(crit.sBookingNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.BOOKING_NO.Equals(crit.BookingNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iActivity)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.ACTIVITY_DES.EndsWith(crit.sActivityPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.ACTIVITY_DES.StartsWith(crit.sActivityPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.ACTIVITY_DES.Contains(crit.sActivityPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.ACTIVITY_DES.StartsWith(crit.sActivityPre) && p.ACTIVITY_DES.EndsWith(crit.sActivityPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.ACTIVITY_DES.Equals(crit.Activity));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iOldWbsNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_NO_OLD.EndsWith(crit.sOldWbsNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_NO_OLD.StartsWith(crit.sOldWbsNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_NO_OLD.Contains(crit.sOldWbsNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_NO_OLD.StartsWith(crit.sOldWbsNoPre) && p.WBS_NO_OLD.EndsWith(crit.sOldWbsNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_NO_OLD.Equals(crit.OldWbsNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iOldWbsDesc)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_DESC.EndsWith(crit.sOldWbsDescPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_DESC.StartsWith(crit.sOldWbsDescPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_DESC.Contains(crit.sOldWbsDescPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_DESC.StartsWith(crit.sOldWbsDescPre) && p.WBS_DESC.EndsWith(crit.sOldWbsDescPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_DESC.Equals(crit.OldWbsDesc));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iOldSuspenseNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && p.SUSPENSE_NO_OLD.ToString().EndsWith(crit.sOldSuspenseNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && p.SUSPENSE_NO_OLD.ToString().StartsWith(crit.sOldSuspenseNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && p.SUSPENSE_NO_OLD.ToString().Contains(crit.sOldSuspenseNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && p.SUSPENSE_NO_OLD.ToString().StartsWith(crit.sOldSuspenseNoPre) 
                                    && p.SUSPENSE_NO_OLD.ToString().EndsWith(crit.sOldSuspenseNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.SUSPENSE_NO_OLD.HasValue && p.SUSPENSE_NO_OLD.ToString().Equals(crit.OldSuspenseNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iNewWbsNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_NO.EndsWith(crit.sNewWbsNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_NO.StartsWith(crit.sNewWbsNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_NO.Contains(crit.sNewWbsNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_NO.StartsWith(crit.sNewWbsNoPre) && p.WBS_NO.EndsWith(crit.sNewWbsNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_NO.Equals(crit.NewWbsNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iNewWbsDesc)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.WBS_DESC_PR.EndsWith(crit.sNewWbsDescPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.WBS_DESC_PR.StartsWith(crit.sNewWbsDescPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.WBS_DESC_PR.Contains(crit.sNewWbsDescPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.WBS_DESC_PR.StartsWith(crit.sNewWbsDescPre) && p.WBS_NO.EndsWith(crit.sNewWbsDescPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.WBS_DESC_PR.Equals(crit.NewWbsDesc));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            switch (crit.iNewSuspenseNo)
            {
                case LikeLogic.igFIRST:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && p.SUSPENSE_NO_PR.ToString().EndsWith(crit.sNewSuspenseNoPost));
                    break;
                case LikeLogic.igLAST:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && p.SUSPENSE_NO_PR.ToString().StartsWith(crit.sNewSuspenseNoPre));
                    break;
                case LikeLogic.igFIRSTLAST:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && p.SUSPENSE_NO_PR.ToString().Contains(crit.sNewSuspenseNoPre));
                    break;
                case LikeLogic.igMIDDLE:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && p.SUSPENSE_NO_PR.ToString().StartsWith(crit.sNewSuspenseNoPre)
                        && p.SUSPENSE_NO_PR.ToString().EndsWith(crit.sNewSuspenseNoPost));
                    break;
                case LikeLogic.igNONE:
                    q = q.Where(p => p.SUSPENSE_NO_PR.HasValue && p.SUSPENSE_NO_PR.ToString().Equals(crit.NewSuspenseNo));
                    break;
                case LikeLogic.igALL:
                    break;
                default:
                    break;
            }

            if (crit.PVType > 0)
            {
                q = q.Where(p => p.PV_TYPE_CD == crit.PVType);
            }

            if (crit.AmountAccrFrom > 0)
            {
                q = q.Where(p => p.INIT_AMT >= crit.AmountAccrFrom);
            }

            if (crit.AmountAccrTo > 0)
            {
                q = q.Where(p => p.INIT_AMT <= crit.AmountAccrTo);
            }

            if (crit.AmountSpentFrom > 0)
            {
                q = q.Where(p => p.INIT_AMT >= crit.AmountRemainFrom);
            }

            if (crit.AmountSpentTo > 0)
            {
                q = q.Where(p => p.INIT_AMT <= crit.AmountRemainTo);
            }

            if (crit.AmountRemainFrom > 0)
            {
                q = q.Where(p => p.INIT_AMT >= crit.AmountRemainFrom);
            }

            if (crit.AmountRemainTo > 0)
            {
                q = q.Where(p => p.INIT_AMT <= crit.AmountRemainTo);
            }

            #endregion

            return q;
        }

        public string GenerateListAccrResult(
            string _username,
            string templateFile,
            string newFile,
            AccruedResultSearch c,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "ACCR";

            x.MarkPage(_sheetName);

            //Get data from search criteria
            c.caller = 2;
            var qq = DataListAccrResult(c);
            List<AccruedListDetail> _list =
                qq.ToList();
            _list.Sort(AccruedListDetail.RuleOrder);

            //Set value Header
            string divisionName = userData.DIV_NAME;
            string titleReport = "LIST ACCRUED TAHUN " + DateTime.Now.ToString("yyyy");
            string accrNo = "";
            string updatedDt = "";
            decimal usdToIdr = 0;
            decimal jpyToIdr = 0;
            string printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            string printedDt = DateTime.Now.ToString("dd-MM-yyyy");
            decimal? totalAmount = 0;
            decimal? totalSpent = 0;
            decimal? totalRemaining = 0;

            foreach (AccruedListDetail hAccr in _list)
            {
                divisionName = hAccr.DIVISION_NAME;
                accrNo = hAccr.ACCRUED_NO;
                updatedDt = hAccr.UPDATED_DT.HasValue
                            ? hAccr.UPDATED_DT.Value.ToString("dd-MM-yyyy")
                            : "";
                totalAmount = totalAmount + hAccr.INIT_AMT;
                totalSpent = totalSpent + hAccr.SAP_SPENT;
                totalRemaining = totalRemaining + hAccr.SAP_REMAINING;
            }

            var q = db.vw_ExchangeRate.ToList();
            if (q.Any())
            {
                var usd = q.Where(a => a.CURRENCY_CD == "USD").FirstOrDefault();
                if (usd != null)
                {
                    usdToIdr = usd.EXCHANGE_RATE;
                }

                var jpy = q.Where(a => a.CURRENCY_CD == "JPY").FirstOrDefault();
                if (jpy != null)
                {
                    jpyToIdr = jpy.EXCHANGE_RATE;
                }
            }

            x.MarkCells("[TITLE_REPORT],[DIVISION],[ACCRUED_NO],[UPDATED_DT],[USD_RATE],[JPY_RATE],[PRINTED_BY],[PRINTED_DT],[TOTAL_AMOUNT],[TOTAL_SAP_SPENT],[TOTAL_SAP_REMAINING]");
            x.PutCells(titleReport, divisionName, accrNo, updatedDt, usdToIdr.ToString("G"), jpyToIdr.ToString("G"), printedBy, printedDt, totalAmount.ToString(), totalSpent.ToString(), totalRemaining.ToString());
            //End set value Header

            //Set Value Detail WBS_DESC
            x.MarkRow("[BOOKING_NO],[PV_TYPE],[ACTIVITY_DES],[WBS_NO_OLD],"
                + "[WBS_DESC],[SUSPENSE_NO_OLD],[WBS_NO],[WBS_DESC_PR],[SUSPENSE_NO_PR],[INIT_AMT],"
                + "[SAP_SPENT],[SAP_REMAINING]");

            int j = 0;

            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedListDetail dx in _list)
            {
                //Input Data to list detail
                x.PutRowInsert(dx.BOOKING_NO, dx.PV_TYPE_NAME, dx.ACTIVITY_DES,
                        dx.WBS_NO_OLD, dx.WBS_DESC, dx.SUSPENSE_NO_OLD, dx.WBS_NO,
                        dx.WBS_DESC_PR, dx.SUSPENSE_NO_PR, dx.INIT_AMT, dx.SAP_SPENT, dx.SAP_REMAINING);
                j++;

            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "", "", "",
                         "", "");
            }
            //End set value Detail

            x.PutTail();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            if (String.IsNullOrEmpty(pdfExport))
            {
                return filePath;
            }
            else
            {
                string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                if (rslt)
                {
                    return pdfPath;
                }
                else
                {
                    return filePath;
                }

            }



        }
        #endregion

        #region Upload Accrued Template

        
        #endregion

    }
}