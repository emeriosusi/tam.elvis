﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Function;
using BusinessLogic.CommonLogic;

namespace BusinessLogic._80Accrued
{
    [Serializable]
    public class AccruedResultSearch
    {
        public string AccruedNo, BookingNo, Activity, OldWbsNo, OldWbsDesc, OldSuspenseNo
            , NewWbsNo, NewWbsDesc, NewSuspenseNo;
        public int PVType;
        public decimal AmountAccrFrom, AmountAccrTo, AmountSpentFrom, AmountSpentTo
            , AmountRemainFrom, AmountRemainTo;

        public string sBookingNoPre, sBookingNoPost, sActivityPre, sActivityPost
            , sOldWbsNoPre, sOldWbsNoPost, sOldWbsDescPre, sOldWbsDescPost, sOldSuspenseNoPre, sOldSuspenseNoPost
            , sNewWbsNoPre, sNewWbsNoPost, sNewWbsDescPre, sNewWbsDescPost, sNewSuspenseNoPre, sNewSuspenseNoPost;
        public int iBookingNo, iActivity, iOldWbsNo, iOldWbsDesc, iOldSuspenseNo
            , iNewWbsNo, iNewWbsDesc, iNewSuspenseNo;

        public int caller; //1 : screen, 2 : report
        public void Init()
        {
            AccruedNo = "";
            BookingNo = "";
            Activity = "";
            OldWbsNo = "";
            OldWbsDesc = "";
            OldSuspenseNo = "";
            NewWbsNo = "";
            NewWbsDesc = "";
            NewSuspenseNo = "";

            AmountAccrFrom = -1;
            AmountAccrTo = -1;
            AmountSpentFrom = -1;
            AmountSpentTo = -1;
            AmountRemainFrom = -1;
            AmountRemainTo = -1;

            PVType = -1;
            AmountAccrFrom = 0;
            AmountAccrTo = 0;
            AmountSpentFrom = 0;
            AmountSpentTo = 0;
            AmountRemainFrom = 0;
            AmountRemainTo = 0;
        }

        public AccruedResultSearch()
        {
            Init();
        }

        public AccruedResultSearch(string _accruedNo, string _bookingNo, string _activity, string _oldWbsNo,
            string _oldWbsDesc, string _oldSuspenseNo, string _newWbsNo, string _newWbsDesc, string _newSuspenseNo, string _pvType,
            string _amountAccrFrom, string _amountAccrTo, string _amountSpentFrom, string _amountSpentTo,
            string _amountRemainFrom, string _amountRemainTo, int _caller)
        {
            Init();

            caller = _caller;
            AccruedNo = _accruedNo;

            BookingNo = _bookingNo;
            iBookingNo = LikeLogic.ignoreWhat(_bookingNo, ref sBookingNoPre, ref sBookingNoPost);

            Activity = _activity;
            iActivity = LikeLogic.ignoreWhat(_activity, ref sActivityPre, ref sActivityPost);

            OldWbsNo = _oldWbsNo;
            iOldWbsNo = LikeLogic.ignoreWhat(_oldWbsNo, ref sOldWbsNoPre, ref sOldWbsNoPost);

            OldWbsDesc = _oldWbsDesc;
            iOldWbsDesc = LikeLogic.ignoreWhat(_oldWbsDesc, ref sOldWbsDescPre, ref sOldWbsDescPost);

            OldSuspenseNo = _oldSuspenseNo;
            iOldSuspenseNo = LikeLogic.ignoreWhat(_oldSuspenseNo, ref sOldSuspenseNoPre, ref sOldSuspenseNoPost);

            NewWbsNo = _newWbsNo;
            iNewWbsNo = LikeLogic.ignoreWhat(_newWbsNo, ref sNewWbsNoPre, ref sNewWbsNoPost);

            NewWbsDesc = _newWbsDesc;
            iNewWbsDesc = LikeLogic.ignoreWhat(_newWbsDesc, ref sNewWbsDescPre, ref sNewWbsDescPost);

            NewSuspenseNo = _newSuspenseNo;
            iNewSuspenseNo = LikeLogic.ignoreWhat(_newSuspenseNo, ref sNewSuspenseNoPre, ref sNewSuspenseNoPost);

            PVType = _pvType.Int(-1);
            AmountAccrFrom = _amountAccrFrom.Dec(0);
            AmountAccrTo = _amountAccrTo.Dec(0);
            AmountSpentFrom = _amountSpentFrom.Dec(0);
            AmountSpentTo = _amountSpentTo.Dec(0);
            AmountRemainFrom = _amountRemainFrom.Dec(0);
            AmountRemainTo = _amountRemainTo.Dec(0);
        }
        
    }
}
