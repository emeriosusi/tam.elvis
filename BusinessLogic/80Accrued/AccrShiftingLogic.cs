﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using BusinessLogic.AccruedForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data._80Accrued;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;
using Dapper;
using System.Data.Objects.SqlClient;

namespace BusinessLogic._80Accrued
{
    public class AccrShiftingLogic : LogicBase
    {
        public const byte SAP_PAID_BY_CASHIER = 1;
        public const byte SAP_CHECK_BY_ACCOUNTING = 2;
        private LogicFactory logic = LogicFactory.Get();
        protected LoggingLogic log = new LoggingLogic();
        protected readonly string _INF = "MSTD00001INF";
        protected List<string> errs = new List<string>();

        public IQueryable<AccruedShiftingData> Search(AccrShiftingSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from trash in db.TB_R_ACCR_SHIFTING_H
                     join a in db.TB_R_ACCR_DOC_NO on trash.SHIFTING_NO equals a.ACCR_NO into aa
                     from a in aa.DefaultIfEmpty()
                     join d in db.vw_Division on trash.DIVISION_ID equals d.DIVISION_ID into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.TB_M_STATUS on trash.STATUS_CD equals s.STATUS_CD into ss
                     from s in ss.DefaultIfEmpty()
                     join b in db.vw_CurNextApprover on a.ACCR_DOC_NO equals b.REFF_NO into bb
                     from b in bb.DefaultIfEmpty()
                     select new AccruedShiftingData()
                     {
                         SHIFTING_NO = trash.SHIFTING_NO,
                         REFF_NO = SqlFunctions.StringConvert(a.ACCR_DOC_NO).Trim(),
                         DIVISION_NAME = d.DIVISION_NAME,
                         DIVISION_ID = trash.DIVISION_ID,
                         CREATED_BY = trash.CREATED_BY,
                         CREATED_DT = trash.CREATED_DT,
                         CHANGED_BY = trash.CREATED_BY,
                         CHANGED_DT = trash.CHANGED_DT,
                         SUBMISSION_STATUS = s.STATUS_NAME,
                         STATUS_CD = s.STATUS_CD != null ? s.STATUS_CD : 0,
                         PIC_CURRENT = b.CURRENT_APPROVER,
                         PIC_NEXT = b.NEXT_APPROVER,
                         TOT_AMOUNT = trash.TOTAL_AMT != null ? trash.TOTAL_AMT.Value : 0,
                         MODULE_CD = s.MODULE_CD,
                         BUDGET_YEAR = trash.BUDGET_YEAR != null ? trash.BUDGET_YEAR.Value : 0,
                         WORKFLOW_STATUS = SqlFunctions.StringConvert((decimal)trash.WORKFLOW_STATUS).Trim()
                     });

            if (!crit.ignoreIssuingDivision)
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (!crit.ignoreShiftingNo)
            {
                q = q.Where(p => (p.SHIFTING_NO == crit.shiftingNo));
            }

            if (!crit.ignoreDateFrom)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT >= crit.createdDtFrom);
            }
            if (!crit.ignoreDateTo)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT <= crit.createdDtTo);
            }
            if (crit.budgetYear > 0)
            {
                q = q.Where(p => p.BUDGET_YEAR == crit.budgetYear);
            }
            if (crit.submissionSts > 0)
            {
                q = q.Where(p => p.STATUS_CD == crit.submissionSts);
            }
            if (!crit.ignoreAmt)
            {
                q = q.Where(p => p.TOT_AMOUNT >= crit.totAmtFrom && p.TOT_AMOUNT <= crit.totAmtTo);
            }

            return q;
        }
        private decimal GetAmount(string bookNo)
        {
            decimal result = 0;

            var q = (from trab in db.TB_R_ACCR_BALANCE
                     where trab.BOOKING_NO == bookNo
                     select new
                     {
                         AVAILABLE_AMT = trab.AVAILABLE_AMT,
                         BLOKING_AMT = trab.BLOKING_AMT,
                         OUTSTANDING_AMT = trab.OUTSTANDING_AMT
                     }).FirstOrDefault();

            result = (q.AVAILABLE_AMT ?? 0) - (q.BLOKING_AMT ?? 0) - (q.OUTSTANDING_AMT ?? 0);

            return result;
        }
        public List<AccruedShiftingData> UpdateRemainAmount(List<AccruedShiftingData> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                data[i].REMAINING_DIRECT = GetAmount(data[i].BOOKING_NO_DIRECT);
                data[i].REMAINING_PR = GetAmount(data[i].BOOKING_NO_PR);
            }

            return data;
        }
        public IQueryable<AccruedShiftingData> SearchForm(string shiftingNo)
        {
            db.CommandTimeout = 3600;

            var q = (from trasd in db.TB_R_ACCR_SHIFTING_D
                     join tms in db.TB_M_SYSTEM on new //trasd.SHIFTING_FROM equals tms.SYSTEM_VALUE_NUM
                     {
                         shiftingFrom = trasd.SHIFTING_FROM,
                         systemType = "SHIFTING_TYPE"
                     } equals new
                     {
                         shiftingFrom = tms.SYSTEM_VALUE_NUM,
                         systemType = tms.SYSTEM_TYPE
                     } into tmss
                     from tms in tmss.DefaultIfEmpty()
                     join trash in db.TB_R_ACCR_SHIFTING_H on trasd.SHIFTING_NO equals trash.SHIFTING_NO
                     join trab in db.TB_R_ACCR_BALANCE on trasd.BOOKING_NO_PR equals trab.BOOKING_NO
                     join trab_ in db.TB_R_ACCR_BALANCE on trasd.BOOKING_NO_DIRECT equals trab_.BOOKING_NO
                     where trasd.SHIFTING_NO == shiftingNo
                     select new AccruedShiftingData()
                     {
                         WBS_NO_OLD = trasd.WBS_NO_OLD,
                         WBS_DESC_OLD = (from vWbs in db.vw_WBS 
                                         where vWbs.WbsNumber == trasd.WBS_NO_OLD 
                                         select vWbs.Description).FirstOrDefault(),
                         SHIFTING_NO = trasd.SHIFTING_NO,
                         WBS_NO_PR = trasd.WBS_NO_PR,
                         WBS_DESC_PR = (from vWbs in db.vw_WBS
                                        where vWbs.WbsNumber == trasd.WBS_NO_PR
                                        select vWbs.Description).FirstOrDefault(),
                         REMAINING_PR = trash.STATUS_CD == 403 ? (decimal?)null :   trab.AVAILABLE_AMT != null ? 
                                                                                    trab.AVAILABLE_AMT.Value - (trab.BLOKING_AMT ?? 0) - (trab.OUTSTANDING_AMT ?? 0) : 0,
                         BOOKING_NO = "",
                         BOOKING_NO_PR = trasd.BOOKING_NO_PR,
                         BOOKING_NO_DIRECT = trasd.BOOKING_NO_DIRECT,
                         REMAINING_DIRECT = trash.STATUS_CD == 403 ? (decimal?)null :   trab_.AVAILABLE_AMT != null ? 
                                                                                        trab_.AVAILABLE_AMT.Value - 
                                                                                        (trab_.BLOKING_AMT ?? 0) - 
                                                                                        (trab_.OUTSTANDING_AMT ?? 0) : 0,
                         SHIFTING_AMT = trasd.SHIFTING_AMT != null ? trasd.SHIFTING_AMT.Value : 0,
                         SHIFTING_FROM = trasd.SHIFTING_FROM != null ? trasd.SHIFTING_FROM.Value : 0,
                         PV_TYPE_CD = tms.SYSTEM_VALUE_NUM,
                         PV_TYPE_NAME = tms.SYSTEM_VALUE_TXT,
                         STATUS_CD = trash.STATUS_CD != null ? trash.STATUS_CD.Value : 0,
                         SAP_DOC_NO = trasd.SAP_DOC_NO
                     }).Distinct();

            return q;
        }

        public IQueryable<AccruedShiftingData> GetWbsNoOldDetail(string wbsNoOld)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join vwOld in db.vw_WBS on trab.WBS_NO_OLD equals vwOld.WbsNumber into a
                     from vwOld in a.DefaultIfEmpty()
                     join vwPr in db.vw_WBS on trab.WBS_NO_PR equals vwPr.WbsNumber into b
                     from vwPr in b.DefaultIfEmpty()
                     join trasd in db.TB_R_ACCR_SHIFTING_D on trab.WBS_NO_OLD equals trasd.WBS_NO_OLD into c
                     from trasd in c.DefaultIfEmpty()
                     select new AccruedShiftingData()
                     {
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC_OLD = vwOld.Description,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         WBS_DESC_PR = vwPr.Description,
                         BOOKING_NO_PR = trasd.BOOKING_NO_PR,
                         REMAINING_PR = 0,
                         BOOKING_NO_DIRECT = trasd.BOOKING_NO_DIRECT,
                         REMAINING_DIRECT = 0
                     });

            if (!string.IsNullOrEmpty(wbsNoOld))
            {
                q = q.Where(p => p.WBS_NO_OLD == wbsNoOld);
            }

            return q;
        }

        //add by FID.Arri on 8 May 2018 for AccrShiftingReport
        public IQueryable<AccruedShiftingData> ReportListData(AccrShiftingSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from traed in db.TB_R_ACCR_EXTEND_D
                     join trab in db.TB_R_ACCR_BALANCE on traed.BOOKING_NO equals trab.BOOKING_NO into a
                     from trab in a.DefaultIfEmpty()
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trab.PV_TYPE_CD equals tmapt.PV_TYPE_CD into b
                     from tmapt in b.DefaultIfEmpty()
                     join ttsw in db.vw_WBS on trab.WBS_NO_OLD equals ttsw.WbsNumber into c
                     from ttsw in c.DefaultIfEmpty()
                     join ttsw1 in db.vw_WBS on trab.WBS_NO_PR equals ttsw1.WbsNumber into x
                     from ttsw1 in x.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO into e
                     from trald in e.DefaultIfEmpty()
                     join traeh in db.TB_R_ACCR_EXTEND_H on traed.EXTEND_NO equals traeh.EXTEND_NO into f
                     from traeh in f.DefaultIfEmpty()
                     join d in db.vw_Division on traeh.DIVISION_ID equals d.DIVISION_ID into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.TB_M_STATUS on traeh.STATUS_CD equals s.STATUS_CD into ss
                     from s in ss.DefaultIfEmpty()
                     select new AccruedShiftingData()
                     {
                         BOOKING_NO = traed.BOOKING_NO,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC_OLD = ttsw.Description,
                         SUSPENSE_NO_OLD = trald.SUSPENSE_NO_OLD,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         WBS_DESC_PR = ttsw1.Description,
                         SUSPENSE_NO_PR = trab.SUSPENSE_NO_NEW,
                         BLOCKING_AMT = trab.BLOKING_AMT ?? 0,
                         AVAILABLE_AMT = trab.AVAILABLE_AMT ?? 0,
                         EXTEND_TYPE = traed.EXTEND_TYPE.HasValue ? (traed.EXTEND_TYPE.Value == 1 ? "EXTEND" : "RECLAIM") : "",
                         EXTEND_AMT = traed.EXTEND_AMT ?? 0,
                         DIVISION_ID = traeh.DIVISION_ID
                     }).Distinct();

            if (!crit.ignoreIssuingDivision)
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (!crit.ignoreShiftingNo)
            {
                switch (crit.iAno)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.EXTEND_NO.EndsWith(crit.sExtPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.EXTEND_NO.StartsWith(crit.sExtPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.EXTEND_NO.Contains(crit.sExtPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.EXTEND_NO.StartsWith(crit.sExtPre) && p.EXTEND_NO.EndsWith(crit.sExtPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.EXTEND_NO.Contains(crit.shiftingNo));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!crit.ignoreDateFrom)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT >= crit.createdDtFrom);
            }

            if (!crit.ignoreDateTo)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT <= crit.createdDtTo);
            }

            return q;
        }

        public List<AccruedShiftingData> GetActivityAccruedShifting(Dictionary<string, object> param)
        {
            string bookingNo = "";
            if (param.ContainsKey("BOOKING_NO"))
                bookingNo = param["BOOKING_NO"].ToString();


            var q = (from d in db.TB_R_ACCR_EXTEND_ACT
                     where d.BOOKING_NO == bookingNo
                     select new AccruedShiftingData
                     {
                         EXTEND_NO = d.EXTEND_NO,
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            //if (param.ContainsKey("BOOKING_NO"))
            //{
            //    string bookingNo = param["BOOKING_NO"].ToString();
            //    q = q.Where(p => p.BOOKING_NO == bookingNo);
            //}

            if (q.Any()) return q.ToList();

            return null;
        }

        public bool isDraft(string shiftingNo)
        {
            var q = (from h in db.TB_R_ACCR_SHIFTING_H
                     where h.SHIFTING_NO == shiftingNo
                    select h).FirstOrDefault();

            return q != null && q.STATUS_CD == 0;
        }

        public bool Delete(string shiftingNo, string bookingNo)
        {
            if (String.IsNullOrEmpty(shiftingNo)) return false;
            if (String.IsNullOrEmpty(bookingNo)) return false;

            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    var det = (from d in DB.TB_R_ACCR_SHIFTING_D
                               where d.SHIFTING_NO == shiftingNo
                              select d).ToList();
                    if (det.Any())
                    {
                        foreach (var d in det)
                        {
                            DB.DeleteObject(d);
                            DB.SaveChanges();
                        }
                    }

                    _result = true;
                    tx.Commit();
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public bool UpdateDetail(string shiftingNo, List<AccruedShiftingData> item, UserData userData, Common.Enum.PageMode pageMode)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    var qHeader = (from trash in db.TB_R_ACCR_SHIFTING_H
                                   where trash.SHIFTING_NO == shiftingNo
                                   select trash).FirstOrDefault();
                    if (qHeader == null)
                    {
                        TB_R_ACCR_SHIFTING_H h = new TB_R_ACCR_SHIFTING_H();
                        h.SHIFTING_NO = shiftingNo;
                        h.DIVISION_ID = userData.DIV_CD.ToString();
                        h.TOTAL_AMT = 0;
                        h.STATUS_CD = 0;
                        h.CREATED_BY = userData.USERNAME;
                        h.CREATED_DT = DateTime.Now;
                        h.BUDGET_YEAR = DateTime.Now.Year - 1;
                        db.TB_R_ACCR_SHIFTING_H.AddObject(h);
                        db.SaveChanges();

                        qHeader = (from trash in db.TB_R_ACCR_SHIFTING_H
                                   where trash.SHIFTING_NO == shiftingNo
                                   select trash).FirstOrDefault();
                    }
                    
                    logic.AccrForm.InsertAccrReffNo(shiftingNo, 4, db);

                    var qDelD = (from trasd in db.TB_R_ACCR_SHIFTING_D
                                where trasd.SHIFTING_NO == shiftingNo
                                select trasd);
                    if (qDelD != null)
                    {
                        foreach (var delParam in qDelD)
                        {
                            db.TB_R_ACCR_SHIFTING_D.DeleteObject(delParam);
                        }
                        db.SaveChanges();
                    }

                    decimal total = 0;
                    foreach (var data in item)
                    {
                        TB_R_ACCR_SHIFTING_D qInsert = new TB_R_ACCR_SHIFTING_D();
                        qInsert.SHIFTING_NO = shiftingNo;
                        qInsert.WBS_NO_OLD = data.WBS_NO_OLD != "" ? data.WBS_NO_OLD : null;
                        qInsert.WBS_NO_PR = data.WBS_NO_PR != "" ? data.WBS_NO_PR : null;
                        qInsert.BOOKING_NO_PR = data.BOOKING_NO_PR != null ? data.BOOKING_NO_PR : "";
                        qInsert.BOOKING_NO_DIRECT = data.BOOKING_NO_DIRECT != null ? data.BOOKING_NO_DIRECT : "";
                        qInsert.SHIFTING_AMT = data.SHIFTING_AMT;
                        qInsert.SHIFTING_FROM = data.SHIFTING_FROM;
                        qInsert.CREATED_BY = qHeader.CREATED_BY;
                        qInsert.CREATED_DT = qHeader.CREATED_DT;
                        qInsert.CHANGED_DT = DateTime.Now;
                        qInsert.CHANGED_BY = userData.USERNAME;
                        db.TB_R_ACCR_SHIFTING_D.AddObject(qInsert);
                        db.SaveChanges();
                        total = total + (qInsert.SHIFTING_AMT != null ? qInsert.SHIFTING_AMT.Value : 0);
                    }

                    qHeader.CHANGED_BY = userData.USERNAME;
                    qHeader.CHANGED_DT = DateTime.Now;
                    qHeader.TOTAL_AMT = total;
                    db.SaveChanges();

                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;

        }
        public bool InsertSAP(List<ShiftingBudget> data, string reffNo, string shiftingNo, ref List<string> errMsg)
        {
            DbTransaction TX = null;
            bool result = true;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    int reffNoInt = reffNo.Int();

                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    var maxItemNo = (from trasdn in db.TB_R_SAP_DOC_NO
                                     where trasdn.DOC_NO == reffNoInt
                                     orderby trasdn.ITEM_NO descending
                                     select trasdn.ITEM_NO).FirstOrDefault();

                    foreach (var d in data)
                    {
                        if (d.DOC_CHANGE_NO.isNotEmpty() && d.DOC_CHANGE_NO != "NOTFOUND" && d.DOC_CHANGE_NO != "OPNBYUSER")
                        {
                            maxItemNo++;
                            var insrt = new TB_R_SAP_DOC_NO();
                            insrt.DOC_NO = reffNo.Int();
                            insrt.DOC_YEAR = DateTime.Now.Year;
                            insrt.ITEM_NO = maxItemNo;
                            insrt.CURRENCY_CD = null;
                            insrt.INVOICE_NO = null;
                            insrt.SAP_DOC_NO = d.DOC_CHANGE_NO;
                            insrt.SAP_DOC_YEAR = DateTime.Now.Year;
                            insrt.SAP_CLEARING_DOC_NO = null;
                            insrt.SAP_CLEARING_DOC_YEAR = null;
                            insrt.PAYPROP_DOC_NO = null;
                            insrt.PAYPROP_ID = null;
                            db.TB_R_SAP_DOC_NO.AddObject(insrt);
                            db.SaveChanges();

                            var listD = (from trasd in db.TB_R_ACCR_SHIFTING_D
                                        where trasd.SHIFTING_NO == shiftingNo
                                        select trasd);

                            // 20180725 | sap return budget no without stripe
                            var updt = listD.Where(x => x.WBS_NO_PR == d.BUDGET_NO).FirstOrDefault();

                            updt.SAP_DOC_NO = d.DOC_CHANGE_NO;
                            updt.SAP_DOC_YEAR = DateTime.Now.Year;
                            db.SaveChanges();
                        }
                        else
                        {
                            if (d.DOC_CHANGE_NO == "NOTFOUND")
                                errMsg.Add(string.Format("WBS No. {0} is not found.", d.BUDGET_NO));
                            else if (d.DOC_CHANGE_NO == "OPNBYUSER")
                                errMsg.Add(string.Format("WBS no. {0} is being opened by another user.", d.BUDGET_NO));
                            else
                                errMsg.Add(string.Format("WBS no. {0} is failed to Post to SAP.", d.BUDGET_NO));

                            result = false;
                        }
                    }

                    TX.Commit();
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    logic.Say("BusinessLogic.80Accrued.InsertSAP", "SAP returns null.");
                    errMsg.Add("SAP returns null.");
                    result = false;
                }
            }

            return result;
        }

        public bool UpdateBalance(string shiftingNo, List<AccruedShiftingData> detail, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    for (int i = 0; i < detail.Count; i++)
                    {
                        string bookNo = "";

                        bookNo = detail[i].BOOKING_NO_PR;
                        var balPR = (from trab in db.TB_R_ACCR_BALANCE
                                     where trab.BOOKING_NO == bookNo
                                     select trab).FirstOrDefault();
                        balPR.CHANGED_BY = userData.USERNAME;
                        balPR.CHANGED_DT = DateTime.Now;

                        bookNo = detail[i].BOOKING_NO_DIRECT;
                        var balDirect = (from trab in db.TB_R_ACCR_BALANCE
                                         where trab.BOOKING_NO == bookNo
                                         select trab).FirstOrDefault();
                        balDirect.CHANGED_BY = userData.USERNAME;
                        balDirect.CHANGED_DT = DateTime.Now;

                        switch (detail[i].SHIFTING_FROM)
                        {
                            case 1: //direct
                                balDirect.AVAILABLE_AMT = balDirect.AVAILABLE_AMT - detail[i].SHIFTING_AMT;
                                balDirect.BUDGET_AMT = balDirect.BUDGET_AMT - detail[i].SHIFTING_AMT;
                                balPR.AVAILABLE_AMT = balPR.AVAILABLE_AMT + detail[i].SHIFTING_AMT;
                                balPR.BUDGET_AMT = balPR.BUDGET_AMT + detail[i].SHIFTING_AMT;
                                db.SaveChanges();
                                break;
                            case 4: //pr
                                balDirect.AVAILABLE_AMT = balDirect.AVAILABLE_AMT + detail[i].SHIFTING_AMT;
                                balDirect.BUDGET_AMT = balDirect.BUDGET_AMT + detail[i].SHIFTING_AMT;
                                balPR.AVAILABLE_AMT = balPR.AVAILABLE_AMT - detail[i].SHIFTING_AMT;
                                balPR.BUDGET_AMT = balPR.BUDGET_AMT - detail[i].SHIFTING_AMT;
                                db.SaveChanges();
                                break;
                            default:
                                break;
                        }
                    }

                    //header.STATUS_CD = 403;
                    //var header = (from trash in db.TB_R_ACCR_SHIFTING_H
                    //              where trash.SHIFTING_NO == shiftingNo
                    //              select trash).FirstOrDefault();
                    //header.STATUS_CD = 403;
                    //header.CHANGED_BY = userData.USERNAME;
                    //header.CHANGED_DT = DateTime.Now;
                    //db.SaveChanges();

                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;

        }

        public bool InsertDetail(string shiftingNo, AccruedShiftingData item, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    var set_d = (from i in db.TB_R_ACCR_EXTEND_D
                                 where i.BOOKING_NO == item.BOOKING_NO && i.EXTEND_NO == item.EXTEND_NO 
                                 select i).FirstOrDefault();
                    if (set_d == null)
                    {
                        TB_R_ACCR_EXTEND_D detail = new TB_R_ACCR_EXTEND_D()
                        {
                            EXTEND_NO = item.EXTEND_NO,
                            BOOKING_NO = item.BOOKING_NO,
                            EXTEND_AMT = item.EXTEND_AMT,
                            CREATED_DT = DateTime.Now,
                            CREATED_BY = userData.USERNAME
                        };
                        db.AddToTB_R_ACCR_EXTEND_D(detail);
                        db.SaveChanges();
                    }
                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;

        }


        private bool ValidateActualOnTime(DateTime? planDate, DateTime? actualDate)
        {
            bool isValid = true;
            if (planDate != null)
            {
                if (actualDate == null)
                {
                    DateTime now = DateTime.Now;
                    if (now.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
                else
                {
                    if (actualDate.Value.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
            }
            return isValid;
        }
        public List<AccruedListDetail> GetActivityList(string bookingNo)
        {

            var q = (from d in db.TB_R_ACCR_LIST_D
                     where d.BOOKING_NO == bookingNo 
                     select new AccruedListDetail
                     {
                         ACCRUED_NO = d.ACCRUED_NO,
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            if (q.Any()) return q.OrderBy(x => x.ACTIVITY_DES).ToList();

            return null;
        }

        public List<AccruedListDetail> GetCurrentActivity(string extendNo, string bookingNo)
        {

            var q = (from d in db.TB_R_ACCR_EXTEND_ACT 
                     where d.BOOKING_NO == bookingNo && d.EXTEND_NO == extendNo 
                     select new AccruedListDetail
                     {
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            if (q.Any()) return q.ToList();

            return new List<AccruedListDetail>();
        }

        public AccruedShiftingData GetCurHeader(string extendNo)
        {

            var q = (from d in db.TB_R_ACCR_EXTEND_H
                     where d.EXTEND_NO == extendNo
                     select new AccruedShiftingData
                     {
                         EXTEND_NO = d.EXTEND_NO,
                         DIVISION_ID = d.DIVISION_ID,
                         EXTEND_AMT = d.TOTAL_AMT,
                         STATUS_CD = d.STATUS_CD.HasValue ? d.STATUS_CD.Value : 0,
                         BUDGET_YEAR = d.BUDGET_YEAR.HasValue ? d.BUDGET_YEAR.Value : 0
                     });

            if (q.Any()) return q.FirstOrDefault();

            return new AccruedShiftingData();
        }

        public bool HasSelectAll(string userName)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                int i = (from p in db.vw_User_Role_Header
                         where p.USERNAME == userName
                           && (p.ROLE_ID == "ELVIS_ACC" || p.ROLE_ID == "ELVIS_FD_STAFF")
                         select p.USERNAME).Count();
                return i > 0;
            }
        }

        #region Get String Builder

        private String DateTimeToString(DateTime? dateTimeParam, String format)
        {
            if (dateTimeParam != null)
            {
                DateTime dateTimeParam1 = (DateTime)dateTimeParam;
                return dateTimeParam1.ToString(format);
            }
            else
            {
                return null;
            }
        }

        public byte[] Get(
            string _username,
            string _imagePath,
            AccrShiftingSearchCriteria c)
        {
            ExcelWriter x = new ExcelWriter();

            var qq = Search(c);

            int sizeForAll = qq.Count();

            string header1 = "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                            "Downloaded Date : " + DateTime.Now.date() + ExcelWriter.COL_SEP +
                            "Total Record Download : " + sizeForAll;
            string header2 = "No|Work Flow Status|Accrued No.|Issuing Division|" +
                            "Created Date|Updated Date|" +
                            "Submission Status|PIC{Current|Next}|Total Amount";
            x.PutPage("General Info");
            x.PutHeader("Accrued Extend List General Info", header1, header2, _imagePath);
            int rownum = 0;
            int pageSize = ExcelWriter.xlRowLimit;
            // the first eleven rows is used as header
            pageSize = pageSize - 11;
            int isNewPage = 1;
            int counter = 1;
            List<AccruedShiftingData> _list =
                qq.ToList();
            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedShiftingData dx in _list)
            {
                if (counter >= pageSize)
                {
                    counter = 1;
                    x.PutTail();
                    x.PutPage("General Info " + ++isNewPage);
                    x.PutHeader("Accrued Extend List General Info", header1, header2, _imagePath);
                }

                x.Put(
                    ++rownum,
                    dx.WORKFLOW_STATUS, dx.EXTEND_NO,
                    dx.DIVISION_NAME, dx.CREATED_DT.date(),
                    dx.CHANGED_DT.date(), dx.SUBMISSION_STATUS,
                    dx.PIC_CURRENT, dx.PIC_NEXT, dx.TOT_AMOUNT);
                counter++;
            }


            x.PutTail();



            return x.GetBytes();
        }
        #endregion

        public string GenerateShiftingFormReport(
            string _username,
            string templateFile,
            string newFile,
            string shiftingNo,
            string issuingDiv,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "ACCR";

            x.MarkPage(_sheetName);

            AccruedShiftingData dataH = GetCurHeader(shiftingNo);

            //Get data from search criteria
            var qq = SearchForm(shiftingNo);
            List<AccruedShiftingData> _list =
                qq.ToList();

            //Set value Header
            string budgetYear = (dataH.BUDGET_YEAR == null) ? DateTime.Now.ToString("yyyy") : dataH.BUDGET_YEAR.ToString();
            string titleReport = "LIST ACCRUED EXTEND TAHUN " + budgetYear;

            string divisionName = String.IsNullOrEmpty(issuingDiv) ? "All" : issuingDiv;
            string accruedExtNo = String.IsNullOrEmpty(shiftingNo) ? "All" : shiftingNo;
            string updatedDt = "";

            string printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            string printedDt = DateTime.Now.ToString("dd-MM-yyyy");

            decimal? totalReclaimable = 0;
            decimal? totalExtendable = 0;
            decimal? totalAmount = 0;

            foreach (AccruedShiftingData hAccr in _list)
            {
                updatedDt = hAccr.CHANGED_DT.HasValue
                             ? hAccr.CHANGED_DT.Value.ToString("dd-MM-yyyy")
                             : "";
                totalReclaimable = totalReclaimable + hAccr.RECLAIMABLE_AMT;
                totalExtendable = totalExtendable + hAccr.EXTENDABLE_AMT;
                totalAmount = totalAmount + (hAccr.EXTEND_AMT ?? 0);
            }

            x.MarkCells("[TITLE_REPORT],[DIVISION],[ACCRUED_EXT_NO],[UPDATED_DT],[PRINTED_BY],[PRINTED_DT],[TOTAL_RECLAIMABLE],[TOTAL_EXTENDABLE],[TOTAL_AMOUNT]");
            x.PutCells(titleReport, divisionName, accruedExtNo, updatedDt, printedBy, printedDt, totalReclaimable.ToString(), totalExtendable.ToString(), totalAmount.ToString());
            //End set value Header

            //Set Value Detail
            x.MarkRow("[BOOKING_NO],[PV_TYPE],[ACTIVITY],[WBS_NO],[WBS_DESC],[SUSPENSE_NO],"
                + "[WBS_PR_NO],[WBS_PR_DESC],[SUSPENSE_PR_NO],[AMOUNT_RECLAIMABLE],"
                + "[AMOUNT_EXTENDABLE],[ACT_TYPE],[ACT_AMOUNT]");

            int j = 0;

            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedShiftingData dx in _list)
            {
                //Get the Activities
                var param = new Dictionary<string, object>();
                param.Add("BOOKING_NO", dx.BOOKING_NO);

                List<AccruedShiftingData> _Activities = GetActivityAccruedShifting(param);
                var act = (from a in _Activities
                           select a.ACTIVITY_DES
                            ).ToList();
                //Input Data to list detail
                string suspenseNo = dx.SUSPENSE_NO_OLD == null ? "" : dx.SUSPENSE_NO_OLD.ToString();
                x.PutRowInsert(dx.BOOKING_NO, dx.PV_TYPE_NAME, String.Join(",", act), dx.WBS_NO_OLD,
                        dx.WBS_DESC_OLD, suspenseNo, dx.WBS_NO_PR, dx.WBS_DESC_PR,
                        dx.SUSPENSE_NO_PR.str(), dx.RECLAIMABLE_AMT, dx.EXTENDABLE_AMT, dx.EXTEND_TYPE
                        , dx.EXTEND_AMT);

                j++;
            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "", "", "",
                         "", "", "");
            }
            //End set value Detail

            x.PutTail();

            //return x.GetBytes();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            //return filePath;
            if (String.IsNullOrEmpty(pdfExport))
            {
                return filePath;
            }
            else
            {
                string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                if (rslt)
                {
                    return pdfPath;
                }
                else
                {
                    return filePath;
                }

            }
        }

        public List<AccruedShiftingData> GetDataWbsNoOld(UserData userData)
        {
            db.CommandTimeout = 3600;

            string budgetYear = (DateTime.Now.Year - 1).str();

            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join trab_ in db.TB_R_ACCR_BALANCE on trab.WBS_NO_OLD equals trab_.WBS_NO_OLD
                     join vOld in (from a in db.vw_WBS 
                                   select new vWBSOld()
                                   {
                                       WbsNumber = a.WbsNumber,
                                       Description = a.Description
                                   }).Distinct() on trab.WBS_NO_OLD equals vOld.WbsNumber
                     join vPr in (from a in db.vw_WBS
                                  select new vWBSPr()
                                  {
                                      WbsNumber = a.WbsNumber,
                                      Description = a.Description
                                  }).Distinct() on trab.WBS_NO_OLD equals vPr.WbsNumber
                     where trab.PV_TYPE_CD == 4 && trab_.PV_TYPE_CD == 1 && trab.WBS_NO_PR != null && trab.BOOKING_NO.Contains(budgetYear) && trab_.BOOKING_NO.Contains(budgetYear)
                     select new AccruedShiftingData()
                     {
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC_OLD = vOld.Description,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         WBS_DESC_PR = vPr.Description,
                         DIVISION_ID = trab.DIVISION_ID,
                         BOOKING_NO_PR = trab.BOOKING_NO,
                         REMAINING_PR = trab.AVAILABLE_AMT != null ? trab.AVAILABLE_AMT.Value - (trab.BLOKING_AMT ?? 0) - (trab.OUTSTANDING_AMT ?? 0) : 0,
                         BOOKING_NO_DIRECT = trab_.BOOKING_NO,
                         REMAINING_DIRECT = trab_.AVAILABLE_AMT != null ? trab_.AVAILABLE_AMT.Value - (trab_.BLOKING_AMT ?? 0) - (trab_.OUTSTANDING_AMT ?? 0) : 0
                     }).Distinct();

            if (userData.Divisions.Count > 0)
            {
                string qDiv = "";
                foreach (string div in userData.Divisions)
                {
                    qDiv = qDiv + div + ";";
                }

                q = q.Where(p => qDiv.Contains(p.DIVISION_ID));
            }

            List<AccruedShiftingData> data = new List<AccruedShiftingData>();
            int i = 0;
            foreach (var p in q)
            {
                p.KEYS = i++.ToString();
                data.Add(p);
            }

            return data;
        }

        public List<AccruedShiftingData> GetDataWbsNoOldBAK(UserData userData)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join vw in db.vw_WBS on trab.WBS_NO_OLD equals vw.WbsNumber into a
                     from vw in a.DefaultIfEmpty()
                     join wv in db.vw_WBS on trab.WBS_NO_PR equals wv.WbsNumber into b
                     from wv in b.DefaultIfEmpty()
                     where (trab.PV_TYPE_CD == 1 || trab.PV_TYPE_CD == 4)
                     select new AccruedShiftingData()
                     {
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC_OLD = vw.Description,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         WBS_DESC_PR = wv.Description,
                         DIVISION_ID = trab.DIVISION_ID,
                         BOOKING_NO_PR = trab.BOOKING_NO,
                         REMAINING_PR = 0,
                         BOOKING_NO_DIRECT = trab.BOOKING_NO,
                         REMAINING_DIRECT = 0
                     }).Distinct();

            if (userData.Divisions.Count > 0)
            {
                string qDiv = "";
                foreach (string div in userData.Divisions)
                {
                    qDiv = qDiv + div + ";";
                }

                q = q.Where(p => qDiv.Contains(p.DIVISION_ID));
            }

            List<AccruedShiftingData> data = new List<AccruedShiftingData>();
            int i = 0;
            foreach (var p in q)
            {
                p.KEYS = i++.ToString();
                data.Add(p);
            }

            return data;
        }

        public IQueryable<AccruedShiftingData> getShiftingType()
        {
            db.CommandTimeout = 3600;
            var q = (from tmapt in db.TB_M_SYSTEM
                     where tmapt.SYSTEM_TYPE == "SHIFTING_TYPE"
                     select new AccruedShiftingData()
                     {
                         Code = tmapt.SYSTEM_VALUE_NUM ?? 0,
                         Description = tmapt.SYSTEM_VALUE_TXT
                     });

            return q;
        }

        public IQueryable<AccruedShiftingData> GetListDivAccrShifting(string AccrShiftingDiv)
        {
            db.CommandTimeout = 3600;
            var q = (from trash in db.TB_R_ACCR_SHIFTING_H
                     select new AccruedShiftingData()
                     {
                         SHIFTING_NO = trash.SHIFTING_NO
                     });

            q = q.Where(p => p.SHIFTING_NO.Contains(AccrShiftingDiv));
            return q;
        }

        public bool IsExist(string shiftNo)
        {
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    var set_h = (from i in db.TB_R_ACCR_SHIFTING_H
                                 where i.SHIFTING_NO == shiftNo
                                 select i);
                    if (set_h.Any())
                    {
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    result = false;
                }
            }

            return result;

        }

        public int GetLatestStatus(string shiftingNo)
        {
            return (from h in db.TB_R_ACCR_SHIFTING_H
                    where h.SHIFTING_NO == shiftingNo
                    select h.STATUS_CD).FirstOrDefault() ?? 0;
        }
    }
}