﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using BusinessLogic.AccruedForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Data._80Accrued;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;
using Dapper;
using System.Data.Objects.SqlClient;

namespace BusinessLogic._80Accrued
{
    public class AccrExtendLogic : LogicBase
    {
        public const byte SAP_PAID_BY_CASHIER = 1;
        public const byte SAP_CHECK_BY_ACCOUNTING = 2;
        private LogicFactory logic = LogicFactory.Get();
        protected LoggingLogic log = new LoggingLogic();
        protected readonly string _INF = "MSTD00001INF";
        protected List<string> errs = new List<string>();

        public IQueryable<AccruedExtendData> Search(AccrExtendSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from traeh in db.TB_R_ACCR_EXTEND_H
                     join a in db.TB_R_ACCR_DOC_NO on traeh.EXTEND_NO equals a.ACCR_NO
                     join d in db.vw_Division on traeh.DIVISION_ID equals d.DIVISION_ID into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.TB_M_STATUS on traeh.STATUS_CD equals s.STATUS_CD into ss
                     from s in ss.DefaultIfEmpty()
                     join c in db.vw_CurNextApprover on a.ACCR_DOC_NO equals c.REFF_NO into ac
                     from c in ac.DefaultIfEmpty()

                     select new AccruedExtendData()
                     {
                         EXTEND_NO = traeh.EXTEND_NO
                         , REFF_NO = SqlFunctions.StringConvert(a.ACCR_DOC_NO).Trim()
                         , DIVISION_NAME = d.DIVISION_NAME
                         , DIVISION_ID = traeh.DIVISION_ID
                         , CREATED_BY = traeh.CREATED_BY
                         , CREATED_DT = traeh.CREATED_DT
                         , CHANGED_BY = traeh.CREATED_BY
                         , CHANGED_DT = traeh.CHANGED_DT
                         , SUBMISSION_STATUS = s.STATUS_NAME
                         , STATUS_CD = s.STATUS_CD != null ? s.STATUS_CD : 0
                         , PIC_CURRENT = c.CURRENT_APPROVER
                         , PIC_NEXT = c.NEXT_APPROVER
                         , TOT_AMOUNT = traeh.TOTAL_AMT != null ? traeh.TOTAL_AMT.Value : 0
                         , MODULE_CD = s.MODULE_CD
                         , BUDGET_YEAR = traeh.BUDGET_YEAR != null ? traeh.BUDGET_YEAR.Value : 0
                         , WORKFLOW_STATUS = traeh.WORKFLOW_STATUS 
                     }).Distinct();


            if (!crit.ignoreIssuingDivision)
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (!crit.ignoreExtendNo)
            {
                switch (crit.iAno)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.EXTEND_NO.EndsWith(crit.sExtPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.EXTEND_NO.StartsWith(crit.sExtPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.EXTEND_NO.Contains(crit.sExtPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.EXTEND_NO.StartsWith(crit.sExtPre) && p.EXTEND_NO.EndsWith(crit.sExtPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.EXTEND_NO.Contains(crit.extendNo));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!crit.ignoreDateFrom)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT >= crit.createdDtFrom);
            }
            if (!crit.ignoreDateTo)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT <= crit.createdDtTo);
            }
            if (crit.budgetYear > 0)
            {
                q = q.Where(p => p.BUDGET_YEAR == crit.budgetYear);
            }
            if (crit.submissionSts != -1)
            {
                q = q.Where(p => p.STATUS_CD == crit.submissionSts);
            }
            if (crit.workflowSts != -1)
            {
                q = q.Where(p => p.WORKFLOW_STATUS == crit.workflowSts);
            }
            if (!crit.ignoreAmt)
            {
                q = q.Where(p => p.TOT_AMOUNT >= crit.totAmtFrom && p.TOT_AMOUNT <= crit.totAmtTo);
            }

            return q;
        }

        public IQueryable<AccruedExtendData> SearchForm(string ExtendNo)
        {
            db.CommandTimeout = 3600;
            var q = (from vab in db.vw_AccrExtend 
                     select new AccruedExtendData()
                     {
                         EXTEND_NO = vab.EXTEND_NO,
                         BOOKING_NO = vab.BOOKING_NO,
                         BOOKING_NO_OLD = vab.BOOKING_NO,
                         PV_TYPE_NAME = vab.PV_TYPE_NAME,
                         WBS_NO_OLD = vab.WBS_NO_OLD,
                         WBS_DESC_OLD = vab.WBS_DESC_OLD,
                         SUSPENSE_NO_OLD = vab.SUSPENSE_NO_OLD,
                         WBS_NO_PR = vab.WBS_NO_PR,
                         WBS_DESC_PR = vab.WBS_DESC_PR,
                         SUSPENSE_NO_PR = vab.SUSPENSE_NO_PR,
                         RECLAIMABLE_AMT = vab.RECLAIMABLE_AMT,
                         EXTENDABLE_AMT = vab.EXTENDABLE_AMT,
                         EXTEND_TYPE = vab.EXTEND_TYPE,
                         EXTEND_TYPE_CD = vab.EXTEND_TYPE_CD.HasValue ? vab.EXTEND_TYPE_CD.Value : 0,
                         EXTEND_AMT = vab.EXTEND_AMT,
                         CREATED_BY = vab.CREATED_BY,
                         CREATED_DT = vab.CREATED_DT,
                         CHANGED_BY = vab.CHANGED_BY,
                         CHANGED_DT = vab.CHANGED_DT
                     });

            
            if (!string.IsNullOrEmpty(ExtendNo))
            {
                q = q.Where(p => p.EXTEND_NO == ExtendNo);
            }
            

            return q;
        }

        public IQueryable<AccruedExtendData> GetBookingDetail(string bookingNo)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE 
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trab.PV_TYPE_CD equals tmapt.PV_TYPE_CD into a
                     from tmapt in a.DefaultIfEmpty()
                     join vw in db.vw_WBS on trab.WBS_NO_OLD equals vw.WbsNumber into b
                     from vw in b.DefaultIfEmpty()
                     join vw2 in db.vw_WBS on trab.WBS_NO_PR equals vw2.WbsNumber into c
                     from vw2 in c.DefaultIfEmpty()
                     select new AccruedExtendData()
                     {
                         BOOKING_NO = trab.BOOKING_NO,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC_OLD = vw.Description,
                         SUSPENSE_NO_OLD = trab.SUSPENSE_NO_OLD,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         WBS_DESC_PR = vw2.Description,
                         SUSPENSE_NO_PR = trab.SUSPENSE_NO_NEW,
                         EXTENDABLE_AMT = trab.AVAILABLE_AMT - trab.BLOKING_AMT,
                         RECLAIMABLE_AMT = trab.BLOKING_AMT
                     });

            if (!string.IsNullOrEmpty(bookingNo))
            {
                q = q.Where(p => p.BOOKING_NO == bookingNo);
            }

            return q;
        }

        public IQueryable<AccruedExtendData> GetDataBookingNo(string divId)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.TB_R_ACCR_BALANCE
                     join vw in db.vw_WBS on trab.WBS_NO_OLD equals vw.WbsNumber into a
                     from vw in a.DefaultIfEmpty()
                     join tmpt in db.TB_M_ACCR_PV_TYPE on trab.PV_TYPE_CD equals tmpt.PV_TYPE_CD into b
                     from tmpt in b.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO
                     join tralh in db.TB_R_ACCR_LIST_H on trald.ACCRUED_NO equals tralh.ACCRUED_NO
                     where tralh.STATUS_CD == 105
                        && trab.BOOKING_STS == 0
                        && trab.DIVISION_ID == divId
                     select new AccruedExtendData()
                     {
                         BOOKING_NO = trab.BOOKING_NO,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC_OLD = vw.Description,
                         PV_TYPE_NAME = tmpt.PV_TYPE_NAME,
                         DIVISION_ID = trab.DIVISION_ID
                     });

            return q;
        }

        //add by FID.Arri on 8 May 2018 for AccrExtendReport
        public IQueryable<AccruedExtendData> ReportListData(AccrExtendSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from traed in db.TB_R_ACCR_EXTEND_D
                     join trab in db.TB_R_ACCR_BALANCE on traed.BOOKING_NO equals trab.BOOKING_NO into a
                     from trab in a.DefaultIfEmpty()
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trab.PV_TYPE_CD equals tmapt.PV_TYPE_CD into b
                     from tmapt in b.DefaultIfEmpty()
                     join ttsw in db.vw_WBS on trab.WBS_NO_OLD equals ttsw.WbsNumber into c
                     from ttsw in c.DefaultIfEmpty()
                     join ttsw1 in db.vw_WBS on trab.WBS_NO_PR equals ttsw1.WbsNumber into x
                     from ttsw1 in x.DefaultIfEmpty()
                     join trald in db.TB_R_ACCR_LIST_D on trab.BOOKING_NO equals trald.BOOKING_NO into e
                     from trald in e.DefaultIfEmpty()
                     join traeh in db.TB_R_ACCR_EXTEND_H on traed.EXTEND_NO equals traeh.EXTEND_NO into f
                     from traeh in f.DefaultIfEmpty()
                     join d in db.vw_Division on traeh.DIVISION_ID equals d.DIVISION_ID into ds
                     from d in ds.DefaultIfEmpty()
                     join s in db.TB_M_STATUS on traeh.STATUS_CD equals s.STATUS_CD into ss
                     from s in ss.DefaultIfEmpty()
                     select new AccruedExtendData()
                     {
                         BOOKING_NO = traed.BOOKING_NO,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_DESC_OLD = ttsw.Description,
                         SUSPENSE_NO_OLD = trald.SUSPENSE_NO_OLD,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         WBS_DESC_PR = ttsw1.Description,
                         SUSPENSE_NO_PR = trab.SUSPENSE_NO_NEW,
                         BLOCKING_AMT = trab.BLOKING_AMT ?? 0,
                         AVAILABLE_AMT = trab.AVAILABLE_AMT ?? 0,
                         EXTEND_TYPE = traed.EXTEND_TYPE.HasValue ? (traed.EXTEND_TYPE.Value == 1 ? "EXTEND" : "RECLAIM") : "",
                         EXTEND_AMT = traed.EXTEND_AMT ?? 0,
                         DIVISION_ID = traeh.DIVISION_ID
                     }).Distinct();

            if (!crit.ignoreIssuingDivision)
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (!crit.ignoreExtendNo)
            {
                switch (crit.iAno)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.EXTEND_NO.EndsWith(crit.sExtPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.EXTEND_NO.StartsWith(crit.sExtPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.EXTEND_NO.Contains(crit.sExtPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.EXTEND_NO.StartsWith(crit.sExtPre) && p.EXTEND_NO.EndsWith(crit.sExtPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.EXTEND_NO.Contains(crit.extendNo));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }

            if (!crit.ignoreDateFrom)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT >= crit.createdDtFrom);
            }

            if (!crit.ignoreDateTo)
            {
                q = q.Where(p => p.CREATED_DT != null && p.CREATED_DT <= crit.createdDtTo);
            }

            return q;
        }

        public List<AccruedExtendData> GetActivityAccruedExtend(Dictionary<string, object> param)
        {
            string bookingNo = "";
            if (param.ContainsKey("BOOKING_NO"))
                bookingNo = param["BOOKING_NO"].ToString();


            var q = (from d in db.TB_R_ACCR_EXTEND_ACT
                     where d.BOOKING_NO == bookingNo
                     select new AccruedExtendData
                     {
                         EXTEND_NO = d.EXTEND_NO,
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            //if (param.ContainsKey("BOOKING_NO"))
            //{
            //    string bookingNo = param["BOOKING_NO"].ToString();
            //    q = q.Where(p => p.BOOKING_NO == bookingNo);
            //}

            if (q.Any()) return q.ToList();

            return null;
        }

        public bool isDraft(string extendNo)
        {
            var q = (from h in db.TB_R_ACCR_EXTEND_H
                     where h.EXTEND_NO == extendNo
                    select h).FirstOrDefault();

            return q != null && q.STATUS_CD == 0;
        }

        public bool Delete(string extendNo, string bookingNo)
        {
            if (String.IsNullOrEmpty(extendNo)) return false;
            if (String.IsNullOrEmpty(bookingNo)) return false;

            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    var det = (from d in DB.TB_R_ACCR_EXTEND_D
                               where d.EXTEND_NO == extendNo && d.BOOKING_NO == bookingNo 
                              select d).ToList();
                    if (det.Any())
                    {
                        foreach (var d in det)
                        {
                            DB.DeleteObject(d);
                            DB.SaveChanges();
                        }
                    }

                    _result = true;
                    tx.Commit();
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public bool DeleteDraft(string extendNo)
        {
            if (String.IsNullOrEmpty(extendNo)) return false;

            bool _result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    logic.AccrForm.DeleteAccrReffNo(extendNo, DB);

                    var detD = (from d in DB.TB_R_ACCR_EXTEND_D
                                where d.EXTEND_NO == extendNo
                                select d).ToList();
                    if (detD.Any())
                    {
                        foreach (var d in detD)
                        {
                            DB.DeleteObject(d);
                            DB.SaveChanges();
                        }
                    }

                    var detH = (from d in DB.TB_R_ACCR_EXTEND_H
                               where d.EXTEND_NO == extendNo
                               select d).ToList();
                    if (detH.Any())
                    {
                        foreach (var h in detH)
                        {
                            DB.DeleteObject(h);
                            DB.SaveChanges();
                        }
                    }

					//fid) prastyo. delete act extend
                    var act = (from d in DB.TB_R_ACCR_EXTEND_ACT
                                where d.EXTEND_NO == extendNo
                                select d).ToList();
                    if (act.Any())
                    {
                        foreach (var h in act)
                        {
                            DB.DeleteObject(h);
                            DB.SaveChanges();
                        }
                    }

                    _result = true;
                    tx.Commit();
                }
                catch (Exception Ex)
                {
                    tx.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }
            }
            return _result;
        }

        public bool InsertUpdateDetail(string extendNo, List<AccruedExtendData> listItem, Dictionary<string, List<string>> listActivity, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    //deleting extend act
                    var extAct = (from d in db.TB_R_ACCR_EXTEND_ACT
                               where d.EXTEND_NO == extendNo
                               select d).ToList();
                    if (extAct.Any())
                    {
                        foreach (var d in extAct)
                        {
                            db.DeleteObject(d);
                            db.SaveChanges();
                        }
                    }


                    //deleting extend
                    var det = (from d in db.TB_R_ACCR_EXTEND_D
                               where d.EXTEND_NO == extendNo 
                               select d).ToList();
                    if (det.Any())
                    {
                        foreach (var d in det)
                        {
                            db.DeleteObject(d);
                            db.SaveChanges();
                        }
                    }

                    decimal total = 0;

                    //insert extend detail
                    foreach(AccruedExtendData item in listItem)
                    {
                        TB_R_ACCR_EXTEND_D detail;
                        if (!String.IsNullOrEmpty(item.BOOKING_NO_OLD))
                        {
                            item.CHANGED_DT = DateTime.Now;
                            item.CHANGED_BY = userData.USERNAME;
                            detail = new TB_R_ACCR_EXTEND_D()
                            {
                                EXTEND_NO = extendNo,
                                BOOKING_NO = item.BOOKING_NO,
                                EXTEND_AMT = item.EXTEND_AMT,
                                EXTEND_TYPE = item.EXTEND_TYPE_CD,
                                CREATED_DT = item.CREATED_DT,
                                CREATED_BY = item.CREATED_BY,
                                CHANGED_DT = DateTime.Now,
                                CHANGED_BY = userData.USERNAME
                            };
                        }
                        else
                        {
                            detail = new TB_R_ACCR_EXTEND_D()
                            {
                                EXTEND_NO = item.EXTEND_NO,
                                BOOKING_NO = item.BOOKING_NO,
                                EXTEND_AMT = item.EXTEND_AMT,
                                EXTEND_TYPE = item.EXTEND_TYPE_CD,
                                CREATED_DT = DateTime.Now,
                                CREATED_BY = userData.USERNAME
                            };
                        }
                       
                        ///db.AddToTB_R_ACCR_EXTEND_D(detail);
                        db.TB_R_ACCR_EXTEND_D.AddObject(detail);
                        db.SaveChanges();

                        decimal tempDecimal = item.EXTEND_AMT.HasValue ? (decimal)item.EXTEND_AMT : 0;
                        total = total + tempDecimal;
                        if (item.EXTEND_TYPE_CD == 0)
                        {
                            tempDecimal = item.EXTENDABLE_AMT.HasValue ? (decimal)item.EXTENDABLE_AMT : 0;
                            total = total + tempDecimal;
                        }
                    }

                    //update extend header
                    var header = (from h in db.TB_R_ACCR_EXTEND_H
                               where h.EXTEND_NO == extendNo
                               select h).ToList();
                    if (header.Any())
                    {
                        foreach (var h in header)
                        {
                            //db.DeleteObject(d);
                            h.TOTAL_AMT = total;
                            h.CHANGED_DT = DateTime.Now;
                            h.CHANGED_BY = userData.USERNAME;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        TB_R_ACCR_EXTEND_H newHeader = new TB_R_ACCR_EXTEND_H()
                        {
                            EXTEND_NO = extendNo,
                            EXTEND_DATE = DateTime.Now,
                            DIVISION_ID = userData.DIV_CD.ToString(),
                            TOTAL_AMT = total,
                            STATUS_CD = 0,
                            CREATED_DT = DateTime.Now,
                            CREATED_BY = userData.USERNAME,
                            CHANGED_DT = DateTime.Now,
                            CHANGED_BY = userData.USERNAME,
                            BUDGET_YEAR = DateTime.Now.Year - 1
                        };
                        //db.AddToTB_R_ACCR_EXTEND_H(newHeader);
                        db.TB_R_ACCR_EXTEND_H.AddObject(newHeader);
                        db.SaveChanges();
                    }


                    //insert extend act
                    foreach (KeyValuePair<string, List<string>> bookingAct in listActivity)
                    {
                        string BookingNo = bookingAct.Key;
                        foreach (string actDes in bookingAct.Value)
                        {
                            TB_R_ACCR_EXTEND_ACT Activity = new TB_R_ACCR_EXTEND_ACT()
                            {
                                EXTEND_NO = extendNo,
                                BOOKING_NO = BookingNo,
                                ACTIVITY_DES = actDes,
                                CREATED_DT = DateTime.Now,
                                CREATED_BY = userData.USERNAME
                            };
                            //db.AddToTB_R_ACCR_EXTEND_ACT(Activity);
                            db.TB_R_ACCR_EXTEND_ACT.AddObject(Activity);
                            db.SaveChanges();

                        }
                        // do something with entry.Value or entry.Key
                    }

                    logic.AccrForm.InsertAccrReffNo(extendNo, 5, db);
                    
                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;

        }

        public bool IsExist(string extNo)
        {
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    var set_h = (from i in db.TB_R_ACCR_EXTEND_H
                                 where i.EXTEND_NO == extNo 
                                    select i);
                    if (set_h.Any())
                    {
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    result = false;
                }
            }

            return result;

        }

        public bool InsertDetail(AccruedExtendData item, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    var set_d = (from i in db.TB_R_ACCR_EXTEND_D
                                 where i.BOOKING_NO == item.BOOKING_NO && i.EXTEND_NO == item.EXTEND_NO 
                                 select i).FirstOrDefault();
                    if (set_d == null)
                    {
                        TB_R_ACCR_EXTEND_D detail = new TB_R_ACCR_EXTEND_D()
                        {
                            EXTEND_NO = item.EXTEND_NO,
                            BOOKING_NO = item.BOOKING_NO,
                            EXTEND_AMT = item.EXTEND_AMT,
                            CREATED_DT = DateTime.Now,
                            CREATED_BY = userData.USERNAME
                        };
                        db.AddToTB_R_ACCR_EXTEND_D(detail);
                        db.SaveChanges();
                    }
                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;

        }


        private bool ValidateActualOnTime(DateTime? planDate, DateTime? actualDate)
        {
            bool isValid = true;
            if (planDate != null)
            {
                if (actualDate == null)
                {
                    DateTime now = DateTime.Now;
                    if (now.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
                else
                {
                    if (actualDate.Value.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
            }
            return isValid;
        }
        public List<AccruedListDetail> GetActivityList(string bookingNo)
        {

            var q = (from d in db.TB_R_ACCR_LIST_D
                     where d.BOOKING_NO == bookingNo 
                     select new AccruedListDetail
                     {
                         ACCRUED_NO = d.ACCRUED_NO,
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            if (q.Any()) return q.OrderBy(x => x.ACTIVITY_DES).ToList();

            return null;
        }

        public List<AccruedListDetail> GetCurrentActivity(string extendNo, string bookingNo)
        {

            var q = (from d in db.TB_R_ACCR_EXTEND_ACT 
                     where d.BOOKING_NO == bookingNo && d.EXTEND_NO == extendNo 
                     select new AccruedListDetail
                     {
                         BOOKING_NO = d.BOOKING_NO,
                         ACTIVITY_DES = d.ACTIVITY_DES
                     });

            if (q.Any()) return q.ToList();

            return new List<AccruedListDetail>();
        }

        //added by FID.Ridwan 04072018
        public List<AccruedExtendData> GetViewActivity(string extendNo) //, string bookingNo)
        {
            List<AccruedExtendData> returnData = new List<AccruedExtendData>();
            returnData = Qx<AccruedExtendData>("GetActivityData", new { EXTEND_NO = extendNo }).ToList(); //, BOOKING_NO = bookingNo

            return returnData;
        }

        public AccruedExtendData GetCurHeader(string extendNo)
        {

            var q = (from d in db.TB_R_ACCR_EXTEND_H
                     where d.EXTEND_NO == extendNo
                     select new AccruedExtendData
                     {
                         EXTEND_NO = d.EXTEND_NO,
                         DIVISION_ID = d.DIVISION_ID,
                         EXTEND_AMT = d.TOTAL_AMT,
                         STATUS_CD = d.STATUS_CD.HasValue ? d.STATUS_CD.Value : 0,
                         BUDGET_YEAR = d.BUDGET_YEAR.HasValue ? d.BUDGET_YEAR.Value : 0
                     });

            if (q.Any()) return q.FirstOrDefault();

            return new AccruedExtendData();
        }

        public bool HasSelectAll(string userName)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                int i = (from p in db.vw_User_Role_Header
                         where p.USERNAME == userName
                           && (p.ROLE_ID == "ELVIS_ACC" || p.ROLE_ID == "ELVIS_FD_STAFF")
                         select p.USERNAME).Count();
                return i > 0;
            }
        }

        #region Get String Builder

        private String DateTimeToString(DateTime? dateTimeParam, String format)
        {
            if (dateTimeParam != null)
            {
                DateTime dateTimeParam1 = (DateTime)dateTimeParam;
                return dateTimeParam1.ToString(format);
            }
            else
            {
                return null;
            }
        }

        public byte[] Get(
            string _username,
            string _imagePath,
            AccrExtendSearchCriteria c)
        {
            ExcelWriter x = new ExcelWriter();

            var qq = Search(c);

            int sizeForAll = qq.Count();

            string header1 = "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                            "Downloaded Date : " + DateTime.Now.date() + ExcelWriter.COL_SEP +
                            "Total Record Download : " + sizeForAll;
            string header2 = "No|Work Flow Status|Accrued No.|Issuing Division|" +
                            "Created Date|Updated Date|" +
                            "Submission Status|PIC{Current|Next}|Total Amount";
            x.PutPage("General Info");
            x.PutHeader("Accrued Extend List General Info", header1, header2, _imagePath);
            int rownum = 0;
            int pageSize = ExcelWriter.xlRowLimit;
            // the first eleven rows is used as header
            pageSize = pageSize - 11;
            int isNewPage = 1;
            int counter = 1;
            List<AccruedExtendData> _list =
                qq.ToList();
            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedExtendData dx in _list)
            {
                if (counter >= pageSize)
                {
                    counter = 1;
                    x.PutTail();
                    x.PutPage("General Info " + ++isNewPage);
                    x.PutHeader("Accrued Extend List General Info", header1, header2, _imagePath);
                }

                x.Put(
                    ++rownum,
                    dx.WORKFLOW_STATUS, dx.EXTEND_NO,
                    dx.DIVISION_NAME, dx.CREATED_DT.date(),
                    dx.CHANGED_DT.date(), dx.SUBMISSION_STATUS,
                    dx.PIC_CURRENT, dx.PIC_NEXT, CommonFunction.Eval_Curr("IDR", dx.TOT_AMOUNT));
                counter++;
            }


            x.PutTail();



            return x.GetBytes();
        }
        #endregion

        public string GenerateExtendFormReport(
            string _username,
            string templateFile,
            string newFile,
            string extendNo,
            string issuingDiv,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "ACCR";

            x.MarkPage(_sheetName);

            AccruedExtendData dataH = GetCurHeader(extendNo);

            //Get data from search criteria
            var qq = SearchForm(extendNo);
            List<AccruedExtendData> _list =
                qq.ToList();

            //Set value Header
            string budgetYear = (dataH.BUDGET_YEAR == null) ? DateTime.Now.ToString("yyyy") : dataH.BUDGET_YEAR.ToString();
            string titleReport = "LIST ACCRUED EXTEND TAHUN " + budgetYear;

            string divisionName = String.IsNullOrEmpty(issuingDiv) ? "All" : issuingDiv;
            string divisionFullName = String.IsNullOrEmpty(GetDivisionFullName(issuingDiv)) ? "All" : GetDivisionFullName(issuingDiv);
            string accruedExtNo = String.IsNullOrEmpty(extendNo) ? "All" : extendNo;
            string updatedDt = "";
            string createdDt = "";

            string printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            string printedDt = DateTime.Now.ToString("dd-MM-yyyy");

            decimal? totalReclaimable = 0;
            decimal? totalExtendable = 0;
            decimal? totalAmount = 0;

            foreach (AccruedExtendData hAccr in _list)
            {
                updatedDt = hAccr.CHANGED_DT.HasValue
                             ? hAccr.CHANGED_DT.Value.ToString("dd-MM-yyyy")
                             : "";
                createdDt = hAccr.CREATED_DT.ToString("dd-MM-yyyy");
                totalReclaimable = totalReclaimable + hAccr.RECLAIMABLE_AMT;
                totalExtendable = totalExtendable + hAccr.EXTENDABLE_AMT;
                totalAmount = totalAmount + (hAccr.EXTEND_AMT ?? 0);
            }

            x.MarkCells("[TITLE_REPORT],[DIVISION],[ACCRUED_EXT_NO],[UPDATED_DT],[PRINTED_BY],[PRINTED_DT],[TOTAL_RECLAIMABLE],[TOTAL_EXTENDABLE],[TOTAL_AMOUNT]");
            x.PutCells(titleReport, divisionFullName, accruedExtNo, createdDt, printedBy, printedDt, (totalReclaimable ?? 0).ToString("N0"), (totalExtendable ?? 0).ToString("N0"), (totalAmount ?? 0).ToString("N0"));
            //End set value Header

            //Set Value Detail
            x.MarkRow("[BOOKING_NO],[PV_TYPE],[ACTIVITY],[WBS_NO],[WBS_DESC],[SUSPENSE_NO],"
                + "[WBS_PR_NO],[WBS_PR_DESC],[SUSPENSE_PR_NO],[AMOUNT_RECLAIMABLE],"
                + "[AMOUNT_EXTENDABLE],[ACT_TYPE],[ACT_AMOUNT]");

            int j = 0;

            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedExtendData dx in _list)
            {
                //Get the Activities
                var param = new Dictionary<string, object>();
                param.Add("BOOKING_NO", dx.BOOKING_NO);

                List<AccruedExtendData> _Activities = GetActivityAccruedExtend(param);

                List<string> act = new List<string>();
                if(_Activities != null)
                {
                    act = (from a in _Activities
                           select a.ACTIVITY_DES
                           ).ToList();
                }

                //Input Data to list detail
                string suspenseNo = dx.SUSPENSE_NO_OLD == null ? "" : dx.SUSPENSE_NO_OLD.ToString();
                x.PutRowInsert(dx.BOOKING_NO, dx.PV_TYPE_NAME, String.Join(";", act), dx.WBS_NO_OLD,
                        dx.WBS_DESC_OLD, suspenseNo, dx.WBS_NO_PR, dx.WBS_DESC_PR,
                        dx.SUSPENSE_NO_PR.str(), dx.RECLAIMABLE_AMT, dx.EXTENDABLE_AMT, dx.EXTEND_TYPE
                        , dx.EXTEND_AMT);

                j++;
            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "", "", "",
                         "", "", "");
            }
            //End set value Detail

            x.PutTail();

            //return x.GetBytes();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            //return filePath;
            if (String.IsNullOrEmpty(pdfExport))
            {
                return filePath;
            }
            else
            {
                string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
                bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
                if (rslt)
                {
                    return pdfPath;
                }
                else
                {
                    return filePath;
                }

            }
        }

        private string GetDivisionFullName(string code)
        {
            string fullName = "";

            var q = (from vDiv in db.vw_Division
                     where vDiv.DIVISION_NAME == code
                     select vDiv);
            if (q != null)
            {
                foreach (var qq in q)
                {
                    fullName = qq.FULL_NAME;
                }
            }

            return fullName;
        }

        public List<CodeConstant> getExtendType(string extendTypeCd = "")
        {

            List<CodeConstant> lstExtendType = new List<CodeConstant>();

            try
            {
                //string fWbs = "wbs_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
                StringBuilder sql = new StringBuilder(
                            "select SYSTEM_VALUE_NUM  AS Code, SYSTEM_VALUE_TXT AS Description from TB_M_SYSTEM where SYSTEM_TYPE = 'ACCR.EXTEND.TYPE'  "
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );


                if (!string.IsNullOrEmpty(extendTypeCd))
                {
                    sql.AppendFormat("WHERE SYSTEM_VALUE_NUM = {0} ", extendTypeCd);
                }

                lstExtendType = Db.Query<CodeConstant>(sql.ToString()).ToList();
                //LoggingLogic.say(fWbs, sql.ToString());

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstExtendType;
        }


        public IQueryable<AccruedExtendData> GetListDivAccrExtend(string AccrExtNoDiv)
        {
            db.CommandTimeout = 3600;
            var q = (from traeh in db.TB_R_ACCR_EXTEND_H
                     select new AccruedExtendData()
                     {
                         EXTEND_NO = traeh.EXTEND_NO
                     });

            q = q.Where(p => p.EXTEND_NO.Contains(AccrExtNoDiv));

            return q;
        }

        public int GetLatestStatus(string extendNo)
        {
            return (from h in db.TB_R_ACCR_EXTEND_H
                    where h.EXTEND_NO == extendNo
                    select h.STATUS_CD).FirstOrDefault() ?? 0;
        }

        // add by FID.Ridwan 06072018
        public void UpdateExtend(String ExtendNo, List<AccruedExtendData> Details, String extendDt)
        {
            foreach (var det in Details)
            {
                Qx<string>("UpdateApproveExtend", new { ExtendNo = ExtendNo, BookingNo = det.BOOKING_NO, extendDt = extendDt });
            }
        }

        public bool UpdateExtendDate(List<AccruedExtendData> Details, DateTime ExtendDt, UserData userData)
        {
            bool result = false;

            DbTransaction tx = null;
            using (ContextWrap CO = new ContextWrap())
            {
                try
                {
                    var DB = CO.db;
                    tx = DB.Connection.BeginTransaction();

                    foreach (var det in Details)
                    {
                        var bal = (from b in DB.TB_R_ACCR_BALANCE
                                   where b.BOOKING_NO == det.BOOKING_NO
                                   select b).FirstOrDefault();

                        bal.EXPIRE_DT = ExtendDt;
                        bal.CHANGED_BY = userData.USERNAME;
                        bal.CHANGED_DT = DateTime.Now;

                        db.SaveChanges();
                    }

                    tx.Commit();
                    result = true;
                }
                catch (Exception e)
                {
                    tx.Rollback();
                    Handle(e);
                    result = false;
                }
            }

            return result;
        }
    }
}