﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Model;
using Common.Data;

namespace BusinessLogic
{
    public class LockingLogic
    {
        public bool IsLocked(string FunctionId, string LockRef)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var q = co.db.TB_T_LOCK
                    .Where(i => i.function_id.ToUpper() == FunctionId.ToUpper()
                        && i.lock_reff.ToUpper() == LockRef.ToUpper());
                if (q.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool DoLock(int process_id, string FunctionId, string LockReff, string UserId)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                TB_T_LOCK _tbTLock = new TB_T_LOCK();
                _tbTLock.process_id = process_id;
                _tbTLock.function_id = FunctionId;
                _tbTLock.lock_reff = LockReff;
                _tbTLock.user_id = UserId;
                _tbTLock.start_time = DateTime.Now;

                _db.AddToTB_T_LOCK(_tbTLock);
                if (_db.SaveChanges() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool DoUnlock(int ProcessId)
        {
            bool _bResult = false;
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = (from i in _db.TB_T_LOCK
                         where i.process_id == ProcessId
                         select i)
                         .FirstOrDefault();
                if (q != null)
                {
                    _db.TB_T_LOCK.DeleteObject(q);
                    if (_db.SaveChanges() > 0)
                    {
                        _bResult = true;
                    }
                }
            }
            return _bResult;
        }
        public LockingData getDataLocking(string FunctionId, string LockReff)
        {
            LockingData _data = null;

            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = (from i in _db.TB_T_LOCK
                         where i.function_id == FunctionId
                         && i.lock_reff == LockReff
                         select i).FirstOrDefault();
                if (q != null)
                {
                    _data = new LockingData();
                    _data.process_id = q.process_id;
                    _data.function_id = q.function_id;
                    _data.lock_reff = q.lock_reff;
                    _data.user_id = q.user_id;
                    _data.start_time = q.start_time;
                }
            }
            return _data;
        }
    }
}
