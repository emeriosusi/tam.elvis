﻿using System;
namespace BusinessLogic
{
    public interface ITick
    {
        void In(string w, params object[] x);
        int Level { get; set; }
        string Op(int op = 0, string x = null);
        void Out(string w = null, params object[] x);
        void Spin(string w, params object[] x);
        void Trace(string w);
    }
}
