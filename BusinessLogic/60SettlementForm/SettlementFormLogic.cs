﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Function;
using Dapper;
using DataLayer.Model;

namespace BusinessLogic._60SettlementForm
{
    public class SettlementFormLogic : LogicBase
    {
        protected List<ExchangeRate> rates;
        protected List<string> policyCurr;
        private LogicFactory logic = LogicFactory.Get();
        public SettlementFormLogic()
        {
            rates =  logic.Look.GetCashierPolicyExchangeRates();
            policyCurr = (from r in rates where r.AmountMin > 0 select r.CurrencyCode).ToList();
        }

        #region Search Inquiry

        public List<SettlementFormDetailData> Details(int sus_no, int sus_yy)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = (from d in _db.vw_Settlement_Detail
                         where d.PV_NO == sus_no && d.PV_YEAR == sus_yy
                         select new SettlementFormDetailData()
                         {
                             SEQ_NO = (d.SET_SEQ_NO != null && d.SET_SEQ_NO != 0) ? d.SET_SEQ_NO : null,
                             PV_SEQ_NO = d.PV_SEQ_NO,
                             PV_SUS_NO = d.PV_NO,
                             PV_YEAR = d.PV_YEAR,
                             COST_CENTER_NAME = d.COST_CENTER_NAME,
                             CURRENCY = d.CURRENCY_CD,
                             SETTLEMENT_AMOUNT = d.SETTLEMENT_AMOUNT,
                             SPENT_AMOUNT = d.SPENT_AMOUNT,

                             SETTLEMENT_NO = d.SETTLEMENT_NO,
                             COST_CENTER_CD = d.COST_CENTER_CD,
                             DESCRIPTION = d.DESCRIPTION,
                             CURRENCY_CD = d.CURRENCY_CD,
                             AMOUNT = d.AMOUNT
                         });
                if (q.Any())
                    return q.ToList();
                else
                    return null;
            }

        }

        public IQueryable SearchUnsettled(string DivCd)
        {
            var db = new ELVIS_DBEntities();
            return (from u in db.vw_UnsettledSuspense
                    // fid for Accrued enhancement
                    join b in db.TB_R_ACCR_BALANCE
                    on u.PV_NO equals b.SUSPENSE_NO_NEW into ub
                    from b in ub.DefaultIfEmpty()
                    where u.DIVISION_ID == DivCd
                        && b.SUSPENSE_NO_NEW == null
                    select u);
        }

        public IQueryable SearchUnsettledAccr(string DivCd)
        {
            var db = new ELVIS_DBEntities();

            var q = (from u in db.vw_UnsettledSuspense
                     join b in db.TB_R_ACCR_BALANCE on u.PV_NO equals b.SUSPENSE_NO_NEW into ub
                     from b in ub.DefaultIfEmpty()
                     join c in db.TB_R_ACCR_LIST_D on b.BOOKING_NO equals c.BOOKING_NO into uc
                     from c in uc.DefaultIfEmpty()
                     where u.DIVISION_ID == DivCd && b.SUSPENSE_NO_NEW != null
                        && b.BOOKING_STS == 0
                        && (b.EXPIRE_DT ?? DateTime.Now) >= DateTime.Now
                     select new SettlementFormDetailData()
                     {
                         REFF_NO = u.REFF_NO != null ? u.REFF_NO.Value : 0,
                         SUSPENSE_NO_NEW = b.SUSPENSE_NO_NEW != null ? b.SUSPENSE_NO_NEW.Value : 0,
                         SUSPENSE_NO_OLD = b.SUSPENSE_NO_OLD != null ? b.SUSPENSE_NO_OLD.Value : 0,
                         ACTIVITY_DES = c.ACTIVITY_DES
                     });

            return q;
        }

        public List<SuspenseStructure> SearchUnclosed(string DivCd, string WbsNo)
        {
            db.CommandTimeout = 3600;

            List<SuspenseStructure> suspense = null;

            try
            {
                suspense = (from u in db.vw_UnclosedSuspense
                                join b in db.TB_R_ACCR_BALANCE
                                on u.PV_NO equals b.SUSPENSE_NO_NEW into ub
                                from b in ub.DefaultIfEmpty()
                                where u.DIVISION_ID == DivCd
                                    && u.BUDGET_NO == WbsNo
                                    && b.SUSPENSE_NO_NEW == null
                                select new SuspenseStructure
                                {
                                    ReffNo = u.REFF_NO,
                                    PVNo = u.PV_NO.ToString(),
                                    TotalAmount = u.TOTAL_AMOUNT,
                                    VendorName = u.VENDOR_NAME,
                                    ActivityDateTo = u.ACTIVITY_DATE_TO,
                                    CurrCode = u.CURRENCY_CD,
                                    CostCenter = u.COST_CENTER_CD,
                                    ActivityDesc = u.ACTIVITY_DESC
                                }).ToList();
            }
            catch (Exception ex)
            {
                Handle(ex);
            }

            return suspense;
        }


        public SettlementFormHeaderData Header(int sus_no, int sus_yy)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = (from p in _db.vw_Settlement_Header
                         where p.PV_NO == sus_no && p.PV_YEAR == sus_yy
                         select p);
                if (q.Any())
                {
                    var d = q.FirstOrDefault();
                    return new SettlementFormHeaderData()
                    {
                        PV_SUS_NO = d.PV_SUS_NO,
                        CONVERT_FLAG = d.CONVERT_FLAG,
                        PV_NO = d.PV_NO,
                        PV_YEAR = d.PV_YEAR,
                        PV_TYPE = d.PV_TYPE_NAME,
                        DIVISION_ID = d.DIVISION_ID,
                        VENDOR_NAME = d.VENDOR_NAME,
                        TRANSACTION_NAME = d.TRANSACTION_NAME,
                        PV_TYPE_CD = d.PV_TYPE_CD,
                        VENDOR_CD = d.VENDOR_CD,
                        VENDOR_GROUP_CD = d.VENDOR_GROUP_CD,
                        TRANSACTION_CD = d.TRANSACTION_CD,
                        PAY_METHOD_CD = d.PAY_METHOD_CD,
                        SETTLEMENT_STATUS = d.SETTLEMENT_STATUS,
                        STATUS_CD = d.STATUS_CD
                    };
                }
                else
                {
                    return null;
                }
            }
        }

     

        #endregion


        private void ReplaceOriginalAmount(SettlementFormDetailData setd, UserData u, ELVIS_DBEntities db)
        {
            var q = (from o in db.TB_R_ORIGINAL_AMOUNT
                     where o.DOC_YEAR == setd.PV_YEAR
                        && o.DOC_NO == setd.PV_SUS_NO
                        && o.SEQ_NO == setd.PV_SEQ_NO
                     select o);

            TB_R_ORIGINAL_AMOUNT oa;
            if (q.Any())
            {
                oa = q.FirstOrDefault();

                oa.CHANGED_BY = u.USERNAME;
                oa.CHANGED_DT = DateTime.Now;
            }
            else
            {
                oa = new TB_R_ORIGINAL_AMOUNT();
                oa.DOC_NO = setd.PV_SUS_NO;
                oa.DOC_YEAR = setd.PV_YEAR;
                oa.SEQ_NO = setd.PV_SEQ_NO;
                oa.EXCHANGE_RATE = (decimal)(
                    from r in rates
                    where setd.CURRENCY_CD.Equals(r.CurrencyCode.Trim())
                    select r.Rate).FirstOrDefault();
                oa.CREATED_BY = u.USERNAME;
                oa.CREATED_DT = DateTime.Now;
            }
            oa.CURRENCY_CD = setd.CURRENCY_CD;
            oa.SUSPENSE_AMOUNT = null;
            oa.AMOUNT = setd.AMOUNT;
            oa.SPENT_AMOUNT = (decimal)setd.SPENT_AMOUNT;
            db.SaveChanges();
        }

        #region Insert
        public bool Insert(SettlementFormHeaderData header,
            List<SettlementFormDetailData> details, UserData userData)
        {
            bool result = false;
            DbTransaction trans = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    trans = db.Connection.BeginTransaction();

                    #region Save Header
                    TB_R_SET_FORM_H seth = new TB_R_SET_FORM_H();

                    seth.PV_SUS_NO = header.PV_NO;
                    seth.PV_YEAR = header.PV_YEAR;
                    seth.CONVERT_FLAG = (int)header.CONVERT_FLAG;
                    seth.CREATED_BY = userData.USERNAME;
                    seth.CREATED_DT = DateTime.Now;

                    db.AddToTB_R_SET_FORM_H(seth);
                    db.SaveChanges();
                    #endregion

                    #region Save Detail

                    foreach (SettlementFormDetailData se in details)
                    {
                        ReplaceOriginalAmount(se, userData, db);

                        if (se.CURRENCY_CD.Equals("IDR")
                            || header.PAY_METHOD_CD != "C"
                            || !policyCurr.Contains(se.CURRENCY_CD))
                        {
                            TB_R_SET_FORM_D setd = new TB_R_SET_FORM_D();
                            setd.SEQ_NO = se.PV_SEQ_NO;
                            setd.PV_SUS_NO = se.PV_SUS_NO;
                            setd.PV_YEAR = se.PV_YEAR;

                            setd.SPENT_AMOUNT = (decimal)(se.SPENT_AMOUNT ?? 0);
                            setd.CREATED_BY = userData.USERNAME;
                            setd.CREATED_DT = DateTime.Now;

                            db.AddToTB_R_SET_FORM_D(setd);
                        }
                        db.SaveChanges();
                    }
                    #endregion

                    trans.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    if (trans != null)
                        trans.Rollback();
                    LoggingLogic.err(ex);

                    result = false;
                }
            }
            return result;
        }
        #endregion

        #region Update
  

        public bool Update(
            SettlementFormHeaderData header,
            List<SettlementFormDetailData> details,
            UserData userData,
            bool doConvert = false,
            PVFormHeaderData pv = null,
            RVFormHeaderData rv = null)
        {
            bool result = false;
            DbTransaction TX = null;

            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    #region Update Header
                    var seth = (from i in db.TB_R_SET_FORM_H
                                where i.PV_SUS_NO == header.PV_SUS_NO
                                where i.PV_YEAR == header.PV_YEAR
                                select i).FirstOrDefault();
                    if (seth != null)
                    {
                        seth.CONVERT_FLAG = (int)(header.CONVERT_FLAG ?? 0);
                        seth.CHANGED_BY = userData.USERNAME;
                        seth.CHANGED_DT = DateTime.Now;
                        db.SaveChanges();
                    }
                    #endregion

                    #region Update Detail
                    #region Delete DB Detail
                    var setd = (from i in db.TB_R_SET_FORM_D
                                where i.PV_SUS_NO == header.PV_SUS_NO
                                where i.PV_YEAR == header.PV_YEAR
                                select i);
                    if ((setd.Any()))
                    {
                        foreach (var d in setd)
                        {
                            var q = (from sfd in details
                                     where sfd.SEQ_NO != null && sfd.SEQ_NO == d.SEQ_NO
                                     select sfd.SEQ_NO);
                            if (!q.Any())
                            {
                                db.DeleteObject(d);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    #region Update Detail
                    foreach (SettlementFormDetailData d in details)
                    {
                        ReplaceOriginalAmount(d, userData, db);

                        bool add = true;
                        if (d.SEQ_NO != null)
                        {
                            var set_d = (from i in db.TB_R_SET_FORM_D
                                         where i.PV_SUS_NO == d.PV_SUS_NO
                                         where i.PV_YEAR == d.PV_YEAR
                                         where i.SEQ_NO == d.SEQ_NO
                                         select i).FirstOrDefault();
                            if (set_d != null)
                            {
                                if (doConvert)
                                    set_d.SETTLEMENT_NO = d.SETTLEMENT_NO;
                                set_d.SPENT_AMOUNT = d.SPENT_AMOUNT ?? 0;
                                set_d.CHANGED_BY = userData.USERNAME;
                                set_d.CHANGED_DT = DateTime.Now;

                                db.SaveChanges();
                                add = false;
                            }
                        }

                        add = add && (d.CURRENCY_CD.Equals("IDR")
                            || header.PAY_METHOD_CD != "C"
                            || !policyCurr.Contains(d.CURRENCY_CD));

                        if (add)
                        {
                            TB_R_SET_FORM_D set_d = new TB_R_SET_FORM_D();
                            set_d.SEQ_NO = d.PV_SEQ_NO;
                            set_d.PV_SUS_NO = d.PV_SUS_NO;
                            set_d.PV_YEAR = d.PV_YEAR;
                            set_d.SPENT_AMOUNT = (decimal)(d.SPENT_AMOUNT ?? 0);
                            if (doConvert)
                            {
                                set_d.SETTLEMENT_NO = d.SETTLEMENT_NO;
                            }
                            set_d.CREATED_BY = userData.USERNAME;
                            set_d.CREATED_DT = DateTime.Now;

                            db.AddToTB_R_SET_FORM_D(set_d);
                            db.SaveChanges();
                        }
                    }
                    #endregion
                    #endregion
                    if (doConvert)
                    {
                        Convert(db, header, details, userData, pv, rv);
                    }

                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;
        }
        #endregion

        private void Convert(
            ELVIS_DBEntities db,
            SettlementFormHeaderData sh,
            List<SettlementFormDetailData> sd,
            UserData userData, PVFormHeaderData _pv, RVFormHeaderData _rv)
        {
            string _no, _year;
            _no = sh.PV_SUS_NO.str();
            _year = sh.PV_YEAR.str();

            // get settlement detail from vw_SettlementDetail 
            // List<SettlementFormDetailData> r = sd;

            PVFormData PV = null;
            PVFormData RV = null;


            PVFormData sus = logic.PVForm.search(sh.PV_SUS_NO ?? 0, sh.PV_YEAR);

            int maxSu = 0;
            if (sus != null && sus.Details != null && sus.Details.Count > 0)
            {
                maxSu = sus.Details.Select(a => a.SequenceNumber).Max();
            }

            var qCurrencySum = sd.GroupBy(c => c.CURRENCY_CD)
                               .Select(x => new
                               {
                                   CURRENCY_CD = x.Key,
                                   AMOUNT = x.Sum(X => (X.SETTLEMENT_AMOUNT ?? 0))
                               });

            Dictionary<String, Decimal> amounts = new Dictionary<String, Decimal>();

            foreach (var qcs in qCurrencySum)
            {
                amounts.Add(qcs.CURRENCY_CD, qcs.AMOUNT);
            }
            DateTime today = DateTime.Now;
            string sql = string.Format("SELECT dbo.fn_GetFiscalYear(CONVERT(DATETIME,'{0}',112))", new object[] { today.ToString("yyyyMMdd") });
            
            int fiscal = Db.Query<int>(sql).FirstOrDefault();
            sql = string.Format("SELECT dbo.fn_ReFiscal('{0}', {1})", new object[] {sus.BudgetNumber, fiscal});
            string wbsFiscal = Db.Query<string>(sql).FirstOrDefault();

            string bookingNo = logic.Look.getBookingNoBySuspense(sh.PV_SUS_NO ?? 0, sh.PV_YEAR);    //fid.Hadid 20180409

            for (int i = 0; i < sd.Count; i++)
            {
                SettlementFormDetailData d = sd[i];
                if (!amounts.ContainsKey(d.CURRENCY))
                    continue;
                int seq = 0;
                if (d.SEQ_NO == null)
                {
                    seq = maxSu + 1;
                    maxSu++;
                }
                else
                {
                    seq = d.SEQ_NO ?? maxSu;
                }

                PVFormDetail pvd = (from s in sus.Details
                                    where s.SequenceNumber == d.PV_SEQ_NO
                                    select s).FirstOrDefault();

                if (amounts[d.CURRENCY] < 0)
                {
                    if (RV == null)
                    {
                        RV = new PVFormData()
                        {
                            StatusCode = 0,
                            PaymentMethodCode = sus.PaymentMethodCode,
                            VendorGroupCode = sus.VendorGroupCode,
                            VendorCode = sus.VendorCode,
                            PVTypeCode = 2,
                            TransactionCode = sus.TransactionCode,
                            PVDate = today,
                            SuspenseNumber = sus.PVNumber,
                            SuspenseYear = sus.PVYear,
                            //fid.Hadid 20180409
                            //BudgetNumber = wbsFiscal,
                            BudgetNumber = bookingNo ?? wbsFiscal,
                            //end fid.Hadid 20180409
                            ActivityDate = sus.ActivityDate,
                            ActivityDateTo = sus.ActivityDateTo,
                            ReferenceNo = sus.ReferenceNo,
                            SubmitHC = sus.SubmitHC,
                            WorkflowStatus = sus.WorkflowStatus,
                            DivisionID = sus.DivisionID
                        };

                    }

                    RV.Details.Add(new PVFormDetail()
                    {
                        SequenceNumber = seq,
                        CostCenterCode = pvd.CostCenterCode,
                        CurrencyCode = pvd.CurrencyCode,
                        Description = pvd.Description,
                        Amount = -1 * (d.SETTLEMENT_AMOUNT ?? 0),
                        InvoiceNumber = pvd.InvoiceNumber,
                        ItemTransaction = pvd.ItemTransaction,
                        TaxNumber = pvd.TaxNumber,
                        TaxCode = pvd.TaxCode
                    });
                }
                else if (amounts[d.CURRENCY] >= 0)
                {

                    if (PV == null)
                    {
                        PV = new PVFormData()
                        {
                            StatusCode = 0,
                            PaymentMethodCode = sus.PaymentMethodCode,
                            VendorGroupCode = sus.VendorGroupCode,
                            VendorCode = sus.VendorCode,
                            PVTypeCode = 3,
                            TransactionCode = sus.TransactionCode,
                            PVDate = today,
                            SuspenseNumber = sus.PVNumber,
                            SuspenseYear = sus.PVYear,
                            BudgetNumber = wbsFiscal,
                            ActivityDate = sus.ActivityDate,
                            ActivityDateTo = sus.ActivityDateTo,
                            ReferenceNo = sus.ReferenceNo,
                            DivisionID = sus.DivisionID
                        };
                    }

                    PV.Details.Add(new PVFormDetail()
                    {
                        SequenceNumber = seq,
                        CostCenterCode = pvd.CostCenterCode,
                        CurrencyCode = pvd.CurrencyCode,
                        Description = pvd.Description,
                        Amount = d.SETTLEMENT_AMOUNT ?? 0,
                        InvoiceNumber = pvd.InvoiceNumber,
                        ItemTransaction = pvd.ItemTransaction,
                        TaxNumber = pvd.TaxNumber,
                        TaxCode = pvd.TaxCode
                    });
                }
            }
            if (PV != null)
            {
                if (logic.PVForm.SaveData(db, PV, userData, false) && _pv != null)
                {
                    _pv.PV_NO = PV.PVNumber ?? 0;
                    _pv.PV_YEAR = PV.PVYear ?? 0;
                    _pv.TRANSACTION_CD = PV.TransactionCode ?? 0; // FID.Ridwan 12072018
                }
            }

            if (RV != null)
            {
                if (logic.RVForm.SaveData(db, RV, userData, false) && _rv != null)
                {
                    _rv.RV_NO = RV.PVNumber ?? 0;
                    _rv.RV_YEAR = RV.PVYear ?? 0;
                    _rv.TRANSACTION_CD = RV.TransactionCode ?? 0; // FID.Ridwan 12072018
                }
            }

            if (PV != null || RV != null)
            {
                saveSuspenseConverted(db, sh, _pv, _rv, userData);
            }

        }

        private void saveSuspenseConverted(ELVIS_DBEntities db, SettlementFormHeaderData sh, PVFormHeaderData pv, RVFormHeaderData rv, UserData u)
        {
            if (sh == null || (pv == null && rv == null) || u == null)
                return;

            var su = (from x in db.TB_R_PV_H
                      where (x.PV_NO == sh.PV_SUS_NO)
                      where (x.PV_YEAR == sh.PV_YEAR)
                      select x).FirstOrDefault();
            if (pv != null && pv.PV_NO != 0 && pv.PV_YEAR != 0)
                su.SET_NO_PV = pv.PV_NO;
            if (rv != null && rv.RV_NO != 0 && rv.RV_YEAR != 0)
                su.SET_NO_RV = rv.RV_NO;
            su.CHANGED_BY = u.USERNAME;
            su.CHANGED_DATE = DateTime.Now;
            db.SaveChanges();
        }

        public List<SuspenseStructure> getSuspenseNoLookup()
        {

            List<SuspenseStructure> lstSuspenseResult = new List<SuspenseStructure>();

            try
            {
                StringBuilder sql = new StringBuilder(
                            "SELECT DISTINCT ab.WBS_NO_OLD as WbsNumber, v.[Description] \r\n" +
                            "  FROM TB_R_ACCR_BALANCE ab left join dbo.vw_WBS v ON ab.WBS_NO_OLD = v.WbsNumber \r\n" +
                            " "
                            );
                lstSuspenseResult = Db.Query<SuspenseStructure>(sql.ToString()).ToList();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstSuspenseResult;
        }
      
    }
}
