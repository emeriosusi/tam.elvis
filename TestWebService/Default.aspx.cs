﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TestWebService
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ServiceKu.HRServices service1 = new ServiceKu.HRServices();
            ServiceKu1.ELVISServices service2 = new ServiceKu1.ELVISServices();
            //ServiceKu.PVData[] data = new ServiceKu.PVData[2];

            //data[0] = new ServiceKu.PVData();
            //data[1] = new ServiceKu.PVData();

            //int i = service1.getPVCount(data);
            //litPVCount.Text = i.ToString();


            //ServiceKu.PVDetailData[] dd = new ServiceKu.PVDetailData[1];
            ServiceKu1.PVDetailData[] dd = new ServiceKu1.PVDetailData[1];

            dd[0] = new ServiceKu1.PVDetailData();
            dd[0].Amount = 999999;
            dd[0].CostCenterCode = "AA2000";
            dd[0].CurrencyCode = "IDR";
            dd[0].Description = "Gaji Karyawan";
            dd[0].InvoiceNumber = "INV00001";
            //dd[0].ItemTransaction = 2;
            //dd[0].TaxCode = null;
            //dd[0].TaxNumber = null;
            dd[0].WbsNumber = "WBS123456";

            //dd[1] = new ServiceKu.PVDetailData();
            //dd[1].CostCenterCode = "cost";

            //service2.PVSave("C", "0000001056", 1, 2, "1", DateTime.Now, DateTime.Now, "5", DateTime.Now, 1, 999, true, false, false, dd);
            
            //service1.PVSave("C", "000000156", 1, 2, "1", DateTime.Now, DateTime.Now, "5", DateTime.Now, 1, 999, true, false, false, dd);
            
        }

        public ServiceKu1.PVOutput test1()
        {
            
            ServiceKu1.ELVISServices service2 = new ServiceKu1.ELVISServices();
            ServiceKu1.PVDetailData[] dd = new ServiceKu1.PVDetailData[1];

            dd[0] = new ServiceKu1.PVDetailData();
            dd[0].Amount = 00006331691;
            //dd[0].CostCenterCode = "AA2000";
            dd[0].CurrencyCode = "IDR";
            //dd[0].Description = "Gaji Karyawan";
            //dd[0].InvoiceNumber = "INV00001";
            //dd[0].ItemTransaction = 2;
            //dd[0].TaxCode = null;
            //dd[0].TaxNumber = null;
            //dd[0].WbsNumber = "WBS123456";
            ServiceKu1.PVOutput output = new ServiceKu1.PVOutput();
            //output = service2.PVSave("C", "0000001056", 1, 2, "1", "", "", "8", DateTime.Now, 1, 999, true, false, false, dd);
            output = service2.PVSave("T", "500013", 1, 106, null, "", "", "8", DateTime.Now, 1, 12012, 6, "SAP_HR", null, false, false, true, dd);
            litPVCount.Text = output.PV_NO.ToString() + "<br/> " + output.PV_YEAR.ToString() + "<br/> " + output.STATUS_PV.ToString() + "<br/>" + output.ERR_MESSAGE.ToString();
            return output;
        }

        protected void btn_test_Click(object sender, EventArgs e)
        {
            test1();

        }
    }
}