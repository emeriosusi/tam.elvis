﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ELVISDashBoard.presentationHelper;
using BusinessLogic;
using Common;
using Common.Data;
using Common.Function;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Data.Linq;

namespace ELVISDashBoard
{
    public partial class FBCJ : BaseCodeBehind
    {
        protected FBCJLogic lo = new FBCJLogic();
        private char[] cta = new char[] { 'P', 'R' };

        private readonly string CAJO = "FBCJ_CAJO";
        protected ScreenHelper sh = null;
        
        protected CashJournalData CaJo 
        {
            get
            {
                if (Session[CAJO] == null)
                    return null;
                else
                    return Session[CAJO] as CashJournalData;
            }

            set
            {
                Session[CAJO] = value;
            }
        } 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
                Response.Redirect("~/Default.aspx");

            lit = litMessage;
            sh = new ScreenHelper("FBCJ", Session, litMessage, hidExpandFlag);

            if (!IsCallback && !IsPostBack)
            {
                coCashJournal.SelectedIndex = 0;
                calDateFrom.Date = DateTime.Now;
                calDateTo.Date = DateTime.Now;
                SetFirstLoad();
                reload();
            }
        }
        protected void reload(bool noWait=false)
        {
            CashJournalData cj = lo.Retrieve(coCashJournal.Value.str(),
                calDateFrom.Date, calDateTo.Date,UserName);
            CaJo = cj;

            
            if (cj.Export.LockStatus.Equals(CajoLock.Locked.str()) && !noWait)
            {
                PopLit.Text = "Data is Locked by another.<br/> Save/Post Disabled. <br/>Proceed Anyway? ";
                mopo.Show();
            }
            else
            {
                FillCajo(cj);
            }
        }

        protected void PopYes_Click(object sender, EventArgs e)
        {
            FillCajo(CaJo);
        }

        protected void coCashJournal_TextChanged(object sender, EventArgs e)
        {
            string v = (sender as ASPxComboBox).Value.str();
            LoggingLogic.say(UserName, "Cash Journal : {0}", v);
            bool frac = !AppSetting.RoundCurrency.Contains(v);
            SetCurr(v);

            GridViewDataTextColumn c = ReceiptGrid.Columns["Amount"] as GridViewDataTextColumn;

            if (c != null) 
                c.PropertiesTextEdit.DisplayFormatString = (frac) ? "{0:N2}" : "{0:N0}";
            c = PaymentGrid.Columns["Amount"] as GridViewDataTextColumn;
            if (c != null)
                c.PropertiesTextEdit.DisplayFormatString = (frac) ? "{0:N2}" : "{0:N0}";

            ApplyMask(OpenBalanceText, frac);
            ApplyMask(TotalCashReciptText, frac);
            ApplyMask(TotalCheckReceiptsText, frac);
            ApplyMask(TotalCashPaymentsText, frac);
            ApplyMask(ClosingBalanceText, frac);
            ApplyMask(CashText, frac);
            
            reload();
        }

        protected void CashJournalDropDown_TextChanged(object sender, EventArgs e)
        {
            GridViewDataTextColumn c = ReceiptGrid.Columns["Amount"] as GridViewDataTextColumn;
            
            if (AppSetting.RoundCurrency.Contains((sender as DropDownList).SelectedValue.str()))
            {
                if (c != null) c.PropertiesTextEdit.DisplayFormatString = "{0:N0}";
            }
            else
            {
                if (c != null) c.PropertiesEdit.DisplayFormatString = "{0:N2}";
            }
            reload();
        }       

        protected void FillCajo(CashJournalData cj)
        {
            CashJournalExportData e = cj.Export;
            bool frac = !AppSetting.RoundCurrency.Contains(coCashJournal.Value.str());

            OpenBalanceText.Text = e.OpeningBalance.SAPnum(frac);
            TotalCashReciptText.Text = e.TotalCashReceipt.SAPnum(frac);
            CashReceiptsNumber.Text = e.NumberCashReceipt.str();
            TotalCheckReceiptsText.Text = e.TotalCheckReceipt.SAPnum(frac);
            CheckReceiptsNumber.Text = e.NumberCheckReceipt.str();

            TotalCashPaymentsText.Text = e.TotalCashPayment.SAPnum(frac);
            CashPaymentsNumber.Text = e.NumberCashPayment.str();
            ClosingBalanceText.Text = e.ClosingBalance.SAPnum(frac);
            CashText.Text = e.Cash.SAPnum(frac);

            PaymentGrid.DataSource = null;


            PaymentGrid.DataSource = CaJo.CashPayments;
            PaymentGrid.DataBind();

            ReceiptGrid.DataSource = null;

            ReceiptGrid.DataSource = CaJo.CashReceipts;
            ReceiptGrid.DataBind();
        }

        protected void SetCurr(string curr)
        {
            OpenBalanceCurr.Text = curr;
            TotalCashReceiptCurr.Text = curr;
            TotalCheckReceiptsCurr.Text = curr;
            TotalCashPaymentsCurr.Text = curr;
            ClosingBalanceCurr.Text = curr;
            CashCurr.Text = curr;
        }

        protected void SetMode(bool Editing = true)
        {
            AddButton.Visible = !Editing;            
            
            DeleteButton.Visible = !Editing;
            PostButton.Visible = !Editing;
            ReverseButton.Visible = !Editing;

            SaveButton.Visible = Editing;
            CancelButton.Visible = Editing;

            calDateFrom.ReadOnly = Editing;
            calDateTo.ReadOnly = Editing;
            calDateFrom.DropDownButton.Visible = !Editing;
            calDateTo.DropDownButton.Visible = !Editing;

            PrevDayButton.Enabled = !Editing;
            DayButton.Enabled = !Editing;
            NextDayButton.Enabled = !Editing;

            PrevWeekButton.Enabled = !Editing;
            WeekButton.Enabled = !Editing;
            NextWeekButton.Enabled = !Editing;

            PrevPeriodButton.Enabled = !Editing;
            PeriodButton.Enabled = !Editing;
            NextPeriodButton.Enabled = !Editing;

            coCashJournal.Enabled = !Editing;
        }

        protected void SetFirstLoad()
        {
            SetMode(false);
        }

        protected void SetCancelEditDetail()
        {
            int ix = JournalPages.ActiveTabIndex;
            switch (ix)
            {
                case 0: PaymentGrid.CancelEdit(); break;
                case 1: ReceiptGrid.CancelEdit(); break;
            }
            
            SetMode(false);
        }


        protected void SetEditDetail()
        {
            SetMode(true);
        }

        protected void ReloadButton_Click(object sender, EventArgs e)
        {
            LoggingLogic.say(UserName, "ReloadButton_Click");
            reload();
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            LoggingLogic.say(UserName, "AddButton_Click");
            int ix = JournalPages.ActiveTabIndex;
            switch (ix)
            {
                case 0: PaymentGrid.AddNewRow(); break;
                case 1: ReceiptGrid.AddNewRow(); break;
            }
            
            PageMode = Common.Enum.PageMode.Add;
            SetEditDetail();
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            LoggingLogic.say(UserName, "CancelButton_Click");
            SetCancelEditDetail();
        }

        protected void Act(CajoTrans t)
        {
            /// gather selected records 
            int ix = JournalPages.ActiveTabIndex;

            /// gather selected records 
            List<CashJournalTable> l = Gather(ix);
            if (l == null || l.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            LoggingLogic.say(UserName, "Act " + t.ToString() + " " + ix.str());
            /// Execute command
            DoIt(ix, t, l);
        }

        protected void DoIt(int ix, CajoTrans t, List<CashJournalTable> l)
        {
            CajoCash cc = (CajoCash)cta[ix];
            if (l == null)
                return;

            CashJournalImportData i = new CashJournalImportData()
            {
                CompanyCode = "TAM",

                CashJournal = coCashJournal.Value.str(),
                CashType = cta[ix].str(),
                TransType = ((int) t).str()
            };

            if (t == CajoTrans.Reverse)
            {
                string b = ReverseReason.Value.str();
                foreach (var lr in l)
                {
                    lr.Reason = b;                         
                }
            }
            try
            {
                List<CashJournalMessage> mx = lo.Maintain(i, l, UserName);
                var qempty = mx.Where(m => m.InternalDocNo == null || m.InternalDocNo.Length < 1);
                string emsg = "MSTD00001INF";
                if (qempty.Any())
                {
                    emsg = "MSTD00002ERR";
                } 
                
                if (lo.LastMesssages != null) {
                    StringBuilder x = new StringBuilder("");
                    for (int j = 0; j < lo.LastMesssages.Count; j++)
                    {
                        CashJournalMessage m = lo.LastMesssages[j];
                        x.AppendFormat("{0}:{1}<br/>\r\n", m.Reference, m.Message);
                    }
                    
                    Nag(emsg, x.ToString());
                }
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }

            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
            reload();
        }

        protected List<CashJournalTable> Gather(int ix)
        {
            ASPxGridView g=null;
            List<CashJournalTable> l = new List<CashJournalTable>();
           
            switch (ix)
            {
                case 0: g = PaymentGrid; break;
                case 1: g = ReceiptGrid; break;
                default: return null;
            }
            for (int i = 0; i < g.VisibleRowCount; i++)
            {
                if (g.Selection.IsRowSelected(i))
                {
                    string re = g.GetRowValues(i, "Reference").str();
                    string bt = g.GetRowValues(i, "BusinessTransaction").str();
                    decimal am = g.GetRowValues(i, "Amount").Dec();
                    string te = g.GetRowValues(i, "Text").str();
                    string ve = g.GetRowValues(i, "Vendor").str();
                    string cu = g.GetRowValues(i, "Customer").str();
                    string id = g.GetRowValues(i, "InternalDocNo").str();

                    l.Add(new CashJournalTable()
                    {
                        InternalDocNo = id,
                        Reference = re,
                        BusinessTransaction = bt,
                        Amount = am,
                        Text = te,
                        Vendor = ve,
                        Customer = cu,
                        
                    });
                }
            }
            return l;
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            LoggingLogic.say(UserName, "DeleteButton_Click");
            Act(CajoTrans.Delete);
        }

        protected void PostButton_Click(object sender, EventArgs e)
        {
            LoggingLogic.say(UserName, "PostButton_Click");
            Act(CajoTrans.Post);
        }

        protected void popReasonGo_Click(object sender, EventArgs e)
        {
            LoggingLogic.say(UserName, "popReasonGo_Click");
            Act(CajoTrans.Reverse);
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            int ix = JournalPages.ActiveTabIndex;

            ASPxGridView g = null;

            switch (ix)
            {
                case 0: g = PaymentGrid; break;
                case 1: g = ReceiptGrid; break;
            }
            LoggingLogic.say(UserName, "SaveButtonButton_Click {0}", ix);
            ASPxComboBox cBusinessTransaction;
            ASPxTextBox tAmount;
            TextBox tText;
            ASPxGridLookup loVendor;
            ASPxGridLookup loCustomer;
            TextBox tReference;

            string [][] aNames = new string[2][] { 
                new string [6] {"cpBusinessTransaction","pAmount","pText","lopVendor","lopCustomer", "pReference"},
                new string [6] {"crBusinessTransaction","rAmount","rText","lorVendor","lorCustomer", "rReference"}
            };

            cBusinessTransaction =
                g.FindEditRowCellTemplateControl(
                g.Columns["BusinessTransaction"] as GridViewDataColumn,
                aNames[ix][0]
                ) as ASPxComboBox;

            tAmount =
                g.FindEditRowCellTemplateControl(
                g.Columns["Amount"] as GridViewDataColumn,
                aNames[ix][1]) as ASPxTextBox;

            tText =
                g.FindEditRowCellTemplateControl(
                g.Columns["Text"] as GridViewDataColumn,
                aNames[ix][2]) as TextBox;
            loVendor =
                g.FindEditRowCellTemplateControl(
                g.Columns["Vendor"] as GridViewDataColumn,
                aNames[ix][3]) as ASPxGridLookup;

            loCustomer =
                g.FindEditRowCellTemplateControl(
                g.Columns["Customer"] as GridViewDataColumn,
                aNames[ix][4]) as ASPxGridLookup;

            tReference =
                g.FindEditRowCellTemplateControl(
                g.Columns["Reference"] as GridViewDataColumn,
                aNames[ix][5]) as TextBox;

            if (PageMode == Common.Enum.PageMode.Add)
            {
                string bt = cBusinessTransaction.Value.str();
                string v = loVendor.Value.str();
                string c = loCustomer.Value.str();
                CashJournalTable t = new CashJournalTable()
                {
                    BusinessTransaction = bt,
                    Amount = tAmount.Text.Dec(),
                    Text = tText.Text,
                    Vendor = v,
                    Customer = c,
                    Reference = tReference.Text
                    
                };
                List<CashJournalTable> l = new List<CashJournalTable>();
                l.Add(t);
                
                DoIt(ix, CajoTrans.Save, l);
            }
        }


        protected void DayButton_Click(object sender, EventArgs e)
        {
            MoveDay(0);
            reload();
        }

        protected void PrevDayButton_Click(object sender, EventArgs e)
        {
            MoveDay(-1);
            reload();
        }

        protected void NextDayButton_Click(object sender, EventArgs e)
        {
            MoveDay(1);
            reload();
        }

        protected void MoveDay(int diff)
        {
            DateTime d;

            if (diff < 0)
                d = calDateFrom.Date;
            else if (diff > 0)
                d = calDateTo.Date;
            else
                d = DateTime.Now;

            DateTime d1 = d.AddDays(diff);
            
            calDateFrom.Date = d1;
            calDateTo.Date = d1;
        }

        protected void MoveWeek(int diff)
        {
            DateTime d;

            if (diff < 0)
            {
                d = calDateFrom.Date;
                d = d.AddDays(-1 * (int)d.DayOfWeek).AddDays(-1);
            }
            else if (diff > 0)
                d = calDateTo.Date.AddDays(1);
            else
                d = DateTime.Now;

            DateTime w0, w1;            

            w0 = d.AddDays(-1*(int) d.DayOfWeek).AddDays(1);
            w1 = w0.AddDays(6);

            calDateFrom.Date = w0;
            calDateTo.Date = w1;            
        }

        protected void MovePeriod(int diff)
        {
            DateTime d;

            if (diff < 0)
                d = calDateFrom.Date.AddDays(-1);
            else if (diff > 0)
                d = calDateTo.Date.AddDays(1);
            else
                d = DateTime.Now;

            DateTime p0, p1;

            p0 = d.AddDays(-1* ((int)d.Day-1));
            p1 = p0.AddMonths(1).AddDays(-1);

            calDateFrom.Date = p0;
            calDateTo.Date = p1;            
        }

        protected void PrevWeekButton_Click(object sender, EventArgs e)
        {
            MoveWeek(-1);
            reload();
        }
        protected void WeekButton_Click(object sender, EventArgs e)
        {
            MoveWeek(0);
            reload();
        }
        protected void NextWeekButton_Click(object sender, EventArgs e)
        {
            MoveWeek(1);
            reload();
        }
        protected void PrevPeriodButton_Click(object sender, EventArgs e)
        {
            MovePeriod(-1);
            reload();
        }
        protected void PeriodButton_Click(object sender, EventArgs e)
        {
            MovePeriod(0);
            reload();
        }
        protected void NextPeriodButton_Click(object sender, EventArgs e)
        {
            MovePeriod(1);
            reload();
        }


        protected void Grid_HtmlRowCreated(object sender,  ASPxGridViewTableRowEventArgs e)
        {
            
                LoggingLogic.say(UserName, "Grid_HtmlRowCreated");

                ASPxGridView  g =  sender as ASPxGridView;
                
                if (g!=null) 
                {
                    string x = "r";
                    if (g.ID.StartsWith("P")) {
                        x = "p";
                    }
                    
                    ASPxTextBox t = g.FindEditRowCellTemplateControl(g.Columns["Amount"] as GridViewDataColumn, x + "Amount") as ASPxTextBox;
                    LoggingLogic.say(UserName, "t {0}{1}", x, "Amount", (t == null) ? "NotExists" : "Exists");
                    bool frac = !AppSetting.RoundCurrency.Contains(coCashJournal.Value.str());
                    LoggingLogic.say(UserName, "Grid_HtmlRowCreated cj={0} frac={1}", coCashJournal.Value, frac);
                    if (t!=null)
                        ApplyMask(t, frac);
                }
            
        }

        protected void PaymentGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            FmtDocStat(e, "DocStatPayment");
        }

        protected void FmtDocStat(ASPxGridViewTableDataCellEventArgs e, string controlName)
        {
            if (e.DataColumn.FieldName == "DocumentStatus")
            {
                Literal l = e.Cell.FindControl(controlName) as Literal;
                if (l != null)
                {
                    char v= e.CellValue.str()[1];
                    CajoStatus cs = (CajoStatus)v;
                    l.Text = string.Format("<div class=\"cajo{0}\">&nbsp;</div>", cs.ToString());
                }
            }
            else if (e.DataColumn.FieldName == "Amount")
            {
                LoggingLogic.say(UserName, "FmtDocStat Amount");
            }
        }

        protected void Grid_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "DocumentStatus")
            {
                string v = e.Value.str();
                char c;
                CajoStatus cj = CajoStatus.None;

                char[] aStatus = new char[] 
                    { 
                      (char) CajoStatus.Save, 
                      (char) CajoStatus.Post, 
                      (char) CajoStatus.Reverse, 
                      (char) CajoStatus.Delete 
                    };

                if (!v.isEmpty())
                {
                    c = v[0];
                    if (aStatus.Contains(c))
                    {
                        cj = (CajoStatus) c;
                        v = cj.ToString();
                    }
                }

                e.DisplayText = string.Format("<div class=\"cajo{0} hint--right\" data-hint=\" {1} \" >&nbsp;</div>", e.Value, v);
            }

        }

        protected void Grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            LoggingLogic.say(UserName, "CellEditorInitialize field = {0}", e.Column.FieldName);
            if (e.Column.FieldName == "Amount")
            {
                ASPxTextBox lo = e.Editor as ASPxTextBox;
                bool frac = !AppSetting.RoundCurrency.Contains(coCashJournal.Value.str());
                LoggingLogic.say(UserName, "CellEditorInitialize cj={0} frac={1}", coCashJournal.Value, frac);
                ApplyMask(lo, frac);
            }
        }

        protected void ApplyMask(ASPxTextBox ed, bool frac)
        {
            ed.MaskSettings.Mask = "<0..9999999999999g>" + (frac ? ".<00..99>" : "");
        }

        protected void ReceiptGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            FmtDocStat(e, "DocStatReceipt");
        }

        protected void dsVendor_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = lo.getVendorCodes();
        }

        protected void dsCustomer_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = lo.getCustomerCodes();
        }
        
        protected void dsBusinessTransactionPay_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = lo.GetBusinessTransactionPay();
        }

        protected void dsBusinessTransactionReceipt_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = lo.GetBusinessTransactionReceipt();
        }

        protected void dsReason_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = lo.GetReason();
        }

    }
}