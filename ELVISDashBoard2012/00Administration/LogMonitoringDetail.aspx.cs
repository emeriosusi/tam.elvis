﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Data;
using Common.Function;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using ELVISDashBoard.presentationHelper;
namespace ELVISDashboard.Admin
{
    public partial class LogMonitoringDetail : BaseCodeBehind
    {
        private string _procId;
        private string _startDT;
        private string _endDT;
        private string _procStat;
        private string _userID;
        private string _funcName;
        
        private string _ScreenID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            if (!IsPostBack)
            {
                PrepareLogout();
            }

            _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_0004").ToString();
            hidScreenID.Value = _ScreenID;

            _procId = Request.QueryString["process_id"];
            _startDT = Request.QueryString["startDT"];
            _endDT = Request.QueryString["endDT"];
            _procStat = Request.QueryString["procStat"];
            _userID = Request.QueryString["userID"];
            _funcName = Request.QueryString["funcName"];
            txtStartDT.Text = _startDT;
            txtProcessID.Text = _procId;
            txtEndDT.Text = _endDT;
            if (_procStat == "0")
                txtStatus.Text = "Success";
            else if (_procStat == "1")
                txtStatus.Text = "Business Error";
            else if (_procStat == "2")
                txtStatus.Text = "System Error";
            else
                txtStatus.Text = "";
            txtUserID.Text = _userID;
            if (_funcName != "")
                txtFunctName.Text = _funcName;
            ASPxGridView1.DataBind();
            btnClose.OnClientClick = @"closeWindow('0')";
        }
        #region SetMessage()
        private void setMessage(string Message, string MessageID)
        {
            #region
            if (Message == "")
            {
                litOpenDiv.Text = "";
                litMessage.Text = Message;
                imgMessageError.Visible = false;
                imgMessageSuccess.Visible = false;
                LitCloseDiv.Text = "";
            }
            else
            {
                if (MessageID == "")//Error Unhandled execption
                {
                    litOpenDiv.Text = "<Div class=\"ERR\">";
                    LitCloseDiv.Text = @"</ div>";
                    imgMessageError.Visible = true;
                    imgMessageSuccess.Visible = false;
                    litMessage.Text += Message;
                }
                else
                {
                    string MessageType = MessageID.Substring(MessageID.Length - 3, 3);//ambil 3 digit terakhir
                    if (MessageType.ToUpper() == "ERR")
                    {
                        litOpenDiv.Text = "<div class=\"ERR\">";
                        LitCloseDiv.Text = @"</ div>";
                        imgMessageError.Visible = true;
                        imgMessageSuccess.Visible = false;
                        litMessage.Text += Message;

                    }
                    else
                    {
                        litOpenDiv.Text = "<Div class=\"INF\">";
                        LitCloseDiv.Text = @"</ div>";
                        imgMessageSuccess.Visible = true;
                        imgMessageError.Visible = false;
                        litMessage.Text += Message;
                    }
                }
            }
            #endregion
        }
        #endregion
        protected void ldsLog_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            BusinessLogic.Admin.LogMonitoringDetailLogic log = new BusinessLogic.Admin.LogMonitoringDetailLogic();
            //ASPxGridView1.DataSourceID = ldsLog.ID;
            e.Result = log.searchLog(_procId);
        }
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string StartDT = txtStartDT.Text;
            string EndDT = txtEndDT.Text;
            string UserID = txtUserID.Text;
            string ProcessID = txtProcessID.Text;
            string Status = txtStatus.Text;
            string FunctionName = txtFunctName.Text;
            ErrorData ed = new ErrorData();

            string _strFileName = "DownloadLogDetail_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            BusinessLogic.Admin.LogMonitoringDetailLogic _logMonitoringDetail = new BusinessLogic.Admin.LogMonitoringDetailLogic();
            List<LogDetailData> _tempLMD = new List<LogDetailData>();
            _tempLMD = _logMonitoringDetail.newLogDetailList(ProcessID, ref ed);
            if (ed != null)
            {
                if (!string.IsNullOrEmpty(ed.ErrMsgID))
                {
                    litMessage.Text = resx.Message( ed.ErrMsgID).ToString();
                    
                    setMessage(litMessage.Text, ed.ErrMsgID);
                }
                if (ed.ErrMsgID == "")
                {
                    litMessage.Text = ed.ErrMsg;
                    if (ed.ErrID > 0) litMessage.Text = ed.ErrMsg; 
                    setMessage(litMessage.Text, "MSTD00109ERR");
                }
                else litMessage.Text = "";
            }
            //int ProcID = 0;
            if (_tempLMD != null)
            {
                
                string _strBuilderValue = _logMonitoringDetail.getLogDetail(StartDT, EndDT, ProcessID, Status, UserID, FunctionName);
                StringBuilder _stbExport = new StringBuilder();
                _stbExport.Append(_strBuilderValue);
                try
                {
                    this.Response.Clear();
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _strFileName));
                    this.Response.ContentType = "application/vnd.ms-excel";


                    this.Response.Write(_stbExport);
                    _stbExport.Clear();
                    this.Response.End();
                }
                catch (System.Threading.ThreadAbortException ex)
                {
                    if (ex.Message.StartsWith("Thread") == false)
                    {
                        litMessage.Text = _m.Message("MSTD00110ERR", _strFileName, ex.Message);
                        setMessage(litMessage.Text, "MSTD00110ERR");
                    }
                }

            }
            else
            {

                litMessage.Text = string.Format(resx.Message( "MSTD00018INF"));

                setMessage(litMessage.Text, "MSTD00018INF");
            }


        }
    }
}