﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using DataLayer.Model;
using DevExpress.Web.ASPxGridView;
using Common.Data;
using Common.Data.Common.Data;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard.Administration
{
    public partial class LogMonitoring : BaseCodeBehind
    {
        private string _ScreenID;

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            _ScreenID = resx.Screen("ELVIS_Screen_0003").ToString();
            hidScreenID.Value = _ScreenID;

            if (!IsPostBack)
            {
                PrepareLogout();
                List<object> FunctionList = resx.getFunctionList();
                System.Collections.DictionaryEntry Data = new System.Collections.DictionaryEntry();
                FunctionList.Insert(0, Data);
                ddlFunctionName.DataSource = FunctionList;
                ddlFunctionName.DataTextField = "key";
                ddlFunctionName.DataValueField = "value";
                ddlFunctionName.DataBind();
                List<ErrorStatusData> StatusList = new ErrorStatusLogic().GetErrorStatusList();
                ddlStatus.DataSource = StatusList;
                ddlStatus.DataTextField = "Status_Name";
                ddlStatus.DataValueField = "Status_CD";
                ddlStatus.DataBind();
                List<string> userIDList = new BusinessLogic.Admin.LogMonitoringLogic().getUserID();
                ddlUserID.DataSource = userIDList;
                ddlUserID.DataBind();
                List<string> rppList = new BusinessLogic.Admin.LogMonitoringLogic().listRecordPerPage();
                ddlRecPerPage.DataSource = rppList;
                ddlRecPerPage.DataBind();
                ASPxGridView1.DataSourceID = null;
                ASPxGridView1.DataBind();

                btnClose.OnClientClick = @"closeWindow('0')";

                AddEnterComponent(ddlStatus);
                AddEnterComponent(ddlFunctionName);
                AddEnterComponent(ddlUserID);
                AddEnterComponent(ddlRecPerPage);
                AddEnterAsSearch(btnSearch, _ScreenID);
            }
        }
        #region SetMessage()
        private void setMessage(string Message, string MessageID)
        {
            #region
            if (Message == "")
            {
                litOpenDiv.Text = "";
                litMessage.Text = Message;
                imgMessageError.Visible = false;
                imgMessageSuccess.Visible = false;
                LitCloseDiv.Text = "";
            }
            else
            {
                if (MessageID == "")//Error Unhandled execption
                {
                    litOpenDiv.Text = "<Div class=\"ERR\">";
                    LitCloseDiv.Text = @"</ div>";
                    imgMessageError.Visible = true;
                    imgMessageSuccess.Visible = false;
                    litMessage.Text += Message;
                }
                else
                {
                    string MessageType = MessageID.Substring(MessageID.Length - 3, 3);//ambil 3 digit terakhir
                    if (MessageType.ToUpper() == "ERR")
                    {
                        litOpenDiv.Text = "<div class=\"ERR\">";
                        LitCloseDiv.Text = @"</ div>";
                        imgMessageError.Visible = true;
                        imgMessageSuccess.Visible = false;
                        litMessage.Text += Message;

                    }
                    else
                    {
                        litOpenDiv.Text = "<Div class=\"INF\">";
                        LitCloseDiv.Text = @"</ div>";
                        imgMessageSuccess.Visible = true;
                        imgMessageError.Visible = false;
                        litMessage.Text += Message;
                    }
                }
            }
            #endregion
        }
        #endregion

        protected void ldsLog_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            BusinessLogic.Admin.LogMonitoringLogic log = new BusinessLogic.Admin.LogMonitoringLogic();
            if (calStartDate.Date != "" && calEndDate.Date != "")
            {
                e.Result = log.searchLog(calStartDate.Date_Format_ddMMyyyy, calEndDate.Date_Format_ddMMyyyy, ddlStatus.SelectedValue.ToString(), ddlFunctionName.SelectedValue.ToString(), txtProcessID.Text, ddlUserID.SelectedValue.ToString());
                object FunctionList = resx.getFunctionList();
            }
            else
            {
                e.Result = null;
                ASPxGridView1.DataSourceID = null;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (calStartDate.Date == "" && calEndDate.Date == "")
            {
                setMessage(string.Format(resx.Message("MSTD00017WRN").ToString(), "Log period"), "MSTD00017WRN");
            }
            else if (calStartDate.Date == "" || calEndDate.Date == "")
            {
                setMessage(string.Format(resx.Message("MSTD00017WRN").ToString(), "Starting/End Date"), "MSTD00017WRN");
            }
            else
            {
                setMessage("", "");
                ASPxGridView1.SettingsPager.Mode = GridViewPagerMode.ShowPager;
                ASPxGridView1.SettingsPager.PageSize = Convert.ToInt32(ddlRecPerPage.SelectedValue.ToString());
                ASPxGridView1.DataSourceID = ldsLog.ID;
                ASPxGridView1.DataBind();
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            calStartDate.Date = "";
            calEndDate.Date = "";
            txtProcessID.Text = "";
            if (ddlFunctionName.Items.Count > 0)
            {
                ddlFunctionName.SelectedIndex = 0;
            }
            if (ddlStatus.Items.Count > 0)
            {
                ddlStatus.SelectedIndex = 0;
            }
            if (ddlUserID.Items.Count > 0)
            {
                ddlUserID.SelectedIndex = 0;
            }
            if (ddlRecPerPage.Items.Count > 0)
            {
                ddlRecPerPage.SelectedIndex = 0;
            }
            ASPxGridView1.DataSourceID = null;
            ASPxGridView1.DataBind();
        }
        protected void ddlRecordPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRecPerPage.Items != null)
            {
                ASPxGridView1.SettingsPager.Mode = GridViewPagerMode.ShowPager;
                ASPxGridView1.SettingsPager.PageSize = Convert.ToInt32(ddlRecPerPage.SelectedValue.ToString());
            }
            else
            {
                ASPxGridView1.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            }
        }

        protected void ASPxGridView1_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
            {
                Literal litGridFunctionName = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridView1.Columns["function_id"] as GridViewDataColumn, "litGridFunctionName") as Literal;
                string functID = e.GetValue("function_id").ToString();
                List<object> Error = resx.getFunctionList();
                foreach (object obj in Error)
                {
                    System.Collections.DictionaryEntry Data = (System.Collections.DictionaryEntry)obj;
                    if (Data.Value.ToString() == functID)
                    {
                        string Value = "";
                        Value = functID.ToString() + ":" + Data.Key;
                        litGridFunctionName.Text = Value;
                        break;
                    }
                }
                if (litGridFunctionName.Text == "")
                {
                    litGridFunctionName.Text = functID;
                }
                string proccID = e.GetValue("process_id").ToString();
                string startDT = e.GetValue("process_dt").ToString();
                string endDT = e.GetValue("end_dt").ToString();
                string procStat = e.GetValue("process_sts").ToString();
                string userID = e.GetValue("user_id").ToString();
                Literal litProcessID = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)ASPxGridView1.Columns["process_id"] as GridViewDataColumn, "litProcessID") as Literal;
                string url = ResolveClientUrl("~/00Administration/LogMonitoringDetail.aspx?process_id=" + proccID + "&startDT=" + startDT + "&endDT=" + endDT + "&procStat=" + procStat + "&userID=" + userID + "&funcName=" + litGridFunctionName.Text);
                litProcessID.Text = "<a href=\"javascript:openWin('" + url + "','" + resx.Screen("ELVIS_Screen_0004").ToString() + "');\" style='color:white'>" + proccID + "</a>";

            }



        }
        protected void ASPxGridView1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if ((string)e.GetValue("process_sts") == "0")
            {
                e.Row.BackColor = System.Drawing.Color.Green;
            }
            else if ((string)e.GetValue("process_sts") == "1" || (string)e.GetValue("process_sts") == "2")
                e.Row.BackColor = System.Drawing.Color.Red;

        }
    }
}