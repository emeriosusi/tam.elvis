﻿<%@ Page Title="Announcement Form" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" 
         AutoEventWireup="true" CodeBehind="AnnouncementForm.aspx.cs" 
         Inherits="ELVISDashBoard._00Administration.AnnouncementForm" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %> 
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxCallback" tagprefix="dx1" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:HiddenField runat="server" ID="HiddenField1" Value="Power Of Attorney" /> 
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script> 
    <style type="text/css">        
        .dx-display-inline
        {
            vertical-align: middle;
            display: inline-table;
        }
        .header 
        {
            margin-top: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
            float: none;
        }
    </style>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Announcement Form" />    
    <br /><br />    
    <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static" Value="false"/>
    <asp:UpdatePanel runat="server" ID="pnupdateLitMessage">
        <ContentTemplate>                 
            <asp:Literal runat="server" ID="messageControl" Visible="false" EnableViewState="false"/>            
        </ContentTemplate>
    </asp:UpdatePanel>   
    <br />
    <asp:UpdatePanel runat="server" ID="pnupdateContentSection">
        <ContentTemplate>
            <div class="contentsection header">
                <table id="tblHeader" width="950px">
                    <tr>
                        <td valign="middle" style="">
                            <dx:ASPxLabel ID="ltTitle" runat="server" Text="Title"/>
                        </td>
                        <td valign="middle" style="">                    
                            <asp:UpdatePanel runat="server" ID="updTitle">
                                <ContentTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblColonTitle" CssClass="dx-display-inline" Text=":" />
                                    <dx:ASPxTextBox runat="server" ID="tboxTitle" CssClass="dx-display-inline" Width="50%"/>
                                </ContentTemplate>
                            </asp:UpdatePanel>                   
                        </td>                    
                        <td valign="middle" style="">
                            <dx:ASPxLabel ID="lblDivision" runat="server" Text="Division" />
                        </td>
                        <td valign="middle" style="">
                            <asp:UpdatePanel runat="server" ID="updDivision">
                                <ContentTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblColonDivision" CssClass="dx-display-inline" Text=":" />                                                                        
                                    <dx:ASPxGridLookup runat="server" ID="ddlDivision" ClientInstanceName="ddlDivision" 
                                                       CssClass="dx-display-inline" Width="40%" KeyFieldName="DivisionID"
                                                       OnLoad="evt_ddlDivision_onLoad" TextFormatString="{1}" 
                                                       SelectionMode="Multiple" EnableViewState="false">
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="40px"/>
                                            <dx:GridViewDataTextColumn Caption="Division ID" FieldName="DivisionID" Width="50px">
                                                <Settings AutoFilterCondition="Contains"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Division Name" FieldName="DivisionName" Width="500px">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                        </Columns>                   
                                    </dx:ASPxGridLookup>
                                </ContentTemplate>
                            </asp:UpdatePanel>                    
                        </td>    
                    </tr>
                    <tr>
                        <td valign="middle">
                            <dx:ASPxLabel ID="lblDescription" runat="server" Text="Description" />
                        </td>
                        <td valign="middle">
                            <asp:UpdatePanel runat="server" ID="updDescription">
                                <ContentTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblColonDescription" CssClass="dx-display-inline" Text=":" />
                                    <dx:ASPxMemo runat="server" ID="mmDescription" CssClass="dx-display-inline" Width="50%">
                                    </dx:ASPxMemo>
                                </ContentTemplate>
                            </asp:UpdatePanel>                    
                        </td>
                        <td valign="middle">
                            <dx:ASPxLabel ID="lblPosition" runat="server" Text="Position" />
                        </td>
                        <td valign="middle">
                            <asp:UpdatePanel runat="server" ID="updPosition">
                                <ContentTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblColonPosition" CssClass="dx-display-inline" Text=":" />
                                    <dx:ASPxGridLookup runat="server" ID="ddlPosition" ClientInstanceName="ddlPosition" 
                                                       CssClass="dx-display-inline" Width="40%" KeyFieldName="PositionID"
                                                       OnLoad="evt_ddlPosition_onLoad" TextFormatString="{1}" 
                                                       SelectionMode="Multiple" EnableViewState="false">
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="40px"/>
                                            <dx:GridViewDataTextColumn Caption="Position ID" FieldName="PositionID" Width="50px">
                                                <Settings AutoFilterCondition="Contains"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Position Name" FieldName="PositionName" Width="500px">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                        </Columns>                   
                                    </dx:ASPxGridLookup>
                                </ContentTemplate>
                            </asp:UpdatePanel>                    
                        </td>
                    </tr>
                    <tr> 
                        <td valign="middle">
                            <dx:ASPxLabel ID="lblReleaseDate" runat="server" Text="Release Date" />
                        </td>         
                         <td valign="middle">
                            <asp:UpdatePanel runat="server" ID="updReleaseDate">
                                <ContentTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblColonReleaseDate" CssClass="dx-display-inline" Text=":" />
                                    <dx:ASPxDateEdit runat="server" ID="dtReleaseDate" CssClass="dx-display-inline" Width="35%"
                                                     DisplayFormatString="dd MMM yyyy" EditFormatString="dd/MM/yyyy"/>
                                </ContentTemplate>
                            </asp:UpdatePanel>                    
                        </td>
                        <td valign="middle">
                            <dx:ASPxLabel ID="lblExpireDate" runat="server" Text="Expire Date" />
                        </td>         
                         <td valign="middle">
                            <asp:UpdatePanel runat="server" ID="updExpireDate">
                                <ContentTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblColonExpireDate" CssClass="dx-display-inline" Text=":" />
                                    <dx:ASPxDateEdit runat="server" ID="dtExpireDate" CssClass="dx-display-inline" Width="35%"
                                                     DisplayFormatString="dd MMM yyyy" EditFormatString="dd/MM/yyyy"/>
                                </ContentTemplate>
                            </asp:UpdatePanel>                    
                        </td>                     
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>  
    <div style="float: right; margin-top: 10px; margin-bottom: 20px;">
        <asp:UpdatePanel runat="server" ID="updHeaderButton">
            <ContentTemplate>
                <asp:Button runat="server" ID="btSave" Text="Save" OnClick="evt_btSave_Clicked"/>
                <asp:Button runat="server" ID="btClose" Text="Close" UseSubmitBehavior="false" 
                            OnClick="evt_btClose_onClick" OnClientClick="closeWin()"/>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="margin-top: 10px; margin-left: auto; margin-right: auto; width: 950px;">
        <asp:UpdatePanel runat="server" ID="updSearchResult">
            <ContentTemplate>
                <dx:ASPxGridView runat="server" ID="gridSearchResult" Width="950px" EnableViewState="false"
                                 KeyFieldName="Code" EnableCallBacks="false"
                                 OnPageIndexChanged="evtSearchResultTable_PageIndexChanged">
                    <Settings VerticalScrollableHeight="200" />
                    <SettingsBehavior AllowFocusedRow="true" AllowSort="false" AllowSelectByRowClick="false" 
                                      AllowDragDrop="false" EnableRowHotTrack="true"/>
                    <Styles>
                        <AlternatingRow Enabled="True"/>
                    </Styles>
                    <Columns>                        
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="40px" FixedStyle="Left">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="Code" Caption="Code" Width="50px" FixedStyle="Left">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Title" Caption="Title" Width="250px" FixedStyle="Left">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Description" Caption="Description" Width="458px" FixedStyle="Left">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CreatedBy" Caption="Creator" Width="150px" FixedStyle="Left">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ReleaseDate" Caption="Release Date" VisibleIndex="0" Width="135px">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                            <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy}"/>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="ExpireDate" Caption="Expire Date" VisibleIndex="0" Width="135px">
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
                            <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy}"/>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
