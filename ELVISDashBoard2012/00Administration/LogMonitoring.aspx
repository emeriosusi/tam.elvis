﻿<%@ Page Title="Log Monitoring Header" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" AutoEventWireup="true" CodeBehind="LogMonitoring.aspx.cs" Inherits="ELVISDashBoard.Administration.LogMonitoring" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register src="../UserControl/Calendar.ascx" tagname="Calendar" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Log Monitoring Header" /> 
    <asp:HiddenField runat="server" ID="hidScreenID" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/ParentChild.js" type="text/javascript"></script>
<asp:UpdatePanel runat="server" ID="upnlLitMessage">
<Triggers>
    <asp:AsyncPostBackTrigger ControlID ="btnSearch" />
</Triggers>
<ContentTemplate>
        <asp:Literal runat="server" ID="litOpenDiv"></asp:Literal>
            <asp:Image ID="imgMessageSuccess" runat="server" SkinID="successMessage" Visible="false" />
            <asp:Image ID="imgMessageError" runat="server" SkinID="errorMessage" 
            Visible="false" ImageAlign="Middle" />
            <asp:Literal runat="server" ID="litMessage" EnableViewState ="false"></asp:Literal>
        <asp:Literal runat="server" ID="LitCloseDiv"></asp:Literal>
</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID ="upnContent" runat = "server">
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnSearch" />
    <asp:AsyncPostBackTrigger ControlID="btnClear" />
</Triggers>
<ContentTemplate>
    <div class="contentsection">
        <div class="row">
            <label>Process Date*</label>
            <uc1:Calendar runat="server" ID="calStartDate" ValidationGroup="search" Required="false"/> To* <uc1:Calendar runat="server" ID="calEndDate" ValidationGroup="search" Required="false" />
            <div class="rowwrapMini">
                    <label>Proccess ID</label>
                <asp:TextBox runat="server" ID="txtProcessID"  ValidationGroup="search"></asp:TextBox>
                <cc1:MaskedEditExtender ID="MaskTxtProcessID" runat="server"
                TargetControlID="txtProcessID"
                Mask="9999"
                AutoComplete="false"
                MessageValidatorTip="true"
                OnFocusCssClass="MaskedEditFocus"
                OnInvalidCssClass="MaskedEditError"
                MaskType="Number"
                DisplayMoney="None"
                AcceptNegative="None"
                CultureName="id-ID"
                ErrorTooltipEnabled="True"
                PromptCharacter=" " />
            </div>
        </div>
        <div class="row">
            <label>Status</label>
            <asp:DropDownList ID="ddlStatus" runat="server"  ValidationGroup="search">
            </asp:DropDownList>
        </div>
        <div class="row">
            <label>Function name</label>
            <asp:DropDownList ID="ddlFunctionName" runat="server"  ValidationGroup="search">
            </asp:DropDownList>
            <div class="rowwrapMini">
                <label>User ID</label>    
                <asp:DropDownList ID="ddlUserID" runat="server"  ValidationGroup="search">
                </asp:DropDownList>
                <div style = "text-align:right;display:inline;float:right">
                <asp:Button runat="server" Text="Search" ID="btnSearch" onclick="btnSearch_Click"  ValidationGroup="search"
                OnClientClick="loading()"/>
                 <%--ValidationGroup="Search" onclick="btnSearch_Click"--%>
                </div>
            </div>        
        </div>
        <div class="row">         
            <label>Records/Page</label>    
            <asp:DropDownList ID="ddlRecPerPage" runat="server"  ValidationGroup="search" AutoPostBack="false" 
                onselectedindexchanged="ddlRecordPerPage_SelectedIndexChanged">
            </asp:DropDownList>
            <div style = "text-align:right;display:inline;float:right">
            <asp:Button runat="server" ID="btnClear" Text="Clear" onclick="btnClear_Click"/>
            <%--onclick="btnClear_Click" --%>
            </div>
        </div>
    
    </div>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>
    <div style = "clear:both"></div>
    <br />
    <%--<asp:UpdatePanel ID="upnGrid" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" />
    </Triggers>
    <ContentTemplate>--%>
    <div>
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"  Width="990px"
            DataSourceID="ldsLog" 
            onhtmlrowcreated="ASPxGridView1_HtmlRowCreated" 
            onhtmlrowprepared="ASPxGridView1_HtmlRowPrepared">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Proccess ID" VisibleIndex="0"  Width="200" FieldName="process_id">
                    <DataItemTemplate>
                        <asp:Literal runat="server" ID="litProcessID"></asp:Literal>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Function Name" VisibleIndex="1" Width="200" FieldName ="function_id">
                    <DataItemTemplate>
                        <asp:Literal runat="server" ID="litGridFunctionName"></asp:Literal>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Start Date Time" VisibleIndex="2" Width="120" FieldName ="process_dt">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="End Date Time" VisibleIndex="3" Width="120" FieldName = "end_dt">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="4" Width="120" FieldName = "process_sts">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="User ID" VisibleIndex="5" Width="120" FieldName = "user_id">
                </dx:GridViewDataTextColumn>
            </Columns>
            
            <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="300" />
            
        </dx:ASPxGridView>
        <asp:LinqDataSource ID="ldsLog" runat="server" 
            ContextTypeName="DataLayer.Model.ELVIS_DBEntities" EntityTypeName="" 
            Select="new (process_id, process_dt, function_id, process_sts, user_id, end_dt)" 
            TableName="TB_R_LOG_H" onselecting="ldsLog_Selecting">
        </asp:LinqDataSource>
    </div>
</ContentTemplate>
</asp:UpdatePanel>
    <div style = "clear:both"></div>
    <br />
    <div class="row">
        <div class="rowwarphalfRight2">
            <asp:Button runat="server" ID="btnClose" Text="Close"/>
            <%--onclick="btnClose_Click"--%>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
