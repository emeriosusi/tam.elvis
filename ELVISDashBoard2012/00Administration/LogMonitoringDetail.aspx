﻿<%@ Page Title="Log Monitoring Detail" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" AutoEventWireup="true" CodeBehind="LogMonitoringDetail.aspx.cs" Inherits="ELVISDashboard.Admin.LogMonitoringDetail" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register src="../UserControl/Calendar.ascx" tagname="Calendar" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Log Monitoring Header" /> 
    <asp:HiddenField runat="server" ID="hidScreenID" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

        <asp:Literal runat="server" ID="litOpenDiv"></asp:Literal>
            <asp:Image ID="imgMessageSuccess" runat="server" SkinID="successMessage" Visible="false" />
            <asp:Image ID="imgMessageError" runat="server" SkinID="errorMessage" Visible="false" ImageAlign="Middle" />
            <asp:Literal runat="server" ID="litMessage" EnableViewState ="false"></asp:Literal>
        <asp:Literal runat="server" ID="LitCloseDiv"></asp:Literal>

    <div class="contentsection">
        <div class="row">
            <label>Start Date Time</label>
            <asp:TextBox runat="server" ID="txtStartDT" Enabled="False"></asp:TextBox>
            <div class="rowwrapMini">
                    <label>Proccess ID</label>
                <asp:TextBox runat="server" ID="txtProcessID" Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <label>End Date Time</label>
            <asp:TextBox runat="server" ID="txtEndDT" Enabled="False"></asp:TextBox>
            <div class="rowwrapMini">
                    <label>Status</label>
                <asp:TextBox runat="server" ID="txtStatus" Enabled="False"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <label>User ID</label>
            <asp:TextBox runat="server" ID="txtUserID" Enabled="False"></asp:TextBox>
            <div class="rowwrapMini">
                <div style = "text-align:right;display:inline;float:right">
                <asp:Button runat="server" Text="Download" ID="btnDownload" onclick="btnDownload_Click"/>
                 <%--ValidationGroup="Search" onclick="btnSearch_Click"--%>
                </div>
            </div>        
        </div>
        <div class="row">         
            <label>Function Name</label>    
            <asp:TextBox runat="server" ID="txtFunctName" Enabled="False"></asp:TextBox>
        </div> 
    </div>
    <div style = "clear:both"></div>
    <br />
    <asp:UpdatePanel ID="upnGrid" runat="server">
    <%--<Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" />
    </Triggers>--%>
    <ContentTemplate>
    <div>
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"  Width="990px"
            DataSourceID="ldsLog" >
           <%-- onhtmlrowcreated="ASPxGridView1_HtmlRowCreated" 
            onhtmldatacellprepared="ASPxGridView1_HtmlDataCellPrepared"
            onhtmlrowprepared="ASPxGridView1_HtmlRowPrepared"--%>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Seq. No" VisibleIndex="0"  Width="100" FieldName="seq_no">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Message Date Time" VisibleIndex="1" Width="120" FieldName ="err_dt">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Location" VisibleIndex="2" Width="200" FieldName ="err_location">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Message Type" VisibleIndex="3" Width="120" FieldName = "msg_type">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Message ID" VisibleIndex="4" Width="120" FieldName = "msg_id">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Message Detail" VisibleIndex="5" Width="250" FieldName = "err_message">
                </dx:GridViewDataTextColumn>
            </Columns>
            
            <Settings ShowVerticalScrollBar="True" />
            
        </dx:ASPxGridView>
        <asp:LinqDataSource ID="ldsLog" runat="server" 
            ContextTypeName="DataLayer.Model.ELVIS_DBEntities" EntityTypeName="" 
            Select="new (seq_no, err_dt, err_location, msg_type, msg_id, err_message)" 
            TableName="TB_R_LOG_D" onselecting="ldsLog_Selecting">
        </asp:LinqDataSource>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    <div style = "clear:both"></div>
    <br />
    <div class="row">
        <div class="rowwarphalfRight2">
            <asp:Button runat="server" ID="btnClose" Text="Close"/>
            <%--onclick="btnClose_Click"--%>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
