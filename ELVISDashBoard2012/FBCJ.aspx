﻿<%@ Page Title="Cash Journal Posting" Language="C#" 
    MasterPageFile="~/MasterPage/GeneralMaster.Master" 
    AutoEventWireup="true" CodeBehind="FBCJ.aspx.cs" Inherits="ELVISDashBoard.FBCJ" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pre" runat="server">
    <link rel="Stylesheet" href="fbcj.css" type="text/css" />
    <link rel="Stylesheet" href="hint.min.css" type="text/css" />
    <script src="fbcj.js" type="text/javascript"></script>

    <script type="text/javascript">
        function ReverseButton_Click() {
            var gp = PaymentGrid;
            var gr = ReceiptGrid;
            var sel = 0;
            if (gr != undefined) {
                sel += gr.GetSelectedRowCount();
            }
            if (gp != undefined) {
                sel += gp.GetSelectedRowCount();
            }
            if (sel < 1) {

                $("#ReverseButtonDiv").addClass("hint--top hint--error hint--always");
                $("#ReverseButtonDiv").attr("data-hint", "Select Document first");
                 
                return false;
            } else {
                $("#ReverseButtonDiv").attr("data-hint", "");
                $("#ReverseButtonDiv").removeClass("hint--error hint--top hint--always");                
            }

            var a = $find("moRev");
            a.show();

            return false;
        }

        function AddButton_OnClick() {
            var ab = document.getElementById("AddButton");
            if (ab) {
                ab.click();
                showHideAdd(0);
            }
            return false;
        }

        function popReasonOk_Click() {
            var x = ReverseReason.GetText();
            
            if (x) {
                $("#ReasonMsgDiv").html("");
                $("#ReasonMsgDiv").attr("data-hint", "");
                $("#ReasonMsgDiv").removeClass("hint--bottom hint--error hint--always");
            }
            else {
                // $("#ReasonMsgDiv").html("Reverse Reason is mandatory");
                
                $("#ReasonMsgDiv").addClass("hint--bottom hint--error hint--always");
                $("#ReasonMsgDiv").attr("data-hint", "Reverse Reason is mandatory");
                // alert("Reverse Reason is mandatory");
                return false;
            }
            var p = $find('popReason');
            if (p!=null && p.length>0)
                p.hide();

            loading();

            var btn = document.getElementById('popReasonGo');
            if (btn!=null)
                btn.click();
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Cash Journal Posting" />
<div id="topwrap">
            <!-- -->

     <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hidExpandFlag" />
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel id="CriteriaPanel" runat="server">
    <Triggers>
    </Triggers>
    <ContentTemplate>
           
            <div id="criteria" class="boxwrap"> 
           
                <div class="title">Data Selection</div> 

                <div class="boxwrap">
                    <div class="title">Display Period</div>
                
                    <div class="line middle">
                        <div class="dateEntry">
                            <div class="lefty">
                                <dx:ASPxDateEdit ID="calDateFrom" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                    ClientInstanceName="calDateFrom" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                    UseMaskBehavior="true" >
                                    <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                        ErrorText="" />
                                    <ClientSideEvents DateChanged="OnCJDateChange" />
                                </dx:ASPxDateEdit>
                            </div>
                            <div class="to">-</div>
                            <div class="lefty">
                                <dx:ASPxDateEdit ID="calDateTo" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                    ClientInstanceName="calDateTo" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                    UseMaskBehavior="true" >
                                    <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                        ErrorText="">
                                    </ValidationSettings>
                                    <ClientSideEvents DateChanged="OnCJDateChange" />
                                </dx:ASPxDateEdit>
                            </div>      
                        </div>
                    </div>

                    <div class="line middle">
                        <asp:Button ID="PrevDayButton" runat="server"  ClientIDMode="Static" Text="<<" 
                            CssClass="prev" Width="50px" OnClick="PrevDayButton_Click" OnClientClick="loading();"/>
                        <asp:Button ID="DayButton" runat="server" ClientIDMode="Static" Text="Today" 
                            Width="120px" CssClass="period" OnClick="DayButton_Click" OnClientClick="loading();"/>
                        <asp:Button ID="NextDayButton" runat="server" ClientIDMode="Static" Text=">>" 
                            CssClass="next" Width="50px" OnClick="NextDayButton_Click" OnClientClick="loading();" />
                    </div>

                    <div class="line middle">
                        <asp:Button ID="PrevWeekButton" runat="server" ClientIDMode="Static" Text="<<" 
                            CssClass="prev" Width="50px" OnClick="PrevWeekButton_Click" OnClientClick="loading();"/>
                        <asp:Button ID="WeekButton" runat="server" ClientIDMode="Static" Text="ThisWeek" 
                            Width="120px" CssClass="period" OnClick="WeekButton_Click" OnClientClick="loading();"/>
                        <asp:Button ID="NextWeekButton" runat="server" ClientIDMode="Static" Text=">>" 
                            CssClass="next" Width="50px" OnClick="NextWeekButton_Click" OnClientClick="loading();"/>
                    </div>

                    <div class="line middle">
                        <asp:Button ID="PrevPeriodButton" runat="server" ClientIDMode="Static" Text="<<" 
                            CssClass="prev" Width="50px" OnClick="PrevPeriodButton_Click" OnClientClick="loading();"/>
                        <asp:Button ID="PeriodButton" runat="server" ClientIDMode="Static" Text="Current Period" 
                            Width="120px" CssClass="period" OnClick="PeriodButton_Click" OnClientClick="loading();"/>
                        <asp:Button ID="NextPeriodButton" runat="server" ClientIDMode="Static" Text=">>" 
                            CssClass="next" Width="50px" OnClick="NextPeriodButton_Click" OnClientClick="loading();"/>
                    </div>
                </div>

                <div class="line">
                    <span class="lbl">Cash Journal</span>
                    <div class="lefty">
                    <dx:ASPxComboBox ID="coCashJournal" runat="server" ClientIDMode="Static" Width="80px" OnTextChanged="coCashJournal_TextChanged">
                        <ClientSideEvents CloseUp="function(s,e) {
                             $('#ReloadButton').click(); }" />
                            
                        <Items>
                            <dx:ListEditItem Text="IDR" Value="IDR" />
                            <dx:ListEditItem Text="JPY" Value="JPY" />
                            <dx:ListEditItem Text="USD" Value="USD" />
                        </Items>
                    </dx:ASPxComboBox>
                    </div>
                    <div class="lefty hidden">
                        <asp:Button ID="ReloadButton" runat="server" ClientIDMode="Static" OnClick="ReloadButton_Click" Width="1px" Height="1px" OnClientClick="loading();"/>
                    </div> 
                </div>
                <div class="line">
                    <span class="lbl">Company Code
                    </span>
                    <asp:Literal ID="CompanyCodeLiteral" runat="server" Text="P.T. Toyota Astra Motor" />
                </div>          
            </div>

           
            <div id="info" class="boxwrap">

                <div class="title">
                    Balance display for display period
                </div>
                <div class="xline topline"><div id="filler" id="topline">&nbsp;</div></div>
                <div class="line">
                    <div class="infolabel">Opening Balance</div>
                    <div class="infotext">
                    
                        <dx:ASPxTextBox ID="OpenBalanceText" runat="server" ClientIDMode="Static" Width="150px" CssClass="num" HorizontalAlign="Right" ReadOnly="true">
                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                        </dx:ASPxTextBox>
                    </div>
                    <div class="infocurr">
                        <asp:Literal ID="OpenBalanceCurr" runat="server" ClientIDMode="Static">IDR</asp:Literal>
                    </div>
                </div>

                <div class="line">
                    <div class="infolabel">+ Total Cash Receipts</div>
                    <div class="infotext">
                        <dx:ASPxTextBox ID="TotalCashReciptText" runat="server" ClientIDMode="Static" Width="150px" CssClass="num" HorizontalAlign="Right" ReadOnly="true">
                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                        </dx:ASPxTextBox>                        
                    
                    </div>
                    <div class="infocurr"><asp:Literal ID="TotalCashReceiptCurr" runat="server" ClientIDMode="Static">IDR</asp:Literal></div>
                    <div class="infonum">Number:</div>
                    <div class="infonumt">
                        <asp:TextBox ID="CashReceiptsNumber" runat="server" ClientIDMode="Static" Width="50px" CssClass="num" ReadOnly="true"></asp:TextBox> 
                    </div>
                </div>

                <div class="line">
                    <div class="infolabel">+ Total Check Receipts</div>
                    <div class="infotext">
                        <dx:ASPxTextBox ID="TotalCheckReceiptsText" runat="server" ClientIDMode="Static" Width="150px" CssClass="num" HorizontalAlign="Right" ReadOnly="true">
                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                        </dx:ASPxTextBox>                        
                    </div>
                    <div class="infocurr"><asp:Literal ID="TotalCheckReceiptsCurr" runat="server" ClientIDMode="Static">IDR</asp:Literal></div>
                    <div class="infonum">Number:</div>
                    <div class="infonumt"><asp:TextBox ID="CheckReceiptsNumber" runat="server" ClientIDMode="Static" Width="50px" CssClass="num" ReadOnly="true"></asp:TextBox> </div>
                </div>

                <div class="line">
                    <div class="infolabel">- Total Cash Payments</div>
                    <div class="infotext">
                        <dx:ASPxTextBox ID="TotalCashPaymentsText" runat="server" ClientIDMode="Static" Width="150px" CssClass="num" HorizontalAlign="Right" ReadOnly="true">
                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                        </dx:ASPxTextBox>                        
                    </div>
                    <div class="infocurr"><asp:Literal ID="TotalCashPaymentsCurr" runat="server" ClientIDMode="Static">IDR</asp:Literal></div>
                    <div class="infonum">Number:</div>
                    <div class="infonumt"><asp:TextBox ID="CashPaymentsNumber" runat="server" ClientIDMode="Static" Width="50px" CssClass="num" ReadOnly="true"></asp:TextBox> </div>
                </div>
                <div class="rowblank">&nbsp;</div>
                <div class="rowline">&nbsp;</div>

                <div class="line">
                    <div class="infolabel">Closing Balance</div>
                    <div class="infotext">
                        <dx:ASPxTextBox ID="ClosingBalanceText" runat="server" ClientIDMode="Static" Width="150px" CssClass="num" HorizontalAlign="Right" ReadOnly="true">
                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                        </dx:ASPxTextBox>                        
                    
                    </div>
                    <div class="infocurr"><asp:Literal ID="ClosingBalanceCurr" runat="server" ClientIDMode="Static">IDR</asp:Literal></div>
                </div>
                   
                <div class="line">
                    <div class="infolabel">Cash</div>
                    <div class="infotext">
                        <dx:ASPxTextBox ID="CashText" runat="server" ClientIDMode="Static" Width="150px" CssClass="num" HorizontalAlign="Right" ReadOnly="true">
                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                        </dx:ASPxTextBox>  
                    </div>
                    <div class="infocurr"><asp:Literal ID="CashCurr" runat="server" ClientIDMode="Static">IDR</asp:Literal></div>
                </div>

            </div>


            <!-- -->
    </ContentTemplate>
    </asp:UpdatePanel>

</div>

<div id="bottomwrap">
    <asp:UpdatePanel ID="GridPanel" runat="server">
    <Triggers>
    </Triggers>
    <ContentTemplate>
        <div id="grid">
        
            <dx:ASPxPageControl runat="server" ID="JournalPages"  ClientIDMode="Static" ClientInstanceName="JournalPages"
                Height="250px" Width="980px" 
                ActiveTabIndex="0" 
                EnableHierarchyRecreation="True" >           
                
                <TabPages>
                    <dx:TabPage Text="Cash Payments" Name="PaymentPage">
                        <ContentCollection>
                            <dx:ContentControl ID="PaymentsCC" runat="server">
                                <dx:ASPxGridView ID="PaymentGrid"  runat="server" 
                                    ClientIDMode="Static" ClientInstanceName="PaymentGrid"
                                    Width="965px" 
                                    KeyFieldName="InternalDocNo"
                                    SettingsBehavior-ColumnResizeMode="Control"                                    
                                    OnHtmlRowCreated="Grid_HtmlRowCreated"
                                    OnCellEditorInitialize="Grid_CellEditorInitialize"
                                    OnCustomColumnDisplayText="Grid_CustomColumnDisplayText"
                                    OnHtmlDataCellPrepared="PaymentGrid_HtmlDataCellPrepared">

                                    <Styles AlternatingRow-CssClass="even" Header-HorizontalAlign="Center">
                                        <Cell VerticalAlign="Middle" />
                                    </Styles>
                                    <Settings ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true"   VerticalScrollBarStyle="Standard" />
                                     <SettingsPager  Mode="ShowAllRecords">
                                     </SettingsPager>
                                    <SettingsEditing Mode="Inline" />
                                <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                     CellStyle-VerticalAlign="Middle">
                                    <EditButton Visible="false" Text=" ">
                                    </EditButton>
                                    <UpdateButton Visible="false" Text=" ">
                                    </UpdateButton>
                                    <NewButton Visible="false" Text=" ">
                                    </NewButton>
                                    <CancelButton Visible="false" Text=" ">
                                    </CancelButton>
                                    <CellStyle VerticalAlign="Middle">
                                    </CellStyle>
                                    <HeaderTemplate>
                                        <dx:ASPxCheckBox ID="SelectAllPayment" runat="server" ClientInstanceName="SelectAllPayment"
                                            ClientSideEvents-CheckedChanged="function(s, e) { PaymentGrid.SelectAllRowsOnPage(s.GetChecked()); 
                                            gridAllSelected = s.GetChecked();}"
                                            CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                        </dx:ASPxCheckBox>
                                    </HeaderTemplate>
                                </dx:GridViewCommandColumn>

                                <dx:GridViewDataTextColumn Caption="Business Transaction" VisibleIndex="1" Width="230px"
                                    FieldName="BusinessTransaction">                                        
                                    <EditItemTemplate>
                                        <dx:ASPxComboBox ID="cpBusinessTransaction" runat="server" Width="226px" 
                                            IncrementalFilteringMode="Contains"
                                            IncrementalFilteringDelay="1000"
                                            DataSourceID="dsBusinessTransactionPay">
                                        </dx:ASPxComboBox>                                        
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Amount" VisibleIndex="2" FieldName="Amount" Width="120px" >
                                    <CellStyle HorizontalAlign="Right" ></CellStyle>
                                    
                                    <PropertiesTextEdit DisplayFormatString="{0:N0}" />
                                    <EditItemTemplate>

                                        <dx:ASPxTextBox ID="pAmount" runat="server" Width="116px" CssClass="num" HorizontalAlign="Right" >
                                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                                        </dx:ASPxTextBox>

                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Stat" VisibleIndex="3" FieldName="DocumentStatus" Width="50px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                    <EditItemTemplate></EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Reference" VisibleIndex="4" FieldName="Reference" Width="100px" >
                                    <CellStyle HorizontalAlign="Center" />
                                    <EditItemTemplate>
                                        <asp:TextBox ID="pReference" runat="server"  Width="96px" MaxLength="16"/>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Text" VisibleIndex="5" FieldName="Text" Width="230px">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="pText" runat="server" Width="226px" MaxLength="50"/>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Customer" VisibleIndex="6" FieldName="Customer" Width="80px">
                                    <EditItemTemplate>
                                        <dx:ASPxGridLookup ID="lopCustomer" runat="server" ClientIDMode="Static" 
                                            ClientInstanceName="lopCustomer" 
                                            SelectionMode="Single" KeyFieldName="CUSTOMER_CD" TextFormatString="{0}"
                                            Width="76px" 
                                            CssClass="display-inline-table" 
                                            IncrementalFilteringMode="Contains"                                            
                                            DataSourceID="dsCustomer">                                            
                                            <GridViewProperties >
                                                    <SettingsBehavior   AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                    <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                                    <SettingsPager PageSize="7"/>
                                            </GridViewProperties>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Code" FieldName="CUSTOMER_CD" Width="60px">
                                                    <Settings AutoFilterCondition="Contains"/>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Search Terms" FieldName="SEARCH_TERMS" Width="60px">
                                                    <Settings AutoFilterCondition="Contains" />                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Name" FieldName="CUSTOMER_NAME" Width="200px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridLookup>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Vendor" VisibleIndex="7" FieldName="Vendor" Width="80px">
                                    <EditItemTemplate>
                                        <dx:ASPxGridLookup ID="lopVendor" runat="server" ClientIDMode="Static" 
                                            ClientInstanceName="lopVendor" 
                                            SelectionMode="Single" KeyFieldName="VENDOR_CD" TextFormatString="{0}"
                                            Width="76px" 
                                            CssClass="display-inline-table" 
                                            IncrementalFilteringMode="Contains"
                                            DataSourceID="dsVendor">                                            
                                            <GridViewProperties>
                                                    <SettingsBehavior   AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                    <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true"/>
                                                    <SettingsPager PageSize="7"/>
                                            </GridViewProperties>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Code" FieldName="VENDOR_CD" Width="60px">
                                                    <Settings AutoFilterCondition="Contains"/>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Search Terms" FieldName="SEARCH_TERMS" Width ="60px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Name" FieldName="VENDOR_NAME" Width="200px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridLookup>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn Caption="Posting Date" VisibleIndex="8" FieldName="PostingDate">
                                    <PropertiesDateEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                    </PropertiesDateEdit>
                                    <EditItemTemplate />
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataDateColumn Caption="Doc Date" VisibleIndex="9" Width="85px" FieldName="DocDate">
                                    <PropertiesDateEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                    </PropertiesDateEdit>
                                    <EditItemTemplate />                                    
                                </dx:GridViewDataDateColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Doc No" VisibleIndex="10" FieldName="DocNo">
                                    <CellStyle HorizontalAlign="Center" />
                                    <EditItemTemplate />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Internal Doc No" VisibleIndex="11" FieldName="InternalDocNo">
                                    <CellStyle HorizontalAlign="Center" />
                                    <EditItemTemplate />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Seq No" VisibleIndex="12" Width="60px" Visible="false">
                                    <EditItemTemplate></EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Assignment" VisibleIndex="13" FieldName="Assignment">
                                    <EditItemTemplate></EditItemTemplate>
                                </dx:GridViewDataTextColumn>

                                </Columns>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="Cash Receipts" Name="ReceiptPage">
                        <ContentCollection>
                            <dx:ContentControl ID="ReceiptCC" runat="server">
                                <dx:ASPxGridView ID="ReceiptGrid"  runat="server" 
                                    ClientIDMode="Static" ClientInstanceName="ReceiptGrid"
                                    Width="925px" 
                                    KeyFieldName="InternalDocNo"
                                    SettingsBehavior-ColumnResizeMode="Control"
                                    OnCellEditorInitialize="Grid_CellEditorInitialize"
                                    OnCustomColumnDisplayText="Grid_CustomColumnDisplayText"
                                    OnHtmlRowCreated="Grid_HtmlRowCreated"
                                    OnHtmlDataCellPrepared="ReceiptGrid_HtmlDataCellPrepared">
                                    <Styles AlternatingRow-CssClass="even" Header-HorizontalAlign="Center" />
                                    <Settings ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true" />                                    
                                    <SettingsPager  Mode="ShowAllRecords">
                                     </SettingsPager>
                                     <SettingsEditing Mode="Inline" />
                                <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                     CellStyle-VerticalAlign="Middle">
                                     <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                    <EditButton Visible="false" Text=" ">
                                    </EditButton>
                                    <UpdateButton Visible="false" Text=" ">
                                    </UpdateButton>
                                    <NewButton Visible="false" Text=" ">
                                    </NewButton>
                                    <CancelButton Visible="false" Text=" ">
                                    </CancelButton>
                                    <CellStyle VerticalAlign="Middle">
                                    </CellStyle>
                                    <HeaderTemplate >
                                        <dx:ASPxCheckBox ID="SelectAllReceipt" runat="server" ClientInstanceName="SelectAllReceipt"
                                            ClientSideEvents-CheckedChanged="function(s, e) { ReceiptGrid.SelectAllRowsOnPage(s.GetChecked()); 
                                            gridAllSelected = s.GetChecked();}"
                                            CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                        </dx:ASPxCheckBox>
                                    </HeaderTemplate>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn Caption="Business Transaction" VisibleIndex="1" Width="230px"
                                    FieldName="BusinessTransaction">
                                     <EditItemTemplate>
                                        <dx:ASPxComboBox ID="crBusinessTransaction" runat="server" Width="226px" 
                                            IncrementalFilteringMode="Contains"
                                            IncrementalFilteringDelay="1000"
                                            DataSourceID="dsBusinessTransactionReceipt">
                                        </dx:ASPxComboBox>                                        
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Amount" VisibleIndex="2" FieldName="Amount" Width="120px">
                                    <PropertiesTextEdit DisplayFormatString="{0:N0}" />
                                    <EditItemTemplate>
                                        <dx:ASPxTextBox ID="rAmount" runat="server" Width="116px" CssClass="num" HorizontalAlign="Right" >
                                            <MaskSettings  Mask="<0..9999999999999g>" IncludeLiterals="DecimalSymbol"/>
                                        </dx:ASPxTextBox>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Stat" VisibleIndex="3" FieldName="DocumentStatus" Width="50px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                    <EditItemTemplate></EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Reference" VisibleIndex="4" FieldName="Reference" Width="100px">  
                                    <CellStyle HorizontalAlign="Center" />
                                    <EditItemTemplate>
                                        <asp:TextBox ID="rReference" runat="server"  Width="96px" MaxLength="16"/>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Text" VisibleIndex="5" FieldName="Text" Width="230px">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="rText" runat="server" Width="226px" MaxLength="50"/>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>

                                
                                <dx:GridViewDataTextColumn Caption="Customer" VisibleIndex="6" FieldName="Customer" Width="80px">
                                    <EditItemTemplate>
                                        <dx:ASPxGridLookup ID="lorCustomer" runat="server" ClientIDMode="Static" 
                                            ClientInstanceName="lorCustomer" 
                                            SelectionMode="Single" KeyFieldName="CUSTOMER_CD" TextFormatString="{0}"
                                            Width="76px" 
                                            CssClass="display-inline-table" 
                                            IncrementalFilteringMode="Contains"
                                            DataSourceID="dsCustomer">                                            
                                            <GridViewProperties >
                                                    <SettingsBehavior   AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                    <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true"/>
                                                    <SettingsPager PageSize="7"/>
                                            </GridViewProperties>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Code" FieldName="CUSTOMER_CD" Width="60px">
                                                    <Settings AutoFilterCondition="Contains"/>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Search Terms" FieldName="SEARCH_TERMS" Width="60px">
                                                    <Settings AutoFilterCondition="Contains" />                                                    
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Name" FieldName="CUSTOMER_NAME" Width="200px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridLookup>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>


                                <dx:GridViewDataTextColumn Caption="Vendor" VisibleIndex="7" FieldName="Vendor" Width="80px">
                                <EditItemTemplate>
                                    <dx:ASPxGridLookup ID="lorVendor" runat="server" ClientIDMode="Static" 
                                        ClientInstanceName="lorVendor" 
                                        SelectionMode="Single" KeyFieldName="VENDOR_CD" TextFormatString="{0}"
                                        Width="76px" 
                                        CssClass="display-inline-table" 
                                        IncrementalFilteringMode="Contains"
                                        DataSourceID="dsVendor">                                            
                                        <GridViewProperties >
                                                <SettingsBehavior   AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true"/>
                                                <SettingsPager PageSize="7"/>
                                        </GridViewProperties>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="Code" FieldName="VENDOR_CD" Width="60px">
                                                <Settings AutoFilterCondition="Contains"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Search Terms" FieldName="SEARCH_TERMS" Width="60px">
                                                <Settings AutoFilterCondition="Contains"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Name" FieldName="VENDOR_NAME" Width="200px">
                                                <Settings AutoFilterCondition="Contains" />
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridLookup>
                                </EditItemTemplate>                                
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataDateColumn Caption="Posting Date" VisibleIndex="8" FieldName="PostingDate">
                                    <PropertiesDateEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                    </PropertiesDateEdit>
                                    <EditItemTemplate ></EditItemTemplate>
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataDateColumn Caption="Doc Date" VisibleIndex="9" Width="85px" FieldName="DocDate">
                                    <PropertiesDateEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                    </PropertiesDateEdit>                                    
                                    <EditItemTemplate />
                                </dx:GridViewDataDateColumn>
                                
                                <dx:GridViewDataTextColumn Caption="Doc No" VisibleIndex="10" FieldName="DocNo">
                                    <CellStyle HorizontalAlign="Center" />
                                    <EditItemTemplate />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Internal Doc No" VisibleIndex="11" FieldName="InternalDocNo">
                                    <CellStyle HorizontalAlign="Center" />
                                    <EditItemTemplate />
                                    </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Seq No" VisibleIndex="12" Width="60px" Visible="false">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Assignment" VisibleIndex="13" FieldName="Assignment">
                                    <EditItemTemplate />
                                </dx:GridViewDataTextColumn>
                                </Columns>
                                </dx:ASPxGridView>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    
                </TabPages>
            </dx:ASPxPageControl>
        </div>
        
        <div class="xline">
            <div class="lefty">
                <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click"  OnClientClick="loading();" />
                <asp:Button ID="CancelButton" runat="server" Text="Cancel" OnClick="CancelButton_Click" />
            </div>
            <div class="lefty" id="AddButtonDiv" runat="server">
                <asp:Button ID="AddButton" runat="server" Text="Add" OnClick="AddButton_Click" ClientIDMode="Static" OnClientClick="loading();"/>                
            </div>

            <div class="lefty" id="PostButtonDiv" data-hint="Select Document first">
                <asp:Button ID="PostButton" runat="server" Text="Post" OnClick="PostButton_Click" OnClientClick="loading();" />                
            </div>
            <div class="lefty">
                <asp:Button ID="DeleteButton" runat="server" Text="Delete" OnClick="DeleteButton_Click" />
            </div>
            <div class="lefty" id="ReverseButtonDiv" >
                <asp:Button ID="ReverseButton" runat="server" Text="Reverse" OnClientClick="ReverseButton_Click();"/>
            </div>
            
            <div class="btnRightLong">
                <asp:Button ID="CloseButton" runat="server" Text="Close" OnClientClick="closeWin()" />
            </div>
        </div>
        <asp:LinqDataSource ID="dsBusinessTransactionPay" runat="server" OnSelecting="dsBusinessTransactionPay_Selecting" />
        <asp:LinqDataSource ID="dsBusinessTransactionReceipt" runat="server" OnSelecting="dsBusinessTransactionReceipt_Selecting" />
        <asp:LinqDataSource ID="dsVendor" runat="server" OnSelecting="dsVendor_Selecting" />
        <asp:LinqDataSource ID="dsCustomer" runat="server" OnSelecting="dsCustomer_Selecting" />
        <asp:LinqDataSource ID="dsReason" runat="server" OnSelecting="dsReason_Selecting" />
    </ContentTemplate>
    </asp:UpdatePanel>

</div>
<asp:Button ID="popButton" runat="server" CssClass="hidden"  Style="display: none;"/>
<act:ModalPopupExtender ID="mopo" runat="server" ClientIDMode="Static" 
    TargetControlID="popButton" 
    PopupControlID="PopPanel" CancelControlID="PopNo" 
    BackgroundCssClass="modalBackground" />
<center>
    <asp:Panel ID="PopPanel" runat="server" CssClass="informationPopUpX">
        <div class="row cellbox">
            <asp:Literal ID="PopLit" runat="server" />
        </div>
        <div class="row" style="height:30px;">
        &nbsp;
        </div>
        <div class="rowbutton">
            <div class="btnRightLong">
            <asp:Button ID="PopYes" Text="Yes" runat="server" Width="60px" OnClientClick="loading();" OnClick="PopYes_Click"/>
            &nbsp; 
            <asp:Button ID="PopNo" Text="No" runat="server" Width="60px" />
            </div>
        </div>
            
    </asp:Panel>
</center>

<asp:Button ID="popReasonButton" runat="server" CssClass="hidden" style="display: none;"/>
<act:ModalPopupExtender ID="moRev" runat="server" ClientIDMode="Static"
    TargetControlID="popReasonButton" 
    PopupControlID="popReason"  
    CancelControlID="popReasonClose"
    BackgroundCssClass="modalBackground" />
<center>
    <asp:Panel ID="popReason" runat="server" CssClass="ReasonPopUp">
    <div class="row" style="height:120px; text-align:center;">
        <div class="lefty">Reason :</div>
        <div class="lefty" id="ReverseReasonDiv" >            
            <dx:ASPxComboBox ID="ReverseReason" runat="server" ClientIDMode="Static" ClientInstanceName="ReverseReason" Width="250px"
                IncrementalFilteringMode="Contains"
                IncrementalFilteringDelay="1000"
                DataSourceID="dsReason" ValueField="Code" TextField="Description" TextFormatString="{0} {1}">
            <Columns>            
                <dx:ListBoxColumn FieldName="Code" Visible="true" Width="50"/>
                <dx:ListBoxColumn FieldName="Description" Visible="true"  Width="200"/>
            </Columns>
            </dx:ASPxComboBox>           
        </div>
        <div class="row" ID="ReasonMsgDiv"></div>

    </div>
    <div class="row" style="height:30px;">
    &nbsp;
    </div>
    <div class="rowbutton">
        <div class="btnRightLong">
        <asp:Button ID="popReasonOk" Text="Ok" runat="server" Width="60px" 
            OnClientClick="return popReasonOk_Click();" />
        &nbsp; 
        <asp:Button ID="popReasonClose" Text="Close" runat="server" Width="60px" />
        <asp:Button ID="popReasonGo" runat="server" ClientIDMode="Static" Width="0px" Height="0px" CssClass="hidden" OnClick="popReasonGo_Click" />
        </div>
    </div>
    </asp:Panel>
</center>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
