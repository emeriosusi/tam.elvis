﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._60SettlementForm
{
    public partial class SettlementFormList : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_6001");
        private string susNo, susYear;
        private int suNO, suYY;

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }
        #endregion

       

        #region Getter Setter for Data List
        private List<SettlementFormDetailData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<SettlementFormDetailData>();
                }
                else
                {
                    return (List<SettlementFormDetailData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion
        #region Getter Setter for Attachment List
        [Serializable]
        private class ListAttachmentData
        {
            private string _NO;
            public string NO
            {
                get
                {
                    return _NO;
                }
                set
                {
                    _NO = value;
                }
            }

            private string _FILENAME;
            public string FILENAME
            {
                get
                {
                    return _FILENAME;
                }
                set
                {
                    _FILENAME = value;
                }
            }
        }
        private List<ListAttachmentData> ListAttachment
        {
            set
            {
                ViewState["_listAttachment"] = value;
            }
            get
            {
                if (ViewState["_listAttachment"] == null)
                {
                    return new List<ListAttachmentData>();
                }
                else
                {
                    return (List<ListAttachmentData>)ViewState["_listAttachment"];
                }
            }
        }
        #endregion
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;
            if (!IsPostBack)
            {
                PrepareLogout();

                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();
                #region Init Startup Script
                
                ddlSuspenseNo.Attributes.Add("onkeydown", "if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; search(); return false; } ");
                txtPVYear.Attributes.Add("onkeydown", "if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; search(); return false; } ");


                #endregion
            }
        }


        protected void DoSearch()
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
                return;
            }
 
            litVendorCode.Text = "";
            litVendorName.Text = "";
    
            litTransactionType.Text = "";

            if (ValidateInputSearch())
            {
                txtPVYear.Text = susYear;
                SettlementFormHeaderData header = logic.Settle.Header(suNO, suYY);

                if (header != null)
                {
                    if (header.PV_TYPE_CD != 2)
                    {
                        litMessage.Text += Nagging("MSTD00020WRN", susNo);
                        gridGeneralInfo.DataSourceID = null;
                        SetFirstLoad();
                    }
                    else if (! UserData.isInDivision(header.DIVISION_ID))
                    {
                        litMessage.Text += Nagging("MSTD00022WRN", susNo, susYear);
                        gridGeneralInfo.DataSourceID = null;
                        SetFirstLoad();
                    }
                    else if (!
                                (
                                    (
                                    (header.STATUS_CD == 29) && "CE".Contains((header.PAY_METHOD_CD ?? "").Trim())
                                    )
                                    ||
                                    (
                                    (header.STATUS_CD == 27) && "AT".Contains((header.PAY_METHOD_CD ?? "").Trim())
                                    )
                                )
                            )
                    {
                        litMessage.Text += Nagging("MSTD00035WRN", susNo, susYear);
                        gridGeneralInfo.DataSourceID = null;
                        SetFirstLoad();
                    }
                    else
                    {
                       
                        litVendorCode.Text = header.VENDOR_CD;
                        litVendorName.Text = header.VENDOR_NAME;

                      
                        litTransactionType.Text = header.TRANSACTION_NAME;
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                       
                        gridGeneralInfo.DataBind();
                        if (header.PV_SUS_NO == null)
                        {
                            SetToEditMode();
                            PageMode = Common.Enum.PageMode.Add;
                        }
                        else
                        {
                            if (header.CONVERT_FLAG == 1 || header.SETTLEMENT_STATUS == 1)
                            {
                                SetReadOnlyDetail();
                                if (header.SETTLEMENT_STATUS == 1)
                                {
                                    litMessage.Text += Nagging("MSTD00033INF", susNo, susYear);
                                    gridGeneralInfo.DataSourceID = null;
                                    SetFirstLoad();
                                }
                            }
                            else
                            {
                                SetCancelEditDetail();
                            }
                            PageMode = Common.Enum.PageMode.View;
                        }
                    }
                }
                else
                {
                    litMessage.Text += Nagging("MSTD00021WRN", string.Format("PV {0} - Year {1}", susNo, susYear));
                    gridGeneralInfo.DataSourceID = null;
                    SetFirstLoad();
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            logic.Say("btnSearch_Click", "Search {0} {1}", ddlSuspenseNo.Text, txtPVYear.Text);
            DoSearch();
        }

        protected void txtGridSpentAmount_Changed(object sender, EventArgs e)
        {
            ASPxTextBox txtGridSpentAmount = sender as ASPxTextBox;
            int _currentIndex = ((GridViewTableRow)(txtGridSpentAmount.Parent.Parent.Parent.Parent)).VisibleIndex;
            HiddenField hidGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(_currentIndex,
                gridGeneralInfo.Columns["AMOUNT"] as GridViewDataColumn, "hidGridAmount") as HiddenField;
            if (txtGridSpentAmount != null && !String.IsNullOrWhiteSpace(txtGridSpentAmount.Text) &&
                hidGridSpentAmount != null && !String.IsNullOrWhiteSpace(hidGridSpentAmount.Value))
            {
                if (CommonFunction.IsDecimal(txtGridSpentAmount.Text))
                {
                    decimal spentAmount = Convert.ToDecimal(txtGridSpentAmount.Text);
                    decimal amount = Convert.ToDecimal(hidGridSpentAmount.Value);
                    decimal settlementAmount = spentAmount - amount;

                    Literal litSettlementAmount = gridGeneralInfo.FindRowCellTemplateControl(_currentIndex,
                        gridGeneralInfo.Columns["SETTLEMENT_AMOUNT"] as GridViewDataColumn, "litSettlementAmount") as Literal;
                    HiddenField hidGridSettlementAmount = gridGeneralInfo.FindRowCellTemplateControl(_currentIndex,
                        gridGeneralInfo.Columns["SETTLEMENT_AMOUNT"] as GridViewDataColumn, "hidSettlementAmount") as HiddenField;
                    hidGridSettlementAmount.Value = Convert.ToString(settlementAmount);
                    litSettlementAmount.Text = settlementAmount.ToString("###,##0");
                    RecalculateSettlementAmount();
                    Focus_On_Next_Empty_TextBox(_currentIndex);
                }
                else
                {
                    litMessage.Text += Nagging("MSTD00018WRN", "Spent Amount");
                    txtGridSpentAmount.Focus();
                }
            }
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            btnEdit.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnConvertPVRV.Visible = false;
            btnClose.Visible = true;
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.PageCount > 0)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    Literal litGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "litGridSpentAmount") as Literal;
                    if (txtGridSpentAmount != null)
                    {
                        txtGridSpentAmount.Visible = false;
                        litGridSpentAmount.Visible = true;
                    }
                }
            }
        }

        protected void dsUnsettled_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.Settle.SearchUnsettled(Convert.ToString(UserData.DIV_CD));
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = logic.Settle.Details(suNO, suYY);
            
            e.Result = ListInquiry;

            if (ListInquiry != null && ListInquiry.Count > 0)
            {
                litSettlementAmount.Text = "<table width='100%'><tr>";
                Dictionary<String, Decimal> amounts = new Dictionary<String, Decimal>();
                foreach (var data in ListInquiry)
                {
                    if (amounts.ContainsKey(data.CURRENCY_CD))
                    {
                        amounts[data.CURRENCY_CD] = amounts[data.CURRENCY_CD] + (data.SETTLEMENT_AMOUNT ?? 0);
                    }
                    else
                    {
                        amounts.Add(data.CURRENCY_CD, (data.SETTLEMENT_AMOUNT ?? 0));
                    }
                }

                foreach (var data in amounts)
                {
                    String formattedValue = CommonFunction.Eval_Curr(data.Key, data.Value);
                    litSettlementAmount.Text += String.Format("<td>{0} {1}</td>", data.Key, formattedValue);
                }
                litSettlementAmount.Text += "</tr></table>";
            }
            else
            {
                litSettlementAmount.Text = "";
            }
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            logic.Say("btnSaveDetail_Click", "Save Detail {0} {1}", ddlSuspenseNo.Value, txtPVYear.Text);
            if (ValidateInputSave())
            {
                bool convertPvRv = false;
                #region Add
                if (PageMode == Common.Enum.PageMode.Add)
                {
                    SettlementFormHeaderData settlement = GetHeader(convertPvRv);
                    List<SettlementFormDetailData> details = GetDetails();
                    bool result = logic.Settle.Insert(settlement, details, UserData);

                    if (result)
                    {
                        SetSavedMode(false);
                        Nag("MSTD00011INF", "Settlement Form");
                    }
                    else
                    {
                        Nag("MSTD00010ERR", "Settlement Form");
                    }
                }
                #endregion
                else if (PageMode == Common.Enum.PageMode.Edit)
                {
                    #region Edit
                    if (ValidateInputSave())
                    {
                        bool result = logic.Settle.Update(
                                GetHeader(convertPvRv), 
                                GetDetails(),
                            UserData);

                        if (result == true)
                        {
                            SetSavedMode(false);
                            Nag("MSTD00013INF", "Settlement Form");
                        }
                        else
                        {
                            Nag("MSTD00012ERR", "Settlement Form");
                        }
                    }
                    #endregion
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            logic.Say("btnEdit_Click", "Save Detail {0} {1}", ddlSuspenseNo.Value, txtPVYear.Text);
            if (ValidateInputSearch())
            {
                if (PageMode == Common.Enum.PageMode.View)
                {
                    SetToEditMode();
                    SetLiteralToTextDetail();
                }
            }
        }


        private void SplitReffNo(string reffno, out string Num, out string Year)
        {
            if (reffno.Length > 4)
            {
                Num = reffno.Substring(0, reffno.Length - 4);
                Year = reffno.Substring(reffno.Length - 4, 4);
            }
            else
            {
                Num = "";
                Year = "";
            }
        }

        protected void ddlSuspenseNo_ValueChanged(object sender, EventArgs e)
        {
            ASPxGridLookup g = sender as ASPxGridLookup;
            if (g != null)
            {
                string v = Convert.ToString(g.Value);
                logic.Say("ddlSuspenseNo_ValueChanged", "Suspense No= {0}", v);
                if (v.Length > 4)
                {
                    getSusNoYear();
                    txtPVYear.Text = susYear;
                    DoSearch();
                }
            }            
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            logic.Say("btnCancelDetail_Click", "Cancel Edit {0}", ddlSuspenseNo.Value);
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
            RecalculateSettlementAmount();
        }

        #region convert to pv/rv
        protected void btnConvertPVRV_Click(object sender, EventArgs e)
        {
            string selectedConfirmation = hidDialogConfirmation.Value;
            if (string.IsNullOrEmpty(selectedConfirmation) || !(selectedConfirmation.ToLower().Equals("y")))
                 return;
            logic.Say("btnConvertPVRV_Click", "Convert {0}", ddlSuspenseNo.Value);
            if (ValidateInputSave())
            {
                bool convertPvRv = true;
                PVFormHeaderData pvHeader = new PVFormHeaderData();
                RVFormHeaderData rvHeader = new RVFormHeaderData();
                SettlementFormHeaderData settlement = GetHeader(convertPvRv);

                getSusNoYear();
                List<SettlementFormDetailData> details = logic.Settle.Details(
                    settlement.PV_SUS_NO??0  , settlement.PV_YEAR);

                
                bool result = logic.Settle.Update(
                        settlement, details, UserData,
                        convertPvRv, pvHeader, rvHeader);

                if (result == true)
                {
                    SetSavedMode(true);
                    lblSettlementNo.Text = _m.Message("MSTD00025INF", susNo); 

                    string links = "";
                    if (pvHeader != null && pvHeader.PV_NO != 0 && pvHeader.PV_YEAR != 0)
                    {
                        links += "PV No. " +
                                        "<a id=\"pvLink\" href=\"#\" onclick=\"javascript:openWin('../30PvFormList/PVFormList.aspx?mode=view&pv_no=" +
                                        pvHeader.PV_NO +
                                        "&pv_year=" +
                                        pvHeader.PV_YEAR +
                                        "','ELVIS_PVFormList')\">" +
                                        pvHeader.PV_NO +
                                        "</a> ";
                        if (rvHeader != null && rvHeader.RV_NO != 0 && rvHeader.RV_YEAR != 0)
                        {
                            links += " / ";
                        }
                        else
                        {
                            links += " ";
                        }
                    }

                    if (rvHeader != null && rvHeader.RV_NO != 0 && rvHeader.RV_YEAR != 0)
                    {
                        links += "RV No. " +
                                        "<a id=\"rvLink\" href=\"#\" onclick=\"javascript:openWin('../40RvFormList/RVFormList.aspx?mode=view&rv_no=" +
                                        rvHeader.RV_NO +
                                        "&rv_year=" +
                                        rvHeader.RV_YEAR +
                                        "','ELVIS_RVFormList')\">" +
                                        rvHeader.RV_NO +
                                        "</a> ";
                    }

                    lblPVRVNo.Text = _m.Message("MSTD00026INF", links);
                    ConfirmationDeletePopUp.Show();
                    ScriptManager.RegisterStartupScript(this,
                       this.GetType(), "",
                       "assignPopClick();",
                       true);
                }
                else
                {
                    Nag("MSTD00028ERR");                    
                }
            }
        }
        #endregion

        protected void SetEditDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                btnSaveDetail.Visible = true && role.isAllowedAccess("btnSave");
                btnCancelDetail.Visible = true;

                btnEdit.Visible = false;
                btnConvertPVRV.Visible = false;
                btnClose.Visible = false;
                gridGeneralInfo.SettingsBehavior.AllowSort = false;
            }

            ddlSuspenseNo.ReadOnly = true;
            ddlSuspenseNo.CssClass = "disableFields";
            txtPVYear.ReadOnly = true;
            
            txtPVYear.CssClass = "disableFields";
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;
        }

        protected void SetCancelEditDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData Err = null;
                btnEdit.Visible = true && role.isAllowedAccess("btnEdit", ref Err);
                btnConvertPVRV.Visible = true && role.isAllowedAccess("btnConvertPVRV");
                SetTextToLiteralDetail();
            }
            
            ddlSuspenseNo.ReadOnly = false;
            ddlSuspenseNo.CssClass = "";
            txtPVYear.ReadOnly = false;
            txtPVYear.CssClass = "";
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
            if (btnConvertPVRV.Visible)
            {
                btnConvertPVRV.Attributes.Add("onclick", "convertConfirmation();");
            }
        }

        private void SetLiteralToTextDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    Literal litGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "litGridSpentAmount") as Literal;
                    if (txtGridSpentAmount != null)
                    {
                        txtGridSpentAmount.Visible = true;
                        litGridSpentAmount.Visible = false;
                    }
                }
            }
        }

        protected void SetReadOnlyDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                btnEdit.Visible = false;
                btnConvertPVRV.Visible = false;
                SetTextToLiteralDetail();
            }

            ddlSuspenseNo.ReadOnly = false;
            ddlSuspenseNo.CssClass = "";
            txtPVYear.ReadOnly = false;
            txtPVYear.CssClass = "";
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        }

        private void SetTextToLiteralDetail()
        {
            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                    gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                Literal litGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                    gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "litGridSpentAmount") as Literal;
                if (txtGridSpentAmount != null)
                {
                    txtGridSpentAmount.Visible = false;
                    litGridSpentAmount.Visible = true;
                }
            }
        }

        private void getSusNoYear()
        {
            string n = "";
            string y = "";
            SplitReffNo(Convert.ToString(ddlSuspenseNo.Value), out n, out y);
            susNo = n;
            susYear = y;
            suNO = susNo.Int();
            suYY = susYear.Int();
        }

        private void SetToEditMode()
        {
            if (gridGeneralInfo.VisibleRowCount > 0)
            {
                getSusNoYear();
                SettlementFormHeaderData header = logic.Settle.Header(suNO, suYY);
                if (header != null && header.PV_SUS_NO != null)
                    PageMode = Common.Enum.PageMode.Edit;
                else
                    PageMode = Common.Enum.PageMode.Add;
                gridGeneralInfo.SettingsBehavior.AllowSort = false;
                SetEditDetail();
            }
        }

        private void SetSavedMode(bool convertPvRv)
        {
            gridGeneralInfo.CancelEdit();
            gridGeneralInfo.DataBind();
            SetFirstLoad();
            PageMode = Common.Enum.PageMode.View;
            gridGeneralInfo.SettingsBehavior.AllowSort = true;
            if (!convertPvRv)
                SetCancelEditDetail();
            else
                SetReadOnlyDetail();
        }

        #endregion

        #region Function

        private bool ValidateInputSave()
        {
            bool valid = ValidateInputSearch();
            if (valid)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
            

                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;

                    if (txtGridSpentAmount != null && String.IsNullOrWhiteSpace(txtGridSpentAmount.Text))
                    {
                        Nag("MSTD00017WRN", "Spent");
                        valid = false;
                    }
                    else
                    {
                        if (!CommonFunction.IsDecimal(txtGridSpentAmount.Text))
                        {
                            litMessage.Text += Nagging("MSTD00018WRN", "Spent");
                            valid = false;
                        }
                        else
                        {
                            Match match = Regex.Match(txtGridSpentAmount.Text, @"^[0-9]{1,14}([\.][0-9]{1,2})?$",
                                RegexOptions.IgnoreCase);
                            if (!match.Success)
                            {
                                litMessage.Text += Nagging("MSTD00032WRN", "Spent", "max (14 digits and 2 decimal)");
                                valid = false;
                            }
                        }
                    }

                    if (!valid)
                        break;
                }
            }
            return valid;
        }

        private bool ValidateInputSearch()
        {
            try
            {
                getSusNoYear();
                if (String.IsNullOrEmpty(Convert.ToString(ddlSuspenseNo.Value)))
                {
                    litMessage.Text += Nagging("MSTD00017WRN", "Suspense No.");
                    ddlSuspenseNo.Focus();
                        
                    return false;
                }
                else
                {
                    if (!CommonFunction.IsDecimal(susNo) 
                        )
                    {
                        if (!CommonFunction.IsDecimal(susNo))
                        {
                            litMessage.Text += Nagging("MSTD00018WRN", "Suspense No.");
                        }
                        
                        return false;
                    }

                    if (susNo.Length > Int32.MaxValue.ToString().Length)
                    {
                        litMessage.Text += Nagging("MSTD00032WRN", "Suspense No", Int32.MaxValue.ToString().Length);
                        SetFocus(txtPVYear);
                        return false;
                    }
                    else
                    {
                        if (Convert.ToInt64(susNo) > Int32.MaxValue)
                        {
                            litMessage.Text += Nagging("MSTD00033WRN", "Suspense No", Int32.MaxValue.ToString());
                            SetFocus(txtPVYear);
                            return false;
                        }
                    }

                    
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
                throw ;
            }
            return true;
        }

        private void Focus_On_Next_Empty_TextBox(int currentIndex)
        {
            bool setFocus = false;
            if (gridGeneralInfo.VisibleRowCount > 0)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (txtGridSpentAmount != null && (String.IsNullOrWhiteSpace(txtGridSpentAmount.Text)))
                    {
                        txtGridSpentAmount.Focus();
                        setFocus = true;
                        break;
                    }
                }

                if (!setFocus)
                {
                    ASPxTextBox nxtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(currentIndex + 1,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (nxtGridSpentAmount != null)
                    {
                        nxtGridSpentAmount.Focus();
                    }
                }
            }
        }

        private SettlementFormHeaderData GetHeader(bool convertPvRv)
        {
            SettlementFormHeaderData header = new SettlementFormHeaderData();            
            header.PV_SUS_NO = Convert.ToInt32(susNo);
            header.PV_NO = Convert.ToInt32(susNo);
            header.PV_YEAR = Convert.ToInt16(txtPVYear.Text);
            if (convertPvRv)
                header.CONVERT_FLAG = 1;    
            else
                header.CONVERT_FLAG = 0;
            return header;
        }

        
        private List<SettlementFormDetailData> GetDetails()
        {
            List<SettlementFormDetailData> settlementFormDetailList = null;
            if (gridGeneralInfo.VisibleRowCount > 0)
            {
                settlementFormDetailList = new List<SettlementFormDetailData>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    SettlementFormDetailData settlementFormDetail = new SettlementFormDetailData();
                    decimal spentAmount = 0;
                        ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                            gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                        spentAmount = Convert.ToDecimal(txtGridSpentAmount.Text);

                    HiddenField hidGridAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["AMOUNT"] as GridViewDataColumn, "hidGridAmount") as HiddenField;
                    HiddenField hidSeqNo = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "hidSeqNo") as HiddenField;
                    HiddenField hidPvSeqNo = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "hidPvSeqNo") as HiddenField;
                    Literal litCurr = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["CURRENCY_CD"] as GridViewDataColumn, "litGridCurrency") as Literal;
                    
                    if (hidSeqNo != null && !String.IsNullOrWhiteSpace(hidSeqNo.Value))
                        settlementFormDetail.SEQ_NO = Convert.ToInt32(hidSeqNo.Value);
                    settlementFormDetail.PV_SEQ_NO = Convert.ToInt32(hidPvSeqNo.Value);
                    settlementFormDetail.PV_SUS_NO = Convert.ToInt32(susNo);
                    settlementFormDetail.PV_YEAR = Convert.ToInt16(txtPVYear.Text);
                    settlementFormDetail.CURRENCY_CD = litCurr.Text;
                    settlementFormDetail.SPENT_AMOUNT = spentAmount;
                    settlementFormDetail.AMOUNT = Convert.ToDecimal(hidGridAmount.Value);                    
                    settlementFormDetail.SETTLEMENT_AMOUNT = settlementFormDetail.SPENT_AMOUNT - settlementFormDetail.AMOUNT;

                    settlementFormDetailList.Add(settlementFormDetail);
                }
            }

            return settlementFormDetailList;
        }

        private void RecalculateSettlementAmount()
        {
            if (gridGeneralInfo.VisibleRowCount > 0)
            {
                litSettlementAmount.Text = "<table width='100%'><tr>";
                Dictionary<String, Decimal> amounts = new Dictionary<String, Decimal>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    HiddenField hidGridSettlementAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                            gridGeneralInfo.Columns["SETTLEMENT_AMOUNT"] as GridViewDataColumn, "hidSettlementAmount") as HiddenField;
                    Literal litGridCurrency = gridGeneralInfo.FindRowCellTemplateControl(i,
                            gridGeneralInfo.Columns["CURRENCY"] as GridViewDataColumn, "litGridCurrency") as Literal;
                    decimal settlementAmount = 0;
                    if (!String.IsNullOrWhiteSpace(hidGridSettlementAmount.Value) && CommonFunction.IsDecimal(hidGridSettlementAmount.Value))
                        settlementAmount = Convert.ToDecimal(hidGridSettlementAmount.Value);
                    if (amounts.ContainsKey(litGridCurrency.Text))
                    {
                        amounts[litGridCurrency.Text] = amounts[litGridCurrency.Text] + settlementAmount;
                    }
                    else
                    {
                        amounts.Add(litGridCurrency.Text, settlementAmount);
                    }
                }
                
                foreach (var data in amounts)
                {
                    String formattedValue = CommonFunction.Eval_Curr(data.Key, data.Value);
                    litSettlementAmount.Text += String.Format("<td>{0} {1}</td>", data.Key, formattedValue);
                }
                litSettlementAmount.Text += "</tr></table>";
            }
            else
            {
                litSettlementAmount.Text = "";
            }
        }
        #endregion
    }
}