﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Common.Data;
using System.Web.UI.WebControls;

namespace ELVISDashboard.presentationHelper
{
    public class RoleHelper
    {
        private Page _Page;
        private string _ScreenID;
        private string _UserID;
        private List<RoleAcessData> _RoleAcessList;
        public RoleHelper(ref Page page, string ScreenID, string UserID)
        {
            this._Page = page;
            this._ScreenID = ScreenID;
            this._UserID = UserID;
            this._RoleAcessList = null;
        }
        public void ValidateAcess(Type[] T, ref ErrorData Err)
        {
            if (_Page != null && string.IsNullOrWhiteSpace(_ScreenID) == false)
            {
                _RoleAcessList = new BusinessLogic.RoleLogic().getRoleAcessDataPerScreen(_UserID, _ScreenID, ref Err);
                if (_RoleAcessList != null)
                {
                    ValidateControl(_Page.Controls, T);
                }
            }
        }
        private void ValidateControl(ControlCollection Controls, Type[] T)
        {
            foreach (System.Web.UI.Control _control in Controls)
            {
                int Ubound = T.GetUpperBound(0);
                for (int i = 0; i < Ubound; i++)
                {
                    if (_control.GetType() == T[i])
                    {
                        for (int j = 0; j < _RoleAcessList.Count; j++)
                        {
                            _control.Visible = false;
                            if (_control.ID == _RoleAcessList[j].OBJECT_ID)
                            {
                                _control.Visible = true;
                                break;
                            }
                        }
                        break;
                    }
                    else
                    {
                        if (_control.Controls != null)
                        {
                            if (_control.Controls.Count > 0)
                            {
                                ValidateControl(_control.Controls, T);
                            }
                        }
                    }
                }
            }
        }

    }
}