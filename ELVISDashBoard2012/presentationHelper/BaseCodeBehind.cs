﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Function;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace ELVISDashBoard.presentationHelper
{
    public class BaseCodeBehind : System.Web.UI.Page
    {
        protected readonly GlobalResourceData resx = new GlobalResourceData();
        protected readonly MessageLogic _m = new MessageLogic();

        readonly protected string green = "#00ff00";
        readonly protected string red = "#ff0000";
        readonly protected string yellow = "#fffb00";
        readonly protected string grey = "#8f8f8f";
        readonly protected string purple = "#fe72ed";

        readonly protected int[] ListColumnWidths = new int[] { 85, 85, 60, 60 };

        protected string[] pages = new string[] { "5", "10", "15", "20", "25" };
        readonly protected string GridCustomPageSizeName = "gridCustomPageSize";

        //protected string[] AuthorizedRoles = new string[] { };

        public LogicFactory logic = LogicFactory.Get();

        protected UserData UserData
        {
            get
            {
                return logic.User.SessionUserData(Context);
            }
        }

        protected string _UserName = null;
        protected string UserName
        {
            get
            {
                if (_UserName.isEmpty())
                {
                    string u = "__";
                    try
                    {
                        u = (Session != null) ?
                                ((UserData != null) ?
                                    UserData.USERNAME
                                    : "")
                                : "_";
                    }
                    catch (Exception ex)
                    {
                        LoggingLogic.err(ex);
                    }
                    _UserName = u;
                }
                return _UserName;
            }
        }

        protected readonly string FORM_AUTH_UPLOAD_DETAIL = "FormAuthUploadDetail";
        protected bool UserCanUploadDetail
        {
            get
            {
                if (Session == null || UserData == null) return false;
                bool c = false;

                List<int> canUploadDetailDiv = new List<int>();
                foreach (string divCd in (logic.Sys.GetText("FORM_AUTH", "UPLOAD_DETAIL") ?? "").Split(','))
                {
                    int DIV_CD = divCd.Int();
                    if (!canUploadDetailDiv.Contains(DIV_CD))
                        canUploadDetailDiv.Add(DIV_CD);
                }
                c = canUploadDetailDiv.Contains(UserData.DIV_CD.Int());

                return c;
            }
        }

        protected Common.Enum.PageMode PageMode
        {
            set
            {
                ViewState["PageMode"] = value;
            }
            get
            {
                if (ViewState["PageMode"] == null)
                {
                    return Common.Enum.PageMode.View;
                }
                else
                {
                    return (Common.Enum.PageMode)ViewState["PageMode"];
                }
            }
        }

        private readonly string _MYROLE_ = "user.role";
        protected UserRoleHeader LoginUserRole
        {
            get
            {
                UserRoleHeader h;
                if (Session[_MYROLE_] == null)
                    h = new UserRoleHeader()
                    {
                        ROLE_ID = "",
                        ROLE_NAME = "",
                        USERNAME = "",
                        FIRST_NAME = "",
                        LAST_NAME = "",
                        REG_NO = 0,
                        APPLICATION_ID = ""
                    };
                else
                    h = Session[_MYROLE_] as UserRoleHeader;
                return h;
            }

            set
            {
                Session[_MYROLE_] = value;
            }
        }

        
        protected int ProcessID
        {
            get
            {
                object o = Session["logPID"];
                int _pid = 0;
                if (o == null)
                {
                    _pid = logic.Activity.Log("INF", "Process Start", this.ToString(), UserName, "", 0);
                    Session["logPID"] = _pid;
                }
                else
                {
                    _pid = Convert.ToInt32(o);
                }
                return _pid;
            }

            set
            {
                Session["logPID"] = value;
            }
        }


        #region button Authorization

        protected static Control FindControlRecursive(Control Root, string Id)
        {
            if (Root.ID == Id)
                return Root;
            foreach (Control Ctl in Root.Controls)
            {
                Control FoundCtl = FindControlRecursive(Ctl, Id);
                if (FoundCtl != null)
                    return FoundCtl;
            }
            return null;
        }

        protected Literal lit;

        protected void Page_Load()
        {
            
        }

       

        #endregion

    
  

        #region Logging
        const string LOG_START = "0";
        const string LOG_END_SUCCESS = "0";
        const string LOG_END_ERR_B = "1";
        const string LOG_END_ERR_S = "2";
        protected LoggingLogic log = new LoggingLogic();
     
        protected int DoStartLog(string function_id, string remarks)
        {
            ProcessID = log.Log("", "", "", UserName, function_id, 0, remarks);
            return ProcessID;
        }

        protected int Log(string msgId, string msgType, string errLocation, string errMsg)
        {
            ProcessID = log.Log(msgId, errMsg, errLocation, UserName, "", ProcessID);
            return ProcessID;
        }

        #endregion

        #region ComboBox
        private const string ComboQuery = "ComboQuery";
        protected void GenerateComboData(DropDownList ddList, string DataSource, bool needAll, string params1 = "")
        {
            ComboClassData comboData = new ComboClassData();
            string query = "";
            if (!String.IsNullOrEmpty(DataSource))
            {
                query = resx.Combo(DataSource) + params1;
                query = query.Replace(";", " ") + ";";
            }
            List<ComboClassData> ComboDataList = new List<ComboClassData>();
            if (needAll)
            {
                ComboDataList = logic.Combo.getComboData(query, true, DataSource);
            }
            else
            {
                ComboDataList = logic.Combo.getComboData(query, false, DataSource);
            }

            if (ComboDataList != null)
            {
                ddList.DataSource = ComboDataList;
                ddList.DataTextField = "COMBO_NAME";
                ddList.DataValueField = "COMBO_CD";
                ddList.DataBind();
            }
        }

        protected void GenerateComboData(DropDownList ddList, string DataSource)
        {
            GenerateComboData(ddList, DataSource, true);
        }

        protected void GenerateComboData(ASPxComboBox ddList, string DataSource)
        {
            GenerateComboData(ddList, DataSource, false);
        }

        //private const string ComboQuery = "ComboQuery";
        protected void GenerateComboData(ASPxComboBox ddList, string DataSource, bool needAll)
        {
            ComboClassData comboData = new ComboClassData();
            string query = "";
            if (!String.IsNullOrEmpty(DataSource))
            {
                query = resx.Combo(DataSource);
            }
            List<ComboClassData> ComboDataList = new List<ComboClassData>();
            if (needAll)
            {
                ComboDataList = logic.Combo.getComboData(query, true, DataSource);
            }
            else
            {
                ComboDataList = logic.Combo.getComboData(query, false, DataSource);
            }

            if (ComboDataList != null)
            {
                ddList.DataSource = ComboDataList;
                ddList.TextField = "COMBO_NAME";
                ddList.ValueField = "COMBO_CD";
                ddList.DataBind();
            }
        }

        protected void GenerateComboData(ASPxComboBox dd, string DataId, string partialkey, bool needAll = false)
        {
            if (String.IsNullOrEmpty(partialkey))
            {
                GenerateComboData(dd, DataId, needAll);
                return;
            }

            List<ComboClassData> ComboDataList = null;

            string query = resx.Combo(DataId);
            ComboDataList = logic.Combo.getComboData(query, needAll, DataId);
            if (ComboDataList == null) return;

            var x = from c in ComboDataList
                    where c.COMBO_CD.StartsWith(partialkey) || c.COMBO_CD == ""
                    select new ComboClassData { COMBO_CD = c.COMBO_CD, COMBO_NAME = c.COMBO_NAME };

            List<ComboClassData> l = x.ToList();
            if (l != null)
            {
                dd.DataSource = l;
                dd.TextField = "COMBO_NAME";
                dd.ValueField = "COMBO_CD";
                dd.DataBind();
            }
        }
        #endregion

        #region to handle unregistered update panel on tab pages
        public void UpdatePanel_Unload(object sender, EventArgs e)
        {
            this.RegisterUpdatePanel(sender as UpdatePanel);
        }

        public void RegisterUpdatePanel(UpdatePanel panel)
        {
            foreach (MethodInfo methodInfo in typeof(ScriptManager).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (methodInfo.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel"))
                {
                    methodInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { panel });
                }
            }
        }
        #endregion

        #region Upload File

        protected FtpLogic ftp = new FtpLogic();

        #endregion

        #region Common Function

        #region Enter as Default Search
        // Add enter event on every input to default search - Neir Kate
        // put the code after SetFirstLoad() method
        private List<ASPxWebControl> aspxInputs { get; set; }
        private List<WebControl> inputs { get; set; }
        private void AddEnterScript(Button btnSearch, String screenId, ref String keyDownScript)
        {
            try
            {
                RoleLogic _RoleLogic = new RoleLogic(UserName, screenId);
                if (btnSearch != null && btnSearch.Visible && _RoleLogic.isAllowedAccess(btnSearch.ID))
                {
                    String script = "   <script type='text/javascript'>" +
                                    "       function search() { " +
                                    "           var btn = document.getElementById('" + btnSearch.ClientID + "'); " +
                                    "           btn.click(); " +
                                    "           loading(); " +
                                    "           return false; " +
                                    "       } " +
                                    "   </script>";

                    ClientScript.RegisterStartupScript(this.GetType(), "startScript", script);

                    keyDownScript = "if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; search(); return false; } ";
                }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }
        }

        protected void AddEnterComponent(ASPxWebControl ctrl)
        {
            if (ctrl != null)
            {
                if (aspxInputs == null)
                    aspxInputs = new List<ASPxWebControl>();
                aspxInputs.Add(ctrl);
            }
        }

        protected void AddEnterComponent(WebControl ctrl)
        {
            if (ctrl != null)
            {
                if (inputs == null)
                    inputs = new List<WebControl>();
                inputs.Add(ctrl);
            }
        }

        protected void AddEnterAsSearch(Button btnSearch, String screenId)
        {
            try
            {
                String keyDownScript = "";
                AddEnterScript(btnSearch, screenId, ref keyDownScript);
                if (!String.IsNullOrWhiteSpace(keyDownScript) && aspxInputs != null && aspxInputs.Count > 0)
                {
                    foreach (ASPxWebControl input in aspxInputs)
                    {
                        input.Attributes.Add("onkeydown", keyDownScript);
                    }
                }
                if (!String.IsNullOrWhiteSpace(keyDownScript) && inputs != null && inputs.Count > 0)
                {
                    foreach (WebControl input in inputs)
                    {
                        input.Attributes.Add("onkeydown", keyDownScript);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }
        }
        #endregion

        #region Set Date Value From/To
        protected void date_ValueChanged(object sender, ASPxDateEdit receiver, bool from)
        {
            ASPxDateEdit txtDate = sender as ASPxDateEdit;
            DateTime initialDate = new DateTime(StrTo.NULL_YEAR, 01, 01);

            if (txtDate.Date.CompareTo(initialDate) > 0)
            {
                if (String.IsNullOrEmpty(receiver.Text))
                {
                    receiver.Text = txtDate.Text;
                    receiver.Focus();
                }
                else
                {
                    if (from)
                    {
                        if (receiver.Date.CompareTo(txtDate.Date) < 0)
                        {
                            receiver.Text = txtDate.Text;
                            receiver.Focus();
                        }
                    }
                    else
                    {
                        if (receiver.Date.CompareTo(txtDate.Date) > 0)
                        {
                            receiver.Text = txtDate.Text;
                            receiver.Focus();
                        }
                    }
                }
            }
        }
        #endregion

        #region Set Textbox From/To Value changed
        protected void text_ValueChanged(object sender, TextBox receiver)
        {
            TextBox txtPvNo = sender as TextBox;

            if (String.IsNullOrEmpty(receiver.Text))
            {
                receiver.Text = txtPvNo.Text;
                receiver.Focus();
            }
        }
        #endregion

        #region Trim Division Name
        protected static string GetDivisionName(string DIV_NAME)
        {
            if (!String.IsNullOrEmpty(DIV_NAME))
            {
                if (DIV_NAME.IndexOf("-") > 0)
                {
                    DIV_NAME = DIV_NAME.Trim().Substring(0, DIV_NAME.IndexOf("-")).Trim();
                }
            }

            return DIV_NAME;
        }
        #endregion


        #endregion

        /* Additions by Lufty */
        #region Page
        protected virtual string getScreenName()
        {
            return "ELVIS";
        }
        #endregion

        #region Session
        protected virtual void clearSession()
        {
            //    // SessionManager.cleanUp();
            //    // SessionManager = null;
        }
        #endregion
        /* ------------------- */

        #region Error Handling

        protected int GetErrCode(string lastErr)
        {
            if (lastErr.IndexOf("Violation of PRIMARY KEY") > 0)
            {
                return 56;
            }
            else if (lastErr.IndexOf("SqlException") > 0)
            {
                return 57;
            }
            else
            {
                return 10;
            }
        }

        protected void GetLastErrMessages(string lastErr, string self, string code, string op, ref string msg, ref string msgCode)
        {
            int ec = GetErrCode(lastErr);
            msgCode = "MSTD000" + String.Format("{0}", ec) + "ERR";
            string res = resx.Message(msgCode);
            if (res != null)
                switch (ec)
                {
                    case 10:
                        msg = string.Format(res, self);
                        break;
                    case 56:
                        msg = String.Format(res, new object[] { self, code });
                        break;
                    case 57:
                        msg = String.Format(res, new object[] { op + " " + self, lastErr });
                        break;
                    default:
                        break;
                }
        }

        protected string Nagging(string Id, params object[] x)
        {
            string say = resx.Message(Id) ?? "";
            string errId = (Id.Length > 2) ? Id.Substring(Id.Length - 3, 3) : "";

            if (x != null && (x.Length > 0))
            {
                return MessageLogic.Wrap(String.Format(say, x), errId);
            }
            else
            {
                return MessageLogic.Wrap(say, Id);
            }
        }

        protected void Nag(string Id, params object[] x)
        {
            string say = Nagging(Id, x);
            if (lit != null)
            {
                lit.Text = say;
            }
            else
            {
                LoggingLogic.say(say);
            }
        }

        protected void NagAdd(string Id, params object[] x)
        {
            string say = Nagging(Id, x);
            if (lit != null)
            {
                lit.Text += ("<br/>" + say);
            }
            else
            {
                LoggingLogic.say(say);
            }
        }

        protected void Handle(Exception x)
        {
            LoggingLogic.err(x);
        }


        protected void Tick(string m, params object[] x)
        {
            log.Say("", m, x);
        }

        # endregion

        #region Added by Akhmad Nuryanto, 18/09/2012

       

        protected string getPVListLinkURL()
        {
            return
                Request.Url.Scheme + "://" +
                Request.ServerVariables["SERVER_NAME"] + ":" +
                Request.ServerVariables["SERVER_PORT"] +
                "/30PvFormList/PVList.aspx?date={0}&doc_no={1}";
        }

        protected void sendMailPVTicket(String PVNo, String PVYear)
        {
            bool result = false;
            String emailTemplate = String.Format("{0}\\PVTicketNotificationEmailTemplate.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
            EmailFunction mail = new EmailFunction(emailTemplate);
            ErrorData _Err = new ErrorData();
            result = mail.ComposeAdminEmailNotification(PVNo, PVYear, getPVListLinkURL(), ref _Err);
        }

        protected void ColorifyCell(string[][] x, ASPxGridViewTableDataCellEventArgs e)
        {
            for (int i = 0; i < x.Length; i++)
            {
                SetCellColor(e, x[i][0], x[i][1], x[i][2], x[i][3], x[i][4]);
            }
        }

        protected void SetWidths(ASPxGridView g, string[] cols, int[] widths)
        {
            int k = Math.Min(cols.Length, widths.Length);
            for (int i = 0; i < k; i++)
            {
                g.Columns[cols[i]].Width = widths[i];
            }
        }

        protected void WidthSet(ASPxGridView g, string[][] x, int[] w)
        {
            for (int i = 0; i < x.Length; i++)
            {
                SetWidths(g, new string[] { x[i][0], x[i][1], x[i][2], x[i][3] }, w);
            }
        }

        protected void SetCellColor(ASPxGridViewTableDataCellEventArgs e, String actualDateFN,
            String planDateFN, String actualLeadTimeFN, String planLeadTimeFN, String skipFlagFN)
        {
            if (e.DataColumn == null) return;

            if (e.DataColumn.FieldName == actualLeadTimeFN)
            {

                if (e.GetValue(actualDateFN) != null)
                {
                    // banding kan plan_lt dengan actual_lt 


                    int skipFlag = e.GetValue(skipFlagFN).Int(0);

                    if (skipFlag > 0)
                    {
                        if (skipFlag == 1)
                            e.Cell.Style.Add("background-color", grey);
                        else if (skipFlag == 2)
                            e.Cell.Style.Add("background-color", purple);
                    }
                    else
                    {
                        decimal planLt = e.GetValue(planLeadTimeFN).Dec(0);
                        decimal actualLt = e.GetValue(actualLeadTimeFN).Dec(0);
                        e.Cell.Style.Add("background-color", (actualLt > planLt) ? red : green);
                    }

                    //if (e.GetValue(planDateFN) != null)
                    //{
                    //    DateTime actualDate = (DateTime)e.GetValue(actualDateFN);
                    //    DateTime planDate = (DateTime)e.GetValue(planDateFN);

                    //    if (actualDate.CompareTo(planDate) > 0)
                    //    {
                    //        e.Cell.Style.Add("background-color", red);
                    //    }
                    //    else
                    //    {
                    //        int skipFlag = 0;

                    //        if (e.GetValue(skipFlagFN) != null)
                    //            skipFlag = (int)e.GetValue(skipFlagFN);

                    //        if (skipFlag == 0)
                    //            e.Cell.Style.Add("background-color", green);
                    //        else if (skipFlag == 1)
                    //            e.Cell.Style.Add("background-color", grey);
                    //        else if (skipFlag == 2)
                    //            e.Cell.Style.Add("background-color", purple);
                    //    }
                    //}
                }
                else
                {
                    if (e.GetValue(planDateFN) != null)
                    {
                        DateTime planDate = (DateTime)e.GetValue(planDateFN);
                        DateTime now = DateTime.Now;
                        e.Cell.Style.Add("background-color", (now.CompareTo(planDate) > 0) ? red : yellow);
                    }
                }
            }
        }

        protected void ShowLoad(string x)
        {
            string s, e;
            s = Request["__EVENTTARGET"];
            e = Request["__EVENTARGUMENT"];
            LoggingLogic.say(UserName, "{0}_Load {1} {2}|Target: {3}|Arg: {4}"
                , x
                , IsCallback.box("Callback ")
                , IsPostBack.box("Postback ")
                , s, e);
        }

        protected void ReloadOpener()
        {
            logic.Say("ReloadOpener", "ReloadOpener()");
            
            String script = "\r\n" +
                            "    ReloadOpener();\r\n";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "startScript", script, true);
        }

      
        #endregion

        protected virtual void Rebind()
        {
        }

        protected void RecordPerPage_Init(object sender, EventArgs e, ASPxGridView g)
        {
            DropDownList d = sender as DropDownList;
            d.DataSource = pages;
            d.DataBind();
            string v = g.SettingsPager.PageSize.str();
            ListItem l = d.Items.FindByValue(v);
            if (l != null) d.SelectedValue = v;
        }

        protected void RecordPerPage_Selected(object sender, EventArgs e, ASPxGridView g)
        {
            int newPageSize;
            DropDownList d = sender as DropDownList;
            newPageSize = d.SelectedValue.Int();
            g.SettingsPager.PageSize = newPageSize;
            Session[GridCustomPageSizeName] = newPageSize;
            Rebind();
        }

        protected void PrepareLogout()
        {
        }

        //fid.Hadid 20180613
        protected bool hasWorklist(string folio)
        {
            return logic.WorkList.isUserHaveWorklist(folio);            
        }

        protected void resetWorklist()
        {
          
        }
    }
}