﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Web.UI.WebControls;

namespace ELVISDashboard.presentationHelper
{
    public delegate void MulitEditingUpdateRow(EditingRowValues rowValues);
    public class MultiEditingHelper
    {
        ASPxGridView grid;
        Dictionary<int, EditingRowValues> editingValues;
        const int ColumnIndexMultiplier = 1000000;
        DevExpress.Web.ASPxGridView.Rendering.ASPxGridViewRenderHelper renderHelper;

        public MultiEditingHelper(ASPxGridView grid)
        {
            this.grid = grid;
            this.editingValues = new Dictionary<int, EditingRowValues>();
        }
        public ASPxGridView Grid { get { return grid; } }
        protected DevExpress.Web.ASPxGridView.Rendering.ASPxGridViewRenderHelper RenderHelper
        {
            get
            {
                if (renderHelper == null)
                {
                    renderHelper = new DevExpress.Web.ASPxGridView.Rendering.ASPxGridViewRenderHelper(Grid);
                }
                return renderHelper;
            }
        }

        public void CreateEditors(ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            if (!IsRowEditing(e.VisibleIndex)) return;
            for (int i = 0; i < grid.VisibleColumns.Count; i++)
            {
                GridViewDataColumn dataColumn = grid.VisibleColumns[i] as GridViewDataColumn;
                if (dataColumn != null && !dataColumn.ReadOnly)
                {
                    GenerateEditor(e.VisibleIndex, dataColumn, e.Row.Cells[i]);
                }
            }
        }
        public void LoadEditingValues(string values)
        {
            DevExpress.Web.ASPxGridView.Helper.GridViewCallBackEditorValuesReader reader = new
    DevExpress.Web.ASPxGridView.Helper.GridViewCallBackEditorValuesReader(values);
            reader.Proccess();
            foreach (DevExpress.Web.ASPxGridView.Helper.EditorValueInfo valueInfo in reader.Values)
            {
                int columnIndex = valueInfo.ColumnIndex / ColumnIndexMultiplier;
                int visibleIndex = valueInfo.ColumnIndex - columnIndex * ColumnIndexMultiplier;
                if (!editingValues.ContainsKey(visibleIndex))
                {
                    editingValues[visibleIndex] = new EditingRowValues(grid, visibleIndex);
                }
                editingValues[visibleIndex].SetValue((grid.Columns[columnIndex] as GridViewDataColumn).FieldName,
    valueInfo.Value);
            }
        }
        public void UpdateData(ASPxGridViewCustomDataCallbackEventArgs e, MulitEditingUpdateRow doUpdateRow)
        {
            e.Result = "";
            try
            {
                UpdateData(doUpdateRow);
            }
            catch (Exception exception)
            {
                e.Result = exception.Message;
            }
        }
        protected void UpdateData(MulitEditingUpdateRow doUpdateRow)
        {
            int startVisibleIndex = grid.PageIndex * grid.SettingsPager.PageSize;
            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                int visibleIndex = i + startVisibleIndex;
                if (!editingValues.ContainsKey(visibleIndex)) continue;
                doUpdateRow(editingValues[visibleIndex]);
            }
        }
        protected bool IsRowEditing(int visibleIndex)
        {
            return grid.Selection.IsRowSelected(visibleIndex);
        }
        protected object GetRowValue(string fieldName, int visibleIndex)
        {
            if (editingValues.ContainsKey(visibleIndex)) return editingValues[visibleIndex].GetValue(fieldName);
            return grid.GetRowValues(visibleIndex, fieldName);
        }

        protected void GenerateEditor(int visibleIndex, GridViewDataColumn column, TableCell cell)
        {
            cell.Controls.Clear();
            ASPxEditBase edit = CreateGridEditor(column, GetRowValue(column.FieldName, visibleIndex),
    EditorInplaceMode.Inplace);
            edit.ID = GetEditorId(column, visibleIndex);
            edit.Width = Unit.Percentage(99.99);
            cell.Controls.Add(edit);
        }
        protected string GetEditorId(GridViewDataColumn column, int visibleIndex)
        {
            return string.Format("multiEdit{0}", column.Index * ColumnIndexMultiplier + visibleIndex);
        }
        protected ASPxEditBase CreateGridEditor(GridViewDataColumn column, object value, EditorInplaceMode mode)
        {
            return RenderHelper.CreateGridEditor(column, value, mode, false);
        }
    }
    public class EditingRowValues
    {
        Dictionary<string, object> values;
        int visibleIndex;
        ASPxGridView grid;

        public EditingRowValues(ASPxGridView grid, int visibleIndex)
        {
            this.visibleIndex = visibleIndex;
            this.grid = grid;
            this.values = new Dictionary<string, object>();
        }
        public int VisibleIndex { get { return visibleIndex; } }
        public ASPxGridView Grid { get { return grid; } }
        public object GetValue(string fieldName)
        {
            if (values.ContainsKey(fieldName)) return values[fieldName];
            return Grid.GetRowValues(VisibleIndex, fieldName);
        }
        public void SetValue(string fieldName, object value)
        {
            values[fieldName] = value;
        }
    }
}