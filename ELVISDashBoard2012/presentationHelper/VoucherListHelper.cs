﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Common.Function;
using BusinessLogic;

namespace ELVISDashBoard.presentationHelper
{
    public static class VoucherListHelper
    {
        public static string GetUrl(this Page p, int roleClass, string docNumber, string docYear, DateTime d, string transCd ="")
        {
            LogicFactory logic = LogicFactory.Get();
            string accruedTt = logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219).str();

            string detailURL = "";
            if (docNumber.isEmpty() || docYear.isEmpty()) return detailURL;
            GlobalResourceData resx = new GlobalResourceData();

            if (docNumber.Int() < 600000000)
            {
               
                if (roleClass == 1)
                {
                    if (transCd == accruedTt)
                    {
                        detailURL = p.ResolveClientUrl(string.Format("~/80Accrued/AccrPVForm.aspx?pv_no={0}&pv_year={1}", docNumber, docYear));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3001"));
                    }
                    else
                    {
                        detailURL = p.ResolveClientUrl(string.Format("~/30PvFormList/PVFormList.aspx?pv_no={0}&pv_year={1}", docNumber, docYear));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3001"));
                    }
                    
                }

                else if (roleClass == 2)
                {
                    detailURL = p.ResolveClientUrl(string.Format("~/30PvFormList/PVList.aspx?date={0}&doc_no={1}", d.ToString("yyyy-MM-dd"), docNumber));
                    detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3002"));
                }
                else
                {
                    detailURL = p.ResolveClientUrl(string.Format("~/30PvFormList/PVApproval.aspx?pvno={0}&pvyear={1}", docNumber, docYear));
                    detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3003"));
                }
                
                
            }
            else
            {
                if (roleClass == 1)
                {
                    if (transCd == accruedTt)
                    {
                        detailURL = p.ResolveClientUrl(string.Format("~/80Accrued/AccrRVForm.aspx?rv_no={0}&rv_year={1}", docNumber, docYear));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4001"));
                    }
                    else
                    {
                        detailURL = p.ResolveClientUrl(string.Format("~/40RvFormList/RVFormList.aspx?rv_no={0}&rv_year={1}", docNumber, docYear));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4001"));
                    }
                }
                else if (roleClass == 2)
                {
                    detailURL = p.ResolveClientUrl(string.Format("~/40RvFormList/RVList.aspx?date={0}&doc_no={1}", d.ToString("yyyy-MM-dd"), docNumber));
                    detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4002"));
                }
                else
                {
                    detailURL = p.ResolveClientUrl(string.Format("~/40RvFormList/RVApproval.aspx?rvno={0}&rvyear={1}", docNumber, docYear));
                    detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4003"));
                }
            }
            return detailURL;
        }
    }
}