﻿using System;
using System.Web;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using Common.Function;
using Common;
using BusinessLogic;
using Common.Data;

namespace ELVISDashBoard
{
    public class FileHandler : IHttpHandler, IRequiresSessionState
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return false; }
        }

        private static string Localize(string p)
        {

            if (!p.isEmpty())
            {
                string fn = p.Replace("\\","").Replace(":","").Replace("/", "\\");
                int ndSlash = fn.IndexOf("\\", 1);
                if (ndSlash> 0)
                {
                    p = fn.Substring(ndSlash + 1);
                }
            }
            return p;
        }

        private static string GetSub(string p)
        {
            if (!p.isEmpty())
            {
                string fn = p;
                int ndSlash = fn.IndexOf("/", 1);
                if (ndSlash > 0)
                {
                    p = fn.Substring(ndSlash + 1);
                }
            }
            return p;
        }

        private string GFn(string fn)
        {
            if (fn.isEmpty()) return "";
            int i = fn.LastIndexOf("/");
            if (i < fn.Length)
                return fn.Substring(i + 1);
            else
                return "";
        }

        private bool Prox(int act, string fx, HttpContext c)
        {
            string url = CommonFunction.CombineUrl(AppSetting.ATTACH_PATH, GetSub(fx));

            LoggingLogic.say("fh", "Prox('{0}')", url); 
            bool ret = false;
            byte [] buffer = new byte[4096];
            try
            {
                WebRequest w = (act == 8) ? HttpWebRequest.Create(url) : FtpWebRequest.Create(url);
                WebResponse r = w.GetResponse();
               
                Stream s = r.GetResponseStream();
                StreamReader b = new StreamReader(s);
                int count = 0;
                //MemoryStream m = new MemoryStream();
                
                c.Response.Clear();
                c.Response.AddHeader("content-disposition", String.Format("attachment;filename=\"{0}\"", GFn(fx)));
                c.Response.ContentType = Xmit.ContentTypeOf(GFn(fx));
                c.Response.Buffer = true;
                do
                {
                    count = s.Read(buffer, 0, buffer.Length);
                    c.Response.OutputStream.Write(buffer, 0, count);
                    //m.Write(buffer, 0, count);
                } while (count > 0);
                //c.Response.BinaryWrite(m.ToArray());

                b.Close();
                s.Close();
                r.Close();
                ret = true;
            }
            catch (Exception ex)
            {
                LoggingLogic.say("fh", LoggingLogic.Trace(ex));
                c.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
            }
            return ret;
        }


        public void ProcessRequest(HttpContext context)
        {
            //write your handler implementation here.

            string fn = "";
            string fx = context.Request.RawUrl;
            int act = 0;
            string acts = "";

            // LoggingLogic.say("fh", fx);
            do
            {
                if (context.Session == null)
                {
                    act = 0;

                    break;
                }

                LogicFactory lf = LogicFactory.Get();
                UserData u = lf.User.SessionUserData(context);
                if (u == null)
                {
                    act = 0;
                    break;
                }

                if (AppSetting.ATTACH_PATH.StartsWith("~/"))
                {
                    fn = Path.Combine(context.Server.MapPath(AppSetting.ATTACH_PATH), Localize(fx));
                    if (!File.Exists(fn))
                    {
                        act = 2;
                        break;
                    }
                    act = 1;

                }
                else if (AppSetting.ATTACH_PATH.StartsWith("http://"))
                {
                    act = 8;
                }

                else if (AppSetting.ATTACH_PATH.StartsWith("ftp://"))
                {
                    act = 16;
                }
                else
                {
                    string ap = AppSetting.ATTACH_PATH;
                    
                    if (!ap.isEmpty() && ap.Length >2 && ap.IndexOf(":\\")==1)
                    {
                        fn = ap + Localize(fx);
                        if (!File.Exists(fn))
                        {
                            act = 2;
                        }
                        else
                        {
                            act = 1;
                        }
                    }
                    else
                    {
                        act = 255;
                    }
                }
            } while (false);

            switch (act)
            {
                case 0: /// no session or Invalid session
                    acts = "[Invalid Session]";
                    LoggingLogic.say("fh", "Unauthorized access to : {0}", fx);
                    context.Response.Clear();
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    context.Response.Redirect(AppSetting.LOGIN_PAGE, true);
                    break;
                case 1: /// serve local or network share files 
                        /// 
                    acts = "[\"" + fn + "\"]";
                    context.Response.Clear();
                    context.Response.AddHeader("content-disposition", String.Format("attachment;filename=\"{0}\"", Path.GetFileName(fn)));
                    context.Response.ContentType = Xmit.ContentTypeOf(fn);
                    context.Response.BinaryWrite(File.ReadAllBytes(fn));
                    break;
                case 2: // local file not found
                    acts = "not found: [\"" + fn + "\"]";
                    context.Response.Clear();
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    break;
                case 8: // reachable http
                    acts = "[http]";
                    Prox(act, fx, context);
                    break;
                case 16: // reachable ftp 
                    acts = "[ftp]";
                    Prox(act, fx, context);
                    break;

                case 255:
                    acts = "[ERR]";
                    context.Response.Clear();
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;

                default:
                    acts = "[NULL]";
                    context.Response.Clear();
                    break;
            }
            LoggingLogic.say("fh", "{0}\t{1}", acts, fx);
        }

        #endregion
    }
}
