﻿<%@ Page Title="Invoice List" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="InvoiceList.aspx.cs" Inherits="ELVISDashboard.Invoice.InvoiceList" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xPre" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">
        function InvoiceDateValidation(s, e) {
            if (!s) {
                e.isValid = false;
            }
            var selectedDate = s.date;
            if (selectedDate == null || selectedDate == false)
                return;
            if (selectedDate < calInvoiceDateFrom.date)
                e.isValid = false;
        }

        function openOtherAmount(pvNo, pvYear) {
            PageMethods.otherAmount_Click(pvNo, pvYear);
        }

        function ClearFileUpload() {
            var fu = document.getElementById("<%= uploadAttachment.ClientID %>");
            if (fu != null) {
                try {
                    fu.setAttribute("type", "input");
                    fu.setAttribute("type", "file");
                }
                catch (ex) {
                    fu.outerHTML = fu.outerHTML;
                }
            }
        }

        function UpdateUploadButton() {
            if (cboxAttachmentCategory.GetText(0) != "" && tboxAttachmentDescription.GetText(0) != ""
                && uploadAttachment.GetText(0) != "") {
                btSendUploadAttachment.SetEnabled(true);
            } else {
                btSendUploadAttachment.SetEnabled(false);
            }
        }

        function Uploader_OnUploadStart() {
            btSendUploadAttachment.SetEnabled(false);
        }

        function DoSearch() {
            var bu = $find("btnSearch");
            if (bu.length > 0) {
                bu.click();
            }
        }

        /*
        function Uploader_OnFileUploadComplete(args) {
        $find('bhvrPopUpUploadAttachment').hide();
        }*/
        function CloseDivisionLookup() {
            DivisionLookup.ConfirmCurrentSelection();
            DivisionLookup.HideDropDown();
        }

        function CloseGridLookup() {
            txtVendorCode.ConfirmCurrentSelection();
            txtVendorCode.HideDropDown();
        }

        function UploadAttachment_Click(s, e) {
            var fn = $("#uploadAttachment").val();
            var sel = true;
            if (fn.indexOf("&") >= 0 || fn.indexOf("+") >= 0 || fn.indexOf("%") >= 0 || fn.indexOf("?") >= 0 || fn.indexOf("*") >= 0) {
                sel = confirm("Uploaded file name contains banned & + % * ? chars \r\n Press Cancel to rename \r\n or Proceed Upload (replace banned chars with '_' ) ?");
            }
            loading();
            return sel;
        }

        function hidePop() {
            var pop = $find("AlertConvertPVPopUp");
            pop.hide();
        }

        function assignPopClick() {
            if ($("#docLink").length > 0) {
                $("#docLink").click(function () {
                    hidePop();
                });
            }
        }
    </script>
    <style type="text/css">
        .lbl
        {
            float: left;
            width: 160px;
            margin: 0px 0px 0px 0px;
        }
        .lefty
        {
            float: left;
            margin: 0px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
        }
        .dateEntry
        {
            float: left;
            width: 120px;
            margin: 0px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            margin: 0px 0px 0px 0px;
        }
        .col1
        {
            float: left;
            display: inline;
            width: 54%;
            padding: 0px 0px 0px 0px;
            margin: 0px 0px 0px 0px;
        }
        .col2
        {
            float: right;
            text-align: left;
            display: inline;
            width: 45%;
            padding: 0px 0px 0px 0px;
            margin: 0px 0px 0px 0px;
        }
        .to
        {
            float: left;
            width: 22px;
            clear: none;
            padding: 0px 0px 0px 0px;
            margin: 0px 5px 0px 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <div id="downloadDiv" class="WRN" runat="server" visible="false">
                <div class="statusWRN">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </div>    
                    <p class="statusMessage">
                    <asp:Literal runat="server" ID="litMessage2" EnableViewState="false"></asp:Literal>
                    <asp:HyperLink runat="server" ID="linkDownloadError" Text="Error list file"></asp:HyperLink>                    
                </p>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="INVOICE LIST" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <div class="row">
                    <div class="col1">
                        <div class="lbl">
                            Invoice Date From</div>
                        <div class="dateEntry">
                            <dx:ASPxDateEdit ID="calInvoiceDateFrom" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calInvoiceDateFrom" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="" />
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                        <div class="to">
                            To
                        </div>
                        <div class="lefty">
                            <dx:ASPxDateEdit ID="calInvoiceDateTo" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calInvoiceDateTo" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="">
                                </ValidationSettings>
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                    </div>
                    <div class="col2">
                        <div class="lbl">
                            Assigned Division</div>
                        <div class="lefty">
                            <asp:Label ID="lblDivision" runat="server" Width="155px" BackColor="Silver" Visible="false"></asp:Label>
                            <dx:ASPxGridLookup ID="DivisionLookup" runat="server" Width="155px" DataSourceID="dsDivision"
                                ClientIDMode="Static" SelectionMode="Multiple" KeyFieldName="DIVISION_ID" TextFormatString="{0}"
                                MultiTextSeparator=";" CssClass="display-inline-table" IncrementalFilteringMode="Contains">
                                <GridViewProperties EnableCallBacks="false">
                                    <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                    <SettingsPager PageSize="10" />
                                    <Templates>
                                        <StatusBar>
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 100%" />
                                                    <td onclick="return _aspxCancelBubble(event)">
                                                        <dx:ASPxButton ID="CloseDivLookup" runat="server" AutoPostBack="false" Text="Close"
                                                            ClientSideEvents-Click="CloseDivisionLookup" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </StatusBar>
                                    </Templates>
                                </GridViewProperties>
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="DIVISION_ID" Width="30px" VisibleIndex="0">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Division Name" FieldName="DIVISION_NAME" Width="115px"
                                        VisibleIndex="1">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="2" Width="30px">
                                        <EditButton Visible="false" Text=" ">
                                        </EditButton>
                                        <UpdateButton Visible="false" Text=" ">
                                        </UpdateButton>
                                        <NewButton Visible="false" Text=" ">
                                        </NewButton>
                                        <CancelButton Visible="false" Text=" ">
                                        </CancelButton>
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                    </dx:GridViewCommandColumn>
                                </Columns>
                            </dx:ASPxGridLookup>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col1">
                        <div class="lbl">
                            Invoice No</div>
                        <div class="lefty">
                            <asp:TextBox runat="server" ID="txtInvoiceNo" Width="85px"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col2">
                        <div class="lbl">
                            Last Status</div>
                        <div style="float: left">
                            <dx:ASPxDropDownEdit ID="ddlLastStatus" ClientInstanceName="ddlLastStatus" runat="server"
                                Width="220px" DropDownWindowHeight="100px">
                                <DropDownWindowStyle BackColor="#EDEDED" />
                                <DropDownWindowTemplate>
                                    <dx:ASPxListBox runat="server" ID="LastStatusListBox" ClientInstanceName="LastStatusListBox"
                                        SelectionMode="CheckColumn" Width="100%" Height="100px" TextField="Description"
                                        ValueField="Code">
                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                        <ClientSideEvents SelectedIndexChanged="function(s,e) {
                                                var selectedItems = s.GetSelectedItems();
                                                var selectedText = GetSelectedListItemsText(selectedItems);
                                                ddlLastStatus.SetText(selectedText);
                                        }" />
                                    </dx:ASPxListBox>
                                    <table style="width: 100%" cellspacing="0" cellpadding="4">
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxButton ID="btClearStatusList" AutoPostBack="False" runat="server" Text="Clear"
                                                    CssClass="display-inline-table">
                                                    <ClientSideEvents 
                                                        Click="function(s, e){ LastStatusListBox.UnselectAll(); ddlLastStatus.SetText(''); }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btCloseStatusList" AutoPostBack="False" runat="server" Text="Close"
                                                    CssClass="display-inline-table">
                                                    <ClientSideEvents Click="function(s, e){ ddlLastStatus.HideDropDown(); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </DropDownWindowTemplate>
                            </dx:ASPxDropDownEdit>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col1">
                        <div class="lbl">
                            Upload Date</div>
                        <div class="dateEntry">
                            <dx:ASPxDateEdit ID="calUploadDateFrom" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calUploadDateFrom" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="" />
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                        <div class="to">
                            To</div>
                        <div class="lefty">
                            <dx:ASPxDateEdit ID="calUploadDateTo" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calUploadDateTo" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="">
                                </ValidationSettings>
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                    </div>
                    <div class="col2">
                     <div class="lbl">
                            Submit Date</div>
                        <div class="dateEntry">
                            <dx:ASPxDateEdit ID="calSubmitDateFrom" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calSubmitDateFrom" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="" />
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                        <div class="to">
                            To</div>
                        <div class="lefty">
                            <dx:ASPxDateEdit ID="calSubmitDateTo" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calSubmitDateTo" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="">
                                </ValidationSettings>
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col1">
                       
                        <div class="lbl">
                            Received Date</div>
                        <div class="dateEntry">
                            <dx:ASPxDateEdit ID="calReceivedDateFrom" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calReceivedDateFrom" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="" />
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                        <div class="to">
                            To</div>
                        <div class="lefty">
                            <dx:ASPxDateEdit ID="calReceivedDateTo" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calReceivedDateTo" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="">
                                </ValidationSettings>
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>


                       
                    </div>
                    <div class="col2">
                        <div class="lbl">
                            Convert Date</div>
                        <div class="dateEntry">
                            <dx:ASPxDateEdit ID="calConvertDateFrom" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calConvertDateFrom" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="" />
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                        <div class="to">
                            To</div>
                        <div class="lefty">
                            <dx:ASPxDateEdit ID="calConvertDateTo" runat="server" Width="105px" DisplayFormatString="dd/MM/yyyy"
                                ClientInstanceName="calConvertDateTo" ClientIDMode="Static" EditFormatString="dd/MM/yyyy"
                                UseMaskBehavior="true" AutoPostBack="true">
                                <ValidationSettings ValidateOnLeave="true" SetFocusOnError="true" ErrorFrameStyle-Border-BorderColor="Red"
                                    ErrorText="">
                                </ValidationSettings>
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col1">
                          <div class="lbl">
                            Vendor Code
                        </div>
                        <div class="lefty">
                            <dx:ASPxGridLookup runat="server" ID="txtVendorCode" ClientInstanceName="txtVendorCode"
                                SelectionMode="Multiple" KeyFieldName="VENDOR_CD" TextFormatString="{0}" Width="100px"
                                OnLoad="lkpgridVendorCode_Load" MultiTextSeparator=";" CssClass="display-inline-table"
                                IncrementalFilteringMode="Contains">
                                <GridViewProperties EnableCallBacks="false">
                                    <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                    <SettingsPager PageSize="7" />
                                    <Templates>
                                        <StatusBar>
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 100%" />
                                                    <td onclick="return _aspxCancelBubble(event)">
                                                        <dx:ASPxButton ID="CloseVendor" runat="server" AutoPostBack="false" Text="Close"
                                                            ClientSideEvents-Click="CloseGridLookup" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </StatusBar>
                                    </Templates>
                                </GridViewProperties>
                                <ClientSideEvents TextChanged="function(s,e) { __doPostBack('txtVendorCode',txtVendorCode.GetGridView().GetFocusedRowIndex()); }" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="20px" />
                                    <dx:GridViewDataTextColumn Caption="Vendor Code" FieldName="VENDOR_CD" Width="75px">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Search Terms" FieldName="SEARCH_TERMS" Width="75px">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Vendor Name" FieldName="VENDOR_NAME" Width="150px">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridLookup>
                            <dx:ASPxLabel ID="lblVendorCode" runat="server" CssClass="display-inline-table" OnLoad="VendorCode_TextChanged" />
                            <asp:TextBox ID="lblVendorName" runat="server" CssClass="disableFields" ReadOnly="true"
                                Width="200px" />
                        </div>


                    </div>
                    <div class="col2">
                       <div class="btnRightLong">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" ClientIDMode="Static" 
                            OnClientClick="loading()" />
                        <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" />
                    </div>
                    </div>
                </div>
                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row" style="width: 618px; display: inline" runat="server" id="DownloadTemplateLinkDiv">
        Download Template: &nbsp; 
        <a href="../ExcelTemplate/Invoice Upload-Template-Export.xls">Export</a>&nbsp;
        <a href="../ExcelTemplate/Invoice Upload-Template-Import.xls">Import</a>&nbsp;
        <a href="../ExcelTemplate/Invoice Upload-Template-TAM.xls">TAM</a>&nbsp;
        <a href="../ExcelTemplate/Invoice Upload-Template-TMC.xls">TMC</a>
    </div>
    <div class="row">
        <div class="rowwarphalf" runat="server" id="UploadDiv" style="vertical-align: middle;">
            <asp:FileUpload ID="fuInvoiceList" runat="server" />
            <asp:Button runat="server" ID="btnUpload" Text="Upload" OnClick="btnUpload_Click" />
        </div>
        <div class="rowwrapright">
            <asp:UpdatePanel runat="server" ID="UpdatePanelDeleteButton">
                <ContentTemplate>
                    <div class="btnRightLong">
                        <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:UpdatePanel runat="server" ID="UpdatePanelGrid">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
            
        </Triggers>
        <ContentTemplate>
            <div>
                <dx:ASPxGridView ID="gridInvoice" runat="server" AutoGenerateColumns="False" Width="990px"
                    ClientInstanceName="gridInvoice" OnHtmlRowCreated="gridInvoice_HtmlRowCreated"
                    KeyFieldName="INVOICE_NO" OnCustomCallback="grid_CustomCallback" EnableCallBacks="False"
                    Styles-AlternatingRow-CssClass="even">
                    <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" />
                    <Columns>
                        <dx:GridViewCommandColumn VisibleIndex="0" ShowSelectCheckbox="True" Width="30px"
                            HeaderStyle-VerticalAlign="Middle">
                            <UpdateButton Visible="false" Text=" ">
                            </UpdateButton>
                            <NewButton Visible="false" Text=" ">
                            </NewButton>
                            <CancelButton Visible="false" Text=" ">
                            </CancelButton>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderTemplate>
                                <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" CheckState="Unchecked">
                                <ClientSideEvents CheckedChanged="function(s, e) { gridInvoice.SelectAllRowsOnPage(s.GetChecked()); }"/>
                                </dx:ASPxCheckBox>
                            </HeaderTemplate>
                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Caption="Invoice No" FieldName="INVOICE_NO" VisibleIndex="1"
                            Width="120px">
                            <PropertiesTextEdit>
                            </PropertiesTextEdit>
                            <DataItemTemplate>
                                <a href="javascript:void(0)" onclick="javascript:openWin('InvoiceListDet.aspx?invoiceNo=<%# Eval("INVOICE_NO")%>&invoiceDate=<%# CommonFunction.Eval_Date(Eval("INVOICE_DATE"), false)%>&vendorCode=<%# Eval("VENDOR_CD")%>&vendorName=<%# Eval("VENDOR_NAME")%>&division=<%# HttpUtility.UrlDecode(lblDivision.Text)%>&invoiceStatus=<%# Eval("STATUS_CD")%>','ELVIS_InvoiceListDet')">
                                    <span class="smallFont">
                                        <asp:Literal runat="server" ID="litGridPVNo" Text='<%# Bind("INVOICE_NO")%>'></asp:Literal></span>
                                </a>
                                <asp:Literal runat="server" ID="litGridInvoiceNo" Text='<%# Eval("INVOICE_NO")%>'
                                    Visible="false"></asp:Literal>
                                <asp:HiddenField runat="server" ID="hidInvoiceNo" Value='<%# Eval("INVOICE_NO")%>'>
                                </asp:HiddenField>
                                <asp:HiddenField runat="server" ID="hidConvertFlag" Value='<%# Eval("CONVERT_FLAG")%>'>
                                </asp:HiddenField>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Vendor Name" VisibleIndex="2" Width="270px" FieldName="VENDOR_NAME">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidVendorName" Value='<%# Eval("VENDOR_NAME")%>'>
                                </asp:HiddenField>
                                <asp:Literal runat="server" ID="litVendorName" Text='<%# Bind("VENDOR_NAME") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Last Status" FieldName="INVOICE_STATUS" VisibleIndex="3"
                            Width="80px">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Invoice Date" VisibleIndex="4" Width="80px" FieldName="INVOICE_DATE">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidInvoiceDate" Value='<%# Eval("INVOICE_DATE")%>'>
                                </asp:HiddenField>
                                <asp:Literal runat="server" ID="litInvoiceDate" Text='<%# CommonFunction.Eval_Date(Eval("INVOICE_DATE"), false) %>'></asp:Literal>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewBandColumn Caption="Total Amount" VisibleIndex="5" HeaderStyle-Wrap="False">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="IDR" FieldName="IDR" VisibleIndex="0" Width="80px">
                                    <CellStyle HorizontalAlign="Right" />
                                    <DataItemTemplate>
                                        <asp:Literal runat="server" ID="litGridAmountIdr" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("IDR")) %>'></asp:Literal>
                                        <asp:HiddenField runat="server" ID="hidAmountIdr" Value='<%# Bind("IDR")%>'></asp:HiddenField>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="USD" FieldName="USD" VisibleIndex="1" Width="80px">
                                    <CellStyle HorizontalAlign="Right" />
                                    <DataItemTemplate>
                                        <asp:Literal runat="server" ID="litGridAmountUsd" Text='<%# CommonFunction.Eval_Curr("USD", Eval("USD")) %>'></asp:Literal>
                                        <asp:HiddenField runat="server" ID="hidAmountUsd" Value='<%# Bind("USD")%>'></asp:HiddenField>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="JPY" FieldName="JPY" VisibleIndex="2" Width="80px">
                                    <CellStyle HorizontalAlign="Right" />
                                    <DataItemTemplate>
                                        <asp:Literal runat="server" ID="litGridAmountJpy" Text='<%# CommonFunction.Eval_Curr("JPY", Eval("JPY")) %>'></asp:Literal>
                                        <asp:HiddenField runat="server" ID="hidAmountJpy" Value='<%# Bind("JPY")%>'></asp:HiddenField>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Oth" FieldName="OTHERS" VisibleIndex="3" Width="40px">
                                    <CellStyle HorizontalAlign="Right" />
                                    <DataItemTemplate>
                                        <asp:LinkButton runat="server" ID="linkOtherAmount" OnClick="LoadGridPopUpOtherAmount"
                                            Text='<%# Eval("OTHERS") %>' CssClass="smallFont" Visible='<%# !((Eval("OTHERS")).ToString().Equals("0")) %>'></asp:LinkButton>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle Wrap="False" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewDataTextColumn Caption="Attachment" VisibleIndex="6">
                            <DataItemTemplate>
                                <asp:Button runat="server" ID="btnGridViewAttachment" CommandName="Attachment" Text="View/Edit"
                                    OnClick="LoadGridAttachment" Style="font-size: 8pt !important;" />
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Upload Date" VisibleIndex="7" Width="125px" FieldName="UPLOAD_DATE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn Caption="Submit Date" VisibleIndex="8" Width="125px" FieldName="SUBMIT_DATE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn Caption="Receive Date" VisibleIndex="9" Width="125px"
                            FieldName="RECEIVE_DATE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn Caption="Convert Date" VisibleIndex="10" Width="125px"
                            FieldName="CONVERT_DATE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataDateColumn Caption="Planning Payment Date" VisibleIndex="11" Width="120px"
                            FieldName="PLANNING_PAYMENT_DATE">
                            <HeaderStyle HorizontalAlign="Center" Wrap="True"/>
                            <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn Caption="Transaction Type" FieldName="TRANSACTION_CD"
                            VisibleIndex="12" Width="150px">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidTransactionCd" Value='<%# Eval("TRANSACTION_CD")%>'>
                                </asp:HiddenField>
                                <asp:Literal runat="server" ID="litTransactionCd" Text='<%# Bind("TRANSACTION_NAME") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Vendor Code" FieldName="VENDOR_CD" VisibleIndex="13"
                            Width="100px">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidVendorCd" Value='<%# Eval("VENDOR_CD")%>'>
                                </asp:HiddenField>
                                <asp:Literal runat="server" ID="litVendorCd" Text='<%# Bind("VENDOR_CD") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="14" Width="300px"
                            FieldName="DESCRIPTION">
                            <DataItemTemplate>
                                <asp:Literal runat="server" ID="litDesc" Text='<%# Eval("DESCRIPTION") %>'></asp:Literal>
                                <asp:HiddenField runat="server" ID="hidDesc" Value='<%# Bind("DESCRIPTION")%>'></asp:HiddenField>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="PV No" FieldName="PV_NO" VisibleIndex="15" Width="80px">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidPvNo" Value='<%# Eval("PV_NO")%>'></asp:HiddenField>
                                <asp:HyperLink runat="server" ID="linkPV" Text="PV_NO" NavigateUrl="#" CssClass="smallFont" />
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="PV Year" FieldName="PV_YEAR" VisibleIndex="16"
                            Width="80px">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidPvYear" Value='<%# Eval("PV_YEAR")%>'></asp:HiddenField>
                                <asp:Literal runat="server" ID="litPvYear" Text='<%# Bind("PV_YEAR") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Assigned Division" FieldName="DIVISION_ID" VisibleIndex="17"
                            Width="120px">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidDivisionId" Value='<%# Eval("DIVISION_ID")%>'>
                                </asp:HiddenField>
                                <asp:Literal runat="server" ID="litDivisionId" Text='<%# Bind("DIVISION_NAME") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Invoice Tax No" FieldName="TAX_NO" VisibleIndex="18"
                            Width="120px">
                            <DataItemTemplate>
                                <asp:HiddenField runat="server" ID="hidTaxNo" Value='<%# Eval("TAX_NO")%>'></asp:HiddenField>
                                <asp:Literal runat="server" ID="litTaxNo" Text='<%# Bind("TAX_NO") %>'></asp:Literal>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Tax Date" VisibleIndex="19" FieldName="INVOICE_TAX_DATE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn Caption="TMMIN Invoice" FieldName="TMMIN_INVOICE" VisibleIndex="20"
                            Width="120px">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Remark" FieldName="REMARK" VisibleIndex="21"
                            Width="150px">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewBandColumn Caption="Cont Qty" VisibleIndex="22">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="20" FieldName="CONT_QTY_20" VisibleIndex="0"
                                    Width="50px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="40" FieldName="CONT_QTY_40" VisibleIndex="1"
                                    Width="50px">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Created" VisibleIndex="23">
                            <Columns>
                                <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="1" FieldName="CREATED_DT"
                                    Width="125px" SortIndex="0" SortOrder="Descending">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <Settings SortMode="Value" AllowSort="True" />
                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn Caption="By" VisibleIndex="0" FieldName="CREATED_BY">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="Changed " VisibleIndex="24">
                            <Columns>
                                <dx:GridViewDataDateColumn Caption="Date" VisibleIndex="1" FieldName="CHANGED_DT"
                                    Width="125px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn Caption="By" VisibleIndex="0" FieldName="CHANGED_BY">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewDataCheckColumn Caption="Convert Flag" FieldName="CONVERT_FLAG" VisibleIndex="25" Visible="false">
                            <PropertiesCheckEdit ValueUnchecked="0" ValueChecked="1" ValueGrayed="" ValueType="System.Int32" />
                            <%--<EditItemTemplate>
                                <dx:ASPxCheckBox runat="server" ID="cbConvertFlag">
                                </dx:ASPxCheckBox>
                            </EditItemTemplate>--%>
                        </dx:GridViewDataCheckColumn>
                    </Columns>
                    <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                    <SettingsPager Visible="false" />
                    <Settings ShowStatusBar="Visible" />
                    <SettingsLoadingPanel ImagePosition="Top" />
                    <Styles>
                        <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False">
                        </Header>
                        <AlternatingRow CssClass="even">
                        </AlternatingRow>
                        <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                        </Cell>
                    </Styles>
                    <Templates>
                        <StatusBar>
                            <div style="text-align: left;">
                                Records per page:
                                <select onchange="gridInvoice.PerformCallback(this.value);">
                                    <option value="5" <%# WriteSelectedIndex(5) %>>5</option>
                                    <option value="10" <%# WriteSelectedIndex(10) %>>10</option>
                                    <option value="15" <%# WriteSelectedIndex(15) %>>15</option>
                                    <option value="20" <%# WriteSelectedIndex(20) %>>20</option>
                                    <option value="25" <%# WriteSelectedIndex(25) %>>25</option>
                                </select>&nbsp; <a title="First" href="JavaScript:gridInvoice.GotoPage(0);">&lt;&lt;</a>
                                &nbsp; <a title="Prev" href="JavaScript:gridInvoice.PrevPage();">&lt;</a> &nbsp;
                                Page
                                <input type="text" onchange="gridInvoice.GotoPage(parseInt(this.value, 10) - 1)"
                                    onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridInvoice.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                    value="<%# (gridInvoice.PageIndex >= 0)? gridInvoice.PageIndex + 1 : 1 %>" style="width: 20px" />
                                of
                                <%# gridInvoice.PageCount%>&nbsp;
                                <%# (gridInvoice.VisibleRowCount > 1) ? "(" + gridInvoice.VisibleRowCount + " Items)&nbsp;" : ""%>
                                <a title="Next" href="JavaScript:gridInvoice.NextPage();">&gt;</a> &nbsp; <a title="Last"
                                    href="JavaScript:gridInvoice.GotoPage(<%# gridInvoice.PageCount - 1 %>);">&gt;&gt;</a>
                                &nbsp;
                            </div>
                        </StatusBar>
                    </Templates>
                </dx:ASPxGridView>
                <asp:LinqDataSource ID="dsDivision" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                    TableName="vw_Division" />
                <dx:LinqServerModeDataSource ID="ldsLog" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                    TableName="vw_Invoice" OnSelecting="ldsLog_Selecting" />
            </div>
            <div class="rowbtn">
                <asp:Button ID="btnExportExcel" runat="server" Text="Download" CssClass="xlongButton"
                    SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="shortButton" SkinID="shortButton"
                    OnClick="btnSubmit_Click" />                
                <asp:Button ID="btnReceived" runat="server" Text="Received" CssClass="shortButton"
                    SkinID="shortButton" OnClick="btnReceived_Click" />
                <asp:Button ID="btnInvoiceReceipt" runat="server" Text="Invoice Receipt" CssClass="xlongButton" SkinID="xlongButton"
                    OnClick="btnInvoiceReceipt_Click" />
                <asp:Button ID="btnConvertPV" runat="server" Text="Convert to PV" CssClass="xlongButton"
                    SkinID="xlongButton" OnClick="btnConvertPV_Click" />
                <div class="btnright">
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                </div>
            </div>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server"></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px"
                            OnClick="BtnCancelConfirmationDelete_Click" />
                        <!-- popup progress -->
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <center>
        <asp:Button ID="hidBtnDetail" runat="server" Style="display: none;" />
        <cc1:ModalPopupExtender ID="popUpList" runat="server" TargetControlID="hidBtnDetail"
            PopupControlID="panelModal" CancelControlID="btnClosePopUp" BackgroundCssClass="modalBackground" />
        <asp:Panel runat="server" ID="panelModal" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="upnModal">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitPopUpMessage"></asp:Literal>
                    <asp:Literal runat="server" ID="LitPopUpModal"></asp:Literal>
                    <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left" width="400px">
                        <tr style="border: 1px solid black">
                            <td>
                                <asp:Label ID="lblPopUpSearchCd" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSearchCd" Width="98%"></asp:TextBox>
                            </td>
                            <td style="text-align: right">
                                <asp:Button runat="server" ID="btnClearPopUp" Text="Clear" OnClick="btnClearPopUp_Click" />
                                &nbsp;
                                <asp:Button runat="server" ID="btnSearchPopUp" Text="Search" OnClick="btnSearchPopUp_Click" />
                            </td>
                        </tr>
                        <tr style="border: 1px solid black">
                            <td>
                                <asp:Label ID="lblPopUpSearchDesc" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtSearchDesc" Width="98%"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <br />
                                <dx:ASPxGridView ID="gridPopUpVendorCd" runat="server" Width="400px" OnPageIndexChanged="gridPopUpVendorCd_PageIndexChanged"
                                    Visible="False" KeyFieldName="VENDOR_CD" AutoGenerateColumns="False" ClientInstanceName="gridPopUpVendorCd"
                                    OnHtmlRowCreated="gridPopUpVendorCd_HtmlRowCreated">
                                    <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                    </SettingsPager>
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="40px" Name="NO">
                                            <DataItemTemplate>
                                                <asp:Literal ID="litGridNoVendorCd" runat="server"></asp:Literal>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="VENDOR_CD">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="VENDOR_NAME">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="3" Width="30px">
                                            <EditButton Visible="false" Text=" ">
                                            </EditButton>
                                            <UpdateButton Visible="false" Text=" ">
                                            </UpdateButton>
                                            <NewButton Visible="false" Text=" ">
                                            </NewButton>
                                            <CancelButton Visible="false" Text=" ">
                                            </CancelButton>
                                            <HeaderTemplate>
                                                <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server"
                                                    CheckState="Unchecked" Style="text-align: center">
                                                    <ClientSideEvents 
                                                        CheckedChanged="function(s, e) { gridPopUpVendorCd.SelectAllRowsOnPage(s.GetChecked()); }"/>
                                                </dx:ASPxCheckBox>
                                            </HeaderTemplate>
                                        </dx:GridViewCommandColumn>
                                    </Columns>
                                    <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                    <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                        VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                    <Styles>
                                        <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                        </Header>
                                    </Styles>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" colspan="3">
                                <br />
                                <asp:Button runat="server" ID="btnSend" Text="Send" OnClick="btnSend_Click" />
                                &nbsp;
                                <asp:Button runat="server" ID="btnClosePopUp" Text="Close" OnClick="btnClosePopUp_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <!-- Pop up Other Amount -->
    <center>
        <asp:Button ID="BtnHidOtherAmount" runat="server" Style="display: none;" />
        <cc1:ModalPopupExtender ID="ModalPopupOtherAmount" runat="server" TargetControlID="BtnHidOtherAmount"
            PopupControlID="panelOtherAmount" CancelControlID="BtnCloseOtherAmount" BackgroundCssClass="modalBackground" />
        <asp:Panel runat="server" ID="panelOtherAmount" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="UpdatePanelOtherAmount">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgOtherAmount"></asp:Literal>
                    <asp:Literal runat="server" ID="LitModalOtherAmount"></asp:Literal>
                    <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left" width="400px">
                        <tr>
                            <td colspan="3">
                                <br />
                                <dx:ASPxGridView ID="gridOtherAmount" runat="server" Width="400px" Visible="true"
                                    KeyFieldName="CURRENCY_CD" AutoGenerateColumns="false" OnHtmlRowCreated="gridOtherAmount_HtmlRowCreated">
                                    <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                    </SettingsPager>
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="40px" Name="NO">
                                            <DataItemTemplate>
                                                <asp:Literal ID="litGridNoOtherAmount" runat="server"></asp:Literal>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Currency" VisibleIndex="1" FieldName="CURRENCY_CD">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Amount" VisibleIndex="5" FieldName="AMOUNT">
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" ID="litGridAmountOther" Text='<%# CommonFunction.Eval_Curr(Eval("CURRENCY_CD"), Eval("CURRENCY_AMOUNT")) %>'></asp:Literal>
                                                <asp:HiddenField runat="server" ID="hidAmountOther" Value='<%# CommonFunction.Eval_Curr(Eval("CURRENCY_CD"), Eval("CURRENCY_AMOUNT")) %>'>
                                                </asp:HiddenField>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                    <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                        VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                    <Styles>
                                        <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                        </Header>
                                    </Styles>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" colspan="3">
                                <br />
                                <asp:Button runat="server" ID="BtnCloseOtherAmount" Text="Close" OnClick="BtnCloseOtherAmount_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlAlertConvertPV" runat="server">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidConvertPVButton" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="AlertConvertPVPopUp" runat="server" TargetControlID="hidConvertPVButton"
                ClientIDMode="Static"
                PopupControlID="ConvertPVPanel" CancelControlID="BtnCloseConvertPV" BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConvertPVPanel" runat="server" CssClass="informationPopUpX" 
                    Height="100px">
                    <center>
                        <asp:Label ID="LblConvertPV" runat="server" Text=""></asp:Label>
                        <br /><br />
                        <span style="font-style: italic; font-weight: bold; font-size:larger;">Please click the link above</span>
                        <asp:Button ID="BtnCloseConvertPV" Text="Close" runat="server" Width="60px" Style="display: none;"/>
                    </center>
                </asp:Panel>    
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
    <!-- Pop up Attachment -->
    <center>
        <asp:Panel runat="server" ID="panelAttachment" CssClass="speakerPopupList">
            <asp:Button ID="ButtonHidAttachment" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ModalPopupAttachment" runat="server" TargetControlID="ButtonHidAttachment"
                PopupControlID="UpdatePanelAttachment" CancelControlID="ButtonCloseAttachment"
                BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel runat="server" ID="UpdatePanelAttachment">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgAttachment" />
                    <asp:Literal runat="server" ID="LitModalAttachment" />
                    <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left; background-color: White;" width="850px">
                        <tr>
                            <td colspan="3">
                                <br />
                                <dx:ASPxGridView ID="gridAttachment" runat="server" Width="850px" Visible="true"
                                    KeyFieldName="SequenceNumber" Styles-AlternatingRow-CssClass="even" AutoGenerateColumns="false"
                                    OnHtmlRowCreated="gridAttachment_HtmlRowCreated">
                                    <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                    </SettingsPager>
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="ReferenceNumber" ReadOnly="True" Caption="Reference No"
                                            VisibleIndex="1">
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SequenceNumber" ReadOnly="True" Caption="Item No"
                                            VisibleIndex="2" Width="60px">
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" ID="litGridSequenceNumber" Text="<%# Bind('SequenceNumber') %>" />
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CategoryCode" ReadOnly="True" Caption="Attachment Category"
                                            VisibleIndex="3" Width="140px">
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" ID="litGridAttach" Text="<%# Bind('CategoryName') %>"></asp:Literal>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Description" ReadOnly="True" Caption="Description"
                                            VisibleIndex="4">
                                            <CellStyle VerticalAlign="Middle" Wrap="False">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FileName" ReadOnly="True" Caption="Filename"
                                            VisibleIndex="5" Width="200px">
                                            <DataItemTemplate>
                                                <dx:ASPxHyperLink runat="server" ID="lblGridFilename" Text="<%# Bind('DisplayFileName') %>"
                                                    Target="_blank" NavigateUrl="<%# Bind('Url') %>" />
                                                <asp:HiddenField runat="server" ID="hidGridPath" Value="<%# Bind('PATH') %>" />
                                            </DataItemTemplate>
                                            <CellStyle VerticalAlign="Middle" Wrap="False">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Visible="false" FieldName="PATH" VisibleIndex="6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn VisibleIndex="7" Caption="Action" Width="100px">
                                            <DataItemTemplate>
                                                <asp:Button runat="server" ID="btnGridAttachmentDel" CommandName="Delete" Text="Del"
                                                    OnClick="btnDelAttch_Click" OnClientClick="loading()" />
                                                <asp:Button runat="server" ID="btnGridAttachmentAdd" CommandName="Add" Text="Add"
                                                    OnClick="btnAddAttch_Click" OnClientClick="ClearFileUpload()" />
                                            </DataItemTemplate>
                                            <CellStyle VerticalAlign="Middle" Wrap="False" HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
                                    <SettingsPager AlwaysShowPager="True">
                                    </SettingsPager>
                                    <SettingsEditing Mode="Inline" />
                                    <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="180" />
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" colspan="3">
                                <br />
                                <asp:Button runat="server" ID="ButtonCloseAttachment" Text="Close" OnClick="ButtonClosePopUpAttachment_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
     <!-- File Upload -->
    <asp:Button ID="popUploadAttachment" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="popdlgUploadAttachment" runat="server" DropShadow="true" 
        TargetControlID="popUploadAttachment" PopupControlID="pnpopUploadAttachment"
        BackgroundCssClass="modalBackground" CancelControlID="btCloseUploadAttachment"
        BehaviorID="bhvrPopUpUploadAttachment" />
    <center>
        <asp:Panel runat="server" ID="pnpopUploadAttachment" CssClass="speakerPopupList">
            <table>
                <tr>
                    <td>Category</td>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upnPnlUploadAttachment2">
                            <ContentTemplate>
                                <dx:ASPxComboBox runat="server" ID="cboxAttachmentCategory"
                                                    TextField="Description" ValueField="Code" ValueType="System.String"
                                                    OnLoad="evt_cboxAttachmentCategory_onLoad" Width="220px"
                                                    ClientInstanceName="cboxAttachmentCategory">
                                </dx:ASPxComboBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>
                        <asp:UpdatePanel runat="server" ID="upnPnlUploadAttachment3">
                            <ContentTemplate>
                                <dx:ASPxTextBox runat="server" ID="tboxAttachmentDescription" MaxLength="100" Width="220px"
                                    ClientInstanceName="tboxAttachmentDescription">
                                </dx:ASPxTextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>File</td>
                    <td>
                        <asp:FileUpload runat="server" ID="uploadAttachment" ClientIDMode="Static"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br /></td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                        <asp:Button ID="btSendUploadAttachment" runat="server" Text="Upload" 
                            OnClick="evt_btSendUploadAttachment_clicked" 
                            OnClientClick="return UploadAttachment_Click();"
                             />
                        <asp:Button ID="btCloseUploadAttachment" runat="server" Text="Close" />
                    </td>                            
                </tr>
            </table>
        </asp:Panel>
    </center>
</asp:Content>
