﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._50Invoice;
using Common.Data._50Invoice;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._50Invoice
{
    public partial class InvoiceListDet : BaseCodeBehind
    {
        #region Init
        readonly InvoiceDetailListLogic _InvoiceDetailListLogic = new InvoiceDetailListLogic();
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_5001");
       
        

        #endregion
        //#region Getter Setter for modal button if any
        //private Common.Enum.ModalButton ModalButton
        //{
        //    set
        //    {
        //        ViewState["ModalButton"] = value;
        //    }
        //    get
        //    {
        //        return (Common.Enum.ModalButton)ViewState["ModalButton"];
        //    }
        //}

        //#endregion

      

        #region Getter Setter for Data List
        private List<InvoiceDetailData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<InvoiceDetailData>();
                }
                else
                {
                    return (List<InvoiceDetailData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #region grid
        protected void gridInvoiceDetailList_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                Literal litGridNoGeneralInfo = gridInvoiceDetailList.FindRowCellTemplateControl(e.VisibleIndex, gridInvoiceDetailList.Columns["No"] as GridViewDataColumn, "litGridNo") as Literal;
                if (litGridNoGeneralInfo != null)
                {
                    litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
                }
            }
            catch (Exception ex)
            {
                litMessage.Text = _m.SetMessage(string.Format(resx.Message( "MSTD00002ERR"), ex.Message), "ERR");
            }
        }
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            hidScreenID.Value = _ScreenID;

            if (!IsPostBack)
            {
                PrepareLogout();
                hidInvoiceNo.Value = WebUtility.HtmlEncode(Request.QueryString["invoiceNo"]);
                hidInvoiceDate.Value = WebUtility.HtmlEncode(Request.QueryString["invoiceDate"]);
                hidVendorCode.Value = WebUtility.HtmlEncode(Request.QueryString["vendorCode"]);
                hidVendorName.Value = WebUtility.HtmlEncode(Request.QueryString["vendorName"]);
                hidDivision.Value = WebUtility.HtmlEncode(Request.QueryString["division"]);
                hidStatus.Value = WebUtility.HtmlEncode(Request.QueryString["invoiceStatus"]);
                hidPvNo.Value = WebUtility.HtmlEncode(Request.QueryString["pvNo"]);
                hidPvYear.Value = WebUtility.HtmlEncode(Request.QueryString["pvYear"]);

                litInvoiceNo.Text = hidInvoiceNo.Value;
                litInvoiceDate.Text = hidInvoiceDate.Value;
                litVendorCode.Text = hidVendorCode.Value;
                litVendorName.Text = hidVendorName.Value;
                litAssignDivision.Text = GetDivisionName(hidDivision.Value);
                litInvoiceStatus.Text = hidStatus.Value;


                InvoiceData invdata = _InvoiceDetailListLogic.getInvoiceInfo(hidInvoiceNo.Value);

                int tt = invdata.TRANSACTION_CD??0;
                hidTransactionType.Value = Convert.ToString(tt);

                lblTransactionType.Text = _InvoiceDetailListLogic.getTransactionName(tt);

                gridInvoiceDetailList.DataSourceID = LinqDataSource2.ID;
                gridInvoiceDetailList.DataBind();
            }
        }

        #endregion

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);
            
            btnClose.Visible = true;
        }
        #endregion

        #region Button
        protected void btnClose_Click(object sender, EventArgs e)
        {

        }
        #endregion

        protected void LinqDataSource2_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = _InvoiceDetailListLogic.SearchInqury(hidInvoiceNo.Value, hidInvoiceDate.Value, hidVendorCode.Value,
                          hidVendorName.Value, hidDivision.Value, hidStatus.Value, hidPvNo.Value, hidPvYear.Value, hidTransactionType.Value);

            e.Result = ListInquiry;

            if (ListInquiry.Any())
            {
                litTotalAmount.Text = "<table width='100%'><tr>";
                Dictionary<String, Decimal> amounts = new Dictionary<String, Decimal>();
                foreach (var data in ListInquiry)
                {
                    litInvoiceStatus.Text = data.STATUS_NAME;
                    if (amounts.ContainsKey(data.CURRENCY_CD))
                    {
                        amounts[data.CURRENCY_CD] = amounts[data.CURRENCY_CD] + Decimal.Parse(data.AMOUNT);
                    }
                    else
                    {
                        amounts.Add(data.CURRENCY_CD, Decimal.Parse(data.AMOUNT));
                    }
                }

                foreach (var data in amounts)
                {
                    if (data.Key == "IDR" || data.Key == "JPY")
                    {
                        litTotalAmount.Text += String.Format("<td>{0} {1:N0}</td>", data.Key, data.Value);
                    }
                    else
                    {
                        litTotalAmount.Text += String.Format("<td>{0} {1:N2}</td>", data.Key, data.Value);
                    }
                }
                litTotalAmount.Text += "</tr></table>";
            }
            else
            {
                litTotalAmount.Text = "";
            }
        }
    }
}