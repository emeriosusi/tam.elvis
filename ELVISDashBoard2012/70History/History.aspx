﻿<%@ Page Title="History" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" AutoEventWireup="true"
    CodeBehind="History.aspx.cs" Inherits="ELVISDashBoard._70History.History" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="History Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <div class="row">
                    <label>
                        Doc No</label>
                    <div class="rowwarphalfLeft">
                        <asp:TextBox runat="server" ID="txtDocNo" AutoPostBack="false" Width="200px">
                        </asp:TextBox>
                    </div>
                    
                </div>
                
                <div class="row">
                    <label>
                        Status</label>
                    <div class="rowwarphalfLeft">
                        <asp:DropDownList  runat="server" ID="ddlStatus" Width="200px">
                        </asp:DropDownList>
                    </div>

                </div>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                         OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" OnClientClick="loading()" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <dx:ASPxGridView ID="gridHistory" runat="server" Width="990px" AutoGenerateColumns="False"
                ClientInstanceName="gridHistory" OnHtmlRowCreated="gridHistory_HtmlRowCreated"
                KeyFieldName="REFF_NO" EnableCallBacks="false" Styles-AlternatingRow-CssClass="even"
                OnCustomCallback="grid_CustomCallback" OnPageIndexChanged="gridHistory_PageIndexChanged">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" Width="50px">
                        <DataItemTemplate>
                            <asp:Literal runat="server" ID="litGridNo"></asp:Literal>
                        </DataItemTemplate>
                        <EditItemTemplate>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Doc No" FieldName="REFF_NO"
                        VisibleIndex="2" Width="160px">
                    </dx:GridViewDataTextColumn>
                 
                    <dx:GridViewDataTextColumn Caption="Status"
                        FieldName="STATUS_NAME" VisibleIndex="4" Width="250px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Username" FieldName="USERNAME" Width="200px"
                        VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Process Date" FieldName="ACTUAL_DT"
                        VisibleIndex="6" Width="200px">
                        <PropertiesDateEdit DisplayFormatString="{0:dd/MM/yyyy HH:mm}"></PropertiesDateEdit>
                    </dx:GridViewDataDateColumn>
                </Columns>
                <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                <SettingsEditing Mode="Inline" />
                <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                <SettingsLoadingPanel ImagePosition="Top" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
                <Settings ShowStatusBar="Visible"  />
                <SettingsPager Visible="false" />
                <Templates>
                    <StatusBar>
                    <div style="text-align:left;">
                        Records per page: 
                        <select onchange="gridHistory.PerformCallback(this.value);" >
                            <option value="5" <%# WriteSelectedIndex(5) %> >5</option>
                            <option value="10"<%# WriteSelectedIndex(10) %>  >10</option>
                            <option value="15" <%# WriteSelectedIndex(15) %> >15</option>
                            <option value="20" <%# WriteSelectedIndex(20) %> >20</option>
                            <option value="25" <%# WriteSelectedIndex(25) %> >25</option>
                        </select>&nbsp;
                         <a title="First" href="JavaScript:gridHistory.GotoPage(0);">&lt;&lt;</a> &nbsp; 
                         <a title="Prev" href="JavaScript:gridHistory.PrevPage();">&lt;</a> &nbsp;
                        Page <input type="text" onchange="gridHistory.GotoPage(parseInt(this.value, 10) - 1)"
                        onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridHistory.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                        value="<%# (gridHistory.PageIndex >= 0)? gridHistory.PageIndex + 1 : 1 %>"
                        style="width:20px" /> of <%# gridHistory.PageCount%> &nbsp;
                        <%# (gridHistory.VisibleRowCount > 1) ? "(" + gridHistory.VisibleRowCount + " Items)&nbsp;" : ""%> 
                         <a title="Next" href="JavaScript:gridHistory.NextPage();">&gt;</a> &nbsp;
                         <a title="Last" href="JavaScript:gridHistory.GotoPage(<%# gridHistory.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; 
                    </div>
                    </StatusBar>
                </Templates>
                <SettingsPager AlwaysShowPager="true">
                </SettingsPager>
            </dx:ASPxGridView>
            
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" 
                ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                Select="new (REFF_NO, STATUS_NAME, USERNAME, ACTUAL_DT)"
                OnSelecting="LinqDataSource1_Selecting" 
                TableName="vw_History">
            </asp:LinqDataSource>
            <div class="row">
                <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                    SkinID="xlongButton" OnClick="btnExportExcel_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="rowbtn">
        <div class="btnright">
            <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
        </div>
    </div>
</asp:Content>
