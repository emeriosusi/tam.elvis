﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._70History;
using Common;
using Common.Data;
using Common.Data._70History;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._70History
{
    public partial class History : BaseCodeBehind
    {
        #region Init
        readonly HistoryLogic _HistoryLogic = new HistoryLogic();
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_7001");
        
        #region Getter Setter for Data List        
        private List<HistoryData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<HistoryData>();
                }
                else
                {
                    return (List<HistoryData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            hidScreenID.Value = _ScreenID;

            if (!IsPostBack)
            {
                PrepareLogout();

                GenerateComboData(ddlStatus, "LAST_STATUS_PV_OK", true);
                if (Request.QueryString["pv_no"] != null && Request.QueryString["pv_year"] != null)
                {
                    String pv_no = Request.QueryString["pv_no"];
                    String pv_year = Request.QueryString["pv_year"];
                    txtDocNo.Text = pv_no + pv_year;
                    gridHistory.DataSourceID = LinqDataSource1.ID;
                    gridHistory.DataBind();
                }
                else
                {
                    gridHistory.DataSourceID = null;
                    SetFirstLoad();
                }
                AddEnterComponent(txtDocNo);
                AddEnterComponent(ddlStatus);
                AddEnterAsSearch(btnSearch, _ScreenID);
            }
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);
            
        }
        #endregion

        #region Grid
        protected void gridHistory_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNo = gridHistory.FindRowCellTemplateControl(e.VisibleIndex, gridHistory.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                litGridNo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
            #endregion
        }

        protected void gridHistory_PageIndexChanged(object sender, EventArgs e)
        {
            gridHistory.DataBind();
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = _HistoryLogic.SearchInqury(txtDocNo.Text, ddlStatus.SelectedValue);
            e.Result = ListInquiry;
        }
        #endregion
        #endregion

        #region Buttons
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (ListInquiry != null)
            {
                try
                {
                    string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                    UserData userdata = (UserData)Session["UserData"];
                    string _FileName = "HISTORY" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                    this.Response.Clear();
                    this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.ContentType = "application/vnd.ms-excel";
                    try
                    {
                        this.Response.BinaryWrite(_HistoryLogic.Get(UserName, imagePath, ListInquiry));
                    }
                    catch (Exception ex)
                    {
                        LoggingLogic.err(ex);
                        litMessage.Text = "Cannot get file: " + ex.Message;
                    }
                    this.Response.End();
                }
                catch (Exception Ex)
                {
                    Handle(Ex);
                    throw ;
                }
            }
            else litMessage.Text = _m.SetMessage(resx.Message( "MSTD00018INF"), "MSTD00018INF");
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridHistory.DataSourceID = null;
            gridHistory.DataBind();
            ddlStatus.SelectedValue = null;
            txtDocNo.Text = "";
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateInputSearch())
            {
                gridHistory.DataSourceID = LinqDataSource1.ID;
                gridHistory.DataBind();
            }
        }
        #endregion

        #region Function

        private bool ValidateInputSearch()
        {
       
            return true;
        }
        #endregion

        #region paging
        
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridHistory.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
        }
        protected void grid_CustomCallback(object sender,
    DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            int newPageSize;
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            gridHistory.SettingsPager.PageSize = newPageSize;
            Session[GridCustomPageSizeName] = newPageSize;
            if (gridHistory.VisibleRowCount > 0)
                gridHistory.DataSourceID = LinqDataSource1.ID;
            gridHistory.DataBind();
        }
        protected string WriteSelectedIndex(int pageSize)
        {
            return pageSize == gridHistory.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
    
        #endregion
    }
}