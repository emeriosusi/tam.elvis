﻿using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._30PVList;
using Common.Function;
using DevExpress.Data.Helpers;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELVISDashBoard._30PvFormList
{
    public partial class PVList : BaseCodeBehind
    {
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_3002");
        private SelectableDropDownHelper sel = null;

        private string[][] aFin = new string[][] 
        {
            new string[] { "ACTUAL_DT_VERIFIED_BUDGET", "PLAN_DT_VERIFIED_BUDGET",
                "ACTUAL_LT_VERIFIED_BUDGET", "PLAN_LT_VERIFIED_BUDGET", "SKIP_FLAG_VERIFIED_BUDGET"}, 
            new string[] { "ACTUAL_DT_VERIFIED_COSTING", "PLAN_DT_VERIFIED_COSTING",
                "ACTUAL_LT_VERIFIED_COSTING", "PLAN_LT_VERIFIED_COSTING", "SKIP_FLAG_VERIFIED_COSTING"}, 
            new string[] { "ACTUAL_DT_VERIFIED_FINANCE", "PLAN_DT_VERIFIED_FINANCE", 
                "ACTUAL_LT_VERIFIED_FINANCE", "PLAN_LT_VERIFIED_FINANCE", 
                "SKIP_FLAG_VERIFIED_FINANCE"}, 
            new string[] { "ACTUAL_DT_APPROVED_FINANCE_SH", "PLAN_DT_APPROVED_FINANCE_SH", 
                "ACTUAL_LT_APPROVED_FINANCE_SH", "PLAN_LT_APPROVED_FINANCE_SH",
                "SKIP_FLAG_APPROVED_FINANCE_SH"}, 
            new string[] { "ACTUAL_DT_APPROVED_FINANCE_DPH", "PLAN_DT_APPROVED_FINANCE_DPH", 
                "ACTUAL_LT_APPROVED_FINANCE_DPH", "PLAN_LT_APPROVED_FINANCE_DPH", 
                "SKIP_FLAG_APPROVED_FINANCE_DPH"}, 
            new string[] { "ACTUAL_DT_APPROVED_FINANCE_DH", "PLAN_DT_APPROVED_FINANCE_DH", 
                "ACTUAL_LT_APPROVED_FINANCE_DH", "PLAN_LT_APPROVED_FINANCE_DH", 
                "SKIP_FLAG_APPROVED_FINANCE_DH"}, 
            new string[] { "ACTUAL_DT_APPROVED_FINANCE_DIRECTOR", "PLAN_DT_APPROVED_FINANCE_DIRECTOR", 
                "ACTUAL_LT_APPROVED_FINANCE_DIRECTOR", "PLAN_LT_APPROVED_FINANCE_DIRECTOR", 
                "SKIP_FLAG_APPROVED_FINANCE_DIRECTOR"}, 
            new string[] { "ACTUAL_DT_POSTED_TO_SAP", "PLAN_DT_POSTED_TO_SAP", 
                "ACTUAL_LT_POSTED_TO_SAP", "PLAN_LT_POSTED_TO_SAP", 
                "SKIP_FLAG_POSTED_TO_SAP"}, 
            new string[] { "ACTUAL_DT_PAID", "PLAN_DT_PAID", 
                "ACTUAL_LT_PAID", "PLAN_LT_PAID", 
                "SKIP_FLAG_PAID"}, 
            new string[] { "ACTUAL_DT_CHECKED", "PLAN_DT_CHECKED", 
                "ACTUAL_LT_CHECKED", "PLAN_LT_CHECKED", 
                "SKIP_FLAG_CHECKED"}
        };

        private string[][] aUser = new string[][] 
        {
            new string[] { "ACTUAL_DT_SH", "PLAN_DT_SH", 
                "ACTUAL_LT_SH", "PLAN_LT_SH", "SKIP_FLAG_SH"},
            new string[] { "ACTUAL_DT_DPH", "PLAN_DT_DPH", 
                "ACTUAL_LT_DPH", "PLAN_LT_DPH", "SKIP_FLAG_DPH"},
            new string[] { "ACTUAL_DT_DH", "PLAN_DT_DH", 
                "ACTUAL_LT_DH", "PLAN_LT_DH", "SKIP_FLAG_DH"},            
            new string[] { "ACTUAL_DT_DIRECTOR", "PLAN_DT_DIRECTOR",
                "ACTUAL_LT_DIRECTOR", "PLAN_LT_DIRECTOR", "SKIP_FLAG_DIRECTOR"},
            new string[] { "ACTUAL_DT_VP", "PLAN_DT_VP", 
                "ACTUAL_LT_VP", "PLAN_LT_VP", "SKIP_FLAG_VP"}
        };


        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        private Common.Enum.SAPMode SAPMode
        {
            set
            {
                ViewState["SAPMode"] = value;
            }
            get
            {
                return (Common.Enum.SAPMode)ViewState["SAPMode"];
            }
        }

        private bool AllSelected
        {
            get
            {
                object b = Session["gridSelectAll"];
                if (b == null)
                {
                    string x = "0";
                    Session["gridSelectAll"] = x;
                    return false;
                }
                else
                {
                    string x = b as string;
                    return (x != null) ? x.Equals("1") : false;
                }
            }

            set
            {
                object b = Session["gridSelectAll"];
                string x = (value) ? "1" : "0";
                Session["gridSelectAll"] = x;
            }
        }

        public readonly string PAGE_SIZE = "pv_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }

        private List<WorklistHelper> Worklist
        {
            get
            {
                return logic.VoucherList.GetWorklist(UserName);
            }
        }

        private int? roleClass = null;
        protected int RoleClass
        {
            get
            {
                if (roleClass == null)
                {
                    roleClass = logic.Look.GetRoleClassOf(UserData);
                }
                return (int)roleClass;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Ticker.In("PVList Page_Load");
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;

            ASPxListBox statusList = (ASPxListBox)DropDownListLastStatus.FindControl("LastStatusListBox");
            statusList.DataSource = logic.PVList.getDocumentStatus();
            statusList.DataBind();

            sel = new SelectableDropDownHelper(IssuingDivisionDropDown, "IssuingDivisionListBox");
            ASPxListBox divBox = (ASPxListBox)IssuingDivisionDropDown.FindControl("IssuingDivisionListBox");

            lit = litMessage;

            if (!IsPostBack && !IsCallback)
            {
                if (divBox != null) sel.SelectAll();

                PrepareLogout();

                HiddenCanSelectAll.Value = "1"; // hasSelectAll ? "1": "0";
                Ticker.In("GenerateComboData");
                GenerateComboData(DropDownListPVType, "PV_LIST_TYPE", true);
                GenerateComboData(DropDownListWorkflowStatus, "WORKFLOW_STATUS", true);

                GenerateComboData(DropDownListNotice, "NOTIFICATION", true);
                GenerateComboData(DropDownListHoldFlag, "HOLD_FLAG", true);
                GenerateComboData(DropDownListSettlementStatus, "SETTLEMENT_STATUS_LIST", true);
                GenerateComboData(ddlPaymentMethod, "PAY_METHOD", true);
                GenerateComboData(ddlVendorGroup, "VENDOR_GROUP_ALL", true);
                Ticker.Out();

                gridGeneralInfo.DataSource = null;
                //grid_CustomInitialization(gridGeneralInfo);
                gridStatusUser.DataSource = null;
                gridStatusFinance.DataSource = null;
                SetFirstLoad();
                HiddenSearchedTabIdx0.Value = "0";
                HiddenSearchedTabIdx1.Value = "0";
                HiddenSearchedTabIdx2.Value = "0";

                #region Init Startup Script
                AddEnterComponent(dtPVDateFrom);
                AddEnterComponent(dtPVDateTo);
                AddEnterComponent(ddlTransactionType);
                AddEnterComponent(TextBoxPVNoFrom);
                AddEnterComponent(TextBoxPVNoTo);
                AddEnterComponent(ddlPaymentMethod);
                AddEnterComponent(DropDownListPVType);
                AddEnterComponent(ddlVendorGroup);
                AddEnterComponent(TextBoxVendorCode);
                AddEnterComponent(TextBoxVendorDesc);
                AddEnterComponent(dtActivityDateFrom);
                AddEnterComponent(dtActivityDateTo);
                AddEnterComponent(dtSubmitDateFrom);
                AddEnterComponent(dtSubmitDateTo);
                AddEnterComponent(PlanningPaymentDateFrom);
                AddEnterComponent(PlanningPaymentDateTo);
                AddEnterComponent(dtPaidDateFrom);
                AddEnterComponent(dtPaidDateTo);
                AddEnterComponent(dtPostingDateFrom);
                AddEnterComponent(dtPostingDateTo);

                if (logic.User.IsAuthorizedFinance || logic.User.IsDirectorUp || UserData.Roles.Contains("ELVIS_ADMIN"))
                {
                    AddEnterComponent(IssuingDivisionDropDown);
                }
                else
                {
                    IssuingDivisionDropDown.Enabled = false;
                }
                AddEnterComponent(DropDownListWorkflowStatus);
                AddEnterComponent(DropDownListLastStatus);
                AddEnterComponent(DropDownListNotice);
                AddEnterComponent(DropDownListHoldFlag);
                AddEnterComponent(DropDownListSettlementStatus);
                AddEnterComponent(CreatedByEdit);
                AddEnterComponent(NextApproverEdit);
                AddEnterAsSearch(ButtonSearch, _ScreenID);
                #endregion

                #region Init Parameter
                String varPaidDate = Request.QueryString["PaidDate"];
                DateTime _paidDate = DateTime.Now;
                bool reload = false;
                if (!String.IsNullOrEmpty(varPaidDate)
                    && DateTime.TryParse(varPaidDate, out _paidDate))
                {
                    dtPVDateFrom.Value = new DateTime(DateTime.Now.Year, 1, 1);
                    dtPVDateTo.Value = new DateTime(DateTime.Now.Year, 12, 31);
                    dtPaidDateFrom.Value = _paidDate;
                    dtPaidDateTo.Value = _paidDate;

                    #region Search data
                    if (validateInputSearch())
                    {
                        reload = true;

                    }
                    #endregion
                }
                String vDate = Request.QueryString["date"];
                DateTime d = System.DateTime.Now;
                if (!String.IsNullOrEmpty(vDate) && DateTime.TryParse(vDate, out d))
                {
                    dtPVDateFrom.Value = d;
                    dtPVDateTo.Value = d;
                    reload = true;
                }
                String docno = Request.QueryString["doc_no"];
                decimal DOC_NO;
                if (!String.IsNullOrEmpty(docno) && decimal.TryParse(docno, out DOC_NO))
                {
                    TextBoxPVNoFrom.Text = docno;
                    TextBoxPVNoTo.Text = docno;
                    reload = true;
                }
                if ((dtPVDateFrom.Value == null) || (dtPVDateTo.Value == null))
                {
                    int days = logic.Sys.GetText("PvList", "INITIAL_LIST_DAYS").Int(7) * -1;
                    dtPVDateFrom.Value = d.AddDays(days);
                    dtPVDateTo.Value = d;
                }

                if (reload || Common.AppSetting.LIST_ON_LOAD)
                    LoadData();
                #endregion
            }
            Ticker.Out("PVList Page_Load");
        }

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }

        private void SetFirstLoad()
        {
            ButtonSearch.Visible = RoLo.isAllowedAccess("ButtonSearch");
            ButtonClear.Visible = true;
            ButtonNewPV.Visible = RoLo.isAllowedAccess("ButtonNewPV");
            enableDataOperationButtons(false);
            ButtonGenerateEntertain.Visible = RoLo.isAllowedAccess("ButtonGenerateEntertain");
            gapGenerateEntertain.Visible = ButtonGenerateEntertain.Visible;
            ButtonPrintWorklist.Visible = RoLo.isAllowedAccess("ButtonPrintWorklist");
            ButtonApprove.Visible = RoLo.isAllowedAccess("ButtonApprove");
            ButtonReject.Visible = RoLo.isAllowedAccess("ButtonReject");
        }

        private void enableDataOperationButtons(bool enable)
        {
            ButtonDirectToMe.Visible = enable;
            gapDirectToMe.Visible = enable;

            ButtonEdit.Visible = enable;
            ButtonDelete.Visible = enable;
            ButtonDownload.Visible = enable;

            ButtonHistory.Visible = enable;
            gapHistory.Visible = enable;

            ButtonPrintCover.Visible = enable;
            gapPrintCover.Visible = enable;

            ButtonPrintTicket.Visible = enable;
            gapPrintTicket.Visible = enable;

            ButtonPostSAP.Visible = enable;
            gapPostSAP.Visible = enable;

            ButtonCheckApp.Visible = enable;
            gapCheckApp.Visible = enable;
        }



        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                string _no = e.GetValue("PV_NO").str();
                string _trans = e.GetValue("TRANSACTION_CD").str();
                string _yy = e.GetValue("PV_YEAR").str();

                object _notices = e.GetValue("NOTICES");
                if (_notices != null)
                {
                    Literal litGridNotices = gridGeneralInfo.FindRowCellTemplateControl(
                        e.VisibleIndex, gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn, "litGridNotices") as Literal;
                    int notices = 0;
                    Int32.TryParse(_notices.ToString(), out notices);
                    if (notices > 0)
                    {
                        litGridNotices.Text = "(" + _notices + ")";
                        litGridNotices.Visible = true;
                    }
                    else
                    {
                        litGridNotices.Text = "";
                        litGridNotices.Visible = false;
                    }
                }

                object _HoldFlag = e.GetValue("HOLD_FLAG");
                Literal litGridHoldFlag = gridGeneralInfo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn, "litGridHoldFlag") as Literal;
                if (_HoldFlag != null)
                {
                    int holdFlag = 0;
                    if (!Int32.TryParse(_HoldFlag.ToString(), out holdFlag)) holdFlag = 0;
                    litGridHoldFlag.Visible = (holdFlag > 0);
                }
                else
                {
                    litGridHoldFlag.Visible = false;
                }

                bool showOthers = e.GetValue("OTHER_CURRENCY").str().Int(0) > 0;

               
                LinkButton aCurr = gridGeneralInfo.FindRowCellTemplateControl(
                    e.VisibleIndex,
                    gridGeneralInfo.Columns["TOTAL_AMOUNT_IDR"] as GridViewDataColumn,
                    "linkSeeAllCurrency") as LinkButton;

                Literal litIDR = gridGeneralInfo.FindRowCellTemplateControl(
                    e.VisibleIndex,
                    gridGeneralInfo.Columns["TOTAL_AMOUNT_IDR"] as GridViewDataColumn,
                    "litGridTotalAmountIDR") as Literal;

                HyperLink hypDetail = gridGeneralInfo.FindRowCellTemplateControl(
                        e.VisibleIndex, gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn
                        , "hypDetail") as HyperLink;
                
                if (hypDetail != null)
                {
                    hypDetail.NavigateUrl = Page.GetUrl(RoleClass, _no, _yy, new DateTime(), _trans);
                    hypDetail.Text = _no;
                }

                 if (aCurr.Text == "IDR")
                {
                     aCurr.Visible = showOthers;
                     litIDR.Visible = !showOthers;
                }
                 else
                 {
                     aCurr.Visible = !showOthers;
                     litIDR.Visible = showOthers;
                 }
               
			   #region EFB
                LinkButton btnDownloadPDF = gridGeneralInfo.FindRowCellTemplateControl(
                        e.VisibleIndex, gridGeneralInfo.Columns["PDF_WITHHOLDING"] as GridViewDataColumn
                        , "btnDownloadPDF") as LinkButton;

                if (e.GetValue("POSTING_DATE") != null)
                {
                    btnDownloadPDF.Visible = true;
                }
                else
                {
                    btnDownloadPDF.Visible = false;
                }
                #endregion
            }
        }


        protected void gridStatusUser_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ColorifyCell(aUser, e);
        }

        protected void gridStatusFinance_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ColorifyCell(aFin, e);
        }

        protected List<int> getSelectedStatus()
        {
            ASPxListBox statusList = (ASPxListBox)DropDownListLastStatus.FindControl("LastStatusListBox");
            SelectedItemCollection selectedItems = statusList.SelectedItems;
            List<int> lstSelectedStatus = new List<int>();
            if ((selectedItems != null) && (selectedItems.Count > 0))
            {
                ListEditItem item;
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    item = selectedItems[i];
                    lstSelectedStatus.Add(Convert.ToInt32(item.Value));
                }
            }

            return lstSelectedStatus;
        }
        protected void clearSelectedStatus()
        {
            ASPxListBox statusList = (ASPxListBox)DropDownListLastStatus.FindControl("LastStatusListBox");
            statusList.UnselectAll();
            DropDownListLastStatus.Text = String.Empty;
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.PVList.GetUserDivisions(UserData);
        }

        protected void dxVendor_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            e.QueryableSource = logic.PVList.GetVendorCodes();
            e.KeyExpression = "VENDOR_CD";
        }

        protected void SetMagic(ASPxGridView g, int pageSize)
        {
            if (g.SettingsPager.PageSize > 128)
            {
                ServerModeKeyedCache.DefaultMagicNumberFetchRowsTop = g.SettingsPager.PageSize;
                ServerModeKeyedCache.DefaultMagicNumberFetchKeysAllThreshold = g.SettingsPager.PageSize;
            }
            else
            {
                ServerModeKeyedCache.DefaultMagicNumberFetchRowsTop = pageSize;
                ServerModeKeyedCache.DefaultMagicNumberFetchKeysAllThreshold = pageSize;
            }
        }

        protected void LinqPVList_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            SetMagic(gridGeneralInfo, PageSize);
            e.QueryableSource = logic.PVList.SearchListIqueryable(SearchCriteria());

            e.KeyExpression = "ROW_NUMBER";
        }

        protected void LinqPVListStatusUser_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            SetMagic(gridStatusUser, PageSize);
            e.QueryableSource = logic.PVList.SearchListStatusUserIqueryable(SearchCriteria());


            e.KeyExpression = "ROW_NUMBER";
        }

        protected void LinqPVListStatusFinance_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            SetMagic(gridStatusFinance, PageSize);
            e.QueryableSource = logic.PVList.SearchListStatusFinanceIqueryable(SearchCriteria());

            e.KeyExpression = "ROW_NUMBER";
        }

        protected void dsDirectToMe_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.PVList.GetDirectToMeReasons(UserName);
        }

        protected void pvListTabs_ActiveTabChanged(object sender, EventArgs e)
        {
            if (validateInputSearch())
            {
                checkForActiveTabChanged();
            }

        }

        private void checkForActiveTabChanged()
        {
            Ticker.In("checkForActiveTabChanged");
            if (PVListTabs.ActiveTabIndex == 0)
            {
                gridGeneralInfo.DataSourceID = LinqPVList.ID;
                gridGeneralInfo.DataBind();
                HiddenSearchedTabIdx0.Value = "1";

            }
            else if (PVListTabs.ActiveTabIndex == 1)
            {
                gridStatusUser.DataSourceID = LinqPVListStatusUser.ID;
                gridStatusUser.DataBind();
                WidthSet(gridStatusUser, aUser, ListColumnWidths);
                HiddenSearchedTabIdx1.Value = "1";

            }
            else if (PVListTabs.ActiveTabIndex == 2)
            {
                gridStatusFinance.DataSourceID = LinqPVListStatusFinance.ID;
                gridStatusFinance.DataBind();
                WidthSet(gridStatusFinance, aFin, ListColumnWidths);
                HiddenSearchedTabIdx2.Value = "1";
            }
            Ticker.Out("checkForActiveTabChanged");
        }

        protected void evt_ddlTransactionType_onLoad(object sender, EventArgs arg)
        {
            ddlTransactionType.DataSource = logic.Persist.getTransactionTypes(UserData, -1, -1, false, true, logic.User.IsAuthorizedFinance, UserCanUploadDetail);
            ddlTransactionType.DataBind();
        }

        private bool validateDateFromTo(string what, ASPxDateEdit a, ASPxDateEdit b)
        {
            if ((!string.IsNullOrEmpty(a.Text) && string.IsNullOrEmpty(b.Text)) ||
              (string.IsNullOrEmpty(a.Text) && !string.IsNullOrEmpty(b.Text)))
            {
                if (string.IsNullOrEmpty(a.Text))
                {
                    Nag("MSTD00017WRN", what + " From");
                    return false;
                }
                if (string.IsNullOrEmpty(b.Text))
                {
                    Nag("MSTD00017WRN", what + " To");
                    return false;
                }
            }
            else
            {
                if (b.Date.CompareTo(a.Date) < 0)
                {
                    Nag("MSTD00029ERR", what + " From", what + " To");
                    return false;
                }
            }
            return true;
        }


        private bool validateInputSearch()
        {
            bool Ok = false;

            Ok = validateDateFromTo("PV Date", dtPVDateFrom, dtPVDateTo);
            if (!Ok) return false;
            Ok = validateDateFromTo("Activity Date", dtActivityDateFrom, dtActivityDateTo);
            if (!Ok) return false;
            Ok = validateDateFromTo("Submit Date", dtSubmitDateFrom, dtSubmitDateTo);
            if (!Ok) return false;
            Ok = validateDateFromTo("Planning Payment Date", PlanningPaymentDateFrom, PlanningPaymentDateTo);
            if (!Ok) return false;
            Ok = validateDateFromTo("Paid Date", dtPaidDateFrom, dtPaidDateTo);
            if (!Ok) return false;
            Ok = validateDateFromTo("Posting Date", dtPostingDateFrom, dtPostingDateTo);
            if (!Ok) return false;

            if (!string.IsNullOrEmpty(TextBoxPVNoTo.Text))
            {
                if (TextBoxPVNoTo.Text.Length > Int32.MaxValue.ToString().Length)
                {
                    litMessage.Text += Nagging("MSTD00032WRN", "PV No To", Int32.MaxValue.ToString().Length);
                    return false;
                }
                if (Convert.ToInt64(TextBoxPVNoTo.Text) > Int32.MaxValue)
                {
                    litMessage.Text += Nagging("MSTD00033WRN", "PV No To", Int32.MaxValue.ToString());
                    return false;
                }

                if (TextBoxPVNoFrom.Text.Length > Int32.MaxValue.ToString().Length)
                {
                    litMessage.Text += Nagging("MSTD00032WRN", "PV No From", Int32.MaxValue.ToString().Length);
                    return false;
                }
                long tmp;
                if (!Int64.TryParse(TextBoxPVNoFrom.Text, out tmp))
                {
                    litMessage.Text += Nagging("MSTD00018WRN", "PV No From");
                    return false;
                }
                if (Convert.ToInt64(TextBoxPVNoFrom.Text) > Int32.MaxValue)
                {
                    litMessage.Text += Nagging("MSTD00033WRN", "PV No From", Int32.MaxValue.ToString());
                    return false;
                }
                if (Convert.ToInt32(TextBoxPVNoFrom.Text) > Convert.ToInt32(TextBoxPVNoTo.Text))
                {
                    Nag("MSTD00030ERR", "PV No From", "PV No To");
                    return false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(TextBoxPVNoFrom.Text))
                {
                    string tmpPvNoFrom = TextBoxPVNoFrom.Text.Trim().Replace("*", "");
                    if (!String.IsNullOrEmpty(tmpPvNoFrom))
                    {
                        long tmp;
                        if (!Int64.TryParse(tmpPvNoFrom, out tmp))
                        {
                            litMessage.Text += Nagging("MSTD00036WRN", "PV No From");
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (PageSize < 1)
                PageSize = 10;
            if (PageSize > 0)
            {
                gridGeneralInfo.SettingsPager.PageSize = PageSize;
                gridStatusUser.SettingsPager.PageSize = PageSize;
                gridStatusFinance.SettingsPager.PageSize = PageSize;
            }
        }

        protected void grid_CustomCallback(object sender,
            DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            int newSize;
            if (!int.TryParse(e.Parameters.ToString(), out newSize)) return;

            if (newSize < 0)
                newSize = 0;
            grid.SettingsPager.PageSize = newSize;
            PageSize = newSize;
            // Session[GridCustomPageSizeName] = newSize;
            if (grid != null && e != null && !String.IsNullOrEmpty(e.Parameters) && grid.VisibleRowCount > 0)
            {
                if (validateInputSearch())
                {
                    grid.DataBind();

                }
            }
        }

        private void setPageSize(int newSize)
        {
            PageSize = newSize;
            if (PageSize > 0)

                if (PVListTabs.ActiveTabIndex == 0)
                    gridGeneralInfo.SettingsPager.PageSize = newSize;
                else if (PVListTabs.ActiveTabIndex == 1)
                    gridStatusUser.SettingsPager.PageSize = newSize;
                else if (PVListTabs.ActiveTabIndex == 2)
                    gridStatusFinance.SettingsPager.PageSize = newSize;

        }

        protected string WriteSelectedIndexInfo(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        protected string WriteSelectedIndexUser(int pageSize)
        {
            return pageSize == gridStatusUser.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        protected string WriteSelectedIndexFinance(int pageSize)
        {
            return pageSize == gridStatusFinance.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected string Status_Color(Object statusValue)
        {
            string color = "";
            short statusInt = 0;
            if (statusValue != null && Int16.TryParse(statusValue.ToString(), out statusInt))
            {
                if (statusInt == 1)
                    color = red;
                else if (statusInt == 2)
                    color = yellow;
                else if (statusInt > 2)
                    color = green;
            }
            return color;
        }

        protected void lkpgridVendorCode_Load(object sender, EventArgs arg)
        {
            // List<VendorCdData> _list = logic.Look.GetVendorCodeData("", "");
            //TextBoxVendorCode.DataSource = _list;
            //TextBoxVendorCode.DataBind();
        }

        private void LoadGridPopUpVendorCd()
        {
            List<VendorCdData> _list = logic.Look.GetVendorCodeData(txtSearchCd.Text, txtSearchDesc.Text);
            gridPopUpVendorCd.DataSource = _list;
            gridPopUpVendorCd.DataBind();
        }

        protected void gridPopUpVendorCd_PageIndexChanged(object sender, EventArgs e)
        {
            LoadGridPopUpVendorCd();
        }

        protected void gridPopUpVendorCd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoVendorCd = gridPopUpVendorCd.FindRowCellTemplateControl(
                    e.VisibleIndex, gridPopUpVendorCd.Columns["NO"] as GridViewDataColumn, "litGridNoVendorCd") as Literal;
                litGridNoVendorCd.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        private void ClearModal()
        {
            txtSearchCd.Text = "";
            txtSearchDesc.Text = "";
        }

        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            // Clear search criteria
            dtPVDateFrom.Text = null;
            dtPVDateTo.Text = null;
            //ddlTransactionType.SelectedIndex = 0;
            ddlTransactionType.Value = null;
            TextBoxPVNoFrom.Text = null;
            TextBoxPVNoTo.Text = null;
            ddlPaymentMethod.SelectedIndex = 0;
            ddlVendorGroup.SelectedIndex = 0;
            DropDownListPVType.SelectedIndex = 0;
            TextBoxVendorCode.Text = null;
            TextBoxVendorDesc.Text = null;
            dtActivityDateFrom.Text = null;
            dtActivityDateTo.Text = null;
            dtSubmitDateFrom.Text = null;
            dtSubmitDateTo.Text = null;
            PlanningPaymentDateFrom.Text = null;
            PlanningPaymentDateTo.Text = null;
            dtPaidDateFrom.Text = null;
            dtPaidDateTo.Text = null;
            dtPostingDateFrom.Text = null;
            dtPostingDateTo.Text = null;

            DropDownListWorkflowStatus.SelectedIndex = 0;
            if (IssuingDivisionDropDown.Enabled)
                sel.Clear();

            clearSelectedStatus();

            DropDownListNotice.SelectedIndex = 0;
            DropDownListHoldFlag.SelectedIndex = 0;
            DropDownListSettlementStatus.SelectedIndex = 0;

            gridGeneralInfo.DataSource = null;
            gridGeneralInfo.DataSourceID = null;
            gridStatusFinance.DataSource = null;
            gridStatusFinance.DataSourceID = null;
            gridStatusUser.DataSource = null;
            gridStatusUser.DataSourceID = null;

            gridGeneralInfo.DataBind();
            gridStatusUser.DataBind();
            gridStatusFinance.DataBind();

            HiddenSearchedTabIdx0.Value = "0";
            HiddenSearchedTabIdx1.Value = "0";
            HiddenSearchedTabIdx2.Value = "0";
            gapDirectToMe.Visible = false;
            ButtonDirectToMe.Visible = false;
            ButtonEdit.Visible = false;
            ButtonDelete.Visible = false;
            ButtonDownload.Visible = false;
            ButtonHistory.Visible = false;
            ButtonPrintCover.Visible = false;
            ButtonPrintTicket.Visible = false;
            ButtonPostSAP.Visible = false;
            ButtonCheckApp.Visible = false;
            ButtonApprove.Visible = false;
            ButtonReject.Visible = false;
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            litEFBMessage.Text = "";
            LoadData();
        }

        private void LoadData()
        {
            Ticker.In("LoadData");
            if (validateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridStatusUser.Selection.UnselectAll();
                gridStatusFinance.Selection.UnselectAll();

                SearchData();

                PageMode = Common.Enum.PageMode.View;
            }
            Ticker.Out();
        }

        private void SearchData()
        {
            Ticker.In("SearchData");
            if (PVListTabs.ActiveTabIndex == 0)
            {
                HiddenSearchedTabIdx0.Value = "1";
                HiddenSearchedTabIdx1.Value = "0";
                HiddenSearchedTabIdx2.Value = "0";
                Ticker.In("ListUser");
                gridGeneralInfo.DataSourceID = LinqPVList.ID;
                gridGeneralInfo.DataBind();
                Ticker.Out();
                SetCancelEditDetail(gridGeneralInfo);
            }
            else if (PVListTabs.ActiveTabIndex == 1)
            {
                HiddenSearchedTabIdx0.Value = "0";
                HiddenSearchedTabIdx1.Value = "1";
                HiddenSearchedTabIdx2.Value = "0";
                Ticker.In("ListUser");
                gridStatusUser.DataSourceID = LinqPVListStatusUser.ID;
                gridStatusUser.DataBind();
                Ticker.Out();
                SetCancelEditDetail(gridStatusUser);
            }
            else if (PVListTabs.ActiveTabIndex == 2)
            {
                HiddenSearchedTabIdx0.Value = "0";
                HiddenSearchedTabIdx1.Value = "0";
                HiddenSearchedTabIdx2.Value = "1";
                Ticker.In("ListFinance");
                gridStatusFinance.DataSourceID = LinqPVListStatusFinance.ID;
                gridStatusFinance.DataBind();
                Ticker.Out();
                SetCancelEditDetail(gridStatusFinance);
            }
            Ticker.Out("SearchData");
        }

        private void SetCancelEditDetail(ASPxGridView gridView)
        {
            if (gridView.VisibleRowCount > 0)
            {
                Ticker.In("SetCancelEditDetail");
                ButtonDirectToMe.Visible = RoLo.isAllowedAccess("ButtonDirectToMe");
                gapDirectToMe.Visible = ButtonDirectToMe.Visible;
                ButtonEdit.Visible = RoLo.isAllowedAccess("ButtonEdit");
                ButtonDelete.Visible = RoLo.isAllowedAccess("ButtonDelete");
                ButtonDownload.Visible = true;
                ButtonHistory.Visible = true;
                gapHistory.Visible = ButtonHistory.Visible;
                ButtonPrintCover.Visible = RoLo.isAllowedAccess("ButtonPrintCover");
                gapPrintCover.Visible = ButtonPrintCover.Visible;
                ButtonPrintTicket.Visible = RoLo.isAllowedAccess("ButtonPrintTicket");
                gapPrintTicket.Visible = ButtonPrintTicket.Visible;
                ButtonPostSAP.Visible = RoLo.isAllowedAccess("ButtonPostSAP");
                gapPostSAP.Visible = ButtonPostSAP.Visible;
                ButtonCheckApp.Visible = RoLo.isAllowedAccess("ButtonCheckApp");
                gapCheckApp.Visible = ButtonCheckApp.Visible;
                ButtonGenerateEntertain.Visible = RoLo.isAllowedAccess("ButtonGenerateEntertain");
                gapGenerateEntertain.Visible = ButtonGenerateEntertain.Visible;
                ButtonApprove.Visible = RoLo.isAllowedAccess("BUttonApprove");
                gapApprove.Visible = ButtonApprove.Visible;
                ButtonReject.Visible = RoLo.isAllowedAccess("BUttonReject");
                gapReject.Visible = ButtonReject.Visible;
                Ticker.Out();
            }
        }

        private void ProcessEdit_Click(ASPxGridView gridView)
        {
            if (gridView.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            if (gridView.Selection.Count > 1)
            {
                Nag("MSTD00016WRN");
                return;
            }
            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (!gridView.Selection.IsRowSelected(i)) continue;
                String pvNo = gridView.GetRowValues(i, "PV_NO").ToString();

                logic.Say("ProcessEdit_Click", "Edit " + pvNo);

                HiddenField hidGridPVYear = gridView.FindRowCellTemplateControl(
                    i, gridView.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
                HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(
                    i, gridView.Columns["PV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;
                string folio = pvNo + Convert.ToString(hidGridPVYear.Value);

                bool hasWorklist = false;
                foreach (WorklistHelper h in Worklist)
                {
                    if (h.Folio.Equals(folio))
                    {
                        hasWorklist = true;
                        break;
                    }
                }

                string userRole = UserData.Roles[0];
                PVFormHeaderData p = logic.PVList.GetPVHeader(Convert.ToInt32(pvNo), Convert.ToInt32(hidGridPVYear.Value));
                bool canEdit = PVFormLogic.CanEdit(
                    true, hasWorklist, p.STATUS_CD, userRole, p.HOLD,
                    p.VENDOR_GROUP_CD, p.PV_TYPE_CD, logic.User.IsAuthorizedCreatorRole, p.BUDGET_NO);

                if (canEdit)
                {
                    //fid.Hadid
                    //if Transaction type is Accrued Year End, redirect to PV Accrued
                    string screenCd = "ELVIS_PVFormList";
                    string screenName = "PVFormList";

                    if (p.TRANSACTION_CD == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
                    {
                        screenCd = "ELVIS_AccrPVForm";
                        screenName = "../80Accrued/AccrPVForm";
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                    screenCd,
                    "openWin('" + screenName + ".aspx?mode=edit&pv_no=" + pvNo + "&pv_year=" + hidGridPVYear.Value.ToString() + "','" + screenCd + "');",
                    true);
                    //end fid.Hadid
                    
                }
                else
                {
                    Nag("MSTD00003WRN", "Not allowed to edit PV " + pvNo);
                }
            }


        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (PVListTabs.ActiveTabIndex == 0)
                ProcessEdit_Click(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                ProcessEdit_Click(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                ProcessEdit_Click(gridStatusFinance);
        }

        protected void ButtonClose_Click(object sender, EventArgs e)
        {

        }

        private void ProcessDelete_Click(ASPxGridView gridView)
        {
            logic.Say("btnProcessDelete_Click", "Delete");
            if (gridView.Selection.Count >= 1)
            {
                Boolean checkValid = true;

                if (checkValid)
                {
                    lblConfirmationDelete.Text = _m.Message("MSTD00008CON", "delete");
                    ConfirmationDeletePopUp.Show();
                }
            }
            else if (gridView.Selection.Count == 0)
            {
                #region Error

                Nag("MSTD00009WRN");
                #endregion
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (PVListTabs.ActiveTabIndex == 0)
                ProcessDelete_Click(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                ProcessDelete_Click(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                ProcessDelete_Click(gridStatusFinance);
        }

        private bool inWorklist(string folio)
        {
            Ticker.In("inWorklist");
            if (Worklist == null) return false;
            bool has = false;
            foreach (var x in Worklist)
            {
                if (x.Folio.Equals(folio)) { has = true; break; }
            }
            Ticker.Spin("inWorkList: " + has);

            Ticker.Out();
            return has;
        }

        private void ProcessConfirmationDelete_Click(ASPxGridView gridView)
        {
            bool result = false;
            if (gridView.Selection.Count < 1) return;


            List<PVFormData> listPVNo = new List<PVFormData>();
            ///added by Akhmad Nuryanto, 25/09/2012, if status is not 0 or 31 then raise error message
            List<String> listInvalidPVno = new List<String>();
            ///
            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (!gridView.Selection.IsRowSelected(i)) continue;

                PVFormData pvFormData = new PVFormData(1);
                HiddenField hidGridPVYear = gridView.FindRowCellTemplateControl(i,
                    gridView.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
                pvFormData.PVNumber = Convert.ToInt32(gridView.GetRowValues(i, "PV_NO"));
                int canceled = Convert.ToInt32(gridView.GetRowValues(i, "CANCEL_FLAG"));
                pvFormData.PVYear = Convert.ToInt32(hidGridPVYear.Value);
                string folio = pvFormData.PVNumber.str() + pvFormData.PVYear.str();
                HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(
                    i, gridView.Columns["PV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;
                string Status = hidGridStatusCd.Value.str();

                int? StatusCode = Status.isEmpty() ? null : (int?)Status.Int();

            
                if (StatusCode != null && (StatusCode == 0 || StatusCode == 31)
                        || (canceled == 1)
                        || inWorklist(folio)
                   )
                {
                    listPVNo.Add(pvFormData);
                }
                else
                {
                    listInvalidPVno.Add(pvFormData.PVNumber.ToString());
                }
                //end
            }

            //aded by Akhmad Nuryanto
            if (listInvalidPVno.Count > 0)
            {
                Nag("MSTD00101ERR", Common.Function.CommonFunction.CommaJoin(listInvalidPVno));
            }
            else
                //end
                if (listPVNo.Count > 0)
            {
                PageMode = Common.Enum.PageMode.View;
                result = logic.PVList.Delete(listPVNo, UserName);
                if (!result)
                {
                    Nag("MSTD00014ERR");
                    gridView.Selection.UnselectAll();
                }
                else
                {
                    //update data EFB
                    string varListPV = "";
                    foreach (var item in listPVNo)
                    {
                        varListPV += item.PVNumber + ";";
                    }
                    logic.Say("start", "Start EFB RollbackDataPVEFB, PV : " + varListPV);
                    logic.PVList.RollbackDataPVEFB(varListPV, "delete");
                    logic.Say("end", "End EFB RollbackDataPVEFB, PV : " + varListPV);

                    Nag("MSTD00015INF");
                    gridView.Selection.UnselectAll();
                    LoadData();
                }
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            logic.Say("btnOkConfirmationDelete_Click", "Confirm Delete");
            ConfirmationDeletePopUp.Hide();
            if (PVListTabs.ActiveTabIndex == 0)
                ProcessConfirmationDelete_Click(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                ProcessConfirmationDelete_Click(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                ProcessConfirmationDelete_Click(gridStatusFinance);
        }

        protected void BtnCancelConfirmationDelete_Click(object sender, EventArgs e)
        {
            ConfirmationDeletePopUp.Hide();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            // gather all PV to be Directed 
            logic.Say("btnGo_Click", "Direct To Me");
            List<PVListData> listData = null;
            if (PVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);

            String cashier = logic.PVList.getCashierName();
            string _Comment = "Direct to Me Reason:" + ddlReason.Text;

            if (listData.Count > 0)
            {

                StringBuilder x = new StringBuilder("");
                int seli = 0;
                foreach (PVListData sel in listData)
                {
                    if (seli > 0) x.Append(",");
                    seli++;
                    x.Append(sel.PV_NO);
                }
                string documents = x.ToString();
                logic.Say("btnGo_Click",
                        "Direct To Me:{0} list: {1}",
                        UserName, documents);

                x.Clear();

                try
                {
                    string uName = UserName;
                    decimal divCd = ((UserData != null) ? UserData.DIV_CD : 0);
                    int RedirectCount = 0;
                    // send reason to all recipient 
                    foreach (var data in listData)
                    {
                        if (UserData == null)
                        {
                            string timeoutErr = "Login Info empty, session timeout or erased";
                            x.AppendLine(timeoutErr);
                            LoggingLogic.say("", timeoutErr);
                            break;
                        }

                        if (logic.PVList.isCanDirect(data.PV_NO, data.PV_YEAR, uName, divCd) && data.STATUS_CD != "0")
                        {
                            
                            List<string> lstUser = null;

                            int? statusCd = logic.PVApproval.SearchStatusFromDistribution(data.PV_NO, data.PV_YEAR, uName);
                            if (statusCd != null)
                            {
                                lstUser = logic.Notice.listWorkflowRecipient(data.PV_NO, data.PV_YEAR, statusCd ?? 0);
                            }
                            if (lstUser == null)
                            {
                                lstUser = new List<string>();
                            }

                            #region Do --> Approve-Reject
                            bool Ok = logic.PVRVWorkflow.Redirect(data.PV_NO + data.PV_YEAR, UserData, data.NEXT_APPROVER);
                            string nagCode = Ok ? "MSTD00060INF" : "MSTD00062ERR";

                            if (Ok)
                            {
                                //int seq = 0;
                                ErrorData err = new ErrorData(0, "", "");
                                string userRole;
                                List<RoleData> uRole = RoLo.getUserRole(uName, ref err);
                                userRole = (uRole != null && uRole.Count > 0) ? uRole[0].ROLE_NAME : "";

                                logic.Say("btnGo_Click",
                                        "{0} Direct To Me: {1} [{2}] To:{3} [{4}] {5}",
                                        data.PV_NO, uName, userRole,
                                        Common.Function.CommonFunction.CommaJoin(lstUser),
                                        "", _Comment);

                                log.Log("INF", "Redirect " + data.PV_NO + data.PV_YEAR + " [" + data.NEXT_APPROVER + "] to [" + uName + "]", "PVList.Redirect", uName);

                                logic.PVList.SetNextApprover(data.PV_NO.Int(0), data.PV_YEAR.Int(0), uName);

                                logic.Notice.InsertNotice(data.PV_NO, data.PV_YEAR, uName,
                                    userRole, "", "", lstUser, _Comment, false,
                                    true // direct to me always has worklist 
                                    );

                                RedirectCount++;

                                // send email 

                            }
                            x.AppendLine(Nagging(nagCode, "Redirect PV " + data.PV_NO));

                            #endregion
                        }
                        else if (logic.PVList.IsDirectorOf(data.DIVISION_ID, UserData))
                        {
                            string by = "";
                            if (logic.PVList.Take(data.PV_NO, data.PV_YEAR, data.DIVISION_ID, UserData, ref by))
                            {
                                bool Redirected = logic.PVRVWorkflow.Redirect(data.PV_NO + data.PV_YEAR, UserData, by);
                                string nagCode = Redirected ? "MSTD00060INF" : "MSTD00062ERR";

                                if (Redirected)
                                {
                                    //int seq = 0;
                                    ErrorData err = new ErrorData(0, "", "");
                                    string userRole;
                                    List<RoleData> uRole = RoLo.getUserRole(uName, ref err);
                                    userRole = (uRole != null && uRole.Count > 0) ? uRole[0].ROLE_NAME : "";
                                    List<string> lu = logic.Notice.listWorkflowRecipient(data.PV_NO, data.PV_YEAR, 16);
                                    logic.Say("btnGo_Click",
                                            "{0} Director Direct To Me: {1} [{2}] To:{3} [{4}] {5}",
                                            data.PV_NO, uName, userRole,
                                            Common.Function.CommonFunction.CommaJoin(lu),
                                            "", _Comment);
                                    log.Log("INF", "Director Redirect " + data.PV_NO + data.PV_YEAR + " [" + by + "] to [" + uName + "]", "PVList.Redirect", uName);
                                    logic.PVList.SetNextApprover(data.PV_NO.Int(0), data.PV_YEAR.Int(0), uName);
                                    logic.Notice.InsertNotice(data.PV_NO, data.PV_YEAR, uName,
                                        userRole, "", "", lu, _Comment, false,
                                        true // direct to me always has worklist 
                                        );
                                    RedirectCount++;

                                }
                                x.AppendLine(Nagging(nagCode, "Redirect PV " + data.PV_NO));
                            }
                            else
                            {
                                x.AppendLine(Nagging("MSTD00002ERR", "Cannot distribute to PV " + data.PV_NO));
                            }
                        }
                        else
                        {
                            x.AppendLine(Nagging("MSTD00050ERR", "PV", data.PV_NO, "Redirected"));
                        }
                    }
                    if (RedirectCount > 0) ReloadOpener();

                }
                catch (Exception Ex)
                {
                    string _ErrMsg = Ex.Message; ;
                    LoggingLogic.err(Ex);
                    x.AppendLine(_m.SetMessage(_ErrMsg, "ERR"));
                }
                if (x.Length > 0)
                    litMessage.Text = x.ToString();
            }

        }

        protected void ButtonPostSAP_Click(object sender, EventArgs e)
        {
            List<PVListData> listData = null;
            if (PVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);
            ErrorData _Err = new ErrorData();
            String cashier = logic.PVList.getCashierName();

            if (listData.Count > 0)
            {
                logic.Say("ButtonPostSAP_Click", "Post To SAP");
                /// Added by Akhmad Nuryanto, 6/6/2012, Post to SAP
                /// 
                SAPMode = Common.Enum.SAPMode.Post;
                lblConfirmationPost.Text = Nagging("MSTD00008CON", "post data to SAP");
                ModalPopupConfirmationPost.Show();
                /// End addition by Akhmad Nuryanto
            }
            else
            {
                Nag("MSTD00009WRN");
            }

            if (_Err.ErrID == 2)
            {
                Nag("MSTD00002ERR", _Err.ErrMsg);
            }
        }

        /// Added by Akhmad Nuryanto, 6/6/2012, Post to SAP
        /// 
        protected void btnOkConfirmationPost_Click(object sender, EventArgs e)
        {
            ModalPopupConfirmationPost.Hide();

            List<String> listData = null;
            if (PVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRow(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRow(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRow(gridStatusFinance);
            logic.Say("btnOkConfirmationPost_Click", "Confirm Post");
            if (SAPMode == Common.Enum.SAPMode.Post)
            {
                DoPost(listData);
            }
            else if (SAPMode == Common.Enum.SAPMode.Check)
            {
                DoCheck(listData);
            }
        }

        protected bool DoPost(List<string> listData)
        {
            bool result = false;
            DoStartLog(resx.FunctionId("FCN_PV_POST_TO_SAP"), "");
            string _ErrMessage = String.Empty;

            logic.PVList.CheckOneTimeVendor = true;
            result = logic.PVList.PostToSap(listData, ProcessID, ref _ErrMessage, UserData);
            List<string> ok = logic.PVList["OK"] as List<string>;

            if (result || (ok != null && (ok.Count > 0)))
            {
                // do approve 
                List<string> _err = new List<string>();

                string _Comment = "";
                string _Command = resx.K2ProcID("Form_Registration_Approval");
                string toApprove;
                int sucessCount = 0;
                foreach (string pvNoYear in ok)
                {
                    toApprove = NoColon(pvNoYear);

                    logic.Say("DoPost", _Command + " " + toApprove);

                    if (logic.PVRVWorkflow.Go(toApprove, _Command, UserData, _Comment))
                    {
                        ++sucessCount;
                        
                        string[] ny = pvNoYear.Split(':');
                        if (ny.Length >= 2)
                        {
                            int no, yy;
                            int.TryParse(ny[0], out no);
                            int.TryParse(ny[1], out yy);

                            decimal amt = logic.PVApproval.GetPVTotalAmount(no, yy);
                            if (Math.Round(amt) > 0)
                                sendMailPVTicket(ny[0], ny[1]);
                            else
                                logic.PVPrint.ticketPrinted(no, yy, UserName);
                        }
                    }
                    else
                    {
                        _err.Add(toApprove);
                    }
                }
                string failedApproval = "";
                foreach (string s in _err) { failedApproval += s + " "; }
                failedApproval = failedApproval.Trim().Replace(' ', ',');
                if ((sucessCount < 1) && (_err.Count > 0))
                {
                    logic.Say("Do Post", "Approve failed " + failedApproval);
                    Log("MSTD00057ERR", "ERR", "btnOkConfirmationPost_Click",
                        _m.Message("MSTD00067INF", "PV Approval " + failedApproval));
                }

                litMessage.Text = _m.SetMessage(_ErrMessage, "MSTD00067INF");
                Log("MSTD00067INF", "INF", "btnOkConfirmationPost_Click", _m.Message("MSTD00067INF", "PV"));
            }
            else if (logic.PVList["NR"] != null || logic.PVList["RF"] != null)
            {
                string NRList = logic.PVList.CommaJoinList("NR");
                string RFList = logic.PVList.CommaJoinList("RF");
                string msg = "";
                if (!NRList.isEmpty())
                    msg += NRList + " not ready to be posted";
                if (!RFList.isEmpty())
                    msg += RFList + " redirect failed";
                Nag("MSTD00002ERR", msg);
                Log("MSTD00002ERR", "ERR", "btnOkConfirmationPost_Click", msg);
            }
            else
            {
                Nag("MSTD00068ERR", "PV");
                // balikkan ke orang asalnya 

                Log("MSTD00068ERR", "ERR", "btnOkConfirmationPost_Click", _m.Message("MSTD00068ERR", "PV"));
            }
            List<string> ng = logic.PVList["NG"] as List<string>;
            List<string> nr = logic.PVList["notready"] as List<string>;

            logic.PVList.RevertApproverInList(ng);
            logic.PVList.RevertApproverInList(nr);

            return result;
        }

        protected bool DoCheck(List<string> listData)
        {
            bool result = false;
            DoStartLog(resx.FunctionId("FCN_PV_CHECK_SAP"), "");
            string _ErrMessage = String.Empty;

            result = logic.PVList.PVCheck(listData, ProcessID, ref _ErrMessage);

            if (result)
            {
                Master.ApprovalType = Common.Enum.ApprovalEnum.Approve;
                List<string> ok = logic.PVList["OK"] as List<string>;

                /// performApproval();
                Approve(ok);
                LoadData();
                Nag("MSTD00069INF", "PV");
                Log("MSTD00069INF", "INF", "btnOkConfirmationCheck_Click", _m.Message("MSTD00069INF", "PV"));
            }
            else
            {
                Nag("MSTD00070ERR", "PV");
                Log("MSTD00070ERR", "ERR", "btnOkConfirmationCheck_Click", _m.Message("MSTD00070ERR", "PV"));
            }
            return result;
        }

        protected void ButtonCheckAcc_Click(object sender, EventArgs e)
        {
            List<PVListData> listData = null;
            if (PVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);
            ErrorData _Err = new ErrorData();
            String cashier = logic.PVList.getCashierName();

            if (listData.Count > 0)
            {
                logic.Say("ButtonCheckAcc_Click", "Check By Accounting");
                ///added by Akhmad Nuryanto, 22/6/2012
                SAPMode = Common.Enum.SAPMode.Check;
                lblConfirmationPost.Text = _m.Message("MSTD00008CON", "check by Accounting");
                ModalPopupConfirmationPost.Show();
                /// End addition by Akhmad Nuryanto                
            }
            else
            {
                Nag("MSTD00009WRN");
            }

            if (_Err.ErrID == 2)
            {
                Nag("MSTD00002ERR", _Err.ErrMsg);
            }
        }

        protected void ButtonDirectToMe_Click(object sender, EventArgs e)
        {
            int rowCount = 0;
            switch (PVListTabs.ActiveTabIndex)
            {
                case 0:
                    rowCount = gridGeneralInfo.Selection.Count;
                    break;
                case 1:
                    rowCount = gridStatusUser.Selection.Count;
                    break;
                case 2:
                    rowCount = gridStatusFinance.Selection.Count;
                    break;
            }
            if (rowCount < 1)
            {
                Nag("MSTD00009WRN"); // no record selected
            }
            else
            {
                logic.Say("ButtonDirectToMe_Click", "Direct To Me");
                SetPopupDirectToMe();
            }
        }

        protected void GetCashierReport()
        {
            string me = "GetCashierReport";
            int pid = 0;
            string userName = UserName;

            pid = log.Log("MSTD00001INF", me + " START",
                me, userName, me, 0);

            logic.Say("GetCashierReport", me + " pid = " + pid.ToString());
            ProcessID = pid;

            string templateName = Server.MapPath(logic.Sys.GetText("TEMPLATE.FILE", "CASHIER_REPORT"));
            if (!File.Exists(templateName))
            {
                Nag("MSTD00021WRN", "Output Template file");
                log.Log("MSTD00021WRN", "Output Template " + templateName + "not found");
                return;
            }
            byte[] CashierReportBytes = null;
            try
            {
                CashierReportBytes = logic.PVList.GetCashierReport(
                    logic.PVList.SearchListIqueryable(SearchCriteria()),
                    templateName,
                    Server.MapPath(AppSetting.TempFileUpload),
                    ProcessID,
                    UserData);
                
                if (CashierReportBytes != null)
                    Xmit.Send(CashierReportBytes, HttpContext.Current, "PV_LIST_" + DateTime.Now.ToString("MMdd_hhmm") + ".xls");
                
            }
            catch (IOException iox)
            {
                Nag("MSTD00002ERR", iox.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "IOException " + me + " " + iox.Message + "\r\n" + iox.StackTrace);
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "Exception " + me + " " + ex.Message + "\r\n" + ex.StackTrace);
            }
            // export file
            
            if (CashierReportBytes == null) 
            {
                Nag("MSTD00018INF");
                return;
            }
            
            log.Log("MSTD00001INF", me + " FINISH", me);
        }

        protected void ButtonGenerateEntertain_Click(object sender, EventArgs e)
        {
            // read selected records pv no 
            List<PVFormHeaderData> listData = null;
            //Boolean valid = false;
            int pid = 0;
            string userName = UserName;
            if (!ValidateGenerateEntertain()) return;
            pid = log.Log("MSTD00001INF", "Generate Entertain START",
                "GenerateEntertain_Click", userName, "GenerateEntertain", 0);

            listData = logic.PVList.GetEntertainmentList(dtPostingDateFrom.Date, dtPostingDateTo.Date);
            if (listData == null || listData.Count < 1)
            {
                Nag("MSTD00018INF");
                log.Log("MSTD00018INF", "No Entertainment data found");
                return;
            }
            logic.Say("ButtonGenerateEntertain_Click", "pid = {0}", pid);

            ProcessID = pid;
            string fullName = "";
            string templateName = Server.MapPath(logic.Sys.GetText("TEMPLATE.FILE", "ENTERTAINMENT"));
            
            if (!File.Exists(templateName))
            {
                Nag("MSTD00021WRN", "Output Template file");
                log.Log("MSTD00021WRN", "Output Template " + templateName + "not found");
                return;
            }

            List<PVFormHeaderData> listNoAttachment = new List<PVFormHeaderData>();
            byte[] EntertainmentReport = null;
            try
            {
                // gather all attachment name, with PV No 
                EntertainmentReport = logic.PVList.GetEntertain(
                    listData,
                    templateName,
                    Server.MapPath(AppSetting.TempFileUpload),
                    ProcessID,
                    UserData, listNoAttachment);

                if (listNoAttachment.Count > 0 && EntertainmentReport != null)
                {
                    StringBuilder sNoAttach = new StringBuilder("");
                    int noAttachCount = 0;
                    foreach (PVFormHeaderData p in listNoAttachment)
                    {
                        if (noAttachCount > 0) sNoAttach.Append(", ");
                        sNoAttach.Append(p.PV_NO);
                        noAttachCount++;
                    }
                    Nag("MSTD0001INF",
                        "Missing Entertainment Attachment for PV No: "
                        + sNoAttach.ToString());
                }

                if (EntertainmentReport != null)
                {
                    Xmit.Send(EntertainmentReport, HttpContext.Current, 
                        "ENTERTAINMENT_LIST_" 
                        + DateTime.Now.ToString("MMdd_hhmm")
                        + ".xls");
                }

            }
            catch (IOException iox)
            {
                Nag("MSTD00002ERR", iox.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "IOException GetEntertain " + iox.Message + "\r\n" + iox.StackTrace);
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "Exception GetEntertain " + ex.Message + "\r\n" + ex.StackTrace);
            }
            // export file
            string filename = System.IO.Path.GetFileName(fullName);
            if (string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(filename))
            {
                Nag("MSTD00018INF");
                return;
            }

            
            log.Log("MSTD00001INF", "Generate Entertain FINISH");
        }

        protected void btnOkConfirmationCheck_Click(object sender, EventArgs e)
        {
            //bool result = false;

            List<PVListData> listData = null;
            if (PVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);
        }

        protected void btnPrintWorklistReport_Click(object sender, EventArgs e)
        {
            popUpWorklistReport.Hide();

            string me = "btnPrintWorklistReport_Click";
            int pid = 0;
            string userName = UserName;

            pid = log.Log("MSTD00001INF", me + " START",
                me, userName, me, 0);
            
            string serverPath = Server.MapPath("~/");
            byte[] WorklistReportBytes = null;
            try
            {
                // gather all attachment name, with PV No 
                string divs = sel.SelectedValues();

                if (divs.isEmpty())
                    divs = CommonFunction.CommaJoin(UserData.Divisions);
                else
                    divs = divs.Replace(';', ',');

                string filename = "WORKLIST_PV_" + DateTime.Now.ToString("yyyyMMdd_hhmm") + ".xls";

                WorklistReportBytes = logic.PVList.WorklistReport(
                    ProcessID
                    , UserData
                    , serverPath
                    , 1
                    , dtPVDateFrom.Date, dtPVDateTo.Date
                    , divs
                    , DelayToleranceText.Text.Int());

                if (WorklistReportBytes!=null) 
                {
                    Xmit.Send(WorklistReportBytes, HttpContext.Current, filename);
                }

            }
            catch (IOException iox)
            {
                Nag("MSTD00002ERR", iox.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "IOException " + me + " " + iox.Message + "\r\n" + iox.StackTrace);
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "Exception " + me + " " + ex.Message + "\r\n" + ex.StackTrace);
            }
            // export file
            
            if (WorklistReportBytes == null) //  string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(filename))
            {
                Nag("MSTD00018INF");
                return;
            }

           
            log.Log("MSTD00001INF", me + " FINISH", me);

        }

        protected void ButtonPrintWorklist_Click(object sender, EventArgs e)
        {
            bool Ok = false;

            Ok = validateDateFromTo("PV Date", dtPVDateFrom, dtPVDateTo);
            if (!Ok)
                return;
            
           

            popUpWorklistReport.Show();
        }

        private bool ValidateGenerateEntertain()
        {
            bool ok = false;
            do
            {
                if (string.IsNullOrEmpty(dtPostingDateFrom.Text) || string.IsNullOrEmpty(dtPostingDateTo.Text))
                {
                    Nag("MSTD00017WRN", "Posting date");
                    break;
                }

                if (dtPostingDateTo.Date.CompareTo(dtPostingDateFrom.Date) < 0)
                {
                    Nag("MSTD00029ERR", "Posting Date From", "Posting Date To");
                    break;
                }
                ok = true;

            } while (false);
            return ok;
        }

        private bool ValidatePrintTicket(List<PVListData> l)
        {
            int errCount = 0;
            foreach (PVListData d in l)
            {
                int statusCd = d.STATUS_CD.Int();
                string payMethod = d.PAY_METHOD_CD.ToUpper();
                if (statusCd == 28 || statusCd == 29)
                {
                    d.ERR = logic.Msg.Message("MSTD00094WRN", d.PV_NO);
                    errCount++;
                    continue;
                }
                if (statusCd!= 27)
                {
                    d.ERR = logic.Msg.Message("MSTD00037WRN", d.PV_NO);
                    errCount++;
                    continue;
                }
                if (!(payMethod.Equals("C") || payMethod.Equals("E")))
                {
                    d.ERR = logic.Msg.Message("MSTD00038WRN", d.PV_NO, "cash or cheque");
                    errCount ++;
                    continue;
                }
                if (d.DELETED)
                {
                    d.ERR = logic.Msg.Message("MSTD00002ERR", "Deleted Document " + d.PV_NO);
                    errCount++;
                    continue;
                }
                if (!logic.PVList.CanPrintTicketOn(d.PLANNING_PAYMENT_DATE ?? DateTime.Now,  d.PV_TYPE_CD.Int()))
                {
                    d.ERR = logic.Msg.Message("MSTD00215ERR");
                    errCount++;
                    continue;
                }
            }
            return (l != null && l.Count > 0 &&  errCount < 1);
        }

        private bool ValidatePrintTicket(ASPxGridView gridView)
        {
            bool checkValid = true;
            int selectedTickets = 0;
            if (gridView.Selection.Count >= 1)
            {
                for (int i = 0; i < gridView.VisibleRowCount; i++)
                {
                    if (!gridView.Selection.IsRowSelected(i)) continue;
                    ++selectedTickets;
                    string No = Convert.ToString(gridView.GetRowValues(i, "PV_NO"));
                    string YY = Convert.ToString(gridView.GetRowValues(i, "PV_YEAR"));

                    int pvNo = 0, pvYear = 0;

                    Int32.TryParse(No, out pvNo);
                    Int32.TryParse(YY, out pvYear);

                    PVHeaderData pv = logic.PVList.GetPVH(pvNo, pvYear);
                    if (pv == null) continue;

                    if (pv.STATUS_CD == 28 || pv.STATUS_CD == 29)
                    {
                        checkValid = false;
                        Nag("MSTD00094WRN", pvNo);
                        break;
                    }
                    else if (!(pv.STATUS_CD == 27))
                    {
                        checkValid = false;
                        Nag("MSTD00037WRN", pvNo);

                        break;
                    }

                    string paymentMethod = (pv.PAY_METHOD_CD ?? "").ToUpper();
                    if (!(paymentMethod.Equals("C") || paymentMethod.Equals("E")))
                    {
                        checkValid = false;

                        Nag("MSTD00038WRN", pvNo, "cash or cheque");
                        break;
                    }

                    // if (!logic.PVList.CanPrintTicketOf(pv.ACTIVITY_DATE_FROM ?? DateTime.MinValue, pv.PV_TYPE_CD))
                    if (!logic.PVList.CanPrintTicketOn(pv.PLANNING_PAYMENT_DATE ?? DateTime.Now, pv.PV_TYPE_CD))
                    {
                        checkValid = false;

                        Nag("MSTD00215ERR");
                        break;
                    }
                }
                checkValid = checkValid && (selectedTickets > 0);
            }
            else 
            {
                Nag("MSTD00009WRN");
                checkValid = false;
            }

            return checkValid;
        }

        private bool ValidatePrintCover(List<PVListData> l)
        {
            List<string> invalidDoc = new List<string>();
            if (l == null || l.Count < 1)
            {
                Nag("MSTD00009WRN");
                return false;
            }
            foreach (PVListData d in l)
            {
                if (d.STATUS_CD.Int() < 10)
                {
                    invalidDoc.Add(d.STATUS_CD);
                }
            }
            if (invalidDoc.Count > 0)
            {
                Nag("MSTD00039WRN", CommonFunction.CommaJoin(invalidDoc, ","), " Registered");
                return false;
            }
            return true;
        }

        private Boolean ValidatePrintCover(ASPxGridView gridView)
        {
            Boolean checkValid = true;
            if (gridView.Selection.Count >= 1)
            {
                List<string> nonValidPrintCover = new List<string>();
                for (int i = 0; i < gridView.VisibleRowCount; i++)
                {
                    if (gridView.Selection.IsRowSelected(i))
                    {
                        String pvNo = gridView.GetRowValues(i, "PV_NO").str();
                        String pvYear = gridView.GetRowValues(i, "PV_YEAR").str();
                        int no = pvNo.Int(0);
                        int yy = pvYear.Int(0);
                        PVHeaderData h = logic.PVList.GetPVH(no, yy);
                        if (h != null)
                        {
                            checkValid = h.STATUS_CD >= 10;
                        }
                        else
                        {
                            checkValid = false;
                            nonValidPrintCover.Add(pvNo);
                        }
                    }
                }

                if (nonValidPrintCover.Count > 0)
                {
                    Nag("MSTD00039WRN", CommonFunction.CommaJoin(nonValidPrintCover, ","), " Registered");
                }
            }
            else if (gridView.Selection.Count == 0)
            {

                Nag("MSTD00009WRN");
                checkValid = false;
            }
            return checkValid;
        }

        protected ASPxGridView GetSelectedTabGrid()
        {
            ASPxGridView g = null;
            switch (PVListTabs.ActiveTabIndex)
            {
                case 0: g= gridGeneralInfo; break;
                case 1: g= gridStatusUser; break;
                case 2: g= gridStatusFinance; break;
            }

            return g;
        }


        protected void ButtonPrintTicket_Click(object sender, EventArgs e)
        {
            List<PVListData> listData = null;
            bool valid = false;
            listData = GetSelectedRowValues(GetSelectedTabGrid());
            if (listData == null || listData.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            valid = ValidatePrintTicket(listData);

            if (!valid)
            {
                StringBuilder n = new StringBuilder("");
                foreach (PVListData d in listData)
                {
                    if (!d.ERR.isEmpty())
                    {
                        n.AppendLine(d.ERR);
                    }
                }
                string errs = n.ToString();
                if (!errs.isEmpty())
                {
                    lit.Text = MessageLogic.Wrap(errs, "ERR");
                }
                return;
            }

            if (valid)
            {
                StringBuilder tickets = new StringBuilder("");
                foreach (PVListData d in listData)
                {
                    tickets.Append(d.PV_NO.str() + " ");
                }
                logic.Say("ButtonPrintTicket_Click", "Print Ticket {0}", tickets.ToString());
                ErrorData _Err = new ErrorData();
                String cashier = logic.PVList.getCashierName();

                if (listData.Count > 0)
                {
                    logic.PVPrint.printTicket(this,
                                            Path.GetDirectoryName( Server.MapPath(AppSetting.CompanyLogo)),
                                            Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE", "DIR")),
                                            UserData,
                                            listData,
                                            cashier,
                                            ref _Err);
                }

                if (_Err.ErrID == 2)
                {
                    Nag("MSTD00002ERR", _Err.ErrMsg);
                }
            }
        }

        protected void ButtonPrintCover_Click(object sender, EventArgs e)
        {
            List<PVListData> listData = null;
            //bool valid = false;
            listData = GetSelectedRowValues(GetSelectedTabGrid());
            if (listData == null || listData.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }

            if (ValidatePrintCover(listData))
            {
                StringBuilder covers = new StringBuilder("");
                foreach (PVListData d in listData)
                {
                    covers.Append(d.PV_NO.str() + " ");
                }
                logic.Say("ButtonPrintCover_Click", "Print cover {0}", covers.ToString());
                ErrorData _Err = new ErrorData();

                if (listData.Count > 0)
                {
                    logic.PVPrint.printCover(this,
                                            Path.GetDirectoryName(Server.MapPath(Common.AppSetting.CompanyLogo)),
                                            Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE", "DIR")),
                                            UserData,
                                            listData,
                                            ref _Err);
                }

                if (_Err.ErrID == 2)
                {

                    Nag("MSTD00002ERR", _Err.ErrMsg);
                }
            }
        }

        private void ProcessHistory_Click(ASPxGridView gridView)
        {
            logic.Say("ProcessHistory_Click", "History");
            if (gridView.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }

            if (gridView.Selection.Count > 1)
            {
                Nag("MSTD00016WRN");
                return;
            }

            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (gridView.Selection.IsRowSelected(i))
                {
                    String pvNo = gridView.GetRowValues(i, "PV_NO").ToString();
                    HiddenField hidGridPVYear = gridView.FindRowCellTemplateControl(
                        i, gridView.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
                    HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(
                        i, gridView.Columns["PV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;

                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "ELVIS_LogHistory",
                        "openWin('../70History/History.aspx?pv_no=" + pvNo + "&pv_year=" + hidGridPVYear.Value.ToString() + "','ELVIS_LogHistory');",
                        true);
                }
            }
        }

        protected void ButtonHistory_Click(object sender, EventArgs e)
        {
            if (PVListTabs.ActiveTabIndex == 0)
                ProcessHistory_Click(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                ProcessHistory_Click(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                ProcessHistory_Click(gridStatusFinance);
        }


        protected void DownloadList()
        {
            
            UserData userdata = (UserData)Session["UserData"];
            
            try
            {
                Xmit.Send(logic.PVList.Get(
                            UserName,
                            Server.MapPath(Common.AppSetting.CompanyLogo),
                            SearchCriteria()
                        ), HttpContext.Current, "PV" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls");
            }

           
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                Nag("MSTD00002ERR", "Error Download List : " + ex.Message);
            }
            //this.Response.End();
        }
        protected void ButtonDownload_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonDownload_Click", "Download");
            if (validateInputSearch())
            {
                if (UserData.Roles.Contains("ELVIS_CASHIER"))
                {
                    GetCashierReport();
                }
                else
                {
                    DownloadList();
                }
            }
        }

        protected void lvVendorCd_Click(object sender, ImageClickEventArgs e)
        {
            ModalButton = Common.Enum.ModalButton.VendorCd;
            LitPopUpMessage.Text = "";
            panelModal.GroupingText = "List Vendor Code";
            lblPopUpSearchCd.Text = "Vendor Code";
            lblPopUpSearchDesc.Text = "Vendor Name";
            //txtSearchDesc.Visible = false;
            gridPopUpVendorCd.Visible = true;
            gridPopUpVendorCd.DataSource = null;
            ClearModal();
            //LoadGridPopUpVendorCd();
            popUpList.Show();
        }

        protected void otherAmount_Click(object sender, EventArgs e)
        {

        }

        protected void btnClearPopUp_Click(object sender, EventArgs e)
        {
            ClearModal();
        }

        protected void btnSearchPopUp_Click(object sender, EventArgs e)
        {
            switch (ModalButton)
            {
                case Common.Enum.ModalButton.VendorCd:
                    LoadGridPopUpVendorCd();
                    break;
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            switch (ModalButton)
            {
                case Common.Enum.ModalButton.VendorCd:
                    {
                        if (gridPopUpVendorCd.Selection.Count == 1)
                        {
                            for (int i = 0; i <= gridPopUpVendorCd.VisibleRowCount; i++)
                            {
                                if (gridPopUpVendorCd.Selection.IsRowSelected(i))
                                {
                                    TextBoxVendorCode.Text = gridPopUpVendorCd.GetRowValues(i, "VENDOR_CD").ToString();
                                    TextBoxVendorDesc.Text = gridPopUpVendorCd.GetRowValues(i, "VENDOR_NAME").ToString();
                                }
                            }
                        }
                        else if (gridPopUpVendorCd.Selection.Count > 1)
                        {
                            LitPopUpMessage.Text = Nagging("MSTD00016WRN");
                            return;
                        }
                        else
                        {
                            LitPopUpMessage.Text = Nagging("MSTD00009WRN");
                            return;
                        }
                        ClearModal();
                        gridPopUpVendorCd.Selection.UnselectAll();
                        gridPopUpVendorCd.Visible = false;
                        popUpList.Hide();
                    }
                    break;
            }
        }

        protected void btnClosePopUp_Click(object sender, EventArgs e)
        {
            ClearModal();
            switch (ModalButton)
            {
                case Common.Enum.ModalButton.VendorCd:
                    gridPopUpVendorCd.Selection.UnselectAll();
                    gridPopUpVendorCd.Visible = false;
                    break;
            }
            popUpList.Hide();
        }

        protected void ButtonCloseTotalAmountCurr_Click(object sender, EventArgs e)
        {
            gridTotalAmountCurr.Visible = false;
            ModalPopupTotalAmountCurr.Hide();
        }

        protected void LoadGridPopUpOtherCurr(object sender, EventArgs e)
        {
            gridTotalAmountCurr.Visible = true;
            LitMsgTotalAmountCurr.Text = "";
            panelTotalAmountCurr.GroupingText = "Total Amount Sum-up / Currency";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;

            string pvNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "PV_NO").str();

            HiddenField hidGridPVYear = gridGeneralInfo.FindRowCellTemplateControl(
                container.VisibleIndex, gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
            List<OtherAmountData> _list = logic.Look.GetAmounts(pvNo, hidGridPVYear.Value);
            gridTotalAmountCurr.DataSource = _list;
            gridTotalAmountCurr.DataBind();
            ModalPopupTotalAmountCurr.Show();
        }

        protected void LoadGridPopUpTotalAmountCurr(object sender, EventArgs e)
        {
            gridTotalAmountCurr.Visible = true;
            LitMsgTotalAmountCurr.Text = "";
            panelTotalAmountCurr.GroupingText = "Total Amount Sum-up / Currency";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            string pvNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "PV_NO").str();
            HiddenField hidGridPVYear = gridGeneralInfo.FindRowCellTemplateControl(
                container.VisibleIndex, gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
            List<OtherAmountData> _list = logic.Look.GetAmounts(pvNo, hidGridPVYear.Value);
            gridTotalAmountCurr.DataSource = _list;
            gridTotalAmountCurr.DataBind();
            ModalPopupTotalAmountCurr.Show();
        }

        protected void gridTotalAmountCurr_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoTotalAmountCurr = gridTotalAmountCurr.FindRowCellTemplateControl(e.VisibleIndex,
                    gridTotalAmountCurr.Columns["NO"] as GridViewDataColumn, "litGridNoTotalAmountCurr") as Literal;
                litGridNoTotalAmountCurr.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }
        private void SetPopupDirectToMe()
        {
            ddlReason.SelectedIndex = -1;

            popUpDirect.Show();
        }
        private List<PVListData> GetSelectedRowValues(ASPxGridView gridView)
        {
            List<PVListData> listData = new List<PVListData>();
            if (gridView == null)
                return listData;
            PVListData data;
            Object[] dataTemp;

            string[] valueToGet = { "PV_NO", "PV_YEAR", "PV_TYPE_NAME" };

            for (int i = 0; i < gridView.Selection.Count; i++)
            {

                data = new PVListData();
                dataTemp = gridView.GetSelectedFieldValues(valueToGet)[i] as Object[];

                if (dataTemp[0] != null)
                    data.PV_NO = dataTemp[0].ToString();
                else
                    data.PV_NO = "";

                if (dataTemp[1] != null)
                {
                    data.PV_YEAR = dataTemp[1].str();
                }
                else
                {
                    data.PV_YEAR = StrTo.NULL_YEAR.str();
                }

                // Start Rinda Rahayu 20160328
                if (dataTemp[2] != null)
                {
                    data.PV_TYPE_NAME = dataTemp[2].str();
                }
                else
                {
                    data.PV_TYPE_NAME = "";
                }
                // End Rinda Rahayu 20160328

                logic.PVList.Fill(data);
                listData.Add(data);

            }
            return listData;
        }

        protected void PVDateFrom_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtPVDateTo, true);
        }

        protected void PVDateTo_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtPVDateFrom, false);
        }

        protected void ActivityDateFrom_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtActivityDateTo, true);
        }

        protected void ActivityDateTo_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtActivityDateFrom, false);
        }

        protected void SubmitDateFrom_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtSubmitDateTo, true);
        }

        protected void SubmitDateTo_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtSubmitDateFrom, false);
        }

        protected void PlanningPaymentDateFrom_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, PlanningPaymentDateTo, true);
        }

        protected void PlanningPaymentDateTo_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, PlanningPaymentDateFrom, false);
        }

        protected void PaidDateFrom_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtPaidDateTo, true);
        }

        protected void PaidDateTo_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtPaidDateFrom, false);
        }

        protected void PostingDateFrom_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtPostingDateTo, true);
        }

        protected void PostingDateTo_ValueChanged(object sender, EventArgs e)
        {
            date_ValueChanged(sender, dtPostingDateFrom, false);
        }

        protected void PVNoFrom_TextChanged(object sender, EventArgs e)
        {
            text_ValueChanged(sender, TextBoxPVNoTo);
        }

        protected void PVNoTo_TextChanged(object sender, EventArgs e)
        {
            text_ValueChanged(sender, TextBoxPVNoFrom);
        }

        protected void VendorCode_TextChanged(object sender, EventArgs e)
        {
            //TextBox txtVendorCode = sender as TextBox;
            ASPxGridLookup txtVendorCode = TextBoxVendorCode;
            if (!String.IsNullOrEmpty(txtVendorCode.Text))
            {
                List<VendorCdData> _list = logic.Look.GetVendorCodeData(txtVendorCode.Text, "");
                if (_list.Count > 0)
                {
                    VendorCdData firstVendorCode = _list[0];
                    TextBoxVendorDesc.Text = firstVendorCode.VENDOR_NAME;
                }
                else
                {
                    Nag("MSTD00021WRN", "Vendor " + txtVendorCode.Text);
                    TextBoxVendorDesc.Text = "";
                }
            }
            else
            {
                TextBoxVendorDesc.Text = "";
            }
        }

        protected void gridSapDocNo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoSapDocNo = gridSapDocNo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridSapDocNo.Columns["NO"] as GridViewDataColumn, "litGridNoSapDocNo") as Literal;
                litGridNoSapDocNo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonClosePopUpSapDocNo_Click(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = false;
            ModalPopupSapDocNo.Hide();
        }

        protected void LoadGridPopUpSapDocNo(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = true;
            LitMsgSapDocNo.Text = "";
            panelSapDocNo.GroupingText = "SAP Document Number";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            string pvNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "PV_NO").str();
            HiddenField hidGridPVYear = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
                            gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;

            List<SapDocNoData> _list = logic.Look.GetSapDocNo(pvNo, hidGridPVYear.Value);
            gridSapDocNo.DataSource = _list;
            gridSapDocNo.DataBind();
            ModalPopupSapDocNo.Show();
        }

        protected void gridBudgetNo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoBudgetNo = gridBudgetNo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridBudgetNo.Columns["NO"] as GridViewDataColumn, "litGridNoBudgetNo") as Literal;
                litGridNoBudgetNo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonCloseBudgetNo_Click(object sender, EventArgs e)
        {
            gridBudgetNo.Visible = false;
            ModalPopupBudgetNo.Hide();
        }


        protected void ButtonClosePopUpBudgetNo_Click(object sender, EventArgs e)
        {
            gridBudgetNo.Visible = false;
            ModalPopupBudgetNo.Hide();
        }

        protected void gridAttachment_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoAttachment = gridAttachment.FindRowCellTemplateControl(
                    e.VisibleIndex, gridAttachment.Columns["NO"] as GridViewDataColumn, "litGridNoAttachment") as Literal;
                litGridNoAttachment.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonClosePopUpAttachment_Click(object sender, EventArgs e)
        {
            gridAttachment.Visible = false;
            ModalPopupAttachment.Hide();
        }

        protected void LoadGridPopUpAttachment(object sender, EventArgs e)
        {
            gridAttachment.Visible = true;
            LitMsgAttachment.Text = "";
            panelAttachment.GroupingText = "Attachment";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            string pvNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "PV_NO").str();
            HiddenField hidGridPVYear = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
                            gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
            List<AttachmentData> _list = logic.Look.GetAttachmentData(pvNo, hidGridPVYear.Value);
            foreach (AttachmentData a in _list)
            {
                a.URL = Common.AppSetting.FTP_DOWNLOADHTTP + a.URL;                
            }
            gridAttachment.DataSource = _list;
            gridAttachment.DataBind();
            ModalPopupAttachment.Show();
        }

        protected void LoadGridPopUpVersion(object sender, EventArgs e)
        {
            gridVersion.Visible = true;
            LitMsgVersion.Text = "";
            panelVersion.GroupingText = "Version";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            string pvNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "PV_NO").str();
            HiddenField hidGridPVYear = gridGeneralInfo.FindRowCellTemplateControl(
                container.VisibleIndex, gridGeneralInfo.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
            List<PVFormHistoryData> _list = logic.PVList.GetHistoryData(pvNo, hidGridPVYear.Value);
            Session["PV_NO"] = pvNo;
            Session["PV_YEAR"] = hidGridPVYear.Value;
            Session["GRID_HISTORY"] = _list;
            gridVersion.DataSource = _list;
            gridVersion.DataBind();
            gridVersion.DetailRows.ExpandAllRows();
            ModalPopupVersion.Show();
        }

        protected void ButtonClosePopUpVersion_Click(object sender, EventArgs e)
        {
            gridVersion.Visible = false;
            ModalPopupVersion.Hide();
            Session["PV_NO"] = null;
            Session["PV_YEAR"] = null;
            Session["GRID_HISTORY"] = null;
        }

        protected void gridPVDetail_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView g = sender as ASPxGridView;
            if (g != null)
            {
                if (Session == null || Session["PV_NO"] == null)
                {
                    g.DataSource = null;
                    g.Visible = false;
                    return;
                }
                int pvNo = Convert.ToInt32(Session["PV_NO"].ToString());
                int pvYear = Convert.ToInt16(Session["PV_YEAR"].ToString());
                int version = Convert.ToInt16(g.GetMasterRowKeyValue());
                List<PVFormHistoryData> _list = (List<PVFormHistoryData>)Session["GRID_HISTORY"];
                if (_list != null && _list.Count > 0)
                {
                    bool noMatch = true;
                    foreach (var d in _list)
                    {
                        if (d.PVNumber == pvNo
                            && d.PVYear == pvYear
                            && d.Version == version
                            && d.Details != null
                            && d.Details.Count > 0)
                        {
                            noMatch = false;
                            g.Visible = true;
                            g.DataSource = d.Details;
                            break;
                        }
                    }

                    if (noMatch)
                    {
                        g.DataSource = null;
                        g.Visible = false;
                    }

                }
                else
                {
                    g.DataSource = null;
                    g.Visible = false;
                }
            }
        }

        protected void gridPVDetail_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                object del = e.GetValue("IsDeleted");
                if (del != null)
                {
                    bool deleted = Convert.ToBoolean(del);
                    if (deleted)
                        e.Row.Style.Add(HtmlTextWriterStyle.BackgroundColor, red);
                }
            }
        }

        protected void gridPVDetail_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            SetVersionCellColor(e, "CostCenterCode");
            SetVersionCellColor(e, "CostCenterName");
            SetVersionCellColor(e, "Description");
            SetVersionCellColor(e, "CurrencyCode");
            SetVersionCellColor(e, "Amount");
            SetVersionCellColor(e, "TaxCode");
            SetVersionCellColor(e, "TaxNumber");
            SetVersionCellColor(e, "InvoiceNumber");
        }

        private void SetVersionCellColor(ASPxGridViewTableDataCellEventArgs e, String fieldName)
        {
            if (e.DataColumn == null) return;

            if (e.DataColumn.FieldName == fieldName)
            {
                if (e.GetValue(fieldName + "Change") != null)
                {
                    object cha = e.GetValue(fieldName + "Change");
                    if (cha != null)
                    {
                        bool changed = Convert.ToBoolean(cha);
                        if (changed)
                            e.Cell.Style.Add("background-color", yellow);
                    }
                }
            }
        }

        protected VoucherSearchCriteria SearchCriteria()
        {

            return new VoucherSearchCriteria(
                dtPVDateFrom.Text,
                dtPVDateTo.Text,
                TextBoxPVNoFrom.Text,
                TextBoxPVNoTo.Text,
                DropDownListPVType.SelectedValue,
                TextBoxVendorCode.Text,
                sel.SelectedValues(CommonFunction.CommaJoin(UserData.Divisions, ";"), (UserData != null) ? UserData.isFinanceDivision : false),
                getSelectedStatus(),
                DropDownListNotice.SelectedValue,
                (ddlTransactionType.Value != null) ? ddlTransactionType.Value.ToString() : null,
                ddlPaymentMethod.SelectedValue,
                ddlVendorGroup.SelectedValue,
                dtActivityDateFrom.Text,
                dtActivityDateTo.Text,
                dtSubmitDateFrom.Text,
                dtSubmitDateTo.Text,
                PlanningPaymentDateFrom.Text,
                PlanningPaymentDateTo.Text,
                dtPaidDateFrom.Text,
                dtPaidDateTo.Text,
                dtPostingDateFrom.Text,
                dtPostingDateTo.Text,
                dtVerifiedDateFrom.Text,
                dtVerifiedDateTo.Text,
                DropDownListWorkflowStatus.SelectedValue,
                DropDownListHoldFlag.SelectedValue,
                DropDownListSettlementStatus.SelectedValue,
                "", //(DropDownListUnknownDataEdit.SelectedValue.Equals("1") ? UserName : ""),
                CreatedByEdit.Text,
                NextApproverEdit.Text,
                CancelFlag.Checked,
                DeleteFlag.Checked);
        }

        protected void Act(Common.Enum.ApprovalEnum a)
        {
            Master.ApprovalType = a;
            string _Command = resx.K2ProcID((a == Common.Enum.ApprovalEnum.Approve) ? "Form_Registration_Approval" : "Form_Registration_Rejected");
            List<PVListData> listData = null;
            if (PVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (PVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (PVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);

            if (listData.Count > 0)
            {
                List<string> OK = new List<string>();
                List<string> NG = new List<string>();
                foreach (string reff in listData.Select(pv => pv.PV_NO + pv.PV_YEAR).ToList())
                {
                    if (logic.PVRVWorkflow.Go(reff, _Command, UserData, ""))
                    {
                        OK.Add(reff);
                    }
                    else
                    {
                        NG.Add(reff);
                    }
                }

            }
            else
            {
                Nag("MSTD00009WRN");
            }
        }

        protected void ButtonApprove_Click(object sender, EventArgs e)
        {
            Act(Common.Enum.ApprovalEnum.Approve);
            LoadData();
        }

        protected void ButtonReject_Click(object sender, EventArgs e)
        {
            pnlMassAct.GroupingText = "Reject";
            btnAct.Text = "Reject";
            btnAct.BackColor = System.Drawing.Color.Red;
            ModalPopupConfirmMassAct.Show();                        
        }

        protected void btnAct_Click(object sender, EventArgs e)
        {
            ModalPopupConfirmMassAct.Hide();
            Act(Common.Enum.ApprovalEnum.Reject);
            LoadData();
        }

        protected void Approve(List<string> x)
        {
            try
            {
                #region Get --> Command
                string _Command = "";
                if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                    _Command = resx.K2ProcID("Form_Registration_Approval");
                else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                    _Command = resx.K2ProcID("Form_Registration_Rejected");
                #endregion
                foreach (string r in x)
                {
                    string rno = NoColon(r);
                    #region Do --> Approve-Reject
                    if (logic.PVRVWorkflow.Go(rno, _Command, UserData, ""))
                    //if (true)
                    {
                        if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        {
                            litMessage.Text += Nagging("MSTD00060INF", "Approve");
                        }
                        else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        {
                            litMessage.Text += Nagging("MSTD00060INF", "Reject");
                        }
                    }
                    else
                    {
                        if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        {
                            litMessage.Text += Nagging("MSTD00062ERR", "Approve");
                        }
                        else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        {
                            litMessage.Text += Nagging("MSTD00062ERR", "Reject");
                        }
                    }
                }
                    #endregion
            }
            catch (Exception Ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(Ex.InnerException)) ? Convert.ToString(Ex.InnerException) : Ex.Message;
                litMessage.Text += _m.SetMessage(_ErrMsg, "ERR");
            }
        }


        #region Function, added by Akhmad Nuryanto
        private List<string> GetSelectedRow(ASPxGridView gridView)
        {

            List<string> listData = new List<string>();
            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (gridView.Selection.IsRowSelected(i))
                {
                    HiddenField hidGridPVYear = gridView.FindRowCellTemplateControl(
                        i, gridView.Columns["PV_NO"] as GridViewDataColumn, "hidGridPVYear") as HiddenField;
                    String pvNo = gridView.GetRowValues(i, "PV_NO").ToString();
                    string pvYear = hidGridPVYear.Value;

                    if (!string.IsNullOrEmpty(pvNo) && !string.IsNullOrEmpty(pvYear))
                    {
                        listData.Add(pvNo + ":" + pvYear);
                    }
                }
            }

            return listData;
        }

        private string NoColon(string s)
        {
            return s.Replace(":", "");
        }

        private string CombineUrl(string d, string f)
        {
            if (d.Length > 1 && !d.Substring(d.Length - 1, 1).Equals("/"))
            {
                d = d + "/";
            }
            return d + HttpUtility.UrlEncode(f);
        }

        #endregion

        #region EFB
        protected void gridGeneralInfo_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            if (e.CommandArgs.CommandName.ToString() == "DownloadPDF")
            {
                litEFBMessage.Text = "";

                string tpath = Common.AppSetting.TempFileZIP;//D:\TAM Efaktur\ELVISDev\ELVISDashBoard2012\UploadTemp
                string opath = Page.Server.MapPath(tpath);
                tpath = tpath.Substring(1, tpath.Length - 1);
                string oLink = Page.Request.Url.Scheme + Uri.SchemeDelimiter
                + Page.Request.Url.Authority + tpath; //http://localhost:9999/UploadTemp/

                if (!Directory.Exists(opath))
                    Directory.CreateDirectory(opath);

                string PV_No = e.CommandArgs.CommandArgument.ToString();
                string zipFileName = "PDF_" + PV_No + "_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
                List<string> FilePaths = logic.PVList.searchPdfWHT(PV_No);
                if (FilePaths.Count > 0)
                {
                    string PathFileZip = logic.PVList.GenerateZipFile(FilePaths, opath, zipFileName);
                    if (PathFileZip != "")
                    {
                        Response.Redirect(new Uri(PathFileZip.Replace(opath, oLink)).ToString());
                    }
                    else
                    {
                        litEFBMessage.Text = Nagging("MSTD00242ERR");
                    }   
                }
                else
                {
                    litEFBMessage.Text = Nagging("MSTD00242ERR");
                }

            }
        }
        
        #endregion
    }
}
