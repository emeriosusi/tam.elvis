﻿<%@ Page Title="PV Approval" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
AutoEventWireup="true" CodeBehind="PVApproval.aspx.cs" Inherits="ELVISDashBoard._30PvFormList.PVApproval" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
	TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
	TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="xPre" ContentPlaceHolderID="pre" runat="server">
    <link href="../approval.css" rel="stylesheet" type="text/css" />
    <script src="../approval.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript">
		function paidConfirmation() {
			var selection = confirm("Are sure want to paid this PV ?");
			if (selection) {
				$("#hidDialogConfirmation").val("y");
			} else {
				$("#hidDialogConfirmation").val("n");
			}
			loading();
			return selection;
		}
		function checkConfirmation() {
			var selection = confirm("Are sure want to check this PV ?");
			if (selection) {
				$("#hidDialogConfirmation").val("y");
			} else {
				$("#hidDialogConfirmation").val("n");
			}
            loading();
			return selection;
		}
		function verifyConfirmation() {
		    var selection = confirm("Mark PV as Verified ?");
		    if (selection) {
		        $("#hidDialogConfirmation").val("y");
		    } else {
		        $("#hidDialogConfirmation").val("n");
		    }
		    loading();
		    return selection;
		}

		function cellBlink() {
		    var dueDate = new Date(document.getElementById('txtDueDate').value);
		    if (dueDate < (new Date())) {
		        document.getElementById('blinkCell').style.backgroundColor = 'yellow';
		    } else {
		        document.getElementById('blinkCell').style.backgroundColor = 'transparent';
		    }
		    setTimeout('cellBlink2()', 500);
		}
		function cellBlink2() {
		    var dueDate = new Date(document.getElementById('txtDueDate').value);
		    if (dueDate < (new Date())) {
		        document.getElementById('blinkCell').style.backgroundColor = 'red';
		    } else {
		        document.getElementById('blinkCell').style.backgroundColor = 'transparent';
		    }
		    setTimeout('cellBlink()', 500);
		}
		function search() {		    
		    var btn = document.getElementById('btnSearch');
		    btn.click();
		    loading();
		    return false;
		}
		function releaseNotice(noticeNoParam) {
		    var btn = document.getElementById('btnReleaseNotice');
		    var noticeNo = document.getElementById('txtNoticeNo');
		    noticeNo.value = noticeNoParam;
		    btn.click();
		    loading();
		    return false;
		}
		function holdConfirmation() {
		    var confirmation = 'Are sure want to hold/un-hold this PV ?';
		    if (confirm(confirmation)) {
		        var btn = document.getElementById('btnHold');
		        btn.click();
		        loading();
		        return false;
		    }
		}

		function ShowApprovals() {
		    if (!$("#ApprovalOverviewDiv").hasClass("hidden")) return;

		    $("#ApprovalOverviewDiv").removeClass("hidden");

		    var docNo = $('#txtPVNo').val();
		    var docYear = $('#txtPVYear').val();
		    var userName = $('#hidUserName').val();
		    var statusCd = $('#hidStatusCD').val();

		    GetApprov(docNo, docYear, 1, userName, statusCd, "gAppUser");
		    GetApprov(docNo, docYear, 2, userName, statusCd, "gAppFinance");
		}


		$(document).ready(function () {

		    $("#txtPVNo").focus(function () { $(this).select() });
		    $("#txtPVYear").focus(function () {
		        if ($(this).val().length < 1)
		            $(this).val((new Date()).getFullYear().toString());
		        else {
		        }
		        $(this).select()
		    });
		    cellBlink();


		    $("#showApprovalOverview").click(function (event) {
		        // event.preventDefault();
		        ShowApprovals();
		    });

		    if ($("#upnlNotice")[0].scrollHeight < 200) {
		        ShowApprovals();
		    }


		    var prevTop = 0;
		    $(document).scroll(function (e) {
		        var currTop = $(this).scrollTop();
		        if (prevTop != currTop) {
		            prevTop = currTop;
		            if (($(document).height() - currTop - $(window).height()) < 20) {
		                ShowApprovals();
		            }
		        }
		    });
		});

	</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
<div id="bodyContent" style="width:100%; height: 590px;">
	<asp:UpdatePanel runat="server" ID="upnLitMessage">
		<Triggers>
		</Triggers>
		<ContentTemplate>
			<asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
			<asp:HiddenField runat="server" ID="hidPageMode" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<asp:HiddenField runat="server" ID="hidPageTitle" Value="PV Approval" />
	<asp:HiddenField runat="server" ID="hidScreenID" />
	<asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />
	<asp:UpdatePanel runat="server" ID="upnContentSection">
		<Triggers>
		</Triggers>
		<ContentTemplate>
			<div style="visibility:hidden; width:0px; height:0px;">
				<asp:TextBox ID="txtDueDate" runat="server" ClientIDMode="Static"/>
				<asp:Button ID="btnSearch" runat="server" Text="Search"
                ClientIDMode="Static"
					onclick="btnSearch_Click" UseSubmitBehavior="False"/>
				<asp:TextBox ID="txtNoticeNo" runat="server" ClientIDMode="Static"></asp:TextBox>
				<asp:Button ID="btnReleaseNotice" ClientIDMode="Static"  runat="server" Text="Release Notice"
					onclick="btnReleaseNotice_Click" UseSubmitBehavior="False" />
				<asp:HiddenField ID="hidStdWording" runat="server"/>
                <asp:HiddenField ID="hidUserName" runat="server" ClientIDMode="Static" />
				<asp:HiddenField runat="server" ID="hidStatusCD" ClientIDMode="Static" />
			</div>
			<div class="contentsection" style="width:985px !important;">
				<table border="0" cellpadding="3" cellspacing="1" 
					style="table-layout:fixed;">
					<tr>
						<td valign="baseline" class="la1">
							<asp:Label ID="lblPVTypeLabel" runat="server">PV Type</asp:Label>
						</td>
						<td valign="top" class="col col1">
							<asp:Literal ID="litPVType" runat="server"></asp:Literal>
						</td>
						<td valign="baseline" class="la2">
							<asp:Label ID="lblPVDateLabel" runat="server">PV Date</asp:Label>
						</td>
						<td valign="top" class="col col2">
							<asp:Literal ID="litPVDate" runat="server"></asp:Literal>
						</td>
						<td valign="baseline" class="la3">
							<asp:Label ID="lblPVNoLabel" runat="server">PV No.</asp:Label>
						</td>
						<td valign="top" class="col col3">
							<asp:TextBox ID="txtPVNo" runat="server" Width="164px" Height="15px" MaxLength="9" ClientIDMode="Static"></asp:TextBox>
						</td>
						<td valign="baseline" class="la4">
							<asp:Label ID="lblPVYearLabel" runat="server">PV Year</asp:Label>
						</td>
						<td valign="top" class="col col4">
							<asp:TextBox ID="txtPVYear" runat="server" Width="55px" Height="15px" MaxLength="4" ClientIDMode="Static"></asp:TextBox>
						</td>
					</tr>
					<tr>
                        <td valign="baseline" >
							<asp:Label ID="lblIssuingDivisionLabel" runat="server">Issuing Division</asp:Label>
						</td>
						<td valign="top" class="col col1">
							<asp:Literal ID="litIssuingDivision" runat="server"></asp:Literal>
							<asp:HiddenField runat="server" ID="hidIssuingDivisionID" />
						</td>
						<td valign="baseline" >
							<asp:Label ID="lblVendorCodeLabel" runat="server">Vendor Code</asp:Label>
						</td>
						<td valign="top" class="col col2">
							<asp:Literal ID="litVendorCode" runat="server"></asp:Literal>
						</td>
						<td valign="baseline" >
							<asp:Label ID="lblVendorNameLabel" runat="server">Vendor Name</asp:Label>
						</td>
						<td valign="top" class="col">
							<asp:Literal ID="litVendorName" runat="server"></asp:Literal>
						</td>
                        <td id="blinkCell" rowspan="2" colspan="2" style="text-align: left; padding-top: 4px;" >
                         <asp:Label ID="lblDueDateLabel" runat="server">Planning Payment Date:</asp:Label>
							<br />
							<span class="largeFont">
								<asp:Literal ID="litDueDate" runat="server" />
							</span>                            
                            <div ID="lblHold" runat="server" visible="false" class="hold">HOLD</div>
                        </td>						
					</tr>
					<tr>
						<td valign="baseline" >
							<asp:Label ID="lblTransactionTypeLabel" runat="server">Transaction Type</asp:Label>
						</td>
						<td valign="top" class="col">
							<asp:Literal ID="litTransactionType" runat="server"></asp:Literal>
						</td>
						<td valign="baseline" >
							<asp:Label ID="lblPaymentMethodLabel" runat="server">Payment Method</asp:Label>
						</td>
						<td valign="top" class="col">
							<asp:Literal ID="litPaymentMethod" runat="server"></asp:Literal>
						</td>
                        <td valign="baseline" >
							<asp:Label ID="lblStatus" runat="server">Status</asp:Label>
						</td>
                        <td valign="top" class="col">
							<asp:Literal ID="litStatus" runat="server"></asp:Literal>
						</td>
					</tr>
                    </table>
                    <div class="row" ID="rowBudgetDiv" runat="server" visible="false" clientidmode="Static">
                        <div class="la1 colBudget">
							<asp:Label ID="lblBudgetNo" runat="server">Budget No</asp:Label>
                        </div>
                        <div class="col col1 colBudget">
							<asp:Literal ID="litBudgetNo" runat="server"></asp:Literal>&nbsp;
                            <dx:ASPxHyperLink ID="lnkBudgetNo" ClientInstanceName="lnkBudgetNo" Text="more" runat="server" 
                                                CssClass="budgetNumber-more-link" Visible="false">
                                <ClientSideEvents Click="function(sender, evt) { $find('popMultipleBudget').show(); }" />
                            </dx:ASPxHyperLink>
						</div>
                        <div class="col colBudgetName">
                            <asp:Literal ID="litBudgetName" ClientIDMode="Static" runat="server"/>                        
                        </div>
                        <!--Added by Akhmad Nuryanto, 22/09/2012-->
                        <div class="col colBudgetRemain">    
                            <div id="divRemainingBudget" runat="server" clientidmode="Static" visible="false">
                                <asp:Label ID="lblRemainingBudget" runat="server">Remaining Budget</asp:Label>
                                &nbsp;
                                <asp:Literal ID="litRemainingBudget" runat="server"></asp:Literal>
                                &nbsp;
                                <asp:LinkButton ID="lnkHideRemainingBudget" runat="server"
                                    Text="Hide" OnClick="lnkHideRemainingBudget_Click" />
                            </div>
                            <div id="divShowRemainingBudget" runat="server" clientidmode="Static" visible="true">
                                <asp:LinkButton ID="lnkRemainingBudget" runat="server" 
                                    Text="Show Remaining Budget" 
                                    OnClick="lnkRemainingBudget_Click"> 
                                </asp:LinkButton>
                            </div>                        
                        </div>
                        <!--end-->
                   
				    </div>
			   </div>
		</ContentTemplate>  
	</asp:UpdatePanel>
	
	<center>
		<asp:Panel runat="server" id="panelModal" CssClass="speakerPopupList">
			<asp:UpdatePanel runat="server" ID="upnModal">       
				<ContentTemplate>
					<table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="100%">
                        <tr> 
                            <td colspan="3" padding="0">
                                <div class="row" ID="divRow1" runat="server" clientidmode="Static">
                                                
                                    <div id="divSuspense" class="divSuspense style5" runat="server" clientidmode="Static">
                                        <div class="labeltotxl med">Total Suspense Amount</div>
                                        <div class="coltot med"> : </div>
                                        <div class="vtot xl"><asp:Literal ID="litTotalSuspense" runat="server"/></div>
                                    </div>
                                                
                                    <div class="colright">
                                        <asp:LinkButton runat="server" ID="linkUnsettled" Text="0 UNSETTLED" 
                                            Visible="false" 
                                            CssClass="pvapproval-unsettled-notification"
                                            OnClick="linkUnsettled_Click"/>                                            
                                    </div>
                                </div>

                                <div class="row" id="divRow2" runat="server" clientidmode="Static">
                                    <div class="colleft">
										<div id="divIDR" class="divIDR" runat="server" clientidmode="Static">
                                            <div class="labeltotxl med">Total Amount (IDR)</div> 
                                            <div class="coltot med"> : </div>
                                            <div class="vtot xl"><asp:Literal ID="litTotalAmount" runat="server"/></div> 
                                        </div> 
                                    </div>
                                    <div class="colmiddle">
                                        <div id="divAmounts" class="divAmounts" runat="server" clientidmode="Static">
                                            <div class="labeltot med">Total Amount / Curr</div> 
                                            <div class="coltot med"> : </div>
                                            <div class="vtot med"><asp:Literal ID="litAmounts" runat="server"/></div> 
                                        </div>                                            
                                    </div>
                                    <div class="colright">                                                    
                                            <a href="#APPROVALS" id="showApprovalOverview">Approval Overview</a>                                                   
                                    </div>
                                </div>
                            </td>
                        </tr>
						<tr>
							<td colspan="3" class="style2">
								<dx:ASPxGridView ID="gridApprovalList" runat="server" 
									AutoGenerateColumns="False"  Width="100%" 
									ClientInstanceName="gridApprovalList" Styles-AlternatingRow-CssClass="even value-pv"
									OnCustomCallback="grid_CustomCallback"
									OnHtmlRowCreated="gridApprovalList_HtmlRowCreated" KeyFieldName = "PV_NO"
									EnableCallBacks="false" OnPageIndexChanged="gridApprovalList_PageIndexChanged">
									<Columns>
										<dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" Width="30px">
											<DataItemTemplate>
												<asp:Literal runat="server" ID="litGridNo"></asp:Literal>
											</DataItemTemplate>
											<CellStyle HorizontalAlign="Center">
											</CellStyle>
										</dx:GridViewDataTextColumn>
										<dx:GridViewBandColumn Caption="Cost Center" VisibleIndex="1">
											<Columns>
												<dx:GridViewDataTextColumn Caption="Code" FieldName="COST_CENTER_CD" ReadOnly="True" 
													VisibleIndex="0" Width="80px" CellStyle-HorizontalAlign="Center">
													<CellStyle HorizontalAlign="Center">
													</CellStyle>
												</dx:GridViewDataTextColumn>
												<dx:GridViewDataTextColumn Caption="Name" FieldName="COST_CENTER_NAME" ReadOnly="True" 
													VisibleIndex="1" Width="200px" CellStyle-CssClass="pvform-standardWording">
												</dx:GridViewDataTextColumn>
											</Columns>
										</dx:GridViewBandColumn>
										<dx:GridViewDataTextColumn Caption="Description" FieldName="DESCRIPTION" ReadOnly="True" VisibleIndex="2" Width="430px"  CellStyle-CssClass="pvform-standardWording" SortOrder="Descending" SortIndex="0">
											<DataItemTemplate>
												<asp:Literal runat="server" ID="litStdWording"></asp:Literal> <%# Eval("DESCRIPTION") %>
											</DataItemTemplate>
										</dx:GridViewDataTextColumn>
										<dx:GridViewBandColumn Caption="Amount" VisibleIndex="3">
											<Columns>
												<dx:GridViewDataTextColumn Caption="Curr" FieldName="CURRENCY_CD" ReadOnly="True" 
													VisibleIndex="0" Width="40px" CellStyle-HorizontalAlign="Center">
													<CellStyle HorizontalAlign="Center">
													</CellStyle>
												</dx:GridViewDataTextColumn>
												<dx:GridViewDataTextColumn Caption="Value" FieldName="AMOUNT" ReadOnly="True" VisibleIndex="1" Width="130px" CellStyle-HorizontalAlign="Right">
													<CellStyle HorizontalAlign="Right">
													</CellStyle>
												</dx:GridViewDataTextColumn>
											</Columns>
										</dx:GridViewBandColumn>
									</Columns>
									<SettingsBehavior AllowDragDrop="true" AllowSort="true" 
												ColumnResizeMode="Control" />
									<SettingsEditing Mode="Inline" />
									<Settings 
										UseFixedTableLayout="True" ShowVerticalScrollBar="True" 
											VerticalScrollableHeight="120" />
									<Border BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px" />
									<Styles Header-BackColor="#d2e4eb">
									<Header BackColor="#D2E4EB" HorizontalAlign="Center" VerticalAlign="Middle"></Header>
												
									</Styles>
									<Settings ShowStatusBar="Visible"  />
									<SettingsPager Visible="false" />
									<Templates>
										<StatusBar>
										<div style="text-align:left;">
											Records per page: 
											<select onchange="gridApprovalList.PerformCallback(this.value);" >
												<option value="5" <%# WriteSelectedIndex(5) %> >5</option>
												<option value="10" <%# WriteSelectedIndex(10) %> >10</option>
												<option value="15" <%# WriteSelectedIndex(15) %> >15</option>
												<option value="20" <%# WriteSelectedIndex(20) %> >20</option>
												<option value="25" <%# WriteSelectedIndex(25) %> >25</option>
											</select>&nbsp;														
												<a title="First" href="JavaScript:gridApprovalList.GotoPage(0);">&lt;&lt;</a> &nbsp; 
												<a title="Prev" href="JavaScript:gridApprovalList.PrevPage();">&lt;</a> &nbsp;
													Page <input type="text" onchange="gridApprovalList.GotoPage(parseInt(this.value, 10) - 1)"
													onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridApprovalList.GotoPage(parseInt(this.value, 10) - 1); return false; }"
													value="<%# (gridApprovalList.PageIndex >= 0)? gridApprovalList.PageIndex + 1 : 1 %>"
													style="width:20px" /> of <%# gridApprovalList.PageCount%> &nbsp;
                                                        <%# (gridApprovalList.VisibleRowCount > 1)? "(" + gridApprovalList.VisibleRowCount + " Items)&nbsp;": "" %> 
												<a title="Next" href="JavaScript:gridApprovalList.NextPage();">&gt;</a> &nbsp;
												<a title="Last" href="JavaScript:gridApprovalList.GotoPage(<%# gridApprovalList.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; 
										</div>
										</StatusBar>
									</Templates>
								</dx:ASPxGridView>
								<asp:LinqDataSource ID="LinqDataSource1" runat="server" 
									ContextTypeName="DataLayer.Model.ELVIS_DBEntities" EntityTypeName="" 
									OnSelecting="LinqDataSource1_Selecting"
									Select="new (PV_NO, COST_CENTER_CD, COST_CENTER_NAME, DESCRIPTION, CURRENCY_CD, AMOUNT)" 
									TableName="vw_PV_Approval_Detail">
								</asp:LinqDataSource>
							</td>
						</tr>                            
						<tr>
							<td style="text-align:left" colspan="3">                                            
											
										<div class="row" style="width:100% !important">
											<div style="display:inline;width:auto;float:left" >

												<div style="display:inline;width:auto;" >
													<dx:ASPxButton ID="btnNoticeSave" runat="server" Text="Notice" Height="35px" 
														EnableDefaultAppearance="false" EnableTheming="false" Native="true" 
														Font-Bold="true" Width="100px" 
														Border-BorderStyle="Outset" Border-BorderColor="silver" Border-BorderWidth="3px"
														BackColor="#0000a8" ForeColor="WhiteSmoke" HoverStyle-BackColor="#000033" 
														onclick="btnNoticeSave_Click">
													</dx:ASPxButton>
												</div>
												<div style="display:inline;width:auto;" >                                                            
													<dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" Height="35px" 
														EnableDefaultAppearance="false" EnableTheming="false" Native="true" 
														Font-Bold="true" Width="100px" 
														Border-BorderStyle="Outset" Border-BorderColor="silver" Border-BorderWidth="3px"
                                                        EnableClientSideAPI="true"
														BackColor="#00a800" ForeColor="WhiteSmoke" HoverStyle-BackColor="#003300"
														onclick="btnApprove_Click">
                                                        <ClientSideEvents Click="function(sender, event) { loading() }" />
													</dx:ASPxButton>
												</div>															
												<div style="display:inline;width:auto;" >
													<dx:ASPxButton ID="btnPaid" runat="server" Text="Paid" Height="35px" 
														EnableDefaultAppearance="false" EnableTheming="false" Native="true" 
														Font-Bold="true" Width="100px" 
														Border-BorderStyle="Outset" Border-BorderColor="silver" Border-BorderWidth="3px"
														BackColor="#00a800" ForeColor="WhiteSmoke" HoverStyle-BackColor="#003300" 
														OnClick="btnPaid_Click">
													</dx:ASPxButton>
												</div>
                                                            
                                                <div style="display:inline;width:auto;" >
                                                              
													<dx:ASPxButton ID="btnVerify" runat="server" Text="Verified" Height="35px" 
														EnableDefaultAppearance="false" EnableTheming="false" Native="true" 
														Font-Bold="true" Width="100px" Visible="false" 
														Border-BorderStyle="Outset" Border-BorderColor="silver" Border-BorderWidth="3px"
														BackColor="#00a800" ForeColor="WhiteSmoke" HoverStyle-BackColor="#003300" 
														OnClick="btnVerify_Click">                                                                    
													</dx:ASPxButton>
                                                              
                                                                
												</div>
                                                                                                                     
												<div style="display:inline;width:auto;" >
													<dx:ASPxButton ID="btnCheck" runat="server" Text="Check By Acc" Height="35px" 
														EnableDefaultAppearance="false" EnableTheming="false" Native="true" 
														Font-Bold="true" Width="120px" 
														Border-BorderStyle="Outset" Border-BorderColor="silver" Border-BorderWidth="3px"
														BackColor="#00a800" ForeColor="WhiteSmoke" HoverStyle-BackColor="#003300" 
														onclick="btnChecks_Click">
													</dx:ASPxButton>
												</div>
											</div>
                                                <div class="badgewrap">
                                                <div ID="divVerified" runat="server" class="badge" visible="false">
                                                HARD COPY <br />VERIFIED
                                                </div>
                                                </div> 
											<div style="display:inline;width:auto;float:right;">
												<input type="button" id="btnClose" class="blueButton" value="Close" onclick="ReloadOpener();closeWin()" />
											</div>
											<div style="display:block;width:110px;float:right;" >
												<dx:ASPxButton ID="btnReject" runat="server" Text="Reject" Height="35px" 
													Font-Bold="true" EnableTheming="false" Width="100px" 
													Border-BorderStyle="Outset" Border-BorderColor="silver" Border-BorderWidth="3px"
													Native="true"  EnableDefaultAppearance="false" BackColor="Red" ForeColor="WhiteSmoke"
													HoverStyle-BackColor="#800000"
													onclick="btnReject_Click">
                                                                
												</dx:ASPxButton>
                                                &nbsp;
											</div>
                                            <div style="display:block;width:110px;float:right;margin-right: 220px;">
                                                <dx:ASPxButton ID="btnHold" ClientIDMode="Static" runat="server" CssClass="greenButton" 
                                                    EnableDefaultAppearance="false" EnableTheming="false" Native="true" 
                                                    onclick="btnHold_Click" Text="HOLD"/>
                                                            
                                                <dx:ASPxButton ID="btnEdit" ClientIDMode="Static" runat="server" CssClass="blueButton"
                                                    EnableDefaultAppearance="false" EnableTheming="false" Native="true"
                                                    Visible="false"
                                                    onClick="btnEdit_Click" Text="Edit"/>
                                            </div>
										</div>
												
										
							</td>
						</tr>
					</table>            
				</ContentTemplate>
			</asp:UpdatePanel>
		</asp:Panel>
	</center>
	
	<div style="display:inline;float:left;width:49%;">       
		<asp:UpdatePanel runat="server" ID="upnlNotice" ClientIDMode="Static">
			<ContentTemplate>
				<div style="display:inline; float:left; width:100%; border:1px solid grey; background-color:#f6f6f6; height:175px;">            
					<div style="text-align:center;font-size:14px !important;font-weight:bold; margin: 4px 0px 5px 0px; width:100%;">
						Notice
					</div>                                         
					<div style="padding-left:10px;display:inline">                        
						<table cellpadding="1" cellspacing="0" border="0" width="100%" style="float:left">
							<tr>
								<td style="width:7px;vertical-align:middle;">
									To&nbsp;:&nbsp;&nbsp;
								</td>
								<td>
									<dx:ASPxComboBox ID="txtSendTo" runat="server"
										EnableCallbackMode="true"
										IncrementalFilteringMode="Contains" 
										ValueType="System.String" DropDownButton-Visible="true"
										ValueField="USERNAME" 
										OnLoad="cbNoticeTo_ItemRequestedByValue" 
										width="280px" CallbackPageSize="10" TextFormatString="{0}"
										AutoPostBack="false" MaxLength="61">
										<Columns>
											<dx:ListBoxColumn FieldName="FULLNAME" Caption="Name" />											
											<dx:ListBoxColumn FieldName="USERNAME" Caption="UserName" Visible="true" />
										</Columns>
									</dx:ASPxComboBox>
								</td>
							</tr>
						</table>
					</div>
					<div class="rowright" style="vertical-align:top !important; float:left; width:100%; border-top:1px dashed grey; margin: 5px 0px 5px 1px">
						<asp:TextBox Enabled="true" runat="server" ID="txtNotice" Height="95" style="width:96.5%; margin: 5px 5px 5px 5px;" TextMode="MultiLine">
                        </asp:TextBox>                        
					</div>
					<asp:Literal runat="server" ID="litNoticeMessage"></asp:Literal>
				</div>
				<div style="display:inline; float:left; width:100%; border:1px solid grey; background-color:#f6f6f6;">            
					<div id="noticeComment" runat="server" style="display: inline; float: left; width: 100%;">
						<asp:Repeater runat="server" ID="rptrNotice" 
								onitemdatabound="rptrNotice_ItemDataBound" >
							<ItemTemplate>
								<asp:Literal runat="server" ID="litOpenDivNotice"></asp:Literal>
								<asp:Literal runat="server" ID="litRptrNoticeComment"></asp:Literal>
								<asp:Literal runat="server" ID="litCloseDivNotice"></asp:Literal>
							</ItemTemplate>
						</asp:Repeater>   
					</div>
				</div>
 
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	<div style="display:inline;float:right;width:49%;border:1px solid grey; margin-left:10px;height: 175px;">
		<asp:UpdatePanel runat="server" ID="upnlAttachment">
			<ContentTemplate>
				<asp:Panel runat="server" ID="pnlAttachment" Width="100%">
					<dx:ASPxGridView ID="gridAttachment" runat="server" AutoGenerateColumns="False" ClientInstanceName="gridAttachment"
						Width="100%" KeyFieldName="REF_SEQ_NO" 
						onhtmlrowcreated="gridAttachment_HtmlRowCreated">
                        <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
						<SettingsPager AlwaysShowPager="false"/>
						<SettingsEditing Mode="Inline" />                        
						<Settings 
                            ShowVerticalScrollBar="True" 
							VerticalScrollableHeight="115" />
					    <Styles 
                            AlternatingRow-CssClass="even">
								<Header Font-Bold="True" Font-Size="20px"/>
						</Styles>

						<Columns>
							<dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" Width="30px">
								<DataItemTemplate>
									<asp:Literal runat="server" ID="litGridNo"></asp:Literal>
								</DataItemTemplate>
								<CellStyle HorizontalAlign="Center">
								</CellStyle>
							</dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn FieldName="ATTACH_NAME" ReadOnly="true" Caption="Attachment Category" 
                            Width="175px"
                                VisibleIndex="1" Visible="true">
                                <CellStyle VerticalAlign="Middle" Wrap="False" />
                            </dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn FieldName="ATTACH_CD" ReadOnly="True" Caption="Attachment Category" 
								VisibleIndex="5" Visible="false">
								<CellStyle VerticalAlign="Middle" Wrap="False">
								</CellStyle>
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn FieldName="DESCRIPTION" ReadOnly="True" Caption="Description" 
								VisibleIndex="2" Visible="False">
								<EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle">
								</EditCellStyle>
								<EditItemTemplate>
								    <asp:TextBox runat="server" ID="txtGridDesc" Width="140px" ValidationGroup="valGroup"></asp:TextBox>                        
							    </EditItemTemplate>
								<CellStyle VerticalAlign="Middle" Wrap="False">
								</CellStyle>
							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn FieldName="FILE_NAME" ReadOnly="True" Caption="Filename" 
								VisibleIndex="3" Visible="True">
								<CellStyle VerticalAlign="Middle" Wrap="False">
								</CellStyle>
                                <DataItemTemplate>
                                    <asp:HyperLink runat="server" ID="lnkFile" Target="_blank"/>
                                </DataItemTemplate>

							</dx:GridViewDataTextColumn>
							<dx:GridViewDataTextColumn Visible="false" FieldName="DIRECTORY" VisibleIndex="4">
							</dx:GridViewDataTextColumn>
						</Columns>
						
					</dx:ASPxGridView>
					<asp:LinqDataSource ID="LinqDataSource2" runat="server" 
						ContextTypeName="DataLayer.Model.ELVIS_DBEntities" EntityTypeName="" 
						OnSelecting="LinqDataSource2_Selecting">
					</asp:LinqDataSource>
				</asp:Panel>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>

    
    <div id="ApprovalOverviewDiv" class="hidden">        
        <a id="APPROVALS"></a>    
         <div class="row appovtopline">
            <div class="appovbigtitle lefty">
                Approval Overview
            </div>
            <div class="righty">
                <div class="lefty"><div class="box Approved"></div>Approved</div>
                <div class="lefty"><div class="box Waiting"></div>Waiting</div>
                <div class="lefty"><div class="box Rejected"></div>Rejected</div>
                <div class="lefty"><div class="box Skipped"></div>Skip</div>
                <div class="lefty"><div class="box Concurred"></div>Concurrent</div>
                <div class="lefty"><div class=""></div></div>
            </div>
        </div>
        <div id="UserApprovalDiv" class="appovtopline appovuser appovgdiv">
            <div class=" appovtitle">User</div>
            <table id="gAppUser" border="0" cellspacing="0" cellpadding="2">
            <thead>
            <tr>
                <td class="dxgvHeader CodeCol">
                    Code
                </td>
                <td class="dxgvHeader NameCol">
                    Name
                </td>
                <td class="dxgvHeader StatusCol">
                    Status
                </td>
                <td class="dxgvHeader DateCol">
                    Date
                </td>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        <div class="appovtopline appovfin appovgdiv">
            <div class="appovtitle">Finance</div>
            <table id="gAppFinance" border="0" cellspacing="0" cellpadding="2">
            <thead>
            <tr>
                <td class="dxgvHeader CodeCol">
                    Code
                </td>
                <td class="dxgvHeader NameCol">
                    Name
                </td>
                <td class="dxgvHeader StatusCol">
                    Status
                </td>
                <td class="dxgvHeader DateCol">
                    Date
                </td>
            </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        <div id="OverviewMsg" class="row"></div>
    </div>
</div>
<asp:HiddenField ID="popVendorSuspenseCountTarget" runat="server" />
<ajaxToolkit:ModalPopupExtender ID="popVendorSuspense" runat="server" 
    DropShadow="true" 
    ClientIDMode="Static"
    TargetControlID="popVendorSuspenseCountTarget" 
    PopupControlID="pnPopVendorSuspense"
    CancelControlID="btnVendorSuspenseClose"
    BackgroundCssClass="modalBackground" Enabled="true"/>
<center>
<asp:Panel runat="server" ID="pnPopVendorSuspense" CssClass="speakerPopupList">
    <asp:UpdatePanel runat="server" ID="pnUpdateVendorSuspense">
    <ContentTemplate>
    <div id="divVendorSuspense" style="background-color:White;width:auto;height:270px;">
        <dx:ASPxGridView ID="gridVendorSuspense" runat="server" ClientIDMode="Static" ClientInstanceName="gridVendorSuspense"
        Width="700px" Visible="true" AutoGenerateColumns="false" > 
         <SettingsPager Mode="ShowAllRecords"/>
         <SettingsBehavior AllowSelectByRowClick="true" AllowSelectSingleRowOnly="true"  AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
        <Styles>
            <Header HorizontalAlign="Center" VerticalAlign="Middle"/>
        </Styles>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="VENDOR_NAME" Caption="Vendor" Width="250px"/>
            <dx:GridViewDataTextColumn FieldName="TRANSACTION_NAME" Caption="Transaction" Width="350px" />
            <dx:GridViewDataTextColumn FieldName="PV_NO" Caption="PV No" Width="100px">            
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center" />
            <DataItemTemplate>
               <a href="javascript:void(0)" 
                onclick="javascript:openWin('PVFormList.aspx?mode=view&pv_no=<%# Eval("PV_NO")%>&pv_year=<%# Eval("PV_YEAR")%>','ELVIS_PVFormList')">
                  <asp:Literal runat="server" ID="litGridPVSettNo" Text='<%# Bind("PV_NO")%>' /></a>
            </DataItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>

        </dx:ASPxGridView>
        <div class="rowbtn">
            <div class="btnright">
                <dx:ASPxButton runat="server" ID="btnVendorSuspenseClose" Text="Close" CssClass="display-inline-table">
                    <ClientSideEvents Click="function(s,e) {$find('popVendorSuspense').hide();}" />
                </dx:ASPxButton>               
            </div>
        </div>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
</center>
</asp:Content>
