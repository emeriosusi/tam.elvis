﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Data._30PVList;
using Common.Data.SAPData;
using Common.Function;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Helpers;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;
using ELVISDashBoard._30PvFormList;
using DevExpress.Web.ASPxPopupControl;

namespace ELVISDashBoard._30PvFormList
{
    public partial class PVCompare : VoucherFormPage
    {
        public PVCompare() : base(VoucherFormPage.SCREEN_TYPE_PV)
        {
            _ScreenID = resx.Screen("ELVIS_Screen_3001");
        }

        public override void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["pv_no"] != null)
            {
                hinPVNo.Value = Request.QueryString["pv_no"];
                hidVendorCode.Value = Request.QueryString["vendor_code"];
                lblPVYear.Text = Request.QueryString["pv_year"];
            }
            VoucherMasterPage = Master;

            postbackInit();
            //base.Page_Load(sender, e);

            #region efb 
            if (!IsPostBack && !IsCallback)
            {
                gridCompare.DataSource = null;
            }
            if (!IsPostBack)
            {
                LoadCompare(hinPVNo.Value, hidVendorCode.Value);
            }

            #endregion
        }
        protected override void postbackInit()
        {

            #region Compare EFB
            CompareGrid = gridCompare;

            PVNOLabel = lblPVNo;
            VendorNameLabel = lblVendorName;
            PASLabel = lblpas;
            ISSUELabel = lblIssue;
            TotInvoiceLabel = lblTotInvoice;
            TotPasLabel = lblTotPassed;
            TotIssueLabel = lblTotIssue;

            btSaveRemoveIssue = btnSaveRemoveIssue;

            #endregion
        }

        public readonly string PAGE_SIZE = "pv_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }

        protected void SetMagic(ASPxGridView g, int pageSize)
        {
            if (g.SettingsPager.PageSize > 128)
            {
                ServerModeKeyedCache.DefaultMagicNumberFetchRowsTop = g.SettingsPager.PageSize;
                ServerModeKeyedCache.DefaultMagicNumberFetchKeysAllThreshold = g.SettingsPager.PageSize;
            }
            else
            {
                ServerModeKeyedCache.DefaultMagicNumberFetchRowsTop = pageSize;
                ServerModeKeyedCache.DefaultMagicNumberFetchKeysAllThreshold = pageSize;
            }
        }

        protected void gridCompare_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                if (e.GetValue("TAX_NO") != null)
                {
                    string taxgabungan = e.GetValue("TAX_NO").ToString();
                    string[] row_tax_all = taxgabungan.Split('~');
                    string url_tax_0 = "";
                    string url_tax_1 = "";
                    string tax_0 = "";
                    string tax_1 = "";

                    //if (taxgabungan.Length > 0 && taxgabungan.Contains("http"))
                    if (taxgabungan.Length > 0)
                    {
                        for (int i = 0; i < row_tax_all.Count(); i++)
                        {
                            if (row_tax_all[i].Trim().Length > 0)
                            {
                                if (i == 0)
                                {
                                    string[] row_tax_0 = row_tax_all[i].Split('|');
                                    for (int ii = 0; ii < row_tax_0.Count(); ii++)
                                    {
                                        if (ii == 0)
                                        {
                                            url_tax_0 = row_tax_0[ii].ToString();
                                        }
                                        else
                                        {
                                            tax_0 = row_tax_0[ii].ToString();
                                        }
                                    }
                                }
                                else
                                {
                                    string[] row_tax_1 = row_tax_all[i].Split('|');
                                    for (int ii = 0; ii < row_tax_1.Count(); ii++)
                                    {
                                        if (ii == 0)
                                        {
                                            url_tax_1 = row_tax_1[ii].ToString();
                                        }
                                        else
                                        {
                                            tax_1 = row_tax_1[ii].ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //else if (taxgabungan.Length > 0 && !taxgabungan.Contains("http"))
                    //{
                    //    tax_0 = taxgabungan;
                    //}


                    if (url_tax_0 != "")
                    {
                        HyperLink hyptax1 = gridCompare.FindRowCellTemplateControl(
                            e.VisibleIndex, gridCompare.Columns["TAX_NO"] as GridViewDataColumn
                            , "hyptax1") as HyperLink;
                        if (hyptax1 != null)
                        {
                            hyptax1.NavigateUrl = url_tax_0;
                            hyptax1.Target = "_blank";
                            hyptax1.Text = tax_0;
                        }
                    }
                    else if (tax_0 != "")
                    {
                        Label lbltax0 = gridCompare.FindRowCellTemplateControl(
                           e.VisibleIndex, gridCompare.Columns["TAX_NO"] as GridViewDataColumn
                           , "lbltax0") as Label;
                        if (lbltax0 != null)
                        {
                            lbltax0.Text = tax_0;
                        }
                    }

                    if (url_tax_1 != "")
                    {
                        Label lbltaxSparator = gridCompare.FindRowCellTemplateControl(
                            e.VisibleIndex, gridCompare.Columns["TAX_NO"] as GridViewDataColumn
                            , "lbltaxSparator") as Label;
                        if (lbltaxSparator != null)
                        {
                            lbltaxSparator.Text = ",";
                        }

                        HyperLink hyptax2 = gridCompare.FindRowCellTemplateControl(
                            e.VisibleIndex, gridCompare.Columns["TAX_NO"] as GridViewDataColumn
                            , "hyptax2") as HyperLink;
                        if (hyptax2 != null)
                        {
                            hyptax2.NavigateUrl = url_tax_1;
                            hyptax2.Target = "_blank";
                            hyptax2.Text = tax_1;
                        }
                    }
                    else if (tax_1 != "")
                    {
                        if (tax_0 != "")
                        {
                            Label lbltaxSparator = gridCompare.FindRowCellTemplateControl(
                            e.VisibleIndex, gridCompare.Columns["TAX_NO"] as GridViewDataColumn
                            , "lbltaxSparator") as Label;
                            if (lbltaxSparator != null)
                            {
                                lbltaxSparator.Text = ",";
                            }
                        }

                        Label lbltax1 = gridCompare.FindRowCellTemplateControl(
                           e.VisibleIndex, gridCompare.Columns["TAX_NO"] as GridViewDataColumn
                           , "lbltax1") as Label;
                        if (lbltax1 != null)
                        {
                            lbltax1.Text = tax_1;
                        }
                    }
                }

            }
        }

        protected string WriteSelectedIndexCompare(int pageSize)
        {
            return pageSize == gridCompare.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected void LinqPVCompare_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            try
            {
                //e.Result = logic.PVList.SearchListCompare(FormData.PVNumber.ToString());
                e.Result = logic.PVList.SearchListCompare(hinPVNo.Value);
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
        }
        public void LoadCompare(string PV_NO, string VENDOR_CODE)
        {
            logic.PVList.CompareViewGeneratorByPV(PV_NO);

            gridCompare.DataSourceID = LinqPVCompare.ID;
            gridCompare.DataBind();

            var _list = logic.PVList.SearchListCompare(PV_NO);
            int totInvoice = _list.Count();
            int totPassed = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "MATCHED").Count();
            int totIssue = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "UNMATCHED").Count();
            TotInvoiceLabel.Text = totInvoice.ToString();
            TotPasLabel.Text = totPassed.ToString();
            TotIssueLabel.Text = totIssue.ToString();

            PVNOLabel.Text = PV_NO; //FormData.PVNumber.ToString();
            VendorNameLabel.Text = formPersistence.getVendorCodeName(VENDOR_CODE); //FormData.VendorCode);
            ISSUELabel.ForeColor = System.Drawing.Color.Red;
            PASLabel.ForeColor = System.Drawing.Color.Green;


            if (totPassed == 0 || totIssue == 0)
            {
                btSaveRemoveIssue.Enabled = false;
            }
            else if (totPassed > 0 && totIssue > 0)
            {
                btSaveRemoveIssue.Enabled = true;
            }

            if (totInvoice == 0)
            {
                pnupdateLitMessage.Visible = true;
            }
            else
            {
                pnupdateLitMessage.Visible = false;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (PageSize < 1)
                PageSize = 10;
            if (PageSize > 0)
            {
                gridCompare.SettingsPager.PageSize = PageSize;
            }
        }
        private void setPageSize(int newSize)
        {
            PageSize = newSize;
            if (PageSize > 0)
                gridCompare.SettingsPager.PageSize = newSize;

        }

        protected void grid_CustomCallback(object sender,
            DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            int newSize;
            if (!int.TryParse(e.Parameters.ToString(), out newSize)) return;

            if (newSize < 0)
                newSize = 0;
            grid.SettingsPager.PageSize = newSize;
            PageSize = newSize;
            if (grid != null && e != null && !String.IsNullOrEmpty(e.Parameters) && grid.VisibleRowCount > 0)
            {
                grid.DataBind();
            }
        }

        protected void btRefreshCompare_Click(object sender, EventArgs e)
        {
            //jalanin SP 
            logic.PVList.CompareViewGeneratorByPV(hinPVNo.Value);

            gridCompare.DataSourceID = LinqPVCompare.ID;
            gridCompare.DataBind();

            var _list = logic.PVList.SearchListCompare(hinPVNo.Value);
            int totInvoice = _list.Count();
            int totPassed = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "MATCHED").Count();
            int totIssue = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "UNMATCHED").Count();
            TotInvoiceLabel.Text = totInvoice.ToString();
            TotPasLabel.Text = totPassed.ToString();
            TotIssueLabel.Text = totIssue.ToString();

            if (totPassed == 0 || totIssue == 0)
            {
                btSaveRemoveIssue.Enabled = false;
            }
            else if (totPassed > 0 && totIssue > 0)
            {
                btSaveRemoveIssue.Enabled = true;
            }
        }

        protected void gridCompare_PageIndexChanged(object sender, EventArgs e)
        {
            gridCompare.DataBind();
        }

        protected void btnSaveCompare_Click(object sender, EventArgs e)
        {
            var _list = logic.PVList.SearchListCompare(hinPVNo.Value);
            int totInvoice = _list.Count();
            int totPassed = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "MATCHED").Count();
            int totIssue = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "UNMATCHED").Count();

            UpdateStatusCompare(Convert.ToInt32(hinPVNo.Value), Convert.ToInt32(FormData.PVYear), totPassed, totIssue, 0);

            ////string status_compare = "";
            ////List<string> _listStatus = logic.PVList.searchStatusCompare(hinPVNo.Value.ToString());
            ////if (_listStatus.Count > 0)
            ////{
            ////    status_compare = _listStatus[0].ToString();
            ////}
            ////if (status_compare.ToUpper() == "MATCHED")
            ////{
            ////    SubmitButton.Enabled = true;
            ////}
            ////else
            ////{
            ////    SubmitButton.Enabled = false;
            ////}

            String script = @"
            var openerWindow = window.opener.location.toString();
            if (openerWindow.indexOf('pv_no') <= -1) {
                openerWindow = openerWindow + '?pv_no=' + " + lblPVNo.Text + " + '&pv_year='+" + lblPVYear.Text + @";
                window.opener.location.href = openerWindow;
            } else {
                window.opener.location.href = window.opener.location;
            };
            window.close();";//"window.opener.location.reload();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "refreshOpener", script, true);
        }

        protected void btnSaveRemoveIssue_Click(object sender, EventArgs e)
        {
            var _list = logic.PVList.SearchListCompare(hinPVNo.Value);
            int totInvoice = _list.Count();
            int totPassed = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "MATCHED").Count();
            int totIssue = _list.Where(u => u.VALIDASI_DATA.ToUpper() == "UNMATCHED").Count();

            string invoice_no = "";
            foreach (var item in _list)
            {
                if (item.VALIDASI_DATA.ToUpper() == "UNMATCHED")
                {
                    invoice_no += item.INVOICE_NO + ";";
                }
            }
            //delete data issue
            logic.PVList.RemoveAfterCompare(Convert.ToInt32(hinPVNo.Value), invoice_no);

            UpdateStatusCompare(Convert.ToInt32(hinPVNo.Value), Convert.ToInt32(FormData.PVYear), totPassed, 0, 0);

            ////PrintPVCoverButton.Visible = true;
            ////SubmitButton.Enabled = true;

            //String script = "refreshParent();window.close();";//"window.opener.location.reload();";
            String script = @"
            var openerWindow = window.opener.location.toString();
            if (openerWindow.indexOf('pv_no') <= -1) {
                openerWindow = openerWindow + '?pv_no=' + " + lblPVNo.Text + " + '&pv_year='+" + lblPVYear.Text + @";
                window.opener.location.href = openerWindow;
            } else {
                window.opener.location.href = window.opener.location;
            };
            window.close();";//"window.opener.location.reload();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "refreshOpener", script, true);
        }

        protected void btnCloseCompare_Click(object sender, EventArgs e)
        {
            String script = @"
            var openerWindow = window.opener.location.toString();
            if (openerWindow.indexOf('pv_no') <= -1) {
                openerWindow = openerWindow + '?pv_no=' + " + lblPVNo.Text + " + '&pv_year='+" + lblPVYear.Text + @";
                window.opener.location.href = openerWindow;
            } else {
                window.opener.location.href = window.opener.location;
            };
            window.close();";//"window.opener.location.reload();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "refreshOpener", script, true);
        }
    }
}