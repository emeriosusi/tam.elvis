﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using AjaxControlToolkit;
using BusinessLogic._30PVFormList;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Function;
using DataLayer.Model;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;
using BusinessLogic;

namespace ELVISDashBoard._30PvFormList
{
	public partial class PVApproval : BaseCodeBehind
	{
		#region Init
     
        //FID.pras 30/06/2018
        private FormPersistence _fp = null;
        private FormPersistence fp
        {
            get
            {
                if (_fp == null)
                    _fp = new FormPersistence();
                return _fp;
            }
        }
		
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_3003");
		private ArrayList listMsg = new ArrayList();
		protected bool reloadData = false;
		protected PVApprovalData _data = null;
		protected PVApprovalData data
		{
			get
			{
				if (_data == null || reloadData)
				{
					return reload();
				}
				return _data;
			}
		}

		private StringMap _HoldReasons = null;
		protected StringMap HoldReasons
		{
			get
			{
				if (_HoldReasons == null)
				{
					_HoldReasons = logic.Sys.GetDict("HOLD_REASON");
				}
				return _HoldReasons;
			}
		}
		private readonly string HOLD = "HOLD";
		private readonly string UNHOLD = "UN-HOLD";

		#region Getter Setter for modal button if any
		#endregion

		protected PVApprovalData reload()
		{
			logic.Say("reload()", "PVApproval({0},{1})", txtPVNo.Text, txtPVYear.Text);

			Ticker.In("Reload");
			_data = logic.PVApproval.Search(txtPVNo.Text, txtPVYear.Text);
			if (_data != null)
			{
				divIDR.Visible = true;
				rowBudgetDiv.Visible = !_data.BUDGET_NO.isEmpty();
				//gridAppUser.DataSource = logic.Base.GetApprovalOverview(txtPVNo.Text, txtPVYear.Text, 1, UserData, hidStatusCD.Value.Int());
				//gridAppFinance.DataSource = logic.Base.GetApprovalOverview(txtPVNo.Text, txtPVYear.Text, 2, UserData, hidStatusCD.Value.Int());
				//gridAppUser.DataBind();
				//gridAppFinance.DataBind();                    
			}
			else
			{
				divAmounts.Visible = false;
				divIDR.Visible = false;
			}
			Ticker.Out("Reload");
			return _data;
		}

		#region Getter Setter for Data List
		private List<PVApprovalData> ListInquiry
		{
			set
			{
				ViewState["_listInquiry"] = value;
			}
			get
			{
				if (ViewState["_listInquiry"] == null)
				{
					return new List<PVApprovalData>();
				}
				else
				{
					return (List<PVApprovalData>)ViewState["_listInquiry"];
				}
			}
		}

		private RoleLogic _rolo = null;
		protected RoleLogic rolo
		{
			get
			{
				if (_rolo == null)
				{
					_rolo = new RoleLogic(UserName, _ScreenID);
				}
				return _rolo;
			}
		}

		private bool isCancel
		{
			get
			{
				DropDownList d = Master.FindControl("ddlCategory") as DropDownList;
				return (d != null && d.SelectedValue == "Cancel");
			}
		}
		#endregion

		#endregion

		#region Events

		protected void Page_Load(object sender, EventArgs e)
		{
			Ticker.In("PVApproval Load");
			if (UserData == null)
			{
				Response.Redirect("~/Default.aspx");
			}
			this.Master.ProceedButtonClicked += new EventHandler(btnApprovalProceed_Click);


			lit = litMessage;
			if (!IsPostBack)
			{
				hidScreenID.Value = _ScreenID;

				if (!IsPostBack)
				{
					PrepareLogout();

					#region Init Parameter
					String varPVNo = Request.QueryString["PVNo"];
					String varPVYear = Request.QueryString["PVYear"];
					#endregion

					#region Init Startup Script
					
					divAmounts.Visible = false;
					txtPVNo.Attributes.Add("onkeydown", "if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; search(); return false; } ");
					txtPVYear.Attributes.Add("onkeydown", "if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; search(); return false; } ");
					btnNoticeSave.Attributes.Add("onclick", "loading();");
					btnHold.Attributes.Add("onclick", "loading();");
					btnApprove.Attributes.Add("onclick", "loading();");
					//btnDirectToMe.Attributes.Add("onclick", "loading();");
					noticeComment.Attributes.Add("style", "display: inline; float: left; width: 100%;");
					#endregion

					#region Init button
					setFirstLoad();
					#endregion

					if (!String.IsNullOrEmpty(varPVNo) && !String.IsNullOrEmpty(varPVYear))
					{
						txtNotice.Focus();

						txtPVYear.Text = varPVYear;
						txtPVNo.Text = varPVNo;

						txtPVNo.Enabled = false;
						txtPVYear.Enabled = false;

						#region Search data
						if (validateSearchCriteria())
						{
							loadData();
						}
						#endregion

					}
					else
					{
						if (txtPVNo.Enabled)
						{
							txtPVNo.Focus();
						}

						clearFields();
					}
					initButton();
					if (txtPVNo.Text.isEmpty())
					{
						divShowRemainingBudget.Visible = false;
					}
				}
			}
			Ticker.Out("PVApproval Load");
			foreach (var msg in listMsg)
			{
				litMessage.Text += msg;
			}
		}

		protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
		{
			try
			{
				//ListInquiry =
				e.Result = logic.PVApproval.SearchDetail(txtPVNo.Text, txtPVYear.Text);
				int pNo = 0, pYear = 0;
				string atext = "";
				if (int.TryParse(txtPVNo.Text, out pNo) && int.TryParse(txtPVYear.Text, out pYear))
				{
					decimal amt = logic.PVApproval.GetPVTotalAmount(pNo, pYear);
					atext = String.Format("{0} {1:N0}", new object[] { "IDR", amt });
				}
				litTotalAmount.Text = atext;
				List<string> a = logic.PVApproval.GetTotalAmounts(pNo, pYear);

				if (a.Count > 0)
				{
					divAmounts.Visible = true;
					string b = "";
					foreach (string s in a)
					{
						b = b + string.Format("<div class=\"vcurr\">{0}</div>", s);
					}
					litAmounts.Text = b;
				}
				else
				{
					litAmounts.Text = "";
					divAmounts.Visible = false;
				}

			}
			catch (Exception ex)
			{
				litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
				Handle(ex);
			}
		}


		protected void LinqDataSource2_Selecting(object sender, LinqDataSourceSelectEventArgs e)
		{
			try
			{
				var d = logic.PVApproval.SearchAttachmentInqury(txtPVNo.Text, txtPVYear.Text);
				e.Result = d;
			}
			catch (Exception ex)
			{
				litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
				Handle(ex);
			}
		}

		protected void gridApprovalList_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			try
			{
				Literal litGridNoGeneralInfo = gridApprovalList.FindRowCellTemplateControl(e.VisibleIndex,
					gridApprovalList.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
				if (litGridNoGeneralInfo != null)
				{
					litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
				}

				Literal litGridStdWording = gridApprovalList.FindRowCellTemplateControl(e.VisibleIndex,
					gridApprovalList.Columns["DESCRIPTION"] as GridViewDataColumn, "litStdWording") as Literal;
				if (litGridStdWording != null)
				{
					litGridStdWording.Text = hidStdWording.Value;
				}
			}
			catch (Exception ex)
			{
				litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
				Handle(ex);
			}
		}

		protected void gridAttachment_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		{
			try
			{
				//if ((e.VisibleIndex + 1) % 2 == 0)
				//{
				//    e.Row.Attributes.Add("style", "background-color: #EAF1F6");
				//}

				Literal litGridNoGeneralInfo = gridAttachment.FindRowCellTemplateControl(
					e.VisibleIndex, gridAttachment.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
				if (litGridNoGeneralInfo != null)
				{
					litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
				}

				HyperLink lnkFile = gridAttachment.FindRowCellTemplateControl(
					e.VisibleIndex, gridAttachment.Columns["FILE_NAME"] as GridViewDataColumn, "lnkFile") as HyperLink;
				if (lnkFile != null)
				{
					lnkFile.Text = e.GetValue("FILE_NAME").str();
					lnkFile.NavigateUrl = System.Web.HttpUtility.UrlPathEncode(e.GetValue("URL").str());
				}
			}
			catch (Exception ex)
			{
				litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
				Handle(ex);
			}
		}

		protected void rptrNotice_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			RepeaterItem item = e.Item;
			if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
			{
				Literal litOpenDivNotice = (Literal)item.FindControl("litOpenDivNotice");
				Literal litRptrNoticeComment = (Literal)item.FindControl("litRptrNoticeComment");
				Literal litCloseDivNotice = (Literal)item.FindControl("litCloseDivNotice");
				String dateHeader;

				var d = (NoticeData) item.DataItem;

				if (d.NOTICE_DT.HasValue)
				{
					litOpenDivNotice.Text = "<div class='noticeLeft'>";
					dateHeader = ((DateTime)d.NOTICE_DT).ToString("d MMMM yyyy hh:mm:ss");
				}
				else
				{
					litOpenDivNotice.Text = "<div class='noticeRight'>";
					dateHeader = ((DateTime)d.REPLY_DT).ToString("d MMMM yyyy hh:mm:ss");
				}

				litRptrNoticeComment.Text = String.Format(
					"<span class='noteSender'>{0}</span> {1}<br>To: <span class='noteSender'>{2}</span><br>{3}"
					, d.NOTICE_FROM_NAME
					, dateHeader
					, d.NOTICE_TO_NAME
					, d.NOTICE_COMMENT);

				litCloseDivNotice.Text = "</div>";
			}
		}

		/*Modified by Nia - Filling dropdown list user data in notice*/
		#region DropDown AutoComplete Notice To
		#region cbGridPrevPartNumber_ItemRequestedByValue
		protected void cbNoticeTo_ItemRequestedByValue(object source, EventArgs e)
		{
			ASPxComboBox cbNoticeTo = (ASPxComboBox)source;
			cbNoticeTo.DataSource = logic.Notice.GetNoticeUser(txtPVNo.Text, txtPVYear.Text);
			cbNoticeTo.DataBind();
		}
		#endregion
		#endregion
		/*End modified*/

		#endregion

		#region Button

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			litMessage.Text = "";
			listMsg = new ArrayList();
			Ticker.In("PVApproval Search");
			logic.Say("btnSearch_Click", "{0}{1}", txtPVNo.Text, txtPVYear.Text);
			try
			{
				#region Search data
				if (validateSearchCriteria())
				{
					loadData();
					initButton();
				}
				#endregion
			}
			catch (Exception ex)
			{
				Handle(ex);
				listMsg.Add(MessageLogic.Wrap(ex.Message, "ERR"));
			}
			Ticker.Out();
			foreach (var msg in listMsg)
			{
				litMessage.Text += msg;
			}
		}

		protected void btnReleaseNotice_Click(object sender, EventArgs e)
		{
			ErrorData err = new ErrorData();
			logic.Say("btnReleaseNotice_Click", "releaseNotice({0}, {1}, {2}, {3}, ref err)", txtPVNo.Text, txtPVYear.Text, txtNoticeNo.Text, UserName);
			rptrNotice.DataSource = logic.Notice.releaseNotice(txtPVNo.Text, txtPVYear.Text, txtNoticeNo.Text, UserName, ref err);
			rptrNotice.DataBind();

			if (err.ErrID == 1)
			{
				litMessage.Text = Nagging(err.ErrMsgID, err.ErrMsg);
			}
			else if (err.ErrID == 0)
			{
				Nag("MSTD00023INF");
			}
		}

		protected void btnNoticeSave_Click(object sender, EventArgs e)
		{
			logic.Say("btnNoticeSave_Click", " ");
			litMessage.Text = "";
			try
			{
				if (txtSendTo.Value.str().isEmpty())
				{
					Nag("MSTD00017WRN", "Notice To");
					return;
				}

				if (txtNotice.Text.isEmpty())
				{
					Nag("MSTD00017WRN", "Notice");
					return;
				}

				String noticeMessage = "";
				

				noticeMessage = txtNotice.Text;

				InsertNotice(noticeMessage, "Comment");

			}
			catch (Exception ex)
			{
				litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
				Handle(ex);
			}
			txtNotice.Text = "";
		}

		protected void btnApprove_Click(object sender, EventArgs e)
		{
			Ticker.In("Approve_Click");
			logic.Say("btnApprove_Click", "Approve {0} {1}", txtPVNo.Text, txtPVYear.Text);
			Master.ApprovalType = Common.Enum.ApprovalEnum.Approve;
			btnApprovalProceed_Click(sender, e);
			// initButton();
			Ticker.Out("Approve_Click");

		}

		protected void btnReject_Click(object sender, EventArgs e)
		{
			logic.Say("btnReject_Click", "Reject {0} {1}", txtPVNo.Text, txtPVYear.Text);
			Master.btnReject_Click(sender, e, txtPVNo.Text, txtPVYear.Text);
			initButton();
		}

		protected void btnEdit_Click(object sender, EventArgs e)
		{
			logic.Say("btnEdit_Click", "Edit");
			ScriptManager.RegisterStartupScript(this, this.GetType(),
				"ELVIS_PVFormList",
				"openWin('PVFormList.aspx?mode=edit&pv_no=" + txtPVNo.Text + "&pv_year=" + txtPVYear.Text + "','ELVIS_PVFormList');",
				true);
		}

		protected void btnHold_Click(object sender, EventArgs e)
		{
			logic.Say("btnHold_Click", "Hold {0} {1}", txtPVNo.Text, txtPVYear.Text);
			if (btnHold.Text == UNHOLD)
			{
				Master.ApprovalType = Common.Enum.ApprovalEnum.UnHold;
			}
			else if (btnHold.Text == HOLD)
			{
				Master.ApprovalType = Common.Enum.ApprovalEnum.Hold;
			}
			Master.btnHold_Click(sender, e, txtPVNo.Text, txtPVYear.Text);
		}

		protected void btnPaid_Click(object sender, EventArgs e)
		{
			logic.Say("btnPaid_Click ", "Paid {0} {1}", txtPVNo.Text, txtPVYear.Text);
			litMessage.Text = "";

			string selectedConfirmation = hidDialogConfirmation.Value;
			if (!string.IsNullOrEmpty(selectedConfirmation) && (selectedConfirmation.ToLower().Equals("y")))
			{
				bool Ok = false;
				try
				{
					int pid = log.Log("MSTD000001INF", "Paid", "btnPaid_Click()", (UserData != null) ? UserName : "", "", 0);
					string msg = "";
					Ok = logic.PVApproval.PVPaid(txtPVNo.Text, txtPVYear.Text, pid, ref msg);
					if (!Ok)
					{
						Nag((Ok) ? "MSTD00060INF" : "MSTD00062ERR", "Check Paid");
						return;
					}

					#region Get --> Command
					string _Command = resx.K2ProcID("Form_Registration_Approval");
					#endregion

					#region Do --> Approve-Reject

					Ok = logic.PVRVWorkflow.Go(txtPVNo.Text + txtPVYear.Text, _Command, UserData, "N/A");
					if (Ok)
					{
						logic.PVApproval.SetPaidDate(txtPVNo.Text, txtPVYear.Text, UserName);
						logic.WorkList.Remove(txtPVNo.Text + txtPVYear.Text);
					}
					Nag((Ok) ? "MSTD00060INF" : "MSTD00062ERR", "Paid");
					#endregion
				}
				catch (Exception Ex)
				{
					litMessage.Text += MessageLogic.Wrap(Ex.Message, "ERR");
					Handle(Ex);
				}

				if (Ok)
				{
					string[] ns = new string[3] { "", "", "" };
					if (WaitForK2UpdateStatus(ref ns))
					{
						loadData();
						initButton();
					}
				}
			}
		}

		protected void btnDirectToMe_clicked(object sender, EventArgs e)
		{
			logic.Say("btnDirectToMe_Click", "Direct To Me");
			litMessage.Text = "";
			if (MessageBox.Show("Are you sure you wish to redirect this PV No?", "Confirm Redirect", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				bool success = false;

				try
				{
					string byPassUser = logic.Look.GetLastUser(txtPVNo.Text, txtPVYear.Text, UserName, "Direct");

					#region Do --> Approve-Reject
					string reff = txtPVNo.Text + txtPVYear.Text;
					if (logic.PVRVWorkflow.Redirect(reff, UserData, byPassUser))
					{
						success = true;
						logic.WorkList.Remove(reff);
						Nag("MSTD00060INF", "Redirect");
					}
					else
					{
						Nag("MSTD00062ERR", "Redirect");
					}
					#endregion
				}
				catch (Exception Ex)
				{
					litMessage.Text += MessageLogic.Wrap(Ex.Message, "ERR");
					Handle(Ex);
				}

				if (success)
				{
					int timeOut = 0;
					while (logic.WorkList.isUserHaveWorklist(txtPVNo.Text + txtPVYear.Text))
					{
						timeOut++;
						if (timeOut == 1200)
						{
							Nag("MSTD00082WRN");
							break;
						}
					}

					if (timeOut < 1200)
					{
						loadData();
					}
				}
			}
		}

		protected void linkUnsettled_Click(object sender, EventArgs e)
		{
			logic.Say("linkUnsettled_Click", "Unsettled Suspense");
			reload();
			gridVendorSuspense.DataSource = logic.PVApproval.GetVendorSuspensesByDiv(data.DIVISION_ID.Int());
			gridVendorSuspense.DataBind();
			popVendorSuspense.Show();
		}

		protected void btnChecks_Click(object sender, EventArgs e)
		{
			logic.Say("btnChecks_Click", "Check");
			reload();
			litMessage.Text = "";
			string selectedConfirmation = hidDialogConfirmation.Value;
			if (!string.IsNullOrEmpty(selectedConfirmation) && (selectedConfirmation.ToLower().Equals("y")))
			{
				bool success = false;

				try
				{
					#region Get --> Command
					
					#endregion

					DoStartLog(resx.FunctionId("FCN_PV_CHECK_SAP"), "");

					string _ErrMessage = String.Empty;

					#region Do --> Approve-Reject
					if (logic.PVApproval.PVCheck(txtPVNo.Text, txtPVYear.Text, ProcessID, ref _ErrMessage))
					//if (false)
					{
						success = true;
						litMessage.Text = MessageLogic.Wrap(_ErrMessage, "INF");
						Log("MSTD00069INF", "INF", "btnOkConfirmationCheck_Click", logic.Msg.Message("MSTD00069INF", "PV"));
					}
					else
					{
						litMessage.Text = MessageLogic.Wrap(_ErrMessage, "ERR");
						Log("MSTD00070ERR", "ERR", "btnOkConfirmationCheck_Click", logic.Msg.Message("MSTD00070ERR", "PV"));
					}
					#endregion
				}
				catch (Exception Ex)
				{
					litMessage.Text += MessageLogic.Wrap(Ex.Message, "ERR");
					Handle(Ex);
				}

				if (success)
				{
					string[] ns = new string[3] { "", "", "" };
					if (WaitForK2UpdateStatus(ref ns, 1000))
					{
						logic.PVApproval.UpdateSettlement(txtPVNo.Text, txtPVYear.Text, UserName);

						loadData();
					}
				}
			}
		}

		protected void btnVerify_Click(object sender, EventArgs e)
		{
			logic.Say("btnVerify_Click", "Verify {0} {1}", txtPVNo.Text, txtPVYear.Text);
			string loc = "Verify";
			int no = txtPVNo.Text.Int(0);
			int yy = txtPVYear.Text.Int(0);

			litMessage.Text = "";

			string selectedConfirmation = hidDialogConfirmation.Value;
			if (!string.IsNullOrEmpty(selectedConfirmation) && (selectedConfirmation.ToLower().Equals("y")))
			{
				bool Ok = false;
				try
				{
					int pid = log.Log("MSTD000001INF", "Verified", "btnVerify_Click()", (UserData != null) ? UserName : "", "", 0);
					Ok = logic.PVApproval.Verified(no, yy, UserData);
					Nag((Ok) ? "MSTD00060INF" : "MSTD00062ERR", loc);
					loadData();
					initButton();
				}
				catch (Exception ex)
				{
					litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
					Handle(ex);
				}
			}
		}
		protected void btnApprovalProceed_Click(object sender, EventArgs e)
		{
			litMessage.Text = "";
			bool Ok = false;
			logic.Say("btnApprovalProceed_Click", "Approve {0} {1}", txtPVNo.Text, txtPVYear.Text);
			Ticker.In("ApprovalProceed");
			ModalPopupExtender popUpApproval = Master.FindControl("popUpApproval") as ModalPopupExtender;

			System.Web.UI.WebControls.TextBox txtApprovalComment = Master.FindControl("txtApprovalComment") as System.Web.UI.WebControls.TextBox;
			string _Comment = string.IsNullOrEmpty(txtApprovalComment.Text) ? "" : txtApprovalComment.Text;
			DropDownList d = Master.FindControl("ddlHoldReason") as DropDownList;
			string holdComment = "";
			if (d != null)
			{
				if (d.SelectedIndex > 0)
				{
					holdComment = HoldReasons.Get(d.SelectedValue, d.Text);
				}
			}

			try
			{
				if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve
					|| (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject && !string.IsNullOrEmpty(_Comment)))
				{
					Ticker.In("DoApproveReject");
					Ok = DoApproveReject(_Comment);
					Ticker.Out();
				}
				else if ((!string.IsNullOrEmpty(holdComment) && Master.ApprovalType == Common.Enum.ApprovalEnum.Hold)
					  || (!string.IsNullOrEmpty(_Comment) && Master.ApprovalType == Common.Enum.ApprovalEnum.UnHold))
				{
					reload();
					String userName = UserName;

					ErrorData err = new ErrorData();
					int holdValue = 0;
					if (Master.ApprovalType == Common.Enum.ApprovalEnum.Hold)
					{
						if (!Int32.TryParse(d.SelectedValue, out holdValue))
							holdValue = 1;
					}
					else
					{
						holdValue = 0;
					}

					bool flag = logic.PVApproval.ChangeHoldValue(txtPVNo.Text,
						txtPVYear.Text, userName, ref err,
						PVApprovalLogic.HoldValueOf(Master.ApprovalType), holdValue);

					if (Master.ApprovalType == Common.Enum.ApprovalEnum.UnHold)
						holdComment = _Comment;

					InsertNotice(holdComment, "Hold");

					if (Master.ApprovalType == Common.Enum.ApprovalEnum.Hold)
					{
						Master.ApprovalType = Common.Enum.ApprovalEnum.Approve;
						if (UserData.isCounter)
						{
							loadData();
						}
						else
							Ok = DoApproveReject(_Comment);

					}
					if (Ok)
					{
						if (flag)
						{
							btnHold.CssClass = "greenButton";
							btnHold.Text = HOLD;
							Nag("MSTD00060INF", UNHOLD);
						}
						else
						{
							btnHold.CssClass = "blueButton";
							btnHold.Text = UNHOLD;
							Nag("MSTD00060INF", HOLD);
						}
					}
					lblHold.Visible = !flag;
					ReloadOpener();
				}
				else
				{
					if (Master.ApprovalType == Common.Enum.ApprovalEnum.Hold)
					{
						Nag("MSTD00088ERR", HOLD);
					}
					else if (Master.ApprovalType == Common.Enum.ApprovalEnum.UnHold)
					{
						Nag("MSTD00088ERR", UNHOLD);
					}
					else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
					{
						Nag("MSTD00059ERR");
					}
				}
				// initButton();
			}
			catch (Exception Ex)
			{
				litMessage.Text += MessageLogic.Wrap(Ex.Message, "ERR");
				Handle(Ex);
			}
			txtApprovalComment.Text = "";
			Ticker.Out("ApprovalProceed");
		}

		private bool DoApproveReject(string _Comment)
		{
			bool r = false;
			_Comment = (string.IsNullOrEmpty(_Comment)) ? "N/A" : _Comment;
			#region Get --> Command
			string _Command = "";
			if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
			{
				if (data.STATUS_CD == 30)
				{
					Nag("MSTD00140ERR");
					return false;
				}

				_Command = resx.K2ProcID("Form_Registration_Approval");

			}
			else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
				_Command = resx.K2ProcID("Form_Registration_Rejected");
			#endregion

			#region Do --> Approve-Reject
			int _no = txtPVNo.Text.Int();
			int _yy = txtPVYear.Text.Int();
			bool Canceling = isCancel && Master.ApprovalType == Common.Enum.ApprovalEnum.Reject;
			if (Canceling)
			{
				logic.PVApproval.setCancel(_no, _yy, 1);
			}
			string reffNo = txtPVNo.Text + txtPVYear.Text;
			Ticker.In("ApprovalRejectionPVForm");
			r = logic.PVRVWorkflow.Go(reffNo, _Command, UserData, _Comment, isCancel);
			Ticker.Out();
			if (r)
			{
				if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
				{
					Nag("MSTD00060INF", "Approve");
				}
				else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
				{
					Nag("MSTD00060INF", isCancel ? "Cancel" : "Reject");
				}
				logic.WorkList.Remove(reffNo);
				Ticker.In("ApprovalSuccessPostProcess");
				ApprovalSuccessPostProcess(_Comment);
				Ticker.Out();
				ReloadOpener();
			}
			else
			{
				if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
				{
					Nag("MSTD00062ERR", "Approve");
				}
				else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
				{
					Nag("MSTD00062ERR", isCancel ? "Cancel" : "Reject");
					if (Canceling)
					{
						logic.PVApproval.setCancel(_no, _yy, 0);
					}
				}

			}
			#endregion
			return r;
		}

		private bool WaitForK2UpdateStatus(ref string[] newStatus, int maxLoop = 1200)
		{
			Ticker.In("WaitForK2UpdateStatus");
			newStatus = logic.PVApproval.GetNewStatus(txtPVNo.Text, txtPVYear.Text);
			int i = 0;

			while ((litStatus.Text == newStatus[1]) && (i < maxLoop))
			{
				newStatus = logic.PVApproval.GetNewStatus(txtPVNo.Text, txtPVYear.Text);
				Ticker.Spin("GetNewStatus {0} : {1}", i, newStatus[0]);
				System.Threading.Thread.Sleep(100 + ((i * 20) % 250));
				i++;
			}
			Ticker.Out();
			if (i >= maxLoop)
			{
				Nag("MSTD00082WRN");
				return false;
			}
			else
				return true;

		}

		private void ApprovalSuccessPostProcess(string _Comment)
		{
			string[] newStatus = new string[3] { "", "", "" };
			int no, yy;
			Int32.TryParse(txtPVNo.Text, out no);
			Int32.TryParse(txtPVYear.Text, out yy);

			if (WaitForK2UpdateStatus(ref newStatus))
			{
				///Added by Akhmad Nuryanto, 18/09/2012, send email
				if (newStatus.Length > 0)
				{
					if (newStatus[0].ToString() == "17")
					{
						//sendMailPVCover(txtPVNo.Text, txtPVYear.Text);
					}
					else if (newStatus[0].ToString() == "27" &&
						(newStatus[2].ToString().Equals("C") || newStatus[2].ToString().Equals("E")))
					{
						decimal amt = logic.PVApproval.GetPVTotalAmount(no, yy);
						if (Math.Round(amt) > 0)
							sendMailPVTicket(txtPVNo.Text, txtPVYear.Text);
						else
							logic.PVPrint.ticketPrinted(no, yy, UserName);
					}
				}
				///end of addition by Akhmad Nuryanto

				if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
				{
                    // fid.Hadid 20180712 | update amount on balance for Accrued PV
                    var pvData = logic.PVForm.search(no, yy);
                    if (pvData.TransactionCode == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
                    {
                        var stsUpdBal = logic.Sys.GetArray("DISTRIBUTION_STATUS_PARAM", "PVRV_BALANCE_ON_REJECT");

                        logic.Say("btApprovalProceed_Click", "status update balance : {0} | new status : {1}"
                            , CommonFunction.CommaJoin(stsUpdBal.ToList()), newStatus[0]);

                        if (stsUpdBal.Contains(newStatus[0]))
                            logic.PVForm.updateBalanceOutstanding(pvData, UserData, false);

                    }
                    // end of fid.Hadid

					string k = isCancel ? "Cancel" : "Reject";
					if (isCancel)
					{
						logic.PVApproval.setCancel(no, yy, 1);
					}
					InsertNotice(_Comment, k);
					Nag("MSTD00060INF", k);
				}
				loadData();
				initButton();
			}
		}

		#endregion

		#region Function
		private void loadData()
		{
			try
			{
				Ticker.In("loadData()");
				reload();

				if (data != null)
				{

					#region Data found
					Ticker.In("Validation");
					bool distributed = logic.Look.DistributedToMe(txtPVNo.Text, txtPVYear.Text, UserName);
					bool canApprove = logic.User.IsAuthorizedFinance || distributed || logic.PoA.AttorneyGrantorOOFCount(UserName) > 0;
					bool canShowData = canApprove || UserData.isInDivision(data.DIVISION_ID); 
					Ticker.Out();
					if (canShowData)
					{
						litPVDate.Text = data.PV_DATE.ToString("dd/MM/yyyy");
						litStatus.Text = data.STATUS_NAME;
						litTransactionType.Text = data.TRANSACTION_TYPE;
						litVendorCode.Text = data.VENDOR_CD;
						litPaymentMethod.Text = data.PAYMENT_METHOD;
						litPVType.Text = data.PV_TYPE;
						litVendorName.Text = data.VENDOR_NAME;
						litIssuingDivision.Text = data.ISSUING_DIVISION;
						//litTotalAmounIDR.Text = header.TOTAL_AMOUNT.ToString();
						lblDueDateLabel.Visible = (data.DUE_DATE != null);
                        //if (data.DUE_DATE != null)
                        if (data.PLANNING_PAYMENT_DATE != null)
                        {
                            DateTime d = (DateTime)data.DUE_DATE;

                            litDueDate.Text = d.ToString("dd/MM/yyyy");
                            txtDueDate.Text = d.ToString("MM/dd/yyyy");
                        }
                        else //Rinda Rahayu 20160413
                        {
                            litDueDate.Text = "";
                            txtDueDate.Text = "";
                        }

						//Added by Akhmad Nuryanto, 22/09/2012
						// litRemainingBudget.Text = "IDR " + header.REMAINING_BUDGET;
						//end of addition by Akhmad Nuryanto

						if (divRemainingBudget.Visible)
						{
							DisplayBudget();
						}
						hidStatusCD.Value = data.STATUS_CD.ToString();
						hidIssuingDivisionID.Value = data.DIVISION_ID;
						hidStdWording.Value = data.STD_WORDING;
						Ticker.In("SearchNoticeInqury");
						List<NoticeData> listNotice = logic.Notice.SearchNoticeInquiry(txtPVNo.Text, txtPVYear.Text);
						Ticker.Out();
						Ticker.In("listNotice.DataBind");
						rptrNotice.DataSource = listNotice;
						rptrNotice.DataBind();
						Ticker.Out();

						/**
						 * Wbs Number
						 * */
						// evtlstboxMultipleBudgetNumber_onLoad(popMultipleBudget, EventArgs.Empty);


						litBudgetNo.Visible = true;
						lnkBudgetNo.Visible = false;
                        
                        //fid.pras ganti budget no untuk accrued year end
                        if (data.TRANSACTION_TYPE.Equals("Accrued Year End"))
                        {
                            //string tes = Convert.ToString(UserData.DIV_CD);
                            string budget_no = data.BUDGET_NO;
                            List<WBSStructure> wbs = fp.WbsNumbers(budget_no);
                            var desc = wbs[0].Description;
                            var wbsno = wbs[0].WbsNumber;
                            data.BUDGET_NO = wbsno + " / " + desc + " / " + budget_no;
                        }

						litBudgetNo.Text = data.BUDGET_NO;
						if (string.IsNullOrEmpty(data.BUDGET_NO))
						{
							litBudgetName.Text = "";
							divShowRemainingBudget.Visible = false;
						}
						else
						{
							litBudgetName.Text = logic.PVApproval.GetBudgetName(data.BUDGET_NO);
						}
						rowBudgetDiv.Visible = !data.BUDGET_NO.isEmpty();

						if (listNotice.Count > 0)
						{
							noticeComment.Attributes.Remove("style");
							noticeComment.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");
						}
						Ticker.In("Bind Approval List, Attaachment");
						gridApprovalList.DataSourceID = LinqDataSource1.ID;
						gridApprovalList.DataBind();

						gridAttachment.DataSourceID = LinqDataSource2.ID;
						gridAttachment.DataBind();
						Ticker.Out();
						Ticker.In("SuspenseCountByDiv");
						data.VENDOR_SUSPENSE_COUNT = logic.PVApproval.GetUnsettledSuspenseByDiv(data.DIVISION_ID.Int()); 
							// logic.PVApproval.SuspenseCountByDiv(data.DIVISION_ID.Int());
						if ((data.VENDOR_SUSPENSE_COUNT ?? 0) > 0 && data.PV_TYPE_CD == 2)
						{
							//ltUnsettled.Text = Convert.ToString(header.VENDOR_SUSPENSE_COUNT.Value) + " PV UNSETTLED";
							//ltUnsettled.Visible = true;

							linkUnsettled.Text = Convert.ToString(data.VENDOR_SUSPENSE_COUNT.Value) + " PV UNSETTLED";
							linkUnsettled.Visible = true;

						}
						else
						{
							//ltUnsettled.Visible = false;
							linkUnsettled.Visible = false;
						}
						Ticker.Out();
						divSuspense.Visible = data.PV_TYPE_CD == 3;
						if (divSuspense.Visible)
						{
							Ticker.In("GetSuspenseAmount");
							litTotalSuspense.Text = String.Format("{0} {1:N0}"
								, new object[] 
								{ 
									"IDR"
									, logic.PVApproval.GetSuspenseAmount(txtPVNo.Text, txtPVYear.Text) 
								});
							Ticker.Out();
						}

						if (linkUnsettled.Visible || divSuspense.Visible)
						{
							divRow1.Style.Remove("display");
						}
						else
						{
							divRow1.Style.Add("display", "hidden");
						}


					}

					if (canApprove)
					{

						btnApprove.Enabled = true;
						btnReject.Enabled = true;
						btnNoticeSave.Enabled = true;

						/*@Lufty. 
						string holdBy = header.HOLD_BY;
						
						if (holdBy == null)   
						{
							btnHoldDummy.BackColor = System.Drawing.Color.LightSeaGreen;
						}
						else
						{
							btnHoldDummy.BackColor = System.Drawing.Color.Red;
						}

						btnHold.Visible = true;
						/* ----------------------------- */

						/*@Nia */
						Int32 holdFlag = data.HOLD_FLAG;

						if (holdFlag <= 0)
						{
							btnHold.CssClass = "greenButton";
							btnHold.Text = HOLD;

							//btnHoldDummy.BackColor = System.Drawing.Color.FromArgb(0, 168, 0);
							//btnHoldDummy.HoverStyle.BackColor = System.Drawing.Color.FromArgb(0, 51, 0);
						}
						else
						{
							btnHold.CssClass = "blueButton";
							btnHold.Text = UNHOLD;

							//btnHoldDummy.BackColor = System.Drawing.Color.FromArgb(255, 0, 0);
							//btnHoldDummy.HoverStyle.BackColor = System.Drawing.Color.FromArgb(128, 0, 0);
						}
						//btnHold.Visible = true;
						/*------------------------------*/
						lblHold.Visible = (holdFlag > 0);
						// initButton();
					}
					else if (!canShowData)
					{
						clearFields();
						Nag("MSTD00045ERR");
					}

					#endregion
				}
				else
				{
					#region No data found
					clearFields();
					Nag("MSTD00018INF");
					#endregion
				}
			}
			catch (Exception ex)
			{
				litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
				Handle(ex);
			}
			finally
			{
				Ticker.Out();
			}
		}

		private bool SendEmail(string sentTo, string _Comment, string op, ref ErrorData _Err)
		{
			int cntSentMail = 0;

			if (String.IsNullOrEmpty(sentTo)) return false;

			string email = "";
			string userRoleTo = "";


			if (logic.PVApproval.GetUserInfo(sentTo, ref email, ref userRoleTo))
			{
				String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE","DIR")));
				string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
				if (!Common.AppSetting.NoticeMailUsePort) port = "";
				String noticeLink = String.Format("{0}://{1}{2}{3}?pvno={4}&pvyear={5}",
					Request.Url.Scheme,
					Request.ServerVariables["SERVER_NAME"],
					port,
					Request.ServerVariables["URL"],
					txtPVNo.Text,
					txtPVYear.Text);

				string noticeLinkForm =
					ResolveClientUrl(string.Format("{0}://{1}{2}{3}?pv_no={4}&pv_year={5}",
					Request.Url.Scheme,
					Request.ServerVariables["SERVER_NAME"],
					port,
					"/30PvFormList/PVFormList.aspx",
					txtPVNo.Text, txtPVYear.Text));

				EmailFunction mail = new EmailFunction(emailTemplate);

				string n = ("ELVIS_USER_ADMIN ELVIS_FD_STAFF ELVIS_PV_MAKER_ONLY".IndexOf(userRoleTo, StringComparison.OrdinalIgnoreCase) >= 0) ?
				noticeLinkForm : noticeLink;

				logic.Say("SendEmail", "SendEmail to : {0} <{1}> [{2}|link:{3}", email, sentTo, userRoleTo, n);

				if (mail.ComposeApprovalNotice_EMAIL(
					txtPVNo.Text, txtPVYear.Text, _Comment,
					n, email, UserData, UserName,
					hidIssuingDivisionID.Value,
					"PV", op, ref _Err))
				{
					cntSentMail++;
				}
			}

			if (cntSentMail > 0)
			{
				Nag("MSTD00024INF");
			}
			return true;
		}

		private void InsertNotice(string _Comment, string mode)
		{
			ErrorData _Err = new ErrorData();

			List<RoleData> listRole = rolo.getUserRole(UserName, ref _Err);
			List<RoleData> listRoleTo = null;
			String userRole = "";
			String userRoleTo = "";
			String noticeTo = "";
			string sendTo = "";
			String appType = "";
			if (listRole.Any())
			{
				userRole = listRole.FirstOrDefault().ROLE_NAME;
			}
			bool hasWorklist;

			hasWorklist = logic.WorkList.isUserHaveWorklist(txtPVNo.Text + txtPVYear.Text);
			if (mode == "Comment")
			{
				if (txtSendTo.Value != null)
				{
					sendTo = Convert.ToString(txtSendTo.Value);
					{
						noticeTo = txtSendTo.Value.ToString();
						listRoleTo = rolo.getUserRole(noticeTo, ref _Err);
						if (listRoleTo.Any())
						{
							userRoleTo = listRoleTo.FirstOrDefault().ROLE_NAME;
						}
					}
				}
				appType = "Notice";
			}
			else
			{
				noticeTo = logic.Look.GetLastUser(txtPVNo.Text, txtPVYear.Text, UserName, mode);
				sendTo = noticeTo;


				string[] x = new string[4] { "Approve", "Reject", "Hold", "UnHold" };
				appType = x[(int)Master.ApprovalType];
				if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
					appType = mode;

				if (Master.ApprovalType == Common.Enum.ApprovalEnum.Hold)
				{
					sendTo = logic.PVApproval.GetFirstUser(txtPVNo.Text, txtPVYear.Text);
				}
				if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
					hasWorklist = true; // assume user has worklist 

				_Comment = String.Format("{0} Reason:<br>{1}", appType, _Comment);
			}

			List<string> lstUser = null;
			if (string.IsNullOrEmpty(noticeTo))
			{
				noticeTo = UserName;
				//Commented by wot.neir since it doesn't accomodate rejected PV
				//PVApprovalData header = logic.PVApproval.SearchHeaderInqury(txtPVNo.Text, txtPVYear.Text);                
			}
			lstUser = logic.Notice.listNoticeRecipient(txtPVNo.Text, txtPVYear.Text, UserName, noticeTo);

			if (lstUser == null)
			{
				lstUser = new List<string>();
			}

			NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

			logic.Notice.InsertNotice(
				txtPVNo.Text,
				txtPVYear.Text,
				UserName,
				userRole,
				noticeTo,
				userRoleTo,
				lstUser,
				_Comment,
				mode != "Hold",
				hasWorklist);

			List<NoticeData> noticeList = logic.Notice.SearchNoticeInquiry(txtPVNo.Text, txtPVYear.Text);

			rptrNotice.DataSource = noticeList;
			rptrNotice.DataBind();

			SendEmail(sendTo, _Comment, appType, ref _Err);
			if (_Err.ErrID == 1)
			{
				litMessage.Text += Nagging(_Err.ErrMsgID, _Err.ErrMsg);
			}
			else if (_Err.ErrID == 2)
			{
				Nag("MSTD00002ERR", _Err.ErrMsg);
			}

			noticeComment.Attributes.Remove("style");
			noticeComment.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");
		}

		private bool validateSearchCriteria()
		{
			Int32 pvNo;
			bool pvNoOk = false;
			Int16 pvYear;

			#region PV No. or PV Year is not filled / invalid value
			if (String.IsNullOrEmpty(txtPVNo.Text))
			{
				listMsg.Add(Nagging("MSTD00017WRN", "PV No."));
			}
			else if (!Int32.TryParse(txtPVNo.Text, out pvNo))
			{
				listMsg.Add(Nagging("MSTD00018WRN", "PV No."));
			}
			else
			{
				pvNoOk = true;
			}

			if (String.IsNullOrEmpty(txtPVYear.Text))
			{
				pvYear = (short)DateTime.Now.Year;
				txtPVYear.Text = pvYear.str();
				// listMsg.Add(Nagging("MSTD00017WRN", "PV Year"));
			}
			else if (!Int16.TryParse(txtPVYear.Text, out pvYear))
			{
				listMsg.Add(Nagging("MSTD00018WRN", "PV Year"));
			}
			else if (txtPVYear.Text.Length != 4)
			{
				listMsg.Add(Nagging("MSTD00019WRN", "PV Year", "4"));
			}
			#endregion

			return listMsg.Count == 0;
		}

		private void clearFields()
		{
			rptrNotice.DataSource = null;
			rptrNotice.DataBind();

			gridApprovalList.DataSourceID = null;
			gridApprovalList.DataBind();

			gridAttachment.DataSourceID = null;
			gridAttachment.DataBind();

			litPVDate.Text = "";
			litStatus.Text = "";
			litTransactionType.Text = "";
			litVendorCode.Text = "";
			litPaymentMethod.Text = "";
			litPVType.Text = "";
			litVendorName.Text = "";
			litIssuingDivision.Text = "";
			litDueDate.Text = "";
			txtDueDate.Text = "";

			litTotalAmount.Text = "";
			litTotalSuspense.Text = "";
			divSuspense.Visible = false;

			btnApprove.Enabled = false;
			btnReject.Enabled = false;
			btnNoticeSave.Enabled = false;
			btnPaid.Enabled = false;
			btnCheck.Enabled = false;

			noticeComment.Attributes.Remove("style");
			noticeComment.Attributes.Add("style", "display: inline; float: left; width: 100%;");
		}

		private void initButton()
		{
			Ticker.In("initButton");
			reload();
			//RoleLogic rolo = new RoleLogic(UserName, _ScreenID);
			//bool status = logic.PVApproval.approvalStatus(txtPVNo.Text, txtPVYear.Text, UserName);
			int statusCd = hidStatusCD.Value.Int(-1);

			Ticker.In("isUserHaveWorklist");
			bool status = logic.WorkList.isUserHaveWorklist(txtPVNo.Text + txtPVYear.Text);
			Ticker.Out();

			btnApprove.Visible = status && rolo.isAllowedAccess("btnApprove");
			btnReject.Visible = status && rolo.isAllowedAccess("btnReject");

			btnPaid.Visible = (hidStatusCD.Value == "27") && rolo.isAllowedAccess("btnPaid");
			if (btnPaid.Visible)
			{
				btnPaid.Enabled = hidStatusCD.Value == "27" && (data == null || data.TicketPrinted > 0);
			}

			btnVerify.Visible = (data != null)
					&& ((data.COUNTER_FLAG ?? 0) == 0 || data.HOLD_FLAG == 1) && (data.PV_NO != 0)
					&& rolo.isAllowedAccess("btnVerify");

			divVerified.Visible = !btnVerify.Visible && (data != null) && (data.COUNTER_FLAG ?? 0) == 1;

			btnCheck.Visible = (statusCd == 28) && rolo.isAllowedAccess("btnCheck");
			if (btnCheck.Visible)
			{
				btnCheck.Enabled = statusCd == 28;
			}
			Ticker.In("isCanHold");
			bool canHold = logic.PVApproval.isCanHold(txtPVNo.Text, txtPVYear.Text, UserName, hidStatusCD.Value, btnHold.Text);
			bool isHold; /// = btnHold.Text.Trim().ToUpper().Equals(HOLD);
			Ticker.Out();

			bool isFinanceApproval = (statusCd <= 27)
				|| (statusCd >= 43 && statusCd <= 46);

			isHold = data != null && (data.HOLD_FLAG) > 0;

			btnHold.Visible = data != null
				// -- && ((isHold && 
						&& (!isHold || (isHold && canHold))
						&& (status || UserData.isCounter)
				//&& canHold
						&& (isFinanceApproval || UserData.isCounter)
						&& (!UserData.isCounter || (UserData.isCounter && (data.COUNTER_FLAG ?? 0) == 0))
						&& rolo.isAllowedAccess("btnHold");

			string pvNo, pvYear;
			pvNo = txtPVNo.Text;
			pvYear = txtPVYear.Text;
			Ticker.In("GetPVHeader");
			PVFormHeaderData p = logic.PVList.GetPVHeader(pvNo.Int(0), pvYear.Int(0));
			Ticker.Out();
			bool canEdit = (p != null)
					&& PVFormLogic.CanEdit(
						rolo.isAllowedAccess("btnEdit"),
						status, p.STATUS_CD,
						UserData.Roles.FirstOrDefault(),
						p.HOLD, p.VENDOR_GROUP_CD, p.PV_TYPE_CD,
						logic.User.IsAuthorizedCreatorRole
					)
					&& !isFinanceApproval;

			btnEdit.Visible = canEdit;
			Ticker.Out("initButton");
		}

		private void setFirstLoad()
		{
			//RoleLogic rolo = new RoleLogic(UserName, _ScreenID);
			hidUserName.Value = UserName;
			btnPaid.Visible = rolo.isAllowedAccess("btnPaid");
			if (btnPaid.Visible)
			{
				btnPaid.Attributes.Add("onclick", "paidConfirmation();");
			}

			btnCheck.Visible = rolo.isAllowedAccess("btnCheck");
			if (btnCheck.Visible)
			{
				btnCheck.Attributes.Add("onclick", "checkConfirmation();");
			}

			btnApprove.Visible = rolo.isAllowedAccess("btnApprove");
			btnReject.Visible = rolo.isAllowedAccess("btnReject");
			btnHold.Visible = rolo.isAllowedAccess("btnHold")
				&& logic.PVApproval.isCanHold(txtPVNo.Text, txtPVYear.Text, UserName, hidStatusCD.Value);

			btnNoticeSave.Visible = true;

			btnSearch.Visible = rolo.isAllowedAccess("btnSearch");
			txtPVNo.Enabled = rolo.isAllowedAccess("btnSearch");
			txtPVYear.Enabled = rolo.isAllowedAccess("btnSearch");

			DropDownList d = Master.FindControl("ddlHoldReason") as DropDownList;
			if (d != null)
			{
				GenerateComboData(d, "HOLD_REASON", false);
			}

			btnVerify.Visible = rolo.isAllowedAccess("btnVerify");
			if (btnVerify.Visible)
			{
				btnVerify.Attributes.Add("onclick", "verifyConfirmation();");
			}
		}
		#endregion

		#region paging
		//const string GridCustomPageSizeName = "gridCustomPageSize";
		const string GridPageSize = "PVRV_GRIDSIZE";
		protected void Page_Init(object sender, EventArgs e)
		{
			if (Session[GridPageSize] != null)
			{
				gridApprovalList.SettingsPager.PageSize = (int)Session[GridPageSize];
			}
			else
			{
				gridApprovalList.SettingsPager.PageSize = 5;
				Session[GridPageSize] = 5;
			}
		}
		protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			int newPageSize;
			if (!int.TryParse(e.Parameters, out newPageSize)) return;
			gridApprovalList.SettingsPager.PageSize = newPageSize;
			Session[GridPageSize] = newPageSize;
			if (gridApprovalList.VisibleRowCount > 0)
				gridApprovalList.DataSourceID = LinqDataSource1.ID;
			gridApprovalList.DataBind();
		}
		protected string WriteSelectedIndex(int pageSize)
		{
			return pageSize == gridApprovalList.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
		}
		protected string GetShowingOnPage()
		{
			int pageSize = gridApprovalList.SettingsPager.PageSize;
			if (gridApprovalList.PageIndex < 0)
				gridApprovalList.PageIndex = 0;
			int startIndex = gridApprovalList.PageIndex * pageSize + 1;
			int endIndex = (gridApprovalList.PageIndex + 1) * pageSize;
			if (endIndex > gridApprovalList.VisibleRowCount)
			{
				endIndex = gridApprovalList.VisibleRowCount;
			}
			return string.Format("Showing {0}-{1} of {2}"
				, startIndex
				, endIndex
				, gridApprovalList.VisibleRowCount);
		}
		protected void gridApprovalList_PageIndexChanged(object sender, EventArgs e)
		{
			gridApprovalList.DataBind();
		}

		#endregion

		protected void DisplayBudget(bool shown = true)
		{
			divRemainingBudget.Visible = shown;
			divShowRemainingBudget.Visible = !shown;
			if (shown)
			{
				if (!string.IsNullOrEmpty(litBudgetNo.Text))
				{
					string err = "";
					litRemainingBudget.Text = "IDR " +
						logic.Look.RemainingBudget(
							txtPVNo.Text,
							logic.Sys.FiscalYear(data.PV_DATE).str(),
							litBudgetNo.Text, ref err);
					if (!string.IsNullOrEmpty(err))
					{
						Nag("MSTD00002ERR", err);
					}
					lblRemainingBudget.Visible = true;
				}
				else
				{
					lblRemainingBudget.Visible = false;
					litRemainingBudget.Text = "";
				}
			}
		}

		protected void lnkHideRemainingBudget_Click(object sender, EventArgs e)
		{
			DisplayBudget(false);
		}
		protected void lnkRemainingBudget_Click(object sender, EventArgs e)
		{
			logic.Say("lnkRemainingBudget_Click", "Show Remaining Budget");
			DisplayBudget();
		}

		//protected void gridAppUser_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		//{

		//}

		//protected void gridAppUser_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
		//{
		//    ListHelper.Colorific(e);
		//}

		//protected void gridAppFinance_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
		//{

		//}

		//protected void gridAppFinance_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
		//{
		//    ListHelper.Colorific(e);
		//}

	}
}