﻿using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Data._30PVList;
using Common.Data.SAPData;
using Common.Function;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI;

namespace ELVISDashBoard._30PvFormList
{
    public partial class PVFormList : VoucherFormPage
    {

        public PVFormList() : base(VoucherFormPage.SCREEN_TYPE_PV)
        {
            _ScreenID = resx.Screen("ELVIS_Screen_3001");
        }

        public override void Page_Load(object sender, EventArgs e)
        {
            VoucherMasterPage = Master;
            GenerateComboData(OneTimeTitle, "TITLE", false);
            postbackInit();
            base.Page_Load(sender, e);
        }
        protected override void postbackInit()
        {            
            DetailGrid = gridPVDetail;
            AttachmentGrid = gridAttachment;
            BudgetNumberListBox = lstboxMultipleBudgetNumber;
            TotalAmountLiteral = ltTotalAmount;
            MessageControlLiteral = messageControl;
            PaymentMethodLiteral = lblPaymentMethod;
            NoteTextBox = txtNotice;
            DocumentDateLabel = tboxDocDate;
            DocumentNumberLabel = tboxDocNo;
            IssuingDivisionLabel = tboxIssueDiv;
            StatusLabel = tboxStatus;
            VendorCodeLabel = lblVendorCode;
            VendorDescriptionLabel = lblVendorDesc;
            VendorGroupLabel = lblVendorGroup;
            PostingDateLabel = lblPostingDate;
            PlanningPaymentDateLabel = lblPlanningPaymentDate;
            BankTypeLabel = lblBankType;
            BankTypeLiteral = bankTypeLiteral;
            DocumentTypeLabel = lblDocType;
            TransactionTypeLabel = lblTransactionType;
            ActivityDateStartLabel = lblActivityDate;
            ActivityDateEndLabel = lblActivityDateTo;
            BudgetNumberLabel = lblBudgetNo;
            DocumentTypeDropDown = ddlDocType;
            PaymentMethodDropDown = ddlPaymentMethod;
            TransactionTypeLookup = ddlTransactionType;
            NoteRecipientDropDown = ddlSendTo;
            AttachmentCategoryDropDown = cboxAttachmentCategory;
            CalculateTaxCheckBox = chkCalculateTax;
            ActivityDateStart = dtActivityDateFrom;
            ActivityDateEnd = dtActivityDateTo;
            PostingDate = dtPostingDate;
            PlanningPaymentDate = dtPlanningPaymentDate;
            VendorCodeLookup = lkpgridVendorCode;
            BudgetNumberLookup = lkpgridBudgetNo;
            BankTypeLookup = lkpgridBankType;
            EditButton = btEdit;
            SaveButton = btSave;
            CancelButton = btCancel;
            CloseButton = btClose;
            SubmitButton = btSubmit;
            RejectButton = btReject;
            HoldButton = btHold;
            UnHoldButton = btUnHold;
            PostSAPButton = btPostToSAP;
            ReverseButton = btReverse;
            SummaryButton = btSummary;
            SimulateUserButton = btSimulateUser; // Rinda Rahayu 20160425
            RemainingBudgetLinkButton = lnkRemainingBudget; // Rinda Rahayu 20160425
            DivShowRemainingBudget = divShowRemainingBudget; // Rinda Rahayu 20160425
            DivRemainingBudget = divRemainingBudget; //Rinda Rahayu 20160425
            DataUploadButton = btnUploadTemplate;
            UploadDiv = UploadDetailDiv;
            
            DownloadUploadTemplateLink = lnkDownloadTemplate;
            DownloadEntertainmentTemplateLink = EntertainmentAttachDiv; // lnkDownloadEntertainmentTemplate;
            BudgetNumberLink = lnkBudgetNo;
            DataUpload = fuInvoiceList;
            AttachmentUpload = uploadAttachment;
            HeaderTab = PVFormListTabs;
            HiddenScreenID = hidScreenID;
            HiddenMessageExpandFlag = hdExpandFlag;
            HiddenReloadOpener = hidReloadOpener;
            HiddenPaymentMethod = hidPaymentMethod;
            HiddenGridFocusedColumn = hdGridFocusedColumn;
            HiddenFinanceAccessFlag = hdFinanceAccessFlag;
            HiddenOneTimeVendorValid = hidOneTimeVendorValid;
            NoteRepeater = rptrNotice;
            AttachmentPopup = popdlgUploadAttachment;
            NoteCommentContainer = noticeComment;
            
            
            ActivitDateToSeparatorLabel = lblTo;
            VendorAdditionPopup = popAddVendor; 
            VendorAdditionButton = btAddNewVendor;
            NewVendorName = tboxNewVendorName;
            NewVendorSearchTerm = tboxNewVendorSearchTerm;

            SimulateGrid = gridSimulation;
            SimulatePanel = panelSimulation;
            SimulatePopUp = popUpSimulation;

            SAPGrid = gridpopSAPDocNumber;
            SAPpop = popSAPDocNumber;

            #region Compare EFB
            CompareButton = btCompare;
            PrintPVCoverButton = btDownloadPVCover;
            labelFlagCompare = lblFlagCompare;
            labelFlagEdit = lblFlagEdit;
            #endregion
        }

        protected override string PrintCover()
        {
            PVFormData f = FormData;
            OrderedDictionary ta = f.FormTable.TotalAmountMap();
            decimal tAmt = f.GetTotalAmount();
            string amt = tAmt.fmt(0);
            PVListData d = new PVListData()
            {
                PV_NO = f.PVNumber.str(),
                PV_YEAR = f.PVYear.str()
            };
            
            List<PVListData> listData = new List<PVListData>();
            ErrorData _Err = new ErrorData();
            (lilo as BusinessLogic._30PVList.PVListLogic).Fill(d);
            listData.Add(d);

            return  logic.PVPrint.printCover(Page,
                    System.IO.Path.GetDirectoryName(Server.MapPath(Common.AppSetting.CompanyLogo)),
                    Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE","DIR")),
                    UserData,
                    listData,
                    ref _Err);
       }

        protected override bool isSettlement(int Code)
        {
            return (Code == 3);
        }

        protected override bool HoldVisibleStatus(int? statusCD)
        {
            //return (statusCD ?? 0) < 27;
            return !(new int[] { 27, 28, 29 }.Contains(statusCD ?? 0));
        }

        protected override OneTimeVendorData OneTimeVendor()
        {
            return new OneTimeVendorData()
            {
                TITLE = OneTimeTitle.Value.str(),
                NAME1 = OneTimeName1.Text,
                NAME2 = OneTimeName2.Text,
                STREET = OneTimeStreet.Text,
                CITY = OneTimeCity.Text,
                BANK_KEY = OneTimeBankKey.Text,
                BANK_ACCOUNT = OneTimeBankAccount.Text
            };
        }

        protected override void ClearOneTimeVendor()
        {
            OneTimeTitle.SelectedIndex = 0;
            OneTimeName1.Text = "";
            OneTimeName2.Text = "";
            OneTimeStreet.Text = "";
            OneTimeCity.Text = "";
            OneTimeBankKey.Value = null;
            OneTimeBankAccount.Text = "";
        }

        protected override void SetOneTimeVendor(OneTimeVendorData o) {
            OneTimeTitle.Value = o.TITLE;
            OneTimeName1.Text = o.NAME1;
            OneTimeName2.Text = o.NAME2;
            OneTimeStreet.Text = o.STREET;
            OneTimeCity.Text = o.CITY;
            OneTimeBankKey.Value = o.BANK_KEY;
            OneTimeBankAccount.Text = o.BANK_ACCOUNT;
        }

        // Start Rinda Rahayu 20160426
        protected void lnkRemainingBudget_Click(object sender, EventArgs e)
        {
            if (BudgetNumberLabel.Text !=null && !BudgetNumberLabel.Text.Equals(""))
            {
               logic.Say("lnkRemainingBudget_Click", "Show Remaining Budget");
               DisplayBudget();
            }
        }

        protected void lnkHideRemainingBudget_Click(object sender, EventArgs e)
        {
            DisplayBudget(false);
        }

        protected void DisplayBudget(bool shown = true)
        {
            divRemainingBudget.Visible = shown;
            divShowRemainingBudget.Visible = !shown;
            if (shown)
            {
                if (!string.IsNullOrEmpty(BudgetNumberLabel.Text))
                {
                    string err = "";
                    litRemainingBudget.Text = "IDR " +
                        logic.Look.RemainingBudget(
                            //txtPVNo.Text,
                            "",
                            logic.Sys.FiscalYear(new DateTime()).str(),
                            BudgetNumberLabel.Text, ref err);
                    if (!string.IsNullOrEmpty(err))
                    {
                        Nag("MSTD00002ERR", err);
                    }
                    lblRemainingBudget.Visible = true;
                }
                else
                {
                    lblRemainingBudget.Visible = false;
                    litRemainingBudget.Text = "";
                }
            }
        }
        // End Rinda Rahayu 20160426      
        protected void btDownloadPVCover_Click(object sender, EventArgs e)
        {
            string PathPVCover = PrintCover();
            if (PathPVCover != string.Empty)
            {
                Response.Redirect(new Uri(PathPVCover).ToString());
            }
        }
        protected void btCompare_Click(object sender, EventArgs e)
        {
            labelFlagCompare.Text = "compare";
            string statusSave = SaveEditDraft();
            if (statusSave == "sukses")
            {
                // go to compare form
                lblFlagCompare.Text = "";
                string url = "PVCompare.aspx?pv_no=" + FormData.PVNumber.ToString() + "&vendor_code=" + VendorCodeLabel.Text.ToString()+ "&pv_year=" + FormData.PVYear.ToString();
                //string s = "window.open('" + url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";
                //Response.Redirect(url);
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ELVIS_PVCompare",
                "openWin('" + url + "','ELVIS_PVCompare');",
                true);
            }
          

        }
    }
}