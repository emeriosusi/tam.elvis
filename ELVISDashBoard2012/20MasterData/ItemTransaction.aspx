﻿<%@ Page Title="Item Transaction Master" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="ItemTransaction.aspx.cs" Inherits="ELVISDashBoard._20MasterData.ItemTransaction" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pre" runat="server">
<style type="text/css">
    .label 
    {
        width: 200px;
        display: inline-block;
        float: left;
    }
    .crit  
        {
            margin: 0px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            width: 49%;
            text-align: left;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Item Transaction Master Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <div class="row">
                <div class="label">
                    Transaction Type
                </div>
                <div class="lefty">
                    <dx:ASPxComboBox runat="server" ID="cbTransaction" DataSourceID="dsTx" TextField="COMBO_NAME" ValueField="COMBO_CD"
                    TextFormatString="{0}" IncrementalFilteringMode="Contains" Width="300px">
                    <Columns>
                    <dx:ListBoxColumn FieldName="COMBO_NAME" Width="250px" Caption="Name" />
                    <dx:ListBoxColumn FieldName="COMBO_CD" Width="50px" Caption="Code" />
                    </Columns>
                    </dx:ASPxComboBox>
                </div>
                </div>
                <div class="row">
                    <div class="lefty crit">
                        <div class="label">
                            Item Transaction Code</div>
                        <div class="rowwarphalfLeft">
                            <asp:TextBox runat="server" ID="txtItemTransCd" MaxLength="10" Columns="9">
                            </asp:TextBox>
                        </div>
                    </div>
                    <div class="righty crit">
                        <div class="label">
                            Item Transaction Description</div>
                        <div class="rowwarphalfLeft">
                            <asp:TextBox runat="server" ID="txtItemTransDesc" MaxLength="50" Columns="30">
                            </asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" OnClientClick="loading()" />
                    </div>
                </div>
            </div>
            <div class="rowbtn">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click"
                        OnClientClick="loading()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                KeyFieldName="ITEM_TRANSACTION_CD" EnableCallBacks="True" Settings-VerticalScrollableHeight="320"
                OnPageIndexChanged="gridGeneralInfo_PageIndexChanged" Styles-AlternatingRow-CssClass="even">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px">
                        <EditButton Visible="false" Text=" ">
                        </EditButton>
                        <UpdateButton Visible="false" Text=" ">
                        </UpdateButton>
                        <NewButton Visible="false" Text=" ">
                        </NewButton>
                        <CancelButton Visible="false" Text=" ">
                        </CancelButton>
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" 
                                ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); }"
                                CheckState="Unchecked" Style="text-align: center">
                            </dx:ASPxCheckBox>
                        </HeaderTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" Width="30px" 
                        Visible="False">
                        <DataItemTemplate>
                            <asp:Literal runat="server" ID="litGridNo"></asp:Literal>
                        </DataItemTemplate>
                        <EditItemTemplate>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Transaction Code" VisibleIndex="2" Width="200px" FieldName="TRANSACTION_CD">
                        <CellStyle HorizontalAlign="Left" />
                        <DataItemTemplate>
                        <asp:Literal runat="server" ID="TransactionLiteral" />
                        </DataItemTemplate>
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="cbgTransaction" DataSourceID="dsTxG" TextField="COMBO_NAME" ValueField="COMBO_CD"
                                TextFormatString="{0}" IncrementalFilteringMode="Contains" Width="200px">
                            <Columns>
                                <dx:ListBoxColumn FieldName="COMBO_NAME" Width="250px" Caption="Name" />
                                <dx:ListBoxColumn FieldName="COMBO_CD" Width="50px" Caption="Code" />
                            </Columns>
                            </dx:ASPxComboBox>
                        </EditItemTemplate>
                     </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Item Transaction Code" FieldName="ITEM_TRANSACTION_CD"
                        VisibleIndex="3" Width="90px">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtGridItemTransCd" Width="82px" MaxLength="10" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Item Transaction Description" FieldName="ITEM_TRANSACTION_DESC"
                        Width="330px" VisibleIndex="4" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtGridItemTransDesc" Width="322px" MaxLength="50" />
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataCheckColumn Caption="DPP Flag" FieldName="DPP_FLAG" 
                        VisibleIndex="5" Width="80px">
                         <PropertiesCheckEdit 
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" />
                        <EditItemTemplate>
                        <dx:ASPxCheckBox runat="server" ID="cbDPPFlag" 
                            ValueChecked="1" 
                            ValueUnchecked="0" 
                            ValueGrayed="" 
                            AllowGrayed="true" 
                            ValueType ="System.Int32"
                            AllowGrayedByClick="true"/>
                        </EditItemTemplate>
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewBandColumn Caption="Created" VisibleIndex="6">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CREATED_DT" VisibleIndex="0"
                                Width="135px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                                <EditItemTemplate />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1">
                                <EditItemTemplate />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Changed" VisibleIndex="7">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CHANGED_DT" VisibleIndex="0"
                                Width="135px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                                <EditItemTemplate />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CHANGED_BY" VisibleIndex="1">
                                <EditItemTemplate />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                <SettingsEditing Mode="Inline" />
                <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                <SettingsLoadingPanel ImagePosition="Top" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <AlternatingRow CssClass="even">
                    </AlternatingRow>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
                <Settings ShowStatusBar="Visible" />
                <SettingsPager Visible="false" />
                <Templates>
                    <StatusBar>
                        <div style="text-align: left;">
                            Records per page:
                            <asp:DropDownList ID="RecordPerPageSelect" runat="server" ClientIDMode="Static" 
                                OnInit="RecordPerPageSelect_Init"
                                AutoPostBack="true"
                                OnSelectedIndexChanged="RecordPerPageSelect_SelectedIndexChanged"/>
                            &nbsp;
                            <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">
                                &lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a>
                            &nbsp; Page
                            <input type="text" onchange="gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                style="width: 20px" />
                            of
                            <%# gridGeneralInfo.PageCount%>&nbsp; 
                            <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%> 
                            <a title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp;
                            <a title="Last" href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">
                                &gt;&gt;</a> &nbsp;
                        </div>
                    </StatusBar>
                </Templates>
                <SettingsPager AlwaysShowPager="true">
                </SettingsPager>
            </dx:ASPxGridView>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new (TRANSACTION_CD, ITEM_TRANSACTION_CD, ITEM_TRANSACTION_DESC, DPP_FLAG, CREATED_DT, CREATED_BY, CHANGED_DT, CHANGED_BY)"
                OnSelecting="LinqDataSource1_Selecting" TableName="TB_M_ITEM_TRANSACTION">
            </asp:LinqDataSource>
            <asp:LinqDataSource ID="dsTx" runat="server" OnSelecting="dsTx_Selecting" />
            <asp:LinqDataSource ID="dsTxG" runat="server" OnSelecting="dsTxG_Selecting" />
            <div class="rowbtn">
                <div class="btnleft">
                    <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                        SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                </div>
                <div class="btnright">
                    <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click"
                        OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click"
                        OnClientClick="loading()" />&nbsp;
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
</asp:Content>
