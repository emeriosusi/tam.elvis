﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._20MasterData;
using BusinessLogic.CommonLogic;
using Common.Data;
using Common.Data._20MasterData;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._20MasterData
{
    public partial class RoleAuthorization : BaseCodeBehind
    {
        private const string mySelf = "Role Authorization";
        readonly RoleAuthorizationLogic _logic = new RoleAuthorizationLogic();
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_2007");
        private RoleLogic role=null;
        
        #region Init

        private static readonly string[] aFields = { "ROLE_ID", "ROLE_NAME" };
        private static readonly string[] aColumns = { "txtGridRoleId", "txtGridRoleName" };
        private object[] aGrid = new object[aFields.Length];

        readonly string _ROLE_ID_ = "RoleAuth.RoleId";
        private string gRole
        {
            get
            {
                if (Session[_ROLE_ID_] == null)
                {
                    return "";
                }
                else
                {
                    return Session[_ROLE_ID_] as string;
                }
            }
            set
            {
                Session[_ROLE_ID_] = value;
            }
        }

        readonly string _SCREEN_ID_ = "RoleAuth.ScreenId";
        private string gScreen
        {
            get
            {
                if (Session[_SCREEN_ID_] == null)
                {
                    return "";
                }
                else
                {
                    return Session[_SCREEN_ID_] as string;
                }
            }
            set
            {
                Session[_SCREEN_ID_] = value;
            }
        }

        readonly string _SSCREEN_ = "RoleAuth.ScreenSelect";
        private string menuSelect
        {
            get
            {
                if (Session[_SSCREEN_] == null)
                {
                    return "";
                }
                else
                {
                    return Session[_SSCREEN_] as string;
                }
            }
            set
            {
                Session[_SSCREEN_] = value;
            }
        }

        private int RoleIndex
        {
            get
            {
                if (Session["RoleAuth.RoleIndex"] == null)
                    return -1;
                else
                    return Convert.ToInt32(Session["RoleAuth.RoleIndex"]);
            }

            set
            {
                Session["RoleAuth.RoleIndex"] = value;
            }
        }

        private int ScreenIndex
        {
            get
            {
                if (Session["RoleAuth.ScreenIndex"] == null)
                    return -1;
                else
                    return Convert.ToInt32(Session["RoleAuth.ScreenIndex"]);
            }

            set
            {
                Session["RoleAuth.ScreenIndex"] = value;
            }
        }

        private RoleLogic myRole
        {
            get
            {
                if (role==null) 
                    role = new RoleLogic(UserName, _ScreenID);
                return role;
            }
        }

        private bool CanDelete = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            CanDelete = myRole.isAllowedAccess("btnDelete");
            
            hidScreenID.Value = _ScreenID;
            lit = litMessage;
            if (!IsPostBack)
            {
                PrepareLogout();

                GenerateComboData(ddlRole, "ROLE", true);
                GenerateComboData(ddlScreen, "SCREEN", true);

                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();

                AddEnterComponent(ddlRole);
                AddEnterComponent(ddlScreen);
                AddEnterAsSearch(btnSearch, _ScreenID);
            }
        }

        private void SetFirstLoad()
        {
            btnSearch.Visible = true;
            btnClear.Visible = true;

            btnAdd.Visible = myRole.isAllowedAccess("btnAdd");
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnExportExcel.Visible = false;
            btnClose.Visible = true;
        }
        #endregion

        #region Data


        protected void dsScreenObject_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string[] a = gScreen.Split(';');
            string _screen = (a.Length > 1) ? a[1] : "";
            
            e.Result = _logic.SearchScreenObject(_screen);
        }

        protected void dsRoles_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            
            e.Result = _logic.SearchRoles(ddlRole.Value as string);
        }

        protected void dsScreen_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = _logic.SearchScreen(gRole, ddlScreen.Value as string);
        }

        protected void dsMenus_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = _logic.SearchScreen(gRole, ddlScreen.Value as string);
        }

        protected void dsRoleDetail_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string[] a = gScreen.Split(';');
            string _id = (a.Length > 0) ? a[0] : "";
            string _screen = (a.Length > 1) ? a[1] : "";
            e.Result = _logic.SearchObject(_id, _screen, txtObject.Text);
        }

        protected void ddlMenu_ValueChanged(object sender, EventArgs e)
        {
            menuSelect = (sender as ASPxGridLookup).Value as string;
            if (string.IsNullOrEmpty(menuSelect))
                menuSelect = (sender as ASPxGridLookup).Text;
        }

        #endregion

        #region Button

        bool IsEditing = false;

        protected void AddScreen_Click(object sender, EventArgs e)
        {
            bool Ok = _logic.AddRow(new RoleAuthorizationData() { ROLE_ID = gRole, SCREEN_ID = menuSelect, OBJECT_ID = "" }, UserData);
            log.Say(UserName, "AddScreen role={0} screen={1}",
                gRole, menuSelect);
            if (Ok)
            {
                Nag("MSTD00011INF", "Screen");

                if (RoleIndex >= 0) {
                    ASPxGridLookup gl = gridGeneralInfo.FindDetailRowTemplateControl(RoleIndex, "ddlMenu") as ASPxGridLookup;
                    ASPxGridView gg = gridGeneralInfo.FindDetailRowTemplateControl(RoleIndex, "Screen") as ASPxGridView;
                    gg.Selection.UnselectAll();
                    gg.DataBind();
                    gl.Value = "";
                    gl.Text = "";
                    
                }
            }
            else
            {
                string msg = "", msgCode = "";
                GetLastErrMessages(_logic.LastErr, mySelf,
                   String.Format("{0} {1}", new object[] { gRole, menuSelect }),
                   "Add", ref msg, ref msgCode);
                litMessage.Text = MessageLogic.Wrap(msg, "ERR");
                log.Log(msgCode, msg, "AddScreen_Click", (UserData!=null)? UserName:"");
            }
        }

        protected void DelScreen_Click(object sender, EventArgs e)
        {
            if (RoleIndex >= 0)
            {
                ASPxGridView gg = gridGeneralInfo.FindDetailRowTemplateControl(RoleIndex, "Screen") as ASPxGridView;
                
                if (gg==null|| gg.Selection.Count < 1) return;
                List<RoleAuthorizationData> l = new List<RoleAuthorizationData>();
                for (int i = 0; i < gg.VisibleRowCount; i++)
                {
                    if (gg.Selection.IsRowSelected(i))
                    {
                        RoleAuthorizationData r = new RoleAuthorizationData()
                        {
                            ROLE_ID = gRole,
                            SCREEN_ID = gg.GetRowValues(i, "SCREEN_ID") as string,
                            OBJECT_ID = ""
                        };

                        if (!string.IsNullOrEmpty(r.SCREEN_ID)) { l.Add(r); };
                    }
                }
                bool Ok = _logic.Delete(l);

                if (Ok)
                {
                    Nag("MSTD00015INF", "Screen");
                    gg.DataBind();
                    
                }
                else
                {
                    string msg = "", msgCode = "";
                    GetLastErrMessages(_logic.LastErr, mySelf,
                       String.Format("{0} {1}", new object[] { gRole, menuSelect }),
                       "Delete", ref msg, ref msgCode);
                    litMessage.Text = MessageLogic.Wrap(msg, "ERR");
                    log.Log(msgCode, msg, "DeleteScreen_Click", UserName);                    
                }
            }
        }


        protected void NewScreen_Click(object sender, EventArgs e)
        {
            ScreenAddDiv.Visible = true;
            ObjectAddDiv.Visible = false;
            popNew.Show();
        }

        protected void NewRoleDetail_Click(object sender, EventArgs e)
        {
            ScreenAddDiv.Visible = false;
            ObjectAddDiv.Visible = true;
            popNew.Show();
        }

        protected void AddRoleDetail_Click(object sender, EventArgs e)
        {
            if (RoleIndex < 0 || ScreenIndex < 0) return;
            
            ASPxGridView gg = gridGeneralInfo.FindDetailRowTemplateControl(RoleIndex, "Screen") as ASPxGridView;
            if (gg == null) return;
            ASPxGridLookup ddlObject = gg.FindDetailRowTemplateControl(ScreenIndex, "ddlObject") as ASPxGridLookup;
            if (ddlObject == null) return;

            string[] a = gScreen.Split(';');
            string _id = (a.Length > 0) ? a[0] : "";
            string _screen = (a.Length > 1) ? a[1] : "";
            string _object = ddlObject.Text;

            RoleAuthorizationData r = new RoleAuthorizationData()
            {
                ROLE_ID = _id,
                SCREEN_ID = _screen,
                OBJECT_ID = _object
            };
            log.Say(UserName, "AddRoleDetail role={0} screen={1} object={2}", 
                _id, _screen, _object);
            bool Ok = _logic.AddRow(r, UserData);
            if (Ok)
            {
                Nag("MSTD00011INF", "Role Detail");
                ddlObject.Text = "";
                
                ASPxGridView gr = gg.FindDetailRowTemplateControl(ScreenIndex, "RoleDetail") as ASPxGridView;
                if (gr != null)
                {
                    gr.Selection.UnselectAll();
                    gr.DataBind();
                }
            }
            else
            {
                string msg = "", msgCode = "";
                GetLastErrMessages(_logic.LastErr, mySelf,
                   String.Format("{0} {1}", new object[] { gRole, menuSelect }),
                   "Add Role Detail", ref msg, ref msgCode);
                litMessage.Text = MessageLogic.Wrap(msg, "ERR");
                log.Log(msgCode, msg, "AddRoleDetail_Click", UserName);                
            }
        }

        protected void DelRoleDetail_Click(object sender, EventArgs e)
        {
            if (RoleIndex < 0 || ScreenIndex < 0) { return; }
            ASPxGridView gg = gridGeneralInfo.FindDetailRowTemplateControl(RoleIndex, "Screen") as ASPxGridView;
            if (gg == null) { return; }
            ASPxGridView gr = gg.FindDetailRowTemplateControl(ScreenIndex, "RoleDetail") as ASPxGridView;
            if (gr == null) { return; }

            string[] a = gScreen.Split(';');
            string _id = (a.Length > 0) ? a[0] : "";
            string _screen = (a.Length > 1) ? a[1] : "";
            if (gr.Selection.Count < 1) return;
            List<RoleAuthorizationData> l = new List<RoleAuthorizationData>();
            for (int i = 0; i < gr.VisibleRowCount; i++)
            {
                if (gr.Selection.IsRowSelected(i))
                {
                    RoleAuthorizationData r = new RoleAuthorizationData()
                    {
                        ROLE_ID = _id,
                        SCREEN_ID = _screen,
                        OBJECT_ID = gr.GetRowValues(i, "OBJECT_ID") as string
                    };

                    if (!string.IsNullOrEmpty(r.OBJECT_ID)) { l.Add(r); };
                }
            }
            bool Ok = _logic.Delete(l);

            if (Ok)
            {
                Nag("MSTD00015INF", "Role Detail");
                gr.DataBind();
            }
            else
            {
                string msg = "", msgCode = "";
                GetLastErrMessages(_logic.LastErr, mySelf,
                   String.Format("{0} {1}", new object[] { _id, _screen }),
                   "Delete", ref msg, ref msgCode);
                litMessage.Text = MessageLogic.Wrap(msg, "ERR");
                log.Log(msgCode, msg, "DeleteRoleDetail_Click", UserName);                
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.AddNewRow();
            PageMode = Common.Enum.PageMode.Add;
            IsEditing = true;
            SetEditDetail();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            PageMode = Common.Enum.PageMode.Edit;
                            gridGeneralInfo.SettingsBehavior.AllowSort = false;
                            IsEditing = true;
                            Session["visibleIndex"] = i;
                            gridGeneralInfo.StartEdit(i);
                            gridGeneralInfo.Selection.UnselectAll();
                            SetEditDetail();
                            break;
                        }
                    }
                }
                else if (gridGeneralInfo.Selection.Count == 0)
                {
                    Nag("MSTD00009WRN");
                }
                else if (gridGeneralInfo.Selection.Count > 0)
                {
                    Nag("MSTD00016WRN");
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.DataSourceID = null;
            gridGeneralInfo.DataBind();
            btnExportExcel.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            ddlRole.SelectedIndex = 0;
            ddlScreen.SelectedIndex = 0;
            txtObject.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                lblConfirmationDelete.Text = string.Format(resx.Message( "MSTD00008CON"), "delete");
                ConfirmationDeletePopUp.Show();
            }
            else if (gridGeneralInfo.Selection.Count == 0)
            {
                Nag("MSTD00009WRN", "delete");
            }
        }


        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            List<RoleAuthorizationData> l = _logic.SearchObject(ddlRole.Value as string,
                ddlScreen.Value as string, txtObject.Text);
            if (l != null)
            {
                try
                {
                    string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                    UserData userdata = (UserData)Session["UserData"];
                    string _FileName = "ROLE_AUTH_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                    this.Response.Clear();
                    this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.ContentType = "application/vnd.ms-excel";
                    this.Response.BinaryWrite(_logic.Get(UserName, imagePath, l));
                    this.Response.End();
                }
                catch (Exception Ex)
                {
                    Nag("MSTD00018INF", Ex.Message);
                    Handle(Ex);                    
                }
            }
            else
                Nag("MSTD00018INF");
        }


        protected void OkSaveNew_Click(object sender, EventArgs e)
        {
            if (RoleIndex < 0)
            {                
                return;
            }
            

            ASPxGridView gg = gridGeneralInfo.FindDetailRowTemplateControl(RoleIndex, "Screen") as ASPxGridView;
            if (gg == null) return;
            bool Ok = false;

            if (ObjectAddDiv.Visible)
            {
                if (ScreenIndex < 0)
                {
                    return;
                }

                string[] a = gScreen.Split(';');
                string _id = (a.Length > 0) ? a[0] : "";
                string _screen = (a.Length > 1) ? a[1] : "";
                string _object = NewObject.Text;

                RoleAuthorizationData r = new RoleAuthorizationData()
                {
                    ROLE_ID = _id,
                    SCREEN_ID = _screen,
                    OBJECT_ID = _object
                };

                log.Say(UserName, "New RoleDetail role={0} screen={1} object={2}",
                    _id, _screen, _object);
                Ok = _logic.AddRow(r, UserData);
                if (Ok)
                {
                    Nag("MSTD00011INF", "Role Detail");
                    NewObject.Text = "";

                    ASPxGridView gr = gg.FindDetailRowTemplateControl(ScreenIndex, "RoleDetail") as ASPxGridView;
                    if (gr != null)
                    {
                        gr.Selection.UnselectAll();
                        gr.DataBind();
                    }
                }
                else
                {
                    string msg = "", msgCode = "";
                    GetLastErrMessages(_logic.LastErr, mySelf,
                       String.Format("{0} {1}", new object[] { gRole, menuSelect }),
                       "New Role Detail", ref msg, ref msgCode);
                    litMessage.Text = MessageLogic.Wrap(msg, "ERR");
                    log.Log(msgCode, msg, "Ok Save New Role Object", UserName);
                   
                    
                }
            } 
            else if (ScreenAddDiv.Visible)
            {
                /// add new screen based on inputs
                ScreenData s = new ScreenData()
                {
                    MENU_ID = NewMenuID.Text,
                    PARRENT_ID = Convert.ToString(NewParentIDLookup.Value),
                    CAPTION = NewCaption.Text,
                    SCREEN_ID = NewScreenID.Text,
                    PATH = NewPath.Text
                };
                log.Say(UserName, 
                    "New Screen||PARENT={0}|ID={1}|SCREEN={2}|CAPTION={3}|PATH={4}"
                    .Replace("|","\r\n\t"),
                    s.PARRENT_ID, s.MENU_ID, s.SCREEN_ID, s.CAPTION, s.PATH);
                Ok = _logic.PutMenu(s, 0);
                if (Ok)
                {
                    Nag("MSTD00011INF", "Menu");

                    NewMenuID.Text = "";
                    NewParentIDLookup.Text = "";
                    NewCaption.Text = "";
                    NewScreenID.Text = "";
                    NewPath.Text = "";
                    
                    if (gg != null)
                    {
                        gg.Selection.UnselectAll();
                        gg.DataBind();
                    }
                }
                else
                {
                    string msg = "", msgCode = "";
                    GetLastErrMessages(_logic.LastErr, mySelf, gRole,
                       "New Menu", ref msg, ref msgCode);
                    litMessage.Text = MessageLogic.Wrap(msg, "ERR");
                    log.Log(msgCode, msg, "Ok Save New Menu", UserName);
                }

            }
        }

        protected void CancelSaveNew_Click(object sender, EventArgs e)
        {

        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                List<RoleMasterData> l = new List<RoleMasterData>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {
                        RoleMasterData g = Gather(i);
                        l.Add(g);
                    }
                }

                if (l.Count > 0)
                {
                    PageMode = Common.Enum.PageMode.View;
                    result = _logic.Delete(l);
                    if (!result)
                    {
                        Nag("MSTD00014ERR");
                        gridGeneralInfo.Selection.UnselectAll();
                    }
                    else
                    {
                        Nag("MSTD00049INF", mySelf);
                        gridGeneralInfo.Selection.UnselectAll();
                        gridGeneralInfo.DataSourceID = dsRoles.ID;
                        gridGeneralInfo.DataBind();
                    }
                }
                GenerateComboData(ddlRole, "ROLE", true);
                GenerateComboData(ddlScreen, "SCREEN", true);
            }
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            Mark();

            RoleMasterData g = Gather(-1);

            if (PageMode == Common.Enum.PageMode.Add)
            {
                #region Add
                if (ValidateInputSave())
                {
                    ErrorData err = new ErrorData();
                    string func = resx.FunctionId("FCN_ROLE_AUTH");

                    bool result = _logic.AddRow(g, UserData);

                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataSourceID = dsRoles.ID;
                        gridGeneralInfo.DataBind();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Nag("MSTD00011INF", mySelf);
                    }
                    else
                    {
                        string msg = "", msgCode = "";
                        GetLastErrMessages(_logic.LastErr, mySelf,
                           String.Format("{0} {1}", new object[] { g.ROLE_ID, g.ROLE_NAME }),
                           "Add", ref msg, ref msgCode);
                        litMessage.Text = MessageLogic.Wrap(msg, "ERR");
                        log.Log(msgCode, msg, "btnSaveDetail_Click", UserName);                        
                    }
                }
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSave())
                {
                    bool result = _logic.AddRow(g, UserData);

                    if (result == true)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataBind();
                        gridGeneralInfo.Selection.UnselectAll();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Nag("MSTD00013INF", mySelf);
                    }
                    else
                    {
                        Nag("MSTD00012ERR", mySelf);
                    }
                }
                #endregion
            }
            GenerateComboData(ddlRole, "ROLE", true);
            GenerateComboData(ddlScreen, "SCREEN", true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridGeneralInfo.DataSourceID = dsRoles.ID;

                gridGeneralInfo.DataBind();
                PageMode = Common.Enum.PageMode.View;
                SetCancelEditDetail();
            }

        }
        #endregion

        #region Grid

        protected void EditingButtons(ASPxGridView g, int vi, string AddText, string EditText, string DeleteText, string ObjectText)
        {
            

            Button newButton = g.FindDetailRowTemplateControl(vi, AddText) as Button;
            newButton.Visible = myRole.isAllowedAccess("btnAdd");

            Button editButton = g.FindDetailRowTemplateControl(vi, EditText) as Button;
            editButton.Visible = myRole.isAllowedAccess("btnEdit");

            Button deleteButton = g.FindDetailRowTemplateControl(vi, DeleteText) as Button;
            deleteButton.Visible = CanDelete;

            Control o = g.FindDetailRowTemplateControl(vi, ObjectText) as Control;
            o.Visible = myRole.isAllowedAccess("btnEdit");
        }

        protected void grid_ExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
        {
            if (e.Expanded)
            {
                RoleIndex = e.VisibleIndex;
                EditingButtons(sender as ASPxGridView, e.VisibleIndex, "NewScreen", "AddScreen", "DelScreen", "ddlMenu");                
            }
            else
                RoleIndex = -2;
        }

        protected void Screen_ExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e) {
            if (e.Expanded)
            {
                ScreenIndex = e.VisibleIndex;
                EditingButtons(sender as ASPxGridView, e.VisibleIndex, "NewRoleDetail", "AddRoleDetail", "DelRoleDetail", "ddlObject");                
            }
            else
                ScreenIndex = -2;
        }

        protected string WriteSelectedIndex(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected void Screen_CustomUnbound(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName.Equals("ScreenKey"))
            {
                e.Value = Convert.ToString(e.GetListSourceFieldValue("ROLE_ID"))
                    + ";"
                    + Convert.ToString(e.GetListSourceFieldValue("SCREEN_ID"));
            }
        }

        protected void Screen_DataSelect(object sender, EventArgs e)
        {
            gRole = (sender as ASPxGridView).GetMasterRowKeyValue() as string;
        }

        protected void RoleDetail_CustomUnbound(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName.Equals("RoleDetailKey"))
            {
                e.Value = Convert.ToString(e.GetListSourceFieldValue("ROLE_ID"))
                    + ";"
                    + Convert.ToString(e.GetListSourceFieldValue("SCREEN_ID"))
                    + ";"
                    + Convert.ToString(e.GetListSourceFieldValue("OBJECT_ID"));
            }
        }

        protected void RoleDetail_DataSelect(object sender, EventArgs e)
        {
            gScreen = (sender as ASPxGridView).GetMasterRowKeyValue() as string;
        }


        protected void grid_CommandButtonInit(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            
            // e.Column.ShowSelectCheckbox = CanDelete;
            ASPxGridView g = (sender as ASPxGridView);
            // e.Column.SelectButton.Visible = CanDelete;
            e.Visible = CanDelete;
            ASPxCheckBox x = g.FindHeaderTemplateControl(e.Column, g.ID + "SelectAllCheckbox") as ASPxCheckBox;
            if (x != null)            
            {
                x.Visible = CanDelete;
            }
        }

        // const string GridCustomPageSizeName = "gridCustomPageSize";
        protected void grid_CustomCallback(object sender,
                 DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            int newPageSize;
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            gridGeneralInfo.SettingsPager.PageSize = newPageSize;
            Session[GridCustomPageSizeName] = newPageSize;
            if (gridGeneralInfo.VisibleRowCount > 0)
                gridGeneralInfo.DataSourceID = dsRoles.ID;
            gridGeneralInfo.DataBind();
        }

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
            }

            if (!IsEditing || e.RowType != GridViewRowType.InlineEdit) return;

            #region init
            Mark();

            ASPxTextBox key1 = aGrid[0] as ASPxTextBox;
            ASPxTextBox key2 = aGrid[1] as ASPxTextBox;

            #endregion
            #region add
            if (PageMode == Common.Enum.PageMode.Add)
            {
                key1.Focus();
            }
            #endregion
            #region edit
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                if (_VisibleIndex == e.VisibleIndex)
                {
                    #region fill data edit
                    RoleMasterData g = Gather(_VisibleIndex);


                    key1.ReadOnly = true;

                    Scatter(g);
                    #endregion
                }
            }
            #endregion

        }

        protected void gridGeneralInfo_PageIndexChanged(object sender, EventArgs e)
        {
            gridGeneralInfo.DataBind();
        }

        #endregion

        #region Function
        private RoleMasterData Gather(int row = -1)
        {
            RoleMasterData r = new RoleMasterData();
            if (row >= 0)
            {
                r.ROLE_ID = gridGeneralInfo.GetRowValues(row, "ROLE_ID") as string;
                r.ROLE_NAME = gridGeneralInfo.GetRowValues(row, "ROLE_NAME") as string;

                // current index
            }
            else
            {
                // current edit 
                r.ROLE_ID = (aGrid[0] as ASPxTextBox).Text as string;
                r.ROLE_NAME = (aGrid[1] as ASPxTextBox).Text as string;
            }
            return r;
        }

        private void Scatter(RoleMasterData r)
        {
            (aGrid[0] as ASPxTextBox).Text = r.ROLE_ID;
            (aGrid[1] as ASPxTextBox).Text = r.ROLE_NAME;
        }

        private void Mark()
        {
            for (int i = 0; i < (aFields.Length); i++)
            {
                Object o = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns[aFields[i]] as GridViewDataColumn, aColumns[i]);
                if (o == null)
                {
                    string s = string.Format("ERR: cannot find field {0} in grid of column {1}", aFields[i], aColumns[i]);
                    
                    log.Log("INF", s, "Mark", UserName, "ROLEAUTH");
                }
                else
                {
                    aGrid[i] = o;
                }
            }
        }

        private bool ValidateInputSearch() { return true; }
        private bool ValidateInputSave() { return true; }

        protected void SetEditDetail()
        {
            btnSaveDetail.Visible = true;
            btnCancelDetail.Visible = true;

            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnClose.Visible = false;
            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridGeneralInfo.SettingsBehavior.AllowSort = false;
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;
            setSearchEnabled(false);
        }

        protected void SetCancelEditDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData err = null;
                btnEdit.Visible = myRole.isAllowedAccess("btnEdit", ref err);
                btnDelete.Visible = CanDelete;
                btnExportExcel.Visible = myRole.isAllowedAccess("btnExportExcel");
            }
            else
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;
                btnExportExcel.Visible = false;
            }
            setSearchEnabled();
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        }

        protected void setSearchEnabled(bool can = true)
        {
            ddlRole.Enabled = can;
            ddlScreen.Enabled = can;
            txtObject.Enabled = can;
        }
        #endregion

    }
}
