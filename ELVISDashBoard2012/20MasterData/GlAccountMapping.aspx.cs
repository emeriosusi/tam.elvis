﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._20MasterData;
using Common.Data;
using Common.Data._20MasterData;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._20MasterData
{
    public partial class GlAccountMapping : BaseCodeBehind
    {
        #region Init
        private const string mySelf = "GL Account Mapping";
        readonly GlAccountMappingLogic myLogic = new GlAccountMappingLogic();
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_2004");
       
        bool IsEditing;

        private static readonly string[] aFields = { "TRANSACTION_CD", "ITEM_TRANSACTION_DESC", "GL_ACCOUNT", 
                                                     "NEW_ITEM_TRANSACTION_DESC", "NEW_FORMULA", "SUM_UP_FLAG_NAME" };
        private static readonly string[] aColumns = { "ddlGridTransactionCode", "ddlGridItemTransaction", "txtGridGlAccount",
                                                      "ddlGridNewitemTransaction", "txtGridNewFormula", "ddlGridSumUpFlag" };
        private object[] aGrid = new object[aFields.Length];

        private enum me:int
        {
            TRANSACTION_CD = 0,
            ITEM_TRANSACTION = 1,
            GL_ACCOUNT = 2,
            NEW_ITEM_TRANSACTION = 3,
            NEW_FORMULA = 4,
            SUM_UP_FLAG_NAME = 5
        };


        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }
        #endregion

        #region Getter Setter for Data List
        private List<GlAccountMappingData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<GlAccountMappingData>();
                }
                else
                {
                    return (List<GlAccountMappingData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            hidScreenID.Value = _ScreenID;

            if (!IsPostBack)
            {
                PrepareLogout();
                GenerateComboData(ddlTransaction, "TRANSACTION_TYPE", true);
                GenerateComboData(ddlItemTransaction, "ITEM_TRANSACTION", true);                

                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();

                AddEnterComponent(ddlTransaction);
                AddEnterComponent(ddlItemTransaction);
                AddEnterAsSearch(btnSearch, _ScreenID);
            }


        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);
            //WorkListLogic _WorkListLogic = new WorkListLogic(UserName);

            btnSearch.Visible = true && _RoleLogic.isAllowedAccess("btnSearch");
            btnClear.Visible = true;

            btnAdd.Visible = true && _RoleLogic.isAllowedAccess("btnAdd");
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnExportExcel.Visible = false;
            btnClose.Visible = true;
        }
        #endregion

        #region Grid

        private bool GetIntFromObject(ref int ifield, object o)
        {
            int v = 0;
            if (o != null && Int32.TryParse(o.ToString(), out v))
            {
                ifield = v;
                return true;
            }
            else
                return false;
        }

        private GlAccountMappingData Gather(int row)
        {
            GlAccountMappingData r = new GlAccountMappingData();
            int iCode = 0, iGl = 0, iItem = 0, iNew = 0 ;
            decimal nf = 0;
            object _nf = "";
            if (row >= 0)
            {
                if (GetIntFromObject(ref iCode, gridGeneralInfo.GetRowValues(row, aFields[(int) me.TRANSACTION_CD])))
                    r.TRANSACTION_CD = iCode;
                else if (GetIntFromObject(ref iCode, gridGeneralInfo.GetRowValues(row, "CODE")))
                    r.TRANSACTION_CD = iCode;

                if (GetIntFromObject(ref iItem, gridGeneralInfo.GetRowValues(row, aFields[(int)me.ITEM_TRANSACTION])))
                    r.ITEM_TRANSACTION_CD = iItem;
                else if (GetIntFromObject(ref iItem, gridGeneralInfo.GetRowValues(row, "ITEM_TRANSACTION_CD")))
                    r.ITEM_TRANSACTION_CD = iItem;


                if (GetIntFromObject(ref iNew, gridGeneralInfo.GetRowValues(row, aFields[(int)me.NEW_ITEM_TRANSACTION])))
                    r.NEW_ITEM_TRANSACTION_CD = iNew;                    
                else if (GetIntFromObject(ref iNew, gridGeneralInfo.GetRowValues(row, "NEW_ITEM_TRANSACTION_CD")))
                    r.NEW_ITEM_TRANSACTION_CD = iNew;
                
                if (GetIntFromObject(ref iGl, gridGeneralInfo.GetRowValues(row, aFields[(int) me.GL_ACCOUNT])))
                    r.GL_ACCOUNT = iGl;

                _nf = gridGeneralInfo.GetRowValues(row, aFields[(int)me.NEW_FORMULA]);
                if (_nf != null)
                {
                    if (Decimal.TryParse(_nf.ToString(), out nf))
                        r.NEW_FORMULA = nf;
                } 
                if (GetIntFromObject(ref iGl, gridGeneralInfo.GetRowValues(row, "SUM_UP_FLAG")))
                    r.SUM_UP_FLAG = iGl;

                r.SUM_UP_FLAG_NAME = Convert.ToString(gridGeneralInfo.GetRowValues(row, aFields[(int)me.SUM_UP_FLAG_NAME]));
            }
            else
            {
                if (GetIntFromObject(ref iCode, (aGrid[(int) me.TRANSACTION_CD] as ASPxComboBox).Value)) 
                    r.TRANSACTION_CD = iCode;
                r.ITEM_TRANSACTION_CD = Convert.ToInt32((aGrid[(int)me.ITEM_TRANSACTION] as ASPxComboBox).Value);
                
                if (GetIntFromObject(ref iGl, (aGrid[(int) me.GL_ACCOUNT] as TextBox).Text))
                    r.GL_ACCOUNT = iGl;

                r.NEW_ITEM_TRANSACTION_CD = Convert.ToInt32((aGrid[(int)me.NEW_ITEM_TRANSACTION] as ASPxComboBox).Value);

                string _newf;
                _newf = (aGrid[(int)me.NEW_FORMULA] as TextBox).Text;
                if (Decimal.TryParse(_newf, out nf)) 
                    r.NEW_FORMULA = nf;

                if (GetIntFromObject(ref iCode, (aGrid[(int) me.SUM_UP_FLAG_NAME] as ASPxComboBox).Value))
                    r.SUM_UP_FLAG = iCode;
            }            
            return r;
        }

        private void Scatter(GlAccountMappingData x)
        {
            (aGrid[(int)me.TRANSACTION_CD] as ASPxComboBox).Value = Convert.ToString(x.TRANSACTION_CD);
            (aGrid[(int)me.ITEM_TRANSACTION] as ASPxComboBox).Value = Convert.ToString(x.ITEM_TRANSACTION_CD);
            // (aGrid[(int)me.ITEM_TRANSACTION] as ASPxComboBox).Text = x.ITEM_TRANSACTION_DESC;

            (aGrid[(int)me.GL_ACCOUNT] as TextBox).Text = x.GL_ACCOUNT.ToString();

            (aGrid[(int)me.NEW_ITEM_TRANSACTION] as ASPxComboBox).Value = Convert.ToString(x.NEW_ITEM_TRANSACTION_CD);
            // (aGrid[(int)me.NEW_ITEM_TRANSACTION] as ASPxComboBox).Text = x.NEW_ITEM_TRANSACTION_DESC;

            (aGrid[(int)me.NEW_FORMULA] as TextBox).Text = x.NEW_FORMULA.ToString();
            (aGrid[(int)me.SUM_UP_FLAG_NAME] as ASPxComboBox).Value = Convert.ToString(x.SUM_UP_FLAG_NAME);
        }

        private void Mark()
        {
            for (int i = 0; i < (aFields.Length); i++)
            {
                Object o = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns[aFields[i]] as GridViewDataColumn, aColumns[i]);


                if (o == null)
                    logic.Say("Mark","ERR: cannot find field {0} in grid of column {1}", aFields[i], aColumns[i]);                    
                else
                {
                    aGrid[i] = o;
                }
            }
        }
        
        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data

            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoGeneralInfo = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex, gridGeneralInfo.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
            #endregion
            #region rowtype edit
            if (!IsEditing || e.RowType != GridViewRowType.InlineEdit) return;

            #region init
            Mark();

            ASPxComboBox key1 = aGrid[(int)me.TRANSACTION_CD] as ASPxComboBox;
            ASPxComboBox key2 = aGrid[(int)me.ITEM_TRANSACTION] as ASPxComboBox;
            ASPxComboBox newItem = aGrid[(int)me.NEW_ITEM_TRANSACTION] as ASPxComboBox;
            
            TextBox gl = aGrid[(int)me.GL_ACCOUNT] as TextBox;
            GenerateComboData(key1, "TRANSACTION_CD", false);

            ASPxComboBox suf = aGrid[(int)me.SUM_UP_FLAG_NAME] as ASPxComboBox;
            GenerateComboData(suf, "SUM_UP_FLAG", false);
            GenerateComboData(key2, "ITEM_TRANSACTION", false);
            GenerateComboData(newItem, "ITEM_TRANSACTION", false);            
            
            #endregion
            #region add
            if (PageMode == Common.Enum.PageMode.Add)
            {
                key1.Focus();
            }
            #endregion
            #region edit
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                if (_VisibleIndex == e.VisibleIndex)
                {
                    #region fill data edit
                    GlAccountMappingData g = Gather(_VisibleIndex);
                    
                    key1.ReadOnly = true;
                    key2.ReadOnly = true;

                    Scatter(g);
                    #endregion
                }
            }
            #endregion


            #endregion
        }

        protected void gridGeneralInfo_PageIndexChanged(object sender, EventArgs e)
        {
            //gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
            gridGeneralInfo.DataBind();
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = myLogic.SearchInqury(ddlTransaction.SelectedValue, ddlItemTransaction.SelectedValue, txtGlAccount.Text);
            e.Result = ListInquiry;
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.DataSourceID = null;
            gridGeneralInfo.DataBind();
            btnExportExcel.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            ddlTransaction.SelectedIndex = 0;
            ddlItemTransaction.SelectedIndex = 0;
            txtGlAccount.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                lblConfirmationDelete.Text = string.Format(resx.Message( "MSTD00008CON"), "delete");
                ConfirmationDeletePopUp.Show();
            }
            else if (gridGeneralInfo.Selection.Count == 0)
            {
                #region Error
                Err("MSTD00009WRN", "WRN", "delete");
                #endregion
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                List<GlAccountMappingData> l = new List<GlAccountMappingData>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {

                        GlAccountMappingData g = Gather(i);

                        l.Add(g);
                    }
                }

                if (l.Count > 0)
                {
                    PageMode = Common.Enum.PageMode.View;
                    result = myLogic.Delete(l);
                    if (!result)
                    {
                        Err("MSTD00014ERR", "ERR", "ConfirmDelete");
                        gridGeneralInfo.Selection.UnselectAll();
                    }
                    else
                    {
                        Err("MSTD00049INF", "INF", "ConfirmDelete", mySelf);
                        gridGeneralInfo.Selection.UnselectAll();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                    }
                }
                GenerateComboData(ddlTransaction, "TRANSACTION_TYPE", true);
                GenerateComboData(ddlItemTransaction, "ITEM_TRANSACTION", true);
            }
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            Mark();

            GlAccountMappingData g = Gather(-1);

            if (PageMode == Common.Enum.PageMode.Add)
            {
                #region Add
                if (ValidateInputSave())
                {
                    ErrorData err = new ErrorData();
                    DoStartLog(resx.FunctionId("FCN_GL_ACCOUNT_MAPPING"), "");
                    bool result = myLogic.InsertRow(g, UserData); 
                        
                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Err("MSTD00011INF", "INF", "btnSaveDetail_Click", mySelf);
                    }
                    else
                    {
                        string msg = "", msgCode = "";                        
                        GetLastErrMessages(myLogic.LastErr, mySelf, 
                           String.Format("{0} {1}",new object[] { g.TRANSACTION_NAME , g.ITEM_TRANSACTION_DESC }), 
                           "Add", ref msg, ref msgCode);
                        litMessage.Text = _m.SetMessage(msg, msgCode);
                        Log(msgCode, "ERR", "btnSaveDetail_Click", msg);                         
                    }
                }
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSave())
                {
                    DoStartLog(resx.FunctionId("FCN_TRANSACTION_TYPE_MASTER"), "");
                    bool result = myLogic.EditRow(g, UserData); 

                    if (result == true)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataBind();
                        gridGeneralInfo.Selection.UnselectAll();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Err("MSTD00013INF", "INF", "btnSaveDetail_Click", mySelf);
                    }
                    else
                    {
                        Err("MSTD00012ERR", "INF", "btnSaveDetail_Click", mySelf);
                    }
                }
                #endregion
            }
            GenerateComboData(ddlTransaction, "TRANSACTION_TYPE", true);
            GenerateComboData(ddlItemTransaction, "ITEM_TRANSACTION", true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;

                gridGeneralInfo.DataBind();
                PageMode = Common.Enum.PageMode.View;
                SetCancelEditDetail();
            }

        }

        protected void setSearchEnabled(bool can = true)
        {
            ddlTransaction.Enabled = can;
            ddlItemTransaction.Enabled = can;
            txtGlAccount.Enabled = can;            
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.AddNewRow();
            PageMode = Common.Enum.PageMode.Add;
            IsEditing = true;
            SetEditDetail();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            PageMode = Common.Enum.PageMode.Edit;
                            gridGeneralInfo.SettingsBehavior.AllowSort = false;
                            IsEditing = true;
                            Session["visibleIndex"] = i;
                            gridGeneralInfo.StartEdit(i);
                            gridGeneralInfo.Selection.UnselectAll();
                            SetEditDetail();
                            break;
                        }
                    }
                }
                else if (gridGeneralInfo.Selection.Count == 0)
                {
                    Err("MSTD00009WRN", "WRN", "");
                }
                else if (gridGeneralInfo.Selection.Count > 0)
                {
                    Err("MSTD00016WRN", "WRN", "");
                }
            }
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }

        #region export to excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (ListInquiry != null)
            {
                string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                UserData userdata = (UserData)Session["UserData"];
                string _FileName = "GL_ACCOUNT_MAPPING" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                this.Response.Clear();
                this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                this.Response.Charset = "";
                this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                this.Response.ContentType = "application/vnd.ms-excel";
                try
                {
                    this.Response.BinaryWrite(myLogic.Get(UserName, imagePath, ListInquiry));
                }
                catch (Exception ex)
                {
                    log.Log("MSTD00018INF", ex.Message, "ExportExcel");
                }
                this.Response.End();
                
            }
            else
                Err("MSTD00018INF", "ERR", "ExportExcel");
        }
        #endregion

        protected void SetEditDetail()
        {
            btnSaveDetail.Visible = true;
            btnCancelDetail.Visible = true;

            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnClose.Visible = false;
            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridGeneralInfo.SettingsBehavior.AllowSort = false;
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;
            setSearchEnabled(false);
        }

        protected void SetCancelEditDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData err = null;
                btnEdit.Visible = role.isAllowedAccess("btnEdit", ref err);
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref err);
                btnExportExcel.Visible = role.isAllowedAccess("btnExportExcel");
            }
            else 
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;
                btnExportExcel.Visible = false;
            }
            setSearchEnabled();
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        }

        #endregion

        #region Function

        private bool ValidateInputSave()
        {
            bool code = true;
            Mark();

            //REGION FOR VALIDATING INPUT SAVE

            ASPxComboBox key1 = (aGrid[(int)me.TRANSACTION_CD] as ASPxComboBox);
            ASPxComboBox key2 = (aGrid[(int)me.ITEM_TRANSACTION] as ASPxComboBox);
            ASPxComboBox newItem = (aGrid[(int)me.NEW_ITEM_TRANSACTION] as ASPxComboBox);
            
            TextBox gl = (aGrid[(int)me.GL_ACCOUNT] as TextBox);
            TextBox nf = (aGrid[(int)me.NEW_FORMULA] as TextBox);
            ASPxComboBox suf = (aGrid[(int)me.SUM_UP_FLAG_NAME] as ASPxComboBox);

            if (key1.Value == null || ((key1.Value != null) && String.IsNullOrEmpty(key1.Value.ToString())))
            {
                Err("MSTD00017WRN", "WRN", "Validate", "Transaction Code");
                code = false;
            }
            if (key2.Value == null || (key2.Value != null && String.IsNullOrEmpty(key2.Value.ToString())))
            {
                Err("MSTD00017WRN", "WRN", "Validate", "Item Transaction");
                code = false;
            }
            if (newItem.Value == null || (newItem.Value != null && String.IsNullOrEmpty(newItem.Value.ToString())))
            {
                Err("MSTD00017WRN", "WRN", "Validate", "New Item Transaction");
                code = false;
            }
            if (String.IsNullOrEmpty(gl.Text))
            {
                Err("MSTD00017WRN", "WRN", "Validate", "GL Code");
                code = false;
            }
            else
            {
                if (Convert.ToInt64(gl.Text) > Int32.MaxValue)
                {
                    Err("MSTD00033WRN", "WRN", "Validate", "GL Code", Int32.MaxValue.ToString());
                    code = false;
                }
            }
            if (!String.IsNullOrEmpty(nf.Text))
            {
                decimal _nf = 0;

                if (!Decimal.TryParse(nf.Text,out _nf) || (_nf >= 10))
                {
                    Err("MSTD00033WRN", "WRN", "Validate", "New Formula", "10");
                    code = false;
                }
            }
            if (suf.Value == null || String.IsNullOrEmpty(suf.Value.ToString()))
            {
                Err("MSTD00017WRN", "WRN", "Validate", "Sum Up Flag");
                code = false;
            }
            return code;
        }

        private bool ValidateInputSearch()
        {
            return true;
        }

        private void Err(string code, string msgType, string Where, string Why = null, string Why2 = null)
        {
            if (String.IsNullOrEmpty(Why))
                litMessage.Text += _m.SetMessage(resx.Message( code), code) + Environment.NewLine;
            else
                litMessage.Text += _m.SetMessage(string.Format(resx.Message( code), Why, Why2), code) + Environment.NewLine;

            if (!msgType.Equals("WRN"))
                Log(code, msgType, Where, string.Format(resx.Message( code), Why, Why2));
        }

        #endregion

        #region paging
        // const string GridCustomPageSizeName = "gridCustomPageSize";
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
        }
        protected void grid_CustomUnbound(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "TRANSACTION_ITEM")
            {
                e.Value = Convert.ToString(e.GetListSourceFieldValue("TRANSACTION_CD")) + "-" + Convert.ToString(e.GetListSourceFieldValue("ITEM_TRANSACTION_CD"));
            }
            else if (e.Column.FieldName == "CODE")
            {
                e.Value = e.GetListSourceFieldValue("TRANSACTION_CD");
            }
        }

        protected void grid_CustomCallback(object sender,
    DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            int newPageSize;
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            gridGeneralInfo.SettingsPager.PageSize = newPageSize;
            Session[GridCustomPageSizeName] = newPageSize;
            if (gridGeneralInfo.VisibleRowCount > 0)
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
            gridGeneralInfo.DataBind();
        }
        protected string WriteSelectedIndex(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        //protected string GetShowingOnPage()
        //{
        //    int pageSize = gridGeneralInfo.SettingsPager.PageSize;
        //    if (gridGeneralInfo.PageIndex < 0)
        //        gridGeneralInfo.PageIndex = 0;
        //    int startIndex = gridGeneralInfo.PageIndex * pageSize + 1;
        //    int endIndex = (gridGeneralInfo.PageIndex + 1) * pageSize;
        //    if (endIndex > gridGeneralInfo.VisibleRowCount)
        //    {
        //        endIndex = gridGeneralInfo.VisibleRowCount;
        //    }
        //    return string.Format("Showing {0}-{1} of {2}", startIndex, endIndex, gridGeneralInfo.VisibleRowCount);
        //}
        #endregion


    }
}