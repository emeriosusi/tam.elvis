﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._20MasterData;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;


namespace ELVISDashBoard._20MasterData
{
    public partial class ItemTransaction : BaseCodeBehind
    {
        #region Init
        readonly ItemTransactionLogic myLogic = new ItemTransactionLogic();
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_2001");
        private const string mySelf = "Item Transaction";
        private ArrayList listMsg = new ArrayList();
        private string[] pages = "5,10,15,20,25".Split(',');
        bool IsEditing;
        

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }
        #endregion

        #region Getter Setter for Data List
        private List<ItemTransactionData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<ItemTransactionData>();
                }
                else
                {
                    return (List<ItemTransactionData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            hidScreenID.Value = _ScreenID;

            if (!IsPostBack)
            {
                PrepareLogout();
                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();
                AddEnterComponent(cbTransaction);
                AddEnterComponent(txtItemTransCd);
                AddEnterComponent(txtItemTransDesc);
                AddEnterAsSearch(btnSearch, _ScreenID);
            }
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);

            btnSearch.Visible = true && _RoleLogic.isAllowedAccess("btnSearch");
            btnClear.Visible = true;

            btnAdd.Visible = true && _RoleLogic.isAllowedAccess("btnAdd");
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnExportExcel.Visible = false;

            btnClose.Visible = true;
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoGeneralInfo = gridGeneralInfo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridGeneralInfo.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                //litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
                string _TRCode = Convert.ToString(gridGeneralInfo.GetRowValues(e.VisibleIndex, "TRANSACTION_CD"));
                Literal litTx = gridGeneralInfo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridGeneralInfo.Columns["TRANSACTION_CD"] as GridViewDataColumn, "TransactionLiteral") as Literal;
                litTx.Text = myLogic.TxName(_TRCode.Int(0));
            }
            #endregion
            #region rowtype edit
            if (IsEditing)
            {
                if (e.RowType == GridViewRowType.InlineEdit)
                {
                    #region init
                    TextBox txtGridTransactionCode = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["ITEM_TRANSACTION_CD"] as GridViewDataColumn, "txtGridItemTransCd") as TextBox;
                    TextBox txtGridTransactionDesc = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["ITEM_TRANSACTION_DESC"] as GridViewDataColumn, "txtGridItemTransDesc") as TextBox;
                    ASPxComboBox cbgTransaction = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["TRANSACTION_CD"] as GridViewDataColumn, "cbgTransaction") as ASPxComboBox;
                    ASPxCheckBox cbDPP = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["DPP_FLAG"] as GridViewDataColumn, "cbDPPFlag") as ASPxCheckBox;
                    #endregion
                    #region add
                    if (PageMode == Common.Enum.PageMode.Add)
                    {
                        cbgTransaction.ReadOnly = false;
                        txtGridTransactionCode.Focus();
                    }
                    #endregion
                    #region edit
                    else if (PageMode == Common.Enum.PageMode.Edit)
                    {
                        int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                        if (_VisibleIndex == e.VisibleIndex)
                        {
                            #region fill data edit
                            string _ItemTransactionCode = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "ITEM_TRANSACTION_CD"));
                            string _ItemTransactionDesc = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "ITEM_TRANSACTION_DESC"));
                            string _TxCode = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "TRANSACTION_CD"));
                            string _dpp = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "DPP_FLAG"));
                            
                            txtGridTransactionCode.Text = _ItemTransactionCode; 
                            txtGridTransactionCode.ReadOnly = true;
                            cbgTransaction.Value = _TxCode;
                            cbDPP.Checked = _dpp.Int(0)==1 ;
                            cbgTransaction.ReadOnly = true;

                            txtGridTransactionDesc.Focus();
                            txtGridTransactionDesc.Text = _ItemTransactionDesc;
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            #endregion
        }

        protected void gridGeneralInfo_PageIndexChanged(object sender, EventArgs e)
        {
            //gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
            gridGeneralInfo.DataBind();
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = myLogic.SearchInqury(cbTransaction.Value.str(), txtItemTransCd.Text, txtItemTransDesc.Text);

            e.Result = ListInquiry;
        }

        protected void dsTx_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string query = resx.Combo("TRANSACTION_CD");
            e.Result = logic.Combo.getComboData(query, true, "TRANSACTION_CD");
        }
        protected void dsTxG_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string query = resx.Combo("TRANSACTION_CD");
            e.Result = logic.Combo.getComboData(query, false, "TRANSACTION_CD");
        }


        #endregion
        #endregion

        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.DataSourceID = null;
            gridGeneralInfo.DataBind();
            btnExportExcel.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            cbTransaction.SelectedIndex = 0;
            txtItemTransCd.Text = "";
            txtItemTransDesc.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                lblConfirmationDelete.Text = string.Format(resx.Message( "MSTD00008CON"), "delete");
                ConfirmationDeletePopUp.Show();
            }
            else if (gridGeneralInfo.Selection.Count == 0)
            {
                #region Error
                litMessage.Text = _m.SetMessage(resx.Message( "MSTD00009WRN"), "WRN");
                #endregion
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                List<string> listTransactionCode = new List<string>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {
                        string itCd = "";
                        string txCd = "";
                        itCd = gridGeneralInfo.GetRowValues(i, "ITEM_TRANSACTION_CD").str();
                        txCd = gridGeneralInfo.GetRowValues(i, "TRANSACTION_CD").str();
                        listTransactionCode.Add(txCd + ":" + itCd);
                    }
                }

                if (listTransactionCode.Count > 0)
                {
                    PageMode = Common.Enum.PageMode.View;
                    result = myLogic.Delete(listTransactionCode);
                    if (!result)
                    {
                        litMessage.Text = _m.SetMessage(resx.Message( "MSTD00014ERR"), "ERR");
                        gridGeneralInfo.Selection.UnselectAll();
                    }
                    else
                    {
                        litMessage.Text = _m.SetMessage(String.Format(resx.Message( "MSTD00049INF"), mySelf), "MSTD00049INF");
                        gridGeneralInfo.Selection.UnselectAll();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                    }
                }
                
            }
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            ASPxComboBox cbgTx = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["TRANSACTION_CD"] as GridViewDataColumn, "cbgTransaction") as ASPxComboBox;
            TextBox txtGridTransactionCode = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["ITEM_TRANSACTION_CD"] as GridViewDataColumn, "txtGridItemTransCd") as TextBox;
            TextBox txtGridTransactionName = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["ITEM_TRANSACTION_DESC"] as GridViewDataColumn, "txtGridItemTransDesc") as TextBox;
            ASPxCheckBox cbDPPFlag = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["DPP_FLAG"] as GridViewDataColumn, "cbDPPFlag") as ASPxCheckBox;

            if (PageMode == Common.Enum.PageMode.Add)
            {
                #region Add
                if (ValidateInputSave())
                {
                    ErrorData Err = new ErrorData();
                    //DoStartLog(_globalResource.GetResxObject("Function", "FCN_TRANSACTION_TYPE_MASTER"), "");
                    bool result = myLogic.InsertItemTransaction(
                        cbgTx.Value.str(), 
                        txtGridTransactionCode.Text, 
                        txtGridTransactionName.Text, 
                        (Convert.ToBoolean(cbDPPFlag.Value)) ? 1 : 0, UserData);

                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        litMessage.Text = _m.SetMessage(string.Format(resx.Message( "MSTD00011INF"), mySelf), "MSTD00011INF");
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Log("MSTD00011INF", "INF", "btnSaveDetail_Click", string.Format(resx.Message( "MSTD00011INF"), mySelf));
                    }
                    else
                    {
                        string msg = "", msgCode = "";
                        GetLastErrMessages(myLogic.LastErr, mySelf, txtGridTransactionCode.Text, "Add", ref msg, ref msgCode);
                        litMessage.Text = _m.SetMessage(msg, msgCode);
                        Log(msgCode, "ERR", "btnSaveDetail_Click", msg);

                        // litMessage.Text = _message.SetMessage(string.Format(_globalResource.GetResxObject("Message", "MSTD00010ERR"), mySelf), "MSTD00010ERR");
                        // Log("MSTD00010ERR", "ERR", "btnSaveDetail_Click", string.Format(_globalResource.GetResxObject("Message", "MSTD00010ERR"), mySelf));
                    }
                }
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSave())
                {
                    //DoStartLog(_globalResource.GetResxObject("Function", "FCN_TRANSACTION_TYPE_MASTER"), "");

                    bool result = myLogic.EditItemTransaction(
                        cbgTx.Value.str(), 
                        txtGridTransactionCode.Text, 
                        txtGridTransactionName.Text, 
                        (Convert.ToBoolean(cbDPPFlag.Value)) ? 1 : 0, UserData);

                    if (result == true)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataBind();
                        gridGeneralInfo.Selection.UnselectAll();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        litMessage.Text = _m.SetMessage(string.Format(resx.Message( "MSTD00013INF"), mySelf), "MSTD00013INF");
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                        Log("MSTD00013INF", "INF", "btnSaveDetail_Click", string.Format(resx.Message( "MSTD00013INF"), mySelf));
                    }
                    else
                    {
                        litMessage.Text = _m.SetMessage(string.Format(resx.Message( "MSTD00012ERR"), mySelf), "MSTD00012ERR");
                        Log("MSTD00012ERR", "INF", "btnSaveDetail_Click", string.Format(resx.Message( "MSTD00012ERR"), mySelf));
                    }
                }
                #endregion
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            listMsg = new ArrayList();
            try
            {
                if (ValidateInputSearch())
                {
                    gridGeneralInfo.Selection.UnselectAll();
                    gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                    
                    gridGeneralInfo.DataBind();
                    PageMode = Common.Enum.PageMode.View;
                    SetCancelEditDetail();
                }
            }
            catch (Exception ex)
            {
                listMsg.Add(_m.SetMessage(string.Format(resx.Message( "MSTD00002ERR"), ex.Message), "ERR"));
                LoggingLogic.err(ex);
            }
            foreach (var msg in listMsg)
            {
                litMessage.Text += msg;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.AddNewRow();
            PageMode = Common.Enum.PageMode.Add;
            IsEditing = true;
            SetEditDetail();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            PageMode = Common.Enum.PageMode.Edit;
                            gridGeneralInfo.SettingsBehavior.AllowSort = false;
                            IsEditing = true;
                            Session["visibleIndex"] = i;
                            gridGeneralInfo.StartEdit(i);
                            gridGeneralInfo.Selection.UnselectAll();
                            SetEditDetail();
                            break;
                        }
                    }
                }
                else if (gridGeneralInfo.Selection.Count == 0)
                {
                    litMessage.Text = _m.SetMessage(resx.Message( "MSTD00009WRN"), "MSTD00009WRN");
                }
                else if (gridGeneralInfo.Selection.Count > 0)
                {
                    litMessage.Text = _m.SetMessage(resx.Message( "MSTD00016WRN"), "MSTD00016WRN");
                }
            }
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }

        #region export to excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (ListInquiry != null)
            {
               
                    string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                    UserData userdata = (UserData)Session["UserData"];
                    string _FileName = "ITEM_TRANSACTION_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                    this.Response.Clear();
                    this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.ContentType = "application/vnd.ms-excel";
                    try
                    {
                        this.Response.BinaryWrite(myLogic.Get(UserName, imagePath, ListInquiry));
                    }
                    catch (Exception ex)
                    {
                        litMessage.Text = "Cannot get file: " + ex.Message;
                    }
                    this.Response.End();
               
            }
            else litMessage.Text = _m.Message("MSTD00018INF");
        }
        #endregion

        protected void SetEditDetail()
        {
            btnSaveDetail.Visible = true;
            btnCancelDetail.Visible = true;

            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnClose.Visible = false;

            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridGeneralInfo.SettingsBehavior.AllowSort = false;
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;

            setSearchEnabled(false);

            btnClose.Enabled = false;

        }

        protected void SetCancelEditDetail()
        {
            btnClose.Enabled = true;

            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData Err = null;
                btnEdit.Visible = role.isAllowedAccess("btnEdit", ref Err);
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref Err);
                btnExportExcel.Visible = role.isAllowedAccess("btnExportExcel");
            }
            else
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;
                btnExportExcel.Visible = false;
            }
            setSearchEnabled();
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        }

        #endregion

        #region Function

        private bool ValidateInputSave()
        {
            StringBuilder s = new StringBuilder("");

            //REGION FOR VALIDATING INPUT SAVE
            TextBox txtGridTransactionCode = gridGeneralInfo.FindEditRowCellTemplateControl(gridGeneralInfo.Columns["ITEM_TRANSACTION_CD"] as GridViewDataColumn, "txtGridItemTransCd") as TextBox;
            TextBox txtGridTransactionName = gridGeneralInfo.FindEditRowCellTemplateControl(gridGeneralInfo.Columns["ITEM_TRANSACTION_DESC"] as GridViewDataColumn, "txtGridItemTransDesc") as TextBox;
            
            if (txtGridTransactionCode.Text.Equals(string.Empty))
            {
                s.AppendLine(_m.SetMessage(string.Format(resx.Message( "MSTD00017WRN"), "Item Transaction Code"), "MSTD00017WRN"));
            }

            if (txtGridTransactionName.Text.Equals(string.Empty))
            {
                s.AppendLine(_m.SetMessage(string.Format(resx.Message( "MSTD00017WRN"), "Item Transaction Description"), "MSTD00017WRN"));
            }
            
            if (s.Length > 0)
            {
                litMessage.Text = s.ToString();
            }
            return (s.Length < 1);
        }

        private void setSearchEnabled(bool can = true)
        {
            txtItemTransCd.Enabled = can;
            txtItemTransDesc.Enabled = can;
        }

        private bool ValidateInputSearch()
        {
            Int32 itemTrCode;

            if (!Int32.TryParse(txtItemTransCd.Text, out itemTrCode) && (txtItemTransCd.Text != ""))
            {
                listMsg.Add(_m.SetMessage(string.Format(resx.Message( "MSTD00018WRN"), "Item Transaction Code"), "WRN"));
            }
            
            return listMsg.Count == 0;
        }
        #endregion

        #region paging
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
        }
        #endregion

        protected override void Rebind()
        {
            if (gridGeneralInfo.VisibleRowCount > 0)
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
            gridGeneralInfo.DataBind();
        }
        protected void RecordPerPageSelect_Init(object sender, EventArgs e)
        {
            RecordPerPage_Init(sender, e, gridGeneralInfo);
        }
        protected void RecordPerPageSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecordPerPage_Selected(sender, e, gridGeneralInfo);
        }


    }
}
