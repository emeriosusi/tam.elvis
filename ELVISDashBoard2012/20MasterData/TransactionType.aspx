﻿<%@ Page Title="Transaction Type Master" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="TransactionType.aspx.cs" Inherits="ELVISDashBoard._20MasterData.TransactionType" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="prex" ContentPlaceHolderID="pre" runat="server">
<style type="text/css">
.label 
{
    width: 150px;
    display: inline-block;
    float:left;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Transaction Type Master Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                
                <div class="row">
                    <div class="lefty" style="width:49%">
                        <div class="label">Transaction Code</div>
                        <div class="rowwarphalfLeft">
                            <asp:DropDownList runat="server" ID="ddlTransaction" AutoPostBack="false"/>
                        </div>
                    </div>
                    <div class="righty" style="width:49%">
                        <div class="label">Transaction Level</div>
                        <div class="rowwarphalfLeft">
                            <asp:DropDownList runat="server" ID="ddlTransactionLevel" />
                        </div>
                    </div>
                </div>  
                <div class="row" style="text-align:left;">
                    <div class="lefty" style="width:49%">

                        <div class="label">
                            Standard Wording</div>
                        <div class="rowwarphalfLeft">
                            <asp:TextBox runat="server" ID="txtStdWording" Columns="35"/>
                            
                        </div>

                    </div>
                    <div class="righty" style="width:49%">
                        <div class="label">Group</div>
                        <div class="rowwarphalfLeft">
                            <asp:DropDownList runat="server" ID="ddlGroup" AutoPostBack="false"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" OnClientClick="loading()" />
                    </div>
                </div>
            </div>
            <div class="rowbtn">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click"
                        OnClientClick="loading()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                Settings-VerticalScrollableHeight="320"
                KeyFieldName="TRANSACTION_CD" EnableCallBacks="False"
                OnPageIndexChanged="gridGeneralInfo_PageIndexChanged" 
                Styles-AlternatingRow-CssClass="even">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px">
                        <EditButton Visible="false" Text=" ">
                        </EditButton>
                        <UpdateButton Visible="false" Text=" ">
                        </UpdateButton>
                        <NewButton Visible="false" Text=" ">
                        </NewButton>
                        <CancelButton Visible="false" Text=" ">
                        </CancelButton>
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" 
                                ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); }"
                                CheckState="Unchecked" Style="text-align: center">
                            </dx:ASPxCheckBox>
                        </HeaderTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="Transaction Code" FieldName="TRANSACTION_CD"
                        VisibleIndex="2" Width="80px">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtGridTransactionCode" Width="76px" MaxLength="10">
                            </asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Fitered_txtGridTransactionCode" runat="server" TargetControlID="txtGridTransactionCode"
                                FilterType="Numbers">
                            </cc1:FilteredTextBoxExtender>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Transaction Name" FieldName="TRANSACTION_NAME"
                        Width="220px" VisibleIndex="3" >
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtGridTransactionName" Width="218px" MaxLength="50" />
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left"/>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Caption="Standard Wording" FieldName="STD_WORDING" VisibleIndex="4"
                        Width="250px">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtGridStdWording" Width="248px" MaxLength="100" />
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left"/>
                    </dx:GridViewDataTextColumn>                    

                    <dx:GridViewDataTextColumn Caption="Transaction Level" FieldName="TRANSACTION_LEVEL_CD"
                        Width="200px" VisibleIndex="5" >
                        <CellStyle HorizontalAlign="Left"/>
                        <DataItemTemplate>
                            <asp:Literal ID="TransactionLevelLiteral" runat="server"/>
                        </DataItemTemplate>
                        <EditItemTemplate>
                        <dx:ASPxComboBox runat="server" ID="ddlGridTransactionLevel" Width="196px" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Group" FieldName="GROUP_CD" 
                        Width="80px" VisibleIndex="6">
                        <CellStyle HorizontalAlign="Center"/>
                        <DataItemTemplate>
                            <asp:Literal ID="GroupLiteral" runat="server" />
                        </DataItemTemplate>
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridGroup" Width="76px" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TEMPLATE_CD" 
                        Caption="Upload Template" 
                        VisibleIndex="7" 
                        Width="200px">
                        <CellStyle HorizontalAlign="Left"/>
                        <DataItemTemplate>
                            <asp:Literal ID="TemplateNameLiteral" runat="server" />
                        </DataItemTemplate>
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlUploadTemplate" Width="196px" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataCheckColumn FieldName="CLOSE_FLAG" 
                        Width="55px"
                        Caption="Closed" VisibleIndex="8">
                        <PropertiesCheckEdit 
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" />
                        <EditItemTemplate>
                        <dx:ASPxCheckBox runat="server" ID="CloseCheck"
                            AllowGrayed="true" AllowGrayedByClick="true"
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" 
                        />
                        </EditItemTemplate>
                    </dx:GridViewDataCheckColumn>

                    <dx:GridViewDataCheckColumn FieldName="SERVICE_FLAG" 
                        Width="55px"
                        Caption="Service" VisibleIndex="9">
                        <PropertiesCheckEdit 
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" />
                        <EditItemTemplate>
                        <dx:ASPxCheckBox runat="server" ID="ServiceCheck"
                            AllowGrayed="true" AllowGrayedByClick="true"
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" 

                        />
                        </EditItemTemplate>
                    </dx:GridViewDataCheckColumn>

                    <dx:GridViewDataCheckColumn FieldName="BUDGET_FLAG" 
                        Width="55px"
                        Caption="Budget" VisibleIndex="10">
                       <PropertiesCheckEdit 
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" />
                        
                        <EditItemTemplate>
                        <dx:ASPxCheckBox runat="server" ID="BudgetCheck"
                            AllowGrayed="true" AllowGrayedByClick="true"
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" 
                        
                        />
                        </EditItemTemplate>
                    </dx:GridViewDataCheckColumn>

                    <dx:GridViewDataCheckColumn FieldName="ATTACHMENT_FLAG" 
                        Width="80px"
                        Caption="Attachment" VisibleIndex="11">
                       <PropertiesCheckEdit 
                            
                            ValueType="System.Int32"
                            ValueUnchecked="0"
                            ValueChecked="1"
                            ValueGrayed="" />
                        
                        <EditItemTemplate>
                            <dx:ASPxCheckBox runat="server" ID="AttachCheck"
                                AllowGrayed="true" AllowGrayedByClick="true"
                                ValueType="System.Int32"
                                ValueUnchecked="0"
                                ValueChecked="1"
                                ValueGrayed="" 
                            />
                        </EditItemTemplate>
                    </dx:GridViewDataCheckColumn>

                    <dx:GridViewDataCheckColumn FieldName="PRODUCTION_FLAG" 
                        Width="75px"
                        Caption="Production" VisibleIndex="12">
                       <PropertiesCheckEdit AllowGrayed="true"  AllowGrayedByClick="true"
                            ValueType="System.Int32"                            
                            ValueUnchecked="2"
                            ValueChecked="1"
                            ValueGrayed="0" />                        
                        <EditItemTemplate>
                        <dx:ASPxCheckBox runat="server" ID="ProdCheck" 
                            AllowGrayed="true" AllowGrayedByClick="true" 
                            ValueType="System.Int32"
                            ValueChecked="1" ValueUnchecked="2" ValueGrayed="0"/>
                        </EditItemTemplate>
                    </dx:GridViewDataCheckColumn>


                    <dx:GridViewDataCheckColumn FieldName="COST_CENTER_FLAG"
                        Width="80px" Caption="Cost Center" VisibleIndex="13"
                    >
                       <PropertiesCheckEdit AllowGrayed="true"  AllowGrayedByClick="true"
                            ValueType="System.Int32"                            
                            ValueUnchecked="2"
                            ValueChecked="1"
                            ValueGrayed="0" />                        
                        <EditItemTemplate>
                        <dx:ASPxCheckBox runat="server" ID="CostCenterCheck" 
                            AllowGrayed="true" AllowGrayedByClick="true" 
                            ValueType="System.Int32"
                            ValueChecked="1" ValueUnchecked="2" ValueGrayed="0"/>
                        </EditItemTemplate>
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewBandColumn Caption="Created" VisibleIndex="14">
                        <Columns>
                            <dx:GridViewDataDateColumn Caption="Date" FieldName="CREATED_DT" VisibleIndex="0"
                                ReadOnly="true" Width="150px">
                                <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                </PropertiesDateEdit>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Changed" VisibleIndex="15">
                        <Columns>
                            <dx:GridViewDataDateColumn Caption="Date" FieldName="CHANGED_DT" VisibleIndex="0"
                                ReadOnly="true" Width="150px">
                                <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                </PropertiesDateEdit>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CHANGED_BY" VisibleIndex="1">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                <SettingsEditing Mode="Inline" />
                <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                <SettingsLoadingPanel ImagePosition="Top" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <AlternatingRow CssClass="even">
                    </AlternatingRow>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
                <Settings ShowStatusBar="Visible" />
                <SettingsPager Visible="false" />
                <Templates>
                    <StatusBar>
                        <div style="text-align: left;">
                            Records per page:
                            
                            <asp:DropDownList ID="RecordPerPageSelect" runat="server" ClientIDMode="Static" 
                                OnInit="RecordPerPageSelect_Init"
                                AutoPostBack="true"
                                OnSelectedIndexChanged="RecordPerPageSelect_SelectedIndexChanged"/>
                            
                                &nbsp;
                            <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">
                                &lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a>
                            &nbsp; Page
                            <input type="text" onchange="gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>" style="width: 20px" />
                            of
                            <%# gridGeneralInfo.PageCount%>&nbsp;
                            <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%> 
                            <a title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp;
                            <a title="Last" href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">
                                &gt;&gt;</a> &nbsp;
                        </div>
                    </StatusBar>
                </Templates>
                <SettingsPager AlwaysShowPager="true">
                </SettingsPager>
            </dx:ASPxGridView>
            
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" OnSelecting="LinqDataSource1_Selecting" TableName="vw_TransactionType">
            </asp:LinqDataSource>

            <div class="rowbtn">
                <div class="btnleft">
                <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                    SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                </div>                
                <div class="btnright">
                <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click"
                        OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click"
                        OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()"
                        Visible="false" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
</asp:Content>
