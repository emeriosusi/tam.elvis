﻿<%@ Page Title="Cluster Master Screen" Language="C#" MasterPageFile="~/MasterPage/Base.Master" 
    AutoEventWireup="true" CodeBehind="Cluster.aspx.cs" Inherits="ELVISDashBoard._20MasterData.Cluster" %>

<asp:Content ID="headplace" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" src="../src/jsgrid.min.js" ></script>
<script type="text/javascript" src="../src/notifications.js"></script>
<script type="text/javascript" src="../src/Cluster.js" ></script>
<link type="text/css" rel="stylesheet" href="../src/jsgrid.min.css" />
<link type="text/css" rel="stylesheet" href="../src/jsgrid-theme.min.css" />
<link type="text/css" rel="stylesheet" href="../src/Cluster.css" />
<link type="text/css" rel="Stylesheet" href="../src/notifications.css" />

</asp:Content>

<asp:Content ID="torso" ContentPlaceHolderID="torso" runat="server">
    
    <div class="tab">
        <button id="clusterButton" class="tablinks" data-show="cluster" type="button">Cluster</button>
        <button id="transactionButton" class="tablinks" data-show="transaction" type="button">Transaction</button>        
    </div>
    <div id="gwrapper">
        <div id="tabcluster" class="tabcontent">    
            <div id="gcluster" class="grid"></div>
        </div>
        <div id="tabtransaction" class="tabcontent">
            <div id="gtransaction" class="grid"></div>            
        </div>        
        
    </div>

    <div class="longbottom">
        <button id="DownloadButton" type="button" class="shortButton">Download</button>
        <button id="SaveButton" type="button" class="shortButton">Save</button>        
        <button id="CloseButton" type="button" class="shortButton">Close</button>        
    </div>
</asp:Content>

<asp:Content ID="postContent" ContentPlaceHolderID="foot" runat="server">
<input type="hidden" id="hidPageTitle" value="Cluster Approval Master Screen" />
<input type="hidden" id="hidscreenID" value="ELVIS_Screen_2008" />
</asp:Content>