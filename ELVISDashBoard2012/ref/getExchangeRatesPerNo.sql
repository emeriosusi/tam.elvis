-- getExchangeRatesPerNo

SELECT 
	  CURRENCY_CD as [CurrencyCode],  
	  EXCHANGE_RATE as [Rate]
FROM  TB_R_PVRV_AMOUNT
WHERE DOC_NO = @anum 
  AND DOC_YEAR = @ayear 
