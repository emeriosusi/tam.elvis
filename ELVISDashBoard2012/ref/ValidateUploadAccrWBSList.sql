﻿-- Check duplicate for booking no and wbs no old
UPDATE tauw
SET tauw.ERRMSG = tauw.ERRMSG + 'NO:' + CAST(tauw.SEQ_NO AS VARCHAR) + ' Duplication data found for Booking No. = ' + tauw.BOOKING_NO + ' & WBS No = ' + tauw.WBS_NO_OLD + ';'
FROM  TB_T_ACCR_UPLOAD_WBS tauw INNER JOIN (
    SELECT BOOKING_NO, WBS_NO_OLD
      FROM TB_T_ACCR_UPLOAD_WBS WHERE PROCESS_ID = @processId
    GROUP BY BOOKING_NO, WBS_NO_OLD
    HAVING COUNT(*) > 1
  ) duplicate ON tauw.BOOKING_NO = duplicate.BOOKING_NO AND tauw.WBS_NO_OLD = duplicate.WBS_NO_OLD;


-- Check Booking No and WBS No Old is exist
UPDATE tauw
SET tauw.ERRMSG = tauw.ERRMSG + 'NO:' + CAST(tauw.SEQ_NO AS VARCHAR) + ' Old WBS No. not exist;'
FROM TB_T_ACCR_UPLOAD_WBS tauw
  LEFT JOIN (SELECT a.BOOKING_NO,a.WBS_NO_OLD,b.ACCRUED_NO FROM TB_R_ACCR_BALANCE a
              INNER JOIN TB_R_ACCR_LIST_D b ON a.BOOKING_NO = b.BOOKING_NO) rab
    ON tauw.BOOKING_NO = rab.BOOKING_NO
       AND tauw.WBS_NO_OLD = rab.WBS_NO_OLD
       AND rab.ACCRUED_NO = @accrNo
  WHERE tauw.PROCESS_ID = @processId
    AND rab.BOOKING_NO is NULL;
