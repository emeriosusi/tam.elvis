-- SetCluster

/*DECLARE @stype varchar(50)='CLUSTER', @id int = '89', @name varchar(50)=null, @text varchar(500) =null, @user varchar(50)=null, @act int = 1*/

DECLARE
    @@stype VARCHAR(50) ,
    @@id INT ,
    @@name VARCHAR(50) ,
    @@text VARCHAR(500) ,
    @@user VARCHAR(50) ,
    @@act INT 

SELECT @@stype = NULLIF(@stype, '')
, @@id = case when isnumeric(x.id)=1 then convert(int, x.id) else null end
, @@name = NULLIF(@name, '')
, @@text = NULLIF(@text, '') 
, @@user = ISNULL(@user, '') 
, @@act = NULLIF(@act, 0) 
FROM (SELECT @id ID ) X;

IF (EXISTS(SELECT 1 FROM TB_M_SYSTEM WHERE SYSTEM_TYPE = @@stype AND SYSTEM_VALUE_NUM = @@id))
BEGIN
    IF @@act IS NULL
    BEGIN
		UPDATE s
            SET SYSTEM_VALUE_TXT = @@text,
                SYSTEM_CD = @@name,
                CHANGED_BY = @@user,
                CHANGED_DT = GETDATE()
        FROM TB_M_SYSTEM s
        WHERE (@@stype IS NOT NULL AND s.SYSTEM_TYPE = @@stype)
        AND (@@id IS NOT NULL AND s.SYSTEM_VALUE_NUM = @@id)		
		AND @@act IS NULL 
    END
    ELSE
    BEGIN
        IF (@@act != 0 AND @@id IS NOT NULL and @@stype IS NOT NULL)
        BEGIN
            DELETE FROM TB_M_SYSTEM WHERE SYSTEM_TYPE = @@stype AND SYSTEM_VALUE_NUM = @@id
        END;
    END
END ELSE
BEGIN
	INSERT INTO TB_M_SYSTEM (SYSTEM_TYPE, SYSTEM_VALUE_NUM, SYSTEM_CD,  SYSTEM_VALUE_TXT, CREATED_BY, CREATED_DT)
	SELECT @@stype SYSTEM_TYPE, @@id SYSTEM_VALUE_NUM, @@name SYSTEM_CD, @@text SYSTEM_VALUE_TXT, @@user CREATED_BY, GETDATE() CREATED_DT 
	WHERE @@act IS NULL AND @@stype is not null and @@id is not null and @@text is not null ;    
END;
