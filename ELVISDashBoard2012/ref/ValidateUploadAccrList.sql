﻿--Checking PV
UPDATE temp
  SET temp.ERRMSG = temp.ERRMSG + 'NO:' + CAST(temp.SEQ_NO AS VARCHAR) + ' PV Type not exist in Master data;'
FROM (SELECT * FROM TB_T_ACCR_UPLOAD_LIST WHERE PROCESS_ID = @processId ) temp
LEFT JOIN TB_M_ACCR_PV_TYPE mapt
ON lower(mapt.PV_TYPE_NAME) = lower(temp.PV_TYPE_NAME)
WHERE mapt.PV_TYPE_CD IS NULL

--Checking Cost Center
UPDATE temp
SET temp.ERRMSG = temp.ERRMSG + 'NO:' + CAST(temp.SEQ_NO AS VARCHAR) + ' Cost Center not exist in master data;'
FROM (SELECT * FROM TB_T_ACCR_UPLOAD_LIST WHERE PROCESS_ID = @processId ) temp
  LEFT JOIN vw_CostCenter vcc
    ON vcc.COST_CENTER = temp.COST_CENTER
WHERE vcc.COST_CENTER IS NULL

--Checking Currency
UPDATE temp
SET temp.ERRMSG = temp.ERRMSG + 'NO:' + CAST(temp.SEQ_NO AS VARCHAR) + ' Currency not exist in master data;'
FROM (SELECT * FROM TB_T_ACCR_UPLOAD_LIST WHERE PROCESS_ID = @processId ) temp
  LEFT JOIN TB_M_CURRENCY mc
    ON mc.CURRENCY_CD = temp.CURRENCY_CD
WHERE mc.CURRENCY_CD IS NULL

--Checking Suspense No
UPDATE temp
SET temp.ERRMSG = temp.ERRMSG + 'NO:' + CAST(temp.SEQ_NO AS VARCHAR) + ' Suspense No is not valid;'
FROM (SELECT * FROM TB_T_ACCR_UPLOAD_LIST WHERE PROCESS_ID = @processId AND SUSPENSE_NO_OLD IS NULL ) temp
  left join TB_R_PV_H h on temp.SUSPENSE_NO_OLD = h.PV_NO AND h.PV_TYPE_CD = 2
WHERE h.PV_NO is null

--Checking Suspense No Duplicate
UPDATE temp
SET temp.ERRMSG = temp.ERRMSG + 'NO:' + CAST(temp.SEQ_NO AS VARCHAR) + ' Suspense No is Duplicate;'
FROM (SELECT * FROM TB_T_ACCR_UPLOAD_LIST WHERE PROCESS_ID = @processId ) temp
  inner join (
               SELECT SUSPENSE_NO_OLD
               FROM TB_T_ACCR_UPLOAD_LIST WHERE SUSPENSE_NO_OLD is NOT NULL AND PROCESS_ID = @processId
               GROUP BY SUSPENSE_NO_OLD
               HAVING COUNT(*) > 1
             ) oc on temp.SUSPENSE_NO_OLD = oc.SUSPENSE_NO_OLD

--Checking WBS master
UPDATE temp
SET temp.ERRMSG = temp.ERRMSG + 'NO:' + CAST(temp.SEQ_NO AS VARCHAR) + ' No ' + temp.WBS_NO_OLD + ' WBS NO is not valid;'
FROM (SELECT * FROM TB_T_ACCR_UPLOAD_LIST WHERE PROCESS_ID = @processId ) temp
  LEFT JOIN vw_WBS vw
    ON vw.WbsNumber = temp.WBS_NO_OLD
		AND vw.Year = @fiscalYear
WHERE vw.WbsNumber IS NULL

--Checking WBS Division
UPDATE temp
SET temp.ERRMSG = temp.ERRMSG + 'NO:' + CAST(temp.SEQ_NO AS VARCHAR) + ' No ' + temp.WBS_NO_OLD + ' WBS No is not same division;'
FROM (SELECT * FROM TB_T_ACCR_UPLOAD_LIST WHERE PROCESS_ID = @processId ) temp
  LEFT JOIN (SELECT * FROM vw_WBS WHERE Division = @divId) vw
    ON vw.WbsNumber = temp.WBS_NO_OLD
WHERE vw.WbsNumber IS NULL