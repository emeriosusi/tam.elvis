-- OriginalApprover

SELECT TOP 1 ad.USERNAME 

FROM WORKFLOW_DB.dbo.TB_R_APPROVAL_DETAIL ad 

  JOIN ELVIS_DB.dbo.TB_M_USER u 
    ON u.USERNAME = ad.USERNAME 

  JOIN WORKFLOW_DB.dbo.TB_R_STATUS s 
    ON s.CLASS = ad.CLASS 
   AND ad.APPROVAL_LEVEL = s.APPROVAL_LEVEL 
   AND ad.MODULE_CODE = s.MODULE_CODE 

 WHERE s.APP_ID = 'ELVIS' 
   AND ad.APP_ID = 'ELVIS' 
   AND s.MODULE_CODE =  @moduleCode
   AND ad.MODULE_CODE =  @moduleCode
   AND s. [ACTION] = 1 
   AND s.STATUS_CD =  @statusCd
   AND ad.JOB_PIC = dbo.fn_JobPicFromTx(@tt)

