-- WorklistAccruedShifting
SELECT 
    w.SHIFTING_NO AS [Folio], 
    w.SHIFTING_NO AS [DocNumber], 
    w.BUDGET_NO AS [DocYear], 
    w.CREATED_DT AS [DocDate], 
    w.SHIFTING_NO
     + (CASE WHEN (w.NO_OF_NOTICE > 0) THEN '(' + CONVERT(VARCHAR(10), w.NO_OF_NOTICE) + ')' ELSE '' END) 
     + (CASE WHEN w.NEED_REPLY > 0 THEN '*' ELSE '' END) AS [HypValue], 
    w.TRANSACTION_NAME AS [Description], 
    w.TOTAL_AMT AS [TotalAmount], 
    w.NO_OF_NOTICE as [Notices], 
    w.RECEIVED_DATE AS [PaidDate], 
    w.HOLD_FLAG AS [HoldFlag], 
    w.NEED_REPLY AS [NeedReply], 
    w.DIVISION_NAME AS [DivName], 
    w.REJECTED AS [Rejected], 
    w.STATUS_CD, 
    w.status_name as [StatusName], 
    null AS [PvTypeName], 
    --w.accr_type_name AS [PvTypeName], fid.pras
    CONVERT(VARCHAR(20), w.ACTIVITY_DATE_FROM, 106) AS [ActivityDateFrom], 
    CONVERT(VARCHAR(20), w.ACTIVITY_DATE_TO, 106) AS [ActivityDateTo], 
    null as [PayMethodName], 
    --w.PAY_METHOD_NAME as [PayMethodName], fid.pras
    w.BUDGET_NO as [BudgetNo], 
    CONVERT(BIT, CASE WHEN w.NO_NOTICE_WORKLIST > 0 THEN 1 ELSE 0 END) AS [WorklistNotice],
	w.TRANSACTION_CD as [TransCd] /* FID.Ridwan 11072018 */  
FROM VW_SHIFTING_WORKLIST_DESCRIPTION w 
WHERE w.SHIFTING_NO IN (
{0}
)