-- GetAnnouncement 

SELECT i.INFORMATION_CD,
       i.INFORMATION_TITLE,
       i.INFORMATION_DESC,
       i.DIVISION_LIST,
       i.POSITION_LIST,
       i.RELEASE_DATE,
       i.EXPIRE_DATE,
       i.CREATED_BY,
       i.CREATED_DT,
       i.CHANGED_BY,
       i.CHANGED_DT
FROM TB_M_INFORMATION i 
JOIN (
	SELECT INFORMATION_CD
	FROM TB_M_INFORMATION
	JOIN (select distinct items DIV_CD FROM dbo.fn_explode(';', @divisions) X) D 
	  ON (charindex( ';' + d.DIV_CD + ';', ';' + DIVISION_LIST + ';') > 0)
	  OR (NULLIF(DIVISION_LIST,'') IS NULL) 
	  
	JOIN (select distinct items POSIT FROM dbo.fn_explode(';', @positions) y) P 
	  on charindex(';' + p.POSIT +';' , ';' + POSITION_LIST + ';') > 0
	  OR NULLIF(POSITION_LIST, '') IS NULL

	WHERE GETDATE() BETWEEN RELEASE_DATE AND EXPIRE_DATE 
	GROUP BY INFORMATION_CD
) J ON J.INFORMATION_CD = I.INFORMATION_CD
ORDER BY I.INFORMATION_CD DESC 

