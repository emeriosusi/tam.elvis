-- GetInvoiceValue
SELECT
    CURRENCY_CD [CURRENCY],
    AMOUNT,
    INVOICE_DATE,
    INVOICE_NO,
    TAX_AMOUNT,
    TAX_NO
FROM vw_PVCoverInvoiceList
WHERE PV_NO = @p1 and PV_YEAR = @p2