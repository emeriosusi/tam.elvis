-- GetNoticeRecipient 

SELECT d.PROCCED_BY
FROM TB_R_DISTRIBUTION_STATUS d
WHERE REFF_NO = dbo.fn_DocNoYearToReff(@no, @year)
AND STATUS_CD <= (
    SELECT TOP 1 STATUS_CD FROM TB_R_DISTRIBUTION_STATUS WHERE REFF_NO = dbo.fn_DocNoYearToReff(@no, @year)
    AND PROCCED_BY = ISNULL(@to,@from)
    order by status_cd
)
AND d.STATUS_CD not in (select STATUS_CD from dbo.vw_ExcludedStatusNotice)
order by d.STATUS_CD