-- getDocumentStatus
SELECT STATUS_CD as [Code]
	 , STATUS_NAME as [Description]
  FROM TB_M_STATUS
 WHERE (MODULE_CD = @module AND STATUS_CD >= 0) 
    OR (STATUS_CD = 0) 
	OR (MODULE_CD = 0)
