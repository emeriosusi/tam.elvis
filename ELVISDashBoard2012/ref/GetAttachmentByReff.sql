-- GetAttachmentsByReff

SELECT 
[Blank] , [Url] , [SequenceNumber] , [ReferenceNumber] , [CategoryCode] , [CategoryName] , [PATH] , [FileName] , [DisplayFilename] , [Description] , [CreatedBy] 
FROM (
	select
          0 [Blank]
		, '' [Url] 
		, ROW_NUMBER() OVER(ORDER BY (SELECT 1)) [SequenceNumber]
        , a.REFERENCE_NO [ReferenceNumber]
        , a.ATTACH_CD [CategoryCode]
		, ISNULL(ac.ATTACH_NAME,'') [CategoryName]
        , a.[DIRECTORY] [PATH]
		, a.[FILE_NAME] [FileName]
        , case when len(rtrim(a.[FILE_NAME])) > 10 THEN 
			SUBSTRING(a.[FILE_NAME], 1, 6) + '...' + SUBSTRING(rtrim(a.[FILE_NAME]), LEN(rtrim(a.[FILE_NAME]))-4, 5) 
		  ELSE A.[FILE_NAME] END  [DisplayFilename]
		, a.[DESCRIPTION] [Description]
        , a.CREATED_BY [CreatedBy]               
  from TB_R_ATTACHMENT A
  LEFT JOIN (SELECT ATTACH_CD, ATTACH_NAME FROM TB_M_ATTACH_CATEGORY GROUP BY ATTACH_CD, ATTACH_NAME) ac ON ac.ATTACH_CD = A.ATTACH_CD
  where REFERENCE_NO = @REFERENCE_NO 
) X 