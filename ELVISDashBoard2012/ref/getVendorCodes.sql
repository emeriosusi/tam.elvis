-- getVendorCodes 
SELECT 
 t.VENDOR_CD,
 t.VENDOR_NAME,
 t.VENDOR_GROUP_CD,
 t.SEARCH_TERMS,
 t.DIVISION_ID						 
from .vw_Vendor t						 
where (@EXCLUDE_EXTERNAL_VENDOR <> '1') 
OR (t.VENDOR_GROUP_CD <> '3')