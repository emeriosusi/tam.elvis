﻿UPDATE  a 
SET
  a.STATUS_CD = 105
  FROM TB_R_ACCR_LIST_H a 
WHERE
  a.ACCRUED_NO = @accrNo AND
  NOT EXISTS(

      SELECT
        trab.BOOKING_NO,
        trab.WBS_NO_OLD,
        INIT_AMT,
        AMOUNT_IDR,
        traw.AVAILABLE_AMT
      FROM
        TB_R_ACCR_BALANCE trab
        INNER JOIN TB_R_ACCR_LIST_D trald
          ON (trab.BOOKING_NO = trald.BOOKING_NO AND
              trald.PV_TYPE_CD = 4 AND
              trald.ACCRUED_NO = a.ACCRUED_NO)
        INNER JOIN TB_R_ACCR_WBS traw
          ON (trab.WBS_NO_OLD = WBS_NO)
      WHERE
        trab.PV_TYPE_CD = 4
        AND
        trab.INIT_AMT <> traw.AVAILABLE_AMT
  );
