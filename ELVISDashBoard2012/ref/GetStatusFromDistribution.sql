-- GetStatusFromDistribution

--declare 
--	@reffNo NUMERIC(20) = 0, 
--	@username VARCHAR(20) = '';
	
with ds as 
(
	SELECT REFF_NO,
		   STATUS_CD,
		   PIC,
		   PLAN_DT,
		   ACTUAL_DT,
		   ACTUAL_LT,
		   ACTUAL_UOM,
		   SLA_UNIT,
		   SLA_UOM,
		   PROCCED_BY,
		   CLASS,
		   SKIP_FLAG
	FROM dbo.TB_R_DISTRIBUTION_STATUS
	where REFF_NO = @reffNo 
) 
select  
	-- z.z, m.STATUS_CD MSTATUS,  u.STATUS_CD USTATUS, 
	CASE WHEN ISNULL(m.STATUS_CD, 0) > 0 
	     THEN m.STATUS_CD 
	     ELSE u.STATUS_CD 
	END as STATUS_CD  
FROM 
	(SELECT 1 Z ) AS Z 
JOIN
	(SELECT MIN(STATUS_CD) STATUS_CD FROM ds WHERE PROCCED_BY = @username) u 
	ON 1 = 1 
JOIN 
	(SELECT
 		 MAX(ds.STATUS_CD) STATUS_CD 
	FROM ds 
	JOIN (SELECT MIN(STATUS_CD) STATUS_CD FROM ds WHERE PROCCED_BY = @username) x ON 1 = 1 
	WHERE ds.STATUS_CD < x.STATUS_CD) m
	ON 1 = 1 
