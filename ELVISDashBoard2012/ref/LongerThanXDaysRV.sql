SELECT p.DIVISION_ID PicDiv
       , vp.DIVISION_NAME DivNamePic
       , dbo.fn_GetUserFullName(x.PIC) as [PIC]      
       , l.POSITION_NAME       
       , VH.DIVISION_NAME DivNameDoc
	   , DATEDIFF(DAY, MODIFIED, GETDATE()) DOC_AGE 
       , x.MODIFIED
	   , dbo.fn_GetUserFullname(x.MODIFIED_BY) as MODIFIED_BY
       , x.DOC_NO
       , x.STATUS_NAME
       , X.[Description]
       , x.TOTAL_AMOUNT
       , X.TRANSACTION_NAME
	   , x.VENDOR_NAME
FROM (SELECT h.DIVISION_ID
             , ISNULL (h.NEXT_APPROVER,ISNULL (h.CREATED_BY,h.CHANGED_BY)) PIC
             , ISNULL (h.CHANGED_DATE,h.CREATED_DATE) MODIFIED
			 , ISNULL (h.CHANGED_BY, h.CREATED_BY) MODIFIED_BY
             , RV_NO DOC_NO
             , s.STATUS_NAME
             , h.TOTAL_AMOUNT
             , t.TRANSACTION_NAME
             , (SELECT TOP 1 d. [DESCRIPTION]
                FROM TB_R_RV_D d
                WHERE d.RV_NO = h.RV_NO) AS [Description]
			 , v.VENDOR_NAME
      FROM TB_R_RV_H h
        LEFT JOIN TB_M_STATUS s ON s.STATUS_CD = h.STATUS_CD
        LEFT JOIN TB_M_TRANSACTION_TYPE t ON t.TRANSACTION_CD = h.TRANSACTION_CD
		LEFT JOIN vw_Vendor v ON v.VENDOR_CD = h.VENDOR_CD
      WHERE CHANGED_DATE < GETDATE () - @delay
	  AND   h.RV_DATE BETWEEN '@from' AND '@to'
	  AND   h.DIVISION_ID IN (@issuingDivision)	  
      AND   h.STATUS_CD NOT IN (29, 99)) AS X
  LEFT JOIN TMMIN_ROLE.dbo.TB_M_POSITION p ON p.USERNAME = x.PIC
  LEFT JOIN TMMIN_ROLE.dbo.TB_M_POSITION_LEVEL l ON p.POSITION_ID = l.POSITION_ID
  LEFT JOIN vw_Division vp ON vp.DIVISION_ID = CONVERT (varchar (10),p.DIVISION_ID)
  LEFT JOIN vw_Division vh ON vh.DIVISION_ID = CONVERT (varchar (10),x.DIVISION_ID)
ORDER BY (CASE WHEN ISNUMERIC (p.DIVISION_ID) = 1 THEN CONVERT (INT,P.division_id) ELSE 0 END)
         , l.CLASS
         , x.PIC
         , x.DIVISION_ID
         , x.MODIFIED
