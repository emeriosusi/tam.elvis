﻿using System;
using System.IO;
using System.Web;
using System.Net;
using System.Web.SessionState;
using BusinessLogic;

namespace ELVISDashBoard
{
    public class RefHandler : IHttpHandler, IRequiresSessionState
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return false; }
        }

        

        public void ProcessRequest(HttpContext context)
        {
            //write your handler implementation here.
            if (context.Session == null)
            {
                context.Response.Clear();
                context.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                
                return;
            }
            string ext = context.Request.RawUrl;
            int ldot = ext.LastIndexOf(".");
            
            if (ldot > 0)
            {
                ext = ext.Substring(ldot, ext.Length - ldot);
            }
            
            if (!(ext.Equals(".css") || ext.Equals(".js")))
            {
                context.Response.Clear();

                context.Response.StatusCode = (int) HttpStatusCode.NotAcceptable;
                
                return;
            }
            string xfile = context.Server.MapPath(context.Request.RawUrl);
            if (!File.Exists(xfile))
            {
                context.Response.Clear();
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                return;
            }
            context.Response.TransmitFile(xfile);            
        }

        #endregion
    }
}
