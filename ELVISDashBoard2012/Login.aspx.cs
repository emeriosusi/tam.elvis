﻿using System;
using System.Threading;
using System.Web.UI;
using BusinessLogic;
using Common.Data;
using ELVISDashBoard.presentationHelper;
using Common;
using System.Collections.Generic;

namespace ELVISDashBoard
{
    public partial class LoginPage : BaseCodeBehind
    {
        private static string _ScreenID = "ELVIS_Login";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidScreenID.Value = resx.Screen("ELVIS_Screen_0001");
                litMessage.Text = "";
                txtUserID.Focus();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string _loc = "btnLogin_Click";
            try
            {
                bool isDebug = false;
#if DEBUG 
                isDebug = true;
#endif 
                isDebug = true;
                BusinessLogic.LdapAuthentication ADAuth = new LdapAuthentication();
                if (isDebug || ADAuth.IsAuthenticated(txtUserID.Text.Trim(), txtPassword.Text.Trim()))
                {
                    string SESSION_ID = string.Empty;
                    UserData userData = logic.User.LogIn(txtUserID.Text.Trim(), out SESSION_ID);

                    if (userData != null)
                    {
                        Session["SESSION_ID"] = SESSION_ID;
                        Session["userData"] = userData;
                        Session["user.role"] = logic.Role.getUserRoleHeader(userData.USERNAME);
                        Session["User.MenuList"] = null;
                        Heap.Set<List<WorklistData>>("Worklist." + userData.USERNAME, null);
                        int _processId = 0;
                        _processId = logic.Activity.Log("INF", Session.SessionID + " " + SESSION_ID, _loc, userData.USERNAME, "", 0, Request.UserHostAddress);
                        Thread.Sleep(500);

                        if (!ClientScript.IsStartupScriptRegistered("openWinLogin"))
                        {
                            #region logging login
                            
                            string _errMsg = string.Format(resx.Message( "MSTD00004INF"), userData.USERNAME);
                            _processId = logic.Activity.Log("MSTD00004INF", _errMsg, _loc, userData.USERNAME, "", _processId, Request.UserHostAddress);
                            
                            #endregion
                            ProcessID = _processId;                            
                            Page.ClientScript.RegisterStartupScript(this.GetType(),
                                "openWinLogin", "openWinLogin('Home.aspx','Home');", true);
                        }
                    }
                    else
                    {
                        Session["userData"] = null;
                        litMessage.Text = resx.Message( "MSTD00007ERR");
                        log.Log("MSTD00007ERR", "Not Registered " + txtUserID.Text, _loc, "?");
                    }
                }
                else
                {
                    litMessage.Text = resx.Message( "MSTD00006ERR");
                    log.Log("MSTD00006ERR", "Authentication failed " + txtUserID.Text, _loc, "?");
                }
            }
            catch (Exception ex)
            {
                litMessage.Text = (ex.InnerException != null && ex.InnerException.ToString() != "") ? ex.InnerException.ToString() : ex.Message;
                // LoggingLogic.err(ex);
                log.Log("MSTD00002ERR", "Login fail: " + txtUserID.Text + "\r\n" + ex.Message, _loc, "?");
            }
        }

        protected void LogoutButton_Click(object sender, EventArgs e)
        {
            if (UserName != null)
            {
                logic.Say("LogoutButton_Click", "{0} Log Out Success", UserName);
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();
                Response.Cookies.Clear();
            }
        }

    }
}
