﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common.Function;
using Common.Data;
using System.Configuration;
using System.Web.Security;
namespace ELVISDashboard
{
    public partial class urlHandler : System.Web.UI.Page
    {
        private string _destinationUrl
        {
            set { Session[Common.AppSetting.NameSessionURL] = value; }
            get
            {
                if (Session[Common.AppSetting.NameSessionURL] == null) return "";
                else return Convert.ToString(Session[Common.AppSetting.NameSessionURL]);
            }
        }
        private string _destinationUrlPageName
        {
            set { Session[Common.AppSetting.NameSessionPageNameURL] = value; }
            get
            {
                if (Session[Common.AppSetting.NameSessionPageNameURL] == null) return "";
                else return Convert.ToString(Session[Common.AppSetting.NameSessionPageNameURL]);
            }
        }
        private string _SESSION_ID
        {
            set { Session["SESSION_ID"] = value; }
            get
            {
                if (Session["SESSION_ID"] == null) return "";
                else return Convert.ToString(Session["SESSION_ID"]);
            }
        }
        private UserData _UserData
        {
            get
            {
                UserData temp = Session["userData"] as UserData; 
                string SESSION_ID = Convert.ToString(Session["SESSION_ID"]);
                if (temp != null && new BusinessLogic.UserLogic().isValidSessionLogin(temp.USERNAME, SESSION_ID))
                {
                    return temp;
                }
                else
                {
                    return null;
                }
            }
            set { Session["userData"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            FormsAuthentication.SetAuthCookie("First", false);
            if (!IsPostBack)
            {
                string ParamName = Request.QueryString["PARAM"];
                string ParamValue = Request.QueryString["VALUE"];
                string Page = Request.QueryString["PAGE"];
                string PageName = Request.QueryString["PAGE_NAME"];
                string[] ParamNameList = null;
                string[] ParamValueList = null;
                if (Page != null && Page != "")
                {
                    Page = Server.UrlDecode(Page.Trim()).Trim();
                    Page = Page.Replace(" ", "+");
                    Page = Common.Function.Crypto.DecryptStringAES(Page, Common.AppSetting.PublicKey);
                    PageName = Server.UrlDecode(PageName.Trim()).Trim();
                    PageName = PageName.Replace(" ", "+");
                    PageName = Common.Function.Crypto.DecryptStringAES(PageName, Common.AppSetting.PublicKey);
                    if (ParamName != null)
                    {
                        #region Param
                        ParamName = Server.UrlDecode(ParamName.Trim()).Trim();
                        ParamName = ParamName.Replace(" ", "+");
                        ParamName = Common.Function.Crypto.DecryptStringAES(ParamName, Common.AppSetting.PublicKey);
                        ParamNameList = ParamName.Split('|');
                        #endregion
                    }
                    if (ParamValue != null)
                    {
                        #region Value
                        ParamValue = Server.UrlDecode(ParamValue.Trim()).Trim();
                        ParamValue = ParamValue.Replace(" ", "+");
                        ParamValue = Common.Function.Crypto.DecryptStringAES(ParamValue, Common.AppSetting.PublicKey);
                        ParamValueList = ParamValue.Split('|');
                        #endregion
                    }
                    string destinationURL = "";
                    #region DestinationURL
                    destinationURL = ResolveClientUrl(Page);
                    if (ParamNameList != null)
                    {
                        destinationURL = destinationURL + "?";
                        for (int i = 0; i < ParamNameList.Count(); i++)
                        {
                            destinationURL = destinationURL + ParamNameList[i];
                            destinationURL = destinationURL + "=";
                            if (ParamValueList != null && ParamValueList.Count() >= i)
                            {
                                destinationURL = destinationURL + ParamValueList[i];
                            }
                            destinationURL = destinationURL + "&";
                        }
                        //remove last &
                        destinationURL = destinationURL.Substring(0, destinationURL.Length - 1);
                    }
                    #endregion
                    _destinationUrl = destinationURL;
                    _destinationUrlPageName = PageName;
                    if (_UserData != null)
                    {
                        //Response.Redirect(_destinationUrl);
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(),
                                    "openWinLogin", "openWinLogin('" + _destinationUrl + "','" + _destinationUrlPageName + "');", true);
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "closeLogin", "closeLogin();", true);
                    }
                    else if (Request.QueryString["SESSION_ID"] != null && Request.QueryString["SESSION_ID"] != "")
                    {
                        BusinessLogic.UserLogic UserLogic = new BusinessLogic.UserLogic();
                        string UsrName = UserLogic.GetUserLoginBySessionID(Request.QueryString["SESSION_ID"]);
                        if (UsrName != "")
                        {
                            string NewSessionID = "";
                            UserData usrData = UserLogic.LogIn(UsrName, out NewSessionID);
                            _UserData = usrData;
                            _SESSION_ID = NewSessionID;
                            //Response.Redirect(_destinationUrl);
                            FormsAuthentication.SetAuthCookie(UsrName, false);
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(),
                                    "openWinLogin", "openWinLogin('" + _destinationUrl + "','" + _destinationUrlPageName + "');", true);
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "closeLogin", "closeLogin();", true);
                        }
                        else
                        {
                            Response.Redirect("~/Default.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    string PARAM = Request.QueryString["PARAM"];
                    string VAL = Request.QueryString["VAL"];

                    PARAM = Server.UrlDecode(PARAM);
                    PARAM = PARAM.Replace(" ", "+");
                    PARAM = Common.Function.Crypto.DecryptStringAES(PARAM, ConfigurationManager.AppSettings["SSO_ENCRYPTION_KEY"]);

                    VAL = Server.UrlDecode(VAL);
                    VAL = VAL.Replace(" ", "+");
                    VAL = Common.Function.Crypto.DecryptStringAES(VAL, ConfigurationManager.AppSettings["SSO_ENCRYPTION_KEY"]);

                    string[] ParamArr = PARAM.Split('|');
                    string[] ValArr = VAL.Split('|');

                    string USERNAME = "";
                    string KEY = "";
                    string TOKEN = "";

                    if (ParamArr != null && ParamArr.Count() > 0)
                    {
                        for (int i = 0; i < ParamArr.Count(); i++)
                        {
                            if (ParamArr[i].ToUpper() == ConfigurationManager.AppSettings["SSO_PARAM_USERNAME"].ToUpper())
                            {
                                if (ValArr != null && ValArr.Count() > i && ValArr[i] != null)
                                {
                                    USERNAME = ValArr[i];
                                }
                            }
                            else if (ParamArr[i].ToUpper() == ConfigurationManager.AppSettings["SSO_PARAM_KEY"].ToUpper())
                            {
                                if (ValArr != null && ValArr.Count() > i && ValArr[i] != null)
                                {
                                    KEY = ValArr[i];
                                }
                            }
                            else if (ParamArr[i].ToUpper() == ConfigurationManager.AppSettings["SSO_PARAM_TOKEN"].ToUpper())
                            {
                                if (ValArr != null && ValArr.Count() > i && ValArr[i] != null)
                                {
                                    TOKEN = ValArr[i];
                                }
                            }
                        }

                        if (new BusinessLogic.TokenLogic().isValidToken(KEY, TOKEN))
                        {
                            string SESSION_ID;
                            UserData userData = new BusinessLogic.UserLogic().LogIn(USERNAME, out SESSION_ID);
                            if (userData != null)
                            {
                                FormsAuthentication.SetAuthCookie(USERNAME, false);
                                Session["SESSION_ID"] = SESSION_ID;
                                Session["userData"] = userData;
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(),
                                        "openWinLogin", "openWinLogin('" + ResolveClientUrl("~/Home.aspx") + "','Home');", true);
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "closeLogin", "closeLogin();", true);
                            }
                            else
                            {
                                Response.Redirect("Default.aspx?ERR=MSTD00007ERR");
                            }
                        }
                        else
                        {
                            Response.Redirect("Default.aspx?ERR=MSTD00007ERR");
                        }
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
            }
        }
    }
}