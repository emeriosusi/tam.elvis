﻿<html>
<head>
<style type="text/css">
.bahasa 
{
color:blue;
}
.doc 
{
padding-left:40px;
}
</style>

</head>
<body>
[<span class="bahasa">Bahasa Indonesia</span>]<br />
<br />
Kepada Bapak/Ibu Administrasi Divisi,<br />
<br />
Diingatkan untuk mencetak dan menyerahkan PV Cover beserta hardcopy attachmentnya untuk diproses di Finance, dengan detail sebagai berikut:
<table border="0"> 
<tr><td class="doc">Document No</td><td>:&nbsp;[DOC_NO]</td></tr>
<tr><td class="doc">Document Date</td><td>:&nbsp;[DOC_DATE]</td><tr>
<tr><td class="doc">Vendor</td><td>:&nbsp;[VENDOR_NM]</td></tr>
<tr><td class="doc">Transaction Type</td><td>:&nbsp;[TRANSACTION_TYPE_NM]</td></tr>
</table> 
<br />
ke <span style="font-size:larger;font-weight:bold;">loket Finance di Head Office TMMIN lantai 1.</span><br />
<br />
Pastikan anda sudah login ke ELVIS sebelum masuk ke link di bawah ini. <br />
Untuk mencetak PV Cover, silakan klik link berikut ini :	<br />
<a href="[DOC_LINK]">[DOC_LINK]</a> <br /> 
<br />
Terima kasih.<br />
<br />
Salam,<br />
ELVIS Administrator <br />
<br />
<hr />
[<span class="bahasa">English</span>]<br />
<br />
Dear Mr./Mrs. Division Administrator,<br />
<br />
Please make sure that PV Cover and its attachments has been submited to be processod on Finance, with the following detail:
<table border="0"> 
<tr><td class="doc">Document No</td><td>:&nbsp;[DOC_NO]</td></tr>
<tr><td class="doc">Document Date</td><td>:&nbsp;[DOC_DATE]</td><tr>
<tr><td class="doc">Vendor</td><td>:&nbsp;[VENDOR_NM]</td></tr>
<tr><td class="doc">Transaction Type</td><td>:&nbsp;[TRANSACTION_TYPE_NM]</td></tr>
</table> 
<br />
to <span style="font-size:larger;font-weight:bold;">Finance counter</span> on Head Office 1st floor.<br />
<br />
Please make sure that you have been logged on ELVIS before click the link below. <br />
Please click this following link to print the PV Cover:	<br />
<a href="[DOC_LINK]">[DOC_LINK]</a> <br />
<br />
Thank you.<br />
<br />
Regards,<br />
ELVIS Administrator
</body></html>