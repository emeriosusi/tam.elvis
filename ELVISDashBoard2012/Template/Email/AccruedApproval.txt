﻿<html>
<head>
<style type="text/css">
.bahasa 
{
color:blue;
}
.doc 
{
padding-left:40px;
}
</style>

</head>
<body>
[<span class="bahasa">Bahasa Indonesia</span>]<br />
<br />
Kepada Bapak/Ibu [DOC_APPROVER],<br />
<br />
Mohon persetujuan atas dokumen accrued berikut :
<table border="0"> 
<tr><td class="doc">Type</td><td>:&nbsp;[DOC_TYPE]</td></tr>
<tr><td class="doc">Document No</td><td>:&nbsp;[DOC_NO]</td></tr>
<tr><td class="doc">Document Date</td><td>:&nbsp;[DOC_DATE]</td><tr>
</table> 
<br />
Pastikan anda sudah login ke ELVIS sebelum masuk ke link di bawah ini. <br />
<a href="[DOC_LINK]">Link</a> <br /> 
<br />
Terima kasih.<br />
<br />
Salam,<br />
ELVIS Administrator <br />
<br />
<hr />
[<span class="bahasa">English</span>]<br />
<br />
Dear Mr./Mrs. Division Administrator,<br />
<br />
Please approve the following Accrued document :
<table border="0"> 
<tr><td class="doc">Type</td><td>:&nbsp;[DOC_TYPE]</td></tr>
<tr><td class="doc">Document No</td><td>:&nbsp;[DOC_NO]</td></tr>
<tr><td class="doc">Document Date</td><td>:&nbsp;[DOC_DATE]</td><tr>
</table> 
<br />
Please make sure that you have been logged on ELVIS before click the link below. <br />
<a href="[DOC_LINK]">Link</a> <br />
<br />
Thank you.<br />
<br />
Regards,<br />
ELVIS Administrator
</body></html>