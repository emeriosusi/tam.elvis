@Default 
	[A7:Y10007]
		[A]	SEQ_NO	N(15)
		[B]	INVOICE_NO	C(16)* 
		[C]	INVOICE_DATE	D* 
		[D]	TAX_NO	C(25)
		[E]	TAX_DT	D
		[F]	CURRENCY_CD	C(3)* 
		[G]	AMOUNT_TURN_OVER N(18,2)* 
		[H]	AMOUNT_SALES_OTHER N(18,2)
		[I]	AMOUNT_RENTAL_EXPENSE N(18, 2)
		[J]	AMOUNT_TRANSPORT_OPERATIONAL N(18, 2)
		[K]	AMOUNT_TRANSPORT_TOLL N(18, 2)
		[L]	AMOUNT_PPN	N(18,2)
		[M]	AMOUNT_SEAL N(18,2)
		[N]	AMOUNT	N(18,2)
		[O]	DESCRIPTION	C(50)* 
		[P] ACC_INF_ASSIGNMENT C(18)
		[Q]	COST_CENTER_CD	C(10)
		[R]	DPP_PPh_AMOUNT N(16,2)
		[S]	NPWP_Available C(50)
		[T] TAX_CODE_PPh23 C(10)
		[U] TAX_TARIFF_PPh23 N(16,4)
		[V] DPP_AMOUNT N(18,2)
		[W] TAX_CODE_PPh26 C(10)
		[X] TAX_TARIFF_PPh26 N(16,4)
		[Y] AMOUNT_PPh26 N(16,2)
		
