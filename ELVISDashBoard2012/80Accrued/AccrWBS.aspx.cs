﻿/*
 * Created By fid.Taufik 
 * Created Dt 29 April 2018
 * 
 */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._80Accrued;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using BusinessLogic.VoucherForm;
using System.IO;
using System.Web;
using Common.Control;
using DevExpress.Web.Data;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrWBS : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8015");
        private string susNo, susYear;
        private int suNO, suYY;
        private string accrNo;
        bool IsEditing;
        private string tempWbsNoPr { set; get; }

        protected HiddenField HiddenMessageExpandFlag { set; get; }
        protected Literal MessageControlLiteral { set; get; }
        protected FileUpload DataUpload { set; get; }

        #region Getter Setter for modal button if any
        protected ASPxGridLookup BudgetNumberLookup { set; get; }
        protected ASPxGridView DetailGrid { set; get; }
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }
        #endregion

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }

        #region Getter Setter for Data List
        private List<AccruedListDetail> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<AccruedListDetail>();
                }
                else
                {
                    return (List<AccruedListDetail>)ViewState["_listInquiry"];
                }
            }
        }

        public int EditIndex
        {
            get
            {
                if (Session["editIndex"] != null)
                    return Convert.ToInt32(Session["editIndex"]);
                else
                    return 0;
            }

            set
            {
                Session["editIndex"] = value;
            }
        }

        public bool IsEdit
        {
            get
            {
                if (Session["isEdit"] != null)
                    return Convert.ToBoolean(Session["isEdit"]);
                else
                    return false;
            }

            set
            {
                Session["isEdit"] = value;
            }
        }

        public string KeyList
        {
            get
            {
                if (Session["accr_no"] != null)
                    return Convert.ToString(Session["accr_no"]);
                else
                    return "";
            }

            set
            {
                Session["accr_no"] = value;
            }
        }

        #endregion
        #region Getter Setter for Attachment List
        [Serializable]
        private class ListAttachmentData
        {
            private string _NO;
            public string NO
            {
                get
                {
                    return _NO;
                }
                set
                {
                    _NO = value;
                }
            }

            private string _FILENAME;
            public string FILENAME
            {
                get
                {
                    return _FILENAME;
                }
                set
                {
                    _FILENAME = value;
                }
            }
        }
        private List<ListAttachmentData> ListAttachment
        {
            set
            {
                ViewState["_listAttachment"] = value;
            }
            get
            {
                if (ViewState["_listAttachment"] == null)
                {
                    return new List<ListAttachmentData>();
                }
                else
                {
                    return (List<ListAttachmentData>)ViewState["_listAttachment"];
                }
            }
        }
        #endregion
        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;
            HiddenMessageExpandFlag = hdExpandFlag;
            MessageControlLiteral = messageControl;
            if (!IsPostBack)
            {
                PrepareLogout();

                gridGeneralInfo.DataSourceID = null;

                #region Init Startup Script
                accrNo = Request["accr_no"];
                KeyList = accrNo;

                AccruedWBSListDetail dataHeader = logic.AccrWBS.GetHeader(accrNo).FirstOrDefault();
                if (dataHeader != null)
                {
                    tboxIssueDiv.Text = dataHeader.DIVISION_NAME;
                    tboxAccruedNo.Text = dataHeader.ACCRUED_NO;
                    tboxPvType.Text = dataHeader.PV_TYPE_NAME;
                    tboxUpdateDt.Text = dataHeader.UPDATED_DT.HasValue
                                ? dataHeader.UPDATED_DT.Value.ToString("dd-MM-yyyy")
                                : "";
                    tboxStatus.Text = dataHeader.STATUS_NAME;

                    SetDataGrid();
                    gridGeneralInfo.DataSource = ListInquiry;
                    gridGeneralInfo.DataBind();

                    LookupBudgetNo_Load(BudgetNumberLookup, EventArgs.Empty);

                    CleanSession();
                }
                else
                {
                    Nag("MSTD00018INF");
                }
                SetFirstLoad();
                #endregion
            }
            
        }

        private decimal GetSapAvailable(string WBSNo)
        {
            decimal sapAvail = 0;

            if (WBSNo.isEmpty())
                return sapAvail;

            var sapRet = logic.SAP.BudgetRemaining(new List<string>() { WBSNo });

            if (sapRet != null)
            {
                sapAvail = sapRet
                            .Where(x => x.BUDGET_NO.Equals(WBSNo))
                            .Select(x => x.REMAINING_AMT)
                            .FirstOrDefault();
            }

            return sapAvail;
        }


        protected void DoSearch()
        {
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DoSearch();
        }

        protected void btnUploadTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                DataUpload = fuAccrWBS;
                
                logic.Say("btUploadTemplate_Click", "Upload : {0}", DataUpload.FileName);
                //clearScreenMessage();

                if (!DataUpload.HasFile)
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00034ERR");
                    return;
                }

                int _processId = 0;
                CommonExcelUpload _CommonExcelUpload = new CommonExcelUpload();
                string _strFileType = Path.GetExtension(DataUpload.FileName).ToString().ToLower();
                string _strFileName = DataUpload.FileName;

                if (!_CommonExcelUpload.IsExcelFormat(_strFileType))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                    return;
                }

                int TemplateCd = 13;

                string _strMetaData = Server.MapPath(
                            string.Format(
                                logic.Sys.GetText("UPLOAD_META_FMT", "0"), logic.Sys.GetText("TEMPLATE_CD", TemplateCd.str())
                            )
                        );

                string _strFunctionId = "";
                _strFunctionId = resx.FunctionId("FCN_ACCRUED_WBS_FORM_UPLOAD");
                _processId = DoStartLog(_strFunctionId, "");

                string _strTempDir = LoggingLogic.GetTempPath("", _processId);

                string xlsFile = Util.GetTmp(_strTempDir, Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(_strFileName)) + "_" + _processId.str(), _strFileType);
                LoggingLogic.ForceDirectories(Path.GetDirectoryName(xlsFile));
                DataUpload.SaveAs(xlsFile);

                string[][] listOfErrors = new string[1][];
                int tt = logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219);

                logic.AccrWBS.DeleteTempAccruedWBS(_processId);

                string accrNo = Convert.ToString(Session["accr_no"]);
                bool Ok = logic.AccrWBS.Upload(accrNo, xlsFile, _strMetaData,
                                ref listOfErrors, tt, UserData, _processId);

                if (!Ok)
                {

                    logic.AccrWBS.GenerateLogAccr(_processId, UserData);

                    string sourceFile = Server.MapPath(
                                            String.Format(
                                                    logic.Sys.GetText("TEMPLATE.FMT", "ERR"), logic.Sys.GetText("TEMPLATE_CD", TemplateCd.str())
                                                )
                                            );
                    string targetFile = Server.MapPath(Common.AppSetting.ErrorUploadFilePath);

                    string fileName = logic.AccrWBS.DownloadError(_processId, _strFileName, sourceFile, targetFile, listOfErrors);

                    String linkErrorFile = String.Format("<a href='{0}://{1}:{2}{3}{4}' target='_blank'>{5}</a>",
                                                            Request.Url.Scheme,
                                                            Request.ServerVariables["SERVER_NAME"],
                                                            Request.ServerVariables["SERVER_PORT"],
                                                            Common.AppSetting.ErrorUploadFilePath.Replace("~", ""),
                                                            fileName,
                                                            fileName);

                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                    return;
                }


                //DetailGrid.UpdateEdit();

                //Tabel.DataList.Clear();
                //Tabel.DataList = dd;

                //Tabel.DataList = Tabel.sanityCheckWBS();
                //Tabel.resetDisplaySequenceNumberWBS();
                //StartEditingTabel();
                SetDataGrid();
                gridGeneralInfo.DataSource = ListInquiry;
                gridGeneralInfo.DataBind();

                PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
            }
            catch (Exception ex)
            {
                PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                Handle(ex);
            }
        }

        private void PostMsg(bool instant, bool clearfirst, byte sts, string id, params object[] x)
        {
            string m = "";
            StringBuilder b = new StringBuilder("");
            if (x != null && x.Length > 0)
            {
                for (int k = 0; k < x.Length; k++)
                    b.Append(x[k].str() + ", ");
            }

            if (!id.isEmpty())
            {
                m = _m.Message(id);
            }
            if (x != null && x.Length > 0)
            {
                if (!m.isEmpty())
                    m = string.Format(m, x);
                else
                {
                    b.Clear();
                    for (int i = 0; i < x.Length; i++)
                    {
                        b.Append(x[i].str());
                        b.Append(" ");
                    }
                    m = b.ToString().Trim();
                }
            }
            postScreenMessage(
                new ScreenMessage[]
                {
                    new ScreenMessage(sts, m)
                }
                , clearfirst
                , instant);
        }

        protected void postScreenMessage(ScreenMessage[] messages, bool clearFirst, bool instantDisplay)
        {
            List<ScreenMessage> lstMessage = ScreenMessageList;
            if (clearFirst)
            {
                lstMessage.Clear();
            }

            if (messages != null)
            {
                for (int i = 0; i < messages.Length; i++)
                {
                    lstMessage.Insert(0, messages[i]);
                }
            }
            updateScreenMessages();
        }

        private void PostNowPush(byte sts, string id, params object[] x)
        {
            PostMsg(true, false, sts, id, x);
        }

        protected void clearScreenMessage()
        {
            ScreenMessageList.Clear();
            updateScreenMessages();
        }

        protected void updateScreenMessages()
        {
            List<ScreenMessage> lstMessages = ScreenMessageList;
            

            int cntMessages = lstMessages.Count;
            if (cntMessages > 0)
            {
                ScreenMessage msg = lstMessages[0];
                StringBuilder stringBuilder = new StringBuilder();
                string expandFlag = HiddenMessageExpandFlag.Value;

                string messageBoxClass = "message-info";
                string messageBoxSpanClass = "message-span-info";
                if (msg.Status == ScreenMessage.STATUS_WARNING)
                {
                    messageBoxClass = "message-warning";
                    messageBoxSpanClass = "message-span-warning";
                }
                else if (msg.Status == ScreenMessage.STATUS_ERROR)
                {
                    messageBoxClass = "message-error";
                    messageBoxSpanClass = "message-span-error";
                }

                if (expandFlag.Equals("true"))
                {
                    messageBoxClass += " expanded-message-box";
                }
                stringBuilder.AppendLine(string.Format("<div id='messageBox' class='{0}'>", messageBoxClass));
                stringBuilder.AppendLine(string.Format("      <span class='{0}'>{1}</span>", messageBoxSpanClass, msg.Message));

                if (cntMessages > 1)
                {
                    string txtMore = "more";
                    if (expandFlag.Equals("true"))
                    {
                        txtMore = "close";
                    }
                    stringBuilder.AppendLine("      <span class='message-span-more'>");
                    stringBuilder.AppendLine(string.Format("          <a id='message-detail-link' href='#' onclick=\"expandScreenMessage()\">{0}</a>", txtMore));
                    stringBuilder.AppendLine("      </span>");
                }

                stringBuilder.AppendLine("</div>");
                if (expandFlag.Equals("false"))
                {
                    stringBuilder.AppendLine("<div id='messageBox-all' style='display: none'>");
                }
                else
                {
                    stringBuilder.AppendLine("<div id='messageBox-all'>");
                }
                stringBuilder.AppendLine("  <ul>");
                string listClass;
                for (int i = 1; i < cntMessages; i++)
                {
                    msg = lstMessages[i];
                    listClass = "message-detail-info";
                    if (msg.Status == ScreenMessage.STATUS_WARNING)
                    {
                        listClass = "message-detail-warning";
                    }
                    else if (msg.Status == ScreenMessage.STATUS_ERROR)
                    {
                        listClass = "message-detail-error";
                    }

                    stringBuilder.AppendLine(string.Format("<li class='{0}'>", listClass));
                    stringBuilder.AppendLine("      <span>" + msg.Message + "</span>");
                    stringBuilder.AppendLine("</li>");
                }
                stringBuilder.AppendLine("  </ul>");
                stringBuilder.AppendLine("</div>");

                MessageControlLiteral.Text = stringBuilder.ToString();
                MessageControlLiteral.Visible = true;
            }
            else
            {
                MessageControlLiteral.Text = "";
                MessageControlLiteral.Visible = false;
            }
        }

        protected List<ScreenMessage> ScreenMessageList
        {
            get
            {
                List<ScreenMessage> lstMessages = Session["AccrWBSMsgList"] as List<ScreenMessage>;
                if (lstMessages == null)
                {
                    lstMessages = new List<ScreenMessage>();
                    Session["AccrWBSMsgList"] = lstMessages;
                    HiddenMessageExpandFlag.Value = "false";
                }
                return lstMessages;
            }
        }

        private void PostNow(byte sts, string id, params object[] x)
        {
            PostMsg(true, true, sts, id, x);
        }
        protected void evt_messageControl_onLoad(object sender, EventArgs args)
        {
            updateScreenMessages();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonDownload_Click", "Download");
            if (ValidateInputSearch())
            {
                DownloadList();
            }
        }

        protected void DownloadList()
        {
            UserData userdata = (UserData)Session["UserData"];
            try
            {
                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "AccruedWBSList.xls");
                string excelName = "List_Accrued_WBS_Download_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                string pdfName = "List_Accrued_WBS_Download_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";


                string fullPath = logic.AccrWBS.GenerateListAccrWBSResult(
                            UserName,
                            templatePath,
                            excelName,
                            Convert.ToString(Session["accr_no"]),
                            userdata,
                            pdfName
                        );
                Xmit.Transmit(HttpContext.Current, fullPath);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    string xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                    Nag("MSTD00042ERR", ex.Message);
                }
            }
        }

        private bool ValidateInputSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Handle(ex);
                throw;
            }
            return true;
        }
        #region SetFirstLoad
        private void SetFirstLoad()
        {
            btnSave.Visible = false;
            btnCancel.Visible = false;
            btnClose.Visible = true;

            if (GetSessData() != null)
            {
                btnEditRow.Visible = RoLo.isAllowedAccess("btnEditRow");
                
                bool complete = RoLo.isAllowedAccess("btnComplete");
                if (tboxStatus.Text == "Complete" && complete == true)
                {
                    btnEditRow.Visible = false;
                    btnComplete.Visible = false;
                }
                else
                {
                    btnComplete.Visible = complete;
                }
                btnDownload.Visible = RoLo.isAllowedAccess("btnDownload");
                btnUploadTemplate.Visible = RoLo.isAllowedAccess("btnUploadTemplate");
            }
            else
            {
                btnEditRow.Visible = false;
                btnComplete.Visible = false;
                btnDownload.Visible = false;
                btnUploadTemplate.Visible = false;
            }
            CleanSession();
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype edit
            if (true)
            {
                if (e.RowType == GridViewRowType.InlineEdit)
                {
                    #region init

                    //Get Element by ID
                    ASPxTextBox txtGridBookingNo = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["BOOKING_NO"] as GridViewDataColumn, "txtGridBookingNo")
                        as ASPxTextBox;
                    ASPxTextBox txtGridWBSNoOld = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["WBS_NO_OLD"] as GridViewDataColumn, "txtGridWBSNoOld")
                        as ASPxTextBox;
                    ASPxTextBox txtGridWBSDesc = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["WBS_DESC"] as GridViewDataColumn, "txtGridWBSDesc")
                        as ASPxTextBox;
                    ASPxTextBox txtGridInitAmt = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["INIT_AMT"] as GridViewDataColumn, "txtGridInitAmt")
                        as ASPxTextBox;
                    ASPxTextBox txtGridSapAvailable = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["SAP_AVAILABLE"] as GridViewDataColumn, "txtGridSapAvailable")
                        as ASPxTextBox;
                    ASPxTextBox txtGridSapRemaining = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["SAP_REMAINING"] as GridViewDataColumn, "txtGridSapRemaining")
                        as ASPxTextBox;
                    ASPxTextBox txtGridWbsNoNew = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["WBS_NO_PR"] as GridViewDataColumn, "txtGridWbsNoNew")
                        as ASPxTextBox;

                    

                    //ASPxGridLookup ddlGridWBSNoNew = gridGeneralInfo.FindEditRowCellTemplateControl(
                    //    gridGeneralInfo.Columns["WBS_NO_PR"] as GridViewDataColumn, "ddlGridWBSNoNew")
                    //    as ASPxGridLookup;

                    #endregion

                    #region edit
                    if (PageMode == Common.Enum.PageMode.Edit)
                    {
                        
                        int _VisibleIndex = EditIndex;
                        
                        if (_VisibleIndex == e.VisibleIndex)    
                        {
                            #region fill data edit

                            //Get value Data Edited
                            string bookingNo = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "BOOKING_NO"));
                            string wbsNoOld = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "WBS_NO_OLD"));
                            string wbsNoNew = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "WBS_NO_PR"));
                            string wbsDescription = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "WBS_DESC"));
                            decimal initAmt = Convert.ToDecimal(gridGeneralInfo.GetRowValues(_VisibleIndex, "INIT_AMT"));
                            decimal sapAvailable = Convert.ToDecimal(gridGeneralInfo.GetRowValues(_VisibleIndex, "SAP_AVAILABLE"));
                            decimal sapRemaining = Convert.ToDecimal(gridGeneralInfo.GetRowValues(_VisibleIndex, "SAP_REMAINING"));

                            //Set SelectBox WBS No (New)
                            //List<WBSStructure> lstWbsNumber = logic.AccrWBS.getWbsNumbers();
                            //ddlGridWBSNoNew.DataSource = lstWbsNumber;
                            //ddlGridWBSNoNew.DataBind();

                            //Set Readonly fields
                            txtGridBookingNo.Text = bookingNo;
                            txtGridBookingNo.ReadOnly = true;

                            txtGridWBSNoOld.Text = wbsNoOld;
                            txtGridWBSNoOld.ReadOnly = true;

                            txtGridWBSDesc.Text = wbsDescription;
                            txtGridWBSDesc.ReadOnly = true;

                            txtGridInitAmt.Text = initAmt.fmt();
                            txtGridInitAmt.ReadOnly = true;

                            txtGridSapAvailable.Text = sapAvailable.fmt();
                            txtGridSapAvailable.ReadOnly = true;

                            txtGridSapRemaining.Text = sapRemaining.fmt();
                            txtGridSapRemaining.ReadOnly = true;
                            if (wbsNoNew == "null")
                            {
                                wbsNoNew = "";
                            }
                            txtGridWbsNoNew.Text = wbsNoNew;

                            if (!string.IsNullOrEmpty(wbsNoNew))
                            {
                                //decimal available = GetSapAvailable(wbsNoNew);
                                if (ListInquiry[_VisibleIndex].INIT_AMT != ListInquiry[_VisibleIndex].SAP_AVAILABLE)
                                {
                                    txtGridSapAvailable.ControlStyle.BackColor = System.Drawing.Color.Red;
                                }
                                else
                                {
                                    txtGridSapAvailable.ControlStyle.BackColor = System.Drawing.Color.White;
                                }
                            }
                            
                            
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            #endregion
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected bool validateComplete()
        {
            bool isValid = true;

            List<AccruedListDetail> data = ListInquiry;
            foreach (var p in data)
            {
                if (string.IsNullOrEmpty(p.WBS_NO_PR))
                {
                    isValid = false;
                    break; ;
                }
            }

            return isValid;
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            logic.Say("btnComplete_Click", "Complete");

            bool isValid = validateComplete();
            if (isValid)
            {
                lblConfirmationComplete.Text = _m.Message("MSTD00008CON", "Complete process");
                ConfirmationCompletePopUp.Show();

                //string accrNo = KeyList;
                //bool isSubmitted = logic.AccrWBS.updateComplete(accrNo, ListInquiry, UserData);

                //if (isSubmitted)
                //{
                //    lblConfirmationComplete.Text = _m.Message("MSTD00008CON", "Complete process");
                //    ConfirmationCompletePopUp.Show();
                //}
                //else
                //{
                //    Nag("MSTD00222ERR", "Accrued WBS");
                //}
            }
            else
            {
                Nag("MSTD00221ERR", "Accrued WBS");
            }
        }

        protected void btnOkConfirmationComplete_Click(object sender, EventArgs e)
        {
            string accrNo = KeyList;
            string reffNo = logic.Look.getAccrReffNo(accrNo);

            logic.Say("btnComplete_Click", "Do Complete {0}", accrNo);

            string command = resx.K2ProcID("Form_Registration_Approval");
            bool success = logic.PVRVWorkflow.Go(reffNo, command, UserData, "N/A", false, accrNo);

            if (success)
            {
                tboxStatus.Text = "Complete";
                SetFirstLoad();
                logic.Say("btnComplete_Click", "Success Complete {0}", accrNo);
                Nag("MSTD00060INF", "Complete process");
            }
            else
            {
                logic.Say("btnComplete_Click", "Failed Complete {0}", accrNo);
                Nag("MSTD00062ERR", "Complete process");
            }

            //string accrNo = Convert.ToString(Session["accr_no"]);
            //logic.AccrWBS.CompleteAccrWBS(accrNo);

        }
        protected void btnCancelConfirmationComplete_Click(object sender, EventArgs e)
        {
            ConfirmationCompletePopUp.Hide();
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            //logic.Say("btnSaveDetail_Click", "Save Detail {0} {1}", ddlSuspenseNo.Value, txtPVYear.Text);
            if (ValidateInputSave())
            {
                
                if (PageMode == Common.Enum.PageMode.Edit)
                {
                    #region Edit
                    if (ValidateInputSave())
                    {
                        //gridGeneralInfo.UpdateEdit();
                        //string NewWbsNo = GetSessWbsNo();
                        int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                        //decimal amtSapVailable = GetSessSapAvail();
                        decimal amtSapVailable = 0;
                        //ListInquiry[_VisibleIndex].WBS_NO_PR = NewWbsNo;

                        bool validAmt = true;
                        foreach (AccruedListDetail tempData in ListInquiry)
                        {
                            amtSapVailable = GetSapAvailable(tempData.WBS_NO_PR);
                            if (tempData.INIT_AMT != amtSapVailable)
                            {
                                validAmt = false;
                            }
                        }

                        //Check Amount is valid
                        if (validAmt)
                        {
                            bool success = logic.AccrWBS.UpdateDetail(
                                ListInquiry,
                                UserData);

                            if (success)
                            {
                                SetSavedMode(false);
                                Nag("MSTD00013INF", "Accrued WBS");
                                CleanSession();
                            }
                            else
                            {
                                Nag("MSTD00012ERR", "Accrued WBS");
                            }
                        }
                        else
                        {
                            Nag("MSTD00002ERR", "Available amount for WBS No (new) does not match Amount in IDR");
                        }
                    }
                    #endregion
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                bool edit = true;
                StartEditing();

                //if (gridGeneralInfo.Selection.Count == 1)
                //{
                //    int _VisibleIndex = -1;
                //    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                //    {
                //        if (gridGeneralInfo.Selection.IsRowSelected(i))
                //        {
                //            _VisibleIndex = i;
                //            PageMode = Common.Enum.PageMode.Edit;
                //            gridGeneralInfo.SettingsBehavior.AllowSort = false;
                //            IsEditing = true;
                //            Session["visibleIndex"] = i;
                //            tempWbsNoPr = ListInquiry[i].WBS_NO_PR;
                //            gridGeneralInfo.StartEdit(i);
                //            gridGeneralInfo.Selection.UnselectAll();
                //            SetEditDetail();
                //            CleanSession();
                //            Session["WbsNoPr"] = ListInquiry[i].WBS_NO_PR;
                //            break;
                //        }
                //    }
                //}
                //else if (gridGeneralInfo.Selection.Count == 0)
                //{
                //    Nag("MSTD00009WRN");
                //}
                //else if (gridGeneralInfo.Selection.Count > 0)
                //{
                //    Nag("MSTD00016WRN");
                //}
            }
        }

        protected void StartEditing()
        {
            PageMode = Common.Enum.PageMode.Edit;
            btnSave.Visible = true;
            btnCancel.Visible = true;
            btnEditRow.Visible = false;
            btnComplete.Visible = false;
            //clearScreenMessage();
            IsEdit = true;

            ReloadGrid();
            StartEditingTabel();
        }
        protected void StartEditingTabel()
        {
            //GridViewColumn column = gridGeneralInfo.Columns["DeletionControl"];
            //column.Visible = true;

            gridGeneralInfo.CancelEdit();
            ListInquiry = GetSessData();
            int cntData = ListInquiry.Count();
            if (cntData > 0)
            {
                //ASPxGridLookup ddlGridBookingNo = gridGeneralInfo.FindEditRowCellTemplateControl(
                //        gridGeneralInfo.Columns["BOOKING_NO"] as GridViewDataColumn, "ddlGridBookingNo")
                //        as ASPxGridLookup;

                //ASPxTextBox txtGridSapAvailable = gridGeneralInfo.FindEditRowCellTemplateControl(
                //        gridGeneralInfo.Columns["BOOKING_NO"] as GridViewDataColumn, "txtGridSapAvailable")
                //        as ASPxTextBox;

                //string bookingNo = Convert.ToString(gridGeneralInfo.GetRowValues(cntData - 1, "BOOKING_NO"));
                //EditIndex = cntData - 1;
                //decimal maxAmt = ListInquiry[EditIndex].RECLAIMABLE_AMT.HasValue ? (decimal)ListInquiry[EditIndex].RECLAIMABLE_AMT : 0;
                //if (ListInquiry[EditIndex].RECLAIMABLE_AMT < ListInquiry[EditIndex].EXTENDABLE_AMT)
                //{
                //    maxAmt = ListInquiry[EditIndex].EXTENDABLE_AMT.HasValue ? (decimal)ListInquiry[EditIndex].EXTENDABLE_AMT : 0;
                //}
                //gridGeneralInfo.JSProperties["cpMaxAmt"] = maxAmt;
                EditIndex = cntData - 1;
                gridGeneralInfo.StartEdit(EditIndex);
                //ddlGridBookingNo.Value = bookingNo;

            }
            else
            {
                //initFirstRow();
            }
        }

        protected void btnCancelEdit_Click(object sender, EventArgs e)
        {
            //logic.Say("btnCancelDetail_Click", "Cancel Edit {0}", ddlSuspenseNo.Value);
            gridGeneralInfo.CancelEdit();
            btnSave.Visible = false;
            btnCancel.Visible = false;
            btnEditRow.Visible = RoLo.isAllowedAccess("btnEditRow");
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }


        protected void SetCancelEditDetail()
        {
            CleanSession();
            SetDataGrid();
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
        }

        private void SetSavedMode(bool convertPvRv)
        {
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.CancelEdit();
            gridGeneralInfo.DataBind();
            SetFirstLoad();
            PageMode = Common.Enum.PageMode.View;
            gridGeneralInfo.SettingsBehavior.AllowSort = true;
            SetCancelEditDetail();
        }

        #endregion

        #region Function

        private bool ValidateInputSave()
        {
            bool valid = true;
            //string WbsNoNew = GetSessWbsNo();
            //if (String.IsNullOrEmpty(WbsNoNew))
            //{
            //    if (String.IsNullOrEmpty(tempWbsNoPr))
            //    {
            //        Nag("MSTD00017WRN", "WBS No. (New)");
            //        valid = false;
            //    }
            //    else
            //    {
            //        valid = true;
            //    }
            //}
            return valid;
        }

        protected void SetDataGrid()
        {
            string accrNo = Convert.ToString(Session["accr_no"]);
            ListInquiry = logic.AccrWBS.Search(accrNo).Distinct().ToList();
            int i = 0;
            decimal available = 0;
            foreach(AccruedListDetail data in ListInquiry){
                available = GetSapAvailable(ListInquiry[i].WBS_NO_PR);
                if (available != 0)
                {
                    ListInquiry[i].SAP_AVAILABLE = available;
                    ListInquiry[i].SAP_REMAINING = ListInquiry[i].INIT_AMT - available;
                }
                else
                {
                    ListInquiry[i].SAP_AVAILABLE = null;
                    ListInquiry[i].SAP_REMAINING = ListInquiry[i].INIT_AMT;
                }
                
                i++;
            }
            ListInquiry = ListInquiry;
            SetSessData(ListInquiry);
        }

        protected void LookupBudgetNo_Load(object sender, EventArgs arg)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                return;
            }

            ASPxGridLookup lookup = (ASPxGridLookup)sender;

            List<WBSStructure> lstWbsNumber = logic.AccrWBS.getWbsNumbers();
            lookup.DataSource = lstWbsNumber;
            lookup.DataBind();

        }

        protected void NewWBSNo_Changed(object sender, EventArgs arg)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                return;
            }

            ASPxGridLookup picker = (ASPxGridLookup)sender;
            if (picker.Value != null)
            {
                if(!String.IsNullOrEmpty(picker.Value.ToString())){
                    decimal available = GetSapAvailable(picker.Value.ToString());

                    Session["WbsNoPr"] = picker.Value.ToString();
                    Session["SapAvailable"] = available;
                    int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                    ListInquiry[_VisibleIndex].SAP_AVAILABLE = available;
                    ListInquiry[_VisibleIndex].WBS_NO_PR = picker.Value.ToString();
                    ListInquiry = ListInquiry;
                    gridGeneralInfo.CancelEdit();
                    gridGeneralInfo.DataSource = ListInquiry;
                    gridGeneralInfo.DataBind();

                }
            }
           

        }

        protected void evt_gridGeneralInfo_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs arg)
        {
            DetailGrid = (ASPxGridView)sender;

            string param = arg.Parameters;
            if (param.isEmpty()) return;
            string[] paramFracs = param.Split(':');
            string prefix = (paramFracs != null && paramFracs.Length > 0) ? paramFracs[0] : "";
            if (!prefix.Equals("p")) return;
            string command = (paramFracs != null && paramFracs.Length > 1) ? paramFracs[1] : "";
            if (!command.Equals("update")) return;


            string WbsNoNew = (paramFracs != null && paramFracs.Length > 2) ? paramFracs[2] : "";
            decimal available = GetSapAvailable(WbsNoNew);

            int _VisibleIndex = EditIndex;

            ListInquiry = GetSessData();
            //ListInquiry[_VisibleIndex].SAP_AVAILABLE = available;
            ListInquiry[_VisibleIndex].WBS_NO_PR = WbsNoNew;
            ListInquiry[_VisibleIndex].SAP_AVAILABLE = available;
            decimal? tempDiff = available - ListInquiry[_VisibleIndex].INIT_AMT;
            ListInquiry[_VisibleIndex].SAP_REMAINING = Math.Abs((decimal)tempDiff);
            //ListInquiry[_VisibleIndex].SAP_REMAINING = ListInquiry[_VisibleIndex].INIT_AMT - available;
            ListInquiry = ListInquiry;
            SetSessData(ListInquiry);

            gridGeneralInfo.CancelEdit();
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
            gridGeneralInfo.StartEdit(_VisibleIndex);


            
        }

        protected void evt_gridGeneralInfo_onSelectionChanged(object sender, EventArgs args)
        {

            if (PageMode == Common.Enum.PageMode.Edit)
            {
                logic.Say("evt_gridGeneralInfo_onSelectionChanged", "Change Row");
                int i = getSelectedRow();
                gridGeneralInfo.CancelEdit();

                ListInquiry = GetSessData();
                //if (ListInquiry[EditIndex].SAP_REMAINING != 0 )
                //{
                //    ListInquiry[EditIndex].SAP_AVAILABLE = 0;
                //    ListInquiry[EditIndex].WBS_NO_PR = "";
                //    ListInquiry[EditIndex].SAP_REMAINING = ListInquiry[EditIndex].INIT_AMT;
                //}
                EditIndex = i;
                ReloadGrid();
                
                gridGeneralInfo.StartEdit(EditIndex);
            }
            else
            {
                return;
            }


        }
        public int getSelectedRow()
        {
            ReloadGrid();
            WebDataSelection selection = gridGeneralInfo.Selection;
            int cntData = ListInquiry.Count;
            for (int i = 0; i < cntData; i++)
            {
                if (selection.IsRowSelected(i))
                {
                    return i;
                }
            }
            return 0;
        }

        protected void gridStatusAmount_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            SetCellColor(e);
        }
        protected void SetCellColor(ASPxGridViewTableDataCellEventArgs e)
        {
            int curRow = e.VisibleIndex;
            int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);

            //if (PageMode != Common.Enum.PageMode.Edit)
            //{
            //    return;
            //}

            //if (curRow == _VisibleIndex)
            //{
            //    if (e.DataColumn == null) return;
                
            //    if (e.DataColumn.FieldName == "SAP_AVAILABLE")
            //    {

            //        if (GetSessSapAvail() != ListInquiry[_VisibleIndex].INIT_AMT)
            //        {
            //            e.Cell.Style.Add("background-color", red);
            //        }
            //    }
            //}

            if (e.DataColumn == null) return;

            if (e.DataColumn.FieldName == "SAP_AVAILABLE")
            {
                string curAvailable = e.GetValue("SAP_AVAILABLE") == null?"":e.GetValue("SAP_AVAILABLE").ToString();
                if (!string.IsNullOrEmpty(curAvailable))
                {
                    if (Convert.ToDecimal(curAvailable) != ListInquiry[curRow].INIT_AMT)
                    {
                        e.Cell.Style.Add("background-color", red);
                    }
                }
                
            }
        }

        #endregion

        private string GetSessWbsNo() 
        {
            string WbsNo = "";
            if (Session["WbsNoPr"] != null)
            {
                WbsNo = Convert.ToString(Session["WbsNoPr"]);
            }
            return WbsNo;
        }

        private decimal GetSessSapAvail()
        {
            decimal SapAvailable = 0;
            if (Session["SapAvailable"] != "")
            {
                SapAvailable = Convert.ToDecimal(Session["SapAvailable"]);
            }
            return SapAvailable;
        }

        private void CleanSession()
        {
            Session["WbsNoPr"] = "";
            Session["SapAvailable"] = "";
        }

        private void SetSessData(List<AccruedListDetail> p)
        {
            ListInquiry = p;
            Session["dataGrid"] = p;
        }
        public void ReloadGrid()
        {
            ListInquiry = GetSessData();
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
        }
        private List<AccruedListDetail> GetSessData()
        {
            List<AccruedListDetail> ret = (List<AccruedListDetail>)Session["dataGrid"];
            return ret;
        }

      
    }
}