﻿<%@ Page Title="Accrued Journal Simulation" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="AccrSimul.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrSimul" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xx" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">

        function ActiveTabChanged(e) {
            var hidButton = document.getElementById("HiddenButtonSearch");
            $("#HidActiveTab").val(e.tab.index);
            console.log('Switch tab to : ' + e.tab.index);
            hidButton.click();
            loading();
            return false;
        }

        function OnKeyDown(s, event) {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                search(); 
                return false; 
            }
        }
    </script>

<style type="text/css">
   .boxDesc
   {
       border: 1px solid #afafaf;
       border-style: inset;
       background-color: #efefef;
       display: block;              
       overflow: auto;
       padding-left: 5px;
       height: 22px;
       float: left;
       vertical-align: middle;
   }
   
   #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 80px;
   }
   #divVendorName
   {
       width: 254px;
   }

   .row-bold-top > td
   {
       border-top: 3px solid black
   }
</style>
</asp:Content>
<asp:Content ID="xPre" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
            <asp:HiddenField runat="server" ID="HidActiveTab" />
            <asp:Button ID="HiddenButtonSearch" runat="server" Style="display: none;" OnClick="SimulationListTabs_OnActiveTabChanged" ClientIDMode="Static" OnClientClick="loading();"/>

            <div class="contentsection" style="width:987px !important;">
                <dx:ASPxPageControl ID="SimulationListTabs" runat="server" ActiveTabIndex="0" EnableHierarchyRecreation="True"
                     Width="970px" BackColor="#EFEFFF" Height="490px" AutoPostBack="false">
                    <ClientSideEvents 
                        ActiveTabChanged="function(s, e) { ActiveTabChanged(e); }">
                    </ClientSideEvents>
                    <TabPages>
                        <dx:TabPage Text="PR" Name="TabPR">
                            <ContentCollection>
                                <dx:ContentControl ID="PRSimulationContent" runat="server">
                                    <div class="contentsectionUnderTab">
                                        <dx:ASPxGridView ID="PRGridSimulation" runat="server" Width="100%" ClientInstanceName="PRGridSimulation"
                                            KeyFieldName="KEYS" AutoGenerateColumns="False">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="Simulation" FieldName="KEYS" VisibleIndex="0">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager AlwaysShowPager="false" Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                VerticalScrollableHeight="470" VerticalScrollBarStyle="Standard" />
                                            <Styles>
                                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                </Header>
                                                <Cell HorizontalAlign="Center" Border-BorderStyle="None">
                                                    <Border BorderStyle="None" />
                                                </Cell>
                                            </Styles>
                                            <Templates>
                                                <DetailRow>
                                                    <table cellpadding="1px" cellspacing="0" border="0" style="text-align: left" width="870px">
                                                        <colgroup>
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                        </colgroup>
                                                        <tr>
                                                            <td> Transaction Type: </td>
                                                            <td> <%# Eval("TRANSACTION_NAME")%> </td>

                                                            <td> Accrued No: </td>
                                                            <td> <%# Eval("ACCRUED_NO")%> </td>

                                                            <td> Fiscal Year: </td>
                                                            <td> <%# Eval("FISCAL_YEAR")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("DOC_DATE")) %> </td>

                                                            <td> Company Code: </td>
                                                            <td> <%# Eval("COMPANY_CODE")%> </td>
                    
                                                            <td> Period: </td>
                                                            <td> <%# Eval("PERIOD")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ref. Doc.: </td>
                                                            <td> <%# Eval("REF_DOC")%> </td>
                    
                                                            <td> Posting Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("POSTING_DATE")) %> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Currency: </td>
                                                            <td> <%# Eval("CURRENCY_CODE")%> </td>

                                                            <td> Witholding Tax: </td>
                                                            <td> <%# Eval("WITHOLDING_TAX")%> </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <dx:ASPxGridView ID="PRgridPopUpSimulation" runat="server" Width="835px" KeyFieldName="SEQ_NO"
                                                        OnBeforePerformDataSelect="gridPopUpSimulation_DataSelect" AutoGenerateColumns="false"
                                                        ClientInstanceName="PRgridPopUpSimulation">
                                                        <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="false">
                                                        </SettingsPager>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn VisibleIndex="0" Width="40px" FieldName="SEQ_NO" Caption="Itm" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="ACCOUNT" Caption="Account"
                                                                Width="110px" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="ACCOUNT_SHORT" Caption="Account short text" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="TX" Caption="Tx" Width="50px"
                                                                CellStyle-HorizontalAlign="Center" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="AMOUNT" Caption="Amount" Width="110px"
                                                                CellStyle-HorizontalAlign="Right" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="TEXT" Caption="Text" />
                                                        </Columns>
                                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                                        <Styles>
                                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </Header>
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                </DetailRow>
                                            </Templates>
                                            <SettingsDetail ShowDetailRow="true" />
                                            <Settings ShowGroupPanel="false" />
                                            <SettingsCustomizationWindow Enabled="True" />
                                        </dx:ASPxGridView>
                                    </div>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="Direct" Name="TabDirect">
                            <ContentCollection>
                                <dx:ContentControl ID="DirectSimulationContent" runat="server">
                                    <div class="contentsectionUnderTab">
                                        <dx:ASPxGridView ID="DirectGridSimulation" runat="server" Width="100%" ClientInstanceName="DirectGridSimulation"
                                            KeyFieldName="KEYS" AutoGenerateColumns="False">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="Simulation" FieldName="KEYS" VisibleIndex="0">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager AlwaysShowPager="false" Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                VerticalScrollableHeight="470" VerticalScrollBarStyle="Standard" />
                                            <Styles>
                                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                </Header>
                                                <Cell HorizontalAlign="Center" Border-BorderStyle="None">
                                                    <Border BorderStyle="None" />
                                                </Cell>
                                            </Styles>
                                            <Templates>
                                                <DetailRow>
                                                    <table cellpadding="1px" cellspacing="0" border="0" style="text-align: left" width="870px">
                                                        <colgroup>
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                        </colgroup>
                                                        <tr>
                                                            <td> Transaction Type: </td>
                                                            <td> <%# Eval("TRANSACTION_NAME")%> </td>

                                                            <td> Accrued No: </td>
                                                            <td> <%# Eval("ACCRUED_NO")%> </td>

                                                            <td> Fiscal Year: </td>
                                                            <td> <%# Eval("FISCAL_YEAR")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("DOC_DATE")) %> </td>

                                                            <td> Company Code: </td>
                                                            <td> <%# Eval("COMPANY_CODE")%> </td>
                    
                                                            <td> Period: </td>
                                                            <td> <%# Eval("PERIOD")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ref. Doc.: </td>
                                                            <td> <%# Eval("REF_DOC")%> </td>
                    
                                                            <td> Posting Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("POSTING_DATE")) %> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Currency: </td>
                                                            <td> <%# Eval("CURRENCY_CODE")%> </td>

                                                            <td> Witholding Tax: </td>
                                                            <td> <%# Eval("WITHOLDING_TAX")%> </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <dx:ASPxGridView ID="DirectGridPopUpSimulation" runat="server" Width="835px" KeyFieldName="SEQ_NO"
                                                        OnBeforePerformDataSelect="gridPopUpSimulation_DataSelect" AutoGenerateColumns="false"
                                                        ClientInstanceName="DirectGridPopUpSimulation">
                                                        <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="false">
                                                        </SettingsPager>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn VisibleIndex="0" Width="40px" FieldName="SEQ_NO" Caption="Itm" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="ACCOUNT" Caption="Account"
                                                                Width="110px" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="ACCOUNT_SHORT" Caption="Account short text" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="TX" Caption="Tx" Width="50px"
                                                                CellStyle-HorizontalAlign="Center" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="AMOUNT" Caption="Amount" Width="110px"
                                                                CellStyle-HorizontalAlign="Right" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="TEXT" Caption="Text" />
                                                        </Columns>
                                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                                        <Styles>
                                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </Header>
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                </DetailRow>
                                            </Templates>
                                            <SettingsDetail ShowDetailRow="true" />
                                            <Settings ShowGroupPanel="false" />
                                            <SettingsCustomizationWindow Enabled="True" />
                                        </dx:ASPxGridView>
                                    </div>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="Suspense Paid" Name="TabSusPaid">
                            <ContentCollection>
                                <dx:ContentControl ID="SusPaidSimulationContent" runat="server">
                                    <div class="contentsectionUnderTab">
                                        <dx:ASPxGridView ID="SusPaidGridSimulation" runat="server" Width="100%" ClientInstanceName="SusPaidGridSimulation"
                                            KeyFieldName="KEYS" AutoGenerateColumns="False">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="Simulation" FieldName="KEYS" VisibleIndex="0">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager AlwaysShowPager="false" Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                VerticalScrollableHeight="470" VerticalScrollBarStyle="Standard" />
                                            <Styles>
                                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                </Header>
                                                <Cell HorizontalAlign="Center" Border-BorderStyle="None">
                                                    <Border BorderStyle="None" />
                                                </Cell>
                                            </Styles>
                                            <Templates>
                                                <DetailRow>
                                                    <table cellpadding="1px" cellspacing="0" border="0" style="text-align: left" width="870px">
                                                        <colgroup>
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                        </colgroup>
                                                        <tr>
                                                            <td> Transaction Type: </td>
                                                            <td> <%# Eval("TRANSACTION_NAME")%> </td>

                                                            <td> Accrued No: </td>
                                                            <td> <%# Eval("ACCRUED_NO")%> </td>

                                                            <td> Fiscal Year: </td>
                                                            <td> <%# Eval("FISCAL_YEAR")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("DOC_DATE")) %> </td>

                                                            <td> Company Code: </td>
                                                            <td> <%# Eval("COMPANY_CODE")%> </td>
                    
                                                            <td> Period: </td>
                                                            <td> <%# Eval("PERIOD")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ref. Doc.: </td>
                                                            <td> <%# Eval("REF_DOC")%> </td>
                    
                                                            <td> Posting Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("POSTING_DATE")) %> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Currency: </td>
                                                            <td> <%# Eval("CURRENCY_CODE")%> </td>

                                                            <td> Witholding Tax: </td>
                                                            <td> <%# Eval("WITHOLDING_TAX")%> </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <dx:ASPxGridView ID="SusPaidGridPopUpSimulation" runat="server" Width="835px" KeyFieldName="SEQ_NO"
                                                        OnBeforePerformDataSelect="gridPopUpSimulation_DataSelect" AutoGenerateColumns="false"
                                                        ClientInstanceName="SusPaidGridPopUpSimulation" 
                                                        OnHtmlRowPrepared="SuspenseGridPopUpSimulation_HtmlRowPrepared">
                                                        <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="false">
                                                        </SettingsPager>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn VisibleIndex="0" Width="40px" FieldName="SEQ_NO" Caption="Itm" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="ACCOUNT" Caption="Account"
                                                                Width="110px" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="ACCOUNT_SHORT" Caption="Account short text" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="TX" Caption="Tx" Width="50px"
                                                                CellStyle-HorizontalAlign="Center" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="AMOUNT" Caption="Amount" Width="110px"
                                                                CellStyle-HorizontalAlign="Right" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="TEXT" Caption="Text" />
                                                        </Columns>
                                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                                        <Styles>
                                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </Header>
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                </DetailRow>
                                            </Templates>
                                            <SettingsDetail ShowDetailRow="true" />
                                            <Settings ShowGroupPanel="false" />
                                            <SettingsCustomizationWindow Enabled="True" />
                                        </dx:ASPxGridView>
                                    </div>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="Suspense Unpaid" Name="TabSusUnpaid">
                            <ContentCollection>
                                <dx:ContentControl ID="SusUnpaidSimulationContent" runat="server">
                                    <div class="contentsectionUnderTab">
                                        <dx:ASPxGridView ID="SusUnpaidGridSimulation" runat="server" Width="100%" ClientInstanceName="SusPaidGridSimulation"
                                            KeyFieldName="KEYS" AutoGenerateColumns="False">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="Simulation" FieldName="KEYS" VisibleIndex="0">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager AlwaysShowPager="false" Mode="ShowAllRecords">
                                            </SettingsPager>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                VerticalScrollableHeight="470" VerticalScrollBarStyle="Standard" />
                                            <Styles>
                                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                </Header>
                                                <Cell HorizontalAlign="Center" Border-BorderStyle="None">
                                                    <Border BorderStyle="None" />
                                                </Cell>
                                            </Styles>
                                            <Templates>
                                                <DetailRow>
                                                    <table cellpadding="1px" cellspacing="0" border="0" style="text-align: left" width="870px">
                                                        <colgroup>
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                            <col width="120px" />
                                                            <col width="100px" />
                                                        </colgroup>
                                                        <tr>
                                                            <td> Transaction Type: </td>
                                                            <td> <%# Eval("TRANSACTION_NAME")%> </td>

                                                            <td> Accrued No: </td>
                                                            <td> <%# Eval("ACCRUED_NO")%> </td>

                                                            <td> Fiscal Year: </td>
                                                            <td> <%# Eval("FISCAL_YEAR")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("DOC_DATE")) %> </td>

                                                            <td> Company Code: </td>
                                                            <td> <%# Eval("COMPANY_CODE")%> </td>
                    
                                                            <td> Period: </td>
                                                            <td> <%# Eval("PERIOD")%> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Ref. Doc.: </td>
                                                            <td> <%# Eval("REF_DOC")%> </td>
                    
                                                            <td> Posting Date: </td>
                                                            <td> <%# string.Format("{0:dd/MM/yyyy}", Eval("POSTING_DATE")) %> </td>
                                                        </tr>
                                                        <tr>
                                                            <td> Doc. Currency: </td>
                                                            <td> <%# Eval("CURRENCY_CODE")%> </td>

                                                            <td> Witholding Tax: </td>
                                                            <td> <%# Eval("WITHOLDING_TAX")%> </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <dx:ASPxGridView ID="SusUnpaidGridPopUpSimulation" runat="server" Width="835px" KeyFieldName="SEQ_NO"
                                                        OnBeforePerformDataSelect="gridPopUpSimulation_DataSelect" AutoGenerateColumns="false"
                                                        ClientInstanceName="SusUnpaidGridPopUpSimulation"
                                                        OnHtmlRowPrepared="SuspenseGridPopUpSimulation_HtmlRowPrepared">
                                                        <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="false">
                                                        </SettingsPager>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn VisibleIndex="0" Width="40px" FieldName="SEQ_NO" Caption="Itm" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="ACCOUNT" Caption="Account"
                                                                Width="110px" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="ACCOUNT_SHORT" Caption="Account short text" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="TX" Caption="Tx" Width="50px"
                                                                CellStyle-HorizontalAlign="Center" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="AMOUNT" Caption="Amount" Width="110px"
                                                                CellStyle-HorizontalAlign="Right" />
                                                            <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="TEXT" Caption="Text" />
                                                        </Columns>
                                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                                        <Styles>
                                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </Header>
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                </DetailRow>
                                            </Templates>
                                            <SettingsDetail ShowDetailRow="true" />
                                            <Settings ShowGroupPanel="false" />
                                            <SettingsCustomizationWindow Enabled="True" />
                                        </dx:ASPxGridView>
                                    </div>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Accrued Journal Simulation" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />



    <div style="clear:both"></div>
    <div style="clear:both"></div>

    <asp:UpdatePanel ID="upnGrid" runat="server">

    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnButtons" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 980px;">
                <tr>
                    <td colspan="3" valign="top" style="width: 270px" align="right">
                        <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
 
</asp:Content>
