﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Data._80Accrued;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using BusinessLogic._80Accrued;
using AjaxControlToolkit;
using Common.Control;
using DevExpress.Web.Data;
using System.Configuration;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrShiftingForm : BaseAccrFormBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8014");
        private string _shiftingNo = null;

        private AccruedShiftingData AccruedData
        {
            get
            {
                var _accruedData = (AccruedShiftingData)ViewState["_AccruedData_ELVIS_Screen_8014"];
                string shiftingNo = GetSessKey();
                if (_accruedData == null && !string.IsNullOrEmpty(shiftingNo))
                {
                    _accruedData = logic
                                    .AccrShifting
                                    .Search(new AccrShiftingSearchCriteria(shiftingNo))
                                    .FirstOrDefault();

                    ViewState["_AccruedData_ELVIS_Screen_8014"] = _accruedData;
                }

                return _accruedData;
            }
            set
            {
                ViewState["_AccruedData_ELVIS_Screen_8014"] = value;
            }
        }
        //private List<>

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }

        public int EditIndex
        {
            get
            {
                if (Session["editIndex"] != null)
                    return Convert.ToInt32(Session["editIndex"]);
                else
                    return 0;
            }

            set
            {
                Session["editIndex"] = value;
            }
        }

        public bool IsEditActivity
        {
            get
            {
                if (Session["IseditAct"] != null)
                    return Convert.ToBoolean(Session["IseditAct"]);
                else
                    return false;
            }

            set
            {
                Session["IseditAct"] = value;
            }
        }

        public string ActivityBookingNo
        {
            get
            {
                if (Session["activityBookNo"] != null)
                    return Convert.ToString(Session["activityBookNo"]);
                else
                    return "";
            }

            set
            {
                Session["activityBookNo"] = value;
            }
        }
        public string IssuingDiv
        {
            get
            {
                if (Session["issueDiv"] != null)
                    return Convert.ToString(Session["issueDiv"]);
                else
                    return "";
            }

            set
            {
                Session["issueDiv"] = value;
            }
        }
        #endregion

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }

        public bool IsNewAccrued
        {
            get
            {
                if (Session["IsNewAccr"] != null)
                    return Convert.ToBoolean(Session["IsNewAccr"]);
                else
                    return false;
            }

            set
            {
                Session["IsNewAccr"] = value;
            }
        }
       

        #region Getter Setter for Data List
        private List<AccruedShiftingData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<AccruedShiftingData>();
                }
                else
                {
                    return (List<AccruedShiftingData>)ViewState["_listInquiry"];
                }
            }
        }

        Dictionary<string, List<string>> listActivity = new Dictionary<string, List<string>>();

        public static readonly string[] enabledFields = new string[]
        {
            "BOOKING_NO","SHIFTING_AMT"
        };

        #endregion
        #region Getter Setter for Attachment List
        [Serializable]
        private class ListAttachmentData
        {
            private string _NO;
            public string NO
            {
                get
                {
                    return _NO;
                }
                set
                {
                    _NO = value;
                }
            }

            private string _FILENAME;
            public string FILENAME
            {
                get
                {
                    return _FILENAME;
                }
                set
                {
                    _FILENAME = value;
                }
            }
        }
        private List<ListAttachmentData> ListAttachment
        {
            set
            {
                ViewState["_listAttachment"] = value;
            }
            get
            {
                if (ViewState["_listAttachment"] == null)
                {
                    return new List<ListAttachmentData>();
                }
                else
                {
                    return (List<ListAttachmentData>)ViewState["_listAttachment"];
                }
            }
        }
        public AttachmentTable AttachmentTable
        {
            get
            {
                string attSessKey = "_attachment_ELVIS_Screen_8014";

                if (Session[attSessKey] == null)
                {
                    Session[attSessKey] = new AttachmentTable();
                }

                return (AttachmentTable)Session[attSessKey];
            }
            set
            {
                Session["_attachment_ELVIS_Screen_8014"] = value;
            }
        }
        public List<FormNote> Notes
        {
            get
            {
                string attSessKey = "_note_ELVIS_Screen_8014";

                if (Session[attSessKey] == null)
                {
                    Session[attSessKey] = new List<FormNote>();
                }

                return (List<FormNote>)Session[attSessKey];
            }
        }

        private Dictionary<string, List<string>> ListActivity 
        {
            set
            {
                Session["_listActivity"] = value;
            }
            get
            {
                if (Session["_listActivity"] == null)
                {
                    return new Dictionary<string, List<string>>();
                }
                else
                {
                    return (Dictionary<string, List<string>>)Session["_listActivity"];
                }
            }
        }
        private List<AccruedListDetail> PopUpActivity
        {
            set
            {
                Session["_popUpActivity"] = value;
            }
            get
            {
                if (Session["_popUpActivity"] == null)
                {
                    return new List<AccruedListDetail>();
                }
                else
                {
                    return (List<AccruedListDetail>)Session["_popUpActivity"];
                }
            }
        }
        private List<AccruedListDetail> TempPopUpActivity
        {
            set
            {
                Session["_tempPopUpActivity"] = value;
            }
            get
            {
                if (Session["_tempPopUpActivity"] == null)
                {
                    return new List<AccruedListDetail>();
                }
                else
                {
                    return (List<AccruedListDetail>)Session["_tempPopUpActivity"];
                }
            }
        }
        private FormPersistence _fp = null;
        private FormPersistence fp
        {
            get
            {
                if (_fp == null)
                    _fp = new FormPersistence();
                return _fp;
            }
        }
        #endregion
        #endregion
       
        #region Notes
        protected void evt_ddlSendTo_onLoad(object sender, EventArgs arg)
        {
            if (AccruedData != null)
                BaseData.ReffNo = AccruedData.REFF_NO;
            else BaseData.ReffNo = null;
            BaseNoteRecipientDropDown = (ASPxComboBox)sender;
            
            List<UserRoleHeader> u = new List<UserRoleHeader>();
            if (BaseData.ReffNo.isNotEmpty())
                u = logic.Notice.GetNoticeUser(BaseData.ReffNo, "");

            BaseNoteRecipientDropDown.DataSource = u;
            BaseNoteRecipientDropDown.DataBind();
        }
        protected void evt_btSendNotice_onClick(object sender, EventArgs e)
        {
            BaseData.AccruedNo = AccruedData.SHIFTING_NO;
            BaseData.CreatedDt = AccruedData.CREATED_DT;
            bool hasWorlist = hasWorklist(BaseData.AccruedNo);
            clearScreenMessage();

            if (BaseNoteRecipientDropDown != null)
            {
                String recipient = Convert.ToString(BaseNoteRecipientDropDown.Value);
                string loggedUserRole = UserData.Roles.FirstOrDefault();
                if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
                    return;
                }
            }

            try
            {
                string strNotice = BaseNoteTextBox.Text;
                if (!String.IsNullOrEmpty(strNotice))
                {
                    string lowerScreenType = BaseScreenType.ToLower();
                    String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
                    String noticeLink = String.Format("{0}://{1}:{2}{3}?accrno={4}&mode=view",
                        Request.Url.Scheme,
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        Request.ServerVariables["URL"],
                        BaseData.AccruedNo);

                    EmailFunction mail = new EmailFunction(emailTemplate);
                    ErrorData err = new ErrorData();
                    List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

                    String userRole = "";
                    String userRoleTo = "";
                    String noticeTo = "";
                    String noticer = "";

                    if (listRole.Any())
                    {
                        userRole = listRole.FirstOrDefault().ROLE_NAME;
                    }

                    List<string> lstUser = null;
                    if (BaseNoteRecipientDropDown.Value != null)
                    {
                        noticeTo = BaseNoteRecipientDropDown.Value.ToString();

                        ErrorData _Err = new ErrorData(0, "", "");
                        List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
                        userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
                        lstUser = logic.Notice.listNoticeRecipient(BaseData.ReffNo, "", UserName, noticeTo);
                    }
                    else
                    {
                        noticeTo = UserName;
                        userRoleTo = "";

                        lstUser = logic.Notice.listNoticeRecipient(BaseData.ReffNo, "", UserName, noticeTo);
                    }

                    if (lstUser == null)
                    {
                        lstUser = new List<string>();
                    }
                    NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

                    logic.Notice.InsertNotice(BaseData.ReffNo.str(),
                                            BaseData.CreatedDt.Year.str(),
                                            UserName,
                                            userRole,
                                            noticeTo,
                                            userRoleTo,
                                            lstUser,
                                            strNotice,
                                            true, hasWorlist);

                    fetchNote();
                    List<FormNote> noticeList = BaseData.Notes;
                    BaseNoteRepeater.DataSource = noticeList;
                    BaseNoteRepeater.DataBind();

                    if (noticeList.Count > 0)
                    {
                        var q = noticeList.Where(a => a.Date != null);
                        if (q.Any())
                        {
                            noticer = q.Last().SenderName;
                        }
                        else
                        {
                            noticer = UserName;
                        }
                    }
                    else
                    {
                        noticer = UserName;
                    }

                    int cntSentMail = 0;
                    if (BaseNoteRecipientDropDown.Value != null)
                    {

                        if (mail.ComposeApprovalNotice_EMAIL(BaseData.ReffNo,
                                                             BaseData.CreatedDt.Year.str(),
                                                             strNotice, noticeLink, noticeTo,
                                                             UserData, noticer,
                                                             BaseData.DivisionID,
                                                             BaseScreenType, "Notice", ref err))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
                            cntSentMail++;
                        }

                        if (cntSentMail > 0)
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF", err.ErrMsg);
                        }

                        if (err.ErrID == 1)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
                        }
                        else if (err.ErrID == 2)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                Handle(ex);
            }
            BaseNoteTextBox.Text = "";

            BaseNoteCommentContainer.Attributes.Remove("style");
            BaseNoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");

            postScreenMessage();
        }
        protected void evt_rptrNotice_onItemDataBound(object sender, RepeaterItemEventArgs arg)
        {
            RepeaterItem item = arg.Item;
            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                Literal litOpenDivNotice = (Literal)item.FindControl("litOpenDivNotice");
                Literal litRptrNoticeComment = (Literal)item.FindControl("litRptrNoticeComment");
                Literal litCloseDivNotice = (Literal)item.FindControl("litCloseDivNotice");
                String dateHeader;

                FormNote note = (FormNote)item.DataItem;

                if (note.Date.HasValue)
                {
                    litOpenDivNotice.Text = "<div class='noticeLeft'>";
                    dateHeader = ((DateTime)note.Date.Value).ToString("d MMMM yyyy hh:mm:ss");
                }
                else
                {
                    litOpenDivNotice.Text = "<div class='noticeRight'>";
                    dateHeader = ((DateTime)note.ReplyDate.Value).ToString("d MMMM yyyy hh:mm:ss");
                }

                litRptrNoticeComment.Text = String.Format("<span class='noteSender'>{0}</span> {1}<br>To: <span class='noteSender'>{2}</span><br>{3}",
                    note.SenderName, dateHeader, note.ReceiverName, note.Message);


                litCloseDivNotice.Text = "</div>";
            }
        }
        protected void reloadNote()
        {
            fetchNote();
            BaseNoteRepeater.DataSource = BaseData.Notes;
            BaseNoteRepeater.DataBind();
        }

        protected void ResetHeader()
        {
            AccruedData = null;
        }

        protected void fetchNote()
        {
            fp.fetchNote(BaseData);
        }
        protected void fetchAttachment(string refNumber, int pid = 0)
        {
            fp.fetchAttachment(refNumber, BaseData.AttachmentTable, pid);
        }
        protected void deleteAttachmentInfo(FormAttachment formAttachment)
        {
            fp.deleteAttachmentInfo(formAttachment);
        }
        #endregion Notes

        #region Event
        protected override void BaseElementInit()
        {
            BaseScreenType = "Shifting";
            BaseScreenID = _ScreenID;
            BaseAttachmentGrid = gridAttachment;
            BaseAttachmentUpload = uploadAttachment;
            BaseAttachmentPopup = popdlgUploadAttachment;
            BaseAttachmentCategoryDropDown = cboxAttachmentCategory;
            BaseNoteRecipientDropDown = ddlSendTo;
            BaseNoteTextBox = txtNotice;
            BaseNoteRepeater = rptrNotice;
            BaseNoteCommentContainer = noticeComment;
            BaseHiddenMessageExpandFlag = hdExpandFlag;
            BaseMessageControlLiteral = messageControl;
        }
        protected override void BaseDataInit()
        {
            BaseData = new BaseAccrData();
            BaseData.Notes = Notes;
            BaseData.AttachmentTable = AttachmentTable;

            if (GetSessKey().isNotEmpty())
                BaseData.AccruedNo = GetSessKey();

            if (AccruedData == null || AccruedData.CREATED_DT == null)
                return;

            BaseData.ReffNo = AccruedData.REFF_NO;
            BaseData.DivisionID = AccruedData.DIVISION_ID;
            BaseData.StatusCode = AccruedData.STATUS_CD;
            BaseData.CreatedDt = AccruedData.CREATED_DT;
        }
        protected void reloadFooter()
        {
            if (BaseData == null)
                return;

            reloadNote();
            reloadAttachment();
        }

        private void OpenEditAttachment()
        {
            if (PageMode == Common.Enum.PageMode.Edit)
            {
                AttachmentTable.openEditing();
            }
            else
            {
                AttachmentTable.closeEditing();
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //Session["shiftingno"] = null;
            base.Page_Init(sender, e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;

            if (!IsPostBack)
            {
                Session["shiftingno"] = null;
                _shiftingNo = Request["shiftingno"];
                SetSessKey(_shiftingNo);
                if (_shiftingNo.Equals("new"))
                {
                    IsNewAccrued = true;
                }
                else
                {
                    IsNewAccrued = false;
                }

                PrepareLogout();
                ResetHeader();
                PrepareDataHeader();
                BaseDataInit();
                OpenEditAttachment();
                //loadApprovalHistory();

                Ticker.In("GenerateComboData");
                Ticker.Out();

                SetFirstLoad();
                SetDataList();
                reloadFooter();

                LoadLookup = logic.AccrShifting.GetDataWbsNoOld(UserData);
                SetSessionLookup(LoadLookup);

                ListActivity = null;

                #region Init Startup Script
                btnDownload.Visible = true;
                btnApprove.Visible = RoLo.isAllowedAccess("btnApprove");
                btnRevise.Visible = RoLo.isAllowedAccess("btnRevise");
                btnPosted.Visible = RoLo.isAllowedAccess("btnPosted");
                btnSaveActivity.Visible = RoLo.isAllowedAccess("btnSaveActivity");
                btnEditRow.Visible = RoLo.isAllowedAccess("btnEditRow")
                    && (logic.AccrShiftingList.isDraft(Session["shiftingNo"].ToString())
                    || logic.AccrShiftingList.isRejected(Session["shiftingNo"].ToString())); ;
                btnSubmit.Visible = RoLo.isAllowedAccess("btnSubmit");

                if (logic.AccrShiftingList.isSubmit(Session["shiftingNo"].ToString()))
                {
                    btnPosted.Visible = false;
                    btnSubmit.Visible = false;
                }

                if (logic.AccrShiftingList.isApproved(Session["shiftingNo"].ToString()))
                {
                    btnSubmit.Visible = false;
                    btnApprove.Visible = false;
                    btnRevise.Visible = false;
                }

                if (logic.AccrShiftingList.isPosted(Session["shiftingNo"].ToString()))
                {
                    btnSubmit.Visible = false;
                    btnApprove.Visible = false;
                    btnRevise.Visible = false;
                    btnPosted.Visible = false;
                }

                if (logic.AccrShiftingList.isDraft(Session["shiftingNo"].ToString()))
                {
                    btnApprove.Visible = false;
                    btnRevise.Visible = false;
                    btnPosted.Visible = false;
                }

                if (logic.AccrShiftingList.isRejected(Session["shiftingNo"].ToString()))
                {
                    btnPosted.Visible = false;
                    btnApprove.Visible = false;
                    btnRevise.Visible = false;
                }

                btnDownload.Visible = false; 
                btnDownload.Enabled = false;

                gridGeneralInfo.CancelEdit();

                if (_shiftingNo.Equals("new"))
                {
                    StartEditing(true);
                }
                #endregion
            }

            clearScreenMessage();
        }

        protected void evaluatePageComponentAvailibility()
        {
            AttachmentTable attachmentTable = AttachmentTable;
            if(PageMode == Common.Enum.PageMode.Edit){
                attachmentTable.Editing = true;
                initFirstRow();
            }
            string loggedUserRole = UserData.Roles.FirstOrDefault();

            AccruedData.CanUploadDetail = UserCanUploadDetail && RoLo.isAllowedAccess("btnUploadTemplate");
        }

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                
            }

            #region rowtype edit
        
            if (e.RowType == GridViewRowType.InlineEdit)
            {
                #region init

                ASPxGridLookup ddlGridWbsNoOld = gridGeneralInfo.FindEditRowCellTemplateControl(
                    gridGeneralInfo.Columns["WBS_NO_OLD"] as GridViewDataColumn, "ddlGridWbsNoOld")
                    as ASPxGridLookup;
                ASPxTextBox txtGridShiftingAmt = gridGeneralInfo.FindEditRowCellTemplateControl(
                    gridGeneralInfo.Columns["SHIFTING_AMT"] as GridViewDataColumn, "txtGridShiftingAmt")
                    as ASPxTextBox;
                ASPxComboBox shiftingType = gridGeneralInfo.FindEditRowCellTemplateControl(
                    gridGeneralInfo.Columns["PV_TYPE_NAME"] as GridViewDataColumn, "shiftingType")
                    as ASPxComboBox;

                #endregion

                #region edit
                if (PageMode == Common.Enum.PageMode.Edit)
                {
                    ListInquiry = GetSessData();

                    int _VisibleIndex = EditIndex;

                    if (_VisibleIndex == e.VisibleIndex)
                    {
                        #region fill data edit

                        //Get value Data Edited
                        string wbsNoOld = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "WBS_NO_OLD"));
                        string shiftingAmt = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "SHIFTING_AMT"));
                        string pvType = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "PV_TYPE_NAME"));

                        //Set SelectBox WBS No (New)
                        //List<WBSStructure> lstWbsNumber = logic.AccrWBS.getWbsNumbers();
                        //ddlGridWBSNoNew.DataSource = lstWbsNumber;
                        //ddlGridWBSNoNew.DataBind();

                        //Set Readonly fields
                        ddlGridWbsNoOld.Value = ListInquiry[_VisibleIndex].WBS_NO_OLD;
                        ddlGridWbsNoOld.Text = ListInquiry[_VisibleIndex].WBS_NO_OLD;
                        txtGridShiftingAmt.Value = ListInquiry[_VisibleIndex].SHIFTING_AMT;
                        shiftingType.Value = ListInquiry[_VisibleIndex].PV_TYPE_NAME;

                        #endregion
                    }
                }
                #endregion
            }
            
            #endregion
        }

        protected void DoSearch()
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
                return;
            }

            if (ValidateInputSearch())
            {
                SetDataList();
            }
        }


        #region Set Button Visible
        private void SetFirstLoad()
        {
            btnSave.Visible = false;
            btnCancel.Visible = false;
            btnClose.Visible = true;
            btnEditRow.Visible = true;
            btnSubmit.Visible = RoLo.isAllowedAccess("btnSubmit");

            if (UserData.Roles.Contains("ELVIS_BUDGET"))
            {
                btnEditRow.Enabled = false;
            }
            else
            {
                btnEditRow.Enabled = true;
            }

            AttachmentTable.Editing = true;
            reloadAttachment();

            GridViewColumn columnDeletion = gridGeneralInfo.Columns["DeletionControl"];
            columnDeletion.Visible = false;
            Session["isEditing"] = "0";
            PageMode = Common.Enum.PageMode.View;
        }

        private void SetSearchLoad(bool noData)
        {
            if (noData)
            {
                SetFirstLoad();
            }
            else
            { 
                btnClose.Visible = true;
            }
        }
        #endregion

        #region Grid

        
        protected void SetDataList()
        {
             string shiftingNo = GetSessKey();

             if (!shiftingNo.isEmpty())
             {
                 ListInquiry = logic.AccrShifting.SearchForm(shiftingNo).ToList();
                 int count = 1;
                 foreach (var data in ListInquiry)
                 {
                     data.DisplaySequenceNumber = count;
                     count++;
                 }
             }
             else
             {
                 ListInquiry = null;
             }
           
            SetSessData(ListInquiry);
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
        }

        #endregion
        #endregion

        #region Buttons

        private string[] paramMessage { get; set; }
        private string nagMessage { get; set; }
        protected bool ValidateEditing()
        {
            bool result = true;

            foreach (var p in ListInquiry)
            {
                if (p.WBS_NO_OLD == "" || p.SHIFTING_AMT == 0 || p.PV_TYPE_NAME == "" || p.PV_TYPE_NAME == null)
                {
                    nagMessage = "MSTD00225WRN";
                    paramMessage = null;
                    result = false;
                    break;
                }

                if (p.PV_TYPE_CD == 1)
                {
                    if (p.SHIFTING_AMT > (p.REMAINING_DIRECT ?? 0 ))
                    {
                        nagMessage = "MSTD00083ERR";
                        paramMessage = new string[] { "Shifting From", "Remaining Direct" };
                        result = false;
                        break;
                    }
                }

                if (p.PV_TYPE_CD == 4)
                {
                    if (p.SHIFTING_AMT > (p.REMAINING_PR ?? 0))
                    {
                        nagMessage = "MSTD00083ERR";
                        paramMessage = new string[] { "Shifting From", "Remaining PR/PO" };
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        protected void BtnCancelConfirmationSave_Click(object sender, EventArgs e)
        {
            ReloadGrid();
            ConfirmationSavePopUp.Hide();
        }
        protected bool ValidationBookingNo()
        {
            bool valid = true;

            var listBnDirect = ListInquiry.Select(x => x.BOOKING_NO_DIRECT).ToList();
            var listBnPr = ListInquiry.Select(x => x.BOOKING_NO_PR).ToList();
            var listBalDirect = logic.AccrBalance.GetBalancesByBookingNo(listBnDirect);
            var listBalPr = logic.AccrBalance.GetBalancesByBookingNo(listBnPr);
            var closedBalDirect = listBalDirect.Where(x => (x.BOOKING_STS ?? 0) == 1).Select(x => x.BOOKING_NO).ToList();
            var closedBalPr = listBalPr.Where(x => (x.BOOKING_STS ?? 0) == 1).Select(x => x.BOOKING_NO).ToList();
            var expBalDirect = listBalDirect.Where(x => (x.EXPIRED_DT ?? DateTime.Now) < DateTime.Now).Select(x => x.BOOKING_NO).ToList();
            var expBalPr = listBalPr.Where(x => (x.EXPIRED_DT ?? DateTime.Now) < DateTime.Now).Select(x => x.BOOKING_NO).ToList();

            if (closedBalDirect.Any())
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No Direct No is Closed: " + CommonFunction.CommaJoin(closedBalDirect));
                valid = false;
            }
            if (closedBalPr.Any())
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No PR/PO No is Closed: " + CommonFunction.CommaJoin(closedBalPr));
                valid = false;
            }

            if (expBalDirect.Any())
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No Direct No is Expired: " + CommonFunction.CommaJoin(expBalDirect));
                valid = false;
            }
            if (expBalPr.Any())
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No Direct No is Expired: " + CommonFunction.CommaJoin(expBalPr));
                valid = false;
            }

            return valid;
        }
        protected void btnOkConfirmationSave_Click(object sender, EventArgs e)
        {
            logic.Say("btnOkConfirmationSave_Click", "Ok");
            clearScreenMessage();

            if (PageMode == Common.Enum.PageMode.Edit)
            {
                string shiftingNo = GetSessKey();

                //var listBnDirect = ListInquiry.Select(x => x.BOOKING_NO_DIRECT).ToList();
                //var listBnPr = ListInquiry.Select(x => x.BOOKING_NO_PR).ToList();
                //var listBalDirect = logic.AccrBalance.GetBalancesByBookingNo(listBnDirect);
                //var listBalPr = logic.AccrBalance.GetBalancesByBookingNo(listBnPr);
                //var closedBalDirect = listBalDirect.Where(x => (x.BOOKING_STS ?? 0) == 1).Select(x => x.BOOKING_NO).ToList();
                //var closedBalPr = listBalPr.Where(x => (x.BOOKING_STS ?? 0) == 1).Select(x => x.BOOKING_NO).ToList();
                //var expBalDirect = listBalDirect.Where(x => (x.EXPIRED_DT ?? DateTime.Now) < DateTime.Now).Select(x => x.BOOKING_NO).ToList();
                //var expBalPr = listBalPr.Where(x => (x.EXPIRED_DT ?? DateTime.Now) < DateTime.Now).Select(x => x.BOOKING_NO).ToList();

                //bool valid = true;
                //if (closedBalDirect.Any())
                //{
                //    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No Direct No is Closed: " + CommonFunction.CommaJoin(closedBalDirect));
                //    valid = false;
                //}
                //if (closedBalPr.Any())
                //{
                //    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No PR/PO No is Closed: " + CommonFunction.CommaJoin(closedBalPr));
                //    valid = false;
                //}

                //if (expBalDirect.Any())
                //{
                //    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No Direct No is Expired: " + CommonFunction.CommaJoin(expBalDirect));
                //    valid = false;
                //}
                //if (expBalPr.Any())
                //{
                //    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "These Booking No Direct No is Expired: " + CommonFunction.CommaJoin(expBalPr));
                //    valid = false;
                //}

                if (ValidationBookingNo())
                {
                    if (shiftingNo.isNotEmpty())
                    {
                        bool isSaved = logic.AccrShifting.UpdateDetail(shiftingNo, ListInquiry, UserData, PageMode);
                        if (isSaved)
                        {
                            SetDataList();
                            ResetHeader();
                            PrepareDataHeader();
                            SetFirstLoad();
                            gridGeneralInfo.CancelEdit();
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00223INF", "Accrued Shifting");
                        }
                        else
                        {
                            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00224ERR", "Accrued Shifting");
                        }
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00223ERR", "Accrued Shifting");
                    }
                }
            }

            ReloadGrid();
            postScreenMessage();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            logic.Say("btnSave_Click", "Save");

            if (PageMode == Common.Enum.PageMode.Edit)
            {
                
                //GetNewShiftingNo();
                string shiftingNo = GetSessKey();

                if (!string.IsNullOrEmpty(shiftingNo))
                {
                    bool isValid = ValidateEditing();
                    if (isValid)
                    {
                        lblConfirmationSave.Text = _m.Message("MSTD00225CON", "save");
                        ConfirmationSavePopUp.Show();
                    }
                    else
                    {
                        Nag(nagMessage, paramMessage);
                        ReloadGrid();
                    }
                }
                else
                {
                    GetNewShiftingNo();
                    bool isValid = ValidateEditing();
                    if (isValid)
                    {
                        lblConfirmationSave.Text = _m.Message("MSTD00225CON", "save");
                        ConfirmationSavePopUp.Show();
                    }
                    else
                    {
                        Nag(nagMessage, paramMessage);
                        ReloadGrid();
                    }
                    //Nag("MSTD00223ERR", "Accrued Shifting");
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            PageMode = Common.Enum.PageMode.View;
            SetFirstLoad();
            SetDataList();

            ListActivity = null;

            #region Init Startup Script

            gridGeneralInfo.CancelEdit();

            #endregion

        }
        protected void btEdit_Click(object sender, EventArgs e)
        {
            logic.Say("btEdit_Click", "Edit");
            
            StartEditing();
        }

        protected void StartEditing(bool isNew = false)
        {

            if (isNew)
            {
                SetSessData(null);
                AddBlankColumn();
            }

            PageMode = Common.Enum.PageMode.Edit;
            OpenEditAttachment();
            updateAttachmentList();
            btnSave.Visible = true;
            btnCancel.Visible = !isNew;
            btnEditRow.Visible = false;
            btnSubmit.Visible = false;
            btnRevise.Visible = false;
            btnApprove.Visible = false;
            btnPosted.Visible = false;
            //clearScreenMessage();
            Session["isEditing"] = "1";
            AttachmentTable.Editing = true;
            reloadAttachment();
            UpdateRemainAmount();
            ReloadGrid();
            StartEditingTabel();
        }

        protected void StartEditingTabel()
        {
            GridViewColumn column = gridGeneralInfo.Columns["DeletionControl"];
            column.Visible = true;


            gridGeneralInfo.CancelEdit();
            int cntData = ListInquiry.Count();
            if (cntData > 0)
            {
                ASPxGridLookup ddlGridShiftingNoOld = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["WBS_NO_OLD"] as GridViewDataColumn, "ddlGridWbsNoOld")
                        as ASPxGridLookup;

                //ASPxTextBox txtGridSapAvailable = gridGeneralInfo.FindEditRowCellTemplateControl(
                //        gridGeneralInfo.Columns["BOOKING_NO"] as GridViewDataColumn, "txtGridSapAvailable")
                //        as ASPxTextBox;

                string wbsNoOld = Convert.ToString(gridGeneralInfo.GetRowValues(cntData - 1, "WBS_NO_OLD"));
                Session["editIndex"] = cntData - 1;
                gridGeneralInfo.StartEdit(cntData - 1);
                //ddlGridShiftingNoOld.Value = bookingNo;
            }
            else
            {
                initFirstRow();
            }
        }
        public void initFirstRow()
        {
            AddBlankColumn(); // todo
            ReloadGrid();
            gridGeneralInfo.StartEdit(0);
            EditIndex = 0;
            gridGeneralInfo.SettingsBehavior.AllowFocusedRow = false;
        }

        private void AddBlankColumn(int idxNew = 1)
        {
            AccruedShiftingData temp = new AccruedShiftingData();
            temp.DisplaySequenceNumber = idxNew;
            temp.SHIFTING_NO = GetSessKey();
            ListInquiry = GetSessData();
            ListInquiry.Add(temp);
            SetSessData(ListInquiry);
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //logic.Say("btnSearch_Click", "Search {0} {1}", ddlSuspenseNo.Text, txtPVYear.Text);
            DoSearch();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            //if (IssuingDivisionDropDown.Enabled)
            //    sel.Clear();
            //txtPVYear.Text = null;
            //txtAccrNo.Text = null;
            //ddlSubSts.SelectedIndex = 0;
            //dtCreatedDateFrom.Text = null;
            //dtCreatedDateTo.Text = null;
            //ddlWorkflowStatus.SelectedIndex = 0;
            //txtTotAmountFrom.Text = null;
            //txtTotAmountTo.Text = null;

            gridGeneralInfo.DataSource = null;
            gridGeneralInfo.DataSourceID = null;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            UserData userdata = (UserData)Session["UserData"];
            try
            {
                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "AccruedShiftingReportTemplate.xls");

                string excelName = "Accrued_Shifting_Report_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                string pdfName = "Accrued_Shifting_Report_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                string fullPath = logic.AccrShifting.GenerateShiftingFormReport(
                            UserName,
                            templatePath,
                            excelName,
                            GetSessKey(),
                            IssuingDiv,
                            userdata,
                            pdfName
                        );
                Xmit.Transmit(HttpContext.Current, fullPath);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    string xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                    Nag("MSTD00042ERR", ex.Message);
                }
            }

            //return fileName;
        
        }
        protected void ReArrangeAttachments(string pre, string post, string yy)
        {
            string olde = pre + yy;
            string neo = post + yy;
            if (!pre.StartsWith("T")) return;
            List<FormAttachment> atts = fp.getAttachments(olde);
            string opath = "";
            foreach (FormAttachment a in atts)
            {
                string oldFile = a.PATH + "/" + a.FileName;
                opath = a.PATH;
                string newFile = "";
                string[] p = a.PATH.Split('/');

                if (p.Length > 0)
                {
                    p[p.Length - 1] = post;
                    newFile = string.Join("/", p, 0, p.Length) + "/" + a.FileName;
                }
                ftp.Move(oldFile, newFile);
                fp.MoveAttachment(pre, a.SequenceNumber, post, yy);
            }
            ftp.RemoveDir(opath);
        }
        
        protected void evt_cboxAttachmentCategory_onLoad(object sender, EventArgs args)
        {
            List<CodeConstant> a = fp.getAttachmentCategories();
            BaseAttachmentCategoryDropDown.DataSource = a;
            BaseAttachmentCategoryDropDown.DataBind();
        }
        protected void evt_btCloseUploadAttachment_clicked(object sender, EventArgs arg)
        {
            BaseAttachmentPopup.Hide();
        }

        protected AttachID GetAttachID()
        {
            AttachID a = new AttachID()
            {
                ReffNo = AccruedData.SHIFTING_NO,
                Seq = 0
            };

            return a;
        }

        protected void evt_btSendUploadAttachment_clicked(object sender, EventArgs arg)
        {
            if (BaseAttachmentUpload.HasFile)
            {
                logic.Say("btSendUploadAttachment_Click", "FileName = {0}", BaseAttachmentUpload.FileName);
                string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                int maxSize = int.Parse(strMaxSize);
                maxSize = maxSize * 1024;
                if (BaseAttachmentUpload.FileBytes.Length > maxSize)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                    BaseAttachmentPopup.Hide();
                    return;
                }

                bool uploadSucceded = true;
                string errorMessage = null;

                AttachID a = GetAttachID();
                string filename = CommonFunction.CleanFilename(BaseAttachmentUpload.FileName);
                ftp.ftpUploadBytes(a.Year, BaseScreenType,
                                a.ReffNo,
                                filename,
                                BaseAttachmentUpload.FileBytes,
                                ref uploadSucceded,
                                ref errorMessage);

                ListEditItem item = BaseAttachmentCategoryDropDown.SelectedItem;
                if (item.Value == null)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                }

                FormAttachment attachment = BaseData.AttachmentTable.getBlankAttachment();
                attachment.ReferenceNumber = a.ReffNo;
                if (item != null)
                {
                    attachment.CategoryCode = item.Value.ToString();
                    attachment.CategoryName = item.Text;
                }
                attachment.PATH = a.Year + "/" + BaseScreenType + "/" + a.ReffNo;
                attachment.FileName = filename;
                attachment.Blank = false;
                fp.saveAttachmentInfo(attachment, UserData);

                fetchAttachment(a.ReffNo, ProcessID);
                updateAttachmentList();
            }
            BaseAttachmentPopup.Hide();
        }
        protected void evt_imgAddAttachment_onClick(object sender, EventArgs arg)
        {
            BaseAttachmentPopup.Show();
        }
        protected void evt_gridAttachment_onHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs arg)
        {
            BaseAttachmentGrid = (ASPxGridView)sender;

            if (arg.RowType != GridViewRowType.Data)
            {
                return;
            }

            decimal key = (decimal)arg.KeyValue;
            if (key > 0)
            {
                FormAttachment attachment = BaseData.AttachmentTable.get((int)key);
                if (attachment != null)
                {
                    KeyImageButton imgAdd = (KeyImageButton)BaseAttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgAddAttachment");
                    if (imgAdd != null)
                    {
                        imgAdd.Visible = attachment.Blank;
                    }
                    KeyImageButton imgDelete = (KeyImageButton)BaseAttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgDeleteAttachment");
                    if (imgDelete != null)
                    {
                        int statusCd = BaseData.StatusCode ?? 0;
                        bool canDelAttach = statusCd == 0 || statusCd == 112;

                        if (attachment.Blank || !canDelAttach)
                            imgDelete.CssClass = "hidden";
                        else
                            imgDelete.CssClass = "poited";
                    }
                }
            }
        }

        protected void evt_imgDeleteAttachment_onClick(object sender, EventArgs arg)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            int keyNum = int.Parse(key);
            logic.Say("evt_imgDeleteAttachment_onClick", "Key= {0}", keyNum);

            FormAttachment attachment = BaseData.AttachmentTable.get(keyNum);
            if ((attachment != null) && (!attachment.Blank))
            {
                AttachID a = GetAttachID();

                string folderPath = a.Year + "/" + BaseScreenType + "/" + a.ReffNo;
                ftp.Delete(folderPath, attachment.FileName);
                deleteAttachmentInfo(attachment);

                fetchAttachment(a.ReffNo, ProcessID);
                updateAttachmentList();
            }
        }

        protected void evt_imgDeleteRow_clicked(object sender, EventArgs args)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string sequenceNumber = image.Key;
            int numSequenceNumber = int.Parse(sequenceNumber.Trim());
            logic.Say("evt_imgDeleteRow_clicked", "seq={0}", numSequenceNumber);
            gridGeneralInfo.CancelEdit();
            int removedList = numSequenceNumber - 1;

            //string removedKey = ListInquiry[removedList].KEYS;
            //ListActivity.Remove(removedKey);

            ListInquiry = GetSessData();
            ListInquiry.RemoveAt(removedList);

            for (int idx = 0; idx < ListInquiry.Count; idx++)
            {
                ListInquiry[idx].DisplaySequenceNumber = idx + 1;
            }

            SetSessData(ListInquiry);
            ReloadGrid();
            if(EditIndex > removedList){
                EditIndex = EditIndex - 1;
            }
            gridGeneralInfo.StartEdit(EditIndex);
            
        }
        protected void evt_imgDeleteAllRow_clicked(object sender, EventArgs args)
        {
            logic.Say("evt_imgDeleteAllRow_clicked", "Delete All Row");

            ListActivity = null;
            ListInquiry = null;
            SetSessData(ListInquiry);
            EditIndex = 0;
            AddBlankColumn();
            ReloadGrid();
            gridGeneralInfo.StartEdit(0);
        }
        protected void evt_imgAddRow_clicked(object sender, EventArgs arg)
        {
            logic.Say("evt_imgAddRow_clicked", "Add Row {0}", ListInquiry.Count + 1);

            gridGeneralInfo.CancelEdit();
            //gridGeneralInfo.UpdateEdit();
            int NewDataIndex = ListInquiry.Count + 1;
            AddBlankColumn(NewDataIndex);
            EditIndex = ListInquiry.Count - 1;
            ReloadGrid();
            gridGeneralInfo.StartEdit(EditIndex);
        }
        public void AddNewRowToGrid()
        {
            
        }

        protected void evt_gridGeneralInfo_onCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs arg)
        {
            
            string fieldName = arg.Column.FieldName;

            if (enabledFields.Contains(fieldName))
            {
                arg.Editor.Enabled = true;
            }
            else
            {
                arg.Editor.Enabled = false;
            }
        }


        //protected void evt_ddlSendTo_onLoad(object sender, EventArgs arg)
        //{
        //    BaseNoteRecipientDropDown = (ASPxComboBox)sender;

        //    List<UserRoleHeader> u = new List<UserRoleHeader>();
        //    if (BaseData.ReffNo.isNotEmpty())
        //        u = logic.Notice.GetNoticeUser(BaseData.ReffNo, "");

        //    BaseNoteRecipientDropDown.DataSource = u;
        //    BaseNoteRecipientDropDown.DataBind();
        //}
        //protected void evt_btSendNotice_onClick(object sender, EventArgs e)
        //{
        //    bool hasWorlist = hasWorklist(AccruedData.SHIFTING_NO);
        //    clearScreenMessage();

        //    if (BaseNoteRecipientDropDown != null)
        //    {
        //        String recipient = Convert.ToString(BaseNoteRecipientDropDown.Value);
        //        string loggedUserRole = UserData.Roles.FirstOrDefault();
        //        if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
        //        {
        //            PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
        //            return;
        //        }
        //    }

        //    try
        //    {
        //        string strNotice = BaseNoteTextBox.Text;
        //        if (!String.IsNullOrEmpty(strNotice))
        //        {
        //            string lowerScreenType = BaseScreenType.ToLower();
        //            String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
        //            String noticeLink = String.Format("{0}://{1}:{2}{3}?accrno={4}&mode=view",
        //                Request.Url.Scheme,
        //                Request.ServerVariables["SERVER_NAME"],
        //                HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
        //                Request.ServerVariables["URL"],
        //                AccruedData.SHIFTING_NO);

        //            EmailFunction mail = new EmailFunction(emailTemplate);
        //            ErrorData err = new ErrorData();
        //            List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

        //            String userRole = "";
        //            String userRoleTo = "";
        //            String noticeTo = "";
        //            String noticer = "";

        //            if (listRole.Any())
        //            {
        //                userRole = listRole.FirstOrDefault().ROLE_NAME;
        //            }

        //            List<string> lstUser = null;
        //            if (BaseNoteRecipientDropDown.Value != null)
        //            {
        //                noticeTo = BaseNoteRecipientDropDown.Value.ToString();

        //                ErrorData _Err = new ErrorData(0, "", "");
        //                List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
        //                userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
        //                lstUser = logic.Notice.listNoticeRecipient(AccruedData.REFF_NO, "", UserName, noticeTo);
        //            }
        //            else
        //            {
        //                noticeTo = UserName;
        //                userRoleTo = "";

        //                lstUser = logic.Notice.listNoticeRecipient(AccruedData.REFF_NO, "", UserName, noticeTo);
        //            }

        //            if (lstUser == null)
        //            {
        //                lstUser = new List<string>();
        //            }
        //            NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

        //            logic.Notice.InsertNotice(AccruedData.REFF_NO.str(),
        //                                    AccruedData.CREATED_DT.Year.str(),
        //                                    UserName,
        //                                    userRole,
        //                                    noticeTo,
        //                                    userRoleTo,
        //                                    lstUser,
        //                                    strNotice,
        //                                    true, hasWorlist);

        //            fetchNote();
        //            List<FormNote> noticeList = BaseData.Notes;
        //            BaseNoteRepeater.DataSource = noticeList;
        //            BaseNoteRepeater.DataBind();

        //            if (noticeList.Count > 0)
        //            {
        //                var q = noticeList.Where(a => a.Date != null);
        //                if (q.Any())
        //                {
        //                    noticer = q.Last().SenderName;
        //                }
        //                else
        //                {
        //                    noticer = UserName;
        //                }
        //            }
        //            else
        //            {
        //                noticer = UserName;
        //            }

        //            int cntSentMail = 0;
        //            if (BaseNoteRecipientDropDown.Value != null)
        //            {

        //                //if (mail.ComposeApprovalNotice_EMAIL(AccruedData.REFF_NO,
        //                //                                     AccruedData.CREATED_DT.Year.str(),
        //                //                                     strNotice, noticeLink, noticeTo,
        //                //                                     UserData, noticer,
        //                //                                     AccruedData.DIVISION_ID,
        //                //                                     BaseScreenType, "Notice", ref err))
        //                //{
        //                //    PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
        //                //    cntSentMail++;
        //                //}

        //                if (true)
        //                {
        //                    PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
        //                    cntSentMail++;
        //                }

        //                if (cntSentMail > 0)
        //                {
        //                    PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF", err.ErrMsg);
        //                }

        //                if (err.ErrID == 1)
        //                {
        //                    PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
        //                }
        //                else if (err.ErrID == 2)
        //                {
        //                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
        //        Handle(ex);
        //    }
        //    BaseNoteTextBox.Text = "";

        //    BaseNoteCommentContainer.Attributes.Remove("style");
        //    BaseNoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");

        //    postScreenMessage();
        //}

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            //string bookingNo = gridGeneralInfo.GetRowValues(e.VisibleIndex, "BOOKING_NO").ToString();

            //ViewActivity(bookingNo);
        }
        

        #endregion

        #region Function

        public void ReloadGrid()
        {
            LoadLookup = GetSessLookup();
            ListInquiry = GetSessData();
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
        }

        public void OnTabKeypress(bool taxCalculated)
        {
            gridGeneralInfo.UpdateEdit();
            //int idxSelected = Tabel.EditIndex;
            //int lastDataIndex = Tabel.DataList.Count - 1;
            //if (Tabel.EditIndex == lastDataIndex)
            //{
            //    Tabel.addNewRow(taxCalculated);
            //    idxSelected = Tabel.EditIndex + 1;
            //}
            //else
            //{
            //    idxSelected++;
            //}

            //ReloadGrid();
            int idxSelected = 1;
            gridGeneralInfo.StartEdit(idxSelected);
            //renderTotalAmount();
        }
        private bool ValidateInputSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Handle(ex);
                throw ;
            }
            return true;
        }

        private void Focus_On_Next_Empty_TextBox(int currentIndex)
        {
            bool setFocus = false;
            if (gridGeneralInfo.VisibleRowCount > 0)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (txtGridSpentAmount != null && (String.IsNullOrWhiteSpace(txtGridSpentAmount.Text)))
                    {
                        txtGridSpentAmount.Focus();
                        setFocus = true;
                        break;
                    }
                }

                if (!setFocus)
                {
                    ASPxTextBox nxtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(currentIndex + 1,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (nxtGridSpentAmount != null)
                    {
                        nxtGridSpentAmount.Focus();
                    }
                }
            }
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.PVList.GetUserDivisions(UserData);
        }

        protected string Status_Color(Object statusValue)
        {
            string color = "";
            short statusInt = 0;
            if (statusValue != null && Int16.TryParse(statusValue.ToString(), out statusInt))
            {
                if (statusInt == 1)
                    color = red;
                else if (statusInt == 2)
                    color = yellow;
                else if (statusInt > 2)
                    color = green;
            }
            return color;
        }

        protected string WriteSelectedIndexInfo(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }


        protected void evt_gridPVDetail_onCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs arg)
        {
            string fieldName = arg.Column.FieldName;



            if (VouCol.disabledFields.Contains(fieldName))
            {
                arg.Editor.Enabled = false;
            }
        }

        private AccruedShiftingData SelectLookup(string key)
        {
            AccruedShiftingData data = new AccruedShiftingData();
            List<AccruedShiftingData> Data = GetSessLookup();
            for (int i = 0; i < Data.Count; i++)
            {
                if (Data[i].KEYS == key)
                {
                    data = Data[i];
                    break;
                }
            }
            
            return data;
        }

        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs arg)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string param = arg.Parameters;
            if (param.isEmpty()) return;
            string[] paramFracs = param.Split(':');
            string prefix = (paramFracs != null && paramFracs.Length > 0) ? paramFracs[0] : "";
            if (!prefix.Equals("p")) return;
            string command = (paramFracs != null && paramFracs.Length > 1) ? paramFracs[1] : "";
            if (!command.Equals("update")) return;
            string field = (paramFracs != null && paramFracs.Length > 2) ? paramFracs[2] : "";

            int _VisibleIndex = EditIndex;

            ListInquiry = GetSessData();
            if (field.Equals("keys"))
            {
                string keys = (paramFracs != null && paramFracs.Length > 3) ? paramFracs[3] : "";
                if (!string.IsNullOrEmpty(keys))
                {
                    AccruedShiftingData temp = SelectLookup(keys);
                    if (temp != null)
                    {
                        ListInquiry[_VisibleIndex].WBS_NO_OLD = temp.WBS_NO_OLD;
                        ListInquiry[_VisibleIndex].WBS_DESC_OLD = temp.WBS_DESC_OLD;
                        ListInquiry[_VisibleIndex].WBS_NO_PR = temp.WBS_NO_PR;
                        ListInquiry[_VisibleIndex].WBS_DESC_PR = temp.WBS_DESC_PR;
                        ListInquiry[_VisibleIndex].BOOKING_NO_PR = temp.BOOKING_NO_PR;
                        ListInquiry[_VisibleIndex].REMAINING_PR= temp.REMAINING_PR;
                        ListInquiry[_VisibleIndex].BOOKING_NO_DIRECT= temp.BOOKING_NO_DIRECT;
                        ListInquiry[_VisibleIndex].REMAINING_DIRECT= temp.REMAINING_DIRECT;
                    }
                }
            }
            else if (field.Equals("shiftingAmt"))
            {
                decimal shiftingAmt = (paramFracs != null && paramFracs.Length > 3) ? Convert.ToDecimal(paramFracs[3]) : 0;
                ListInquiry[_VisibleIndex].SHIFTING_AMT = shiftingAmt;
            }
            else if (field.Equals("shiftingType"))
            {
                string shiftingType = (paramFracs != null && paramFracs.Length > 3) ? paramFracs[3] : "";
                string shiftingTypeLabel = (paramFracs != null && paramFracs.Length > 4) ? paramFracs[4] : "";
                ListInquiry[_VisibleIndex].PV_TYPE_CD = Convert.ToInt32(shiftingType);
                ListInquiry[_VisibleIndex].PV_TYPE_NAME = shiftingTypeLabel;
                ListInquiry[_VisibleIndex].SHIFTING_FROM = Convert.ToInt32(shiftingType);
            }
            SetSessData(ListInquiry);
            gridGeneralInfo.CancelEdit();
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
            gridGeneralInfo.StartEdit(_VisibleIndex);
        }

        protected void gridActivity_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs arg)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string param = arg.Parameters;
            if (param.isEmpty()) return;
            string[] paramFracs = param.Split(':');
            string prefix = (paramFracs != null && paramFracs.Length > 0) ? paramFracs[0] : "";
            if (!prefix.Equals("p")) return;
            string command = (paramFracs != null && paramFracs.Length > 1) ? paramFracs[1] : "";
            if (!command.Equals("update")) return;
            string field = (paramFracs != null && paramFracs.Length > 2) ? paramFracs[2] : "";
            Dictionary<string, List<string>> savedActivity = ListActivity;
            List<string> listSelectedAct = new List<string>();
            TempPopUpActivity = PopUpActivity;
            string index = (paramFracs != null && paramFracs.Length > 3) ? paramFracs[3] : "";
            if (!string.IsNullOrEmpty(index))
            {
                int idx = Convert.ToInt32(index);
                string selectedAct = PopUpActivity[idx].ACTIVITY_DES;
                
                if (field.Equals("active"))
                {
                    //user checking new item
                    TempPopUpActivity[idx].IS_CHECKED = true;
                }
                else
                {
                    //user unchecking exist item
                    TempPopUpActivity[idx].IS_CHECKED = false;
                }
                ListActivity = savedActivity;
            }

            gridActivity.DataSource = TempPopUpActivity;
            gridActivity.DataBind();
            
        }

        protected void evt_gridGeneralInfo_onRowUpdating(object sender, ASPxDataUpdatingEventArgs args)
        {
            //decimal amt = 0;
            //OnRowUpdating(sender, args);
        }

        protected void evt_gridGeneralInfo_onSelectionChanged(object sender, EventArgs args)
        {

            if (PageMode == Common.Enum.PageMode.Edit)
            {
                logic.Say("evt_gridGeneralInfo_onSelectionChanged", "Change Row");
                int i = getSelectedRow();
                gridGeneralInfo.CancelEdit();
                EditIndex = i;
                ReloadGrid();
                gridGeneralInfo.StartEdit(EditIndex);
            }
            else
            {
                return;
            }
            
            //int i = OnSelectionChanged(sender, args);
            //string cc = "", des = "", curr = "", gla = "";
            //decimal amt = 0;
            //int seq = 0;
            //if (i >= 0 && i < Tabel.DataList.Count)
            //{
            //    seq = Tabel.DataList[i].SequenceNumber;
            //    cc = Tabel.DataList[i].CostCenterCode;
            //    curr = Tabel.DataList[i].CurrencyCode;
            //    des = Tabel.DataList[i].Description;
            //    amt = Tabel.DataList[i].Amount;
            //    gla = Tabel.DataList[i].GlAccount;
            //}
            //logic.Say("evt_gridPVDetail_onSelectionChanged", "{0} OnSelectionChanged [{1}] @{2} - {3} '{4}' {5} {6} {7}",
            //    ScreenType, i, seq, cc, des, curr, amt, gla);
            
        }

        public int getSelectedRow()
        {
            ReloadGrid();
            WebDataSelection selection = gridGeneralInfo.Selection;
            int cntData = ListInquiry.Count;
            for (int i = 0; i < cntData; i++)
            {
                if (selection.IsRowSelected(i))
                {
                    return i;
                }
            }
            return 0;
        }
        protected void PrepareDataHeader()
        {
            if (AccruedData == null)
            {
                IssuingDiv = normalizeDivisionName(UserData.DIV_NAME);
                tboxIssueDiv.Text = IssuingDiv;
                if (IsNewAccrued)
                {
                    string AccrShiftingNo = GetNewShiftingNo();
                    //tboxAccruedNo.Text = AccrExtNo;
                    tboxTotAmt.Text = "0";

                }
            }
            else
            {
                this.OpenEditAttachment();
                IssuingDiv = normalizeDivisionName(logic.Vendor.GetDivisionName(AccruedData.DIVISION_ID));
                tboxIssueDiv.Text = IssuingDiv;
                tboxTotAmt.Text = AccruedData.TOT_AMOUNT.fmt(0);
                tboxAccruedNo.Text = AccruedData.SHIFTING_NO;
                tboxPicCur.Text = AccruedData.PIC_CURRENT;
                tboxSubmSts.Text = AccruedData.SUBMISSION_STATUS;
                tboxUpdateDt.Text = AccruedData.CHANGED_DT.HasValue ? AccruedData.CHANGED_DT.Value.ToString("dd.MM.yyyy") : "";
                tboxPicNext.Text = AccruedData.PIC_NEXT;
                if(AccruedData.WORKFLOW_STATUS != null){
                    tboxWFSts.Text = logic.Sys.GetText("WORKFLOW_STATUS", AccruedData.WORKFLOW_STATUS);
                }
                else
                {
                    tboxWFSts.Text = "";
                }
            }

            

            var ExchRate = logic.Look.getExchangeRates();

            tboxUsdIdr.Text = ExchRate
                .Where(x => x.CurrencyCode == "USD")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);

            tboxJpyIdr.Text = ExchRate
                .Where(x => x.CurrencyCode == "JPY")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);
        }

        protected string normalizeDivisionName(string divName)
        {
            if (divName != null)
            {
                if (divName.ToLower().EndsWith("head"))
                {
                    divName = divName.Substring(0, divName.IndexOf('-'));
                }
            }
            return divName;
        }

        private string GetNewShiftingNo()
        {
            string shiftingNo = "BS-" + IssuingDiv + "-" + (DateTime.Now.Year - 1).ToString() + "-"; // cuman buat testing tar ilangin -1 nya // sori ini udah bener XDDD
            List<AccruedShiftingData> listHeadAccr = logic.AccrShifting.GetListDivAccrShifting(shiftingNo).ToList();
            int maxNumb = 0;
            foreach (AccruedShiftingData temp in listHeadAccr)
            {
                string[] splitedshifting = temp.SHIFTING_NO.Split('-');
                int NumbInRow = Convert.ToInt32(splitedshifting[splitedshifting.Length - 1]);
                if (NumbInRow > maxNumb)
                {
                    maxNumb = NumbInRow;
                }
            }
            maxNumb++;
            string seqShiftingNo = maxNumb.ToString();
            string AccrShiftingNo = shiftingNo + seqShiftingNo.PadLeft(2, '0');
            SetSessKey(AccrShiftingNo);
            return AccrShiftingNo;
        }

        protected int GetFirstSelectedRow()
        {
            int row = -1;
            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                if (!gridGeneralInfo.Selection.IsRowSelected(i)) continue;

                row = i;
                break;
            }
            return row;
        }
        #endregion

        private int GetEditIndex()
        {
            int editIndex = 0;
            if (Session["editIndex"] != null)
            {
                editIndex = Convert.ToInt32(Session["editIndex"]);
            }
            return editIndex;
        }

        protected void LookupWbsNoOld_Load(object sender, EventArgs arg)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                return;
            }

            ASPxGridLookup lookup = (ASPxGridLookup)sender;

            //List<AccruedShiftingData> lstShiftingNoOld = logic.AccrShifting.GetDataWbsNoOld(UserData).Distinct().ToList();
            List<AccruedShiftingData> lstShiftingNoOld = GetSessLookup();
            lookup.DataSource = lstShiftingNoOld;
            lookup.DataBind();
        }

        private List<AccruedShiftingData> dataLoadLookup { get; set; }

        public List<AccruedShiftingData> LoadLookup
        {
            get
            {
                return dataLoadLookup;
            }
            set
            {
                dataLoadLookup = value;
            }
        }


        protected void gridActivity_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region edit
            if (PageMode == Common.Enum.PageMode.Edit)
            {
                if (gridActivity.VisibleRowCount > 0)
                {

                    if (IsEditActivity)
                    {
                        for (int i = 0; i < gridActivity.VisibleRowCount; i++)
                        {
                            ASPxCheckBox x = (ASPxCheckBox)gridActivity.FindRowCellTemplateControl(i,
                                (GridViewDataColumn)gridActivity.Columns["checkList"], "checkList");
                            if(x != null)
                                x.ReadOnly = false;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < gridActivity.VisibleRowCount; i++)
                        {
                            ASPxCheckBox x = (ASPxCheckBox)gridActivity.FindRowCellTemplateControl(i,
                                (GridViewDataColumn)gridActivity.Columns["checkList"], "checkList");
                            if (x != null)
                                x.ReadOnly = true;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < gridActivity.VisibleRowCount; i++)
                {
                    ASPxCheckBox x = (ASPxCheckBox)gridActivity.FindRowCellTemplateControl(i,
                        (GridViewDataColumn)gridActivity.Columns["checkList"], "checkList");
                    if (x != null)
                        x.ReadOnly = true;
                }
            }
            #endregion
        }

        private void SetSessionLookup(List<AccruedShiftingData> data)
        {
            LoadLookup = data;
            Session["dataLookup"] = data;
        }
        private void UpdateRemainAmount()
        {
            ListInquiry = logic.AccrShifting.UpdateRemainAmount(ListInquiry);
            Session["dataGrid"] = ListInquiry;
        }
        private void SetSessData(List<AccruedShiftingData> p){
            ListInquiry = p;
            Session["dataGrid"] = p;
        }
        private List<AccruedShiftingData> GetSessData()
        {
            List<AccruedShiftingData> ret = (List<AccruedShiftingData>)Session["dataGrid"];
            return ret;
        }
        private List<AccruedShiftingData> GetSessLookup()
        {
            List<AccruedShiftingData> ret = (List<AccruedShiftingData>)Session["dataLookup"];
            return ret;
        }
        private void SetSessKey(string shiftingNo)
        {
            Session["shiftingNo"] = shiftingNo;
        }
        private string GetSessKey()
        {
            return Convert.ToString(Session["shiftingNo"] ?? "");
        }

        private bool ValidateSave()
        {
            ListInquiry = GetSessData();
            var bookingExist = ListInquiry.GroupBy(x => x.BOOKING_NO).Any(g => g.Count() > 1);
            foreach(AccruedShiftingData t in ListInquiry){

            }

            return true;
        }

        protected void btnAvtivity_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            IsEditActivity = false;
            if (PageMode == Common.Enum.PageMode.Edit)
            {
                ViewActivity(s.CommandName,1);
            }
            else
            {
                ViewActivity(s.CommandName);
            }
            
        }
        protected void btnAddAvtivity_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            ListInquiry = GetSessData();
            IsEditActivity = true;
            ActivityBookingNo = ListInquiry[Convert.ToInt32(s.CommandName)].BOOKING_NO;
            ViewActivity(ActivityBookingNo, 1);
        }

        protected void ViewActivity(string bookingNo, int type = 0)
        {
            logic.Say("btnActivity_Click", "Activity");
            panelActivity.GroupingText = "Simulation";
            gridActivity.BeginUpdate();
            gridActivity.Visible = true;
            string shiftingNo = GetSessKey();
            List<AccruedListDetail> _list = null;
            List<string> _listActChecked = new List<string>();

            if (IsEditActivity)
            {
                btnSaveActivity.Visible = true;
                btnCancelActivity.Visible = true;
                btnCloseActivity.Visible = false;
            }
            else
            {
                btnSaveActivity.Visible = false;
                btnCancelActivity.Visible = false;
                btnCloseActivity.Visible = true;
            }

            if (type == 0)
            {
                _list = logic.AccrShifting.GetCurrentActivity(shiftingNo, bookingNo);
                foreach (AccruedListDetail temp in _list)
                {
                    _listActChecked.Add(temp.ACTIVITY_DES);
                }
            }
            else
            {
                if (ListActivity.ContainsKey(bookingNo))
                {
                    _listActChecked = ListActivity[bookingNo];
                }
                _list = logic.AccrShifting.GetCurrentActivity(shiftingNo, bookingNo);
                foreach (AccruedListDetail temp in _list)
                {
                    if (!_listActChecked.Contains(temp.ACTIVITY_DES))
                    {
                        _listActChecked.Add(temp.ACTIVITY_DES);
                    }
                    
                }
                ListActivity[bookingNo] = _listActChecked;
            }
            _list = logic.AccrShifting.GetActivityList(bookingNo);
            if (_list != null)
            {
                for (int i = 0; i < _list.Count; i++ )
                {
                    if (_listActChecked.Contains(_list[i].ACTIVITY_DES))
                    {
                        _list[i].IS_CHECKED = true;
                    }
                }
            }

            PopUpActivity = _list;
            gridActivity.DataSource = _list;
            gridActivity.DataBind();

            lblBookingNo.Text = bookingNo;

            gridActivity.DetailRows.ExpandAllRows();
            gridActivity.EndUpdate();
            (popupActivity as ModalPopupExtender).Show();
        }

        protected void btnCloseActivity_Click(object sender, EventArgs e)
        {
            lblBookingNo.Text = "";
            (popupActivity as ModalPopupExtender).Hide();
        }

        protected void btnSaveActivity_Click(object sender, EventArgs e)
        {
            Dictionary<string, List<string>> savedActivity = ListActivity;
            List<string> listSelectedAct = new List<string>();
            PopUpActivity = TempPopUpActivity;
            foreach (AccruedListDetail temp in PopUpActivity)
            {
                if (temp.IS_CHECKED)
                {
                    listSelectedAct.Add(temp.ACTIVITY_DES);
                }
            }
            if (savedActivity.ContainsKey(ActivityBookingNo))
            {
                savedActivity[ActivityBookingNo] = listSelectedAct;
            }
            else
            {
                savedActivity.Add(ActivityBookingNo, listSelectedAct);
            }
            TempPopUpActivity = null;
            PopUpActivity = null;
            lblBookingNo.Text = "";
            (popupActivity as ModalPopupExtender).Hide();
        }
        protected void btnCancelActivity_Click(object sender, EventArgs e)
        {
            TempPopUpActivity = null;
            PopUpActivity = null;
            lblBookingNo.Text = "";
            (popupActivity as ModalPopupExtender).Hide();
        }

        protected void ddlShiftingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = 0;
        }

        protected void shiftingType_Load(object sender, EventArgs e)
        {
            ASPxComboBox co = (ASPxComboBox)sender;


            co.DataSource = logic.AccrShifting.getShiftingType();
            co.DataBind();

            if (gridGeneralInfo.IsEditing)
            {
                int idxSelected = gridGeneralInfo.EditingRowVisibleIndex;
                ListInquiry = GetSessData();
                int cntData = ListInquiry.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccruedShiftingData detail = ListInquiry[idxSelected];
                    co.Value = detail.Code;
                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            logic.Say("btSubmit_Click", "Submit");
            clearScreenMessage();

            try
            {
                bool valid = ValidationBookingNo();

                if (AccruedData.STATUS_CD != 0 && AccruedData.STATUS_CD != 411)
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00218ERR", "Draft or Reject by budget", "submitted");
                    //Nag("MSTD00218ERR", "Draft or Reject by budget", "submitted");
                    valid = false;
                }
                
                if (AttachmentTable.Attachments.Count < 2)
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00229ERR", "Submit", "attachement(s).");
                    //Nag("MSTD00229ERR", "Submit", "attachement(s).");
                    valid = false;
                }

                if (valid)
                {
                    logic.Say("btSubmit_Click", "K2StartWorklist K2 {0}", AccruedData.SHIFTING_NO);

                    K2DataField k2Data = new K2DataField();
                    k2Data.Command = resx.K2ProcID("Form_Registration_Submit_New");
                    k2Data.folio = AccruedData.SHIFTING_NO;
                    k2Data.Reff_No = AccruedData.REFF_NO;
                    k2Data.ApprovalLevel = "1";
                    k2Data.ApproverClass = "1";
                    k2Data.ApproverCount = "1";
                    k2Data.CurrentDivCD = UserData.DIV_CD.ToString();
                    k2Data.CurrentPIC = UserData.USERNAME;
                    k2Data.EmailAddress = UserData.EMAIL;
                    k2Data.Entertain = "0";
                    k2Data.K2Host = "";
                    k2Data.LimitClass = "0";
                    k2Data.ModuleCD = "4";
                    k2Data.NextClass = "0";
                    k2Data.PIC = UserData.DIV_CD.ToString();
                    k2Data.RegisterDt = string.Format("{0:G}", DateTime.Now);
                    k2Data.RejectedFinance = "0";
                    k2Data.StatusCD = AccruedData.STATUS_CD.ToString();
                    k2Data.StepCD = "";
                    k2Data.VendorCD = "0";
                    k2Data.TotalAmount = ListInquiry.Sum(x => x.TOT_AMOUNT).ToString();

                    if (AccruedData.STATUS_CD != 0)
                    {
                        k2Data.isResubmitting = true;
                    }

                    bool result = logic.WorkFlow.K2StartWorklist(k2Data, UserData);

                    if (result)
                    {
                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Submit");
                        //Nag("MSTD00060INF", "Submit");
                        SetDataList();
                        ResetHeader();
                        PrepareDataHeader();
                        
                        btnSubmit.Visible = false;
                        btnEditRow.Visible = false;
                        AccruedData.STATUS_CD = 401;
                        tboxSubmSts.Text = logic.AccrForm.GetStatusName(AccruedData.STATUS_CD);
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Submit");
                        //Nag("MSTD00062ERR", "Submit");
                    }
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Submit");
                //Nag("MSTD00062ERR", "Submit");
                Handle(ex);
            }
            ReloadOpener();

            postScreenMessage();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            string loc = "btnApprove_Click";
            logic.Say(loc, "Approve Shifting {0} by {1} on status {2}", AccruedData.SHIFTING_NO, UserData.USERNAME, AccruedData.STATUS_CD);
            clearScreenMessage();

            // do validation here
            bool valid = ValidationBookingNo();

            if (valid)
            {
                string command = resx.K2ProcID("Form_Registration_Approval");
                // Call K2
                bool success = logic.PVRVWorkflow.Go(AccruedData.REFF_NO, command, UserData, "N/A", false, AccruedData.SHIFTING_NO);
                if (success)
                {
                    int newStatus = 0;
                    if (WaitForK2UpdateStatus(out newStatus))
                    {
                        //AccruedData.STATUS_CD = 402;
                        //tboxSubmSts.Text = logic.AccrForm.GetStatusName(AccruedData.STATUS_CD);
                        ResetHeader();
                        PrepareDataHeader();
                        tboxSubmSts.Text = logic.AccrForm.GetStatusName(newStatus);
                        btnPosted.Visible = true;
                        btnApprove.Visible = false;
                        btnRevise.Visible = false;
                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00227INF", "Approve");
                        //Nag("MSTD00227INF", "Approve");
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_WARNING, "MSTD00082WRN");
                        //Nag("MSTD00082WRN");
                    }
                }
                else
                {
                    logic.Say(loc, "Process approval to K2 failed");
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Approve");
                    //Nag("MSTD00062ERR", "Approve");
                }
            }
            ReloadOpener();

            postScreenMessage();
        }

        protected void btnReviseCancel_Click(object sender, EventArgs e)
        {
            popdlgRevise.Hide();
        }
        protected void evt_btnReviseProceed_Click(object sender, EventArgs e)
        {
            popdlgRevise.Hide();
            string loc = "evt_btnReviseProceed_Click";
            logic.Say(loc, "Revise Shifting {0} by {1} on status {2}", AccruedData.SHIFTING_NO, UserData.USERNAME, AccruedData.STATUS_CD);

            bool success = postNotice();
            if (success)
            {
                doReject(loc);
            }
            ReloadOpener();
        }
        protected bool postNotice()
        {
            bool result = true;
            bool hasWorlist = hasWorklist(AccruedData.SHIFTING_NO);
            clearScreenMessage();

            if (BaseNoteRecipientDropDown != null)
            {
                String recipient = Convert.ToString(UserData.USERNAME);
                string loggedUserRole = UserData.Roles.FirstOrDefault();
                if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
                    return false;
                }
            }

            try
            {
                string strNotice = txtReviseComment.Text;
                if (!String.IsNullOrEmpty(strNotice))
                {
                    string lowerScreenType = BaseScreenType.ToLower();
                    String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
                    String noticeLink = String.Format("{0}://{1}:{2}{3}?shiftingno={4}&mode=view",
                        Request.Url.Scheme,
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        Request.ServerVariables["URL"],
                        AccruedData.SHIFTING_NO);

                    EmailFunction mail = new EmailFunction(emailTemplate);
                    ErrorData err = new ErrorData();
                    List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

                    String userRole = "";
                    String userRoleTo = "";
                    String noticeTo = "";
                    String noticer = "";

                    if (listRole.Any())
                    {
                        userRole = listRole.FirstOrDefault().ROLE_NAME;
                    }

                    List<string> lstUser = null;
                    if (BaseNoteRecipientDropDown.Value != null)
                    {
                        noticeTo = UserData.USERNAME;

                        ErrorData _Err = new ErrorData(0, "", "");
                        List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
                        userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
                        lstUser = logic.Notice.listNoticeRecipient(AccruedData.REFF_NO, "", UserName, noticeTo);
                    }
                    else
                    {
                        noticeTo = UserName;
                        userRoleTo = "";

                        lstUser = logic.Notice.listNoticeRecipient(AccruedData.REFF_NO, "", UserName, noticeTo);
                    }

                    if (lstUser == null)
                    {
                        lstUser = new List<string>();
                    }
                    NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

                    logic.Notice.InsertNotice(AccruedData.REFF_NO.str(),
                                            AccruedData.CREATED_DT.Year.str(),
                                            UserName,
                                            userRole,
                                            noticeTo,
                                            userRoleTo,
                                            lstUser,
                                            strNotice,
                                            true, hasWorlist);

                    fetchNote();
                    List<FormNote> noticeList = BaseData.Notes;
                    BaseNoteRepeater.DataSource = noticeList;
                    BaseNoteRepeater.DataBind();

                    if (noticeList.Count > 0)
                    {
                        var q = noticeList.Where(a => a.Date != null);
                        if (q.Any())
                        {
                            noticer = q.Last().SenderName;
                        }
                        else
                        {
                            noticer = UserName;
                        }
                    }
                    else
                    {
                        noticer = UserName;
                    }

                    int cntSentMail = 0;
                    if (UserData.USERNAME != null)
                    {

                        if (mail.ComposeApprovalNotice_EMAIL(AccruedData.REFF_NO,
                                                             AccruedData.CREATED_DT.Year.str(),
                                                             strNotice, noticeLink, noticeTo,
                                                             UserData, noticer,
                                                             AccruedData.DIVISION_ID,
                                                             BaseScreenType, "Notice", ref err))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
                            cntSentMail++;
                        }

                        if (err.ErrID == 1)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
                            result = false;
                        }
                        else if (err.ErrID == 2)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
                            result = false;
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                Handle(ex);
                result = false;
            }
            BaseNoteTextBox.Text = "";

            BaseNoteCommentContainer.Attributes.Remove("style");
            BaseNoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");

            postScreenMessage();
            return result;
        }

        protected void doReject(string loc)
        {
            // do validation here
            bool valid = true;

            if (valid)
            {
                string command = resx.K2ProcID("Form_Registration_Rejected");

                // Call K2
                bool success = logic.PVRVWorkflow.Go(AccruedData.REFF_NO, command, UserData, "N/A", false, AccruedData.SHIFTING_NO);
                if (success)
                {
                    int newStatus = 0;
                    if (WaitForK2UpdateStatus(out newStatus, 200))
                    {
                        ResetHeader();
                        PrepareDataHeader();
                        resetWorklist();

                        string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                        if (!Common.AppSetting.NoticeMailUsePort)
                            port = "";
                        string linkEmail = ResolveClientUrl(string.Format("{0}://{1}{2}{3}?shiftingno={4}&mode=view",
                                            Request.Url.Scheme,
                                            Request.ServerVariables["SERVER_NAME"],
                                            port,
                                            "/80Accrued/AccrShiftingForm.aspx",
                                            AccruedData.SHIFTING_NO));

                        if (SendEmail(linkEmail))
                        {
                            logic.Say("SendEmail", "SendEmail Shifting {0} finished successfully", AccruedData.SHIFTING_NO);
                        }
                        else
                        {
                            logic.Say("SendEmail", "SendEmail Shifting {0} finished with failed", AccruedData.SHIFTING_NO);
                        }

                        btnApprove.Visible = false;
                        btnRevise.Visible = false;

                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Revise");
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00082WRN");
                    }
                }
                else
                {
                    logic.Say(loc, "Process approval to K2 failed");
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Revise");
                }
            }
            PrepareDataHeader();
            postScreenMessage();
        }

        protected bool SendEmail(string linkEmail)
        {
            String emailTemplate = String.Format("{0}\\AccruedApproval.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));

            EmailFunction mail = new EmailFunction(emailTemplate);

            string[] mailTo = null;
            if (AccruedData.STATUS_CD != 403)
            {
                var recipient = logic.Role.getUserRoleHeader(AccruedData.PIC_NEXT);
                if (recipient == null)
                    return false;

                mailTo = new string[] { recipient.EMAIL };
            }
            else
            {
                var listRecipient = logic.Role.getUserWorklist(AccruedData.REFF_NO);
                if (listRecipient == null || listRecipient.Count == 0)
                    return false;

                mailTo = listRecipient.Select(x => x.EMAIL).ToArray();
            }
            logic.Say("SendEmail", "SendEmail Shifting Approval {0} to : {1}. [link:{2}]"
                , AccruedData.SHIFTING_NO
                , string.Join(",", mailTo)
                , linkEmail);

            ErrorData err = null;
            if (mail.ComposeAccruedApproval_EMAIL(4, AccruedData.SHIFTING_NO, mailTo, linkEmail, ref err))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnRevise_Click(object sender, EventArgs e)
        {
            string shiftingNo = AccruedData.SHIFTING_NO;
            if (!string.IsNullOrEmpty(shiftingNo))
            {
                // do validation here
                bool valid = true;
                if (valid)
                {
                    txtReviseShiftingNo.Text = shiftingNo;
                    ddlReviseCategory.Items.Clear();
                    ddlReviseCategory.Items.Add("Reject");
                    ddlReviseCategory.SelectedIndex = 0;
                    txtReviseComment.Text = "";
                    popdlgRevise.Show();
                }
                else
                {
                    Nag("MSTD00230ERR", shiftingNo);
                }
            }

            //string loc = "btnRevise_Click";
            //logic.Say(loc, "Revise Shifting {0} by {1} on status {2}", AccruedData.SHIFTING_NO, UserData.USERNAME, AccruedData.STATUS_CD);

            //// do validation here
            //bool valid = true;

            //if (valid)
            //{
            //    string command = resx.K2ProcID("Form_Registration_Rejected");
            //    // Call K2
            //    bool success = logic.PVRVWorkflow.Go(AccruedData.REFF_NO, command, UserData, "N/A", false, AccruedData.SHIFTING_NO);
            //    if (success)
            //    {
            //        int newStatus = 0;
            //        if (WaitForK2UpdateStatus(out newStatus))
            //        {
            //            //AccruedData.STATUS_CD = 411;
            //            //tboxSubmSts.Text = logic.AccrForm.GetStatusName(AccruedData.STATUS_CD);
            //            ResetHeader();
            //            PrepareDataHeader();
            //            tboxSubmSts.Text = logic.AccrForm.GetStatusName(newStatus);
            //            Nag("MSTD00060INF", "Reject");
            //        }
            //        else
            //        {
            //            Nag("MSTD00082WRN");
            //        }
            //    }
            //    else
            //    {
            //        logic.Say(loc, "Process approval to K2 failed");
            //        Nag("MSTD00062ERR", "Reject");
            //    }
            //}
        }

        private bool isValidPosted()
        {
            bool result = false;

            if (AccruedData.STATUS_CD == 402) result = true;

            return result;
        }
        protected void BtnCancelConfirmationPost_Click(object sender, EventArgs e)
        {
            ConfirmationPostPopUp.Hide();
        }
        protected void btnOkConfirmationPost_Click(object sender, EventArgs e)
        {
            logic.Say("btnOkConfirmationPost_Click", "Ok");
            clearScreenMessage();

            if (PageMode == Common.Enum.PageMode.View)
            {
                string shiftingNo = GetSessKey();
                bool valid = ValidationBookingNo();
                if (!string.IsNullOrEmpty(shiftingNo) && valid)
                {
                    try
                    {
                        var dataSAP = (from i in ListInquiry
                                       where i.SAP_DOC_NO == "" || i.SAP_DOC_NO == null
                                       select new Common.Data.SAPData.ShiftingBudget
                                       {
                                           BUDGET_NO = i.WBS_NO_PR,
                                           SHIFTING_AMT = i.SHIFTING_AMT,
                                           SHIFTING_TYPE = i.SHIFTING_FROM == 1 ? "FD" : "FP",
                                           SHIFTING_DESC = string.Format("Shifting {0}", AccruedData.SHIFTING_NO)
                                       }).ToList();

                        var o = logic.SAP.ShiftingBudget(dataSAP, UserData.USERNAME);

                        List<string> errMsg = new List<string>();

                        bool isInserted = logic.AccrShifting.InsertSAP(o, AccruedData.REFF_NO, AccruedData.SHIFTING_NO, ref errMsg);
                        if (isInserted)
                        {
                            string command = resx.K2ProcID("Form_Registration_Approval");
                            // Call K2
                            bool success = logic.PVRVWorkflow.Go(AccruedData.REFF_NO, command, UserData, "N/A", false, shiftingNo);
                            if (success)
                            {
                                int newStatus = 0;
                                if (WaitForK2UpdateStatus(out newStatus))
                                {
                                    bool isUpdated = logic.AccrShifting.UpdateBalance(AccruedData.SHIFTING_NO, ListInquiry, UserData);
                                    if (isUpdated)
                                    {
                                        ResetHeader();
                                        PrepareDataHeader();
                                        SetFirstLoad();
                                        SetDataList();
                                        gridGeneralInfo.CancelEdit();
                                        btnPosted.Visible = false;
                                        btnEditRow.Visible = false;
                                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00228INF", AccruedData.SHIFTING_NO);
                                    }
                                    else
                                    {
                                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00228ERR", "Accrued Shifting");
                                    }
                                }
                                else
                                {
                                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00228ERR", "Accrued Shifting");
                                }
                            }
                            else
                            {
                                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Failed to approve data.");
                            }
                        }
                        else
                        {
                            if (errMsg.Count > 0)
                            {
                                foreach (string msg in errMsg)
                                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", msg);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Handle(ex);
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00228ERR", "Accrued Shifting");
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00228ERR", "Accrued Shifting");
                }
            }
             
            postScreenMessage();
        }
        protected void btnPosted_Click(object sender, EventArgs e)
        {
            string loc = "btnPosted_Click";
            logic.Say(loc, "Post to SAP {0} by {1} on status {2}", AccruedData.SHIFTING_NO, UserData.USERNAME, AccruedData.STATUS_CD);

            // do validation here
            bool valid = this.isValidPosted();

            if (valid)
            {
                lblConfirmationPost.Text = _m.Message("MSTD00227CON", AccruedData.SHIFTING_NO);
                ConfirmationPostPopUp.Show();
            }
            else
            {
                logic.Say(loc, "Post to SAP {0} is never approved by budget.", AccruedData.SHIFTING_NO);
                Nag("MSTD00227ERR", AccruedData.SHIFTING_NO);
            }
        }

        private bool WaitForK2UpdateStatus(out int newStatus, int maxLoop = 800)
        {
            Ticker.In("WaitForK2UpdateStatus");
            int oldStatus = AccruedData.STATUS_CD;
            int i = 0;

            do
            {
                System.Threading.Thread.Sleep(100 + ((i * 20) % 250));
                newStatus = logic.AccrShifting.GetLatestStatus(AccruedData.SHIFTING_NO);
                i++;
            } while (oldStatus == newStatus && i < maxLoop);

            return i < maxLoop;
        }

        private void loadApprovalHistory()
        {
            List<AccrFormHistory> data = logic.AccrForm.getApprovalHistory(AccruedData.SHIFTING_NO, AccruedData.STATUS_CD, UserData);
            gridApprovalHistory.DataSource = data;
            gridApprovalHistory.DataBind();
        }
        protected void gridApprovalHistory_Load(object sender, EventArgs e)
        {
            if (!Request["shiftingno"].Equals("new"))
                loadApprovalHistory();
        }

        protected void gridApprovalHistory_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                AccrFormHistory data = (AccrFormHistory)gridApprovalHistory.GetRow(e.VisibleIndex);
                if (data != null)
                {
                    Image imgStatus = gridApprovalHistory.FindRowCellTemplateControl(e.VisibleIndex, gridApprovalHistory.Columns["status"] as GridViewDataColumn, "imgStatus") as Image;

                    string img = "wait";
                    int stsCd = data.STATUS;
                    if (stsCd == 1)
                    {
                        img = "check";
                    }
                    else if (stsCd == 2)
                    {
                        img = "cross";
                    }


                    string logoPath = AppSetting.CompanyLogo.Substring(1, AppSetting.CompanyLogo.LastIndexOf("/"));
                    imgStatus.ImageUrl = logoPath + img + ".gif";
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
        }

        protected void gridGeneralInfo_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            int curRow = e.VisibleIndex;
            if (e.DataColumn == null) return;
            if (AccruedData == null) return;

            if (AccruedData.STATUS_CD == 403)
            {
                if (e.DataColumn.FieldName == "REMAINING_PR" || e.DataColumn.FieldName == "REMAINING_DIRECT")
                {
                    e.Cell.Style.Add("background-color", grey);
                }
            }
        }

        
    }
}