﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._80Accrued;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using Common.Messaging;
using BusinessLogic._80Accrued;
using AjaxControlToolkit;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrBalanceList : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8009");
        private string susNo, susYear;
        private int suNO, suYY;
        private SelectableDropDownHelper sel = null;
        public string _IssuingDivision = "";
        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }
        #endregion

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }


        #region Getter Setter for Data List
        private List<AccruedListDetail> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<AccruedListDetail>();
                }
                else
                {
                    return (List<AccruedListDetail>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #region SUSPENSE AND WBS

        private List<WBSStructure> _listWbsNoOld
        {
            set
            {
                ViewState["_listWbsNoOld"] = value;
            }
            get
            {
                if (ViewState["_listWbsNoOld"] == null)
                {
                    return new List<WBSStructure>();
                }
                else
                {
                    return (List<WBSStructure>)ViewState["_listWbsNoOld"];
                }
            }
        }

        private List<WBSStructure> _listWbsNoNew
        {
            set
            {
                ViewState["_listWbsNoNew"] = value;
            }
            get
            {
                if (ViewState["_listWbsNoNew"] == null)
                {
                    return new List<WBSStructure>();
                }
                else
                {
                    return (List<WBSStructure>)ViewState["_listWbsNoNew"];
                }
            }
        }

        private List<SuspenseComboStructure> _getSuspenseOld
        {
            set
            {
                ViewState["_getSuspenseOld"] = value;
            }
            get
            {
                if (ViewState["_getSuspenseOld"] == null)
                {
                    return new List<SuspenseComboStructure>();
                }
                else
                {
                    return (List<SuspenseComboStructure>)ViewState["_getSuspenseOld"];
                }
            }
        }

        private List<SuspenseComboStructure> _getSuspenseNew
        {
            set
            {
                ViewState["_getSuspenseNew"] = value;
            }
            get
            {
                if (ViewState["_getSuspenseNew"] == null)
                {
                    return new List<SuspenseComboStructure>();
                }
                else
                {
                    return (List<SuspenseComboStructure>)ViewState["_getSuspenseNew"];
                }
            }
        }

        #endregion

        #region Getter Setter for Attachment List
        [Serializable]
        private class ListAttachmentData
        {
            private string _NO;
            public string NO
            {
                get
                {
                    return _NO;
                }
                set
                {
                    _NO = value;
                }
            }

            private string _FILENAME;
            public string FILENAME
            {
                get
                {
                    return _FILENAME;
                }
                set
                {
                    _FILENAME = value;
                }
            }
        }
        private List<ListAttachmentData> ListAttachment
        {
            set
            {
                ViewState["_listAttachment"] = value;
            }
            get
            {
                if (ViewState["_listAttachment"] == null)
                {
                    return new List<ListAttachmentData>();
                }
                else
                {
                    return (List<ListAttachmentData>)ViewState["_listAttachment"];
                }
            }
        }
        #endregion
        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;

            sel = new SelectableDropDownHelper(IssuingDivisionDropDown, "IssuingDivisionListBox");
            ASPxListBox divBox = (ASPxListBox)IssuingDivisionDropDown.FindControl("IssuingDivisionListBox");

            if (divBox != null) 
            {
                divBox.SelectedIndexChanged += (o, args) =>
                {
                    var _retData = new List<ComboClassData>();
                    var dataAll = new ComboClassData { COMBO_CD = "", COMBO_NAME = "All" };
                    _retData.Insert(0, dataAll);
                    if (string.IsNullOrEmpty(IssuingDivisionDropDown.Text))
                    {
                        const string _clear = "";
                        _IssuingDivision = "'" + _clear + "'";

                    }
                    else
                    {
                        _IssuingDivision = "'" + IssuingDivisionDropDown.Text.Trim().Replace("; ", ";").Replace(";", "','") + "'";

                        //suspenseWbsCombo(_IssuingDivision);
                    }
                    //suspenseWbsCombo(_IssuingDivision);
                };
            }




            if (!IsPostBack && !IsCallback)
            {
                if (divBox != null)
                {
                    sel.SelectAll();
                    _IssuingDivision = "'" + string.Join("','", (from object x in divBox.SelectedItems select Convert.ToString(x)).ToArray()) + "'";
                    //suspenseWbsCombo(_IssuingDivision);

                }



                PrepareLogout();

                Ticker.In("GenerateComboData");
                var _whereCondition = " AND DIVISION_ID IN (SELECT DIVISION_ID FROM vw_Division WHERE DIVISION_NAME IN (" + _IssuingDivision + "))";
                GenerateComboData(ddlOpenStatus, "STATUS_OPEN_CLOSED", true);
                GenerateComboData(ddlExtend, "YN_COMMON", true);
                GenerateComboData(ddlShifted, "YN_COMMON", true);
                GenerateComboData(ddlPvType, "ACCR_PV_TYPE", true);
                Ticker.Out();
                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();

                gridGeneralInfo.DataSource = null;
                 //suspenseWbsCombo(_IssuingDivision);
                #region Init Startup Script

                btnCloseAccrued.Visible = RoLo.isAllowedAccess("btnCloseAccrued");
                AddEnterComponent(IssuingDivisionDropDown);
                AddEnterComponent(txtBookingNo);
                AddEnterComponent(txtBDYear);
                AddEnterComponent(dtExpiryDateFrom);
                AddEnterComponent(dtExpiryDateTo);
                AddEnterComponent(dtPostingDt);

                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                dtPostingDt.MaxDate = lastDayOfMonth;

                if (logic.User.IsAuthorizedFinance || logic.User.IsDirectorUp || UserData.Roles.Contains("ELVIS_ADMIN"))
                {
                    AddEnterComponent(IssuingDivisionDropDown);
                }
                else
                {
                    IssuingDivisionDropDown.Enabled = false;
                }
                AddEnterAsSearch(btnSearch, _ScreenID);

                txtBDYear.Text = DateTime.Now.AddYears(-1).Year.ToString(); //add FID.Ridwan 13072018
                
                DoSearch();
                #endregion
            }
        }

        protected void suspenseWbsCombo(string _IssuingDivision)
        {

            ddlWBSNoOld.DataSourceID = LinqDataSource2.ID;
            ddlWBSNoOld.DataBind();
            ddlWBSNoNew.DataSourceID = LinqDataSource3.ID;
            ddlWBSNoNew.DataBind();

            ddlSusNoOld.DataSourceID = LinqDataSource4.ID;
            ddlSusNoOld.DataBind();
            ddlSusNoNew.DataSourceID = LinqDataSource5.ID;
            ddlSusNoNew.DataBind();

            //ddlWBSNoOld.DataSource = logic.AccrBalance.getWbsNoOld(_IssuingDivision);
            //ddlWBSNoOld.DataBind();
            //ddlWBSNoNew.DataSource = logic.AccrBalance.getWbsNoNew(_IssuingDivision);
            //ddlWBSNoNew.DataBind();

            //ddlSusNoOld.DataSource = logic.AccrBalance.getSuspenseOld(_IssuingDivision);
            //ddlSusNoOld.DataBind();
            //ddlSusNoNew.DataSource = logic.AccrBalance.getSuspenseNew(_IssuingDivision);
            //ddlSusNoNew.DataBind();
        }

        protected void btClearDivisionList_OnClick(object sender, EventArgs e)
        {

        }

        protected void btSelectAllDiv_OnClick(object sender, EventArgs e)
        {
            sel.SelectAll();
            var _retData = new List<ComboClassData>();
            var dataAll = new ComboClassData { COMBO_CD = "", COMBO_NAME = "All" };
            _retData.Insert(0, dataAll);
            _IssuingDivision = "'" + IssuingDivisionDropDown.Text.Trim().Replace("; ", ";").Replace(";", "','") + "'";
            //suspenseWbsCombo(_IssuingDivision);
        }

        protected void DoSearch()
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
                return;
            }

            if (ValidateInputSearch())
            {

                //SetDataGrid();
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;

                gridGeneralInfo.DataBind();

                _IssuingDivision = "'" + IssuingDivisionDropDown.Text.Trim().Replace("; ", ";").Replace(";", "','") + "'";

                //suspenseWbsCombo(_IssuingDivision);
            }
        }
        protected void gridSapDocNo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoSapDocNo = gridSapDocNo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridSapDocNo.Columns["NO"] as GridViewDataColumn, "litGridNoSapDocNo") as Literal;
                litGridNoSapDocNo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonClosePopUpSapDocNo_Click(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = false;
            ModalPopupSapDocNo.Hide();
        }

        protected void LoadGridPopUpSapDocNo(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = true;
            LitMsgSapDocNo.Text = "";
            panelSapDocNo.GroupingText = "SAP Document Number";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            string bookingNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "BOOKING_NO").str();

            List<SapDocNoData> _list = logic.Look.GetSapDocNoByBookNo(bookingNo, "");
            gridSapDocNo.DataSource = _list;
            gridSapDocNo.DataBind();
            ModalPopupSapDocNo.Show();
        }

        #region Set Button Visible
        private void SetFirstLoad()
        {
            lbPostingDt.Visible = RoLo.isAllowedAccess("btnCloseAccrued");
            dtPostingDt.Visible = RoLo.isAllowedAccess("btnCloseAccrued");
            btnSearch.Visible = RoLo.isAllowedAccess("btnSearch");
            btnClear.Visible = RoLo.isAllowedAccess("btnClear");
            btnCloseAccrued.Visible = RoLo.isAllowedAccess("btnCloseAccrued");
            btnSimulate.Visible = RoLo.isAllowedAccess("btnSimulate");
            btnDownload.Visible = true;
        }

        private void SetSearchLoad(bool noData)
        {
            if (noData)
            {
                SetFirstLoad();
            }
            else
            {
                lbPostingDt.Visible = RoLo.isAllowedAccess("btnCloseAccrued");
                dtPostingDt.Visible = RoLo.isAllowedAccess("btnCloseAccrued");
                btnSearch.Visible = RoLo.isAllowedAccess("btnSearch");
                btnClear.Visible = RoLo.isAllowedAccess("btnClear");
                btnCloseAccrued.Visible = RoLo.isAllowedAccess("btnCloseAccrued");
                btnSimulate.Visible = RoLo.isAllowedAccess("btnSimulate");
                btnDownload.Visible = true;
            }
        }

        protected void LookupWbsNoOld_Load(object sender, EventArgs arg)
        {
            _IssuingDivision = "'" + IssuingDivisionDropDown.Text.Trim().Replace("; ", ";").Replace(";", "','") + "'";
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            List<WBSStructure> lstWbsNumber = logic.AccrBalance.getWbsNoOld(_IssuingDivision);
            lookup.DataSource = lstWbsNumber;
            lookup.DataBind();

        }

        protected void LookupWbsNoNew_Load(object sender, EventArgs arg)
        {
            _IssuingDivision = "'" + IssuingDivisionDropDown.Text.Trim().Replace("; ", ";").Replace(";", "','") + "'";
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            List<WBSStructure> lstWbsNumber = logic.AccrBalance.getWbsNoNew(_IssuingDivision);
            lookup.DataSource = lstWbsNumber;
            lookup.DataBind();

        }

        protected void LookupSusNoNew_Load(object sender, EventArgs arg)
        {
            _IssuingDivision = "'" + IssuingDivisionDropDown.Text.Trim().Replace("; ", ";").Replace(";", "','") + "'";
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            List<SuspenseComboStructure> suspense = logic.AccrBalance.getSuspenseNew(_IssuingDivision);
            lookup.DataSource = suspense;
            lookup.DataBind();

        }

        protected void LookupSusNoOld_Load(object sender, EventArgs arg)
        {
            _IssuingDivision = "'" + IssuingDivisionDropDown.Text.Trim().Replace("; ", ";").Replace(";", "','") + "'";
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            List<SuspenseComboStructure> suspense = logic.AccrBalance.getSuspenseOld(_IssuingDivision);
            lookup.DataSource = suspense;
            lookup.DataBind();

        }
        #endregion

        #region Grid

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = logic.AccrBalance.Search(SearchCriteria()).ToList();
            SetSearchLoad(!ListInquiry.Any());

            e.Result = ListInquiry;
        }

        protected void LinqDataSource2_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            _listWbsNoOld = logic.AccrBalance.getWbsNoOld(_IssuingDivision);
            SetSearchLoad(!_listWbsNoOld.Any());

            e.Result = _listWbsNoOld;
        }

        protected void LinqDataSource3_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            _listWbsNoNew = logic.AccrBalance.getWbsNoNew(_IssuingDivision);
            SetSearchLoad(!_listWbsNoNew.Any());

            e.Result = _listWbsNoNew;
        }

        protected void LinqDataSource4_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            _getSuspenseOld = logic.AccrBalance.getSuspenseOld(_IssuingDivision);
            SetSearchLoad(!_getSuspenseOld.Any());

            e.Result = _getSuspenseOld;
        }

        protected void LinqDataSource5_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            _getSuspenseNew = logic.AccrBalance.getSuspenseNew(_IssuingDivision);
            SetSearchLoad(!_getSuspenseNew.Any());

            e.Result = _getSuspenseNew;
        }
        //protected void SetDataGrid()
        //{
        //    ListInquiry = logic.AccrBalance.Search(SearchCriteria()).Distinct().ToList();
        //}

        protected AccrBalanceSearchCriteria SearchCriteria()
        {
            return new AccrBalanceSearchCriteria(
                    sel.SelectedValues(CommonFunction.CommaJoin(UserData.Divisions, ";"), (UserData != null) ? UserData.isFinanceDivision : false),
                    ddlOpenStatus.SelectedValue, (ddlWBSNoOld.Value != null) ? ddlWBSNoOld.Value.ToString() : "", txtBookingNo.Text, ddlExtend.SelectedValue, (ddlWBSNoNew.Value != null) ? ddlWBSNoNew.Value.ToString() : "",
                     ddlPvType.SelectedValue, ddlShifted.SelectedValue, (ddlSusNoOld.Value != null) ? ddlSusNoOld.Value.ToString() : "",
                     dtExpiryDateFrom.Text, dtExpiryDateTo.Text, (txtBDYear.Text != null) ? txtBDYear.Text.ToString() : "", (ddlSusNoNew.Value != null) ? ddlSusNoNew.Value.ToString() : ""
                );
            //return new AccrBalanceSearchCriteria();
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //logic.Say("btnSearch_Click", "Search {0} {1}", ddlSuspenseNo.Text, txtPVYear.Text);
            DoSearch();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (IssuingDivisionDropDown.Enabled)
                sel.Clear();
            //txtAccrNo.Text = null;
            dtExpiryDateFrom.Text = null;
            dtExpiryDateTo.Text = null;
            txtBookingNo.Text = null;
            txtBDYear.Text = null;

            ddlOpenStatus.SelectedValue = null;
            ddlExtend.SelectedValue = null;
            ddlPvType.SelectedValue = null;
            ddlShifted.SelectedValue = null;
           
            ddlSusNoOld.Value = null;
            ddlSusNoNew.Value = null;

            ddlWBSNoOld.Value = null;
            ddlWBSNoNew.Value = null;

            gridGeneralInfo.DataSource = null;
            gridGeneralInfo.DataSourceID = null;
        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            string bookingNo = gridGeneralInfo.GetRowValues(e.VisibleIndex, "BOOKING_NO").ToString();

            ViewActivity(bookingNo);
        }

        protected void ViewActivity(string bookingNo)
        {
            //logic.Say("btnActivity_Click", "Activity {0}", ScreenType);
            panelActivity.GroupingText = "Simulation";


            var param = new Dictionary<string, object>();

            param.Add("BOOKING_NO", bookingNo);

            List<AccruedListDetail> _list = logic.AccrList.GetActivityBooking(param);

            gridActivity.DataSource = _list;
            gridActivity.DataBind();

            lblBookingNo.Text = bookingNo;

            gridActivity.DetailRows.ExpandAllRows();
            (popupActivity as ModalPopupExtender).Show();
        }

        protected void btnCloseActivity_Click(object sender, EventArgs e)
        {
            lblBookingNo.Text = "";
            (popupActivity as ModalPopupExtender).Hide();
        }


        protected void btnCloseAccrued_Click(object sender, EventArgs e)
        {
            logic.Say("Close Accrued", "btnCloseAccrued_Click");
            if (gridGeneralInfo.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            if(dtPostingDt.Text.isEmpty())
            {
                Nag("MSTD00017WRN", "Closing Date");
                return;
            }

            try
            {
                string bookingNo = "";

                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {
                        bookingNo = bookingNo + gridGeneralInfo.GetRowValues(i, "BOOKING_NO").ToString() + ",";
                    }
                }
                bookingNo = bookingNo.Substring(0, bookingNo.Length - 1);

                if (logic.AccrBalance.isAnyClosed(bookingNo.Split(',').ToList()))
                {
                    logic.Say("Close Accrued", "Failed. Some booking no already closed");

                    Nag("MSTD00227WRN", "because some booking no already closed");
                    return;
                }

                // Post to SAP and close Booking no
                List<Common.Data.SAPData.AccrSAPError> sapErr = new List<Common.Data.SAPData.AccrSAPError>();
                if (logic.AccrForm.CloseBalanceToSAP(bookingNo, dtPostingDt.Date, UserData, ref sapErr))
                {
                    Nag("MSTD00060INF", "Closing");
                }
                else
                {
                    Nag("MSTD00002ERR", "There is at least 1 Booking No that failed to Closing");
                }
            }
            catch (Exception ex)
            {
                Nag("MSTD00062ERR", "Closing");
                Handle(ex);
            }

            DoSearch();
            ReloadOpener();
        }

        protected void btnSimulate_Click(object sender, EventArgs args)
        {
            logic.Say("btnSimulate_Click", "Accrued Closing Journal Simulation");
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                Boolean checkValid = true;
                if (checkValid)
                {
                    string bookingNo = "";
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            bookingNo = bookingNo + gridGeneralInfo.GetRowValues(i, "BOOKING_NO").ToString() + ",";
                        }
                    }
                    bookingNo = bookingNo.Substring(0, bookingNo.Length - 1);
                    ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "ELVIS_AccrBalanceSimul",
                    "openWin('AccrSimulBal.aspx?bookno=" + bookingNo + "','ELVIS_AccrSimulBal');",
                    true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(),
                    //"ELVIS_AccrBalanceSimul",
                    //"openWin('AccrBalanceSimul.aspx?bookingno=" + bookingNo + "','ELVIS_AccrBalanceSimul');",
                    //true);
                }
            }
            else
            {
                Nag("MSTD00009WRN");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logic.Say("btnDelete_Click", "Delete");
            if (gridGeneralInfo.Selection.Count == 1)
            {
                Boolean checkValid = true;

                if (checkValid)
                {
                    lblConfirmationDelete.Text = _m.Message("MSTD00008CON", "delete");
                    ConfirmationDeletePopUp.Show();
                }
            }
            else if (gridGeneralInfo.Selection.Count > 1)
            {
                #region Error

                Nag("MSTD00016WRN");
                #endregion
            }
            else
            {
                Nag("MSTD00009WRN");
            }

        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            string accruedNo = "";
            if (gridGeneralInfo.Selection.Count != 1) return;

            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                if (!gridGeneralInfo.Selection.IsRowSelected(i)) continue;

                accruedNo = gridGeneralInfo.GetRowValues(i, "ACCRUED_NO").ToString();
                break;
            }

            logic.Say("btnOkConfirmationDelete_Click", String.Format("Confirm Delete Accrued List no : {0}", accruedNo));

            if (!String.IsNullOrEmpty(accruedNo) && logic.AccrList.isDraft(accruedNo))
            {
                result = logic.AccrList.Delete(accruedNo, UserName);
                if (!result)
                {
                    Nag("MSTD00014ERR");
                    gridGeneralInfo.Selection.UnselectAll();
                }
                else
                {
                    Nag("MSTD00015INF");
                    gridGeneralInfo.Selection.UnselectAll();
                    DoSearch();
                }
            }
            else
            {
                Nag("MSTD00216ERR", "Draft List Accrued");
            }
        }

        protected void BtnCancelConfirmationDelete_Click(object sender, EventArgs e)
        {
            ConfirmationDeletePopUp.Hide();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            var crit = new AccruedSearchCriteria();
            crit.issuingDivision = UserData.DIV_CD.ToString();
            crit.budgetYear = DateTime.Now.Year;
            crit.ignoreIssuingDivision = false;

            var qdata = logic.AccrList.Search(crit);
            if (qdata.Any())
            {
                var accrData = qdata.FirstOrDefault();
                if (logic.AccrList.isDraft(accrData.ACCRUED_NO))
                {
                    Nag("MSTD00056ERR", "Accrued", "Draft");
                    return;
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ELVIS_AccrForm",
                "openWin('AccrForm.aspx','ELVIS_AccrForm');",
                true);

        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count == 1)
            {
                Nag("MSTD00001INF", "COMING SOON!");
            }
            else if (gridGeneralInfo.Selection.Count > 1)
            {
                #region Error

                Nag("MSTD00016WRN");
                #endregion
            }
            else
            {
                Nag("MSTD00009WRN");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        /*
         * Created By : fid.Taufik
         */
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            UserData userdata = (UserData)Session["UserData"];
            try
            {
                //modified by FID.Arri on 8 May 2018 for AccrBalanceReport
                //string path = Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR"));
                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "AccruedBalanceReportTemplate.xls");
                string excelName = "Accrued_Balance_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                string pdfName = "Accrued_Balance_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                string fullPath = logic.AccrBalance.GenerateListAccrBalanceResult(
                            UserName,
                            templatePath,
                            excelName,
                            SearchCriteria(),
                            userdata,
                            pdfName
                        );
                Xmit.Transmit(HttpContext.Current, fullPath);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    string xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                    Nag("MSTD00042ERR", ex.Message);
                }
            }
        }

        #endregion

        #region Function

        private bool ValidateInputSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Handle(ex);
                throw;
            }
            return true;
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.PVList.GetUserDivisions(UserData);
        }


        protected string Status_Color(Object statusValue)
        {
            string color = "";
            short statusInt = 0;
            if (statusValue != null && Int16.TryParse(statusValue.ToString(), out statusInt))
            {
                if (statusInt == 1)
                    color = red;
                else if (statusInt == 2)
                    color = yellow;
                else if (statusInt > 2)
                    color = green;
            }
            return color;
        }

        protected string WriteSelectedIndexInfo(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected void grid_CustomCallback(object sender,
            DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            int newSize;
            if (!int.TryParse(e.Parameters.ToString(), out newSize)) return;

            if (newSize < 0)
                newSize = 0;
            grid.SettingsPager.PageSize = newSize;
            PageSize = newSize;
            // Session[GridCustomPageSizeName] = newSize;
            if (grid != null && e != null && !String.IsNullOrEmpty(e.Parameters) && grid.VisibleRowCount > 0)
            {
                if (ValidateInputSearch())
                {
                    grid.DataBind();

                }
            }
        }
        #endregion
    }
}