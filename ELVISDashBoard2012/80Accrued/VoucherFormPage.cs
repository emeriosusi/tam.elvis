﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Diagnostics;
using AjaxControlToolkit;
using ELVISDashboard.MasterPage;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Control;
using Common.Data;
using Common.Data._20MasterData;
using Common.Data._30PVFormList;
using Common.Data.SAPData;
using Common.Function;
using Common.Messaging;
using DataLayer;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._80Accrued
{
    public abstract class VoucherFormPage : BaseCodeBehind
    {
        public const string SCREEN_TYPE_PV = "PV";
        public const string SCREEN_TYPE_RV = "RV";
        public const int MAX_PAGE_ROW = 5;

        public const int VIEW_MODE = 0;
        public const int ADD_MODE = 1;
        public const int EDIT_MODE = 2;
        public const string FINANCE_AUTHORIZED_EDITOR_ROLE = "ELVIS_FD_STAFF";
        public const string FINANCE_APPROVER_ROLE = "ELVIS_FD_APPROVER";
        public const string USER_ADMIN_ROLE = "ELVIS_USER_ADMIN";

        //fid.Hadid 20180406
        private int ACCRUED_TRANS;
        //end fid.Hadid 20180406

        private readonly List<string> RoleAllowedEditHoldData;
        private readonly List<string> FinanceTabRoles;

        protected string _ScreenID;
        protected readonly FormPersistence formPersistence;

        protected readonly VoucherListLogic lilo;
        protected int Mode;

        protected int[] EntertainmentTransactionTypes;
        protected int[] NonCostCenterGlAccounts = new int[] { 0 };
        protected int[] WorkflowNonOp = new int[] { 22, 27, 30, 60, 63, 99 };
        protected StringMap HoldReasons;

        public VoucherFormPage(string screenType)
        {
            ACCRUED_TRANS = logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219);

            RoleAllowedEditHoldData = logic.Sys.GetArray("ROLES", "EDIT_HOLD").ToList();
            FinanceTabRoles = logic.Sys.GetArray("ROLES", "FINANCE").ToList();
            Mode = VIEW_MODE;
            ScreenType = screenType;
            switch (screenType)
            {
                case SCREEN_TYPE_PV:
                    formPersistence = logic.PVForm;
                    lilo = logic.PVList;
                    break;
                case SCREEN_TYPE_RV:
                    formPersistence = logic.RVForm;
                    lilo = logic.RVList;
                    break;
                default:
                    formPersistence = logic.PVForm;
                    lilo = logic.PVList;
                    break;
            }
            HoldReasons = logic.Sys.GetDict("HOLD_REASON");
            List<int?> et = formPersistence.getEntertainmentTransactionCodes();
            var q = et.Where(c => c != null).Distinct();
            if (q != null)
            {
                q = q.OrderBy(a => a ?? 0);
                EntertainmentTransactionTypes = q.Select(b => b ?? 0).ToArray();
            }
            else
            {
                EntertainmentTransactionTypes = new int[0];
            }
            string[] ngl = logic.Sys.GetArray("GL_ACCOUNT_NO_COST_CENTER", "1");
            if (ngl.Length > 0)
            {
                NonCostCenterGlAccounts = new int[ngl.Length];
                for (int i = 0; i < ngl.Length; i++)
                {
                    NonCostCenterGlAccounts[i] = ngl[i].Int();
                }
            }
        }

        public struct AttachID
        {
            public string No;
            public string Year;
            public string ReffNo
            {
                get
                {
                    return No + Year;
                }
            }
            public int Seq;
        }

        protected string ScreenType { set; get; }

        public GeneralMaster VoucherMasterPage { set; get; }

        #region Components
        protected ASPxGridView DetailGrid { set; get; }
        protected ASPxGridView AttachmentGrid { set; get; }
        protected ASPxListBox BudgetNumberListBox { set; get; }
        protected Literal TotalAmountLiteral { set; get; }
        protected Literal MessageControlLiteral { set; get; }
        protected Literal PaymentMethodLiteral { set; get; }

        protected TextBox NoteTextBox { set; get; }
        protected ASPxLabel DocumentDateLabel { set; get; }
        protected ASPxLabel DocumentNumberLabel { set; get; }
        protected ASPxLabel IssuingDivisionLabel { set; get; }
        protected ASPxLabel StatusLabel { set; get; }
        protected ASPxLabel VendorCodeLabel { set; get; }
        protected ASPxLabel BookingWbs { set; get; }
        protected ASPxLabel VendorDescriptionLabel { set; get; }
        protected ASPxLabel VendorGroupLabel { set; get; }

        private ASPxLabel _postingDateLabel = null;

        protected ASPxGridView SimulateGrid = null;
        protected Panel SimulatePanel = null;
        protected Control SimulatePopUp = null;
        protected ASPxGridView SAPGrid = null;
        protected ModalPopupExtender SAPpop = null;

        protected ASPxGridView BankGrid = null;
        protected ModalPopupExtender BankPop = null;


        protected ASPxLabel PostingDateLabel
        {
            set
            {
                _postingDateLabel = value;
            }
            get
            {
                if (_postingDateLabel == null)
                {
                    _postingDateLabel = new ASPxLabel();
                    _postingDateLabel.Text = "";
                }
                return _postingDateLabel;
            }
        }
        private ASPxLabel _planningPaymentDateLabel = null;
        protected ASPxLabel PlanningPaymentDateLabel
        {
            set
            {
                _planningPaymentDateLabel = value;
            }
            get
            {
                if (_planningPaymentDateLabel == null)
                {
                    _planningPaymentDateLabel = new ASPxLabel();
                    _planningPaymentDateLabel.Text = "";
                }
                return _planningPaymentDateLabel;
            }
        }
        private ASPxLabel _bankTypeLabel = null;
        protected ASPxLabel BankTypeLabel
        {
            set
            {
                _bankTypeLabel = value;
            }
            get
            {
                if (_bankTypeLabel == null)
                {
                    _bankTypeLabel = new ASPxLabel();
                    _bankTypeLabel.Text = "";
                }
                return _bankTypeLabel;
            }
        }
        protected ASPxLabel DocumentTypeLabel { set; get; }
        protected ASPxLabel TransactionTypeLabel { set; get; }
        protected ASPxLabel ActivityDateStartLabel { set; get; }
        protected ASPxLabel ActivityDateEndLabel { set; get; }
        protected ASPxLabel BudgetNumberLabel { set; get; }
        private ASPxLabel activityDateSeparator = null;
        protected ASPxLabel ActivitDateToSeparatorLabel
        {
            set
            {
                activityDateSeparator = value;
            }
            get
            {
                if (activityDateSeparator == null)
                {
                    activityDateSeparator = new ASPxLabel();
                    activityDateSeparator.Value = "";
                }
                return activityDateSeparator;
            }
        }
        protected ASPxComboBox DocumentTypeDropDown { set; get; }
        protected ASPxComboBox PaymentMethodDropDown { set; get; }
        protected ASPxComboBox NoteRecipientDropDown { set; get; }
        protected ASPxComboBox AttachmentCategoryDropDown { set; get; }
        protected ASPxCheckBox CalculateTaxCheckBox { set; get; }
        protected ASPxDateEdit ActivityDateStart { set; get; }
        protected ASPxDateEdit ActivityDateEnd { set; get; }
        protected ASPxDateEdit PostingDate { set; get; }
        private ASPxEditBase _planningPaymentDate = null;
        protected ASPxEditBase PlanningPaymentDate
        {
            set
            {
                _planningPaymentDate = value;
            }
            get
            {
                if (_planningPaymentDate == null)
                {
                    _planningPaymentDate = new ASPxDateEdit();
                    _planningPaymentDate.Value = "";
                }
                return _planningPaymentDate;
            }
        }
        protected ASPxGridLookup VendorCodeLookup { set; get; }
        protected ASPxGridLookup BookingNoLookup { set; get; }
        private ASPxGridLookup _bankTypeLookup = null;
        protected ASPxGridLookup BankTypeLookup
        {
            set
            {
                _bankTypeLookup = value;
            }
            get
            {
                if (_bankTypeLookup == null)
                {
                    _bankTypeLookup = new ASPxGridLookup();
                }
                return _bankTypeLookup;
            }
        }
        private Literal _bankTypeLiteral = null;
        protected Literal BankTypeLiteral
        {
            set
            {
                _bankTypeLiteral = value;
            }
            get
            {
                if (_bankTypeLiteral == null)
                {
                    _bankTypeLiteral = new Literal();
                }
                return _bankTypeLiteral;
            }
        }

        protected ASPxGridLookup TransactionTypeLookup { set; get; }
        protected Button EditButton { set; get; }
        protected Button SaveButton { set; get; }
        protected Button CancelButton { set; get; }
        protected Button CloseButton { set; get; }
        protected Button SubmitButton { set; get; }
        protected Button RejectButton { set; get; }
        protected Button HoldButton { set; get; }
        protected Button UnHoldButton { set; get; }
        protected Button PostSAPButton { set; get; }
        protected Button ReverseButton { set; get; }
        protected Button SummaryButton { set; get; }
        protected Button SimulateUserButton { set; get; } // Rinda Rahayu 20160425
        protected LinkButton RemainingBudgetLinkButton { set; get; } // Rinda Rahayu 20160425
        protected HtmlGenericControl DivShowRemainingBudget { set; get; } // Rinda Rahayu 20160425
        protected HtmlGenericControl DivRemainingBudget { set; get; } // Rinda Rahayu 20160425
        protected Button DataUploadButton { set; get; }
        protected HtmlControl UploadDiv { set; get; }
        protected ASPxHyperLink DownloadUploadTemplateLink { set; get; }
        protected HtmlGenericControl DownloadEntertainmentTemplateLink { set; get; }
        protected ASPxHyperLink BudgetNumberLink { set; get; }
        protected FileUpload DataUpload { set; get; }
        protected FileUpload AttachmentUpload { set; get; }
        protected ASPxPageControl HeaderTab { set; get; }
        protected HiddenField HiddenScreenID { set; get; }
        protected HiddenField HiddenMessageExpandFlag { set; get; }
        protected HiddenField HiddenReloadOpener { set; get; }
        protected HiddenField HiddenPaymentMethod { set; get; }
        protected HiddenField HiddenOneTimeVendorValid { set; get; }

        private HiddenField hSelectedBankType = null;
        protected HiddenField HiddenSelectedBankType
        {
            set
            {
                hSelectedBankType = value;
            }

            get
            {
                if (hSelectedBankType == null)
                {
                    hSelectedBankType = new HiddenField();
                    hSelectedBankType.Value = "";
                }
                return hSelectedBankType;
            }
        }
        protected HiddenField HiddenGridFocusedColumn { set; get; }
        protected HiddenField HiddenFinanceAccessFlag { set; get; }
        protected Repeater NoteRepeater { set; get; }
        protected ModalPopupExtender AttachmentPopup { set; get; }
        protected HtmlGenericControl NoteCommentContainer { set; get; }
        protected ASPxPopupControl VendorAdditionPopup { set; get; }
        protected ASPxButton VendorAdditionButton { set; get; }
        protected ASPxTextBox NewVendorName { set; get; }
        protected ASPxTextBox NewVendorSearchTerm { set; get; }
        protected ASPxComboBox NewVendorDivision { set; get; }
        #endregion Components

        private RoleLogic roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (roleLogic == null)
                {
                    roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return roleLogic;
            }
        }

        private bool isCancel
        {
            get
            {
                DropDownList d = VoucherMasterPage.FindControl("ddlCategory") as DropDownList;
                return (d != null && d.SelectedValue == "Cancel");
            }
        }

        #region Page Loading
        public virtual void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            formPersistence.UserName = UserName;

            Mode = getMode();

            postbackInit();
            ShowLoad();
            initFormTable();

            VoucherMasterPage.ProceedButtonClicked += new EventHandler(btnApprovalProceed_Click);

            if (!IsPostBack)
            {
                if (UserData == null)
                {

                    Response.Write("<script language='javascript'> { window.close(); }</script>");
                    formPersistence.UserName = UserName;
                    return;
                }
                PrepareLogout();
                DropDownList d = Master.FindControl("ddlHoldReason") as DropDownList;
                if (d != null)
                {
                    GenerateComboData(d, "HOLD_REASON", false);
                }
                HiddenScreenID.Value = _ScreenID;
                init();
                SetAccrued();
            }

            HiddenFinanceAccessFlag.Value = Convert.ToString(FormData.FinanceAccessEnabled);
        }
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
        }
        protected override void clearSession()
        {
            base.clearSession();
            Session[ScreenType + SID_DATA] = null;
            Session[SID_WORKLIST] = null;
            Session[ScreenType + "MsgList"] = null;
        }

        protected abstract void postbackInit();
        protected void init()
        {
            clearSession();
            initComponents();
            evaluateExternalParameter();
            evaluatePageComponentAvailibility();
            loadBankTypeDesc();
        }
        private void SetAccrued()
        {
            FormData.IsAccrued = true;
        }
        protected void refreshLabels()
        {
            int? statusCode = FormData.StatusCode;
            if (statusCode != null)
            {
                StatusLabel.Text = formPersistence.getDocumentStatus(statusCode.Value);
            }
            else
            {
                StatusLabel.Text = "";
            }
        }

        protected int getMode()
        {
            string mode = Request["mode"];
            int m = VIEW_MODE;

            if (mode.isEmpty())
            {
                m = ADD_MODE;
            }
            else if (mode.ToLower().Equals("view"))
            {
                m = VIEW_MODE;
            }
            else if (mode.ToLower().Equals("edit"))
            {
                m = EDIT_MODE;
            }
            return m;

        }

        protected void initComponents()
        {
            refreshLabels();
            IssuingDivisionLabel.Text = normalizeDivisionName(UserData.DIV_NAME);

            if (Mode == VIEW_MODE)
            {
                ddlPaymentMethod_LoadRvView(PaymentMethodDropDown, EventArgs.Empty);
                return;// ignore loading on view            
            }

            ddlPaymentMethod_Load(PaymentMethodDropDown, EventArgs.Empty);
            ddlDocType_Load(DocumentTypeDropDown, EventArgs.Empty);
            ddlTransactionType_Load(TransactionTypeLookup, EventArgs.Empty);
            lkpgridVendorCode_Load(VendorCodeLookup, EventArgs.Empty);
            evt_ddlSendTo_onLoad(NoteRecipientDropDown, EventArgs.Empty);

            ////set only Accrued transaction type
            FormData.TransactionCode = ACCRUED_TRANS;
            TransactionTypeLookup.Value = ACCRUED_TRANS;
            var _transactionType = formPersistence.GetTransactionType(ACCRUED_TRANS);
            string transactionName = "";
            if (_transactionType != null)
                transactionName = _transactionType.Name;
            TransactionTypeLookup.Text = transactionName;
            TransactionTypeLabel.Text = transactionName;

            //fid.Hadid
            LookupBookingNo_Load(BookingNoLookup, EventArgs.Empty);

            //set only Direct type
            DocumentTypeDropDown.SelectedItem = DocumentTypeDropDown.Items.FindByValue("1");
            DocumentTypeLabel.Text = formPersistence.getDocType(1);
            //end fid.Hadid
        }

        protected void loadBankTypeDesc()
        {
            if (FormData.PaymentMethodCode == "T")
            {
                // var q = formPersistence.GetVendorBank(FormData.VendorCode);
                // int c = q.Count();
                int c = formPersistence.Qx<int>("GetVendorBanksCountByVendor", new { @vendorCd = FormData.VendorCode }).First();
                if (c == 1)
                {
                    // var x = q.FirstOrDefault();
                    VendorBank x = formPersistence.GetVendorBank(FormData.VendorCode, "");
                    BankTypeLiteral.Text = (x.Name ?? "").Trim() + " - " + (x.Account ?? "");
                }
                else if (c > 1)
                {
                    VendorBank v = formPersistence.GetVendorBank(FormData.VendorCode, FormData.BankType.str());
                    if (v != null)
                        BankTypeLiteral.Text = (v.Name ?? "").Trim() + " - " + (v.Account ?? "");
                    else
                        BankTypeLiteral.Text = "Please choose Vendor Bank";
                }
                else
                {
                    if (FormData.VendorGroupCode == 4)
                    {
                        BankTypeLiteral.Text = "Please Fill Vendor Bank Detail when Posting";
                    }
                    else
                    {
                        BankTypeLiteral.Text = "Vendor doesn't have bank";
                    }
                }
            }
            else
            {
                BankTypeLiteral.Text = "";
            }
        }
        protected void evaluateExternalParameter()
        {
            string lowerScreenType = ScreenType.ToLower();
            string strPVNumber = Request[lowerScreenType + "_no"];
            string strPVYear = Request[lowerScreenType + "_year"];
            string mode = Request["mode"];

            bool dataPrefetchRequested = (strPVYear != null) && (strPVNumber != null);

            if (dataPrefetchRequested)
            {
                int pvYear = int.Parse(strPVYear);
                int pvNumber = int.Parse(strPVNumber);

                PVFormData f = formPersistence.search(pvNumber, pvYear);
                if (f != null)
                {
                    f.copyStatus(FormData);
                    if (isSettlement(f.PVTypeCode ?? 0) & UserData.Roles.Contains(USER_ADMIN_ROLE))
                        mode = "view";

                    if (((f.StatusCode >= 30 && f.StatusCode <= 46 && ScreenType == SCREEN_TYPE_PV)
                        || ((f.StatusCode >= 71) && (f.StatusCode <= 85) && ScreenType == SCREEN_TYPE_RV))
                        && UserData.isAuthorizedCreator)
                        formPersistence.LoadOri(f);

                    FormData = f;
                    FormData.InWorklist = hasPVAsWorklist();
                    ReviewTxCode();

                    f.OpenExisting = true;
                    f.OpenEmpty = false;
                    int? status = f.StatusCode;
                    f.Editable = ((status >= 0) && (status <= 16)) ||
                                 ((status >= 20) && (status <= 26)) ||
                                 ((status >= 30) && (status <= 46)) ||
                                 ((status >= 50) && (status <= 56)) ||
                                 ((status >= 62) && (status <= 67)) ||
                                 ((status >= 70) && (status <= 87));

                    int? pvType = f.PVTypeCode;
                    if (pvType.HasValue && (pvType.Value == 3))
                    {
                        f.OpenEmpty = false;
                        f.Editable = false;
                    }

                    initFormTable();
                    loadForm();

                    if (f.Editable && !mode.isEmpty() && mode.ToLower().Equals("edit"))
                    {
                        StartEditing();
                    }

                }
                else
                {
                    // not found script

                }
            }
        }
        public void deleteAllRow()
        {
            Tabel.DataList.Clear();
            initFirstRow();
        }
        protected void evaluatePageComponentAvailibility()
        {
            PVFormData f = FormData;
            AttachmentTable attachmentTable = f.AttachmentTable;
            if (f.OpenEmpty)
            {
                f.Editing = true;
                initFirstRow();
            }
            string loggedUserRole = UserData.Roles.FirstOrDefault();

            if (FINANCE_AUTHORIZED_EDITOR_ROLE.Equals(loggedUserRole)
                || FINANCE_APPROVER_ROLE.Equals(loggedUserRole)
                || FinanceTabRoles.Contains(loggedUserRole))
            {
                f.Editable = true;
                f.FinanceAccessEnabled = true;
            }

            f.CanUploadDetail = UserCanUploadDetail;

            evaluateHeaderVisibility();
            evaluateEditingControlVisibility();
        }

        protected virtual bool isSettlement(int Code)
        {
            return false;
        }

        protected virtual bool HoldVisibleStatus(int? statusCD)
        {
            return true;
        }

        protected void ShowUpload(bool b = true)
        {
            if (UploadDiv == null) return; //fid.Hadid
            if (b)
            {
                UploadDiv.Style.Clear();
            }
            else
            {
                UploadDiv.Style.Add("display", "none");
            }
        }

        protected void evaluateEditingControlVisibility()
        {
            PVFormData f = FormData;
            bool hasWorklist = hasPVAsWorklist();

            bool hasEditingAccess = RoLo.isAllowedAccess("btEdit");
            bool editButtonVisible;

            bool isFinanceApprover = UserData.Roles.Contains(FINANCE_APPROVER_ROLE);
            editButtonVisible = PVFormLogic.CanEdit(
                    hasEditingAccess,
                    (hasWorklist || isFinanceApprover) && f.Editable,
                    f.StatusCode ?? -1,
                    UserData.Roles.FirstOrDefault(),
                    f.OnHold,
                    f.VendorGroupCode,
                    f.PVTypeCode ?? 0,
                    UserData.isAuthorizedCreator
                    );

            EditButton.Visible = editButtonVisible
                            && (!f.Submitted)
                            && !f.Editing;
            if (isSettlement(f.PVTypeCode ?? 0) && UserData.Roles.Contains(USER_ADMIN_ROLE))
            {
                f.OpenEmpty = false;
                f.Editing = false;
            }

            if (f.OpenEmpty)
            {
                CloseButton.Enabled = true;
                CancelButton.Visible = false;
            }
            else
            {
                CloseButton.Visible = !f.Editing;
                CancelButton.Visible = f.Editing;
            }

            SaveButton.Visible = f.Editing;
            if (isSettlement(f.PVTypeCode ?? 0) && UserData.Roles.Contains(USER_ADMIN_ROLE))
            {
                SaveButton.Visible = false;
                f.OpenEmpty = false;
            }

            DataUpload.Visible = f.Editing && !f.FinanceAccessEnabled;

            DataUploadButton.Visible = f.Editing && !f.FinanceAccessEnabled && f.CanUploadDetail;
            DownloadUploadTemplateLink.Visible = f.Editing && !f.FinanceAccessEnabled && f.CanUploadDetail;
            ShowUpload(f.CanUploadDetail);

            if (f.Editing)
            {
                TransactionCodeRefresh();
            }

            bool submitButtonVisible = RoLo.isAllowedAccess("btSubmit") && hasWorklist;
            bool verifyRV = ((f.StatusCode == 60 && f.PVTypeCode == 1))
                         || ((f.StatusCode == 61 && f.PVTypeCode == 2));

            if (f.OpenEmpty)

            {
                submitButtonVisible = true;
            }
            else if (f.FinanceAccessEnabled)
            {
                submitButtonVisible = submitButtonVisible
                     && (!WorkflowNonOp.Contains(f.StatusCode ?? 0));



            }
            else
            {
                submitButtonVisible = submitButtonVisible || (f.StatusCode == 0 && RoLo.isAllowedAccess("btSubmit"));
            }


            SubmitButton.Visible = submitButtonVisible;

            //Start Rinda Rahayu 20160516
            /* if (FormData != null && FormData.ScreenType.Equals(SCREEN_TYPE_RV) && SubmitButton.Visible)
             {
                 PaymentMethodDropDown.Visible = true;
                 PaymentMethodLiteral.Visible = false;
             } */
            //End Rinda Rahayu 20160516

            bool canReject = RoLo.isAllowedAccess("btReject");
            bool canHold = RoLo.isAllowedAccess("btHold");
            bool canUnHold = RoLo.isAllowedAccess("btUnHold");
            bool canPost = RoLo.isAllowedAccess("btPostToSAP");
            bool canReverse = RoLo.isAllowedAccess("btReverse");
            bool canGenerateSummary = RoLo.isAllowedAccess("btSummary")
                && f.VendorGroupCode == PVFormData.EXTERNAL_VENDOR_GROUP_CODE
                && (Tabel.InvoiceCount() > 0);

            RejectButton.Visible = canReject && hasWorklist
                && (!WorkflowNonOp.Contains(f.StatusCode ?? 0) || f.StatusCode == 30) // ready to post can reject too
                && f.FinanceAccessEnabled && !f.Editing;

            HoldButton.Visible = canHold && (hasWorklist || UserData.Roles.Contains("ELVIS_COUNTER"))
                && !f.OnHold && f.FinanceAccessEnabled
                && HoldVisibleStatus(f.StatusCode)
                && submitButtonVisible;
            UnHoldButton.Visible = canUnHold && f.OnHold;
            PostSAPButton.Visible = canPost
                                    && (f.StatusCode == 30);
            ReverseButton.Visible = canReverse && (f.StatusCode == 27);
            SummaryButton.Visible = canGenerateSummary;
            // Rinda Rahayu 20160525
            if (f.StatusCode != null && !f.FinanceAccessEnabled)
            {
                SimulateUserButton.Visible = true;
            }
            else
            {
                SimulateUserButton.Visible = false;
            }

            // Rinda Rahayu 20160425
            if (BudgetNumberLabel.Text != null && !BudgetNumberLabel.Text.Equals(""))
            {
                RemainingBudgetLinkButton.Visible = true;
            }
            else
            {
                RemainingBudgetLinkButton.Visible = false;
            }
            initFormTable();
            if (f.Editing)
            {
                DetailGrid.CancelEdit();
                if (!f.FinanceAccessEnabled && !isSettlement(f.PVTypeCode ?? 0))
                {
                    StartEditingTabel();
                    f.AttachmentTable.openEditing();
                }
            }
            else
            {
                StartBrowsingTabel();
                f.AttachmentTable.closeEditing();
            }

            if (isSettlement(f.PVTypeCode ?? 0))
            {
                if ((f.StatusCode == 0) || (f.StatusCode == 31) || (f.StatusCode == 71))
                {
                    f.AttachmentTable.openEditing();
                    EditButton.Visible = false;
                }
            }
            else
            {
                if (f.StatusCode < 20 && f.StatusCode != 17)
                {
                    f.AttachmentTable.openEditing();
                }
            }

            if (f.HOLD_FLAG == 1)
            {
                if (UserData.Roles.Intersect(RoleAllowedEditHoldData).Count() > 0)
                    f.AttachmentTable.openEditing();
            }

            updateAttachmentList();
        }
        protected void evaluateHeaderVisibility()
        {
            PVFormData f = FormData;
            bool editing = f.Editing && !(isSettlement(f.PVTypeCode ?? 0) && UserData.Roles.Contains(USER_ADMIN_ROLE));
            bool financeEnabled = f.FinanceAccessEnabled;
            bool editingByFinance = financeEnabled && f.Editing;

            bool isExternalVendorGroup = (f.VendorGroupCode == PVFormData.EXTERNAL_VENDOR_GROUP_CODE);
            /* Finance Header */
            PostingDate.Visible = editingByFinance;
            PostingDateLabel.Visible = !editingByFinance;
            PlanningPaymentDate.Visible = editingByFinance;
            PlanningPaymentDateLabel.Visible = !editingByFinance;

            BankTypeLookup.Visible = editingByFinance;
            if (BankTypeLookup.Visible)
            {
                BankTypeLookup.Value = f.BankType;
            }
            BankTypeLabel.Visible = !editingByFinance;
            HeaderTab.TabPages[1].Visible = financeEnabled || FinanceTabRoles.Contains(UserData.Roles.FirstOrDefault());

            if (financeEnabled)
            {
                HeaderTab.ActiveTabIndex = 1;
            }

            /* General Header */
            bool DocTypeVisible = (editing /*|| openHeaderAnyway*/) && !editingByFinance;

            if (DocTypeVisible && FormData != null)
            {
                /// settlement: transaction type read only                
                if (isSettlement(FormData.PVTypeCode ?? 0))
                    DocTypeVisible = false;
            }
            DocumentTypeDropDown.Visible = DocTypeVisible;
            DocumentTypeLabel.Visible = !DocTypeVisible;
            PaymentMethodDropDown.Visible = editing;
            PaymentMethodLiteral.Visible = !PaymentMethodDropDown.Visible;
            bool ttVisible = (editing /*|| openHeaderAnyway*/) && !editingByFinance;

            BookingNoLookup.Visible = editing;
            BookingWbs.Visible = !BookingNoLookup.Visible;
            BudgetNumberLabel.Visible = false;

            //TransactionTypeLookup.Visible = ttVisible; 
            //  TransactionTypeLabel.Visible = !ttVisible;
            if (f.StatusCode != null)
            {
                if (isExternalVendorGroup && Tabel.InvoiceCount() > 0)
                {
                    VendorCodeLookup.Visible = false;
                    VendorCodeLabel.Visible = true;
                    VendorAdditionButton.Visible = false;
                    TransactionTypeLookup.Visible = false; // Rinda Rahayu 20160412
                    TransactionTypeLabel.Visible = true; // Rinda Rahayu 20160412
                }
                else
                {
                    //VendorCodeLookup.Visible = (editing /*|| openHeaderAnyway*/) && !editingByFinance;                
                    //VendorAdditionButton.Visible = (editing /*|| openHeaderAnyway*/)
                    //        && !editingByFinance
                    //        && RoLo.isAllowedAccess("btAddNewVendor"); ;
                    //VendorCodeLabel.Visible = !editing || editingByFinance;

                    // Rinda Rahayu 20160412
                    VendorCodeLookup.Visible = false;
                    VendorAdditionButton.Visible = false;
                    VendorCodeLabel.Visible = true;
                    TransactionTypeLookup.Visible = false;
                    TransactionTypeLabel.Visible = true;
                    // End Rinda Rahayu 20160412
                }
            }
            else
            {
                VendorCodeLookup.Visible = (editing /*|| openHeaderAnyway*/) && !editingByFinance;
                VendorAdditionButton.Visible = (editing /*|| openHeaderAnyway*/)
                          && !editingByFinance
                          && RoLo.isAllowedAccess("btAddNewVendor"); ;
                VendorCodeLabel.Visible = !editing || editingByFinance;
            }

            ActivityDateStart.Visible = (editing /*|| openHeaderAnyway*/) && !editingByFinance;
            ActivityDateStartLabel.Visible = !editing || editingByFinance;

            ActivityDateEnd.Visible = (editing /* || openHeaderAnyway*/) && !editingByFinance;
            ActivityDateEndLabel.Visible = !editing || editingByFinance;

            if (f.OpenExisting && (f.ActivityDateTo == null))
            {
                ActivitDateToSeparatorLabel.Visible = false;
            }

            BudgetNumberLink.Visible = false;

            CalculateTaxCheckBox.Visible = false;

            if (isExternalVendorGroup && Tabel.InvoiceCount() > 0)
            {
                // VendorCodeLookup.Visible = false; Rinda Rahayu
            }
        }
        #endregion Page Loading

        #region Form
        protected void loadForm(PVFormData f = null)
        {
            if (f == null && FormData == null) return;
            if (f == null) f = FormData;

            DateTime? pvDate = f.PVDate;
            if (pvDate != null)
            {
                DocumentDateLabel.Text = string.Format("{0:dd MMM yyyy}", pvDate.Value); ;
            }

            int? pvNumber = f.PVNumber;
            if (pvNumber.HasValue)
            {
                DocumentNumberLabel.Text = pvNumber.Value.ToString();
            }

            IssuingDivisionLabel.Text = normalizeDivisionName(formPersistence.getDivisionName(f.DivisionID));

            int? docType = f.PVTypeCode;
            if (docType.HasValue)
            {
                DocumentTypeDropDown.SelectedItem = DocumentTypeDropDown.Items.FindByValue(docType.Value.ToString());
                DocumentTypeLabel.Text = formPersistence.getDocType(docType);
            }

            string paymentMethodCode = f.PaymentMethodCode;
            if (paymentMethodCode != null)
            {
                PaymentMethodDropDown.SelectedItem = PaymentMethodDropDown.Items.FindByValue(paymentMethodCode);
                PaymentMethodLiteral.Text = formPersistence.getPaymentMethodName(paymentMethodCode);
                HiddenPaymentMethod.Value = paymentMethodCode;
            }

            int? statusCode = f.StatusCode;
            if (statusCode.HasValue)
            {
                StatusLabel.Text = formPersistence.getDocumentStatus(statusCode.Value);
            }

            string vendorCode = f.VendorCode;
            if (vendorCode != null)
            {
                vendorCode = vendorCode.Trim();
                VendorCodeLabel.Text = vendorCode;
                VendorDescriptionLabel.Text = formPersistence.getVendorCodeName(vendorCode);
                CodeConstant group = formPersistence.getVendorGroupNameByVendor(vendorCode);
                f.VendorGroupCode = null;
                if (group != null)
                {
                    f.VendorGroupCode = Convert.ToInt32(group.Code);
                }

                VendorGroupLabel.Text = group != null ? group.Description : "";
                VendorCodeLookup.GridView.Selection.SelectRowByKey(vendorCode);
            }

            int? transactionCode = f.TransactionCode;

            if (transactionCode.HasValue)
            {
                TransactionTypeLookup.Value = transactionCode.Value.ToString();
                f._TransactionType = formPersistence.GetTransactionType(transactionCode ?? 0);
                string transactionName = "";
                if (f._TransactionType != null)
                    transactionName = f._TransactionType.Name;
                TransactionTypeLookup.Text = transactionName;
                TransactionTypeLabel.Text = transactionName;
            }
            ActivityDateStart.Value = f.ActivityDate;
            ActivityDateStartLabel.Text = string.Format("{0:dd MMM yyyy}", f.ActivityDate);
            ActivityDateEnd.Value = f.ActivityDateTo;
            ActivityDateEndLabel.Text = string.Format("{0:dd MMM yyyy}", f.ActivityDateTo);

            DateTime? postingDate = null;
            if (f.StatusCode == 30)
            {
                postingDate = DateTime.Now;
            }
            if (f.PostingDate != null)
            {
                postingDate = (DateTime)f.PostingDate;
            }
            PostingDate.Value = postingDate;
            if (postingDate != null)
            {
                PostingDateLabel.Text = string.Format("{0:dd MMM yyyy}", postingDate.Value);
            }
            else
            {
                PostingDateLabel.Text = String.Empty;
            }
            PlanningPaymentDate.Value = f.PlanningPaymentDate;
            PlanningPaymentDateLabel.Text = string.Format("{0:dd MMM yyyy}", f.PlanningPaymentDate);

            int? bankType = f.BankType;
            if (bankType.HasValue)
            {
                BankTypeLabel.Text = (bankType == null) ? "" : bankType.str();

                BankTypeLookup.Value = bankType;
            }

            List<string> lstWbsNumbers = new List<string>();
            string prevWbs = null;
            string wbsNumber;
            foreach (PVFormDetail detail in f.Details)
            {
                wbsNumber = detail.WbsNumber;
                if ((prevWbs == null) && (wbsNumber != null))
                {
                    lstWbsNumbers.Add(wbsNumber);
                }
                else
                {
                    if ((wbsNumber != null) && !prevWbs.Equals(wbsNumber))
                    {
                        lstWbsNumbers.Add(wbsNumber);
                    }
                }
                prevWbs = wbsNumber;
            }

            string displayedWbs = String.Empty;
            int cntWbs = lstWbsNumbers.Count;
            if (cntWbs > 1)
            {
                f.SingleWbsUsed = false;
                displayedWbs = lstWbsNumbers[0];
                List<WBSStructure> lstWbsData = new List<WBSStructure>();
                foreach (string st in lstWbsNumbers)
                {
                    lstWbsData.Add(new WBSStructure()
                    {
                        WbsNumber = st
                    });
                }
                BudgetNumberListBox.DataSource = lstWbsData;
                BudgetNumberListBox.DataBind();
            }
            else
            {
                f.SingleWbsUsed = true;
                if (cntWbs == 1)
                {
                    displayedWbs = lstWbsNumbers[0];
                }
                else
                {
                    displayedWbs = f.BookingNo;
                }
            }
            BudgetNumberLabel.Text = displayedWbs;

            renderTotalAmount();

            formPersistence.fetchNote(f);
            NoteRepeater.DataSource = f.Notes;
            NoteRepeater.DataBind();

            formPersistence.fetchAttachment(f, ProcessID);
            updateAttachmentList();

            f.OpenExisting = true;
        }

        private bool DateRangeValid(DateTime? d)
        {
            return (d == null || ((DateTime)d).Year > 1969 && ((DateTime)d).Year <= 9999);
        }

        protected bool DocInWorklist = false;

        private void reloadFormData()
        {
            PVFormData _loaded = formPersistence.search(FormData.PVNumber.Value, FormData.PVYear.Value);

            if (_loaded != null)
            {
                _loaded.copyStatus(FormData);

                FormData = _loaded;
                FormData.InWorklist = hasPVAsWorklist();
                initFormTable();

                loadForm();

                FormData.OpenExisting = true;
                FormData.Editable = true;

                formPersistence.fetchAttachment(FormData, ProcessID);
                updateAttachmentList();
            }
            ReloadGrid();

            dump();
        }
        protected void resetInputFields()
        {
            clearScreenMessage();

            DocumentDateLabel.Text = null;
            DocumentNumberLabel.Text = null;
            IssuingDivisionLabel.Text = null;
            DocumentTypeDropDown.SelectedIndex = -1;
            PaymentMethodDropDown.SelectedIndex = -1;
            StatusLabel.Text = null;
            TransactionTypeLookup.Value = null;
            ActivityDateStart.Value = null;
            ActivityDateEnd.Value = null;
        }
        protected void initFormHeader()
        {
            if (FormData != null)
            {
                DocumentDateLabel.Text = string.Format("{0:dd/MM/yyyy}", FormData.PVDate);
                DocumentNumberLabel.Text = FormData.PVNumber.ToString();
            }
        }

        protected readonly string SID_DATA = "FormData";

        protected PVFormData FormData
        {
            set
            {
                Session[ScreenType + SID_DATA] = value;
            }
            get
            {
                PVFormData f = Session[ScreenType + SID_DATA] as PVFormData;
                if (f == null)
                {
                    f = new PVFormData((ScreenType == SCREEN_TYPE_PV) ? 1 : 2);

                    int pvNumber = 0;
                    int pvYear = int.Parse(string.Format("{0:yyyy}", DateTime.Now));
                    f.PVNumber = pvNumber;
                    f.PVYear = pvYear;
                    f.StatusCode = null;
                    PVFormTable Tabel = f.FormTable;

                    Session[ScreenType + SID_DATA] = f;
                }
                return f;
            }
        }


        protected PVFormTable Tabel
        {
            get
            {
                return FormData.FormTable;
            }
        }

        protected void initFormTable()
        {
        }
        protected bool hasPVAsWorklist()
        {
            List<WorklistHelper> lstWorklist = Worklist;
            if ((lstWorklist != null) && (lstWorklist.Any()))
            {

                string folio = FormData.PVNumber.ToString() + FormData.PVYear.ToString();
                foreach (WorklistHelper h in lstWorklist)
                {
                    if (h.Folio.Equals(folio))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private readonly string SID_WORKLIST = "UserWorklist";
        private List<WorklistHelper> Worklist
        {
            get
            {
                List<WorklistHelper> lstWorklist = Session[SID_WORKLIST] as List<WorklistHelper>;
                if (lstWorklist == null)
                {
                    Ticker.In("Form GetAllWorklistComplete " + UserName);
                    logic.k2.Open();

                    logic.Say("WorkList", "K2.GetAllWorklistComplete({0})", UserName);
                    lstWorklist = logic.k2.GetAllWorklistComplete(UserName);

                    logic.k2.Close();

                    Ticker.Out("Form GetAllWorklistComplete " + UserName);
                    Session[SID_WORKLIST] = lstWorklist;
                }
                else
                {
                }

                return lstWorklist;
            }
        }

        private void resetWorklist()
        {
            Session[SID_WORKLIST] = null;
        }
        #endregion Form

        #region Detail
        protected void evt_gridPVDetail_onSelectionChanged(object sender, EventArgs args)
        {
            int i = OnSelectionChanged(sender, args);
            string cc = "", des = "", curr = "", gla = "";
            decimal amt = 0;
            int seq = 0;
            if (i >= 0 && i < Tabel.DataList.Count)
            {
                seq = Tabel.DataList[i].SequenceNumber;
                cc = Tabel.DataList[i].CostCenterCode;
                curr = Tabel.DataList[i].CurrencyCode;
                des = Tabel.DataList[i].Description;
                amt = Tabel.DataList[i].Amount;
                gla = Tabel.DataList[i].GlAccount;
            }
            logic.Say("evt_gridPVDetail_onSelectionChanged", "{0} OnSelectionChanged [{1}] @{2} - {3} '{4}' {5} {6} {7}",
                ScreenType, i, seq, cc, des, curr, amt, gla);
        }
        protected void evt_gridPVDetail_onCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs arg)
        {
            string fieldName = arg.Column.FieldName;

            if (FormData.VendorGroupCode == PVFormData.EXTERNAL_VENDOR_GROUP_CODE && VouCol.disabledFields.Contains(fieldName))
            {
                arg.Editor.Enabled = false;
            }
        }
        protected void evt_gridPVDetail_onRowUpdating(object sender, ASPxDataUpdatingEventArgs args)
        {
            OnRowUpdating(sender, args);
        }
        protected void evt_gridPVDetail_onStartRowEditing(object sender, ASPxStartRowEditingEventArgs arg)
        {
            string key = arg.EditingKeyValue.ToString();

            if (FormData.EntryCostCenter)
            {
                ASPxGridLookup lookup = (ASPxGridLookup)DetailGrid.FindEditRowCellTemplateControl(
                (GridViewDataColumn)DetailGrid.Columns[VouCol.COST_CENTER_CODE], "lkpgridCostCenterCode");
                lookup.Focus();
            }
            else
            {
                ASPxEdit e = (ASPxEdit)DetailGrid.FindEditRowCellTemplateControl(
                  (GridViewDataColumn)DetailGrid.Columns[VouCol.DESCRIPTION], "tboxDescription");
                e.Focus();
            }

        }
        protected void evt_gridPVDetail_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs arg)
        {
            string param = arg.Parameters;
            if (param.isEmpty()) return;
            string[] paramFracs = param.Split(':');
            string prefix = (paramFracs != null && paramFracs.Length > 0) ? paramFracs[0] : "";
            if (!prefix.Equals("p")) return;
            string command = (paramFracs != null && paramFracs.Length > 1) ? paramFracs[1] : "";
            if (!command.Equals("update")) return;


            string argument = (paramFracs != null && paramFracs.Length > 2) ? paramFracs[2] : "";

            if (argument.Equals("TxCode"))
            {
                evt_ddlTransactionType_onSelectedIndexChanged(TransactionTypeLookup, EventArgs.Empty);
            }
            else if (argument.Equals("CostCenterCode"))
            {
                if (!isNull(paramFracs[3]))
                {
                    updateCostCenterCode(DetailGrid.EditingRowVisibleIndex, paramFracs[3]);
                    CostCenterAutoFill(paramFracs[3]);
                }
                else
                {
                    updateCostCenterCode(DetailGrid.EditingRowVisibleIndex, null);
                }
            }
            else if (argument.Equals("BankType"))
            {
                if (!isNull(paramFracs[3]))
                {
                    FormData.BankType = paramFracs[3].Int();
                }
                else
                {
                }
            }
            else if (argument.Equals("CurrencyCode"))
            {
                if (!isNull(paramFracs[3]))
                {
                    int row = DetailGrid.EditingRowVisibleIndex;
                    if (row >= 0 && row < FormData.Details.Count)
                    {
                        updateCurrencyCode(DetailGrid.EditingRowVisibleIndex, paramFracs[3]);
                    }
                    else
                    {
                    }
                }
                else
                {
                }
            }
        }

        protected void CostCenterAutoFill(string code)
        {
            if (!code.isEmpty())
            {
                string ccName = formPersistence.getCostCenterDescription(code);
                for (int i = 0; i < Tabel.DataList.Count; i++)
                {
                    if (Tabel.rows[i].CostCenterCode.isEmpty())
                    {
                        Tabel.rows[i].CostCenterCode = code;
                        Tabel.rows[i].CostCenterName = ccName;
                    }
                }
            }
        }

        protected void lkpgridCostCenterCode_Load(object sender, EventArgs arg)
        {
            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            if (!FormData.EntryCostCenter)
            {
                lookup.Visible = false;
                return;
            }
            int pf = 0;
            if (FormData._TransactionType != null)
            {
                pf = FormData._TransactionType.ProductionFlag;
            }
            List<CodeConstant> cc = formPersistence.getCostCenterCodes(pf);
            lookup.DataSource = cc;
            lookup.DataBind();
            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    PVFormDetail detail = Tabel.DataList[idxSelected];
                    lookup.Value = detail.CostCenterCode;
                }
            }
        }

        protected void BankType_OnTextChanged(object sender, EventArgs e)
        {
            string v = (sender as ASPxGridLookup).Value.str();
            logic.Say("BankType_OnTextChanged", v);
            FormData.BankType = v.Int();
            VendorBank vb = formPersistence.GetVendorBank(FormData.VendorCode, v);
            if (vb != null)
            {
                BankTypeLiteral.Text = vb.Name + " - " + vb.Account;
            }
            BankTypeLabel.Text = v;
        }
        protected void evt_imgDeleteRow_clicked(object sender, EventArgs args)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string sequenceNumber = image.Key;
            int numSequenceNumber = int.Parse(sequenceNumber.Trim());
            logic.Say("evt_imgDeleteRow_clicked", "seq={0}", numSequenceNumber);
            deleteRow(numSequenceNumber);
            ReloadGrid();
            StartEditingTabel();
        }

        public void deleteRow(int sequence)
        {
            List<PVFormDetail> dataList = Tabel.DataList;
            if (sequence <= dataList.Count)
            {
                PVFormDetail matched = null;
                foreach (PVFormDetail d in dataList)
                {
                    if (d.DisplaySequenceNumber == sequence)
                    {
                        matched = d;
                        break;
                    }
                }

                if (matched != null)
                {
                    dataList.Remove(matched);
                    int seqNumber = 0;
                    foreach (PVFormDetail d in dataList)
                    {
                        seqNumber++;
                        d.DisplaySequenceNumber = seqNumber;
                    }
                }
            }

            renderTotalAmount();
        }

        protected void evt_imgDeleteAllRow_clicked(object sender, EventArgs args)
        {
            logic.Say("evt_imgDeleteAllRow_clicked", "Delete All Row");
            deleteAllRow();
            ReloadGrid();
        }
        public int OnSelectionChanged(object sender, EventArgs args)
        {
            if (DetailGrid.EditingRowVisibleIndex > -1)
            {
                DetailGrid.UpdateEdit();
                int idxSelected = getSelectedRow();
                DetailGrid.StartEdit(idxSelected);
                return idxSelected;
            }
            else
                return -1;
        }
        public int getSelectedRow()
        {
            ReloadGrid();
            WebDataSelection selection = DetailGrid.Selection;
            int cntData = Tabel.DataList.Count;
            for (int i = 0; i < cntData; i++)
            {
                if (selection.IsRowSelected(i))
                {
                    return i;
                }
            }
            return 0;
        }
        public void OnCtrlUpKeypress()
        {
            int idxEditing = DetailGrid.EditingRowVisibleIndex;
            int cntData = Tabel.DataList.Count;
            if ((idxEditing >= 0) && (idxEditing <= (cntData - 1)))
            {
                DetailGrid.UpdateEdit();
                ReloadGrid();
                if (idxEditing > 0)
                {
                    idxEditing = idxEditing - 1;
                }
                else
                {
                    idxEditing = 0;
                }
                DetailGrid.StartEdit(idxEditing);
            }
        }
        public void OnCtrlDownKeypress()
        {
            int idxEditing = DetailGrid.EditingRowVisibleIndex;
            int cntData = Tabel.DataList.Count;
            int lowerBound = cntData - 1;
            if ((idxEditing >= 0) && (idxEditing <= lowerBound))
            {
                DetailGrid.UpdateEdit();
                ReloadGrid();
                if (idxEditing < lowerBound)
                {
                    idxEditing = idxEditing + 1;
                }
                else
                {
                    idxEditing = lowerBound;
                }

                DetailGrid.StartEdit(idxEditing);
            }
        }

        public void OnTabKeypress(bool taxCalculated)
        {
            DetailGrid.UpdateEdit();
            int idxSelected = Tabel.EditIndex;
            int lastDataIndex = Tabel.DataList.Count - 1;
            if (Tabel.EditIndex == lastDataIndex)
            {
                Tabel.addNewRow(taxCalculated);
                idxSelected = Tabel.EditIndex + 1;
            }
            else
            {
                idxSelected++;
            }

            ReloadGrid();
            DetailGrid.StartEdit(idxSelected);
            renderTotalAmount();
        }

        protected void evt_imgAddRow_clicked(object sender, EventArgs arg)
        {
            logic.Say("evt_imgAddRow_clicked", "Add Row {0}", Tabel.DataList.Count + 1);
            OnTabKeypress(CalculateTaxCheckBox.Checked);
        }

        protected void evt_onCtrlUpKeypress(object sender, EventArgs arg)
        {
            logic.Say("evt_onCtrlUpKeypress", "Up");
            OnCtrlUpKeypress();
        }
        protected void evt_onCtrlDownKeypress(object sender, EventArgs arg)
        {
            logic.Say("evt_onCtrlDownKeypress", "Down");
            OnCtrlDownKeypress();
        }
        protected void evt_gridDetail_ReturnKeypress(object sender, EventArgs arg)
        {
            logic.Say("evt_gridDetail_ReturnKeypress", "Enter");
            OnTabKeypress(CalculateTaxCheckBox.Checked);
        }
        protected void evt_lblCostCenterName_onLoad(object sender, EventArgs arg)
        {
        }
        protected void evt_ddlCurrencyCode_Load(object sender, EventArgs arg)
        {
            ASPxComboBox ddlCurrencyCode = (ASPxComboBox)sender;
            List<CodeConstant> c = formPersistence.getCurrencyCodeList();
            ddlCurrencyCode.DataSource = c;
            ddlCurrencyCode.DataBind();
        }
        protected void evt_tboxDescription_onLoad(object sender, EventArgs arg)
        {
            if (sender == null)
            {
                return;
            }

            if (FormData.VendorGroupCode == PVFormData.EXTERNAL_VENDOR_GROUP_CODE && (Tabel.InvoiceCount() > 0))
            {
                ASPxTextBox tbox = (ASPxTextBox)sender;
                tbox.Enabled = false;
            }


        }
        protected void evt_tboxAmount_onLoad(object sender, EventArgs arg)
        {
            if (sender == null)
            {
                return;
            }

            if (FormData.VendorGroupCode == PVFormData.EXTERNAL_VENDOR_GROUP_CODE && (Tabel.InvoiceCount() > 0))
            {
                ASPxTextBox tbox = (ASPxTextBox)sender;
                tbox.Enabled = false;
            }
        }

        protected void evt_tboxAmount_TabKeypress(object sender, EventArgs e)
        {
            OnTabKeypress(CalculateTaxCheckBox.Checked);
        }

        private void updateCostCenterCode(int row, string code)
        {
            int idxEditing = DetailGrid.EditingRowVisibleIndex;
            DetailGrid.UpdateEdit();

            string lastCC = Tabel.rows[row].CostCenterCode;
            int itemTransaction = Tabel.rows[row].ItemTransaction ?? 1;

            Tabel.rows[row].CostCenterCode = code;
            Tabel.rows[row].CostCenterName = code.isEmpty() ? "" : formPersistence.getCostCenterDescription(code);
            //Tabel.rows[row].GlAccount = logic.PVForm.GLAccount(code, FormData.TransactionCode ?? 0, itemTransaction).str();

            // Start Rinda Rahayu 20160525
            if (idxEditing == 0)
            {
                Tabel.rows[row].GlAccount = logic.PVForm.GLAccount(code, FormData.TransactionCode ?? 0, itemTransaction).str();
            }
            // End Rinda Rahayu 20160525

            logic.Say("updateCostCenterCode(" + row.str() + "," + code + ")",
                    "[{0}] from: {1} to: {2} GLAccount: {3} ", row, lastCC, code, Tabel.rows[row].GlAccount);

            ReloadGrid();

            HiddenGridFocusedColumn.Value = VouCol.DESCRIPTION;

            DetailGrid.StartEdit(idxEditing);
        }

        private void updateCurrencyCode(int row, string code)
        {
            int idxEditing = DetailGrid.EditingRowVisibleIndex;
            DetailGrid.UpdateEdit();

            Tabel.rows[row].CurrencyCode = code.ToUpper();

            ASPxComboBox cocurr = DetailGrid.FindEditRowCellTemplateControl(
                (GridViewDataColumn)DetailGrid.Columns[VouCol.CURRENCY_CODE], "coCurrencyCode") as ASPxComboBox;
            if (cocurr != null)
            {
                cocurr.Value = code;
            }

            ReloadGrid();

            HiddenGridFocusedColumn.Value = VouCol.AMOUNT;

            DetailGrid.StartEdit(idxEditing);
        }
        #endregion

        #region Screen Message
        protected List<ScreenMessage> ScreenMessageList
        {
            get
            {
                List<ScreenMessage> lstMessages = Session[ScreenType + "MsgList"] as List<ScreenMessage>;
                if (lstMessages == null)
                {
                    lstMessages = new List<ScreenMessage>();
                    Session[ScreenType + "MsgList"] = lstMessages;
                    HiddenMessageExpandFlag.Value = "false";
                }
                return lstMessages;
            }
        }
        protected void postScreenMessage()
        {
            postScreenMessage(null, false, true);
        }

        protected void postScreenMessage(ScreenMessage[] messages, bool clearFirst, bool instantDisplay)
        {
            List<ScreenMessage> lstMessage = ScreenMessageList;
            if (clearFirst)
            {
                lstMessage.Clear();
            }

            if (messages != null)
            {
                for (int i = 0; i < messages.Length; i++)
                {
                    lstMessage.Insert(0, messages[i]);
                }
            }
            updateScreenMessages();
        }
        protected void updateScreenMessages()
        {
            List<ScreenMessage> lstMessages = ScreenMessageList;
            int cntMessages = lstMessages.Count;
            if (cntMessages > 0)
            {
                ScreenMessage msg = lstMessages[0];
                StringBuilder stringBuilder = new StringBuilder();
                string expandFlag = HiddenMessageExpandFlag.Value;

                string messageBoxClass = "message-info";
                string messageBoxSpanClass = "message-span-info";
                if (msg.Status == ScreenMessage.STATUS_WARNING)
                {
                    messageBoxClass = "message-warning";
                    messageBoxSpanClass = "message-span-warning";
                }
                else if (msg.Status == ScreenMessage.STATUS_ERROR)
                {
                    messageBoxClass = "message-error";
                    messageBoxSpanClass = "message-span-error";
                }

                if (expandFlag.Equals("true"))
                {
                    messageBoxClass += " expanded-message-box";
                }
                stringBuilder.AppendLine(string.Format("<div id='messageBox' class='{0}'>", messageBoxClass));
                stringBuilder.AppendLine(string.Format("      <span class='{0}'>{1}</span>", messageBoxSpanClass, msg.Message));

                if (cntMessages > 1)
                {
                    string txtMore = "more";
                    if (expandFlag.Equals("true"))
                    {
                        txtMore = "close";
                    }
                    stringBuilder.AppendLine("      <span class='message-span-more'>");
                    stringBuilder.AppendLine(string.Format("          <a id='message-detail-link' href='#' onclick=\"expandScreenMessage()\">{0}</a>", txtMore));
                    stringBuilder.AppendLine("      </span>");
                }

                stringBuilder.AppendLine("</div>");
                if (expandFlag.Equals("false"))
                {
                    stringBuilder.AppendLine("<div id='messageBox-all' style='display: none'>");
                }
                else
                {
                    stringBuilder.AppendLine("<div id='messageBox-all'>");
                }
                stringBuilder.AppendLine("  <ul>");
                string listClass;
                for (int i = 1; i < cntMessages; i++)
                {
                    msg = lstMessages[i];
                    listClass = "message-detail-info";
                    if (msg.Status == ScreenMessage.STATUS_WARNING)
                    {
                        listClass = "message-detail-warning";
                    }
                    else if (msg.Status == ScreenMessage.STATUS_ERROR)
                    {
                        listClass = "message-detail-error";
                    }

                    stringBuilder.AppendLine(string.Format("<li class='{0}'>", listClass));
                    stringBuilder.AppendLine("      <span>" + msg.Message + "</span>");
                    stringBuilder.AppendLine("</li>");
                }
                stringBuilder.AppendLine("  </ul>");
                stringBuilder.AppendLine("</div>");

                MessageControlLiteral.Text = stringBuilder.ToString();
                MessageControlLiteral.Visible = true;
            }
            else
            {
                MessageControlLiteral.Text = "";
                MessageControlLiteral.Visible = false;
            }
        }
        protected void clearScreenMessage()
        {
            ScreenMessageList.Clear();
            updateScreenMessages();
        }
        protected void evt_messageControl_onLoad(object sender, EventArgs args)
        {
            updateScreenMessages();
        }
        #endregion Screen Message

        #region Header
        protected bool validateFinanceHeader()
        {
            clearScreenMessage();
            string paymentMethod = null;
            if (PaymentMethodDropDown.Visible && PaymentMethodDropDown.SelectedItem != null)
            {
                paymentMethod = PaymentMethodDropDown.SelectedItem.Value.ToString();
            }
            else
            {
                paymentMethod = FormData.PaymentMethodCode;
            }
            if ((paymentMethod.Equals("T")) && (FormData.BankType == null))
            {
                //var q = formPersistence.GetVendorBank(FormData.VendorCode);
                int vendorbankCount = formPersistence.Qx<int>("GetVendorBanksCountByVendor", new { @vendorCd = FormData.VendorCode }).First();
                if (vendorbankCount < 1 && (FormData.VendorGroupCode != 4))
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Vendor doesn't have Bank");
                }
                if (vendorbankCount > 1)
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Bank Type");
                }
            }

            return ScreenMessageList.Count == 0;
        }
        protected bool validateGeneralData()
        {
            string msgNullOrEmpty = _m.Message("MSTD00017WRN");
            int? docType = FormData.PVTypeCode;
            if (!docType.HasValue)
            {
                object objDocType = DocumentTypeDropDown.Value;
                if (objDocType != null)
                {
                    docType = Convert.ToInt32(objDocType);
                }
            }

            if (DocumentTypeDropDown.Visible)
            {

                if (DocumentTypeDropDown.SelectedItem == null
                    &&
                        (docType == null ||
                        (docType.Value != 2 && ScreenType.Equals(SCREEN_TYPE_RV)) ||
                        (docType.Value != 3 && ScreenType.Equals(SCREEN_TYPE_PV))
                    ))

                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", ScreenType + " Type");
            }
            else if (docType == null)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", ScreenType + " Type");
            }


            if (PaymentMethodDropDown.Visible)
            {
                if (PaymentMethodDropDown.SelectedItem == null)
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Payment Method");
            }
            else if (FormData.PaymentMethodCode.isEmpty())
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Payment Method");
            }

            string vendorCode = VendorCodeLookup.Text;
            if (FormData.VendorGroupCode == PVFormData.EXTERNAL_VENDOR_GROUP_CODE || !VendorCodeLookup.Visible)
            {
                vendorCode = FormData.VendorCode;
            }
            if (string.IsNullOrEmpty(vendorCode) || string.IsNullOrWhiteSpace(vendorCode))
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Vendor Code");
            }
            if (TransactionTypeLookup.Visible && TransactionTypeLookup.Value == null)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Transaction Type");
            }
            else if (FormData.TransactionCode == null)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Transaction Type");

            if (docType != 1)
            {
                if (ActivityDateStart.Visible || ActivityDateEnd.Visible)
                {

                    if ((ActivityDateStart.Value == null) || (ActivityDateEnd.Value == null))
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Activity Date");
                    }
                    else if ((ActivityDateStart.Value != null) || (ActivityDateEnd.Value != null))
                    {
                        DateTime activityDate = (DateTime)ActivityDateStart.Value;
                        DateTime activityDateTo = (DateTime)ActivityDateEnd.Value;
                        ValidateActivityDate(activityDate, activityDateTo, docType ?? 0);
                    }
                }
                else
                {
                    if ((FormData.ActivityDate == null) || (FormData.ActivityDateTo == null))
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Activity Date");
                    }
                    ValidateActivityDate(FormData.ActivityDate ?? DateTime.Now, FormData.ActivityDateTo ?? DateTime.Now, docType ?? 0);
                }
            }


            if (FormData.Budgeted)
            {
                // fid.Hadid 20180626, Use Booking No instead of Budget No for accrued PV
                //if (string.IsNullOrEmpty(FormData.BudgetNo))
                if (string.IsNullOrEmpty(FormData.BookingNo))
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Booking No");
                }
            }

            List<PVFormDetail> lstDetail = Tabel.getBlankRowCleanedDetails().OrderByDescending(x => x.CostCenterCode).ToList();
            if (lstDetail.Count == 0)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Details");
                FormData.Details = lstDetail;
            }
            else
            {
                bool ignoreMinusAmount = (Mode == VIEW_MODE) || ((logic.Sys.GetValue("PvForm", "AllowNegativeAmount") ?? 0) == 1);

                if (Tabel.hasInvalidDetail(ignoreMinusAmount)) // ignore zeroes when mode is view
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Currency Code and Amount of details");
                }
                if (Tabel.hasEmptyDescription())
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Description");
                }
                List<string> ecc = new List<string>();
                FormData._TransactionType = formPersistence.GetTransactionType(FormData.TransactionCode ?? 0);

                if (FormData._TransactionType != null && !Tabel.validCostCenter(NonCostCenterGlAccounts, FormData._TransactionType.CostCenterFlag, ecc))
                {
                    for (int ei = ecc.Count - 1; ei >= 0; ei--)
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ecc[ei]);
                    }
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Invalid Cost Center");
                }

                // Start Rinda Rahayu 20160414
                if (FormData._TransactionType != null && FormData._TransactionType.CostCenterFlag == true &&
                           (lstDetail[0].CostCenterCode == null || lstDetail[0].CostCenterCode.Equals("")))
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Cost Center");
                }
                // End Rinda Rahayu 20160414


                if (FormData._TransactionType != null && !Tabel.validTxInGlAccount(FormData._TransactionType))
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Please choose correct transaction type related to PRD/Non PRD");
                }
            }

            if (FormData.AttachmentRequired)
            {
                int attachments = 0;
                foreach (var a in FormData.AttachmentTable.Attachments)
                {
                    if (!string.IsNullOrEmpty(a.DisplayFilename) || !string.IsNullOrEmpty(a.CategoryCode))
                        attachments++;
                }
                if (attachments < 1)
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00103ERR");
                else
                {
                    const string ENTERTAINMENT_MANDATORY_ATTACH = "3";
                    if (EntertainmentTransactionTypes.Contains(FormData.TransactionCode ?? 0))
                    {
                        var hasXls = FormData.AttachmentTable.Attachments
                            .Where(a => a.CategoryCode == ENTERTAINMENT_MANDATORY_ATTACH
                            && (a.FileName != null
                                && (a.FileName.EndsWith(".xls") || a.FileName.EndsWith(".xlsx"))
                            ));

                        if (!hasXls.Any())
                        {
                            PostLater(ScreenMessage.STATUS_ERROR,
                                "MSTD00002ERR",
                                "Entertainment Transaction must have "
                                + formPersistence.getAttachmentCategoryName(ENTERTAINMENT_MANDATORY_ATTACH)
                                + " in Excel format");
                        }
                    }
                }
            }

            return ScreenMessageList.Count == 0;
        }

        private bool ValidateActivityDate(DateTime activityDate, DateTime activityDateTo, int docType)
        {
            var r = false;
            r = ValidateDateRange(activityDate, "Activity Date From");
            if (!r) return r;
            r = ValidateDateRange(activityDateTo, "Activity Date To");
            if (!r) return r;

            if (DateTime.Compare(activityDate, activityDateTo) > 0)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00083ERR", "Activity Date From", "Activity Date To");
                return false;
            }
            else if (ScreenType.Equals(SCREEN_TYPE_PV))
            {
                DateTime today = DateTime.Now.Date;
                if (docType == 2)
                {
                    if (DateTime.Compare(today, activityDate) > 0)
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00094ERR", "Activity Date From", "Current date");
                        return false;
                    }
                }
            }
            return true;
        }

        private bool ValidateDateRange(Control o, string e)
        {
            ASPxDateEdit d = o as ASPxDateEdit;
            if (d.Value != null)
            {
                DateTime a = (DateTime)d.Value;
                if (!DateRangeValid(a))
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00107ERR", e, "1970", "9999");
                    return false;
                }
            }
            return true;
        }

        private bool ValidateDateRange(DateTime a, string e)
        {

            if (!DateRangeValid(a))
            {
                PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00107ERR", e, "1970", "9999");
                return false;
            }

            return true;
        }

        protected bool performNonSubmissionValidation()
        {
            List<PVFormDetail> details = FormData.Details.OrderByDescending(x => x.CostCenterCode).ToList();
            decimal amount;
            DetailGrid.UpdateEdit();
            string[] roundCurrs = Common.AppSetting.RoundCurrency;
            string roundCurrencies = string.Join(" and ", roundCurrs, 0, roundCurrs.Length);
            foreach (PVFormDetail d in details)
            {
                if (!string.IsNullOrEmpty(d.CurrencyCode) && roundCurrs.Contains(d.CurrencyCode))
                {
                    amount = d.Amount;

                    if (amount.Frac() > 0 && FormData.PVTypeCode != 3)
                    {
                        PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00102ERR", roundCurrencies);
                        StartEditingTabel();
                        return false;
                    }
                }
            }


            return true;
        }

        public void ReloadGrid()
        {
            DetailGrid.DataSource = Tabel.DataList;
            DetailGrid.SettingsPager.PageSize = Tabel.DataList.Count;
            DetailGrid.DataBind();
        }

        public void initFirstRow()
        {
            Tabel.DataList.Clear();
            Tabel.addNewRow(false);
            ReloadGrid();
            DetailGrid.StartEdit(0);
            Tabel.EditIndex = 0;
            DetailGrid.SettingsBehavior.AllowFocusedRow = false;
        }

        protected void lkpgridVendorCode_Load(object sender, EventArgs arg)
        {
            if (Mode == VIEW_MODE) return;
            List<VendorData> vendorCodes = formPersistence.getVendorCodes();
            VendorCodeLookup.DataSource = vendorCodes;
            VendorCodeLookup.DataBind();
        }
        protected void evt_lblVendorCode_onLoad(object sender, EventArgs arg)
        {
            string vendorCode = VendorCodeLookup.Value.str();
            if (FormData.OpenExisting)
            {
                vendorCode = FormData.VendorCode;
            }
            if (!string.IsNullOrEmpty(vendorCode))
            {
                VendorDescriptionLabel.Text = formPersistence.getVendorCodeName(vendorCode);
                CodeConstant vendorGroup = formPersistence.getVendorGroupNameByVendor(vendorCode);
                VendorGroupLabel.Text = (vendorGroup != null) ? vendorGroup.Description : "";
            }
        }

        protected void evt_lblBooking_onLoad(object sender, EventArgs arg)
        {
            string selectedBookingNo = BookingNoLookup.Value.str();
            if (FormData.OpenExisting)
            {
                selectedBookingNo = FormData.BookingNo;
            }
            if (!string.IsNullOrEmpty(selectedBookingNo))
            {
                //string tes = Convert.ToString(UserData.DIV_CD);
                List<WBSStructure> wbs = formPersistence.WbsNumbers(selectedBookingNo);
                var desc = wbs[0].Description;
                var wbsno = wbs[0].WbsNumber;
                CodeConstant vendorGroup = formPersistence.getVendorGroupNameByVendor(selectedBookingNo);
                BookingWbs.Text = (selectedBookingNo != null) ? wbsno + '/' + desc + '/' + selectedBookingNo : "";
            }
        }
        protected void evt_btSubmitNewVendor_Click(object sender, EventArgs arg)
        {

            string searchTerm = NewVendorSearchTerm.Text;
            string vendorName = NewVendorName.Text;
            logic.Say("btSubmitNewVendor_Click", "New Vendor name={0} search_term={1}", vendorName, searchTerm);
            if (string.IsNullOrEmpty(searchTerm) || string.IsNullOrEmpty(vendorName))
            {
                VendorCodeLookup.Value = formPersistence.getVendorCodeByName(vendorName);

                PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00106ERR", "");

                return;
            }

            VendorData v = formPersistence.Qx<VendorData>("GetVendorDataBySearchTerms",
                    new { term = searchTerm }).FirstOrDefault();

            if (v != null)
            {
                PostNow(ScreenMessage.STATUS_ERROR, "MSTD00056ERR", "Vendor '" + v.VENDOR_NAME + "' with Reg No: ", searchTerm);
                return;
            }

            bool succeed = logic.Vendor.InsertVendor(
                new VendorData()
                {
                    VENDOR_CD = "",
                    VENDOR_NAME = NewVendorName.Text,
                    SEARCH_TERMS = searchTerm,
                    DIVISION_ID = Convert.ToString(UserData.DIV_CD),
                    DIVISION_NAME = ""
                }, UserData);
            if (succeed)
            {
                Page.Cache.Remove("form.VendorCodes");
                lkpgridVendorCode_Load(VendorCodeLookup, EventArgs.Empty);

                VendorCodeLookup.Value = formPersistence.getVendorCodeByName(NewVendorName.Text);
                VendorDescriptionLabel.Text = vendorName;
                CodeConstant group = formPersistence.getVendorGroupNameByVendor(VendorCodeLookup.Value.str());
                VendorGroupLabel.Text = group != null ? group.Description : "";
                PostLaterOn(ScreenMessage.STATUS_INFO, "MSTD00105INF", "");
            }
            else
            {
                PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00105ERR", "");
            }
        }
        protected void LookUpBookingNo_TextChanged(object sender, EventArgs arg)
        {

            object selectedBookingNo = BookingNoLookup.Value;
            FormData.BookingNo = selectedBookingNo != null ? Convert.ToString(selectedBookingNo) : null;
            logic.Say("LookUpBudgetNo_TextChanged", "Budget No {0}", selectedBookingNo.str());
            BudgetNumberLabel.Text = Convert.ToString(selectedBookingNo);
        }

        // fid.Hadid 20180406. CR Accrued
        protected void LookupBookingNo_Load(object sender, EventArgs arg)
        {
            if (Mode == VIEW_MODE) return;

            //if (Mode == 2)
            //{
            //    FormData.BookingNo = FormData.BudgetNumber;
            //}
            object selectedBookingNo = (FormData.BookingNo != null ? FormData.BookingNo : FormData.BudgetNumber);
            List<BookingNoStructure> lstBookingNo = formPersistence.getBookingNo(UserData, FormData);
            BookingNoLookup.DataSource = lstBookingNo;
            BookingNoLookup.DataBind();
            BookingNoLookup.Value = selectedBookingNo;
            if (selectedBookingNo != null && !selectedBookingNo.Equals(""))
            {
                DivShowRemainingBudget.Visible = true;
                DivRemainingBudget.Visible = false;
                RemainingBudgetLinkButton.Visible = true;
            }
            else
            {
                DivShowRemainingBudget.Visible = false;
                DivRemainingBudget.Visible = false;
                RemainingBudgetLinkButton.Visible = false;
            }
        }
        // End fid.Hadid 20180406.
        protected void ddlPaymentMethod_Load(object sender, EventArgs arg)
        {
            if (Mode == VIEW_MODE) return;
            List<CodeConstant> lstPaymentMethod = BusinessLogic.CommonData.PaymentMethod;
            int? vendorGroup = FormData.VendorGroupCode;
            if (vendorGroup == PVFormData.EXTERNAL_VENDOR_GROUP_CODE)
            {
                List<CodeConstant> lstFilteredPaymentMethod = new List<CodeConstant>();
                string val;
                foreach (CodeConstant item in lstPaymentMethod)
                {
                    val = (string)item.Code;
                    if (val.Equals("A") || val.Equals("T"))
                    {
                        lstFilteredPaymentMethod.Add(item);
                    }
                }

                PaymentMethodDropDown.DataSource = lstFilteredPaymentMethod;
            }
            else
            {
                PaymentMethodDropDown.DataSource = lstPaymentMethod;
            }

            PaymentMethodDropDown.DataBind();
        }

        //Start Rinda Rahayu 20160516
        protected void ddlPaymentMethod_LoadRvView(object sender, EventArgs arg)
        {
            if (Mode == VIEW_MODE)
            {
                List<CodeConstant> lstPaymentMethod = BusinessLogic.CommonData.PaymentMethod;
                int? vendorGroup = FormData.VendorGroupCode;
                if (vendorGroup == PVFormData.EXTERNAL_VENDOR_GROUP_CODE)
                {
                    List<CodeConstant> lstFilteredPaymentMethod = new List<CodeConstant>();
                    string val;
                    foreach (CodeConstant item in lstPaymentMethod)
                    {
                        val = (string)item.Code;
                        if (val.Equals("A") || val.Equals("T"))
                        {
                            lstFilteredPaymentMethod.Add(item);
                        }
                    }

                    PaymentMethodDropDown.DataSource = lstFilteredPaymentMethod;
                }
                else
                {
                    PaymentMethodDropDown.DataSource = lstPaymentMethod;
                }

                PaymentMethodDropDown.DataBind();
            }
        }
        // End Rinda Rahayu 20160516
        protected void evt_ddlPaymentMethod_onSelectedIndexChanged(object sender, EventArgs arg)
        {
            logic.Say("evt_ddlPaymentMethod_onSelectedIndexChanged", "Payment Method= {0}", (
                (PaymentMethodDropDown.SelectedItem == null) ? null : PaymentMethodDropDown.SelectedItem.Value));

            if ((Tabel != null) && (DetailGrid.EditingRowVisibleIndex > -1))
            {
                int idxEditing = DetailGrid.EditingRowVisibleIndex;
                DetailGrid.UpdateEdit();

                string paymentMethod = (string)PaymentMethodDropDown.SelectedItem.Value;
                if (!paymentMethod.isEmpty())
                {
                    FormData.PaymentMethodCode = paymentMethod;
                }
                if (!paymentMethod.isEmpty() &&
                    (paymentMethod.Equals(PVFormData.PAYMENT_METHOD_CASH)
                     || paymentMethod.Equals(PVFormData.PAYMENT_METHOD_CHEQUE))
                   )
                {
                    string upperCode;
                    foreach (PVFormDetail detail in FormData.Details)
                    {
                        if (detail.CurrencyCode != null)
                        {
                            upperCode = detail.CurrencyCode.ToUpper();
                            if (!upperCode.Equals("IDR") && !upperCode.Equals("USD") && !upperCode.Equals("JPY"))
                            {
                                detail.CurrencyCode = null;
                                detail.Amount = 0;
                            }
                        }
                    }

                }

                ReloadGrid();
                DetailGrid.StartEdit(idxEditing);
                renderTotalAmount();
            }
        }
        protected void ddlDocType_Load(object sender, EventArgs arg)
        {
            if (Mode == VIEW_MODE) return;
            List<CodeConstant> dt = formPersistence.getDocTypes();
            
            //fid.Hadid only Direct type in PV Accrued
            dt = dt.Where(x => x.Code == "1").ToList();
            
            DocumentTypeDropDown.DataSource = dt;
            DocumentTypeDropDown.DataBind();
        }
        protected void evt_ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            logic.Say("evt_ddlDocType_SelectedIndexChanged", "DocType= {0}", DocumentTypeDropDown.Value);
            FormData.PVTypeCode = DocumentTypeDropDown.Value.Int();
            ddlTransactionType_Load(TransactionTypeLookup, EventArgs.Empty);
        }
        protected void ddlTransactionType_Load(object sender, EventArgs arg)
        {
            if (Mode == VIEW_MODE) return;
            if (Mode == EDIT_MODE)
            {

            }
            int vGroupCode = FormData.VendorGroupCode ?? 1;
            if (!(new int[] { 1, 3 }).Contains(vGroupCode))
                vGroupCode = 1;

            List<TransactionType> tt = formPersistence.getTransactionTypes(
                UserData, -1, vGroupCode
                , (ScreenType == SCREEN_TYPE_PV && FormData.PVTypeCode == 2)
                , FormData.FormTable.InvoiceCount() <= 0
                , logic.User.isAuthorizedFinance(UserData)
                , UserCanUploadDetail
                );

            //fid.Hadid : Only retrieve trans type accrued
            tt = tt.Where(x => x.Code == ACCRUED_TRANS.ToString()).ToList();

            TransactionTypeLookup.DataSource = tt;
            TransactionTypeLookup.DataBind();
        }
        protected void VendorCode_TextChanged(object sender, EventArgs e)
        {
            FormData.VendorCode = VendorCodeLookup.Value.str();
            logic.Say("VendorCode_TextChanged", "Vendor Code= {0}", VendorCodeLookup.Value);

            if (!string.IsNullOrEmpty(FormData.VendorCode))
            {
                VendorDescriptionLabel.Text = formPersistence.getVendorCodeName(FormData.VendorCode);
                CodeConstant vendorGroup = formPersistence.getVendorGroupNameByVendor(FormData.VendorCode);
                VendorGroupLabel.Text = (vendorGroup != null) ? vendorGroup.Description : "";
                FormData.VendorGroupCode = vendorGroup.Code.Int();
            }
            PaymentMethodDropDown.DataBind();
        }

        private void ReviewTxCode()
        {
            PVFormData f = FormData;
            int txCode = f.TransactionCode ?? 0;
            if (f.TransactionCode != null)
            {
                f._TransactionType = formPersistence.GetTransactionType(f.TransactionCode ?? 0);


                DownloadEntertainmentTemplateLink.Visible = EntertainmentTransactionTypes.Contains(f.TransactionCode ?? 0);
                if (f._TransactionType != null)
                    f.Budgeted = f._TransactionType.BudgetFlag;
                else
                    f.Budgeted = false;
                if (!f.Budgeted) f.BudgetNumber = null;

                if (!f.EntryCostCenter)
                {
                    Tabel.EmptyCostCenter();
                }

                if (f._TransactionType != null)
                    DownloadUploadTemplateLink.NavigateUrl = NavigateTemplate(FormData._TransactionType.TemplateName);
            }
            else
            {
                f.Budgeted = false;
                f.AttachmentRequired = false;
                f.EntryCostCenter = true;
            }
        }
        protected void evt_ddlTransactionType_onSelectedIndexChanged(object sender, EventArgs e)
        {
            object objTxCode = TransactionTypeLookup.Value;

            if (objTxCode != null)
            {
                FormData.TransactionCode = objTxCode.Int();
            }
            TransactionCodeRefresh(true);
        }

        protected void TransactionCodeRefresh(bool DoUpdateGLAccount = false)
        {
            logic.Say("TransactionCodeRefresh", "Transaction code= {0}", FormData.TransactionCode);
            ReviewTxCode();
            if (DoUpdateGLAccount)
                FormData.UpdateGlAccount(logic.PVForm);
            int idxEditing = DetailGrid.EditingRowVisibleIndex;
            if (idxEditing == -1)
            {
                idxEditing = 0;
            }
            DetailGrid.UpdateEdit();

            int? txCode = FormData.TransactionCode;
            if (txCode != null)
            {
                string defaultDescriptionWording = formPersistence.getDescriptionDefaultWording(txCode);
                Tabel.StandardWording = defaultDescriptionWording;
            }
            ReloadGrid();
            DetailGrid.StartEdit(idxEditing);

            bool showUpload = txCode != null && FormData.CanUploadDetail;

            DataUpload.Visible = showUpload;
            DataUploadButton.Visible = showUpload;
            DownloadUploadTemplateLink.Visible = showUpload;
            ShowUpload(txCode != null && FormData.CanUploadDetail);

            LookupBookingNo_Load(BookingNoLookup, EventArgs.Empty);

            evaluateHeaderVisibility();
        }

        protected string NavigateTemplate(string t)
        {
            return string.Format(logic.Sys.GetText("TEMPLATE.FMT", "UPLOAD"), t);
        }

        protected void evt_chkCalculateTax_onCheckedChanged(object sender, EventArgs arg)
        {
            bool selected = CalculateTaxCheckBox.Checked;
            List<SystemMaster> lstSetting = logic.Sys.GetSystemMaster("DEFAULT_TAX_CODE");
            string key = selected ? "1" : "0";

            string defaultTaxCode = "V0";
            int cntSetting = lstSetting != null ? lstSetting.Count : 0;
            SystemMaster systemMaster;
            for (int i = cntSetting - 1; i >= 0; i--)
            {
                systemMaster = lstSetting[i];
                if (systemMaster.Code.Equals(key))
                {
                    defaultTaxCode = (string)systemMaster.Value;
                    break;
                }
            }

            int idxEditing = DetailGrid.EditingRowVisibleIndex;
            DetailGrid.UpdateEdit();


            FormData.TaxCalculated = selected;

            List<PVFormDetail> details = Tabel.DataList;
            int cntDetail = details.Count;
            PVFormDetail detail;
            for (int i = cntDetail - 1; i >= 0; i--)
            {
                detail = details[i];
                detail.TaxCode = defaultTaxCode;
            }
            ReloadGrid();
            DetailGrid.StartEdit(idxEditing);
        }
        //protected void evt_gridpopBankType_Load(object sender, EventArgs arg)
        //{
        //    List<VendorBank> v = formPersistence.getVendorBanks(FormData.VendorCode);

        //    BankTypeLookup.DataSource = v;
        //    BankTypeLookup.DataBind();
        //}
        protected void evt_lblVendorDesc_onLoad(object sender, EventArgs arg)
        {
            string vendorCode = VendorCodeLookup.Text.Trim();
            if (!string.IsNullOrEmpty(vendorCode))
            {
                VendorDescriptionLabel.Text = formPersistence.getVendorCodeName(vendorCode);
                CodeConstant vendorGroup = formPersistence.getVendorGroupNameByVendor(vendorCode);
                if (vendorGroup != null)
                    VendorGroupLabel.Text = vendorGroup.Description;
                else
                    VendorGroupLabel.Text = " ? ";
            }
        }
        #endregion

        #region Attachment
        protected void updateAttachmentList()
        {
            AttachmentTable attachmentTable = FormData.AttachmentTable;
            GridViewColumn column = AttachmentGrid.Columns["#"];
            column.Visible = attachmentTable.Editing;
            AttachmentGrid.DataSource = attachmentTable.Attachments;
            AttachmentGrid.DataBind();
        }

        protected AttachID GetAttachID()
        {
            AttachID a = new AttachID()
            {
                No = ((FormData.PVNumber ?? 0) > 0) ? (FormData.PVNumber ?? 0).str() : "T" + ProcessID.str(),
                Year = (FormData.PVYear ?? DateTime.Now.Year).str(),
                Seq = 0
            };

            return a;
        }

        protected void evt_btSendUploadAttachment_clicked(object sender, EventArgs arg)
        {
            if (AttachmentUpload.HasFile)
            {
                logic.Say("btSendUploadAttachment_Click", "FileName = {0}", AttachmentUpload.FileName);
                string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                int maxSize = int.Parse(strMaxSize);
                maxSize = maxSize * 1024;
                if (AttachmentUpload.FileBytes.Length > maxSize)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                    AttachmentPopup.Hide();
                    return;
                }

                bool uploadSucceded = true;
                string errorMessage = null;

                AttachID a = GetAttachID();
                string filename = CommonFunction.CleanFilename(AttachmentUpload.FileName);
                ftp.ftpUploadBytes(a.Year, ScreenType,
                                a.No,
                                filename,
                                AttachmentUpload.FileBytes,
                                ref uploadSucceded,
                                ref errorMessage);

                ListEditItem item = AttachmentCategoryDropDown.SelectedItem;
                if (item.Value == null)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                }
                AttachmentTable attachmentTable = FormData.AttachmentTable;
                FormAttachment attachment = attachmentTable.getBlankAttachment();
                attachment.ReferenceNumber = a.ReffNo;
                if (item != null)
                {
                    attachment.CategoryCode = item.Value.ToString();
                    attachment.CategoryName = item.Text;
                }
                attachment.PATH = a.Year + "/" + ScreenType + "/" + a.No;
                attachment.FileName = filename;
                attachment.Blank = false;
                formPersistence.saveAttachmentInfo(attachment, UserData);

                formPersistence.fetchAttachment(FormData, ProcessID);
                updateAttachmentList();
            }
            AttachmentPopup.Hide();
        }
        protected void evt_imgAddAttachment_onClick(object sender, EventArgs arg)
        {
            AttachmentPopup.Show();
        }

        protected void evt_gridAttachment_onHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs arg)
        {
            AttachmentGrid = (ASPxGridView)sender;

            if (arg.RowType != GridViewRowType.Data)
            {
                return;
            }

            decimal key = (decimal)arg.KeyValue;
            if (key > 0)
            {
                AttachmentTable attachmentTable = FormData.AttachmentTable;
                FormAttachment attachment = attachmentTable.get((int)key);
                if (attachment != null)
                {
                    KeyImageButton imgAdd = (KeyImageButton)AttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgAddAttachment");
                    if (imgAdd != null)
                    {
                        imgAdd.Visible = attachment.Blank;
                    }
                    KeyImageButton imgDelete = (KeyImageButton)AttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgDeleteAttachment");
                    if (imgDelete != null)
                    {
                        int statusCd = FormData.StatusCode ?? 0;
                        bool isPV = ScreenType == SCREEN_TYPE_PV;
                        bool canDelAttach = statusCd == 0
                                          || statusCd == 31
                                          || (
                                              (statusCd > 0 && FormData.InWorklist) &&
                                              !(
                                                    ((statusCd > 20 && statusCd < 30) && isPV)
                                                 || ((statusCd > 60 && statusCd < 70) && !isPV)
                                              )
                                              );

                        if (attachment.Blank || !canDelAttach)
                            imgDelete.CssClass = "hidden";
                        else
                            imgDelete.CssClass = "poited";
                    }
                }
            }
        }
        protected void evt_imgDeleteAttachment_onClick(object sender, EventArgs arg)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            int keyNum = int.Parse(key);
            logic.Say("evt_imgDeleteAttachment_onClick", "Key= {0}", keyNum);
            AttachmentTable attachmentTable = FormData.AttachmentTable;
            FormAttachment attachment = attachmentTable.get(keyNum);
            if ((attachment != null) && (!attachment.Blank))
            {
                AttachID a = GetAttachID();

                string folderPath = a.Year + "/" + ScreenType + "/" + a.No;
                ftp.Delete(folderPath, attachment.FileName);
                formPersistence.deleteAttachmentInfo(attachment);

                formPersistence.fetchAttachment(FormData, ProcessID);
                updateAttachmentList();
            }
        }
        protected void evt_cboxAttachmentCategory_onLoad(object sender, EventArgs args)
        {
            List<CodeConstant> a = formPersistence.getAttachmentCategories();
            AttachmentCategoryDropDown.DataSource = a;
            AttachmentCategoryDropDown.DataBind();
        }
        protected void evt_btCloseUploadAttachment_clicked(object sender, EventArgs arg)
        {
            AttachmentPopup.Hide();
        }
        #endregion Attachment

        #region Notes
        protected void evt_ddlSendTo_onLoad(object sender, EventArgs arg)
        {
            NoteRecipientDropDown = (ASPxComboBox)sender;

            string reffNo = String.Concat(FormData.PVNumber, FormData.PVYear);

            List<UserRoleHeader> u = logic.Notice.GetNoticeUser(FormData.PVNumber.str(), FormData.PVYear.str());

            NoteRecipientDropDown.DataSource = u;
            NoteRecipientDropDown.DataBind();
        }
        protected void evt_btSendNotice_onClick(object sender, EventArgs e)
        {
            bool hasWorlist = hasPVAsWorklist();
            clearScreenMessage();

            if (NoteRecipientDropDown != null)
            {
                String recipient = Convert.ToString(NoteRecipientDropDown.Value);
                string loggedUserRole = UserData.Roles.FirstOrDefault();
                if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
                    return;
                }
            }

            try
            {
                string strNotice = NoteTextBox.Text;
                if (!String.IsNullOrEmpty(strNotice))
                {
                    string lowerScreenType = ScreenType.ToLower();
                    String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
                    String noticeLink = String.Format("{0}://{1}:{2}{3}?{4}={5}&{6}={7}",
                        Request.Url.Scheme,
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        Request.ServerVariables["URL"],
                        lowerScreenType + "_no", FormData.PVNumber.str(),
                        lowerScreenType + "_year", FormData.PVYear.str());
                    string _Screen = ScreenType.Equals(SCREEN_TYPE_PV) ? "/80Accrued/PVApproval.aspx" : "/40RvFormList/RVApproval.aspx";
                    String noticeLinkApproval = String.Format("http://{0}:{1}{2}?{3}={4}&{5}={6}",
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        _Screen,
                        lowerScreenType + "no", FormData.PVNumber.str(),
                        lowerScreenType + "year", FormData.PVYear.str());

                    EmailFunction mail = new EmailFunction(emailTemplate);
                    ErrorData err = new ErrorData();
                    List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

                    String userRole = "";
                    String userRoleTo = "";
                    String noticeTo = "";
                    String noticer = "";

                    /* Modified by Adit */
                    if (listRole.Any())
                    {
                        userRole = listRole.FirstOrDefault().ROLE_NAME;
                    }

                    List<string> lstUser = null;
                    if (NoteRecipientDropDown.Value != null)
                    {
                        noticeTo = NoteRecipientDropDown.Value.ToString();

                        ErrorData _Err = new ErrorData(0, "", "");
                        List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
                        userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
                        lstUser = logic.Notice.listNoticeRecipient(FormData.PVNumber.str(), FormData.PVYear.str(), UserName, noticeTo);
                    } /* End modified */
                    else
                    {
                        noticeTo = UserName;
                        userRoleTo = "";

                        lstUser = logic.Notice.listNoticeRecipient(FormData.PVNumber.str(), FormData.PVYear.str(), UserName, noticeTo);
                    }

                    if (lstUser == null)
                    {
                        lstUser = new List<string>();
                    }
                    NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

                    logic.Notice.InsertNotice(FormData.PVNumber.str(),
                                            FormData.PVYear.str(),
                                            UserName,
                                            userRole,
                                            noticeTo,
                                            userRoleTo,
                                            lstUser,
                                            strNotice,
                                            true, hasWorlist);

                    formPersistence.fetchNote(FormData);
                    List<FormNote> noticeList = FormData.Notes;
                    NoteRepeater.DataSource = noticeList;
                    NoteRepeater.DataBind();

                    if (noticeList.Count > 0)
                    {
                        var q = noticeList.Where(a => a.Date != null);
                        if (q.Any())
                        {
                            noticer = q.Last().SenderName;
                        }
                        else
                        {
                            noticer = UserName;
                        }
                    }
                    else
                    {
                        noticer = UserName;
                    }

                    int cntSentMail = 0;
                    if (NoteRecipientDropDown.Value != null)
                    {
                        string n = ("ELVIS_USER_ADMIN ELVIS_FD_STAFF ELVIS_PV_MAKER_ONLY".IndexOf(userRoleTo, StringComparison.OrdinalIgnoreCase) >= 0) ?
                            noticeLink : noticeLinkApproval;

                        if (mail.ComposeApprovalNotice_EMAIL(FormData.PVNumber.str(),
                                                             FormData.PVYear.str(),
                                                             strNotice, n, noticeTo,
                                                             UserData, noticer,
                                                             FormData.DivisionID,
                                                             ScreenType, "Notice", ref err))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
                            cntSentMail++;
                        }

                        if (cntSentMail > 0)
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF", err.ErrMsg);
                        }

                        if (err.ErrID == 1)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
                        }
                        else if (err.ErrID == 2)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
            NoteTextBox.Text = "";


            postScreenMessage();
        }

        private void PostMsg(bool instant, bool clearfirst, byte sts, string id, params object[] x)
        {
            string m = "";
            StringBuilder b = new StringBuilder("");
            if (x != null && x.Length > 0)
            {
                for (int k = 0; k < x.Length; k++)
                    b.Append(x[k].str() + ", ");
            }

            if (!id.isEmpty())
            {
                m = _m.Message(id);
            }
            if (x != null && x.Length > 0)
            {
                if (!m.isEmpty())
                    m = string.Format(m, x);
                else
                {
                    b.Clear();
                    for (int i = 0; i < x.Length; i++)
                    {
                        b.Append(x[i].str());
                        b.Append(" ");
                    }
                    m = b.ToString().Trim();
                }
            }
            postScreenMessage(
                new ScreenMessage[]
                {
                    new ScreenMessage(sts, m)
                }
                , clearfirst
                , instant);
        }

        private void PostLaterOn(byte sts, string id, params object[] x)
        {
            PostMsg(false, true, sts, id, x);
        }

        private void PostNowPush(byte sts, string id, params object[] x)
        {
            PostMsg(true, false, sts, id, x);
        }

        private void PostNow(byte sts, string id, params object[] x)
        {
            PostMsg(true, true, sts, id, x);
        }

        private void PostLater(byte ScreenMessageStatus, string id, params object[] x)
        {
            PostMsg(false, false, ScreenMessageStatus, id, x);
        }

        protected void evt_rptrNotice_onItemDataBound(object sender, RepeaterItemEventArgs arg)
        {
            RepeaterItem item = arg.Item;
            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                Literal litOpenDivNotice = (Literal)item.FindControl("litOpenDivNotice");
                Literal litRptrNoticeComment = (Literal)item.FindControl("litRptrNoticeComment");
                Literal litCloseDivNotice = (Literal)item.FindControl("litCloseDivNotice");
                String dateHeader;

                FormNote note = (FormNote)item.DataItem;

                if (note.Date.HasValue)
                {
                    litOpenDivNotice.Text = "<div class='noticeLeft'>";
                    dateHeader = ((DateTime)note.Date.Value).ToString("d MMMM yyyy hh:mm:ss");
                }
                else
                {
                    litOpenDivNotice.Text = "<div class='noticeRight'>";
                    dateHeader = ((DateTime)note.ReplyDate.Value).ToString("d MMMM yyyy hh:mm:ss");
                }

                litRptrNoticeComment.Text = String.Format("<span class='noteSender'>{0}</span> {1}<br>To: <span class='noteSender'>{2}</span><br>{3}",
                    note.SenderName, dateHeader, note.ReceiverName, note.Message);


                litCloseDivNotice.Text = "</div>";
            }
        }
        #endregion Notes

        #region Upload
        protected void btnUploadTemplate_Click(object sender, EventArgs e)
        {
            logic.Say("btUploadTemplate_Click", "Upload : {0}", DataUpload.FileName);
            clearScreenMessage();
            try
            {
                object transactionType = TransactionTypeLookup.Value;
                if (transactionType == null || transactionType.Equals(""))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Transaction Type");
                    return;
                }
                if (!DataUpload.HasFile)
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00034ERR");
                    return;
                }

                int _processId = 0;
                CommonExcelUpload _CommonExcelUpload = new CommonExcelUpload();
                string _strFileType = Path.GetExtension(DataUpload.FileName).ToString().ToLower();
                string _strFileName = DataUpload.FileName;

                if (!_CommonExcelUpload.IsExcelFormat(_strFileType))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                    return;
                }

                int tt = 0;
                if ((FormData.TransactionCode ?? 0) != 0)
                    tt = FormData.TransactionCode ?? 0;



                int TemplateCd = (FormData._TransactionType != null) ? FormData._TransactionType.TemplateCd : 0;

                string _strMetaData = Server.MapPath(
                            string.Format(
                                logic.Sys.GetText("UPLOAD_META_FMT", "0"), logic.Sys.GetText("TEMPLATE_CD", TemplateCd.str())
                            )
                        );

                string _strFunctionId = "";
                _strFunctionId = resx.FunctionId("FCN_" + ScreenType + "_FORM_UPLOAD");
                _processId = DoStartLog(_strFunctionId, "");

                string _strTempDir = LoggingLogic.GetTempPath("", _processId);

                string xlsFile = Util.GetTmp(_strTempDir, Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(_strFileName)) + "_" + _processId.str(), _strFileType);
                LoggingLogic.ForceDirectories(Path.GetDirectoryName(xlsFile));
                DataUpload.SaveAs(xlsFile);

                List<PVFormDetail> dd = new List<PVFormDetail>();
                string[][] listOfErrors = new string[1][];

                bool Ok = formPersistence.Upload(xlsFile, _strMetaData, dd,
                                ref listOfErrors, tt, FormData, UserData, _processId);

                if (!Ok)
                {
                    string sourceFile = Server.MapPath(
                                            String.Format(
                                                    logic.Sys.GetText("TEMPLATE.FMT", "ERR"), logic.Sys.GetText("TEMPLATE_CD", TemplateCd.str())
                                                )
                                            );
                    string targetFile = Server.MapPath(Common.AppSetting.ErrorUploadFilePath);

                    string fileName = formPersistence.DownloadError(_processId, _strFileName, sourceFile, targetFile, listOfErrors);

                    String linkErrorFile = String.Format("<a href='{0}://{1}:{2}{3}{4}' target='_blank'>{5}</a>",
                                                            Request.Url.Scheme,
                                                            Request.ServerVariables["SERVER_NAME"],
                                                            Request.ServerVariables["SERVER_PORT"],
                                                            Common.AppSetting.ErrorUploadFilePath.Replace("~", ""),
                                                            fileName,
                                                            fileName);

                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                    return;
                }



                string headerWbsNumber = null;
                foreach (PVFormDetail detail in dd)
                {
                    if (headerWbsNumber == null)
                    {
                        headerWbsNumber = detail.WbsNumber;
                    }
                    else
                    {
                        if (!headerWbsNumber.Equals(detail.WbsNumber))
                        {
                            FormData.BudgetNumber = detail.WbsNumber;
                            BookingNoLookup.Value = detail.WbsNumber;
                            BudgetNumberLabel.Text = detail.WbsNumber;
                            break;
                        }
                        headerWbsNumber = detail.WbsNumber;
                    }
                }

                DetailGrid.UpdateEdit();

                Tabel.DataList.Clear();
                Tabel.DataList = dd;

                Tabel.DataList = Tabel.sanityCheck();
                Tabel.resetDisplaySequenceNumber();
                ReloadGrid();
                StartEditingTabel();
                PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");

            }
            catch (Exception ex)
            {
                PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                LoggingLogic.err(ex);
            }
        }
        #endregion Upload

        #region Editing Control
        protected void btClose_Click(object sender, EventArgs args)
        {
            AttachID a = GetAttachID();
            if (a.No.StartsWith("T"))
            {
                string folderPath = a.Year + "/" + ScreenType + "/" + a.No;
                foreach (FormAttachment att in FormData.AttachmentTable.Attachments)
                {
                    if (!att.Blank)
                    {
                        ftp.Delete(folderPath, att.FileName);
                    }
                    formPersistence.deleteAttachmentInfo(att);
                }

                try
                {
                    ftp.RemoveDir(folderPath);
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Contains("550"))
                    {
                        LoggingLogic.err(ex);
                        throw;
                    }
                }
            }


        }
        protected void evt_btReverse_onClick(object sender, EventArgs e)
        {
            logic.Say("evt_btReverse_onClick", "Reverse {0}{1}", FormData.PVNumber, FormData.PVYear);
            if (formPersistence.Reverse(UserName, FormData))
            {
                string msg = "";
                if (formPersistence.BudgetCheckReverse(UserName, FormData, ref msg))
                {
                    PostNow(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Reverse");
                }
                else
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Reverse", msg);
                }
            }
            else
            {
                PostNow(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Reverse", " failed invoke");
            }
        }

        protected void PostToSapButton_Click()
        {
            string _loc = "PostToSapButton_Click";

            if (FormData.OnHold)
            {
                PostNow(ScreenMessage.STATUS_ERROR, "MSTD00097ERR");

                return;
            }

            if ((FormData.COUNTER_FLAG ?? 0) != 1)
            {
                PostNow(ScreenMessage.STATUS_ERROR, "MSTD00039WRN", FormData.PVNumber, "Verified");

                return;
            }

            bool result = false;

            List<String> listData = new List<string>() {
                FormData.PVNumber.Value.ToString() + ":" + FormData.PVYear.Value.ToString()
            };

            DoStartLog(resx.FunctionId("FCN_" + ScreenType + "_POST_TO_SAP"), "");
            string _ErrMessage = String.Empty;

            result = lilo.PostToSap(listData, ProcessID, ref _ErrMessage, UserData, FormData.TransactionCode);

            byte messageStatus = ScreenMessage.STATUS_INFO;
            string messageCode;
            if (result)
            {
                //formPersistence.updateBalanceVoucherPosting(FormData, UserData);

                VoucherMasterPage.ApprovalType = Common.Enum.ApprovalEnum.Approve;
                btnApprovalProceed_Click(null, null);

                reloadFormData();
                loadForm();
                messageCode = "MSTD00067INF";
                PostLater(messageStatus, messageCode, FormData.PVNumber);

                ///Added by Akhmad Nuryanto, 18/09/2012, send email
                if (FormData != null)
                {
                    if (FormData.StatusCode == 27
                        && (FormData.PaymentMethodCode.Equals("C")
                            || FormData.PaymentMethodCode.Equals("E"))

                        )
                    {
                        /// 2013-09-11 dan + only amount more than 0 printed as PV Ticket 
                        if (Math.Round(FormData.GetTotalAmount()) > 0)
                            sendMailPVTicket(FormData.PVNumber.ToString(), FormData.PVYear.ToString());
                        else
                        {
                            // set print ticket flag to 1
                            logic.PVPrint.ticketPrinted(FormData.PVNumber ?? 0, FormData.PVYear ?? 0, UserName);
                        }
                    }
                }
                ///end of addition by Akhmad Nuryanto

                evaluateEditingControlVisibility(); /// 2012.11.12 wot.daniel + 
            }
            else if (lilo["NR"] != null || lilo["RF"] != null)
            {
                string NRList = lilo.CommaJoinList("NR");
                string RFList = lilo.CommaJoinList("RF");
                string msg = "";
                messageCode = "MSTD00068ERR";
                if (!NRList.isEmpty())
                    PostLater(ScreenMessage.STATUS_ERROR, messageCode, NRList + " not ready to be posted");
                if (!RFList.isEmpty())
                    PostLater(ScreenMessage.STATUS_ERROR, messageCode, RFList + " redirect failed");


                Log(messageCode, "ERR", _loc, msg);
                postScreenMessage();

                return;
            }
            else
            {
                messageCode = "MSTD00068ERR";
                messageStatus = ScreenMessage.STATUS_ERROR;
            }
            List<string> ng = lilo["NG"] as List<string>;
            List<string> nr = lilo["notready"] as List<string>;

            logic.PVList.RevertApproverInList(ng);
            logic.PVList.RevertApproverInList(nr);

            string statusCode = "INF";
            if (messageStatus == ScreenMessage.STATUS_ERROR)
            {
                statusCode = "ERR";
                PostNowPush(messageStatus, "MSTD00002ERR", _ErrMessage);
            }
            else
            {
                PostNowPush(messageStatus, "MSTD00001INF", _ErrMessage);
            }
            Log(messageCode, statusCode, _loc, _m.Message(messageCode, ScreenType));

            if (!result)
                PostNowPush(messageStatus, "MSTD00001INF", CommonFunction.CommaJoin(lilo.LastErrors, ";"));
        }

        protected void evt_btPostToSAP_onClick(object sender, EventArgs arg)
        {
            logic.Say("evt_btPostToSAP_onClick", "Post {0} {1} {2}", ScreenType, FormData.PVNumber, FormData.PVYear);
            if (FormData.VendorGroupCode == 4)
            {
                logic.Say("evt_btPostToSAP_onClick", "for One time vendor, invoke through One Time vendor submit");
            }
            else
            {
                PostToSapButton_Click();
            }
        }

        protected void getBudget()
        {
            string selectedBookingNo = BookingNoLookup.Value.str();
            //string tes = Convert.ToString(UserData.DIV_CD);
            // modifie by FID.Ridwan 10072018
            if (FormData.OpenExisting)
            {
                selectedBookingNo = FormData.BookingNo;
            }
            if (!string.IsNullOrEmpty(selectedBookingNo))
            {
                //string tes = Convert.ToString(UserData.DIV_CD);
                List<WBSStructure> wbs = formPersistence.WbsNumbers(selectedBookingNo);
                var desc = wbs[0].Description;
                var wbsno = wbs[0].WbsNumber;
                CodeConstant vendorGroup = formPersistence.getVendorGroupNameByVendor(selectedBookingNo);
                FormData.WbsDetailBudget = (selectedBookingNo != null) ? wbsno + '/' + desc + '/' + selectedBookingNo : "";
            }

            //List<WBSStructure> wbs = formPersistence.WbsNumbers(selectedBookingNo);
            //var desc = wbs[0].Description;
            //var wbsno = wbs[0].WbsNumber;
            //CodeConstant vendorGroup = formPersistence.getVendorGroupNameByVendor(selectedBookingNo);
            //FormData.WbsDetailBudget = (selectedBookingNo != null) ? wbsno + " / " + desc : "";
        }

        protected virtual string PrintCover()
        {
            return "";
        }

        protected void btSubmit_Click(object sender, EventArgs e)
        {
            logic.Say("btSubmit_Click", "Submit");
            clearScreenMessage();
            getBudget();
            UpdateTabel();

            bool openEmpty = FormData.OpenEmpty;
            bool Ok = false;

            do
            {
                try
                {
                    //FID.PRAS
                    if (!validateBookingNo())
                        break;

                    FormData.isSubmit = true;

                    //added by Akhmad Nuryanto
                    if ((FormData.StatusCode ?? 0) != 0)
                    {
                        FormData.isResubmitting = true;
                    }
                    //end by Akhmad Nuryanto

                    Ok = (FormData.FinanceAccessEnabled) ? ValidFinance() : ValidGeneral();
                    if (!Ok)
                    {
                        logic.Say("btSubmit_Click", "Validation failed");
                        ReloadGrid();

                        FormData.Submitted = false;
                        bool isSettlementData = isSettlement(FormData.PVTypeCode ?? 0);
                        FormData.Editable = !isSettlementData;
                        FormData.Editing = !isSettlementData;
                        break;
                    }
                    SaveFormData();

                    if (FormData.isResubmitting && UserData.isAuthorizedCreator)
                    {
                        logic.Say("btSubmit_Click", "ReSubmit # {0} {1} [{2}]", FormData.PVNumber, FormData.PVYear, FormData.BudgetNumber);
                        Ok = CheckBudgetDelete();
                    }

                    if (!UserData.isAuthorizedCreator)
                    {
                        FormData.BudgetChecked = true; /// skip budget check for non author
                    }

                    //if (FormData.FormIsValid && FormData.Saved
                    //    && FormData.Budgeted && !FormData.BudgetChecked)
                    //{
                    //    Ok = CheckBudget();
                    //}
                    //else
                    //{
                    //    Ok = FormData.Saved;
                    //}
                    //UpdateTabel();


                    decimal totAmount = FormData.TotalAmount;
                    var amtBal = logic.AccrBalance.GetBalanceByBookingNo(BudgetNumberLabel.Text);
                    decimal remaining = (amtBal.AVAILABLE_AMT ?? 0) - (amtBal.OUTSTANDING_AMT ?? 0);

                    if (totAmount > remaining)
                    {
                        Ok = false;
                        FormData.BudgetChecked = false;
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Submit", "Accrued amount exceeds Remaining budget");
                    }

                    else
                    {
                        Ok = true;
                        FormData.BudgetChecked = true;
                    }


                    if (FormData.FormIsValid && FormData.isSubmit /* isSubmitting */)
                    {
                        if (FormData.FinanceAccessEnabled)
                        {
                            VoucherMasterPage.ApprovalType = Common.Enum.ApprovalEnum.Approve;
                            btnApprovalProceed_Click(null, null);
                            FormData.Submitted = FormData.Saved;
                        }
                        else
                        {
                            bool goK2 = (ScreenType == SCREEN_TYPE_PV
                                        && (!FormData.Budgeted ||
                                            (FormData.Budgeted &&
                                            FormData.BudgetChecked)))
                                    || (ScreenType == SCREEN_TYPE_RV && Ok);
                            if (goK2)
                            {
                                logic.Say("btSubmit_Click", "postToK2 K2 {0} {1}", FormData.PVNumber, FormData.PVYear);
                                FormData.K2Command = resx.K2ProcID("Form_Registration_Submit_New");
                                FormData.Submitted = formPersistence.
                                    postToK2(FormData, UserData, ScreenType.Equals(SCREEN_TYPE_PV));
                                resetWorklist();
                            }
                        }

                        FormData.Editable = !FormData.Submitted;
                        FormData.Editing = !FormData.Submitted;


                    }
                    FormData.isSubmit = false;

                }
                catch (Exception ex)
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                    LoggingLogic.err(ex);
                }

                if (FormData.Submitted)
                {
                    // fid.Hadid-Ridwan 20180711. Update balance amount on Submit process
                    formPersistence.updateBalanceOutstanding(FormData, UserData, true); 

                    FormData.Editable = false;
                    // Start Rinda Rahayu 20160401
                    if (!FormData.FinanceAccessEnabled)
                    {
                        string cover = PrintCover();
                        logic.Say("btSubmit_Click",
                            "Print Cover {0} {1}{2} : {3}", ScreenType,
                            FormData.PVNumber.str(), FormData.PVYear.str()
                            , cover);

                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00001INF",
                            _m.Message("MSTD00058INF") + "." +
                            "Download <a href=\"" + cover + "\">"
                            + ScreenType + " Cover</a>");
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00058INF");
                    }
                    // End Rinda Rahayu 20160401
                }
                else
                {
                    FormData.OpenEmpty = openEmpty;
                    if (FormData.isSubmit /* isSubmitting */)
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Submision");
                    }
                }

                int lastStatus = FormData.StatusCode ?? 0;

                reloadFormData();
                if (!Ok && UserData.isAuthorizedCreator) /// when error happens while creating new and submitting 
                {
                    formPersistence.LoadOri(FormData);
                    ReloadGrid();
                }

                if (FormData.Submitted)
                {
                    // done as the last resort --- bohwat 
                    if (FormData.FinanceAccessEnabled)
                    {
                        FormData.StatusCode = 22;
                    }
                    else
                    {
                        FormData.StatusCode = 10;
                    }

                }

                logic.Say("btSubmit_Click", "Submit=> StatusCode= {0}", FormData.StatusCode);
            } while (false);
            evaluateHeaderVisibility();
            evaluateEditingControlVisibility();
            postScreenMessage();
            refreshLabels();
            FormData.Submitted = false;
            ReloadOpener();
        }

        private List<CodeConstant> _currs = null;
        protected List<CodeConstant> Currs
        {
            get
            {
                if (_currs == null)
                    _currs = formPersistence.getCurrencyCodeList();

                return _currs;

            }
        }
        protected void coCurrencyCode_Load(object sender, EventArgs e)
        {
            PVFormData f = FormData;
            ASPxComboBox co = (ASPxComboBox)sender;

            if (!f.PaymentMethodCode.isEmpty() &&
                    (f.PaymentMethodCode.Equals(PVFormData.PAYMENT_METHOD_CASH)
                  || f.PaymentMethodCode.Equals(PVFormData.PAYMENT_METHOD_CHEQUE))
               )
            {
                List<CodeConstant> lstAllowedCurrency = new List<CodeConstant>();

                lstAllowedCurrency = Currs.Where(
                    x => Common.AppSetting.CASH_CURRENCIES
                        .Contains(x.Code.ToUpper()))
                    .ToList();

                co.DataSource = lstAllowedCurrency;
            }
            else
                co.DataSource = Currs;

            co.DataBind();

            if (DetailGrid.IsEditing)
            {
                int idxSelected = DetailGrid.EditingRowVisibleIndex;

                int cntData = Tabel.DataList.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    PVFormDetail detail = Tabel.DataList[idxSelected];
                    co.Value = detail.CurrencyCode;
                }
            }

        }
        protected void StartEditingTabel()
        {
            GridViewColumn column = DetailGrid.Columns[VouCol.DELETION_CONTROL];
            column.Visible = true;

            ReloadGrid();

            DetailGrid.CancelEdit();
            int cntData = Tabel.DataList.Count;
            if (cntData > 0)
            {
                Tabel.EditIndex = cntData - 1;
                DetailGrid.StartEdit(Tabel.EditIndex);
            }
            else
            {
                initFirstRow();
            }
        }

        public void CancelEditingTabel()
        {
            DetailGrid.CancelEdit();

            Tabel.CancelNonPersistedData();
            Tabel.DataList = Tabel.sanityCheck(FormData.OpenExisting);
        }


        public void StartBrowsingTabel()
        {
            DetailGrid.CancelEdit();
            ASPxGridViewPagerSettings pager = DetailGrid.SettingsPager;
            pager.PageSize = MAX_PAGE_ROW;
            pager.Mode = GridViewPagerMode.ShowPager;

            GridViewColumn column = DetailGrid.Columns[VouCol.DELETION_CONTROL];
            column.Visible = false;
            ReloadGrid();
        }


        protected void StartEditing()
        {
            clearScreenMessage();
            FormData.Editing = true;
            evaluatePageComponentAvailibility();
            if (!FormData.FinanceAccessEnabled && UserData.isAuthorizedCreator)
            {
                if (!(FormData.StatusCode == 0))
                    formPersistence.LoadOri(FormData);
                ReloadGrid();
                StartEditingTabel();
            }
            loadForm();
        }

        protected void btEdit_Click(object sender, EventArgs e)
        {
            logic.Say("btEdit_Click", "Edit");

            StartEditing();
        }

        protected bool ValidFinance()
        {
            return validateFinanceHeader();
        }

        protected bool saveFinance(PVFormData f, bool isSubmit = false)
        {
            f.FormIsValid = true;
            f.UserName = UserName;

            object pm = PaymentMethodDropDown.Value;
            if (pm != null)
                f.PaymentMethodCode = pm.str();

            object objValue = PostingDate.Value;
            if (objValue != null)
            {
                f.PostingDate = (DateTime)PostingDate.Value;
            }
            else
            {
                f.PostingDate = null;
            }

            objValue = PlanningPaymentDate.Value;
            if (objValue != null)
            {
                f.PlanningPaymentDate = (DateTime)PlanningPaymentDate.Value;
            }
            else
            {
                f.PlanningPaymentDate = null;
            }

            string value = BankTypeLookup.Value.str() ?? "".Trim();
            if (!value.isEmpty())
            {
                f.BankType = value.Int();
            }
            else
            {
                f.BankType = null;
            }

            AttachID a = GetAttachID();

            f.Saved = formPersistence.save(f, UserData, true);


            ReArrangeAttachments(a.No, (f.PVNumber ?? 0).str(), a.Year);

            logic.Say("saveFinance", "{0}# {1}{2} {3}", ScreenType, f.PVNumber, f.PVYear, f.Saved.box("Saved"));
            return f.Saved;
        }

        protected bool ValidGeneral()
        {
            return performNonSubmissionValidation() && validateGeneralData();
        }

        protected bool saveGeneral(PVFormData f)
        {
            if (f.FormIsValid)
            {
                f.PVDate = DateTime.Now;
                f.StatusCode = formPersistence.getDocumentStatusCode(StatusLabel.Text.Trim());
                f.UserName = UserName;

                ListEditItem listItem = DocumentTypeDropDown.SelectedItem;
                if (listItem != null)
                {
                    if (Mode != VIEW_MODE && DocumentTypeDropDown.Visible)
                        f.PVTypeCode = int.Parse(listItem.Value.ToString());
                }
                else
                {
                    int? pvType = f.PVTypeCode;
                    if (pvType.HasValue
                        && ((pvType.Value != 3 && ScreenType.Equals(SCREEN_TYPE_PV))
                            || (pvType.Value != 2 && ScreenType.Equals(SCREEN_TYPE_RV))
                           )
                        )
                    {
                        f.PVTypeCode = null;
                    }
                }

                if (Mode != VIEW_MODE && TransactionTypeLookup.Visible)
                {
                    if (TransactionTypeLookup.Value != null)
                    {
                        f.TransactionCode = Convert.ToInt32(TransactionTypeLookup.Value);
                    }
                    else
                    {
                        f.TransactionCode = null;
                    }
                }
                f.DivisionID = Convert.ToString(UserData.DIV_CD);
                if (f.VendorGroupCode != PVFormData.EXTERNAL_VENDOR_GROUP_CODE)
                {
                    if (Mode != VIEW_MODE && VendorCodeLookup.Visible)
                        f.VendorCode = (string)VendorCodeLookup.Value;
                }

                listItem = PaymentMethodDropDown.SelectedItem;
                /*if (Mode != VIEW_MODE && PaymentMethodDropDown.Visible)
                    if (listItem != null)
                    {
                        f.PaymentMethodCode = listItem.Value.ToString();
                    }
                    else
                    {
                        f.PaymentMethodCode = null;
                    }*/

                // Start Rinda Rahayu 20160516
                if (Mode != VIEW_MODE && PaymentMethodDropDown.Visible)
                {
                    if (listItem != null)
                    {
                        f.PaymentMethodCode = listItem.Value.ToString();
                    }
                    else
                    {
                        f.PaymentMethodCode = null;
                    }
                }
                else if (Mode == VIEW_MODE)
                {
                    if (listItem != null)
                    {
                        f.PaymentMethodCode = listItem.Value.ToString();
                    }
                    else
                    {
                        f.PaymentMethodCode = null;
                    }
                }

                // End Rinda Rahayu 20160516

                object objValue = ActivityDateStart.Value;
                if (objValue != null)
                {
                    f.ActivityDate = (DateTime)objValue;
                }
                else
                {
                    f.ActivityDate = null;
                }

                objValue = ActivityDateEnd.Value;
                if (objValue != null)
                {
                    f.ActivityDateTo = (DateTime)objValue;
                }
                else
                {
                    f.ActivityDateTo = null;
                }

                initFormTable();

                AttachID a = GetAttachID();
                FormData.EmptyCostCenterWithBannedGLAccount(NonCostCenterGlAccounts);

                f.Saved = formPersistence.save(f, UserData, false);


                ReArrangeAttachments(a.No, (f.PVNumber ?? 0).str(), a.Year);
            }
            if (f.Saved)
                f.Saved = (f.PVNumber ?? 0) != 0;
            logic.Say("saveGeneral", "{0}# {1}{2} {3}", ScreenType, f.PVNumber, f.PVYear, f.Saved.box("Saved"));

            return f.Saved;

        }

        protected void SaveFormData()
        {
            logic.Say("SaveFormData", "Count={0} {1} {2}{3}", Tabel.DataList.Count, ScreenType, FormData.PVNumber, FormData.PVYear);
            clearScreenMessage();
            bool saved = false;

            try
            {
                FormData.Submitted = false;
                if (FormData.FinanceAccessEnabled)
                {
                    saved = saveFinance(FormData);
                }
                else
                {
                    int statusCd = FormData.StatusCode ?? 0;

                    if (((statusCd > 30 && statusCd <= 46 && ScreenType == SCREEN_TYPE_PV)
                       || ((statusCd >= 71) && (statusCd <= 85) && ScreenType == SCREEN_TYPE_RV)
                       || (statusCd == 0)
                       )
                       && ((FormData.PVTypeCode != 3 && ScreenType == SCREEN_TYPE_PV) ||
                          (FormData.PVTypeCode != 2 && ScreenType == SCREEN_TYPE_RV)))
                        Tabel.ReSequence(); /// prevent unable to edit when user added second line then delete first line then add line

                    saved = saveGeneral(FormData);
                }
                saved = saved && (!FormData.isSubmit || FormData.FormIsValid);

            }
            catch (Exception ex)
            {
                PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
        }

        //START FID.PRAS 24-07-2018
        private bool validateBookingNo()
        {
            bool valid = true;
            var listBn = BookingNoLookup.Value.str();
            var Bn = FormData.BookingNo;
            if (listBn == null)
            {
                listBn = Bn;
            }
            var listBal = logic.AccrBalance.GetBalanceByBookingNo(listBn);
            var closedBal = listBal.BOOKING_STS == 1;
            if (closedBal == true)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR"
                    , "These Booking No is Closed : " + listBn);
                valid = false;
            }

            // check expired
            var expBal = (listBal.EXPIRED_DT ?? DateTime.Now) < DateTime.Now;
            if (expBal == true)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR"
                    , "These Booking No is Expired : " + listBn);
                valid = false;
            }

            return valid;
        }
        //END OF FID.PRAS

        protected void btSave_Click(object sender, EventArgs e)
        {
            logic.Say("btSave_Click", "Save");
            clearScreenMessage();
            getBudget();
            List<PVFormDetail> lstDetail = Tabel.getBlankRowCleanedDetails();

            //FID.PRAS
            bool valid = validateBookingNo();

            // Start Rinda Rahayu 20160510

            if (lstDetail.Count > 0 && valid == true)
            {
                if (FormData._TransactionType != null && FormData._TransactionType.CostCenterFlag == true &&
                            (lstDetail[0].CostCenterCode == null || lstDetail[0].CostCenterCode.Equals("")))
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Cost Center");
                }
                else
                {

                    UpdateTabel();

                    var det = FormData.Details;
                    decimal totAmount = det.Sum(x => x.Amount * logic.Look.getExchangeRates()
                                                                                .Where(y => y.CurrencyCode == x.CurrencyCode)
                                                                                .Select(z => z.Rate)
                                                                                .FirstOrDefault());

                    var amtBal = logic.AccrBalance.GetBalanceByBookingNo(BudgetNumberLabel.Text);
                    decimal remaining = (amtBal.AVAILABLE_AMT ?? 0) - (amtBal.OUTSTANDING_AMT ?? 0);

                    if (totAmount > remaining)
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00057ERR", "Save", "Accrued amount exceeds Remaining budget");
                    }
                    else
                    {

                        SaveFormData();
                        if (FormData.Saved)
                        {
                            CancelEdit();

                            PostLaterOn(ScreenMessage.STATUS_INFO, "MSTD00055INF");

                            DownloadEntertainmentTemplateLink.Visible = false;

                        }
                        else
                        {
                            PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Save");
                        }

                        // Start Rinda Rahayu 20160401
                        //if (FormData.Saved && !FormData.FinanceAccessEnabled)
                        // fid.Hadid 20181022. Only print cover if pv saved successfully
                        if (FormData.Saved && !FormData.FinanceAccessEnabled)
                        {
                            string cover = PrintCover();
                            logic.Say("btSubmit_Click",
                                "Print Cover {0} {1}{2} : {3}", ScreenType,
                                FormData.PVNumber.str(), FormData.PVYear.str()
                                , cover);

                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00001INF",
                                _m.Message("MSTD00058INF") + "." +
                                "Download <a href=\"" + cover + "\">"
                                + ScreenType + " Cover</a>");
                        }
                        else
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00058INF");
                        }
                        // End Rinda Rahayu 20160401
                    }

                    reloadFormData();
                    FormData.Submitted = false;
                    evaluateEditingControlVisibility();
                    postScreenMessage();
                }

            }
            // End Rinda Rahayu 20160510 

        }

        protected void ReArrangeAttachments(string pre, string post, string yy)
        {
            string olde = pre + yy;
            string neo = post + yy;
            if (!pre.StartsWith("T")) return;
            List<FormAttachment> atts = formPersistence.getAttachments(olde);
            string opath = "";
            foreach (FormAttachment a in atts)
            {
                string oldFile = a.PATH + "/" + a.FileName;
                opath = a.PATH;
                string newFile = "";
                string[] p = a.PATH.Split('/');

                if (p.Length > 0)
                {
                    p[p.Length - 1] = post;
                    newFile = string.Join("/", p, 0, p.Length) + "/" + a.FileName;
                }
                ftp.Move(oldFile, newFile);
                formPersistence.MoveAttachment(pre, a.SequenceNumber, post, yy);
            }
            ftp.RemoveDir(opath);
        }

        protected bool CheckBudgetDelete()
        {
            string me = "CheckBudgetDelete()";
            bool Ok = false;
            int no = FormData.PVNumber ?? 0;
            int yy = logic.Sys.FiscalYear(FormData.PVDate ?? new DateTime(StrTo.NULL_YEAR, 1, 1));
            if (no < 1 || yy < 1) return false;
            List<BudgetCheckInput> prevBudgets = formPersistence.GetBudgetNumberHistory(no, yy, FormData.BudgetNumber);
            if (prevBudgets == null)
                prevBudgets = new List<BudgetCheckInput>();
            if (!FormData.BudgetNumber.isEmpty())
                prevBudgets.Add(new BudgetCheckInput()
                {
                    DOC_NO = no,
                    DOC_YEAR = yy,
                    AMOUNT = FormData.GetTotalAmount(),
                    WBS_NO = FormData.BudgetNumber
                });
            if (prevBudgets != null && prevBudgets.Count > 0)
            {
                try
                {
                    if (prevBudgets.Count > 1)
                        log.Log("INF", "Delete all previous Budget " + Ok, me, UserName);

                    for (int ix = 0; ix < prevBudgets.Count; ix++)
                    {
                        BudgetCheckInput bu = prevBudgets[ix];
                        bu.DOC_STATUS = (int)BudgetCheckStatus.Delete;
                        BudgetCheckResult o = logic.SAP.BudgetCheck(bu, UserName);

                        if (o.STATUS.Equals("E"))
                        {
                            log.Log("MSTD00002ERR", o.MESSAGE, me, UserName);
                        }
                        else if (o.STATUS.Equals("S") && ix == prevBudgets.Count - 1)
                        {
                            Ok = true;
                        }

                    }
                }
                catch (Exception ex)
                {
                    log.Log("MSTD00002ERR", LoggingLogic.Trace(ex), me, UserName);
                }
            }

            if (FormData.BudgetNumber.isEmpty()) return true;

            log.Log("INF", "CheckBudgetDelete = " + Ok, "CheckBudgetDelete()", UserName);
            return Ok;
        }

        protected bool CheckBudget()
        {
            bool Ok = false;

            if (ScreenType.Equals(SCREEN_TYPE_RV)
                || !FormData.isSubmit
                || !FormData.Budgeted || FormData.BudgetNumber.isEmpty()
                || Common.AppSetting.SKIP_BUDGET_CHECK || !Common.AppSetting.BUDGET_CHECK_BOOK
                || FormData.StatusCode == 20
                || FormData.FormTable.DataList.Count < 1
                )
            {
                return true;
            }
            try
            {
                int fiscalYear = logic.Sys.FiscalYear(FormData.PVDate ?? new DateTime(StrTo.NULL_YEAR, 1, 1));
                //  decimal amt = FormData.GetTotalAmount();
                decimal amt = FormData.GetTotalAmountToBudget(); // Rinda Rahayu 20160406

                BudgetCheckResult o;
                if (amt > 0)
                    o = logic.SAP.BudgetCheck(new BudgetCheckInput()
                    {
                        DOC_NO = FormData.PVNumber ?? 0,
                        DOC_YEAR = fiscalYear,
                        WBS_NO = FormData.BudgetNumber,
                        AMOUNT = amt,
                        DOC_STATUS = (int)BudgetCheckStatus.Book
                    }, UserName);
                else
                {
                    o = new BudgetCheckResult()
                    {
                        STATUS = "S",
                        DOC_NO = FormData.PVNumber ?? 0,
                        DOC_YEAR = fiscalYear,
                        MESSAGE = ""
                    };
                }
                if (o.STATUS.Equals("E"))
                {
                    string m = o.MESSAGE;

                    string rex = "";
                    if (m.Contains("NOT AVAILABLE"))
                    {
                        string rem = logic.Look.RemainingBudget(FormData.PVNumber.str(), fiscalYear.str(), FormData.BudgetNumber, ref rex);
                        if (rex.isEmpty())
                            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", m + " Remaining Budget: " + rem);
                        else
                            PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", m + " Get Remaining Budget Result: " + rex);
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", m);
                    }

                    return false;
                }
                else if (o.STATUS.Equals("S"))
                {
                    Ok = true;
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, ex.Message);
                LoggingLogic.err(ex);
            }
            log.Log("INF", "CheckBudget = " + Ok, "CheckBudget()", UserName);
            FormData.BudgetChecked = Ok;
            return Ok;
        }

        protected void CancelEdit()
        {
            loadForm();
            FormData.OpenExisting = true;
            FormData.Editing = false;
            CancelEditingTabel();
            evaluatePageComponentAvailibility();
        }

        public void UpdateTabel()
        {
            if (DetailGrid.IsEditing)
            {
                DetailGrid.UpdateEdit();
            }
        }

        protected void btCancel_Click(object sender, EventArgs e)
        {
            logic.Say("btCancel_Click", "Cancel");
            if (!FormData.FinanceAccessEnabled && UserData.isAuthorizedCreator)
            {
                reloadFormData();
            }
            CancelEdit();
        }
        protected void evt_btReject_onClick(object sender, EventArgs arg)
        {
            logic.Say("btReject_onClick", "Reject {0} {1}", FormData.PVNumber, FormData.PVYear);
            VoucherMasterPage.btnReject_Click(sender, arg, FormData.PVNumber.ToString(), FormData.PVYear.ToString());
            FormData = formPersistence.search(FormData.PVNumber.Value, FormData.PVYear.Value);
            FormData.InWorklist = hasPVAsWorklist();
            initFormTable();
            loadForm();
            evaluatePageComponentAvailibility();
            CheckBudgetDelete();
        }
        protected void evt_btHold_onClick(object sender, EventArgs arg)
        {
            logic.Say("btHold_onClick", "Hold {0} {1}", FormData.PVNumber, FormData.PVYear);
            VoucherMasterPage.ApprovalType = Common.Enum.ApprovalEnum.Hold;
            VoucherMasterPage.btnHold_Click(sender, arg, FormData.PVNumber.ToString(), FormData.PVYear.ToString());
        }
        protected void evt_btUnHold_onClick(object sender, EventArgs arg)
        {
            logic.Say("btUnHold_Click", "UnHold {0} {1}", FormData.PVNumber, FormData.PVYear);
            VoucherMasterPage.ApprovalType = Common.Enum.ApprovalEnum.UnHold;
            VoucherMasterPage.btnHold_Click(sender, arg, FormData.PVNumber.ToString(), FormData.PVYear.ToString());
        }
        protected void hidVendorGroup_Load(object sender, EventArgs e)
        {
            HiddenField x = sender as HiddenField;
            x.Value = (FormData.VendorGroupCode ?? 0).str();
        }

        protected void RemoveFromWorklist()
        {
            int j = -1;
            /// remove worklist manually because reload is not working

            for (int i = 0; i < Worklist.Count; i++)
            {
                if (Worklist[i].Folio.Equals(FormData.PVNumber.str() + FormData.PVYear.str()))
                {
                    j = i;
                    break;
                }
            }
            if (j > -1 && j < Worklist.Count)
            {
                logic.Say("RemoveFromWorklist", "{0} # {1} {2} @index {3}", ScreenType, FormData.PVNumber, FormData.PVYear, j);
                Worklist.RemoveAt(j);
            }
        }
        protected void btnApprovalProceed_Click(object sender, EventArgs e)
        {
            logic.Say("btApprovalProceed_Click", "Approval {0} {1}", FormData.PVNumber, FormData.PVYear);
            string _Comment = "";
            TextBox txtApprovalComment = Master.FindControl("txtApprovalComment") as TextBox;

            ModalPopupExtender popUpApproval = Master.FindControl("popUpApproval") as ModalPopupExtender;
            bool success = false;

            string strPVNumber = FormData.PVNumber.ToString();
            string strPVYear = FormData.PVYear.ToString();
            DropDownList d = Master.FindControl("ddlHoldReason") as DropDownList;
            string holdComment = "";
            if (d != null && d.Visible && (d.SelectedIndex > 0))
            {
                holdComment = HoldReasons.Get(d.SelectedValue, d.Text);
            }

            if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.UnHold)
                holdComment = txtApprovalComment.Text;

            try
            {
                if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Approve
                || (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Reject
                    && txtApprovalComment.Text != ""))
                {
                    #region Get --> Comment
                    if (string.IsNullOrEmpty(txtApprovalComment.Text))
                    {
                        _Comment = "N/A";
                    }
                    else
                    {
                        _Comment = txtApprovalComment.Text;
                    }
                    #endregion

                    #region Get --> Command
                    string _Command = "";
                    if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        _Command = resx.K2ProcID("Form_Registration_Approval");
                    else if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        _Command = resx.K2ProcID("Form_Registration_Rejected");
                    #endregion

                    #region Do --> Approve-Reject
                    bool Canceling = isCancel && (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Reject);
                    if (Canceling)
                    {
                        logic.PVApproval.setCancel(FormData.PVNumber ?? 0, FormData.PVYear ?? 0, 1);
                    }
                    if (logic.PVRVWorkflow.Go(strPVNumber + strPVYear, _Command, UserData, _Comment))
                    {
                        success = true;
                        if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        {
                            PostNow(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Approve");
                        }
                        else if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        {
                            // start fid.Hadid. poho, basa bulan mei tea
                            int no = FormData.PVNumber ?? 0;
                            int year = FormData.PVYear ?? 0;
                            int docCd = 0;
                            if (ScreenType == SCREEN_TYPE_PV)
                                docCd = 1;
                            else if (ScreenType == SCREEN_TYPE_RV)
                                docCd = 2;

                            //wait K2 process finished before check last status
                            if (formPersistence.WaitStatusChanged(no, year, docCd, FormData.StatusCode ?? 0))
                            {
                                int lastSts = formPersistence.GetLastStatusDocument(no, year, docCd);
                                var stsUpdBal = logic.Sys.GetArray("DISTRIBUTION_STATUS_PARAM", "PVRV_BALANCE_ON_REJECT");

                                logic.Say("btApprovalProceed_Click", "status update balance : {0}, last status : {1}"
                                    , CommonFunction.CommaJoin(stsUpdBal.ToList()), lastSts);

                                if (stsUpdBal.Contains(lastSts.str()))
                                    formPersistence.updateBalanceOutstanding(FormData, UserData, false);
                            }
                            // end fid.Hadid


                            PostNow(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Reject");
                            RemoveFromWorklist();
                        }
                        evaluateEditingControlVisibility();
                    }
                    else
                    {
                        if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        {
                            PostNow(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Approve");

                        }
                        else if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        {
                            if (Canceling)
                            {
                                logic.PVApproval.setCancel(FormData.PVNumber ?? 0, FormData.PVYear ?? 0, 0);
                            }
                            PostNow(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Reject");
                        }
                    }
                    #endregion
                }
                else if (!string.IsNullOrEmpty(holdComment) &&
                        (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Hold
                      || VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.UnHold))
                {
                    PVApprovalData header = logic.PVApproval.Search(strPVNumber, strPVYear);
                    String userName = UserName;

                    ErrorData err = new ErrorData();
                    int holdValue = 0;
                    if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Hold)
                    {
                        if (!Int32.TryParse(d.SelectedValue, out holdValue))
                            holdValue = 1;
                    }
                    else
                    {
                        holdValue = 0;
                    }

                    bool flag = logic.PVApproval.ChangeHoldValue(strPVNumber, strPVYear, userName, ref err,
                        CommonFunction.HoldValueOf(VoucherMasterPage.ApprovalType), holdValue);

                    if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Hold)
                    {
                        string _Command = resx.K2ProcID("Form_Registration_Approval");
                        if (logic.PVRVWorkflow.Go(strPVNumber + strPVYear, _Command, UserData, _Comment))
                        {
                            FormData.Submitted = true;

                            if (FormData.FinanceAccessEnabled)
                            {
                                FormData.StatusCode = 22;
                                RemoveFromWorklist();
                                refreshLabels();
                            }
                        }
                    }

                    insertNotice(holdComment, "Hold");

                    if (flag)
                    {
                        PostNow(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Un-Hold");
                    }
                    else
                    {
                        PostNow(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Hold");
                    }

                    FormData.OnHold = (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Hold);
                    evaluateEditingControlVisibility();
                }
                else
                {
                    if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Hold)
                    {
                        PostNow(ScreenMessage.STATUS_ERROR, "MSTD00088ERR", "Hold");
                    }
                    else if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.UnHold)
                    {
                        PostNow(ScreenMessage.STATUS_ERROR, "MSTD00088ERR", "Un-Hold");
                    }
                    else if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                    {
                        PostNow(ScreenMessage.STATUS_ERROR, "MSTD00059ERR", "");
                    }
                }
            }
            catch (Exception Ex)
            {
                string _ErrMsg = Ex.Message;
                LoggingLogic.err(Ex);
                PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", _ErrMsg);
            }

            txtApprovalComment.Text = "";

        }
        protected void evt_btSummary_onClick(object sender, EventArgs e)
        {
            if (FormData.TransactionCode == null)
            {
                PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Transaction Type");
                return;
            }

            if (FormData.Budgeted && FormData.BudgetNumber.isEmpty())
            {
                PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Budget Number");
                return;
            }

            string imgPath = Path.GetDirectoryName(Server.MapPath(AppSetting.CompanyLogo));
            string tPath = Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE", "DIR") + "/VoucherInvoiceSummaryTemplate.htm");
            string oPath = Server.MapPath(Common.AppSetting.TempFileUpload);

            string html = formPersistence.GetSummary(UserData, imgPath, tPath, FormData.PVNumber, FormData.PVYear);

            string oFile = formPersistence.WriteSummary(imgPath, oPath, UserData, html);

            string outLink = Request.Url.Scheme + @"://"
                + Request.Url.Authority + @"/"
                + Common.AppSetting.TempFileUpload.Substring(2) + oFile;

            ScriptManager.RegisterStartupScript(this,
                                   this.GetType(), "openWinLogin",
                                   "openWin('" + outLink + "','pdfDownload');",
                                   true);
        }
        #endregion Editing Control

        #region Utils
        protected string normalizeDivisionName(string divName)
        {
            if (divName != null)
            {
                if (divName.ToLower().EndsWith("head"))
                {
                    divName = divName.Substring(0, divName.IndexOf('-'));
                }
            }
            return divName;
        }
        private void insertNotice(string _Comment, string mode)
        {
            string strPVNumber = FormData.PVNumber.ToString();
            string strPVYear = FormData.PVYear.ToString();
            bool hasWorlist = hasPVAsWorklist();
            String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
            String noticeLink = String.Format("http://{0}:{1}{2}?pv_no={3}&pv_year={4}",
                Request.ServerVariables["SERVER_NAME"],
                HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                Request.ServerVariables["URL"],
                strPVNumber, strPVYear);

            EmailFunction mail = new EmailFunction(emailTemplate);
            ErrorData _Err = new ErrorData();
            String noticer = "";
            List<RoleData> listRole = RoLo.getUserRole(UserName, ref _Err);
            List<RoleData> listRoleTo = null;
            String userRole = "";
            String userRoleTo = "";
            String noticeTo = "";
            String appType = "";
            //List<string> listNoticeTo = null;

            if (listRole.Any())
            {
                userRole = listRole.FirstOrDefault().ROLE_NAME;
            }

            if (mode == "Comment")
            {
                if (NoteRecipientDropDown.Value != null)
                {
                    noticeTo = NoteRecipientDropDown.Value.ToString();
                    listRoleTo = RoLo.getUserRole(noticeTo, ref _Err);

                    if (listRoleTo.Any())
                    {
                        userRoleTo = listRoleTo.FirstOrDefault().ROLE_NAME;
                    }

                }
                appType = "Notice";
            }
            else
            {
                noticeTo = logic.Look.GetLastUser(strPVNumber, strPVYear, UserName, mode);

                if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                {
                    appType = "Reject";
                }
                else if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.Hold)
                {
                    appType = "Hold";
                }
                else if (VoucherMasterPage.ApprovalType == Common.Enum.ApprovalEnum.UnHold)
                {
                    appType = "Un-Hold";
                }
                hasWorlist = true;
                _Comment = String.Format("{0} Reason:<br>{1}", appType, _Comment);
            }

            if (string.IsNullOrEmpty(noticeTo))
            {
                noticeTo = UserName;
                logic.Notice.GetNoticeUser(strPVNumber, strPVYear);

            }
            List<string> lstUser = logic.Notice.listNoticeRecipient(strPVNumber, strPVYear, UserName, noticeTo);

            logic.Notice.InsertNotice(strPVNumber,
                                      strPVYear,
                                      UserName,
                                      userRole,
                                      noticeTo,
                                      userRoleTo,
                                      lstUser,
                                      _Comment,
                                      mode != "Hold", hasWorlist);

            formPersistence.fetchNote(FormData);
            List<FormNote> noticeList = FormData.Notes;
            NoteRepeater.DataSource = noticeList;
            NoteRepeater.DataBind();

            if (mail.ComposeApprovalNotice_EMAIL(
                    strPVNumber, strPVYear, _Comment, noticeLink, noticeTo,
                    UserData, noticer, FormData.DivisionID,
                    ScreenType, appType, ref _Err))
            {
                PostNow(ScreenMessage.STATUS_INFO, "MSTD00024INF", "");

            }

            if (_Err.ErrID == 1)
            {
                PostNow(ScreenMessage.STATUS_INFO, _Err.ErrMsgID, "");

            }
            else if (_Err.ErrID == 2)
            {
                PostNow(ScreenMessage.STATUS_INFO, "MSTD00002ERR", _Err.ErrMsg);
            }

            NoteCommentContainer.Attributes.Remove("style");
            NoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");
        }
        #endregion


        #region added by Akhmad Nuryanto, 13-9-2012,
        protected void evt_btSimulation_onClick(object sender, EventArgs args)
        {
            logic.Say("evt_btSimulation_onClick", "Simulate {0}", ScreenType);
            SimulatePanel.GroupingText = "Simulation";
            SimulateGrid.Visible = true;

            List<PVFormSimulationData> _list =
                logic.PVForm.GetSimulationData(FormData.PVNumber.ToString(), FormData.PVYear.ToString());

            Session["GRID_SIMULATION"] = _list;

            SimulateGrid.DataSource = _list;
            SimulateGrid.DataBind();

            SimulateGrid.DetailRows.ExpandAllRows();
            (SimulatePopUp as ModalPopupExtender).Show();
        }

        protected void btnCloseSimulation_Click(object sender, EventArgs e)
        {
            Session["GRID_SIMULATION"] = null;
            (SimulatePopUp as ModalPopupExtender).Hide();
        }

        protected void gridPopUpSimulation_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView g = sender as ASPxGridView;
            if (g != null)
            {
                if (Session == null || Session["GRID_SIMULATION"] == null)
                {
                    g.DataSource = null;
                    g.Visible = false;
                    return;
                }
                String masterKey = g.GetMasterRowKeyValue().ToString();

                List<PVFormSimulationData> _list = (List<PVFormSimulationData>)Session["GRID_SIMULATION"];
                if (_list != null && _list.Count > 0)
                {
                    bool noMatch = true;
                    foreach (var d in _list)
                    {
                        if (d.KEYS == masterKey && d.Details != null && d.Details.Count > 0)
                        {
                            noMatch = false;
                            g.Visible = true;
                            g.DataSource = d.Details;
                            break;
                        }
                    }

                    if (noMatch)
                    {
                        g.DataSource = null;
                        g.Visible = false;
                    }
                }
                else
                {
                    g.DataSource = null;
                    g.Visible = false;
                }
            }
        }

        protected void grid_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Value != null && e.Column.FieldName == "AMOUNT")
            {
                e.DisplayText = e.Value.ToString().Replace(" ", "&nbsp;");
            }
        }

        #endregion

        public void dump(string act = "")
        {
            string fn = ((FormData.PVNumber ?? 0) == 0 || (FormData.PVYear ?? 0) == 0) ?
                   FormData.ScreenType + "_%TIME%_%MS%" + "act"
                   : string.Format("{0}{1}", FormData.PVNumber, FormData.PVYear);
            File.WriteAllText(
              Util.GetTmp(
                  LoggingLogic.GetPath(), fn, ".txt")
              , FormData.dump());
        }
        private void ShowLoad()
        {
            string s, e;
            s = Request["__EVENTTARGET"];
            e = Request["__EVENTARGUMENT"];
            
            if ("lkpgridBookingNo".Equals(s))
            {
                LookUpBookingNo_TextChanged(BookingNoLookup, null);
            }
        }

        protected void evt_lnkSAPDocNumber_clicked(object sender, EventArgs args)
        {
            SAPGrid.DataSource = formPersistence.getSAPDocumentNumbers(FormData);
            SAPGrid.DataBind();
            SAPpop.Show();
        }
        protected void evt_btSAPDocPopupClose_clicked(object sender, EventArgs args)
        {
            SAPpop.Hide();
        }

        private bool isNull(string s)
        {
            return s.isEmpty() || s.ToLower().Equals("null");
        }

        protected void btSubmitOneTimeVendor_Click(object sender, EventArgs e)
        {
            logic.Say("btSubmitOneTimeVendor_Click", "Submit One Time Vendor");
            Ticker.Trace("SubmitOneTimeVendor");
            formPersistence.save(FormData.PVNumber ?? 0, FormData.PVYear ?? 0, OneTimeVendor());

            HiddenOneTimeVendorValid.Value = "0";
            PostToSapButton_Click();
        }

        protected virtual OneTimeVendorData OneTimeVendor()
        {
            return new OneTimeVendorData();
        }
        protected void dxBankKey_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            if (Mode == VIEW_MODE)
            {
                e.QueryableSource = new List<BankKeysData>().AsQueryable();
            }
            else
            {
                e.QueryableSource = formPersistence.GetBankKeys();
            }
            e.KeyExpression = "BANK_KEY";
        }

        protected void dxBankType_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            if (Mode == VIEW_MODE)
            {
                e.QueryableSource = new List<VendorBank>().AsQueryable();
            }
            else
            {
                var q = formPersistence.GetVendorBanks(FormData.VendorCode);
                e.QueryableSource = q;
            }
            e.KeyExpression = "Type";
        }

        protected virtual void ClearOneTimeVendor()
        {

        }

        protected virtual void SetOneTimeVendor(OneTimeVendorData o)
        {

        }

        public void renderTotalAmount()
        {
            int _no = FormData.PVNumber ?? 0;
            int _yy = FormData.PVYear ?? 0;
            OrderedDictionary mapTotalAmount = Tabel.TotalAmountMap();
            Dictionary<string, decimal> aSuspense = formPersistence.GetSuspenseAmounts(_no, _yy);
            if (mapTotalAmount.Count > 0)
            {
                StringBuilder sb = new StringBuilder();

                if (aSuspense.Count > 0)
                {
                    sb.AppendLine("<div id=\"TotalSuspense\">");
                    sb.Append("<table style=\"display: inline;\"><tr><td>Total Suspense/Curr: </td></tr></table>");
                    sb.AppendLine("<table style=\"display: inline;\">");
                    sb.AppendLine("<tr>");
                    int suspenseCurr = aSuspense.Count;
                    int i = 0;
                    foreach (string key in aSuspense.Keys)
                    {
                        sb.AppendFormat("<td class=\"totalAmount-detail\">{0}</td>\r\n",
                            CommonFunction.Eval_Curr(key, aSuspense[key], 1));
                        i++;

                        if ((i == 5) || (i >= suspenseCurr))
                        {
                            i = 0;
                            sb.AppendLine("</tr><tr>");
                        }
                    }
                    sb.AppendLine("</table></div>");
                }

                sb.AppendLine("<div id=\"TotalAmounts\">");
                sb.AppendFormat(System.Globalization.CultureInfo.CurrentCulture.NumberFormat,
                      "Total Amount (IDR): <span class=\"totalAmount-total\">  {0}</span>"
                      , Common.Function.CommonFunction.Eval_Curr("IDR", FormData.GetTotalAmount(), 1));
                sb.AppendLine("</div>");

                sb.AppendLine("<div class=\"totalAmount-detail-wrapper\">");
                sb.Append("<table style=\"display: inline;\"><tr><td>Total Amount/Curr: </td></tr></table>");
                sb.AppendLine("<table style=\"display: inline;\">");
                sb.AppendLine("<tr>");
                int itemCount = 0;
                int cntItem = mapTotalAmount.Count;
                foreach (string key in mapTotalAmount.Keys)
                {
                    sb.AppendFormat("<td class=\"totalAmount-detail\">{0}</td>\r\n",
                        CommonFunction.Eval_Curr(key, mapTotalAmount[key], 1));
                    itemCount++;

                    if ((itemCount == 5) || (itemCount == cntItem))
                    {
                        itemCount = 0;
                        sb.AppendLine("</tr>");
                    }
                }
                sb.AppendLine("</table>");
                sb.AppendLine("</div>");


                TotalAmountLiteral.Text = sb.ToString();
                TotalAmountLiteral.DataBind();
            }
        }

        public void OnRowUpdating(object sender, ASPxDataUpdatingEventArgs args)
        {
            OrderedDictionary values = args.NewValues;

            if (values.Count > 0)
            {
                int sequenceNumber = (int)values[VouCol.SEQUENCE_NUMBER];
                if (sequenceNumber >= 0)
                {
                    Tabel.EditIndex = sequenceNumber - 1;
                }

                PVFormDetail data = Tabel.getDataBySequence(sequenceNumber);
                if (data == null)
                {
                    Tabel.DataList.Add(new PVFormDetail());
                }

                data.Amount = (decimal)values.Val(VouCol.AMOUNT, data.Amount);
                data.Description = (string)values.Val(VouCol.DESCRIPTION, data.Description);
                data.StandardDescriptionWording = (string)values.Val(VouCol.DESCRIPTION_DEFAULT_WORDING, data.StandardDescriptionWording);
                data.InvoiceNumber = (string)values.Val(VouCol.INVOICE_NUMBER, data.InvoiceNumber);
                data.TaxNumber = (string)values.Val(VouCol.TAX_NUMBER, data.TaxNumber);

                renderTotalAmount();
                args.Cancel = true;
                DetailGrid.CancelEdit();
            }
        }

    }
}