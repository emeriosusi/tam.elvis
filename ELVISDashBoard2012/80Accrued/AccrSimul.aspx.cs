﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Data._80Accrued;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using Common.Messaging;
using DevExpress.Web.ASPxTabControl;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrSimul : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8010");
        private readonly string SESS_PR_SIMUL = "PR_GRID_SIMULATION";
        private readonly string SESS_DIRECT_SIMUL = "DIRECT_GRID_SIMULATION";
        private readonly string SESS_SUSPAID_SIMUL = "SUSPAID_GRID_SIMULATION";
        private readonly string SESS_SUSUNPAID_SIMUL = "SUSUNPAID_GRID_SIMULATION";
        private bool firstLoadSuspense = true;
        private int idxHeader = 0;
        private int susType = 0;

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }
        #endregion
        
        #region Getter Setter for Data List
        private string AccruedNo
        {
            get 
            {
                if (!Session["ACCRUED_SIMULATION"].ToString().isEmpty())
                {
                    return Session["ACCRUED_SIMULATION"].ToString();
                }
                return null;
            }
            set
            {
                Session["ACCRUED_SIMULATION"] = value;
            }
        }

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }
        protected List<AccrFormSimulationData> SimulDataPR
        {
            get { return (List<AccrFormSimulationData>)ViewState[SESS_PR_SIMUL]; }
            set { ViewState[SESS_PR_SIMUL] = value; }
        }

        protected List<AccrFormSimulationData> SimulDataDirect
        {
            get { return (List<AccrFormSimulationData>)ViewState[SESS_DIRECT_SIMUL]; }
            set { ViewState[SESS_DIRECT_SIMUL] = value; }
        }

        protected List<AccrFormSimulationData> SimulDataSusPaid
        {
            get { return (List<AccrFormSimulationData>)ViewState[SESS_SUSPAID_SIMUL]; }
            set { ViewState[SESS_SUSPAID_SIMUL] = value; }
        }

        protected List<AccrFormSimulationData> SimulDataSusUnpaid
        {
            get { return (List<AccrFormSimulationData>)ViewState[SESS_SUSUNPAID_SIMUL]; }
            set { ViewState[SESS_SUSUNPAID_SIMUL] = value; }
        }
       

        
        #endregion
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;

            if (!IsPostBack && !IsCallback)
            {
                string _accrNo = Request["accrno"];
                if (!_accrNo.isEmpty())
                {
                    AccruedNo = _accrNo;
                }

                PrepareLogout();
                SetFirstLoad();

                SimulateAccrued(0);
                HidActiveTab.Value = "0";
            }
        }

        public void SimulationListTabs_OnActiveTabChanged(object sender, EventArgs e)
        {
            int idxTab = SimulationListTabs.ActiveTabIndex;
            HidActiveTab.Value = idxTab.str();

            SimulateAccrued(idxTab);
        }

        protected void SimulateAccrued(int idxTab)
        {
            logic.Say("evt_btSimulation_onClick", "Simulate {0} : {1}", "Accrued Journal", idxTab);

            if (Session == null)
            {
                Nag("MSTD00002ERR", "No Session!");
                return;
            }

            #region Retrieve Simulation
            
            ASPxGridView gridSimul = null;
            List<AccrFormSimulationData> _list = null;

            if (idxTab == 0)
            {
                if (SimulDataPR == null)
                {
                    SimulDataPR = logic.AccrForm.GetPRSimulationData(AccruedNo);
                }

                _list = SimulDataPR;
                gridSimul = PRGridSimulation;
            }
            else if (idxTab == 1)
            {
                if (SimulDataDirect == null)
                {
                    SimulDataDirect = logic.AccrForm.GetDirectSimulationData(AccruedNo);
                }

                _list = SimulDataDirect;
                gridSimul = DirectGridSimulation;
            }
            else if (idxTab == 2)
            {
                if (SimulDataSusPaid == null)
                {
                    SimulDataSusPaid = logic.AccrForm.GetSuspenseSimulationData(AccruedNo, true);
                }

                _list = SimulDataSusPaid;
                gridSimul = SusPaidGridSimulation;
                
                firstLoadSuspense = true;
                susType = 1;
                idxHeader = 0;
            }
            else
            {
                if (SimulDataSusUnpaid == null)
                {
                    SimulDataSusUnpaid = logic.AccrForm.GetSuspenseSimulationData(AccruedNo, false);
                }

                _list = SimulDataSusUnpaid;
                gridSimul = SusUnpaidGridSimulation;
                
                firstLoadSuspense = true;
                susType = 2;
                idxHeader = 0;
            }
            #endregion

            gridSimul.DataSource = _list;
            gridSimul.DataBind();
            gridSimul.DetailRows.ExpandAllRows();
            gridSimul.Visible = true;
        }

        protected void gridPopUpSimulation_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView g = sender as ASPxGridView;
            if (g == null) return;

            if(Session == null)
            {
                g.DataSource = null;
                g.Visible = false;
                return;
            }

            int idxTab = -1;
            if (!int.TryParse(HidActiveTab.Value, out idxTab) || idxTab < 0)
            {
                g.DataSource = null;
                g.Visible = false;
                return;
            }

            List<AccrFormSimulationData> _list = null;

            switch (idxTab)
            {
                case 0: _list = SimulDataPR; break;
                case 1: _list = SimulDataDirect; break;
                case 2: _list = SimulDataSusPaid; break;
                case 3: _list = SimulDataSusUnpaid; break;
                default: break;
            }

            if (_list == null || _list.Count <= 0)
            {
                g.DataSource = null;
                g.Visible = false;
                return;
            }

            String masterKey = g.GetMasterRowKeyValue().ToString();
            AccrFormSimulationData d = null;
            if ((d = _list.Where(x => x.KEYS == masterKey).FirstOrDefault()) != null)
            {
                g.DataSource = d.Details;
                g.Visible = true;
            }
            else
            {
                g.DataSource = null;
                g.Visible = false;
            }
        }

        protected void SuspenseGridPopUpSimulation_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                if ((e.VisibleIndex % 4) / 2 > 0)
                {
                    e.Row.BackColor = System.Drawing.Color.LightBlue;
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.Transparent;
                }
            }
        }

        #region Set Button Visible

        private void SetFirstLoad()
        {
            btnClose.Visible = true;
        }

        #endregion


        #endregion

        #region Buttons
        

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        #endregion

        #region Function


        #endregion
    }
}