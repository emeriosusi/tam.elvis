﻿using AjaxControlToolkit;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using BusinessLogic.AccruedForm;
using Common.Control;
using Common.Data;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace ELVISDashBoard._80Accrued
{
    public abstract class BaseAccrFormBehind : BaseCodeBehind
    {
        public const string USER_ADMIN_ROLE = "ELVIS_USER_ADMIN";
        protected string BaseScreenType { get; set; }
        protected string BaseScreenID { get; set; }
        protected ASPxGridView BaseAttachmentGrid { set; get; }
        protected FileUpload BaseAttachmentUpload { set; get; }
        protected ModalPopupExtender BaseAttachmentPopup { set; get; }
        protected ASPxComboBox BaseAttachmentCategoryDropDown { set; get; }
        protected ASPxComboBox BaseNoteRecipientDropDown { set; get; }
        protected TextBox BaseNoteTextBox { set; get; }
        protected Repeater BaseNoteRepeater { set; get; }
        protected HtmlGenericControl BaseNoteCommentContainer { set; get; }
        protected HiddenField BaseHiddenMessageExpandFlag { set; get; }
        protected Literal BaseMessageControlLiteral { set; get; }
        private FormPersistence _fp = null;
        private FormPersistence fp
        {
            get
            {
                if (_fp == null)
                    _fp = new FormPersistence();
                return _fp;
            }
        }
        protected BaseAccrData BaseData { get; set; }

        private RoleLogic roleLogic = null;
        protected RoleLogic RoLo
        {
            get
            {
                if (roleLogic == null)
                {
                    roleLogic = new RoleLogic(UserName, BaseScreenID);
                }
                return roleLogic;
            }
        }
        protected abstract void BaseElementInit();
        protected abstract void BaseDataInit();

        protected void Page_Init(object sender, EventArgs e)
        {
            BaseElementInit();
            BaseDataInit();
        }

        #region Attachment

        protected struct AttachID
        {
            public string ReffNo;
            public string Year;
            public int Seq;
        }

        protected AttachID GetAttachID()
        {
            AttachID a = new AttachID()
            {
                ReffNo = BaseData.AccruedNo,
                Seq = 0
            };

            return a;
        }

        protected void updateAttachmentList()
        {
            GridViewColumn column = BaseAttachmentGrid.Columns["#"];
            column.Visible = BaseData.AttachmentTable.Editing;
            BaseAttachmentGrid.DataSource = BaseData.AttachmentTable.Attachments;
            BaseAttachmentGrid.DataBind();
        }

        protected void evt_btSendUploadAttachment_clicked(object sender, EventArgs arg)
        {
            if (BaseAttachmentUpload.HasFile)
            {
                logic.Say("btSendUploadAttachment_Click", "FileName = {0}", BaseAttachmentUpload.FileName);
                string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                int maxSize = int.Parse(strMaxSize);
                maxSize = maxSize * 1024;
                if (BaseAttachmentUpload.FileBytes.Length > maxSize)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                    BaseAttachmentPopup.Hide();
                    return;
                }

                bool uploadSucceded = true;
                string errorMessage = null;

                AttachID a = GetAttachID();
                string filename = CommonFunction.CleanFilename(BaseAttachmentUpload.FileName);
                ftp.ftpUploadBytes(a.Year, BaseScreenType,
                                a.ReffNo,
                                filename,
                                BaseAttachmentUpload.FileBytes,
                                ref uploadSucceded,
                                ref errorMessage);

                ListEditItem item = BaseAttachmentCategoryDropDown.SelectedItem;
                if (item.Value == null)
                {
                    PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                }

                FormAttachment attachment = BaseData.AttachmentTable.getBlankAttachment();
                attachment.ReferenceNumber = a.ReffNo;
                if (item != null)
                {
                    attachment.CategoryCode = item.Value.ToString();
                    attachment.CategoryName = item.Text;
                }
                attachment.PATH = a.Year + "/" + BaseScreenType + "/" + a.ReffNo;
                attachment.FileName = filename;
                attachment.Blank = false;
                fp.saveAttachmentInfo(attachment, UserData);

                fetchAttachment(a.ReffNo, ProcessID);
                updateAttachmentList();
            }
            BaseAttachmentPopup.Hide();
        }
        protected void evt_imgAddAttachment_onClick(object sender, EventArgs arg)
        {
            BaseAttachmentPopup.Show();
        }

        protected void evt_gridAttachment_onHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs arg)
        {
            BaseAttachmentGrid = (ASPxGridView)sender;

            if (arg.RowType != GridViewRowType.Data)
            {
                return;
            }

            decimal key = (decimal)arg.KeyValue;
            if (key > 0)
            {
                FormAttachment attachment = BaseData.AttachmentTable.get((int)key);
                if (attachment != null)
                {
                    KeyImageButton imgAdd = (KeyImageButton)BaseAttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgAddAttachment");
                    if (imgAdd != null)
                    {
                        imgAdd.Visible = attachment.Blank;
                    }
                    KeyImageButton imgDelete = (KeyImageButton)BaseAttachmentGrid.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgDeleteAttachment");
                    if (imgDelete != null)
                    {
                        int statusCd = BaseData.StatusCode ?? 0;
                        bool canDelAttach = statusCd == 0 || 
                                        (attachment.CreatedBy.isNotEmpty() && 
                                        attachment.CreatedBy.ToLower().Equals(UserData.USERNAME.ToLower()));

                        if (attachment.Blank || !canDelAttach)
                            imgDelete.CssClass = "hidden";
                        else
                            imgDelete.CssClass = "poited";
                    }
                }
            }
        }
        protected void evt_imgDeleteAttachment_onClick(object sender, EventArgs arg)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            int keyNum = int.Parse(key);
            logic.Say("evt_imgDeleteAttachment_onClick", "Key= {0}", keyNum);

            FormAttachment attachment = BaseData.AttachmentTable.get(keyNum);
            if ((attachment != null) && (!attachment.Blank))
            {
                AttachID a = GetAttachID();

                string folderPath = a.Year + "/" + BaseScreenType + "/" + a.ReffNo;
                ftp.Delete(folderPath, attachment.FileName);
                deleteAttachmentInfo(attachment);

                fetchAttachment(a.ReffNo, ProcessID);
                updateAttachmentList();
            }
        }
        protected void resetAttachment()
        {
            AttachID a = GetAttachID();
            string folderPath = a.Year + "/" + BaseScreenType + "/" + a.ReffNo;

            foreach (var att in BaseData.AttachmentTable.Attachments)
            {
                if (att.FileName.isEmpty())
                    continue;

                ftp.Delete(folderPath, att.FileName);
                deleteAttachmentInfo(att);
            }

            fetchAttachment(a.ReffNo, ProcessID);
            updateAttachmentList();
        }

        protected void evt_cboxAttachmentCategory_onLoad(object sender, EventArgs args)
        {
            List<CodeConstant> a = fp.getAttachmentCategories();
            BaseAttachmentCategoryDropDown.DataSource = a;
            BaseAttachmentCategoryDropDown.DataBind();
        }
        protected void evt_btCloseUploadAttachment_clicked(object sender, EventArgs arg)
        {
            BaseAttachmentPopup.Hide();
        }

        protected void ReArrangeAttachments(string pre, string post, string yy)
        {
            string olde = pre + yy;
            string neo = post + yy;
            if (!pre.StartsWith("T")) return;
            List<FormAttachment> atts = fp.getAttachments(olde);
            string opath = "";
            foreach (FormAttachment a in atts)
            {
                string oldFile = a.PATH + "/" + a.FileName;
                opath = a.PATH;
                string newFile = "";
                string[] p = a.PATH.Split('/');

                if (p.Length > 0)
                {
                    p[p.Length - 1] = post;
                    newFile = string.Join("/", p, 0, p.Length) + "/" + a.FileName;
                }
                ftp.Move(oldFile, newFile);
                fp.MoveAttachment(pre, a.SequenceNumber, post, yy);
            }
            ftp.RemoveDir(opath);
        }

        protected void reloadAttachment()
        {
            fetchAttachment(BaseData.AccruedNo, ProcessID);
            updateAttachmentList();
        }

        #endregion Attachment

        #region Notes
        protected void evt_ddlSendTo_onLoad(object sender, EventArgs arg)
        {
            BaseNoteRecipientDropDown = (ASPxComboBox)sender;

            List<UserRoleHeader> u = new List<UserRoleHeader>();
            if (BaseData.ReffNo.isNotEmpty())
                u = logic.Notice.GetNoticeUser(BaseData.ReffNo, "");

            BaseNoteRecipientDropDown.DataSource = u;
            BaseNoteRecipientDropDown.DataBind();
        }
        protected void evt_btSendNotice_onClick(object sender, EventArgs e)
        {
            bool hasWorlist = hasWorklist(BaseData.AccruedNo);
            clearScreenMessage();

            if (BaseNoteRecipientDropDown != null)
            {
                String recipient = Convert.ToString(BaseNoteRecipientDropDown.Value);
                string loggedUserRole = UserData.Roles.FirstOrDefault();
                if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
                    return;
                }
            }

            try
            {
                string strNotice = BaseNoteTextBox.Text;
                if (!String.IsNullOrEmpty(strNotice))
                {
                    string lowerScreenType = BaseScreenType.ToLower();
                    String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
                    String noticeLink = String.Format("{0}://{1}:{2}{3}?accrno={4}&mode=view",
                        Request.Url.Scheme,
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        Request.ServerVariables["URL"],
                        BaseData.AccruedNo);

                    EmailFunction mail = new EmailFunction(emailTemplate);
                    ErrorData err = new ErrorData();
                    List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

                    String userRole = "";
                    String userRoleTo = "";
                    String noticeTo = "";
                    String noticer = "";

                    if (listRole.Any())
                    {
                        userRole = listRole.FirstOrDefault().ROLE_NAME;
                    }

                    List<string> lstUser = null;
                    if (BaseNoteRecipientDropDown.Value != null)
                    {
                        noticeTo = BaseNoteRecipientDropDown.Value.ToString();

                        ErrorData _Err = new ErrorData(0, "", "");
                        List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
                        userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
                        lstUser = logic.Notice.listNoticeRecipient(BaseData.ReffNo, "", UserName, noticeTo);
                    }
                    else
                    {
                        noticeTo = UserName;
                        userRoleTo = "";

                        lstUser = logic.Notice.listNoticeRecipient(BaseData.ReffNo, "", UserName, noticeTo);
                    }

                    if (lstUser == null)
                    {
                        lstUser = new List<string>();
                    }
                    NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

                    logic.Notice.InsertNotice(BaseData.ReffNo.str(),
                                            BaseData.CreatedDt.Year.str(),
                                            UserName,
                                            userRole,
                                            noticeTo,
                                            userRoleTo,
                                            lstUser,
                                            strNotice,
                                            true, hasWorlist);

                    fetchNote();
                    List<FormNote> noticeList = BaseData.Notes;
                    BaseNoteRepeater.DataSource = noticeList;
                    BaseNoteRepeater.DataBind();

                    if (noticeList.Count > 0)
                    {
                        var q = noticeList.Where(a => a.Date != null);
                        if (q.Any())
                        {
                            noticer = q.Last().SenderName;
                        }
                        else
                        {
                            noticer = UserName;
                        }
                    }
                    else
                    {
                        noticer = UserName;
                    }

                    int cntSentMail = 0;
                    if (BaseNoteRecipientDropDown.Value != null)
                    {

                        if (mail.ComposeApprovalNotice_EMAIL(BaseData.ReffNo,
                                                             BaseData.CreatedDt.Year.str(),
                                                             strNotice, noticeLink, noticeTo,
                                                             UserData, noticer,
                                                             BaseData.DivisionID,
                                                             BaseScreenType, "Notice", ref err))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
                            cntSentMail++;
                        }

                        if (cntSentMail > 0)
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF", err.ErrMsg);
                        }

                        if (err.ErrID == 1)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
                        }
                        else if (err.ErrID == 2)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                Handle(ex);
            }
            BaseNoteTextBox.Text = "";

            BaseNoteCommentContainer.Attributes.Remove("style");
            BaseNoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");

            postScreenMessage();
        }
        protected void evt_rptrNotice_onItemDataBound(object sender, RepeaterItemEventArgs arg)
        {
            RepeaterItem item = arg.Item;
            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                Literal litOpenDivNotice = (Literal)item.FindControl("litOpenDivNotice");
                Literal litRptrNoticeComment = (Literal)item.FindControl("litRptrNoticeComment");
                Literal litCloseDivNotice = (Literal)item.FindControl("litCloseDivNotice");
                String dateHeader;

                FormNote note = (FormNote)item.DataItem;

                if (note.Date.HasValue)
                {
                    litOpenDivNotice.Text = "<div class='noticeLeft'>";
                    dateHeader = ((DateTime)note.Date.Value).ToString("d MMMM yyyy hh:mm:ss");
                }
                else
                {
                    litOpenDivNotice.Text = "<div class='noticeRight'>";
                    dateHeader = ((DateTime)note.ReplyDate.Value).ToString("d MMMM yyyy hh:mm:ss");
                }

                litRptrNoticeComment.Text = String.Format("<span class='noteSender'>{0}</span> {1}<br>To: <span class='noteSender'>{2}</span><br>{3}",
                    note.SenderName, dateHeader, note.ReceiverName, note.Message);


                litCloseDivNotice.Text = "</div>";
            }
        }
        protected void reloadNote()
        {
            fetchNote();
            BaseNoteRepeater.DataSource = BaseData.Notes;
            BaseNoteRepeater.DataBind();
        }

        protected void fetchNote()
        {
            fp.fetchNote(BaseData);
        }
        protected void fetchAttachment(string refNumber, int pid = 0)
        {
            fp.fetchAttachment(refNumber, BaseData.AttachmentTable, pid);
        }
        protected void deleteAttachmentInfo(FormAttachment formAttachment)
        {
            fp.deleteAttachmentInfo(formAttachment);
        }
        #endregion Notes

        #region Screen Message
        protected List<ScreenMessage> ScreenMessageList
        {
            get
            {
                List<ScreenMessage> lstMessages = Session[BaseScreenType + "MsgList"] as List<ScreenMessage>;
                if (lstMessages == null)
                {
                    lstMessages = new List<ScreenMessage>();
                    Session[BaseScreenType + "MsgList"] = lstMessages;
                    BaseHiddenMessageExpandFlag.Value = "false";
                }
                return lstMessages;
            }
        }
        private void PostMsg(bool instant, bool clearfirst, byte sts, string id, params object[] x)
        {
            string m = "";
            StringBuilder b = new StringBuilder("");
            if (x != null && x.Length > 0)
            {
                for (int k = 0; k < x.Length; k++)
                    b.Append(x[k].str() + ", ");
            }

            if (!id.isEmpty())
            {
                m = _m.Message(id);
            }
            if (x != null && x.Length > 0)
            {
                if (!m.isEmpty())
                    m = string.Format(m, x);
                else
                {
                    b.Clear();
                    for (int i = 0; i < x.Length; i++)
                    {
                        b.Append(x[i].str());
                        b.Append(" ");
                    }
                    m = b.ToString().Trim();
                }
            }
            postScreenMessage(
                new ScreenMessage[]
                {
                    new ScreenMessage(sts, m)
                }
                , clearfirst
                , instant);
        }

        protected void PostLaterOn(byte sts, string id, params object[] x)
        {
            PostMsg(false, true, sts, id, x);
        }

        protected void PostNowPush(byte sts, string id, params object[] x)
        {
            PostMsg(true, false, sts, id, x);
        }

        protected void PostNow(byte sts, string id, params object[] x)
        {
            PostMsg(true, true, sts, id, x);
        }

        protected void PostLater(byte ScreenMessageStatus, string id, params object[] x)
        {
            PostMsg(false, false, ScreenMessageStatus, id, x);
        }

        protected void postScreenMessage()
        {
            postScreenMessage(null, false, true);
        }

        protected void postScreenMessage(ScreenMessage[] messages, bool clearFirst, bool instantDisplay)
        {
            List<ScreenMessage> lstMessage = ScreenMessageList;
            if (clearFirst)
            {
                lstMessage.Clear();
            }

            if (messages != null)
            {
                for (int i = 0; i < messages.Length; i++)
                {
                    lstMessage.Insert(0, messages[i]);
                }
            }
            updateScreenMessages();
        }
        protected void updateScreenMessages()
        {
            List<ScreenMessage> lstMessages = ScreenMessageList;
            int cntMessages = lstMessages.Count;
            if (cntMessages > 0)
            {
                ScreenMessage msg = lstMessages[0];
                StringBuilder stringBuilder = new StringBuilder();
                string expandFlag = BaseHiddenMessageExpandFlag.Value;

                string messageBoxClass = "message-info";
                string messageBoxSpanClass = "message-span-info";
                if (msg.Status == ScreenMessage.STATUS_WARNING)
                {
                    messageBoxClass = "message-warning";
                    messageBoxSpanClass = "message-span-warning";
                }
                else if (msg.Status == ScreenMessage.STATUS_ERROR)
                {
                    messageBoxClass = "message-error";
                    messageBoxSpanClass = "message-span-error";
                }

                if (expandFlag.Equals("true"))
                {
                    messageBoxClass += " expanded-message-box";
                }
                stringBuilder.AppendLine(string.Format("<div id='messageBox' class='{0}'>", messageBoxClass));
                stringBuilder.AppendLine(string.Format("      <span class='{0}'>{1}</span>", messageBoxSpanClass, msg.Message));

                if (cntMessages > 1)
                {
                    string txtMore = "more";
                    if (expandFlag.Equals("true"))
                    {
                        txtMore = "close";
                    }
                    stringBuilder.AppendLine("      <span class='message-span-more'>");
                    stringBuilder.AppendLine(string.Format("          <a id='message-detail-link' href='#' onclick=\"expandScreenMessage()\">{0}</a>", txtMore));
                    stringBuilder.AppendLine("      </span>");
                }

                stringBuilder.AppendLine("</div>");
                if (expandFlag.Equals("false"))
                {
                    stringBuilder.AppendLine("<div id='messageBox-all' style='display: none'>");
                }
                else
                {
                    stringBuilder.AppendLine("<div id='messageBox-all'>");
                }
                stringBuilder.AppendLine("  <ul>");
                string listClass;
                for (int i = 1; i < cntMessages; i++)
                {
                    msg = lstMessages[i];
                    listClass = "message-detail-info";
                    if (msg.Status == ScreenMessage.STATUS_WARNING)
                    {
                        listClass = "message-detail-warning";
                    }
                    else if (msg.Status == ScreenMessage.STATUS_ERROR)
                    {
                        listClass = "message-detail-error";
                    }

                    stringBuilder.AppendLine(string.Format("<li class='{0}'>", listClass));
                    stringBuilder.AppendLine("      <span>" + msg.Message + "</span>");
                    stringBuilder.AppendLine("</li>");
                }
                stringBuilder.AppendLine("  </ul>");
                stringBuilder.AppendLine("</div>");

                BaseMessageControlLiteral.Text = stringBuilder.ToString();
                BaseMessageControlLiteral.Visible = true;
            }
            else
            {
                BaseMessageControlLiteral.Text = "";
                BaseMessageControlLiteral.Visible = false;
            }
        }
        protected void clearScreenMessage()
        {
            ScreenMessageList.Clear();
            updateScreenMessages();
        }
        protected void evt_messageControl_onLoad(object sender, EventArgs args)
        {
            updateScreenMessages();
        }

        #endregion Screen Message
    }
}