﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Data._30PVList;
using Common.Data.SAPData;
using Common.Function;
using ELVISDashBoard._80Accrued;
using ELVISDashBoard.presentationHelper;
using DevExpress.Web.ASPxGridLookup;
using System.Web.UI.WebControls;
using System.Text;
using DevExpress.Web.ASPxEditors;


namespace ELVISDashBoard._80Accrued
{
    public partial class AccrForm : AccruedFormPage
    {
        private const int VIEW_MODE = 0;
        private const int ADD_MODE = 1;
        private const int EDIT_MODE = 2;
        
        public AccrForm() : base(AccruedFormPage.SCREEN_TYPE_ACCR)
        {
            _ScreenID = resx.Screen("ELVIS_Screen_8005");
        }

        public override void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            base.Page_Load(sender, e);
        }

        protected override void postbackInit()
        {
            HistoryGrid = gridApprovalHistory;
            DetailGrid = gridAccrDetail;
            AttachmentGrid = gridAttachment;
            NoteRecipientDropDown = ddlSendTo;
            AttachmentCategoryDropDown = cboxAttachmentCategory;
            //CalculateTaxCheckBox = chkCalculateTax;
            DownloadUploadTemplateLink = lnkDownloadTemplate;

            IssuingDivisionLabel = tboxIssueDiv;
            AccruedNoLabel = tboxAccruedNo;
            USDIDRLabel = tboxUsdIdr;
            JPYIDRLabel = tboxJpyIdr;
            PicCurLabel = tboxPicCur;
            PicNextLabel = tboxPicNext;
            TotAmountLabel = tboxTotAmt;
            CreatedDtLabel = tboxCreateDt;
            SubmStsLabel = tboxSubmSts;
            WorkflowStsLabel = tboxWFSts;

            MessageControlLiteral = messageControl;
            TotalAmountLiteral = ltTotalAmount;
            NoteTextBox = txtNotice;
            NoteRepeater = rptrNotice;
            NoteCommentContainer = noticeComment;

            HiddenMessageExpandFlag = hdExpandFlag;
            HiddenScreenID = hidScreenID;
            EditButton = btEdit;
            SaveButton = btSave;
            CancelButton = btCancel;
            CloseButton = btnClose;
            SubmitButton = btSubmit;
            RejectButton = btReject;
            CheckBudgetButton = btCheckBudget;
            ExportButton = btExport;
            DataUploadButton = btnUploadTemplate;
            PostSAPButton = btPostToSAP;
            SimulateUserButton = btSimulation;
            ApproveButton = btnApprove;
            DataUpload = fuInvoiceList;
            AttachmentUpload = uploadAttachment;
            UploadDiv = UploadDetailDiv;
            AttachmentPopup = popdlgUploadAttachment;
            WbsUpdateButton = btWBSUpdate;
            PostingDate = dtPostingDt;
            PostingDateLiteral = ltPostingDt;

            DownloadEntertainmentTemplateLink = EntertainmentAttachDiv;

            PopUpRevise = popdlgRevise;
            BtnReviseProceed = btnReviseProceed;
            BtnReviseCancel = btnReviseCancel;
            TxtReviseAccruedNo = txtReviseAccrNo;
            DdlReviseCategory = ddlReviseCategory;
            TxtReviseComment = txtReviseComment;
        }

        protected bool isSettlement(int Code)
        {
            return (Code == 3);
        }

        protected bool HoldVisibleStatus(int? statusCD)
        {
            //return (statusCD ?? 0) < 27;
            return !(new int[] { 27, 28, 29 }.Contains(statusCD ?? 0));
        }

    }
}