﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Data._80Accrued;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using BusinessLogic._80Accrued;
using AjaxControlToolkit;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrResultList : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8008");
        private string _accruedNo = null;



        private AccruedListData AccruedData
        {
            get
            {
                var _accruedData = (AccruedListData)Session["_AccruedData_ELVIS_Screen_8008"];

                if (_accruedData == null && _accruedNo != null)
                {
                    _accruedData = logic
                                    .AccrList
                                    .Search(new AccruedSearchCriteria(_accruedNo))
                                    .FirstOrDefault();

                    Session["_AccruedData_ELVIS_Screen_8008"] = _accruedData;
                }

                return _accruedData;
            }
            set
            {
                Session["_AccruedData_ELVIS_Screen_8008"] = value;
            }
        }
        //private List<>

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }
        #endregion

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }
       

        #region Getter Setter for Data List
        private List<AccruedListDetail> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<AccruedListDetail>();
                }
                else
                {
                    return (List<AccruedListDetail>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion
        #region Getter Setter for Attachment List
        [Serializable]
        private class ListAttachmentData
        {
            private string _NO;
            public string NO
            {
                get
                {
                    return _NO;
                }
                set
                {
                    _NO = value;
                }
            }

            private string _FILENAME;
            public string FILENAME
            {
                get
                {
                    return _FILENAME;
                }
                set
                {
                    _FILENAME = value;
                }
            }
        }
        private List<ListAttachmentData> ListAttachment
        {
            set
            {
                ViewState["_listAttachment"] = value;
            }
            get
            {
                if (ViewState["_listAttachment"] == null)
                {
                    return new List<ListAttachmentData>();
                }
                else
                {
                    return (List<ListAttachmentData>)ViewState["_listAttachment"];
                }
            }
        }
        #endregion
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;

            if (!IsPostBack)
            {
                AccruedData = null;
                _accruedNo = Request["accrno"];
                if (_accruedNo.Trim().isEmpty())
                {
                    Response.Redirect("~/Default.aspx");
                }

                

                PrepareLogout();
                PrepareDataHeader();

                Ticker.In("GenerateComboData");
                GenerateComboData(DropDownListPVType, "PV_LIST_TYPE", true);
                Ticker.Out();

                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();

                gridGeneralInfo.DataSource = null;
                #region Init Startup Script
                AddEnterComponent(tboxBookingNo);
                AddEnterComponent(DropDownListPVType);
                AddEnterComponent(tboxActivity);
                AddEnterComponent(tboxOldWbsNo);
                AddEnterComponent(tboxOldWbsDesc);
                AddEnterComponent(tboxOldSusNo);
                AddEnterComponent(tboxNewWbsNo);
                AddEnterComponent(tboxNewWbsDesc);
                AddEnterComponent(tboxNewSusNo);
                AddEnterComponent(tboxAmtAccrFrom);
                AddEnterComponent(tboxAmtAccrTo);
                //AddEnterComponent(tboxAmtSpentFrom);
                //AddEnterComponent(tboxAmtSpentTo);
                //AddEnterComponent(tboxAmtRemainFrom);
                //AddEnterComponent(tboxAmtRemainTo);

                AddEnterAsSearch(btnSearch, _ScreenID);


                DoSearch();
                #endregion
            }
        }


        protected void DoSearch()
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
                return;
            }

            if (ValidateInputSearch())
            {
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;

                gridGeneralInfo.DataBind();
            }
        }

        protected void gridSapDocNo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoSapDocNo = gridSapDocNo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridSapDocNo.Columns["NO"] as GridViewDataColumn, "litGridNoSapDocNo") as Literal;
                litGridNoSapDocNo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonClosePopUpSapDocNo_Click(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = false;
            ModalPopupSapDocNo.Hide();
        }

        protected void LoadGridPopUpSapDocNo(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = true;
            LitMsgSapDocNo.Text = "";
            panelSapDocNo.GroupingText = "SAP Document Number";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            string bookingNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "BOOKING_NO").str();
            string accrNo = AccruedData.ACCRUED_NO;

            List<SapDocNoData> _list = logic.Look.GetSapDocNoByBookNo(bookingNo, accrNo);
            gridSapDocNo.DataSource = _list;
            gridSapDocNo.DataBind();
            ModalPopupSapDocNo.Show();
        }


        #region Set Button Visible
        private void SetFirstLoad()
        {
            btnSearch.Visible = RoLo.isAllowedAccess("btnSearch");
            btnClear.Visible = RoLo.isAllowedAccess("btnClear");
            btnDownload.Visible = RoLo.isAllowedAccess("btnDownload");
        }

        private void SetSearchLoad(bool noData)
        {
            if (noData)
            {
                SetFirstLoad();
            }
            else
            {
                btnSearch.Visible = RoLo.isAllowedAccess("btnSearch");
                btnClear.Visible = RoLo.isAllowedAccess("btnClear");
                btnDownload.Visible = RoLo.isAllowedAccess("btnDownload");
            }
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            //if (PageMode == Common.Enum.PageMode.View)
            //{
            //    if (gridGeneralInfo.PageCount > 0)
            //    {
            //        ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
            //            gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
            //        Literal litGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
            //            gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "litGridSpentAmount") as Literal;
            //        if (txtGridSpentAmount != null)
            //        {
            //            txtGridSpentAmount.Visible = false;
            //            litGridSpentAmount.Visible = true;
            //        }
            //    }
            //}
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            ListInquiry = logic.AccrList.DataListAccrResult(SearchCriteria(1)).ToList();
            ListInquiry.Sort(AccruedListDetail.RuleOrder);
            ListInquiry.Distinct();

            SetSearchLoad(!ListInquiry.Any());

            e.Result = ListInquiry;
        }

        protected AccruedResultSearch SearchCriteria(int caller)
        {
            return new AccruedResultSearch(AccruedData.ACCRUED_NO, tboxBookingNo.Text, tboxActivity.Text
                , tboxOldWbsNo.Text, tboxOldWbsDesc.Text, tboxOldSusNo.Text, tboxNewWbsNo.Text, tboxNewWbsDesc.Text
                , tboxNewSusNo.Text, DropDownListPVType.Text, tboxAmtAccrFrom.Text, tboxAmtAccrTo.Text, ""
                , "", "", "", caller);
        }
        #endregion
        #endregion

        #region Buttons
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //logic.Say("btnSearch_Click", "Search {0} {1}", ddlSuspenseNo.Text, txtPVYear.Text);
            DoSearch();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            //if (IssuingDivisionDropDown.Enabled)
            //    sel.Clear();
            //txtPVYear.Text = null;
            //txtAccrNo.Text = null;
            //ddlSubSts.SelectedIndex = 0;
            //dtCreatedDateFrom.Text = null;
            //dtCreatedDateTo.Text = null;
            //ddlWorkflowStatus.SelectedIndex = 0;
            //txtTotAmountFrom.Text = null;
            //txtTotAmountTo.Text = null;

            tboxBookingNo.Text = null;
            tboxOldWbsNo.Text = null;
            tboxNewWbsNo.Text = null;
            DropDownListPVType.SelectedIndex = 0;
            tboxOldWbsDesc.Text = null;
            tboxNewWbsDesc.Text = null;
            tboxActivity.Text = null;
            tboxOldSusNo.Text = null;
            tboxNewSusNo.Text = null;
            tboxAmtAccrFrom.Text = null;
            tboxAmtAccrTo.Text = null;
            //tboxAmtSpentFrom.Text = null;
            //tboxAmtSpentTo.Text = null;
            //tboxAmtRemainFrom.Text = null;
            //tboxAmtRemainTo.Text = null;


            gridGeneralInfo.DataSource = null;
            gridGeneralInfo.DataSourceID = null;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            UserData userdata = (UserData)Session["UserData"];
            try
            {
                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "AccruedResultList.xls");
                string excelName = "List_Accrued_Result_Download_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                //string pdfName = "List_Accrued_Result_Download_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";


                string fullPath = logic.AccrList.GenerateListAccrResult(
                            UserName,
                            templatePath,
                            excelName,
                            SearchCriteria(2),
                            userdata
                        );
                Xmit.Transmit(HttpContext.Current, fullPath);

                //Xmit.Send(logic.AccrList.GetDataReport(
                //            UserName,
                //            Server.MapPath(Common.AppSetting.CompanyLogo),
                //            SearchCriteria(1)
                //        ), HttpContext.Current, "List_Accrued_Result_Download_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls");

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    string xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                    Nag("MSTD00042ERR", ex.Message);
                }
            }

            //return fileName;
        
        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            string bookingNo = gridGeneralInfo.GetRowValues(e.VisibleIndex, "BOOKING_NO").ToString();

            ViewActivity(bookingNo);
        }
        protected void ViewActivity(string bookingNo)
        {
            //logic.Say("btnActivity_Click", "Activity {0}", ScreenType);
            panelActivity.GroupingText = "Simulation";
            gridActivity.Visible = true;

            var param = new Dictionary<string, object>();
            param.Add("ACCRUED_NO", AccruedData.ACCRUED_NO);
            param.Add("BOOKING_NO", bookingNo);

            List<AccruedListDetail> _list = logic.AccrList.GetActivityAccrued(param);

            gridActivity.DataSource = _list;
            gridActivity.DataBind();

            lblBookingNo.Text = bookingNo;

            gridActivity.DetailRows.ExpandAllRows();
            (popupActivity as ModalPopupExtender).Show();
        }

        protected void btnCloseActivity_Click(object sender, EventArgs e)
        {
            lblBookingNo.Text = "";
            (popupActivity as ModalPopupExtender).Hide();
        }
        #endregion

        #region Function

        private bool ValidateInputSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Handle(ex);
                throw ;
            }
            return true;
        }

        private void Focus_On_Next_Empty_TextBox(int currentIndex)
        {
            bool setFocus = false;
            if (gridGeneralInfo.VisibleRowCount > 0)
            {
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    ASPxTextBox txtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(i,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (txtGridSpentAmount != null && (String.IsNullOrWhiteSpace(txtGridSpentAmount.Text)))
                    {
                        txtGridSpentAmount.Focus();
                        setFocus = true;
                        break;
                    }
                }

                if (!setFocus)
                {
                    ASPxTextBox nxtGridSpentAmount = gridGeneralInfo.FindRowCellTemplateControl(currentIndex + 1,
                        gridGeneralInfo.Columns["SPENT_AMOUNT"] as GridViewDataColumn, "txtGridSpentAmount") as ASPxTextBox;
                    if (nxtGridSpentAmount != null)
                    {
                        nxtGridSpentAmount.Focus();
                    }
                }
            }
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.PVList.GetUserDivisions(UserData);
        }

        protected string Status_Color(Object statusValue)
        {
            string color = "";
            short statusInt = 0;
            if (statusValue != null && Int16.TryParse(statusValue.ToString(), out statusInt))
            {
                if (statusInt == 1)
                    color = red;
                else if (statusInt == 2)
                    color = yellow;
                else if (statusInt > 2)
                    color = green;
            }
            return color;
        }

        protected string WriteSelectedIndexInfo(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected void grid_CustomCallback(object sender,
            DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            int newSize;
            if (!int.TryParse(e.Parameters.ToString(), out newSize)) return;

            if (newSize < 0)
                newSize = 0;
            grid.SettingsPager.PageSize = newSize;
            PageSize = newSize;
            // Session[GridCustomPageSizeName] = newSize;
            if (grid != null && e != null && !String.IsNullOrEmpty(e.Parameters) && grid.VisibleRowCount > 0)
            {
                if (ValidateInputSearch())
                {
                    grid.DataBind();

                }
            }
        }

        protected void PrepareDataHeader()
        {
            tboxIssueDiv.Text = normalizeDivisionName(logic.Vendor.GetDivisionName(AccruedData.DIVISION_ID));
            tboxTotAmt.Text = AccruedData.TOT_AMOUNT.fmt(2);
            tboxAccruedNo.Text = AccruedData.ACCRUED_NO;

            tboxPicCur.Text = AccruedData.PIC_CURRENT;
            tboxSubmSts.Text = AccruedData.SUBMISSION_STATUS;
            tboxUpdateDt.Text = AccruedData.CHANGED_DT.HasValue ? AccruedData.CHANGED_DT.Value.ToString("dd MMM yyyy") : AccruedData.CREATED_DT.ToString("dd MMM yyyy");
            tboxPicNext.Text = AccruedData.PIC_NEXT;
            tboxWFSts.Text = AccruedData.WORKFLOW_STATUS.isNotEmpty() ? logic.Sys.GetText("WORKFLOW_STATUS", AccruedData.WORKFLOW_STATUS) : "";

            var ExchRate = logic.Look.getExchangeRates();

            tboxUsdIdr.Text = ExchRate
                .Where(x => x.CurrencyCode == "USD")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);

            tboxJpyIdr.Text = ExchRate
                .Where(x => x.CurrencyCode == "JPY")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);
        }

        protected string normalizeDivisionName(string divName)
        {
            if (divName != null)
            {
                if (divName.ToLower().EndsWith("head"))
                {
                    divName = divName.Substring(0, divName.IndexOf('-'));
                }
            }
            return divName;
        }

        protected int GetFirstSelectedRow()
        {
            int row = -1;
            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                if (!gridGeneralInfo.Selection.IsRowSelected(i)) continue;

                row = i;
                break;
            }
            return row;
        }
        #endregion
    }
}