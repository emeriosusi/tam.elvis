﻿<%@ Page Title="PV List" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="AccrPVList.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrPVList" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="preX" ContentPlaceHolderID="pre" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>
    <script src="../App_Themes/BMS_Theme/Script/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        var gridAllSelected = false;
       
        function openOtherAmount(pvNo, pvYear) {
            PageMethods.otherAmount_Click(pvNo, pvYear);
        }

        function setPVKey(pvNo, pvYear) {
            var _pvNo = document.getElementById("HiddenPVNo");
            var _pvYear = document.getElementById("HiddenPVYear");
            _pvYear.value = pvYear;
            _pvNo.value = pvNo;
        }

        function HideIt(x) {

            var d = document.getElementById(x);
            if (d) d.style.display = "none";
        }

        function InitPageControl() {
            var tab = PVListTabs.GetActiveTab();
            if (tab.index == 0) {
                document.getElementById("<%= pnlLegendInfo.ClientID %>").style.display = "block";
                document.getElementById("<%= pnlLegendStatus.ClientID %>").style.display = "none";
            }
            else {
                document.getElementById("<%= pnlLegendInfo.ClientID %>").style.display = "none";
                document.getElementById("<%= pnlLegendStatus.ClientID %>").style.display = "block";
            }

            var hasSelectAll = document.getElementById("HiddenCanSelectAll");

            if (hasSelectAll.value == '0') {
                
                HideIt("SelectAllCheckBox");
                HideIt("SelectAllgridStatusUser");
                HideIt("SelectAllgridStatusFinance");
            }
        }

        function ActiveTabChanged(e) {
            if (e.tab.index == 0) {
                document.getElementById("<%= pnlLegendInfo.ClientID %>").style.display = "block";
                document.getElementById("<%= pnlLegendStatus.ClientID %>").style.display = "none";
            }
            else {
                document.getElementById("<%= pnlLegendInfo.ClientID %>").style.display = "none";
                document.getElementById("<%= pnlLegendStatus.ClientID %>").style.display = "block";
            }

            var hasSelectAll = document.getElementById("HiddenCanSelectAll");

            if (hasSelectAll.value == '1') {
                if (e.tab.index == 0) {
                    SelectAllCheckBox.SetChecked(gridAllSelected);
                    gridGeneralInfo.SelectAllRowsOnPage(gridAllSelected);
                }
                else if (e.tab.index == 1) {

                    SelectAllgridStatusUser.SetChecked(gridAllSelected);
                    gridStatusUser.SelectAllRowsOnPage(gridAllSelected);
                }
                else if (e.tab.index == 2) {

                    SelectAllgridStatusFinance.SetChecked(gridAllSelected);
                    gridStatusFinance.SelectAllRowsOnPage(gridAllSelected);
                }
            } else {

            }

            var hidButton = document.getElementById("HiddenButtonSearch");

            var n = "HiddenSearchedTabIdx" + e.tab.index;
            var v = document.getElementById(n).value;
            if (parseInt(v) == 0) {
                hidButton.click();
                loading();
            }
            return false;
        }

        function popProceed() {
            loading();
            var btn = document.getElementById('btnGo');
            btn.click();
            return false;
        }

        function popWorklistReport() {
            
            loading();
            var btn = document.getElementById('btnPrintWorklistReport');
            
            btn.click();
            return false;
        }

        function popPostGo() {
            var btn = document.getElementById('btnPostGo');
            btn.click();
            return false;
        }

        function clearComment() {
            document.getElementById("txtApprovalComment").value = "";
        }
    </script>

  
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
            <asp:HiddenField runat="server" ID="hidProcessID"  ClientIDMode="Static"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="PV List" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 954px;">
                    <tr>
                        <td valign="baseline" style="width: 99px">
                            PV Date
                        </td>
                        <td valign="top" style="width: 74px">
                            <dx:ASPxDateEdit ID="dtPVDateFrom" runat="server" Width="100px" DisplayFormatString="dd MMM yyyy" 
                                EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                                AutoPostBack="false" ClientIDMode="Static" >
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </td>
                        <td style="width: 24px" valign="top">
                            To
                        </td>
                        <td style="width: 73px" valign="top">
                            <dx:ASPxDateEdit ID="dtPVDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                                EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                AutoPostBack="false" ClientIDMode="Static">
                                <ClientSideEvents DateChanged="OnDateChanged" />
                            </dx:ASPxDateEdit>
                        </td>
                        <td valign="baseline" style="width: 120px">
                            Transaction Type
                        </td>
                        <td valign="top" style="width: 77px">

                            <dx:ASPxGridLookup runat="server" ID="ddlTransactionType" ClientInstanceName="ddlTransactionType"
                                SelectionMode="Single" KeyFieldName="Code" TextFormatString="{0}" ClientIDMode="Static"
                                Width="280px" OnLoad="evt_ddlTransactionType_onLoad" CssClass="display-inline-table"
                                IncrementalFilteringMode="Contains" IncrementalFilteringDelay="2000">
                                <GridViewProperties EnableCallBacks="false">
                                    <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                    <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" ShowColumnHeaders="false" />
                                    <SettingsPager PageSize="10"  NumericButtonCount="7" />
                                </GridViewProperties>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="30px" />
                                    <dx:GridViewDataTextColumn Caption="Transaction Name" FieldName="Name" Width="200px">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Standard Wording" FieldName="StandardWording"
                                        Visible="false" Width="200px" />
                                </Columns>
                            </dx:ASPxGridLookup>
                        </td>
                        <td style="width: 20px" valign="top">
                            &nbsp;
                        </td>
                        <td style="width: 80px" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="baseline" style="width: 99px">
                            PV No From
                        </td>
                        <td valign="top" colspan="3">
                            <asp:TextBox ID="TextBoxPVNoFrom" runat="server" Width="90px" MaxLength="9"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp; To&nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:TextBox ID="TextBoxPVNoTo" runat="server" Width="90px" OnTextChanged="PVNoTo_TextChanged"
                                AutoPostBack="true" MaxLength="9" onchange="extractNumber (this, 0, false);"
                                onblur="extractNumber (this, 0, false);" onkeyup="extractNumber (this, 0, false);"
                                onkeypress="return blockNonNumbers (this, event, false, false);"></asp:TextBox>
                        </td>
                        <td valign="baseline" style="width: 120px">
                            Payment Method
                        </td>
                        <td valign="top" style="width: 77px">
                            <asp:DropDownList runat="server" Width="103px" ID="ddlPaymentMethod" AutoPostBack="false"/>
                            
                        </td>
                        <td style="width: 20px" valign="top">
                            &nbsp;
                        </td>
                        <td style="width: 80px" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="baseline" style="width: 99px">
                            Last Status
                        </td>
                        <td valign="top" colspan="3">
                            <dx:ASPxDropDownEdit ID="DropDownListLastStatus" ClientInstanceName="DropDownListLastStatus"
                                runat="server" Width="210px">
                                <DropDownWindowStyle BackColor="#EDEDED" />
                                <DropDownWindowTemplate>
                                    <dx:ASPxListBox runat="server" ID="LastStatusListBox" ClientInstanceName="LastStatusListBox"
                                        SelectionMode="CheckColumn" Width="300px" Height="300px" TextField="Description"
                                        ValueField="Code">
                                        <Border BorderStyle="None" />
                                        <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                        <ClientSideEvents SelectedIndexChanged="function(s,e) {
                                                var selectedItems = s.GetSelectedItems();
                                                var selectedText = GetSelectedListItemsText(selectedItems);
                                                DropDownListLastStatus.SetText(selectedText);
                                        }" />
                                    </dx:ASPxListBox>
                                    <table style="width: 100%" cellspacing="0" cellpadding="4">
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxButton ID="btClearStatusList" AutoPostBack="False" runat="server" Text="Clear"
                                                    CssClass="display-inline-table">
                                                    <ClientSideEvents Click="function(s, e){ LastStatusListBox.UnselectAll(); DropDownListLastStatus.SetText(''); }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btCloseStatusList" AutoPostBack="False" runat="server" Text="Close"
                                                    CssClass="display-inline-table">
                                                    <ClientSideEvents Click="function(s, e){ DropDownListLastStatus.HideDropDown(); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </DropDownWindowTemplate>
                            </dx:ASPxDropDownEdit>
                        </td>
                        <td>
                            Vendor Code
                        </td>
                        <td colspan="3">
                            <div class="lefty">
                            <dx:ASPxGridLookup runat="server" ID="TextBoxVendorCode" ClientInstanceName="TextBoxVendorCode"
                                SelectionMode="Single" KeyFieldName="VENDOR_CD" TextFormatString="{0}" Width="85px"
                                DataSourceID="dxVendor"
                                OnLoad="lkpgridVendorCode_Load" CssClass="display-inline-table" IncrementalFilteringMode="Contains">
                                <GridViewProperties EnableCallBacks="false">
                                    <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                    <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                    <SettingsPager PageSize="7" />
                                </GridViewProperties>
                                <ClientSideEvents TextChanged="function(s,e) { __doPostBack('TextBoxVendorCode',TextBoxVendorCode.GetGridView().GetFocusedRowIndex()); }" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="20px" />
                                    <dx:GridViewDataTextColumn Caption="Vendor Code" FieldName="VENDOR_CD" Width="75px">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Search Terms" FieldName="SEARCH_TERMS" Width="75px">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Vendor Name" FieldName="VENDOR_NAME" Width="150px">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridLookup>
                            <dx:ASPxLabel ID="lblVendorCode" runat="server" CssClass="display-inline-table" OnLoad="VendorCode_TextChanged" />
                            </div>
                            <div class="lefty">
                            <asp:TextBox ID="TextBoxVendorDesc" runat="server" CssClass="disableFields" ReadOnly="true"
                                Width="180px" />
                                </div>
                        </td>
                        
                    </tr>
                </table>
                <asp:Panel runat="server" ID="pBody">
                    <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 954px;">
                        <tr>
                            <td style="width: 99px" valign="baseline">
                                Vendor Group
                            </td>
                            <td valign="top" style="width: 74px">
                                <asp:DropDownList runat="server" Width="103px" ID="ddlVendorGroup" AutoPostBack="false" />
                            </td>
                            <td colspan="2" style="width: 105px" valign="top">
                                &nbsp;
                            </td>
                            <td style="width: 120px">
                                Activity Date
                            </td>
                            <td style="width: 77px">
                                <dx:ASPxDateEdit ID="dtActivityDateFrom" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px"
                                    ClientIDMode="Static"
                                    AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                            <td style="width: 20px">
                                To
                            </td>
                            <td style="width: 80px">
                                <dx:ASPxDateEdit ID="dtActivityDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px"
                                    ClientIDMode="Static" AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline">
                                <asp:Literal ID="litIssuingDivision" runat="server" Text="Issuing Division" />
                            </td>
                            <td colspan="3" valign="top">
                                <dx:ASPxDropDownEdit ID="IssuingDivisionDropDown" ClientInstanceName="IssuingDivisionDropDown"
                                    runat="server" Width="210px">
                                    <DropDownWindowStyle BackColor="#EDEDED" />
                                    <DropDownWindowTemplate>
                                        <dx:ASPxListBox runat="server" ID="IssuingDivisionListBox" ClientInstanceName="IssuingDivisionListBox"
                                            SelectionMode="CheckColumn" Width="300px" Height="300px" TextField="Description"                                            
                                            ValueField="Code" DataSourceID="dsDivision">
                                            <Border BorderStyle="None" />
                                            <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                            <ClientSideEvents SelectedIndexChanged="function(s,e) {
                                                    var selectedItems = s.GetSelectedItems();
                                                    var selectedText = GetSelectedListItemsText(selectedItems);
                                                    IssuingDivisionDropDown.SetText(selectedText);
                                            }" />
                                        </dx:ASPxListBox>
                                        <table style="width: 100%" cellspacing="0" cellpadding="4">
                                            <tr>
                                                <td align="right">
                                                    <dx:ASPxButton ID="btClearDivisionList" AutoPostBack="False" runat="server" Text="Clear"
                                                        CssClass="display-inline-table">
                                                        <ClientSideEvents Click="function(s, e){ IssuingDivisionListBox.UnselectAll(); IssuingDivisionDropDown.SetText(''); }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btCloseDivisionList" AutoPostBack="False" runat="server" Text="Close"
                                                        CssClass="display-inline-table">
                                                        <ClientSideEvents Click="function(s, e){ IssuingDivisionDropDown.HideDropDown(); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </DropDownWindowTemplate>
                                </dx:ASPxDropDownEdit>


                                <asp:DropDownList ID="DropDownListIssuingDivision" runat="server" Width="240px" CssClass="hidden"/>
                                
                            </td>
                            <td valign="baseline">
                                Submit Date
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtSubmitDateFrom" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" ClientIDMode="Static"
                                    AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                To
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtSubmitDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" ClientIDMode="Static"
                                    AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline">
                                PV Type
                            </td>
                            <td valign="top" colspan="3">
                                <asp:DropDownList ID="DropDownListPVType" runat="server" Width="103px">
                                </asp:DropDownList>
                            </td>
                             <td valign="top">
                                Planning Payment Date
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="PlanningPaymentDateFrom" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    ClientIDMode="Static" AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                To
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="PlanningPaymentDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    ClientIDMode="Static"
                                    AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Workflow Status
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="DropDownListWorkflowStatus" runat="server" Width="103px" />
                                
                            </td>
                            <td valign="top">   
                                Paid Date
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtPaidDateFrom" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    AutoPostBack="false" ClientIDMode="Static">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                To
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtPaidDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    ClientIDMode="Static"
                                    AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Settlement Status
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="DropDownListSettlementStatus" runat="server" Width="103px">
                                </asp:DropDownList>
                            </td>
                           
                            <td valign="baseline">
                                Posting Date
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtPostingDateFrom" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    ClientIDMode="Static" AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                To
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtPostingDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    ClientIDMode="Static" AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                Created by
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="CreatedByEdit" runat="server" Width="103px" MaxLength="200" ClientIDMode="Static" />
                            </td>

                            <td valign="baseline">
                                Verified Date
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtVerifiedDateFrom" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    ClientIDMode="Static" AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                            <td>
                                To
                            </td>
                            <td>
                                <dx:ASPxDateEdit ID="dtVerifiedDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                                    ClientIDMode="Static" AutoPostBack="false">
                                    <ClientSideEvents DateChanged="OnDateChanged" />
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>

                         <tr>
                           <td valign="top">
                           Current PIC
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="NextApproverEdit" runat="server" Width="103px" MaxLength="200" ClientIDMode="Static" />
                            </td>
                            <td valign="baseline">
                                 Notice
                            </td>
                            <td colspan="3" valign="top">
                               <asp:DropDownList ID="DropDownListNotice" runat="server" Width="103px"/>
                            </td>
                            
                        </tr>

                        <tr>
                           <td valign="top" colspan="2">
                                <asp:CheckBox ID="CancelFlag" runat="server" Text="&nbsp;&nbsp;Canceled" />
                            </td>
                            <td valign="top" colspan="2">
                                <asp:CheckBox ID="DeleteFlag" runat="server" Text="&nbsp;&nbsp;Deleted" />
                            </td>
                            
                            <td valign="baseline">
                               Hold
                            </td>
                            <td colspan="3" valign="top">
                                <asp:DropDownList ID="DropDownListHoldFlag" runat="server" Width="103px"/>
                            </td>

                            
                        </tr>
                    </table>
                </asp:Panel>
                <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 954px;">
                    <tr>
                        <td align="left">
                            <asp:LinkButton ID="linkShowDetail" runat="server">
                                <asp:Label ID="lblText" runat="server" />
                            </asp:LinkButton></td><td align="right">
                            <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="Search"
                                OnClientClick="loading()" />&nbsp;&nbsp;&nbsp;<asp:Button ID="ButtonClear" runat="server"
                                    OnClick="ButtonClear_Click" Text="Clear" />
                        </td>
                    </tr>
                </table>
                <cc1:CollapsiblePanelExtender ID="collapsiblePanelExtCriteria" runat="server" TargetControlID="pBody"
                    CollapseControlID="linkShowDetail" ExpandControlID="linkShowDetail" Collapsed="true"
                    TextLabelID="lblText" CollapsedText="See More Details Criteria" ExpandedText="Hide Details Criteria"
                    CollapsedSize="0">
                </cc1:CollapsiblePanelExtender>
            </div>
            <div class="rowbtn" style="text-align: right">
                <asp:HiddenField ID="HiddenPVNo" runat="server" ClientIDMode="Inherit" />
                <asp:HiddenField ID="HiddenPVYear" runat="server" ClientIDMode="Inherit" />
                <asp:Button ID="ButtonNewPV" runat="server" Text="New PV" OnClientClick="openWin('PVFormList.aspx','ELVIS_PVFormList')" />
                &nbsp; <asp:Button ID="ButtonEdit" runat="server" Text="Edit" OnClick="ButtonEdit_Click" />
                &nbsp; <asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upnContentData">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonSearch" />
            <asp:AsyncPostBackTrigger ControlID="ButtonDelete" />
            <asp:PostBackTrigger ControlID="ButtonDownload" />
            <asp:PostBackTrigger ControlID="ButtonPrintCover" />
            <asp:PostBackTrigger ControlID="ButtonPrintTicket" />
            <asp:PostBackTrigger ControlID="ButtonPostSAP" />
            <asp:PostBackTrigger ControlID="ButtonCheckApp" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="HiddenButtonSearch" runat="server" Style="display: none;" OnClick="pvListTabs_ActiveTabChanged" ClientIDMode="Static" OnClientClick="loading();"/>
            <asp:HiddenField ID="HiddenSearchedTabIdx0" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HiddenSearchedTabIdx1" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HiddenSearchedTabIdx2" runat="server" ClientIDMode="Static" />
            
            <asp:HiddenField ID="HiddenCanSelectAll" runat="server" ClientIDMode="Static" />
            <dx:ASPxPageControl 
                ID="PVListTabs" runat="server" ActiveTabIndex="0" EnableHierarchyRecreation="True"
                Height="170px" Width="980px" ClientInstanceName="PVListTabs" 
                AutoPostBack="false">
                <ClientSideEvents 
                    Init="function(s, e) { InitPageControl(); }" 
                    ActiveTabChanged="function(s, e) { ActiveTabChanged(e); }">
                </ClientSideEvents>
                <TabPages>
                    <dx:TabPage Text="General Info" Name="GeneralInfo">
                        <ContentCollection>
                            <dx:ContentControl ID="GeneralDataCC" runat="server">
                                <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="980px" 
                                    AutoGenerateColumns="False"
                                    ClientInstanceName="gridGeneralInfo" 
                                    OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                                    KeyFieldName="ROW_NUMBER" 
                                    OnCustomCallback="grid_CustomCallback" 
                                    EnableCallBacks="false"
                                    Styles-AlternatingRow-CssClass="even">
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                            CellStyle-VerticalAlign="Middle" >
                                            <EditButton Visible="false" Text=" ">
                                            </EditButton>
                                            <UpdateButton Visible="false" Text=" ">
                                            </UpdateButton>
                                            <NewButton Visible="false" Text=" ">
                                            </NewButton>
                                            <CancelButton Visible="false" Text=" ">
                                            </CancelButton>
                                            <CellStyle VerticalAlign="Middle">
                                            </CellStyle>
                                            <HeaderTemplate>
                                                <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientInstanceName="SelectAllCheckBox"
                                                    ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked(); }"
                                                    CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                                </dx:ASPxCheckBox>
                                            </HeaderTemplate>
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Sett Stat" FieldName="SETTLEMENT_STATUS" Width="35px"
                                            VisibleIndex="1">
                                            <DataItemTemplate>
                                                <div class="dynamicDiv" style='background-color: <%# Status_Color(Eval("SETTLEMENT_STATUS")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="WF Stat" FieldName="WORKFLOW_STATUS" Width="35px"
                                            VisibleIndex="2">
                                            <DataItemTemplate>
                                                <div class="dynamicDiv" style='background-color: <%# Status_Color(Eval("WORKFLOW_STATUS")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="PV No" FieldName="PV_NO" Width="100px"
                                            VisibleIndex="3" CellStyle-HorizontalAlign="Left" >
                                            <DataItemTemplate>
                                                <asp:HyperLink runat="server" ID="hypDetail" Text="Detail" NavigateUrl="#"/>
                                                    <sub class="notices">
                                                        <asp:Literal runat="server" ID="litGridNotices" Text='(n)' />
                                                    </sub>
                                                    <span class="holdflag">
                                                        <asp:Literal runat="server" ID="litGridHoldFlag" Text='*' Visible="false" />
                                                    </span>
                                                <asp:HiddenField runat="server" ID="hidGridPVYear" Value='<%# Bind("PV_YEAR")%>' />
                                                <asp:HiddenField runat="server" ID="hidGridStatusCd" Value='<%# Bind("STATUS_CD")%>' />
                                                <asp:HiddenField runat="server" ID="hidGridPaymentMethod" Value='<%# Bind("PAY_METHOD_CD")%>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Vendor Name" FieldName="VENDOR_NAME" VisibleIndex="4"
                                           Width="210px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn 
                                            Caption="Transaction Type" FieldName="TRANSACTION_NAME"
                                            VisibleIndex="5" Width="140px"></dx:GridViewDataTextColumn>

                                         <dx:GridViewDataTextColumn Caption="PV Type" FieldName="PV_TYPE_NAME" 
                                            VisibleIndex="6" Width="75px" />

                                        <dx:GridViewDataDateColumn Caption="Planning Payment Date" FieldName="PLANNING_PAYMENT_DATE"
                                            VisibleIndex="6">
                                            <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                            </PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
        
                                        <dx:GridViewDataTextColumn
                                            Caption="Total Amount (IDR)" FieldName="TOTAL_AMOUNT_IDR"
                                            VisibleIndex="7" Width="100px">
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                           <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridTotalAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("TOTAL_AMOUNT_IDR")) %>' />
                                                
                                                <asp:LinkButton runat="server" 
                                                    ID="linkSeeAllCurrency" 
                                                    OnClick="LoadGridPopUpOtherCurr"
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("TOTAL_AMOUNT_IDR")) %>'
                                                    CommandName="TotalAmount" Style="font-size: 8pt !important;" />
                                           </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="OTHER_CURRENCY" Visible="false"/>
                                        <dx:GridViewDataTextColumn Caption="Last Status" 
                                            FieldName="LAST_STATUS" VisibleIndex="8"
                                            Width="105px"/>
                                        <dx:GridViewDataTextColumn FieldName="NEXT_APPROVER_NAME" VisibleIndex="10" Caption="Current PIC" />
                                        
                                        <dx:GridViewDataTextColumn 
                                            Caption="Total Amount (Original Curr)" VisibleIndex="13" Visible="false"
                                            Width="110px">
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" ID="linkSeeTotalAmount" OnClick="LoadGridPopUpTotalAmountCurr"
                                                    Text="See Detail" CommandName="TotalAmount" Style="font-size: 8pt!important;" />

                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn Caption="PV Date" 
                                            FieldName="PV_DATE" VisibleIndex="14"><PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                            </PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn Caption="Submit Date" 
                                            FieldName="SUBMIT_DATE" VisibleIndex="15"><PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                            </PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn Caption="Posting Date" 
                                            FieldName="POSTING_DATE" VisibleIndex="16"><PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                            </PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn Caption="Paid Date" 
                                            FieldName="PAID_DATE" VisibleIndex="17"><PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                            </PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn 
                                            Caption="Issuing Division" FieldName="DIVISION_NAME" VisibleIndex="18"></dx:GridViewDataTextColumn>
                                       
                                        <dx:GridViewDataTextColumn Caption="Suspense No" FieldName="SUSPENSE_NO" VisibleIndex="19">
                                            <DataItemTemplate>
                                                <a href="javascript:void(0)" 
                                                    onclick="javascript:openWin('PVFormList.aspx?mode=view&pv_no=<%# Eval("SUSPENSE_NO")%>&pv_year=<%# Eval("SUSPENSE_YEAR")%>','ELVIS_PVFormList')"
                                                    class="smallFont">
                                                    <asp:Literal runat="server" ID="litGridSuspenseNo" Text='<%# Bind("SUSPENSE_NO")%>'></asp:Literal></a>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        
                                        <dx:GridViewDataTextColumn Caption="PV Sett No" FieldName="SET_NO_PV" VisibleIndex="20">
                                            <DataItemTemplate>
                                                <a href="javascript:void(0)" 
                                                   onclick="javascript:openWin('PVFormList.aspx?mode=view&pv_no=<%# Eval("SET_NO_PV")%>&pv_year=<%# Eval("PV_YEAR")%>','ELVIS_PVFormList')"
                                                    class="smallFont">
                                                    <asp:Literal runat="server" ID="litGridPVSettNo" Text='<%# Bind("SET_NO_PV")%>' /></a>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="RV Sett No" FieldName="SET_NO_RV" VisibleIndex="23">
                                            <DataItemTemplate>
                                                <a class="smallFont" 
                                                    href="javascript:void(0)" 
                                                    onclick="javascript:openWin('../40RVFormList/RVFormList.aspx?mode=view&rv_no=<%# Eval("SET_NO_RV")%>&rv_year=<%# Eval("PV_YEAR")%>','ELVIS_RVFormList')">
                                                    <asp:Literal runat="server" ID="litGridRVSettNo" Text='<%# Bind("SET_NO_RV")%>'></asp:Literal></a>
                                            </DataItemTemplate></dx:GridViewDataTextColumn>
                                        
                                        <dx:GridViewDataTextColumn Caption="Vendor Code" FieldName="VENDOR_CD" VisibleIndex="24"/>
                                        
                                        <dx:GridViewDataTextColumn Caption="Payment Method" FieldName="PAY_METHOD_NAME" VisibleIndex="26" Width="130px"/>
                                        
                                        <dx:GridViewDataTextColumn Caption="Budget No"  
                                            FieldName="BUDGET_NO" VisibleIndex="27"
                                            Width="150px">
                                        </dx:GridViewDataTextColumn>
                                        
                                        <dx:GridViewDataTextColumn Caption="Bank Type" FieldName="BANK_TYPE" VisibleIndex="28" Width="60px"/>
                                        
                                        <dx:GridViewDataTextColumn Caption="SAP Doc No" FieldName="SAP_DOC_NO" VisibleIndex="29">
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" ID="linkSapDocNo" OnClick="LoadGridPopUpSapDocNo"
                                                    Text='<%# Eval("SAP_DOC_NO") %>' CssClass="smallFont"></asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Caption="Attachment" FieldName="ATTACHMENT_QUANTITY" VisibleIndex="30" Width="80">
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" ID="linkAttachmentQuantity" OnClick="LoadGridPopUpAttachment"
                                                    Text='<%# Eval("ATTACHMENT_QUANTITY") %>' CssClass="smallFont"></asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                                    
                                         <dx:GridViewBandColumn Caption="Activity Date" VisibleIndex="31">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="From" FieldName="ACTIVITY_DATE_FROM" VisibleIndex="0">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="To" FieldName="ACTIVITY_DATE_TO" VisibleIndex="1">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataTextColumn Caption="Hold By" FieldName="HOLD_BY_NAME" Width="80px"
                                            VisibleIndex="32" />
                                        <dx:GridViewDataTextColumn ShowInCustomizationForm="True" Caption="Version" FieldName="VERSION"
                                            VisibleIndex="33" Width="55px">
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" ID="linkVersion" OnClick="LoadGridPopUpVersion" 
                                                    Text='<%# Eval("VERSION") %>'
                                                    CssClass="smallFont">
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataCheckColumn FieldName="CANCEL_FLAG" Caption="Canceled" VisibleIndex="34" Width="80px">
                                            <PropertiesCheckEdit ValueType="System.Int32"
                                                ValueChecked="1"
                                                ValueUnchecked="0" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataCheckColumn FieldName="DELETED" Caption="Deleted" VisibleIndex="35" Width="80px">
                                            <PropertiesCheckEdit ValueType="System.Int32"
                                                ValueChecked="1"
                                                ValueUnchecked="0" />
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataTextColumn Visible="false" FieldName="PV_TYPE_CD" />
                                        <dx:GridViewDataCheckColumn Caption="Hard Copy Verified" FieldName="COUNTER_FLAG" VisibleIndex="36" Width="80px">
                                            <PropertiesCheckEdit ValueType="System.Int32" ValueChecked="1" />
                                        </dx:GridViewDataCheckColumn> 
                                        <dx:GridViewDataCheckColumn Caption="Ticked Printed" FieldName="PRINT_TICKET_FLAG" VisibleIndex="37" Width="80px">
                                            <PropertiesCheckEdit ValueType="System.Int32" ValueChecked="1" />
                                        </dx:GridViewDataCheckColumn> 

                                        <dx:GridViewDataTextColumn Caption="Ticket Printed By" FieldName="PRINT_TICKET_BY" VisibleIndex="38"
                                            Width="130px" />
                                        <dx:GridViewDataDateColumn Caption="Verified Date" FieldName="SUBMIT_HC_DOC_DATE" VisibleIndex="39" Width="110px">
                                              <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}"/>
                                        </dx:GridViewDataDateColumn>

                                        <dx:GridViewBandColumn Caption="Created" VisibleIndex="40">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Date" FieldName="CREATED_DATE" VisibleIndex="0"
                                                    Width="130px" Settings-SortMode="Value" Settings-AllowSort="True" SortIndex="0" SortOrder="Descending">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Changed" VisibleIndex="41">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Date" FieldName="CHANGED_DATE" VisibleIndex="0"
                                                    Width="130px">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="By" FieldName="CHANGED_BY" VisibleIndex="1">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        
                                    </Columns>
                                    <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                                    <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                                    <SettingsLoadingPanel ImagePosition="Top" />
                                    <Styles>
                                        <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                        </Header>
                                        <AlternatingRow CssClass="even">
                                        </AlternatingRow>
                                        <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                            <Paddings PaddingTop="1px" PaddingBottom="1px" />
                                        </Cell>
                                        <Row CssClass="doubleRow" />
                                    </Styles>
                                    <Settings ShowStatusBar="Visible" />
                                    <SettingsPager Visible="false" />
                                    <Templates>
                                        <StatusBar>
                                            <div style="text-align: left;">
                                                Records per page: <select onchange="gridGeneralInfo.PerformCallback(this.value);">
                                                    <option value="5" <%# WriteSelectedIndexInfo(5) %>>5</option>
                                                    <option value="10" <%# WriteSelectedIndexInfo(10) %>>10</option>
                                                    <option value="15" <%# WriteSelectedIndexInfo(15) %>>15</option>
                                                    <option value="20" <%# WriteSelectedIndexInfo(20) %>>20</option>
                                                    <option value="25" <%# WriteSelectedIndexInfo(25) %>>25</option>
                                                    <option value="50" <%# WriteSelectedIndexInfo(50) %>>50</option>
                                                    <option value="100" <%# WriteSelectedIndexInfo(100) %>>100</option>
                                                    <option value="150" <%# WriteSelectedIndexInfo(150) %>>150</option>
                                                    <option value="200" <%# WriteSelectedIndexInfo(200) %>>200</option>
                                                    <option value="250" <%# WriteSelectedIndexInfo(250) %>>250</option>
                                                    <option value="500" <%# WriteSelectedIndexInfo(500) %>>500</option>
                                                </select>&nbsp; <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">&lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a> &nbsp;
                                                Page <input type="text" onchange="if(!gridGeneralInfo.InCallback()) gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                                    onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                                    value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                                    style="width: 20px" />of <%# gridGeneralInfo.PageCount%>&nbsp; <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%><a
                                                    title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp; <a title="Last"
                                                        href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; </div></StatusBar></Templates></dx:ASPxGridView></dx:ContentControl></ContentCollection></dx:TabPage><dx:TabPage Text="Status (User)" Name="StatusUser">
                        <ContentCollection>
                            <dx:ContentControl ID="StatusUserCC" runat="server" Width="970px">
                                <dx:ASPxGridView ID="gridStatusUser" runat="server" Width="100%" AutoGenerateColumns="False"
                                    KeyFieldName="ROW_NUMBER" EnableCallBacks="false" ClientInstanceName="gridStatusUser"
                                    Styles-AlternatingRow-CssClass="even" OnCustomCallback="grid_CustomCallback"
                                    OnHtmlDataCellPrepared="gridStatusUser_HtmlDataCellPrepared">
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                            CellStyle-VerticalAlign="Middle">
                                            <EditButton Visible="false" Text=" ">
                                            </EditButton>
                                            <UpdateButton Visible="false" Text=" ">
                                            </UpdateButton>
                                            <NewButton Visible="false" Text=" ">
                                            </NewButton>
                                            <CancelButton Visible="false" Text=" ">
                                            </CancelButton>
                                            <CellStyle VerticalAlign="Middle">
                                            </CellStyle>
                                            <HeaderTemplate>
                                                <dx:ASPxCheckBox ID="SelectAllgridStatusUser" runat="server" ClientInstanceName="SelectAllgridStatusUser"
                                                    ClientSideEvents-CheckedChanged="function(s, e) { gridStatusUser.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked();}"
                                                    CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                                </dx:ASPxCheckBox>
                                            </HeaderTemplate>
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="PV No" VisibleIndex="2" FieldName="PV_NO" Width="70px">
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" ID="litGridPVNo" Text='<%# Bind("PV_NO") %>'></asp:Literal>

                                                <asp:HiddenField runat="server" ID="hidGridPVYear" Value='<%# Bind("PV_YEAR")%>'/>

                                                <asp:HiddenField runat="server" ID="hidGridStatusCd" Value='<%# Bind("STATUS_CD")%>'/>
                                                
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="PV Year" VisibleIndex="3" FieldName="PV_YEAR" Width="42px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="Registered" VisibleIndex="4">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="0" FieldName="REG_ACTUAL_DT" Width="85px">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by SH" VisibleIndex="5" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_SH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_SH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_SH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_SH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_SH"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by DpH" VisibleIndex="6" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_DPH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_DPH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_DPH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_DPH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_DPH"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by DH" VisibleIndex="7" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_DH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_DH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_DH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_DH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_DH"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>                                        
                                        <dx:GridViewBandColumn Caption="Approved by Director" VisibleIndex="9" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_DIRECTOR">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_DIRECTOR">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_DIRECTOR">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_DIRECTOR">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_DIRECTOR"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by VP" VisibleIndex="10" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_VP">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_VP">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_VP">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_VP">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_VP"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataDateColumn FieldName="PV_DATE" Visible="false">
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="DIVISION_NAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="VENDOR_CD" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="VENDOR_NAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TRANSACTION_NAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTAL_AMOUNT_IDR" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DIVISION_ID" Visible ="false" />
                                        <dx:GridViewDataDateColumn FieldName="CREATED_DATE" Visible="false" 
                                            SortIndex="0" SortOrder="Descending" Settings-SortMode="Value" />
                                    </Columns>
                                    <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                                    <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                                    <SettingsLoadingPanel ImagePosition="Top" />
                                    <Styles>
                                        <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                        </Header>
                                        <AlternatingRow CssClass="even">
                                        </AlternatingRow>
                                        <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                        </Cell>
                                        <Row CssClass="doubleRow" />
                                    </Styles>
                                    <Settings ShowStatusBar="Visible" />
                                    <SettingsPager Visible="false" />
                                    <Templates>
                                        <StatusBar>
                                            <div style="text-align: left;">
                                                Records per page: <select onchange="gridStatusUser.PerformCallback(this.value);">
                                                    <option value="5" <%# WriteSelectedIndexUser(5) %>>5</option>
                                                    <option value="10" <%# WriteSelectedIndexUser(10) %>>10</option>
                                                    <option value="15" <%# WriteSelectedIndexUser(15) %>>15</option>
                                                    <option value="20" <%# WriteSelectedIndexUser(20) %>>20</option>
                                                    <option value="25" <%# WriteSelectedIndexUser(25) %>>25</option>
                                                    <option value="50" <%# WriteSelectedIndexUser(50) %>>50</option>
                                                    <option value="100" <%# WriteSelectedIndexUser(100) %>>100</option>
                                                    <option value="150" <%# WriteSelectedIndexUser(150) %>>150</option>
                                                    <option value="200" <%# WriteSelectedIndexUser(200) %>>200</option>
                                                    <option value="250" <%# WriteSelectedIndexUser(250) %>>250</option>
                                                    <option value="500" <%# WriteSelectedIndexUser(500) %>>500</option>
                                                </select>&nbsp; <a title="First" href="JavaScript:gridStatusUser.GotoPage(0);">&lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridStatusUser.PrevPage();">&lt;</a> &nbsp;
                                                Page <input type="text" onchange="if(!gridStatusUser.InCallback()) gridStatusUser.GotoPage(parseInt(this.value, 10) - 1)"
                                                    onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridStatusUser.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                                    value="<%# (gridStatusUser.PageIndex >= 0)? gridStatusUser.PageIndex + 1 : 1 %>"
                                                    style="width: 20px" />of <%# gridStatusUser.PageCount%>&nbsp; <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%><a
                                                    title="Next" href="JavaScript:gridStatusUser.NextPage();">&gt;</a> &nbsp; <a title="Last"
                                                        href="JavaScript:gridStatusUser.GotoPage(<%# gridStatusUser.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; </div></StatusBar></Templates></dx:ASPxGridView></dx:ContentControl></ContentCollection></dx:TabPage><dx:TabPage Text="Status (Finance)" Name="StatusFinance">
                        <ContentCollection>
                            <dx:ContentControl ID="StatusFinanceCC" runat="server" Width="970px">
                                <dx:ASPxGridView ID="gridStatusFinance" runat="server" Width="100%" AutoGenerateColumns="False"
                                    KeyFieldName="ROW_NUMBER" EnableCallBacks="false" OnHtmlDataCellPrepared="gridStatusFinance_HtmlDataCellPrepared"
                                    OnCustomCallback="grid_CustomCallback" Styles-AlternatingRow-CssClass="even"
                                    ClientInstanceName="gridStatusFinance">
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                            CellStyle-VerticalAlign="Middle">
                                            <EditButton Visible="false" Text=" ">
                                            </EditButton>
                                            <UpdateButton Visible="false" Text=" ">
                                            </UpdateButton>
                                            <NewButton Visible="false" Text=" ">
                                            </NewButton>
                                            <CancelButton Visible="false" Text=" ">
                                            </CancelButton>
                                            <CellStyle VerticalAlign="Middle">
                                            </CellStyle>
                                            <HeaderTemplate>
                                                <dx:ASPxCheckBox ID="SelectAllgridStatusFinance" runat="server" ClientInstanceName="SelectAllgridStatusFinance"
                                                    ClientSideEvents-CheckedChanged="function(s, e) { gridStatusFinance.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked();}"
                                                    CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                                </dx:ASPxCheckBox>
                                            </HeaderTemplate>
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="PV No" VisibleIndex="2" FieldName="PV_NO" Width="70px">
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" ID="litGridPVNo" Text='<%# Bind("PV_NO") %>'/>

                                                <asp:HiddenField runat="server" ID="hidGridPVYear" Value='<%# Bind("PV_YEAR")%>'/>
                                                <asp:HiddenField runat="server" ID="hidGridStatusCd" Value='<%# Bind("STATUS_CD")%>'/>                                                
                                                <asp:HiddenField runat="server" ID="hidGridPaymentMethod" Value='<%# Bind("PAY_METHOD_CD")%>'/>
                                                
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="PV Year" VisibleIndex="3" FieldName="PV_YEAR" Width="42px">
                                        </dx:GridViewDataTextColumn>
                                        

                                        <dx:GridViewBandColumn Caption="Verified by Budget" VisibleIndex="5" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_VERIFIED_BUDGET">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_VERIFIED_BUDGET">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_VERIFIED_BUDGET">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_VERIFIED_BUDGET">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_VERIFIED_BUDGET"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>

                                        <dx:GridViewBandColumn Caption="Verified by Costing" VisibleIndex="5" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_VERIFIED_COSTING">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_VERIFIED_COSTING">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_VERIFIED_COSTING">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_VERIFIED_COSTING">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_VERIFIED_COSTING"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>


                                        <dx:GridViewBandColumn Caption="Verified by FD Staff" VisibleIndex="6" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_VERIFIED_FINANCE">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_VERIFIED_FINANCE">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_VERIFIED_FINANCE">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_VERIFIED_FINANCE">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_VERIFIED_FINANCE"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by SH" VisibleIndex="7" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_APPROVED_FINANCE_SH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd/MM/yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_APPROVED_FINANCE_SH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd/MM/yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_APPROVED_FINANCE_SH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_APPROVED_FINANCE_SH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_APPROVED_FINANCE_SH"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by DpH" VisibleIndex="8" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_APPROVED_FINANCE_DPH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_APPROVED_FINANCE_DPH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_APPROVED_FINANCE_DPH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_APPROVED_FINANCE_DPH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_APPROVED_FINANCE_DPH"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by DH" VisibleIndex="9" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_APPROVED_FINANCE_DH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_APPROVED_FINANCE_DH">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_APPROVED_FINANCE_DH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_APPROVED_FINANCE_DH">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_APPROVED_FINANCE_DH"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Approved by Director" VisibleIndex="10" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_APPROVED_FINANCE_DIRECTOR">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_APPROVED_FINANCE_DIRECTOR">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_APPROVED_FINANCE_DIRECTOR">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_APPROVED_FINANCE_DIRECTOR">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_APPROVED_FINANCE_DIRECTOR"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Post to SAP" VisibleIndex="11" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_POSTED_TO_SAP">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_POSTED_TO_SAP">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_POSTED_TO_SAP">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_POSTED_TO_SAP">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_POSTED_TO_SAP"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Paid By Cashier" VisibleIndex="12" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_PAID">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_PAID">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_PAID">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_PAID">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_PAID"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="Checked By Acc" VisibleIndex="13" HeaderStyle-Wrap="False">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="Plan" VisibleIndex="0" FieldName="PLAN_DT_CHECKED">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="Actual" VisibleIndex="1" FieldName="ACTUAL_DT_CHECKED">
                                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                    </PropertiesDateEdit>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="Pln L/T" VisibleIndex="2" FieldName="PLAN_LT_CHECKED">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Act L/T" VisibleIndex="3" FieldName="ACTUAL_LT_CHECKED">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Skip Flag" VisibleIndex="4" FieldName="SKIP_FLAG_CHECKED"
                                                    Visible="false">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle Wrap="False" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataDateColumn FieldName="PV_DATE" Visible="false">
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataTextColumn FieldName="DIVISION_NAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="VENDOR_CD" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="VENDOR_NAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TRANSACTION_NAME" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTAL_AMOUNT_IDR" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataDateColumn FieldName="CREATED_DATE" 
                                            Visible="false" 
                                            SortIndex="0" SortOrder="Descending" 
                                            Settings-SortMode="Value" />
                                    </Columns>
                                    <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                                    <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                                    <SettingsLoadingPanel ImagePosition="Top" />
                                    <Styles>
                                        <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                        </Header>
                                        <AlternatingRow CssClass="even">
                                        </AlternatingRow>
                                        <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                        </Cell>
                                        <Row CssClass="doubleRow" />
                                    </Styles>
                                    <Settings ShowStatusBar="Visible" />
                                    <SettingsPager Visible="false" />
                                    <Templates>
                                        <StatusBar>
                                            <div style="text-align: left;">
                                                Records per page: <select onchange="gridStatusFinance.PerformCallback(this.value);">
                                                    <option value="5" <%# WriteSelectedIndexFinance(5) %>>5</option>
                                                    <option value="10" <%# WriteSelectedIndexFinance(10) %>>10</option>
                                                    <option value="15" <%# WriteSelectedIndexFinance(15) %>>15</option>
                                                    <option value="20" <%# WriteSelectedIndexFinance(20) %>>20</option>
                                                    <option value="25" <%# WriteSelectedIndexFinance(25) %>>25</option>
                                                    <option value="50" <%# WriteSelectedIndexFinance(50) %>>50</option>
                                                    <option value="100" <%# WriteSelectedIndexFinance(100) %>>100</option>
                                                    <option value="150" <%# WriteSelectedIndexFinance(150) %>>150</option>
                                                    <option value="200" <%# WriteSelectedIndexFinance(200) %>>200</option>
                                                    <option value="250" <%# WriteSelectedIndexFinance(250) %>>250</option>
                                                    <option value="500" <%# WriteSelectedIndexFinance(500) %>>500</option>
                                                </select>&nbsp; <a title="First" href="JavaScript:gridStatusFinance.GotoPage(0);">&lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridStatusFinance.PrevPage();">&lt;</a> &nbsp; Page <input type="text" onchange="if(!gridStatusFinance.InCallback()) gridStatusFinance.GotoPage(parseInt(this.value, 10) - 1)"
                                                    onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridStatusFinance.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                                    value="<%# (gridStatusFinance.PageIndex >= 0)? gridStatusFinance.PageIndex + 1 : 1 %>"
                                                    style="width: 20px" />of <%# gridStatusFinance.PageCount%>&nbsp; <%# (gridStatusFinance.VisibleRowCount > 1) ? "(" + gridStatusFinance.VisibleRowCount + " Items)&nbsp;" : ""%><a
                                                    title="Next" href="JavaScript:gridStatusFinance.NextPage();">&gt;</a> &nbsp; <a title="Last" href="JavaScript:gridStatusFinance.GotoPage(<%# gridStatusFinance.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; </div></StatusBar></Templates></dx:ASPxGridView></dx:ContentControl></ContentCollection></dx:TabPage></TabPages></dx:ASPxPageControl><dx:LinqServerModeDataSource ID="LinqPVList" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                TableName="vw_PV_List" OnSelecting="LinqPVList_Selecting"/>
            <dx:LinqServerModeDataSource ID="LinqPVListStatusUser" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                TableName="vw_PV_List_Status_User" OnSelecting="LinqPVListStatusUser_Selecting" />
            <dx:LinqServerModeDataSource ID="LinqPVListStatusFinance" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                TableName="vw_PV_List_Status_Finance" OnSelecting="LinqPVListStatusFinance_Selecting"  />
            <dx:LinqServerModeDataSource ID="dxVendor" runat="server" OnSelecting="dxVendor_Selecting" />
            <asp:LinqDataSource ID="dsReason" runat="server" 
                ContextTypeName="DataLayer.Model.ELVIS_DBEntities" 
                TableName="TB_M_DTM_REASON" OnSelecting="dsDirectToMe_Selecting"/>
            <asp:LinqDataSource ID="dsDivision" runat="server" OnSelecting="dsDivision_Selecting"/>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="rowbtn">
        <div class="btnleft" style="float: left;">
            <asp:UpdatePanel runat="server" ID="upnContentButton">
                <Triggers>
                </Triggers>
                <ContentTemplate>
                    <asp:Button runat="server" ID="ButtonDownload" Text="Download" OnClick="ButtonDownload_Click" CssClass="xlongButton" SkinID="xlongButton"/>
                    <asp:Literal runat="server" ID="gapHistory">&nbsp;</asp:Literal><asp:Button runat="server" ID="ButtonHistory" Text="History" OnClick="ButtonHistory_Click" />
                    <asp:Literal runat="server" ID="gapPrintCover">&nbsp;</asp:Literal><asp:Button runat="server" ID="ButtonPrintCover" Text="Print Cover" OnClick="ButtonPrintCover_Click"
                        CssClass="xlongButton" SkinID="xlongButton" />
                    <asp:Literal runat="server" ID="gapPrintTicket">&nbsp;</asp:Literal><asp:Button runat="server" ID="ButtonPrintTicket" Text="Print Ticket" OnClick="ButtonPrintTicket_Click"
                        CssClass="xlongButton" SkinID="xlongButton" />
                    <asp:Literal runat="server" ID="gapPostSAP">&nbsp;</asp:Literal><asp:Button runat="server" ID="ButtonPostSAP" Text="Post to SAP" OnClick="ButtonPostSAP_Click"
                        CssClass="xlongButton" SkinID="xlongButton" OnClientClick="loading()" />
                    <asp:Literal runat="server" ID="gapCheckApp">&nbsp;</asp:Literal><asp:Button runat="server" ID="ButtonCheckApp" Text="Checked by Acc" OnClick="ButtonCheckAcc_Click"
                        CssClass="xlongButton" SkinID="xlongButton" OnClientClick="loading()" />
                    <asp:Literal runat="server" ID="gapDirectToMe">&nbsp;</asp:Literal><asp:Button runat="server" ID="ButtonDirectToMe" Text="Direct To Me" OnClick="ButtonDirectToMe_Click"
                        CssClass="xlongButton" SkinID="xlongButton" OnClientClick="loading()" />
                    <asp:Literal runat="server" ID="gapGenerateEntertain">&nbsp;</asp:Literal><asp:Button runat="server" ID="ButtonGenerateEntertain" Text="Generate Entertain"
                        OnClick="ButtonGenerateEntertain_Click" CssClass="xlongButton" SkinID="xlongButton"
                        OnClientClick="loading()" />
                    <asp:Literal runat="server" ID="gapWorklistReport" >&nbsp;</asp:Literal>
                    <asp:Button runat="server" ID="ButtonPrintWorklist" Text="Download Report"
                        OnClick="ButtonPrintWorklist_Click" CssClass="xlongButton" SkinID="xlongButton" />                        
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        
        <div class="closeWrap">
            <asp:Button runat="server" ID="ButtonClose" Text="Close" 
                OnClick="ButtonClose_Click"
                OnClientClick="closeWin()" />
        </div>
        
    </div>
    <div class="rowbtn">
        <asp:UpdatePanel runat="server" ID="updPnlLegend">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlLegendInfo" GroupingText="Legend" Width="983px">
                    <div style="float: left; width: 50%">
                        &nbsp;&nbsp;&nbsp;Settlement Status : <table>
                            <tr>
                                <td>
                                    &nbsp; </td><td style="background-color: #ff0000; width: 20px">
                                    &nbsp; </td><td style="width: 75px">
                                    Outstanding </td><td>
                                    &nbsp; </td><td style="background-color: #ffff00; width: 20px">
                                    &nbsp; </td><td style="width: 50px">
                                    Submited/Received</td>
                                    <td>
                                    &nbsp; </td><td style="background-color: #00ff00; width: 20px">
                                    &nbsp; </td><td style="width: 50px">
                                    Closed</td>
                                    <td>
                                    &nbsp; </td></tr></table></div><div style="float: right; width: 50%">
                        &nbsp;&nbsp;&nbsp;Work Flow Status : <table>
                            <tr>
                                <td>
                                    &nbsp; </td><td style="background-color: #ff0000; width: 20px">
                                    &nbsp; </td><td style="width: 50px">
                                    Delay </td><td>
                                    &nbsp; </td><td style="background-color: #00ff00; width: 20px">
                                    &nbsp; </td><td style="width: 75px">
                                    On time </td><td>
                                    &nbsp; </td></tr></table></div><div style="clear: both">
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlLegendStatus" GroupingText="Legend" Width="983px">
                    <div style="float: left; width: 100%">
                        &nbsp;&nbsp;&nbsp;&nbsp;Status : <table>
                            <tr>
                                <td>
                                    &nbsp; </td><td style="background-color: #ff0000; width: 20px">
                                    &nbsp; </td><td style="width: 50px">
                                    Delay </td><td>
                                    &nbsp; </td><td style="background-color: #fffb00; width: 20px">
                                    &nbsp; </td><td style="width: 200px">
                                    On Time But Not Yet Approved </td><td>
                                    &nbsp; </td><td style="background-color: #00ff00; width: 20px">
                                    &nbsp; </td><td style="width: 150px">
                                    On Time And Approved </td><td>
                                    &nbsp; </td><td style="background-color: #8f8f8f; width: 20px">
                                    &nbsp; </td><td style="width: 150px">
                                    Redirect Approved </td><td style="background-color: #fe72ed; width: 20px">
                                    &nbsp; </td><td style="width: 200px">
                                    Duplicate User Approved </td><td>
                                    &nbsp; </td></tr></table></div><div style="clear: both">
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete" OkControlID="BtnOkConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn buttonRight">
                            <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" 
                                OnClientClick="loading();HiddenDeleteOkButton.click();"/>
                                &nbsp; 
                            <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" 
                                OnClick="BtnCancelConfirmationDelete_Click" />
                        </div>
                            <asp:Button ID="HiddenDeleteOkButton" ClientIDMode="Static" Text=" " runat="server" Width="0px" Height="0px" 
                                OnClick="btnOkConfirmationDelete_Click" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- popup region -->
    <asp:Button ID="hidBtnDetail" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="popUpList" runat="server" TargetControlID="hidBtnDetail"
        PopupControlID="panelModal" CancelControlID="btnClosePopUp" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelModal" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="upnModal">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitPopUpMessage"></asp:Literal><asp:Literal runat="server"
                        ID="LitPopUpModal"></asp:Literal><table cellpadding="2px" cellspacing="0" border="0"
                            style="text-align: left" width="400px">
                            <tr style="border: 1px solid black">
                                <td>
                                    <asp:Label ID="lblPopUpSearchCd" runat="server"></asp:Label></td><td>
                                    <asp:TextBox runat="server" ID="txtSearchCd" Width="98%"></asp:TextBox></td><td>
                                </td>
                            </tr>
                            <tr style="border: 1px solid black">
                                <td>
                                    <asp:Label ID="lblPopUpSearchDesc" runat="server"></asp:Label></td><td>
                                    <asp:TextBox runat="server" ID="txtSearchDesc" Width="98%"></asp:TextBox></td><td style="text-align: right">
                                    <asp:Button runat="server" ID="btnClearPopUp" Text="Clear" OnClick="btnClearPopUp_Click" />
                                    &nbsp; <asp:Button runat="server" ID="btnSearchPopUp" Text="Search" OnClick="btnSearchPopUp_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxGridView ID="gridPopUpVendorCd" runat="server" Width="400px" OnPageIndexChanged="gridPopUpVendorCd_PageIndexChanged"
                                        Visible="False" KeyFieldName="VENDOR_CD" AutoGenerateColumns="False" ClientInstanceName="gridPopUpVendorCd"
                                        OnHtmlRowCreated="gridPopUpVendorCd_HtmlRowCreated" Styles-AlternatingRow-CssClass="even">
                                        <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="40px" Name="NO">
                                                <DataItemTemplate>
                                                    <asp:Literal ID="litGridNoVendorCd" runat="server"></asp:Literal></DataItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="VENDOR_CD">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="VENDOR_NAME">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="3" Width="30px">
                                                <EditButton Visible="false" Text=" ">
                                                </EditButton>
                                                <UpdateButton Visible="false" Text=" ">
                                                </UpdateButton>
                                                <NewButton Visible="false" Text=" ">
                                                </NewButton>
                                                <CancelButton Visible="false" Text=" ">
                                                </CancelButton>
                                            </dx:GridViewCommandColumn>
                                        </Columns>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" AllowSelectSingleRowOnly="True" />
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" colspan="3">
                                    <br />
                                    <asp:Button runat="server" ID="btnSend" Text="Send" OnClick="btnSend_Click" />
                                    &nbsp; <asp:Button runat="server" ID="btnClosePopUp" Text="Close" OnClick="btnClosePopUp_Click" />
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <!-- Pop up Total Amount Per Currency-->
    <asp:Button ID="ButtonHidTotalAmountCurr" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupTotalAmountCurr" runat="server" TargetControlID="ButtonHidTotalAmountCurr"
        PopupControlID="panelTotalAmountCurr" CancelControlID="ButtonCloseTotalAmountCurr"
        BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelTotalAmountCurr" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="UpdatePanelTotalAmountCurr">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgTotalAmountCurr"></asp:Literal><asp:Literal
                        runat="server" ID="LitModalTotalAmountCurr"></asp:Literal><table cellpadding="2px"
                            cellspacing="0" border="0" style="text-align: left" width="400px">
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxGridView ID="gridTotalAmountCurr" runat="server" Width="400px" Visible="true"
                                        KeyFieldName="CURRENCY_CD" AutoGenerateColumns="false" Styles-AlternatingRow-CssClass="even"
                                        OnHtmlRowCreated="gridTotalAmountCurr_HtmlRowCreated">
                                        <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="40px" Name="NO">
                                                <DataItemTemplate>
                                                    <asp:Literal ID="litGridNoTotalAmountCurr" runat="server"></asp:Literal></DataItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Currency" VisibleIndex="1" FieldName="CURRENCY_CD">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Amount" VisibleIndex="2" FieldName="TOTAL_AMOUNT">
                                                <DataItemTemplate>
                                                    <asp:Literal runat="server" ID="litGridAmountOtr" Text='<%# CommonFunction.Eval_Curr(Eval("CURRENCY_CD"), Eval("TOTAL_AMOUNT")) %>'>
                                                    </asp:Literal></DataItemTemplate></dx:GridViewDataTextColumn><dx:GridViewDataTextColumn Caption="Exchange Rate" VisibleIndex="3" FieldName="EXCHANGE_RATE">
                                                <DataItemTemplate>
                                                    <asp:Literal runat="server" ID="litGridAmountOtr" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("EXCHANGE_RATE")) %>'>
                                                    </asp:Literal></DataItemTemplate></dx:GridViewDataTextColumn></Columns><SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" colspan="3">
                                    <br />
                                    <asp:Button runat="server" ID="ButtonCloseTotalAmountCurr" Text="Close" OnClick="ButtonCloseTotalAmountCurr_Click" />
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <!-- Pop up SAP Doc No -->
    <asp:Button ID="ButtonHidSapDocNo" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupSapDocNo" runat="server" TargetControlID="ButtonHidSapDocNo"
        PopupControlID="panelSapDocNo" CancelControlID="ButtonCloseSapDocNo" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelSapDocNo" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="UpdatePanelSapDocNo">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgSapDocNo"></asp:Literal><asp:Literal runat="server"
                        ID="LitModalSapDocNo"></asp:Literal><table cellpadding="2px" cellspacing="0" border="0"
                            style="text-align: left" width="680px">
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxGridView ID="gridSapDocNo" runat="server" Width="675px" Visible="true" KeyFieldName="CURRENCY_CD"
                                        Styles-AlternatingRow-CssClass="even" AutoGenerateColumns="false" OnHtmlRowCreated="gridSapDocNo_HtmlRowCreated">
                                        <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="40px" Name="NO">
                                                <DataItemTemplate>
                                                    <asp:Literal ID="litGridNoSapDocNo" runat="server"></asp:Literal></DataItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Invoice No" VisibleIndex="0" FieldName="INVOICE_NO" Width="150px"/>
                                             <dx:GridViewDataTextColumn Caption="SAP Doc No" VisibleIndex="1" FieldName="SAP_DOC_NO" />                                            
                                            <dx:GridViewDataTextColumn Caption="Sap Clearing Doc No" VisibleIndex="2" FieldName="SAP_CLEARING_DOC_NO" Width="135px"/>                                                                           <dx:GridViewDataTextColumn Caption="Pay Prop Doc No" VisibleIndex="3" FieldName="PAYPROP_DOC_NO" Width="140px"/>
                                            <dx:GridViewDataTextColumn Caption="Pay Prop ID" VisibleIndex="4" FieldName="PAYPROP_ID" Width="90px"/>
                                        </Columns>
                                        
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False"  ColumnResizeMode="Control"/>
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                            <Cell HorizontalAlign="Center">
                                            </Cell>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" colspan="3">
                                    <br />
                                    <asp:Button runat="server" ID="ButtonCloseSapDocNo" ClientIDMode="Static" Text="Close" OnClick="ButtonClosePopUpSapDocNo_Click" />
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <!-- Pop up Budget No -->
    <asp:Button ID="ButtonHidBudgetNo" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupBudgetNo" runat="server" TargetControlID="ButtonHidBudgetNo"
        PopupControlID="panelBudgetNo" CancelControlID="ButtonCloseBudgetNo" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelBudgetNo" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="UpdatePanelBudgetNo">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgBudgetNo"></asp:Literal><asp:Literal runat="server"
                        ID="LitModalBudgetNo"></asp:Literal><table cellpadding="2px" cellspacing="0" border="0"
                            style="text-align: left" width="400px">
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxGridView ID="gridBudgetNo" runat="server" Width="400px" Visible="true" KeyFieldName="SEQ_NO"
                                        Styles-AlternatingRow-CssClass="even" AutoGenerateColumns="false" OnHtmlRowCreated="gridBudgetNo_HtmlRowCreated">
                                        <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="40px" Name="NO">
                                                <DataItemTemplate>
                                                    <asp:Literal ID="litGridNoBudgetNo" runat="server"></asp:Literal></DataItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Budget No" VisibleIndex="0" FieldName="WBS_NO">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                            <Cell HorizontalAlign="Center">
                                            </Cell>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" colspan="3">
                                    <br />
                                    <asp:Button runat="server" ID="ButtonCloseBudgetNo" Text="Close" OnClick="ButtonClosePopUpBudgetNo_Click" />
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <!-- Pop up Attachment -->
    <asp:Button ID="ButtonHidAttachment" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupAttachment" runat="server" TargetControlID="ButtonHidAttachment"
        PopupControlID="panelAttachment" CancelControlID="ButtonCloseAttachment" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelAttachment" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="UpdatePanelAttachment">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgAttachment"></asp:Literal><asp:Literal runat="server"
                        ID="LitModalAttachment"></asp:Literal><table cellpadding="2px" cellspacing="0" border="0"
                            style="text-align: left" width="400px">
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxGridView ID="gridAttachment" runat="server" Width="400px" Visible="true"
                                        KeyFieldName="REF_SEQ_NO" Styles-AlternatingRow-CssClass="even" AutoGenerateColumns="false"
                                        OnHtmlRowCreated="gridAttachment_HtmlRowCreated">
                                        <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="REF_SEQ_NO" ReadOnly="True" Caption="No" VisibleIndex="0"
                                                Width="40px">
                                                <DataItemTemplate>
                                                    <asp:Literal ID="litGridNoAttachment" runat="server"></asp:Literal></DataItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataHyperLinkColumn FieldName="ATTACH_NAME" ReadOnly="True" Caption="Attachment Category"
                                                VisibleIndex="1">
                                                <DataItemTemplate>
                                                    <asp:HyperLink Text='<%# Eval("ATTACH_NAME") %>' runat="server" ID="hprAttachID"
                                                        ToolTip='<%# String.Format("Download {0}",Eval("FILE_NAME")) %>' Target="_blank"
                                                        NavigateUrl='<%# Eval("URL") %>'>
                                                    </asp:HyperLink></DataItemTemplate><CellStyle VerticalAlign="Middle" Wrap="False">
                                                </CellStyle>
                                            </dx:GridViewDataHyperLinkColumn>
                                            <dx:GridViewDataTextColumn FieldName="DESCRIPTION" ReadOnly="True" Caption="Description"
                                                VisibleIndex="2" Visible="False">
                                                <CellStyle VerticalAlign="Middle" Wrap="False">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="FILE_NAME" ReadOnly="True" Caption="Filename"
                                                VisibleIndex="3" Visible="False">
                                                <DataItemTemplate>
                                                    <asp:Literal runat="server" ID="litGridFilename" />
                                                    <asp:LinkButton ID="Linkbtn" runat="server" Text='<%#Eval("FILE_NAME") %>' ToolTip='<%# String.Format("Download {0}",Eval("FILE_NAME")) %>' />
                                                </DataItemTemplate>
                                                <CellStyle VerticalAlign="Middle" Wrap="False">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Visible="false" FieldName="DIRECTORY" VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                            <Cell HorizontalAlign="Center">
                                            </Cell>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" colspan="3">
                                    <br />
                                    <asp:Button runat="server" ID="ButtonCloseAttachment" Text="Close" OnClick="ButtonClosePopUpAttachment_Click" />
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <!-- Pop up Version -->
    <asp:Button ID="ButtonHidVersion" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupVersion" runat="server" TargetControlID="ButtonHidVersion"
        PopupControlID="panelVersion" CancelControlID="ButtonCloseVersion" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelVersion" CssClass="speakerPopupList" Height="600px">
            <asp:UpdatePanel runat="server" ID="UpdatePanelVersion">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgVersion"></asp:Literal><asp:Literal runat="server"
                        ID="LitModalVersion"></asp:Literal><table cellpadding="2px" cellspacing="0" border="0"
                            style="text-align: left" width="95%">
                            <tr>
                                <td>
                                    <br />
                                    <dx:ASPxGridView ID="gridVersion" runat="server" Width="100%" ClientInstanceName="gridVersion"
                                        KeyFieldName="Version" AutoGenerateColumns="False">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="Version." FieldName="Version" VisibleIndex="0">
                                                <HeaderStyle ForeColor="#d500ff" />
                                                <CellStyle ForeColor="#d500ff">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataDateColumn Caption="Modified Date" FieldName="UpdatedDt" VisibleIndex="1">
                                                <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}">
                                                </PropertiesDateEdit>
                                                <HeaderStyle ForeColor="#d500ff" />
                                                <CellStyle ForeColor="#d500ff">
                                                </CellStyle>
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn Caption="Modified By" FieldName="ModifiedBy" VisibleIndex="2">
                                                <HeaderStyle ForeColor="#d500ff" />
                                                <CellStyle ForeColor="#d500ff">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <SettingsPager AlwaysShowPager="True">
                                        </SettingsPager>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                            VerticalScrollableHeight="470" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                            <Cell HorizontalAlign="Center" Border-BorderStyle="None">
                                                <Border BorderStyle="None" />
                                            </Cell>
                                        </Styles>
                                        <Templates>
                                            <DetailRow>
                                                <table cellpadding="2" cellspacing="1" style="border-collapse: collapse;">
                                                    <tr class='<%# (Eval("PVDate")==null) ? "hide": "" %>'>
                                                        <td style="font-weight: bold">
                                                            PV Date: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("PVDate")) %>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("DivisionID")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Issuing Division: </td><td>
                                                            <%# Eval("DivisionID")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("PVTypeName")==null) ? "hide" : ""%>'>
                                                        <td style="font-weight: bold">
                                                            PV Type: </td><td>
                                                            <%# Eval("PVTypeName")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("TotalAmount")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Total Amount: </td><td>
                                                            <%# CommonFunction.Eval_Curr("IDR", Eval("TotalAmount"))%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("VendorGroupName")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Vendor Group: </td><td>
                                                            <%# Eval("VendorGroupName")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("PaymentMethodName")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Payment Method: </td><td>
                                                            <%# Eval("PaymentMethodName")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("StatusName")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Status: </td><td>
                                                            <%# Eval("StatusName")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("VendorName")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Vendor: </td><td>
                                                            <%# Eval("VendorCode")%>
                                                            - <%# Eval("VendorName")%></td></tr><tr class='<%# (Eval("TransactionName")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Transaction Type: </td><td>
                                                            <%# Eval("TransactionName")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("ActivityDate")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Activity Date From: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("ActivityDate")) %>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("ActivityDateTo")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Activity Date To: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("ActivityDateTo"))%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("PostingDate")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Posting Date: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("PostingDate"))%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("PlanningPaymentDate")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Planning Payment Date: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("PlanningPaymentDate"))%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("BankType")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Bank Type: </td><td>
                                                            <%# Eval("BankType")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("BudgetNumber")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Budget Number: </td><td>
                                                            <%# Eval("BudgetNumber")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("SuspenseNumber")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Suspense Number: </td><td>
                                                            <%# Eval("SuspenseNumber")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("SetNoPv")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Settlement No PV: </td><td>
                                                            <%# Eval("SetNoPv")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("SetNoRv")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Settlement No RV: </td><td>
                                                            <%# Eval("SetNoRv")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("SubmitDate")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Submit Date: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("SubmitDate"))%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("PaidDate")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Paid Date: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("PaidDate"))%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("WorkflowStatus")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Workflow Status: </td><td>
                                                            <%# Eval("WorkflowStatus")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("SettlementStatus")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Settlement Status: </td><td>
                                                            <%# Eval("SettlementStatus")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("ReferenceNo")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Reference No: </td><td>
                                                            <%# Eval("ReferenceNo")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("SubmitHcDocDate")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Submit HC Doc Date: </td><td>
                                                            <%# string.Format("{0:dd/MM/yyyy}", Eval("SubmitHcDocDate"))%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("HoldBy")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Hold By: </td><td>
                                                            <%# Eval("HoldBy")%>
                                                        </td>
                                                    </tr>
                                                    <tr class='<%# (Eval("HoldFlag")==null) ? "hide": ""%>'>
                                                        <td style="font-weight: bold">
                                                            Hold Flag: </td><td>
                                                            <%# Eval("HoldFlag")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <dx:ASPxGridView runat="server" ID="gridPVDetail" Width="100%" ClientInstanceName="gridPVDetail"
                                                    KeyFieldName="SequenceNumber" OnBeforePerformDataSelect="gridPVDetail_DataSelect"
                                                    AutoGenerateColumns="false" OnHtmlRowCreated="gridPVDetail_HtmlRowCreated" OnHtmlDataCellPrepared="gridPVDetail_HtmlDataCellPrepared">
                                                    <Columns>
                                                        <dx:GridViewBandColumn Caption="Cost Center" VisibleIndex="0">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Code" FieldName="CostCenterCode" Width="110px"
                                                                    VisibleIndex="0">
                                                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Name" FieldName="CostCenterName" Width="120px"
                                                                    VisibleIndex="1">
                                                                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" VisibleIndex="1">
                                                            <CellStyle VerticalAlign="Middle" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewBandColumn Caption="Amount" VisibleIndex="2">
                                                            <Columns>
                                                                <dx:GridViewDataComboBoxColumn Caption="Curr" FieldName="CurrencyCode" VisibleIndex="0">
                                                                    <CellStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                                                </dx:GridViewDataComboBoxColumn>
                                                                <dx:GridViewDataTextColumn Caption="Value" FieldName="Amount" VisibleIndex="1">
                                                                    <CellStyle VerticalAlign="Middle" HorizontalAlign="Right" Paddings-PaddingLeft="5px"
                                                                        Paddings-PaddingRight="10px" />
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataTextColumn Caption="Invoice No" FieldName="InvoiceNumber" VisibleIndex="3">
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Tax Code" FieldName="TaxCode" VisibleIndex="4">
                                                            <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataTextColumn Caption="Tax No" FieldName="TaxNumber" VisibleIndex="5">
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                                                Paddings-PaddingRight="5px" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="IsDeleted" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CostCenterCodeChange" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CostCenterNameChange" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DescriptionChange" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CurrencyCodeChange" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="AmountChange" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="TaxCodeChange" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="TaxNumberChange" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="InvoiceNumberChange" Visible="false" />
                                                    </Columns>
                                                    <Styles>
                                                        <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </Header>
                                                    </Styles>
                                                </dx:ASPxGridView>
                                            </DetailRow>
                                        </Templates>
                                        <SettingsDetail ShowDetailRow="true" />
                                        <Settings ShowGroupPanel="false" />
                                        <SettingsCustomizationWindow Enabled="True" />
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="lwrap">
                                        <div class="xbox">
                                            &nbsp;</div><div class="boxText">
                                            <b>Legend: </b></div><div class="xbox">
                                            &nbsp;</div><div class="wbox">
                                            &nbsp;</div><div class="boxText">
                                            Initial</div><div class="ybox">
                                            &nbsp;</div><div class="boxText">
                                            Modified, New</div><div class="rbox">
                                            &nbsp;</div><div class="boxText">
                                            Deleted</div></div><div style="text-align: right; float: right;">
                                        <asp:Button runat="server" ID="ButtonCloseVersion" Text="Close" OnClick="ButtonClosePopUpVersion_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <asp:UpdatePanel ID="upnPostToSAPConf" runat="server">
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationPost" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ModalPopupConfirmationPost" runat="server" 
                TargetControlID="hidBtnConfirmationPost"
                PopupControlID="ConfirmationPostPanel" 
                OkControlID="btnOkConfirmationPost"
                CancelControlID="BtnCancelConfirmationPost" 
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationPostPanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationPost" runat="server" Text=""/>
                        
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn buttonRight">
                            <asp:Button ID="btnOkConfirmationPost" Text="OK" runat="server"                             
                                OnClientClick="popPostGo();" />
                            &nbsp; <asp:Button ID="BtnCancelConfirmationPost" Text="Cancel" runat="server"  />
                            <asp:Button ID="btnPostGo" runat="server" 
                                Width="0px" Height="0px" ClientIDMode="Static" 
                                onclick="btnOkConfirmationPost_Click" 
                                OnClientClick="window.scrollTo(0,0);loading()"/>
                        </div>
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnCheckedByAccConf" runat="server">
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationCheck" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ModalPopupConfirmationCheck" runat="server" TargetControlID="hidBtnConfirmationCheck"
                PopupControlID="ConfirmationCheckPanel" CancelControlID="BtnCancelConfirmationCheck"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationCheckPanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationCheck" runat="server" Text=""/><br />
                        <asp:Button ID="btn2" Text="OK" runat="server" OnClick="btnOkConfirmationCheck_Click"
                            OnClientClick="loading()" />
                        &nbsp; <asp:Button ID="BtnCancelConfirmationCheck" Text="Cancel" runat="server" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnApproval" runat="server">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidButtonPop" runat="server" style="display:none;" />
            <cc1:ModalPopupExtender ID="popUpDirect" runat="server"
                TargetControlID="hidButtonPop"
                PopupControlID="pnlPop"
                OkControlID="btnPopOk"
                CancelControlID="btnPopCancel"
                OnOkScript="popProceed()"
                BackgroundCssClass="modalBackground"
                OnCancelScript="clearComment()" />    
            <center>
            <asp:Panel ID="pnlPop" runat="server" CssClass="speakerPopupList" 
                    GroupingText="Direct To Me Confirmation"><br />
                <div class="row">
                    <label>Reason :</label>
                    <asp:DropDownList ID="ddlReason" runat="server"
                        DataSourceID="dsReason"
                        DataTextField="REASON_DESCRIPTION" 
                        DataValueField="REASON_DESCRIPTION"
                        DataTextFormatString="{0}" 
                         />
                </div>
                <div class="rowbtn">
                    <div class="btnRightLong">
                        <asp:Button ID="btnPopOk" runat="server" text="Proceed"/>&nbsp; 
                        <asp:Button ID="btnPopCancel" runat="server" Text="Cancel" 
                            OnClientClick="window.scrollTo(0,0);loading()" />
                        <asp:Button ID="btnGo" runat="server" 
                            Width="0px" Height="0px" ClientIDMode="Static" 
                            onclick="btnGo_Click" 
                            OnClientClick="window.scrollTo(0,0);loading()"/>
                    </div>
                </div>
                </div>
            </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Worklist Report Dialog -->
    <asp:UpdatePanel ID="upnWorklistReport" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnPrintWorklistReport" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidWorklistReport" runat="server" style="display:none;" />
            <cc1:ModalPopupExtender ID="popUpWorklistReport" runat="server"
                TargetControlID="hidWorklistReport"
                PopupControlID="pnlWorklist"
                OkControlID="btnOkWorklistReport"
                CancelControlID="btnCancelWorklistReport"
                OnOkScript="popWorklistReport()"
                BackgroundCssClass="modalBackground"
                />    
            <center>
            <asp:Panel ID="pnlWorklist" runat="server" CssClass="speakerPopupList" 
                    GroupingText="Download Report">
                    <br />
                <div class="row">
                    <span class="lefty" style="margin-right: 10px">Delay Tolerance (days)</span>
					<asp:TextBox ID="DelayToleranceText" runat="server" Columns="2" Text="2"/>
                </div>
                <div class="rowbtn">
                    <div class="btnRightLong">
                        <asp:Button ID="btnOkWorklistReport" runat="server" text="Ok"/>&nbsp; 
                        <asp:Button ID="btnCancelWorklistReport" runat="server" Text="Cancel" 
                            OnClientClick="window.scrollTo(0,0);loading()" />
                        <asp:Button ID="btnPrintWorklistReport" runat="server" 
                            Width="0px" Height="0px" ClientIDMode="Static" 
                            onclick="btnPrintWorklistReport_Click" 
                            OnClientClick="window.scrollTo(0,0);loading()"/>
                    </div>
                </div>
                </div>
            </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
