﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._30PVFormList;
using Common.Data._40RVFormList;
using Common.Data._60SettlementForm;
using Common.Data._80Accrued;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using Common.Messaging;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrList : BaseCodeBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8004");
        private string susNo, susYear;
        private int suNO, suYY;
        private SelectableDropDownHelper sel = null;

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }
        #endregion

        private RoleLogic _roleLogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_roleLogic == null)
                {
                    _roleLogic = new RoleLogic(UserName, _ScreenID);
                }
                return _roleLogic;
            }
        }


        #region Getter Setter for Data List
        private List<AccruedListData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<AccruedListData>();
                }
                else
                {
                    return (List<AccruedListData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion
        #region Getter Setter for Attachment List
        [Serializable]
        private class ListAttachmentData
        {
            private string _NO;
            public string NO
            {
                get
                {
                    return _NO;
                }
                set
                {
                    _NO = value;
                }
            }

            private string _FILENAME;
            public string FILENAME
            {
                get
                {
                    return _FILENAME;
                }
                set
                {
                    _FILENAME = value;
                }
            }
        }
        private List<ListAttachmentData> ListAttachment
        {
            set
            {
                ViewState["_listAttachment"] = value;
            }
            get
            {
                if (ViewState["_listAttachment"] == null)
                {
                    return new List<ListAttachmentData>();
                }
                else
                {
                    return (List<ListAttachmentData>)ViewState["_listAttachment"];
                }
            }
        }
        #endregion
        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;

            sel = new SelectableDropDownHelper(IssuingDivisionDropDown, "IssuingDivisionListBox");
            ASPxListBox divBox = (ASPxListBox)IssuingDivisionDropDown.FindControl("IssuingDivisionListBox");

            if (!IsPostBack && !IsCallback)
            {
                if (divBox != null) sel.SelectAll();

                PrepareLogout();

                Ticker.In("GenerateComboData");
                GenerateComboData(ddlWorkflowStatus, "WORKFLOW_STATUS", true);
                GenerateComboData(ddlSubSts, "ACCR_SUBM_STATUS", true);
                Ticker.Out();

                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();

                gridGeneralInfo.DataSource = null;
                #region Init Startup Script
                AddEnterComponent(IssuingDivisionDropDown);
                AddEnterComponent(txtPVYear);
                AddEnterComponent(txtAccrNo);
                AddEnterComponent(ddlSubSts);
                AddEnterComponent(dtCreatedDateFrom);
                AddEnterComponent(dtCreatedDateTo);
                AddEnterComponent(ddlWorkflowStatus);
                AddEnterComponent(txtTotAmountFrom);
                AddEnterComponent(txtTotAmountTo);

                btnNew.Visible = RoLo.isAllowedAccess("btnNew");
                btnDelete.Visible = RoLo.isAllowedAccess("btnDelete");

                if (logic.User.IsAuthorizedFinance || logic.User.IsDirectorUp || UserData.Roles.Contains("ELVIS_ADMIN"))
                {
                    AddEnterComponent(IssuingDivisionDropDown);
                }
                else
                {
                    IssuingDivisionDropDown.Enabled = false;
                }
                AddEnterAsSearch(btnSearch, _ScreenID);
                #endregion

                DoSearch();
            }
        }


        protected void DoSearch()
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
                return;
            }

            if (ValidateInputSearch())
            {
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;

                gridGeneralInfo.DataBind();
            }

            btnView.Visible = false;
        }


        #region Set Button Visible
        private void SetFirstLoad()
        {
            btnSearch.Visible = RoLo.isAllowedAccess("btnSearch");
            btnClear.Visible = RoLo.isAllowedAccess("btnClear");
            btnView.Visible = false;
            btnResult.Visible = false;
            btnNew.Visible = RoLo.isAllowedAccess("btnNew");
            btnDelete.Visible = false;
            btnDownload.Visible = false;
        }

        private void SetSearchLoad(bool noData)
        {
            if (noData)
            {
                SetFirstLoad();
            }
            else
            {
                btnSearch.Visible = RoLo.isAllowedAccess("btnSearch");
                btnClear.Visible = RoLo.isAllowedAccess("btnClear");
                btnView.Visible = RoLo.isAllowedAccess("btnView");
                btnResult.Visible = RoLo.isAllowedAccess("btnResult");
                btnNew.Visible = RoLo.isAllowedAccess("btnNew");
                btnDelete.Visible = RoLo.isAllowedAccess("btnDelete");
                btnDownload.Visible = RoLo.isAllowedAccess("btnDownload");
            }
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.PageCount > 0)
                {
                    HyperLink hypDetail = gridGeneralInfo.FindRowCellTemplateControl(
                        e.VisibleIndex, gridGeneralInfo.Columns["ACCRUED_NO"] as GridViewDataColumn
                        , "hypDetail") as HyperLink;

                    string accruedNo = e.GetValue("ACCRUED_NO").str();

                    if (hypDetail != null)
                    {
                        string url = Page.ResolveClientUrl(string.Format("~/80Accrued/AccrForm.aspx?accrno={0}&mode=view", accruedNo));
                        url = string.Format("javascript:openWin('{0}', '{1}')", url, resx.Screen("ELVIS_Screen_8005"));

                        hypDetail.NavigateUrl = url;
                        hypDetail.Text = accruedNo;
                    }
                }
            }
        }
      
        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            var result = logic.AccrList.Search(SearchCriteria()).ToList();
            ListInquiry = result;
            SetSearchLoad(!result.Any());

            e.Result = ListInquiry;
        }

        protected AccruedSearchCriteria SearchCriteria()
        {
            return new AccruedSearchCriteria(
                    sel.SelectedValues(CommonFunction.CommaJoin(UserData.Divisions, ";"), (UserData != null) ? UserData.isFinanceDivision : false),
                    txtAccrNo.Text, ddlSubSts.SelectedValue, ddlWorkflowStatus.SelectedValue, txtPVYear.Text,
                    txtTotAmountFrom.Text, txtTotAmountTo.Text, dtCreatedDateFrom.Text, dtCreatedDateTo.Text
                );
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //logic.Say("btnSearch_Click", "Search {0} {1}", ddlSuspenseNo.Text, txtPVYear.Text);
            DoSearch();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (IssuingDivisionDropDown.Enabled)
                sel.Clear();
            txtPVYear.Text = null;
            txtAccrNo.Text = null;
            ddlSubSts.SelectedIndex = 0;
            dtCreatedDateFrom.Text = null;
            dtCreatedDateTo.Text = null;
            ddlWorkflowStatus.SelectedIndex = 0;
            txtTotAmountFrom.Text = null;
            txtTotAmountTo.Text = null;

            gridGeneralInfo.DataSource = null;
            gridGeneralInfo.DataSourceID = null;

            DoSearch();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logic.Say("btnDelete_Click", "Delete");
            if (gridGeneralInfo.Selection.Count == 1)
            {
                Boolean checkValid = true;

                if (checkValid)
                {
                    lblConfirmationDelete.Text = _m.Message("MSTD00008CON", "delete");
                    ConfirmationDeletePopUp.Show();
                }
            }
            else if (gridGeneralInfo.Selection.Count > 1)
            {
                #region Error

                Nag("MSTD00016WRN");
                #endregion
            }
            else
            {
                Nag("MSTD00009WRN");
            }

        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            string accruedNo = "";
            if (gridGeneralInfo.Selection.Count != 1) return;

            int row = GetFirstSelectedRow();
            accruedNo = gridGeneralInfo.GetRowValues(row, "ACCRUED_NO").ToString();

            logic.Say("btnOkConfirmationDelete_Click", String.Format("Confirm Delete Accrued List no : {0}", accruedNo));

            int statusCd = logic.AccrForm.GetLatestStatus(accruedNo);
            if (!String.IsNullOrEmpty(accruedNo) && (statusCd == 0 || statusCd == 112))
            {
                result = logic.AccrList.Delete(accruedNo, UserName);
                if (!result)
                {
                    gridGeneralInfo.Selection.UnselectAll();

                    Nag("MSTD00014ERR");
                }
                else
                {
                    Nag("MSTD00015INF");
                    gridGeneralInfo.Selection.UnselectAll();
                    DoSearch();
                }
            }
            else
            {
                Nag("MSTD00216ERR", "Draft List Accrued");
            }
        }

        protected void BtnCancelConfirmationDelete_Click(object sender, EventArgs e)
        {
            ConfirmationDeletePopUp.Hide();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            var crit = new AccruedSearchCriteria();
            crit.ignoreIssuingDivision = false;
            crit.issuingDivision = UserData.DIV_CD.ToString();
            //crit.budgetYear = DateTime.Now.Year - 1; // cuman buat testing tar ilangin -1 nya
            crit.budgetYear = DateTime.Now.Year;

            var qdata = logic.AccrList.Search(crit);
            if (qdata.Any())
            {
                bool anyDraft = qdata.Any(x => x.STATUS_CD != 105);

                if (anyDraft)
                {
                    Nag("MSTD00002ERR", "There is still Document that has not \"Complete\"");
                    return;
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ELVIS_AccrForm",
                "openWin('AccrForm.aspx','ELVIS_AccrForm');",
                true);

        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count == 1)
            {
                int row = GetFirstSelectedRow();
                string accruedNo = gridGeneralInfo.GetRowValues(row, "ACCRUED_NO").ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "ELVIS_AccrForm",
                    "openWin('AccrForm.aspx?accrno=" + accruedNo + "&mode=view','ELVIS_AccrForm');",
                    true);
            }
            else if (gridGeneralInfo.Selection.Count > 1)
            {
                #region Error

                Nag("MSTD00016WRN");
                #endregion
            }
            else
            {
                Nag("MSTD00009WRN");
            }
        }

        protected void btnResult_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count == 1)
            {
                int row = GetFirstSelectedRow();
                string accruedNo = gridGeneralInfo.GetRowValues(row, "ACCRUED_NO").ToString();

                ScriptManager.RegisterStartupScript(this, this.GetType(),
                    "ELVIS_AccrResultList",
                    "openWin('AccrResultList.aspx?accrno=" + accruedNo + "','ELVIS_AccrResultList');",
                    true);
            }
            else if (gridGeneralInfo.Selection.Count > 1)
            {
                Nag("MSTD00016WRN");
            }
            else
            {
                Nag("MSTD00009WRN");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonDownload_Click", "Download");
            if (ValidateInputSearch())
            {
                DownloadListPDF();
            }
        }

        protected void DownloadListPDF()
        {
            UserData userdata = (UserData)Session["UserData"];
            try
            {
                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "AccruedListPDF.xls");
                string excelName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                string pdfName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";

                string fullPath = logic.AccrList.DownloadListAccrPDF(
                            UserName,
                            templatePath,
                            excelName,
                            SearchCriteria(),
                            userdata,
                            pdfName
                        );
                Xmit.Transmit(HttpContext.Current, fullPath);



            }
            catch (Exception ex)
            {
                string xmsg = null;
                if (ex.InnerException != null)
                {
                    xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                }
                Nag("MSTD00042ERR", xmsg.isNotEmpty() ? xmsg : ex.Message);
            }
        }

        #endregion

        #region Function


        private bool ValidateInputSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Handle(ex);
                throw;
            }
            return true;
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.PVList.GetUserDivisions(UserData);
        }

        protected string Status_Color(Object statusValue)
        {
            string color = "";
            short statusInt = 0;
            if (statusValue != null && Int16.TryParse(statusValue.ToString(), out statusInt))
            {
                if (statusInt == 1)
                    color = red;
                else if (statusInt == 2)
                    color = yellow;
                else if (statusInt > 2)
                    color = green;
            }
            return color;
        }

        protected string WriteSelectedIndexInfo(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected void grid_CustomCallback(object sender,
            DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;

            int newSize;
            if (!int.TryParse(e.Parameters.ToString(), out newSize)) return;

            if (newSize < 0)
                newSize = 0;
            grid.SettingsPager.PageSize = newSize;
            PageSize = newSize;
            // Session[GridCustomPageSizeName] = newSize;
            if (grid != null && e != null && !String.IsNullOrEmpty(e.Parameters) && grid.VisibleRowCount > 0)
            {
                if (ValidateInputSearch())
                {
                    grid.DataBind();

                }
            }
        }

        protected int GetFirstSelectedRow()
        {
            int row = -1;
            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                if (!gridGeneralInfo.Selection.IsRowSelected(i)) continue;

                row = i;
                break;
            }
            return row;
        }
        #endregion
    }
}