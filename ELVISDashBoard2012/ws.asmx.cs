﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Net;
using Common.Data._30PVFormList;
using Common.Function;
using BusinessLogic;
using Common.Data;
using BusinessLogic._00Administration;
using Common;

namespace ELVISDashBoard
{
    /// <summary>
    /// Summary description for ws
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ws : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        
       
        public List<ApprovalData> approvalOverview(int docNo, int docYear, int level, string userName, int statusCode)
        {
            // LoggingLogic.say("ws", "GetApprovalOverview({0}, {1}, {2}, {3}, {4})", docNo, docYear, level, userName, statusCode);
            if (Session == null || Session["userData"] == null)
            {
                Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                return null;
            }

            return LogicFactory.Get().Base.GetApprovalOverview(docNo.ToString(), docYear.ToString(), level, new Common.Data.UserData() { USERNAME = userName }, statusCode);
        }

        [WebMethod(EnableSession = true, CacheDuration = 10, Description = "UserData from login")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string userdata()
        {
            UserData r = null;

            if (Session != null)
            {
                r = Session["userData"] as UserData;
            }
            else
            {
                Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }

            return (r != null) ? StrTo.toJSON(r) : "";
        }

        [WebMethod(EnableSession = true, CacheDuration = 3, Description = "Session List and Get")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string session(string name)
        {
            if (Session != null)
            {
                if (string.IsNullOrEmpty(name))
                {
                    List<string> t = new List<string>();

                    foreach (var s in Session.Keys)
                    {
                        t.Add(s.ToString());
                    }
                    return StrTo.toJSON(t.ToArray());
                }

                else
                {
                    if (Session[name] != null)
                        return StrTo.toJSON(Session[name]);
                    else
                    {
                        Context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        return null;
                    }
                }
            }
            else
            {
                Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
            return "";
        }

        [WebMethod(BufferResponse = false, CacheDuration = 3, EnableSession = false, Description = "Heap List and Get")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string heap(string name, string act = null)
        {
            int del = (!act.isEmpty()) ? act.Int() : 0;

            if (string.IsNullOrEmpty(name))
                return StrTo.toJSON(Heap.Keys());
            else
            {
                if (del == 0)
                {
                    if (Heap.Has(name))
                        return StrTo.toJSON(Heap.Get<object>(name));
                    else
                    {
                        Context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        return null;
                    }
                }
                else
                {
                    return (Heap.Set<string>(name, null) ? "1" : "0");
                }
            }
        }


        [WebMethod(BufferResponse = false, EnableSession = false, Description = "Get Log Data")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<LogData> log(int? id = 0, int? start = 1, int? limit = -1, string filter = "")
        {
            return (new LogLogic()).List(id ?? 0, start ?? 1, limit ?? -1, filter);
        }

        [WebMethod(BufferResponse = false, CacheDuration = 3, EnableSession = false, Description = "Cluster Data")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string cluster(string id, string name, string text = null, string act = null)
        {
            try
            {
                int del = (!act.isEmpty()) ? act.Int() : 0;
                if (text.isEmpty() && del==0)
                    return StrTo.toJSON((new ClusterLogic()).GetClusterData(id, name));
                else
                {
                    var clo = new ClusterLogic(); 
                    if (del != 0)
                    {
                        clo.SetClusterTransaction(id, "", del);
                    }
                    return StrTo.toJSON(clo.SetClusterData(id, name, text, del));
                }
            }
            catch (Exception e)
            {
                Context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return LoggingLogic.Trace(e);
            }
        }

        [WebMethod(BufferResponse = false, CacheDuration = 3, EnableSession = false, Description = "Cluster Transaction")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string clusterTransaction(string id, string text = null, string act = null)
        {
            try
            {
                int del = (!act.isEmpty()) ? act.Int() : 0;
                if (text.isEmpty() && del==0)
                    return StrTo.toJSON((new ClusterLogic()).GetClusterTransaction(id));
                else
                {
                    
                    return StrTo.toJSON((new ClusterLogic()).SetClusterTransaction(id, text, del));
                }
            }
            catch (Exception e)
            {
                Context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return LoggingLogic.Trace(e);
            }
        }

        [WebMethod(BufferResponse = false, CacheDuration = 3, EnableSession = false, Description = "Cluster Status")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string clusterStatus(string id)
        {
            try
            {
                return StrTo.toJSON((new ClusterLogic()).GetClusterStatus(id));
            }
            catch (Exception e)
            {
                Context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return LoggingLogic.Trace(e);
            }
        }

        [WebMethod(BufferResponse = false, CacheDuration = 3, EnableSession = false, Description = "Transaction Cluster")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string transactionCluster(string id)
        {
            try
            {
                return StrTo.toJSON((new ClusterLogic()).GetTransactionCluster(id));
            }
            catch (Exception e)
            {
                Context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return LoggingLogic.Trace(e);
            }
        }
    }




}
