-- GetSapDocNoDummy
DECLARE @sap_doc_no varchar(10);

select @sap_doc_no = RIGHT('0000000000' + CAST(SYSTEM_VALUE_NUM AS varchar(10)), 10)
from TB_M_SYSTEM
where SYSTEM_TYPE = 'DUMMY_SAP_DOC'
	and SYSTEM_CD = 'SEQ_NO'

UPDATE TB_M_SYSTEM
SET SYSTEM_VALUE_NUM = SYSTEM_VALUE_NUM + 1
where SYSTEM_TYPE = 'DUMMY_SAP_DOC'
	and SYSTEM_CD = 'SEQ_NO'

select @sap_doc_no