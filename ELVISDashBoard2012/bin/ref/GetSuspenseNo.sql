-- GetSuspenseNo 	
SELECT TOP 1 Z.VERSION
       , S.SUSPENSE_NO
       , S.SUSPENSE_YEAR
       , S.PV_NO
       , S.PV_YEAR
FROM (SELECT 0 VERSION) AS Z
  LEFT JOIN (SELECT suspense_no
                    , suspense_year
                    , pv_no
                    , pv_year
             FROM tb_r_pv_h
             WHERE pv_no = @no
             AND   pv_year = @year) S ON 1 = 1
