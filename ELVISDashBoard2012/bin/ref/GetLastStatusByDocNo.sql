-- GetLastStatusByDocNo
select 
	case 
	when @docCd = 1 
	then (
		select top 1 status_cd 
		from TB_R_PV_H 
		where PV_NO = @no 
		and PV_YEAR = @yy 
		)	
	when @docCd = 2 
	then (
		select top 1 status_cd 
		from TB_R_RV_H 
		where RV_NO = @no 
		and RV_YEAR = @yy 
	)
	ELSE NULL 
	END;