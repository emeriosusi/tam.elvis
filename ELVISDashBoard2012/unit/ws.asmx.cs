﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Common.Data._30PVFormList;
using Common.Function;
using BusinessLogic;
using Common.Data;
using BusinessLogic._00Administration;

namespace ELVISDashBoard.unit
{
    /// <summary>
    /// Summary description for ws
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ws : System.Web.Services.WebService
    {
        LogicFactory logic = LogicFactory.Get();        

        [WebMethod (EnableSession=true)]
        public List<ApprovalData> GetApprovalOverview(int docNo, int docYear, int level, string userName, int statusCode) 
        {
            // LoggingLogic.say("ws", "GetApprovalOverview({0}, {1}, {2}, {3}, {4})", docNo, docYear, level, userName, statusCode);
            if (Session == null ||Session["userData"] == null)
                return null;
                
            return logic.Base.GetApprovalOverview(docNo.ToString(), docYear.ToString(), level, new Common.Data.UserData() { USERNAME = userName }, statusCode);
        }

        [WebMethod (EnableSession=true)]
        public UserData GetUserData()
        {
            UserData r = null;
            
            if (Session!=null)
                r = Session["userData"] as UserData;

            return r;
        }

        [WebMethod(EnableSession = true)]
        public object GetSession(string name)
        {
            if (Session != null)
            {
                return Session[name];
            }
            return null;
        }

        [WebMethod] 
        public List<LogData> GetLog(int? pid = 0, int? offset=0, int? limit=0, string criteria="")
        {
            LogLogic l = new LogLogic();
            return l.List(pid??0, offset??0, limit??0, criteria);
        }
    }

    


}
