﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using Common.Function;
using Common;


namespace ELVISDashBoard.unit
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dateLabel.Text = DateTime.Now.ToString();
        }

        protected void ResetSQLCacheButton_Click(object sender, EventArgs e)
        {
            LogicFactory f = LogicFactory.Get();

            f.SQL.ResetCache();

            resultLabel.Text = "SQL cache cleared"; 
        }

        protected void ResetSystemMasterCacheButton_Click(object sender, EventArgs e)
        {
            LogicFactory f = LogicFactory.Get();
            f.Sys.ResetCache();
            
            resultLabel.Text = "System Master cache cleared"; 
        }
        
        protected void ResetHeapButton_Click(object sender, EventArgs e)
        {
            Heap.Clear();
            resultLabel.Text = "Heap Cleared";
        }

        protected void LogDirGoButton_Click(object sender, EventArgs e)
        {
            LogicFactory f = LogicFactory.Get();
            if (DirText.Text.isEmpty())
            {
                /// return log dir contents 
                
            }
        }
    }
}