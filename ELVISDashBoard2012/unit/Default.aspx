﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ELVISDashBoard.unit.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Destructive center</title>


<style type = "text/css">

.header 
{
}

.warningLabel
{
}

.dateIndicator
{
}

.ActionGroup
{
}

.aHead  
{
    display: block;
    clear: both; 
    height: 25px; 
    border-bottom: 1px solid black;
    font-family: Sans-Serif Verdana;
    font-weight: bolder;
    font-size: 18pt;
    text-align: right;
}

.aLine
{
    display: inline-block;
    clear: both;
    height: 35px;
}

.resultText 
{
}


</style>

<script type="text/javascript">

</script>
</head>
<body>
    <form id="form1" runat="server">

    <div class="header">
        <div class="warningLabel">
            for destructive purposes only <br />
            click below button under extreme caution <br />
            that you are willing to deal with consequences<br />
        </div>
        <div class="dateIndicator">
            <asp:Label ID="dateLabel" runat="server" Text=""></asp:Label>
        </div>
    </div>


    <div class="ActionGroup">
        <div class="aHead">Reset Cache</div>
        <div class="aLine">
            <asp:Button ID="ResetSQLCacheButton" runat="server" Text="SQL" OnClick="ResetSQLCacheButton_Click" />
            <asp:Button ID="ResetSystemMasterCacheButton" runat="server" Text="System" OnClick="ResetSystemMasterCacheButton_Click"/>
            <asp:Button ID="ResetHeapButton" runat="server" Text="Heap" OnClick="ResetHeapButton_Click"/>
        </div>
        <div class="aLine">
            <div class="resultText">
            <asp:Label ID="resultLabel" runat="server" Text="" />
            </div>
        </div>
    </div>

    <div class="ActionGroup">
        <div class="aHead">Log File Retriever</div>
        <div class="aLine">
            <asp:TextBox runat="server" ID="DirText" Width="320"/>
            <asp:Button runat="server" ID="LogDirGoButton" Text="Go" OnClick="LogDirGoButton_Click" />
        </div>
    </div>
    
    </form>
</body>
</html>
