﻿<%@ Page Title="Budget Check Test" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" AutoEventWireup="true" CodeBehind="SapBudgetCheck.aspx.cs" Inherits="ELVISDashBoard.presentationHelper.SapBudgetCheck" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pre" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
.Output
{
    font-family: Consolas, Monospace, Courier;
    font-size: 10px;
    color: Green;
}
</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Sap Budget Check Test" />
    
    <asp:UpdatePanel ID="mainPanel" runat="server">
    <Triggers>
    </Triggers>
    <ContentTemplate>
        <div class = "row">
            <label>DOC_NO</label>
            <asp:TextBox ID="DocNoText" runat="server" ClientIDMode="Static" MaxLength="9" />
        </div>
        <div class = "row">
                    <label>DOC_YEAR</label>
            <asp:TextBox ID="DocYearText" runat="server" ClientIDMode="Static" MaxLength="4" />

        </div>
        <div class = "row">
                    <label>DOC_STATUS</label>
                    <asp:DropDownList runat="server" ID="OperationBox" ClientIDMode="Static">
                        <asp:ListItem Value="0" Text="Get"/>
                        <asp:ListItem Value="1" Text="Book"/>
                        <asp:ListItem Value="2" Text="Release"/>
                        <asp:ListItem Value="3" Text="Delete"/>
                        <asp:ListItem Value="4" Text="Reverse"/>
                    </asp:DropDownList>
        </div>
        <div class="rowbutton">
            <div class="btnright">
                <asp:Button ID="RunButton" runat="server" OnClick="RunButton_Click" Text="Run" />
            </div>
        </div>

        <div class="Output">
        <asp:Label runat="server" ID="OutputLabel" ></asp:Label>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
