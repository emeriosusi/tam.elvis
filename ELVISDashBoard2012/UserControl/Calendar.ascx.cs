﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELVISDashboard.UserControl
{
    public partial class Calendar : System.Web.UI.UserControl
    {
        public bool Enable
        {
            get { return txtCalendar.Enabled; }
            set
            {
                txtCalendar.Enabled = Convert.ToBoolean(value);
                rfv.Enabled = Convert.ToBoolean(value);
                MaskedEditValidator.Enabled = Convert.ToBoolean(value);
                MaskedEditExtender.Enabled = Convert.ToBoolean(value);
                imgCalendar.Enabled = Convert.ToBoolean(value);
            }
        }

        public string Date
        {
            get { return txtCalendar.Text; }
            set { txtCalendar.Text = value; }
        }

        public bool Visible1
        {
            get { return txtCalendar.Visible; }
            set { 
                txtCalendar.Visible = Convert.ToBoolean(value);
                rfv.Visible = Convert.ToBoolean(value);
                MaskedEditValidator.Visible = Convert.ToBoolean(value);
                imgCalendar.Visible = Convert.ToBoolean(value);
            }
        }

        public string ValidationGroup
        {
            set { rfv.ValidationGroup = value; }
            get { return rfv.ValidationGroup; }
        }
        public DateTime Date_Format_ddMMyyyy
        {
            get
            {
                string[] temp = txtCalendar.Text.Trim().Split('/');
                return new DateTime(Convert.ToInt32(temp[2]), Convert.ToInt32(temp[1]), Convert.ToInt32(temp[0]));
            }
            set
            {
                txtCalendar.Text = ((DateTime)value).ToString("dd/MM/yyyy");
            }
        }
        public DateTime Date_Format_MMddyyyy
        {
            get
            {
                string[] temp = txtCalendar.Text.Trim().Split('/');
                return new DateTime(Convert.ToInt32(temp[2]), Convert.ToInt32(temp[0]), Convert.ToInt32(temp[1]));
            }
            set
            {
                txtCalendar.Text = ((DateTime)value).ToString("MM/dd/yyyy");
            }
        }
        public bool AutoPostBack
        {
            get { return txtCalendar.AutoPostBack; }
            set 
            {
                txtCalendar.AutoPostBack = Convert.ToBoolean(value);
            }
        }
        public event EventHandler TextChanged;
        protected void Page_Load(object sender, EventArgs e)
        {
            txtCalendar.TextChanged += new EventHandler(txtCalendar_TextChanged);
            if (!IsPostBack)
            {

            }
        }


        protected void txtCalendar_TextChanged(object sender, EventArgs e)
        {
            if (AutoPostBack == true)
            {
                this.TextChanged(sender, e);
            }
        }
        public string Text
        {
            get { return txtCalendar.Text.Trim(); }
        }

    }
}