﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELVISDashboard.UserControl
{
    public partial class NumberedTextbox : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region
        //public void SetMaskIDR()
        //{
        //    MaskedEditExtender.Mask = "999,999,999,999,999";
        //}

        //public void SetMaskUSD()
        //{
        //    MaskedEditExtender.Mask = "999,999,999.00";
        //}
        #endregion

        public decimal GetValue()
        {
            if (TextBox.Text != "")
            {
                return Convert.ToDecimal(TextBox.Text);
            }
            else
            {
                return 0;
            }
        }

        public void SetValue(decimal Value)
        {
            TextBox.Text = Value.ToString();
        }

        public void SetEnabled(bool Value)
        {
            TextBox.Enabled = Value;
        }
    }
}