﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using BusinessLogic.VoucherForm;
using BusinessLogic._40RVList;
using Common.Data;
using Common.Data._40RVList;
using Common.Data.SAPData;
using Common.Function;
using ELVISDashBoard._30PvFormList;

namespace ELVISDashboard._40RVFormList
{
    public partial class RVFormList : VoucherFormPage
    {
        public RVFormList() : base(VoucherFormPage.SCREEN_TYPE_RV)
        {
            _ScreenID = resx.Screen( "ELVIS_Screen_4001");
        }

        public override void Page_Load(object sender, EventArgs e)
        {
            VoucherMasterPage = Master;
            base.Page_Load(sender, e);
        }

        protected override void postbackInit()
        {
            DetailGrid = gridPVDetail;
            AttachmentGrid = gridAttachment;
            BudgetNumberListBox = lstboxMultipleBudgetNumber;
            TotalAmountLiteral = ltTotalAmount;
            MessageControlLiteral = messageControl;
            PaymentMethodLiteral = lblPaymentMethod;
            NoteTextBox = txtNotice;
            DocumentDateLabel = tboxDocDate;
            DocumentNumberLabel = tboxDocNo;
            IssuingDivisionLabel = tboxIssueDiv;
            StatusLabel = tboxStatus;
            VendorCodeLabel = lblVendorCode;
            VendorDescriptionLabel = lblVendorDesc;
            VendorGroupLabel = lblVendorGroup;
            PostingDateLabel = lblPostingDate;
            PlanningPaymentDateLabel = null; 
            BankTypeLabel = null; 
            DocumentTypeLabel = lblDocType;
            TransactionTypeLabel = lblTransactionType;
            ActivityDateStartLabel = lblActivityDate;
            ActivityDateEndLabel = lblActivityDateTo;
            BudgetNumberLabel = lblBudgetNo;
            DocumentTypeDropDown = ddlDocType;
            PaymentMethodDropDown = ddlPaymentMethod;
            TransactionTypeLookup = ddlTransactionType;
            NoteRecipientDropDown = ddlSendTo;
            AttachmentCategoryDropDown = cboxAttachmentCategory;
            CalculateTaxCheckBox = chkCalculateTax;
            ActivityDateStart = dtActivityDateFrom;
            ActivityDateEnd = dtActivityDateTo;
            PostingDate = dtPostingDate;
            PlanningPaymentDate = null; 
            VendorCodeLookup = lkpgridVendorCode;
            BudgetNumberLookup = lkpgridBudgetNo;
            BankTypeLookup = null; // lkpgridBankType;
            SimulateUserButton = new System.Web.UI.WebControls.Button(); // Rinda Rahayu 20160425
            RemainingBudgetLinkButton = new System.Web.UI.WebControls.LinkButton(); // Rinda Rahayu 20160425
            DivShowRemainingBudget = new System.Web.UI.HtmlControls.HtmlGenericControl(); // Rinda Rahayu 20160425
            DivRemainingBudget = new System.Web.UI.HtmlControls.HtmlGenericControl(); //Rinda Rahayu 20160425
            EditButton = btEdit;
            SaveButton = btSave;
            CancelButton = btCancel;
            CloseButton = btClose;
            SubmitButton = btSubmit;
            RejectButton = btReject;
            HoldButton = btHold;
            UnHoldButton = btUnHold;
            PostSAPButton = btPostToSAP;
            ReverseButton = btReverse;
            SummaryButton = btSummary;
            DataUploadButton = btnUploadTemplate;
            UploadDiv = UploadDetailDiv;
            DownloadUploadTemplateLink = lnkDownloadTemplate;
            DownloadEntertainmentTemplateLink = EntertainmentAttachDiv; 
            BudgetNumberLink = lnkBudgetNo;
            DataUpload = fuInvoiceList;
            AttachmentUpload = uploadAttachment;
            HeaderTab = PVFormListTabs;
            HiddenScreenID = hidScreenID;
            HiddenMessageExpandFlag = hdExpandFlag;
            HiddenReloadOpener = hidReloadOpener;
            HiddenPaymentMethod = hidPaymentMethod;
            HiddenGridFocusedColumn = hdGridFocusedColumn;
            HiddenFinanceAccessFlag = hdFinanceAccessFlag;
            HiddenOneTimeVendorValid = hidOneTimeVendorValid;
            
            NoteRepeater = rptrNotice;
            AttachmentPopup = popdlgUploadAttachment;
            NoteCommentContainer = noticeComment;

            ActivitDateToSeparatorLabel = lblTo;
            VendorAdditionPopup = popAddVendor;
            VendorAdditionButton = btAddNewVendor;
            NewVendorName = tboxNewVendorName;
            NewVendorSearchTerm = tboxNewVendorSearchTerm;

            SimulateGrid = gridSimulation;
            SimulatePanel = panelSimulation;
            SimulatePopUp = popUpSimulation;

            SAPGrid = gridpopSAPDocNumber;
            SAPpop = popSAPDocNumber;

        }

        protected override string PrintCover()
        {
            PVFormData f = FormData;
            OrderedDictionary ta = Tabel.TotalAmountMap();
            decimal tAmt = f.GetTotalAmount();
            string amt = tAmt.fmt(0);
            RVListData d = new RVListData()
            {
                RV_NO = f.PVNumber.str(),
                RV_DATE = f.PVDate ?? DateTime.Now,
                RV_YEAR = f.PVYear.str()
            };

            List<RVListData> listData = new List<RVListData>();
            ErrorData _Err = new ErrorData();
            (lilo as RVListLogic).Fill(d);
            listData.Add(d);
            
            return logic.RVPrint.printCover(Page,
                    System.IO.Path.GetDirectoryName(Server.MapPath(Common.AppSetting.CompanyLogo)),
                    Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE", "DIR")),
                    UserData,
                    listData,
                    ref _Err);
        }

        protected override bool isSettlement(int Code)
        {
            return (Code == 2);
        }

        protected override bool HoldVisibleStatus(int? statusCD)
        {
            //return (statusCD ?? 0) < 68;
            return !((statusCD ?? 0)== 68);
        }

        protected override OneTimeVendorData OneTimeVendor()
        {
            return null; 
        }
    }
}