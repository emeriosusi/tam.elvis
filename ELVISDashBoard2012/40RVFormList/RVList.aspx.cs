﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._40RVFormList;
using Common.Data._40RVList;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Data.Helpers;
using ELVISDashBoard.presentationHelper;
using DataLayer;
using System.IO;

namespace ELVISDashBoard._40RvFormList
{
    public partial class RVList : BaseCodeBehind
    {
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_4002");

        private SelectableDropDownHelper sel = null;

        private string[][] aUser = new string[][] 
        {
            new string [] { "ACTUAL_DT_SH", "PLAN_DT_SH", 
                "ACTUAL_LT_SH", "PLAN_LT_SH", "SKIP_FLAG_SH"},
            new string [] { "ACTUAL_DT_DPH", "PLAN_DT_DPH", 
                "ACTUAL_LT_DPH", "PLAN_LT_DPH", "SKIP_FLAG_DPH"},
            new string [] { "ACTUAL_DT_DH", "PLAN_DT_DH", 
                "ACTUAL_LT_DH", "PLAN_LT_DH", "SKIP_FLAG_DH"}
        };
        private string[][] aFin = new string[][] 
        {
            new string [] { "ACTUAL_DT_RECEIVED", "PLAN_DT_RECEIVED", 
                "ACTUAL_LT_RECEIVED", "PLAN_LT_RECEIVED", "SKIP_FLAG_RECEIVED"},
            new string [] { "ACTUAL_DT_VERIFIED_FINANCE", "PLAN_DT_VERIFIED_FINANCE", 
                "ACTUAL_LT_VERIFIED_FINANCE", "PLAN_LT_VERIFIED_FINANCE", 
                "SKIP_FLAG_VERIFIED_FINANCE"},
            new string [] { "ACTUAL_DT_APPROVED_FINANCE_SH", "PLAN_DT_APPROVED_FINANCE_SH", 
                "ACTUAL_LT_APPROVED_FINANCE_SH", "PLAN_LT_APPROVED_FINANCE_SH",
                "SKIP_FLAG_APPROVED_FINANCE_SH"},
            new string [] { "ACTUAL_DT_APPROVED_FINANCE_DPH", "PLAN_DT_APPROVED_FINANCE_DPH", 
                "ACTUAL_LT_APPROVED_FINANCE_DPH", "PLAN_LT_APPROVED_FINANCE_DPH",
                "SKIP_FLAG_APPROVED_FINANCE_DPH"},
            new string [] { "ACTUAL_DT_CLOSING", "PLAN_DT_CLOSING", 
                "ACTUAL_LT_CLOSING", "PLAN_LT_CLOSING", 
                "SKIP_FLAG_CLOSING"}
	    };


        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        private Common.Enum.SAPMode SAPMode
        {
            set
            {
                ViewState["SAPMode"] = value;
            }
            get
            {
                return (Common.Enum.SAPMode)ViewState["SAPMode"];
            }
        }

        private int? roleClass = null;
        protected int RoleClass
        {
            get
            {
                if (roleClass == null)
                {

                    roleClass = logic.Look.GetRoleClassOf(UserData);

                }
                return (int)roleClass;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            hidScreenID.Value = _ScreenID;

            ASPxListBox statusList = (ASPxListBox)DropDownListLastStatus.FindControl("LastStatusListBox");
            statusList.DataSource = logic.RVList.getDocumentStatus();
            statusList.DataBind();

            sel = new SelectableDropDownHelper(IssuingDivisionDropDown, "IssuingDivisionListBox");
            ASPxListBox divBox = (ASPxListBox)IssuingDivisionDropDown.FindControl("IssuingDivisionListBox");


            lit = litMessage;

            if (!IsPostBack && !IsCallback)
            {

                if (divBox != null) sel.SelectAll();
                PrepareLogout();

                HiddenCanSelectAll.Value = "1";


                GenerateComboData(DropDownListRVType, "RV_LIST_TYPE", true);

                GenerateComboData(DropDownListWorkflowStatus, "WORKFLOW_STATUS", true);


                GenerateComboData(DropDownListNotice, "NOTIFICATION", true);
                GenerateComboData(DropDownListHoldFlag, "HOLD_FLAG", true);

                GenerateComboData(ddlPaymentMethod, "RECEIVE_METHOD", true);
                GenerateComboData(ddlVendorGroup, "VENDOR_GROUP", true);

                gridGeneralInfo.DataSource = null;

                gridStatusUser.DataSource = null;

                gridStatusFinance.DataSource = null;

                SetFirstLoad();
                HiddenSearchedTabIdx0.Value = "0";
                HiddenSearchedTabIdx1.Value = "0";
                HiddenSearchedTabIdx2.Value = "0";

                #region Init Startup Script
                AddEnterComponent(dtRVDateFrom);
                AddEnterComponent(dtRVDateTo);
                AddEnterComponent(ddlTransactionType);
                AddEnterComponent(TextBoxRVNoFrom);
                AddEnterComponent(TextBoxRVNoTo);
                AddEnterComponent(ddlPaymentMethod);
                AddEnterComponent(DropDownListRVType);
                AddEnterComponent(ddlVendorGroup);
                AddEnterComponent(TextBoxVendorCode);
                AddEnterComponent(TextBoxVendorDesc);
                AddEnterComponent(dtActivityDateFrom);
                AddEnterComponent(dtActivityDateTo);
                AddEnterComponent(dtSubmitDateFrom);
                AddEnterComponent(dtSubmitDateTo);
                

                if (logic.User.IsAuthorizedFinance || logic.User.IsDirectorUp || UserData.Roles.Contains("ELVIS_ADMIN"))

                    AddEnterComponent(IssuingDivisionDropDown);
                else
                {
                    IssuingDivisionDropDown.Enabled = false;

                }

                AddEnterComponent(DropDownListWorkflowStatus);
                AddEnterComponent(DropDownListLastStatus);

                AddEnterComponent(DropDownListNotice);
                AddEnterComponent(DropDownListHoldFlag);
                AddEnterComponent(CreatedByEdit);
                AddEnterComponent(NextApproverEdit);
                AddEnterAsSearch(ButtonSearch, _ScreenID);
                #endregion

                #region Init Parameter
                //String varPaidDate = Request.QueryString["PaidDate"];
                //DateTime _paidDate = DateTime.Now;
                #endregion
                //if (!String.IsNullOrEmpty(varPaidDate) && !String.IsNullOrEmpty(varPaidDate)
                //    && DateTime.TryParse(varPaidDate, out _paidDate))
                //{
                //    dtRVDateFrom.Value = new DateTime(DateTime.Now.Year, 1, 1);
                //    dtRVDateTo.Value = new DateTime(DateTime.Now.Year, 12, 31);
                //    //dtPaidDateFrom.Value = _paidDate;
                //    //dtPaidDateTo.Value = _paidDate;

                //    #region Search data
                //    //if (validateInputSearch())
                //    //{
                //    //    LoadData();
                //    //}
                //    #endregion
                //}
                bool reload = false;
                String vDate = Request.QueryString["date"];
                DateTime d = System.DateTime.Now;
                if (!String.IsNullOrEmpty(vDate) && DateTime.TryParse(vDate, out d))
                {
                    dtRVDateFrom.Value = d;
                    dtRVDateTo.Value = d;
                    reload = true;
                }
                String docno = Request.QueryString["doc_no"];
                decimal DOC_NO;
                if (!String.IsNullOrEmpty(docno) && decimal.TryParse(docno, out DOC_NO))
                {
                    TextBoxRVNoFrom.Text = docno;
                    TextBoxRVNoTo.Text = docno;
                    reload = true;
                }
                if ((dtRVDateFrom.Value == null) || (dtRVDateTo.Value == null))
                {
                    int days = logic.Sys.GetText("PvList", "INITIAL_LIST_DAYS").Int(7) * -1;
                    dtRVDateFrom.Value = d.AddDays(days);
                    dtRVDateTo.Value = d;
                }
                if (reload || Common.AppSetting.LIST_ON_LOAD)
                    LoadData();
            }
        }

        private RoleLogic _rolelogic = null;
        private RoleLogic RoLo
        {
            get
            {
                if (_rolelogic == null)
                {
                    _rolelogic = new RoleLogic(UserData.USERNAME, _ScreenID);
                }
                return _rolelogic;
            }
        }

        private List<WorklistHelper> Worklist
        {
            get
            {
                return logic.VoucherList.GetWorklist(UserName);
            }
        }

        private void SetFirstLoad()
        {
            ButtonSearch.Visible = RoLo.isAllowedAccess("ButtonSearch");
            ButtonClear.Visible = true;
            ButtonPrintWorklist.Visible = RoLo.isAllowedAccess("ButtonPrintWorklist");
            ButtonNewRV.Visible = RoLo.isAllowedAccess("ButtonNewRV");
            ButtonDirectToMe.Visible = false; //  _RoleLogic.isAllowedAccess("ButtonDirectToMe");
            ButtonEdit.Visible = false;
            ButtonDelete.Visible = false;
            ButtonDownload.Visible = false;
            ButtonHistory.Visible = false;
            ButtonPrintCover.Visible = false;
            ButtonPrintTicket.Visible = false;
            ButtonPostSAP.Visible = false;
            ButtonCheckApp.Visible = false;
        }

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                string _no = e.GetValue("RV_NO").str();
                string _trans = e.GetValue("TRANSACTION_CD").str(); //FID.Ridwan
                string _yy = e.GetValue("RV_YEAR").str();

                object _notices = e.GetValue("NOTICES");
                if (_notices != null)
                {
                    Literal litGridNotices = gridGeneralInfo.FindRowCellTemplateControl(
                        e.VisibleIndex, gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "litGridNotices") as Literal;
                    int notices = 0;
                    Int32.TryParse(_notices.ToString(), out notices);
                    if (notices > 0)
                    {
                        litGridNotices.Text = "(" + _notices + ")";
                        litGridNotices.Visible = true;
                    }
                    else
                    {
                        litGridNotices.Text = "";
                        litGridNotices.Visible = false;
                    }
                }

                object _HoldFlag = e.GetValue("HOLD_FLAG");
                Literal litGridHoldFlag = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                    gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "litGridHoldFlag") as Literal;
                if (_HoldFlag != null)
                {
                    int holdFlag = 0;
                    if (!Int32.TryParse(_HoldFlag.ToString(), out holdFlag)) holdFlag = 0;
                    if (holdFlag > 0)
                    {
                        litGridHoldFlag.Visible = true;
                        litGridHoldFlag.Text = "*";
                    }
                    else
                    {
                        litGridHoldFlag.Visible = false;
                        litGridHoldFlag.Text = " ";
                    }
                }
                else
                {
                    litGridHoldFlag.Visible = false;
                    litGridHoldFlag.Text = " ";
                }

                bool showOthers = e.GetValue("OTHER_CURRENCY").str().Int(0) > 0;

                LinkButton aCurr = gridGeneralInfo.FindRowCellTemplateControl(
                    e.VisibleIndex,
                    gridGeneralInfo.Columns["TOTAL_AMOUNT_IDR"] as GridViewDataColumn,
                    "linkSeeAllCurrency") as LinkButton;

                Literal litIDR = gridGeneralInfo.FindRowCellTemplateControl(
                    e.VisibleIndex,
                    gridGeneralInfo.Columns["TOTAL_AMOUNT_IDR"] as GridViewDataColumn,
                    "litGridTotalAmountIDR") as Literal;

                HyperLink hypDetail = gridGeneralInfo.FindRowCellTemplateControl(
                        e.VisibleIndex, gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn
                        , "hypDetail") as HyperLink;
                if (hypDetail != null)
                {
                    hypDetail.NavigateUrl = Page.GetUrl(RoleClass, _no, _yy, new DateTime(), _trans);
                    hypDetail.Text = _no;
                }

                aCurr.Visible = showOthers;
                litIDR.Visible = !showOthers;

            }
        }

        protected void ddlTransactionType_Load(object sender, EventArgs arg)
        {
            ddlTransactionType.DataSource = logic.Persist.getTransactionTypes(
                    UserData, -1, -1, false, true, logic.User.IsAuthorizedFinance, UserCanUploadDetail);
            ddlTransactionType.DataBind();
        }

        protected void gridStatusUser_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ColorifyCell(aUser, e);
        }

        protected void gridStatusFinance_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ColorifyCell(aFin, e);
        }
        protected void dxVendor_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            e.QueryableSource = logic.RVList.GetVendorCodes();
            e.KeyExpression = "VENDOR_CD";
        }

        protected void SetMagic(ASPxGridView g, int pageSize)
        {
            if (g.SettingsPager.PageSize > 128)
            {
                ServerModeKeyedCache.DefaultMagicNumberFetchRowsTop = g.SettingsPager.PageSize;
                ServerModeKeyedCache.DefaultMagicNumberFetchKeysAllThreshold = g.SettingsPager.PageSize;
            }
            else
            {
                ServerModeKeyedCache.DefaultMagicNumberFetchRowsTop = pageSize;
                ServerModeKeyedCache.DefaultMagicNumberFetchKeysAllThreshold = pageSize;
            }
        }

        protected void LinqRVList_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            SetMagic(gridGeneralInfo, PageSize);
            e.QueryableSource = logic.RVList.SearchListIqueryable(SearchCriteria());

            e.KeyExpression = "ROW_NUMBER";
        }

        protected void LinqRVListStatusUser_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            SetMagic(gridStatusUser, PageSize);
            e.QueryableSource = logic.RVList.SearchListStatusUserIqueryable(SearchCriteria());

            e.KeyExpression = "ROW_NUMBER";
        }

        protected void LinqRVListStatusFinance_Selecting(object sender, DevExpress.Data.Linq.LinqServerModeDataSourceSelectEventArgs e)
        {
            SetMagic(gridStatusFinance, PageSize);
            e.QueryableSource = logic.RVList.SearchListStatusFinanceIqueryable(SearchCriteria());

            e.KeyExpression = "ROW_NUMBER";
        }

        protected void dsDirectToMe_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.RVList.GetDirectToMeReasons(UserName);
        }

        protected void rvListTabs_ActiveTabChanged(object sender, EventArgs e)
        {
            if (validateInputSearch())
            {
                checkForActiveTabChanged();
            }
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = logic.RVList.GetUserDivisions(UserData);
        }

        private void checkForActiveTabChanged()
        {
            if (RVListTabs.ActiveTabIndex == 0)
            {
                gridGeneralInfo.DataSourceID = LinqRVList.ID;
                gridGeneralInfo.DataBind();
                HiddenSearchedTabIdx0.Value = "1";
            }
            else if (RVListTabs.ActiveTabIndex == 1)
            {
                gridStatusUser.DataSourceID = LinqRVListStatusUser.ID;
                gridStatusUser.DataBind();
                WidthSet(gridStatusUser, aUser, ListColumnWidths);
                HiddenSearchedTabIdx1.Value = "1";

            }
            else if (RVListTabs.ActiveTabIndex == 2)
            {

                gridStatusFinance.DataSourceID = LinqRVListStatusFinance.ID;
                gridStatusFinance.DataBind();
                WidthSet(gridStatusFinance, aFin, ListColumnWidths);
                HiddenSearchedTabIdx2.Value = "1";

            }
        }

        private bool validateInputSearch()
        {
            if (dtRVDateTo.Date.CompareTo(dtRVDateFrom.Date) < 0)
            {
                Nag("MSTD00029ERR", "RV Date From", "RV Date To");
                SetFocus(dtRVDateTo);
                return false;
            }

            if ((!string.IsNullOrEmpty(dtActivityDateFrom.Text) && string.IsNullOrEmpty(dtActivityDateTo.Text)) ||
                (string.IsNullOrEmpty(dtActivityDateFrom.Text) && !string.IsNullOrEmpty(dtActivityDateTo.Text)))
            {
                if (string.IsNullOrEmpty(dtActivityDateFrom.Text))
                {
                    Nag("MSTD00017WRN", "Activity Date From");
                    return false;
                }
                if (string.IsNullOrEmpty(dtActivityDateTo.Text))
                {
                    Nag("MSTD00017WRN", "Activity Date To");
                    return false;
                }
            }
            else
            {
                if (dtActivityDateTo.Date.CompareTo(dtActivityDateFrom.Date) < 0)
                {
                    Nag("MSTD00029ERR", "Activity Date From", "Activity Date To");
                    return false;
                }
            }

            if ((!string.IsNullOrEmpty(dtSubmitDateFrom.Text) && string.IsNullOrEmpty(dtSubmitDateTo.Text)) ||
                (string.IsNullOrEmpty(dtSubmitDateFrom.Text) && !string.IsNullOrEmpty(dtSubmitDateTo.Text)))
            {
                if (string.IsNullOrEmpty(dtSubmitDateFrom.Text))
                {
                    Nag("MSTD00017WRN", "Submit Date From");
                    return false;
                }
                if (string.IsNullOrEmpty(dtSubmitDateTo.Text))
                {
                    Nag("MSTD00017WRN", "Submit Date To");
                    return false;
                }
            }
            else
            {
                if (dtSubmitDateTo.Date.CompareTo(dtSubmitDateFrom.Date) < 0)
                {
                    Nag("MSTD00029ERR", "Submit Date From", "Submit Date To");
                    return false;
                }
            }

            //if ((!string.IsNullOrEmpty(dtPaidDateFrom.Text) && string.IsNullOrEmpty(dtPaidDateTo.Text)) ||
            //    (string.IsNullOrEmpty(dtPaidDateFrom.Text) && !string.IsNullOrEmpty(dtPaidDateTo.Text)))
            //{
            //    if (string.IsNullOrEmpty(dtPaidDateFrom.Text))
            //    {
            //        Nag("MSTD00017WRN", "Paid Date From");
            //        return false;
            //    }
            //    if (string.IsNullOrEmpty(dtPaidDateTo.Text))
            //    {
            //        Nag("MSTD00017WRN", "Paid Date To");
            //        return false;
            //    }
            //}
            //else
            //{
            //    if (dtPaidDateTo.Date.CompareTo(dtPaidDateFrom.Date) < 0)
            //    {
            //        Nag("MSTD00029ERR", "Paid Date From", "Paid Date To");
            //        return false;
            //    }
            //}

            if (!string.IsNullOrEmpty(TextBoxRVNoTo.Text))
            {
                if (TextBoxRVNoTo.Text.Length > Int32.MaxValue.ToString().Length)
                {
                    litMessage.Text += Nagging("MSTD00032WRN", "RV No To", Int32.MaxValue.ToString().Length);
                    return false;
                }
                if (Convert.ToInt64(TextBoxRVNoTo.Text) > Int32.MaxValue)
                {
                    litMessage.Text += Nagging("MSTD00033WRN", "RV No To", Int32.MaxValue.ToString());
                    return false;
                }
                if (string.IsNullOrEmpty(TextBoxRVNoFrom.Text))
                {
                    litMessage.Text += Nagging("MSTD00017WRN", "RV No From");
                    return false;
                }

                if (TextBoxRVNoFrom.Text.Length > Int32.MaxValue.ToString().Length)
                {
                    litMessage.Text += Nagging("MSTD00032WRN", "RV No From", Int32.MaxValue.ToString().Length);
                    return false;
                }
                long tmp;
                if (!Int64.TryParse(TextBoxRVNoFrom.Text, out tmp))
                {
                    litMessage.Text += Nagging("MSTD00018WRN", "RV No From");
                    return false;
                }
                if (Convert.ToInt64(TextBoxRVNoFrom.Text) > Int32.MaxValue)
                {
                    litMessage.Text += Nagging("MSTD00033WRN", "RV No From", Int32.MaxValue.ToString());
                    return false;
                }
                if (Convert.ToInt32(TextBoxRVNoFrom.Text) > Convert.ToInt32(TextBoxRVNoTo.Text))
                {
                    litMessage.Text += Nagging("MSTD00030ERR", "RV No From", "RV No To");
                    return false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(TextBoxRVNoFrom.Text))
                {
                    string tmpPvNoFrom = TextBoxRVNoFrom.Text.Trim().Replace("*", "");
                    if (!String.IsNullOrEmpty(tmpPvNoFrom))
                    {
                        long tmp;
                        if (!Int64.TryParse(tmpPvNoFrom, out tmp))
                        {
                            litMessage.Text += Nagging("MSTD00036WRN", "RV No From");
                            return false;
                        }
                    }
                }
            }

            return true;
        }
        public readonly string PAGE_SIZE = "rv_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }

        new const string GridCustomPageSizeName = "gridCustomPageSizeRV";
        const string GridOtherCustomPageSizeName = "gridCustomPageSize";
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
                gridStatusUser.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
                gridStatusFinance.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
            else if (Session[GridOtherCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridOtherCustomPageSizeName];
                gridStatusUser.SettingsPager.PageSize = (int)Session[GridOtherCustomPageSizeName];
                gridStatusFinance.SettingsPager.PageSize = (int)Session[GridOtherCustomPageSizeName];
                Session[GridCustomPageSizeName] = Session[GridOtherCustomPageSizeName];
            }
        }

        protected void grid_CustomCallback(object sender,
            DevExpress.Web.ASPxGridView.ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            int newSize;
            if (!int.TryParse(e.Parameters.ToString(), out newSize)) return;

            if (newSize < 0)
                newSize = 0;
            grid.SettingsPager.PageSize = newSize;
            Session[GridCustomPageSizeName] = newSize;
            if (grid != null && e != null && !String.IsNullOrEmpty(e.Parameters) && grid.VisibleRowCount > 0)
            {
                if (validateInputSearch())
                {
                    grid.DataBind();
                }
            }
        }

        private void setPageSize(int newSize)
        {
            if (RVListTabs.ActiveTabIndex == 0)
                gridGeneralInfo.SettingsPager.PageSize = newSize;
            else if (RVListTabs.ActiveTabIndex == 1)
                gridStatusUser.SettingsPager.PageSize = newSize;
            else if (RVListTabs.ActiveTabIndex == 2)
                gridStatusFinance.SettingsPager.PageSize = newSize;
            Session[GridCustomPageSizeName] = newSize;
        }


        protected string WriteSelectedIndexInfo(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        protected string WriteSelectedIndexUser(int pageSize)
        {
            return pageSize == gridStatusUser.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }

        protected string WriteSelectedIndexFinance(int pageSize)
        {
            return pageSize == gridStatusFinance.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        protected string Status_Color(Object statusValue)
        {
            string color = "";
            short statusInt = 0;
            if (statusValue != null && Int16.TryParse(statusValue.ToString(), out statusInt))
            {
                if (statusInt == 0)
                    color = red;
                else if (statusInt == 1)
                    color = green;
            }
            return color;
        }

        protected void lkpgridVendorCode_Load(object sender, EventArgs arg)
        {
            //List<VendorCdData> _list = logic.Look.GetVendorCodeData("", "");
            //TextBoxVendorCode.DataSource = _list;
            //TextBoxVendorCode.DataBind();
        }

        private void LoadGridPopUpVendorCd()
        {
            List<VendorCdData> _list = logic.Look.GetVendorCodeData(txtSearchCd.Text, txtSearchDesc.Text);
            gridPopUpVendorCd.DataSource = _list;
            gridPopUpVendorCd.DataBind();
        }
        protected void gridPopUpVendorCd_PageIndexChanged(object sender, EventArgs e)
        {
            LoadGridPopUpVendorCd();
        }

        protected void gridPopUpVendorCd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoVendorCd = gridPopUpVendorCd.FindRowCellTemplateControl(e.VisibleIndex,
                    gridPopUpVendorCd.Columns["NO"] as GridViewDataColumn, "litGridNoVendorCd") as Literal;
                litGridNoVendorCd.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        private void ClearModal()
        {
            txtSearchCd.Text = "";
            txtSearchDesc.Text = "";
        }

        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            // Clear search criteria
            dtRVDateFrom.Text = null;
            dtRVDateTo.Text = null;
            ddlTransactionType.Value = 0;
            TextBoxRVNoFrom.Text = null;
            TextBoxRVNoTo.Text = null;
            ddlPaymentMethod.SelectedIndex = 0;
            ddlVendorGroup.SelectedIndex = 0;
            DropDownListRVType.SelectedIndex = 0;
            TextBoxVendorCode.Text = null;
            TextBoxVendorDesc.Text = null;
            dtActivityDateFrom.Text = null;
            dtActivityDateTo.Text = null;
            dtSubmitDateFrom.Text = null;
            dtSubmitDateTo.Text = null;
            //dtPaidDateFrom.Text = null;
            //dtPaidDateTo.Text = null;
            if (DropDownListIssuingDivision.Enabled)
                DropDownListIssuingDivision.SelectedIndex = 0;
            if (IssuingDivisionDropDown.Enabled)
                sel.Clear();

            DropDownListWorkflowStatus.SelectedIndex = 0;

            DropDownListNotice.SelectedIndex = 0;
            DropDownListHoldFlag.SelectedIndex = 0;

            clearSelectedStatus();
            gridGeneralInfo.DataSource = null;
            gridGeneralInfo.DataSourceID = null;
            gridStatusFinance.DataSource = null;
            gridStatusFinance.DataSourceID = null;
            gridStatusUser.DataSource = null;
            gridStatusUser.DataSourceID = null;

            gridGeneralInfo.DataBind();
            gridStatusUser.DataBind();
            gridStatusFinance.DataBind();
            //HiddenSearchedFlag.Value = "0";
            HiddenSearchedTabIdx0.Value = "0";
            HiddenSearchedTabIdx1.Value = "0";
            HiddenSearchedTabIdx2.Value = "0";
            ButtonDirectToMe.Visible = false;
            ButtonEdit.Visible = false;
            ButtonDelete.Visible = false;
            ButtonDownload.Visible = false;
            ButtonHistory.Visible = false;
            ButtonPrintCover.Visible = false;
            ButtonPrintTicket.Visible = false;
            ButtonPostSAP.Visible = false;
            ButtonCheckApp.Visible = false;
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            if (validateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridStatusUser.Selection.UnselectAll();
                gridStatusFinance.Selection.UnselectAll();

                SearchData();

                PageMode = Common.Enum.PageMode.View;
            }
        }
        protected void clearSelectedStatus()
        {
            ASPxListBox statusList = (ASPxListBox)DropDownListLastStatus.FindControl("LastStatusListBox");
            statusList.UnselectAll();
            DropDownListLastStatus.Text = String.Empty;
        }


        private void SearchData()
        {
            if (RVListTabs.ActiveTabIndex == 0)
            {
                HiddenSearchedTabIdx0.Value = "1";
                HiddenSearchedTabIdx1.Value = "0";
                HiddenSearchedTabIdx2.Value = "0";

                gridGeneralInfo.DataSourceID = LinqRVList.ID;
                gridGeneralInfo.DataBind();
                SetCancelEditDetail(gridGeneralInfo);
            }
            else if (RVListTabs.ActiveTabIndex == 1)
            {
                HiddenSearchedTabIdx0.Value = "0";
                HiddenSearchedTabIdx1.Value = "1";
                HiddenSearchedTabIdx2.Value = "0";

                gridStatusUser.DataSourceID = LinqRVListStatusUser.ID;
                gridStatusUser.DataBind();
                SetCancelEditDetail(gridStatusUser);
            }
            else if (RVListTabs.ActiveTabIndex == 2)
            {
                HiddenSearchedTabIdx0.Value = "0";
                HiddenSearchedTabIdx1.Value = "0";
                HiddenSearchedTabIdx2.Value = "1";
                gridStatusFinance.DataSourceID = LinqRVListStatusFinance.ID;
                gridStatusFinance.DataBind();
                SetCancelEditDetail(gridStatusFinance);
            }
        }


        private void SetCancelEditDetail(ASPxGridView gridView)
        {
            if (gridView.VisibleRowCount > 0)
            {
                ButtonDirectToMe.Visible = RoLo.isAllowedAccess("ButtonDirectToMe");
                ButtonEdit.Visible = RoLo.isAllowedAccess("ButtonEdit");
                ButtonDelete.Visible = RoLo.isAllowedAccess("ButtonDelete");
                ButtonDownload.Visible = true;
                ButtonHistory.Visible = true;
                ButtonPrintCover.Visible = RoLo.isAllowedAccess("ButtonPrintCover");
                ButtonPrintTicket.Visible = RoLo.isAllowedAccess("ButtonPrintTicket");
                ButtonPostSAP.Visible = RoLo.isAllowedAccess("ButtonPostSAP");
                ButtonCheckApp.Visible = RoLo.isAllowedAccess("ButtonCheckApp");
            }
        }

        private void ProcessEdit_Click(ASPxGridView gridView)
        {
            if (gridView.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            if (gridView.Selection.Count > 1)
            {
                Nag("MSTD00016WRN");
                return;
            }
                
            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (!gridView.Selection.IsRowSelected(i)) continue;

                string rvNo = gridView.GetRowValues(i, "RV_NO").ToString();
                string rvYear = gridView.GetRowValues(i, "RV_YEAR").ToString();
                string folio = rvNo + rvYear;
                logic.Say("ProcessEdit_Click", "Edit " + rvNo);
                HiddenField hidGridRVYear = gridView.FindRowCellTemplateControl(i,
                    gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;
                HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(i,
                    gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;
                if (hidGridStatusCd.Value != null && (hidGridStatusCd.Value.ToString().Equals("0")))
                {
                    //fid.Hadid
                    object susNo = gridView.GetRowValues(i, "SUSPENSE_NO");
                    int suNo = susNo == null ? 0 : Convert.ToInt32(susNo);
                    string bookingNo = logic.Look.getBookingNoBySuspense(suNo, 0);
                    
                    if(bookingNo == null) 
                    //end fid.Hadid
                    {
                        //Page.ClientScript.RegisterStartupScript(this.GetType(),
                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "ELVIS_RVFormList",
                        "openWin('RVFormList.aspx?mode=edit&rv_no=" + rvNo + "&rv_year=" + hidGridRVYear.Value.ToString() + "','ELVIS_RVFormList');",
                        true);
                    }
                    else
                    {
                        //fid.Hadid
                        ScriptManager.RegisterStartupScript(this, this.GetType(),
                        "ELVIS_RVFormList",
                        "openWin('../80Accrued/AccrRVForm.aspx?mode=edit&rv_no=" + rvNo + "&rv_year=" + hidGridRVYear.Value.ToString() + "','ELVIS_RVFormList');",
                        true);
                        //end fid.Hadid
                    }

                }
                else
                {
                    Nag("MSTD00034WRN", "RV", rvNo);
                }
            }

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (RVListTabs.ActiveTabIndex == 0)
                ProcessEdit_Click(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                ProcessEdit_Click(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                ProcessEdit_Click(gridStatusFinance);
        }

        protected void ButtonClose_Click(object sender, EventArgs e)
        {

        }

        private void ProcessDelete_Click(ASPxGridView gridView)
        {
            if (gridView.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            Boolean checkValid = true;
            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (gridView.Selection.IsRowSelected(i))
                {
                    string rvNo = gridView.GetRowValues(i, "RV_NO").ToString();
                    logic.Say("ProcessDelete_Click", "Delete " + rvNo);
                    HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(i,
                        gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;
                    HiddenField hidGridRVYear = gridView.FindRowCellTemplateControl(i,
                        gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;
                    int _no, _yy;
                    _no = rvNo.Int();
                    _yy = hidGridRVYear.Value.Int();
                    string folio = rvNo + hidGridRVYear.Value.str();
                    
                    PVFormData p = logic.RVForm.search(_no, _yy);
                    int StatusCd = p.StatusCode ?? -1;

                    if (  !((p.StatusCode != null && (p.StatusCode == 0 || p.StatusCode == 71))
                           || p.Canceled || inWorklist(folio))  )
                    //if (hidGridStatusCd.Value != null && !hidGridStatusCd.Value.ToString().Equals("0"))
                    {
                        checkValid = false;
                        Nag("MSTD00034WRN", "RV", rvNo);
                        break;
                    }
                }
            }

            if (checkValid)
            {
                lblConfirmationDelete.Text = _m.Message("MSTD00008CON", "delete");
                ConfirmationDeletePopUp.Show();
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (RVListTabs.ActiveTabIndex == 0)
                ProcessDelete_Click(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                ProcessDelete_Click(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                ProcessDelete_Click(gridStatusFinance);
        }

        private void ProcessConfirmationDelete_Click(ASPxGridView gridView)
        {
            logic.Say("ProcessConfirmationDelete_Click", "Confirm Delete");
            bool result = false;
            if (gridView.Selection.Count < 1) return;
            List<RVFormData> listRVNo = new List<RVFormData>();
            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (!gridView.Selection.IsRowSelected(i)) continue;
                RVFormData rvFormData = new RVFormData();
                HiddenField hidGridRVYear = gridView.FindRowCellTemplateControl(i,
                    gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;

                rvFormData.RVNumber = Convert.ToInt32(gridView.GetRowValues(i, "RV_NO"));
                rvFormData.RVYear = Convert.ToInt32(hidGridRVYear.Value);
                string folio = rvFormData.RVNumber.str() + rvFormData.RVYear.str();
                
                PVFormData p = logic.RVForm.search(rvFormData.RVNumber, rvFormData.RVYear);
                int StatusCd = p.StatusCode??-1;
                
                HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(i,
                    gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;

                if ( (p.StatusCode!=null && (p.StatusCode == 0 || p.StatusCode == 71))
                    || p.Canceled || inWorklist(folio))
                    listRVNo.Add(rvFormData);
            }

            if (listRVNo.Count > 0)
            {
                PageMode = Common.Enum.PageMode.View;
                result = logic.RVList.Delete(listRVNo, UserData.USERNAME);
                if (!result)
                {
                    Nag("MSTD00014ERR");
                    gridView.Selection.UnselectAll();
                }
                else
                {
                    Nag("MSTD00015INF");
                    gridView.Selection.UnselectAll();
                    LoadData();
                }
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            if (RVListTabs.ActiveTabIndex == 0)
                ProcessConfirmationDelete_Click(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                ProcessConfirmationDelete_Click(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                ProcessConfirmationDelete_Click(gridStatusFinance);
        }

        protected void BtnCancelConfirmationDelete_Click(object sender, EventArgs e)
        {

        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            // gather all PV to be Directed 
            logic.Say("btnGo_Click", "Go");
            List<RVListData> listData = null;
            if (RVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);

            String cashier = logic.RVList.getCashierName();
            string _Comment = "Direct to Me Reason:" + ddlReason.Text;

            if (listData.Count > 0)
            {
                StringBuilder x = new StringBuilder("");
                int seli = 0;
                foreach (RVListData sel in listData)
                {
                    if (seli > 0) x.Append(",");
                    seli++;
                    x.Append(sel.RV_NO);
                }
                string documents = x.ToString();
                logic.Say("btnGo_Click",
                        "Direct To Me:{0} list: {1}",
                        UserName, documents);
                x.Clear();

                try
                {
                    string uName = UserName;
                    decimal divCd = ((UserData != null) ? UserData.DIV_CD : 0);
                    int Redirected = 0;
                    // send reason to all recipient 
                    foreach (var data in listData)
                    {
                        if (UserData == null)
                        {
                            string timeoutErr = "Login Info empty, session timeout or erased";
                            x.AppendLine(timeoutErr);
                            // LoggingLogic.say(timeoutErr);
                            break;
                        }

                        if (logic.RVList.isCanDirect(data.RV_NO, data.RV_YEAR, uName, divCd) && data.STATUS_CD != "0")
                        {
                            //string byPassUser = logic.RVList.GetLastUser(data.RV_NO, data.RV_YEAR);

                            List<string> lstUser = null;

                            int? statusCd = logic.PVApproval.SearchStatusFromDistribution(data.RV_NO, data.RV_YEAR, uName);
                            if (statusCd != null)
                            {
                                lstUser = logic.Notice.listWorkflowRecipient(data.RV_NO, data.RV_YEAR, statusCd ?? 0);
                            }
                            if (lstUser == null)
                            {
                                lstUser = new List<string>();
                            }

                            #region Do --> Approve-Reject
                            bool Ok = logic.PVRVWorkflow.Redirect(data.RV_NO + data.RV_YEAR, UserData, data.NEXT_APPROVER);
                            string nagCode = Ok ? "MSTD00060INF" : "MSTD00062ERR";

                            if (Ok)
                            {
                                //int seq = 0;
                                ErrorData err = new ErrorData(0, "", "");
                                string userRole;
                                List<RoleData> uRole = RoLo.getUserRole(uName, ref err);
                                userRole = (uRole != null && uRole.Count > 0) ? uRole[0].ROLE_NAME : "";

                                logic.Say("btnGo_Click",
                                        "{0} Direct To Me: {1} [{2}] To:{3} [{4}] {5}",
                                        data.RV_NO, uName, userRole,
                                        Common.Function.CommonFunction.CommaJoin(lstUser),
                                        "", _Comment);
                                log.Log("INF", "Redirect " + data.RV_NO + data.RV_YEAR + " [" + data.NEXT_APPROVER + "] to [" + uName + "]", "RVList.Redirect", uName);
                                logic.RVList.SetNextApprover(data.RV_NO.Int(0), data.RV_YEAR.Int(0), UserName);
                                logic.Notice.InsertNotice(data.RV_NO, data.RV_YEAR, uName,
                                     userRole, "", "", lstUser, _Comment, false,
                                     true // direct to me always has worklist 
                                     );
                                Redirected++;
                                // send email 

                            }
                            x.AppendLine(Nagging(nagCode, "Redirect RV " + data.RV_NO));

                            #endregion
                        }
                        else
                        {
                            x.AppendLine(Nagging("MSTD00050ERR", "RV", data.RV_NO, "Redirected"));
                        }
                    }
                    if (Redirected > 0) ReloadOpener();
                }
                catch (Exception Ex)
                {
                    string _ErrMsg = Ex.Message; ;
                    LoggingLogic.err(Ex);
                    x.AppendLine(_m.SetMessage(_ErrMsg, "ERR"));
                }
                if (x.Length > 0)
                    litMessage.Text = x.ToString();
            }

        }

        protected void ButtonPostSAP_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonPostSAP_Click", "Post to SAP");
            List<RVListData> listData = null;
            if (RVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);
            ErrorData _Err = new ErrorData();
            String cashier = logic.RVList.getCashierName();

            if (listData.Count > 0)
            {
                /// Added by Akhmad Nuryanto, 6/6/2012, Post to SAP
                /// 
                SAPMode = Common.Enum.SAPMode.Post;
                lblConfirmationPost.Text = _m.Message("MSTD00008CON", "Post data to SAP");
                ModalPopupConfirmationPost.Show();
                /// End addition by Akhmad Nuryanto
            }
            else
            {
                Nag("MSTD00009WRN");
            }

            if (_Err.ErrID == 2)
            {
                Nag("MSTD00002ERR", _Err.ErrMsg);
            }
        }

        /// Added by Akhmad Nuryanto, 6/6/2012, Post to SAP
        /// 
        protected void btnOkConfirmationPost_Click(object sender, EventArgs e)
        {
            bool result = false;
            ModalPopupConfirmationPost.Hide();

            logic.Say("btnOkConfirmationPost_Click", "Ok");

            List<String> listData = null;
            if (RVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRow(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRow(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRow(gridStatusFinance);

            if (SAPMode == Common.Enum.SAPMode.Post)
            {
                DoStartLog(resx.FunctionId("FCN_RV_POST_TO_SAP"), "");
                string _ErrMessage = String.Empty;

                result = logic.RVList.RVPostToSap(listData, ProcessID, ref _ErrMessage, UserName);

                if (result)
                {
                    litMessage.Text = _m.SetMessage(_ErrMessage, "MSTD00067INF");
                    CloseOk();
                    Log("MSTD00067INF", "INF", "btnOkConfirmationPost_Click", _m.Message("MSTD00067INF", "RV"));

                }
                else
                {
                    litMessage.Text = _m.SetMessage(_ErrMessage, "MSTD00068ERR");
                    Log("MSTD00068ERR", "ERR", "btnOkConfirmationPost_Click", _m.Message("MSTD00068ERR", "RV"));
                }
                // revert unposted to original Approver
                List<string> ng = logic.RVList["NG"] as List<string>;
                if (ng != null && ng.Count > 0)
                    for (int i = 0; i < ng.Count; i++)
                    {
                        int _no = ng[i].Substring(0, ng[i].Length - 4).Int();
                        int _yy = ng[i].Substring(ng[i].Length - 4).Int();
                        logic.PVList.RevertApprover(_no, _yy, 61, 2);
                    }
                List<string> nr = logic.PVList["notready"] as List<string>;
                if (nr != null && nr.Count > 0)
                    for (int i = 0; i < nr.Count; i++)
                    {
                        string[] rx = nr[i].Split(':');

                        int _no = (rx != null && rx.Length > 0) ? rx[0].Int() : 0;
                        int _yy = (rx != null && rx.Length > 1) ? rx[1].Int() : 0;
                        logic.PVList.RevertApprover(_no, _yy);
                    }
            }
            else if (SAPMode == Common.Enum.SAPMode.Check)
            {
                DoStartLog(resx.FunctionId("FCN_RV_CHECK_SAP"), "");
                string _ErrMessage = String.Empty;

                result = logic.RVList.RvCheck(listData, ProcessID, ref _ErrMessage);

                if (result)
                {
                    litMessage.Text = _m.SetMessage(_ErrMessage, "MSTD00069INF");
                    CloseOk();

                    Log("MSTD00069INF", "INF", "btnOkConfirmationCheck_Click", _m.Message("MSTD00069INF", "RV"));
                }
                else
                {
                    litMessage.Text = _m.SetMessage(_ErrMessage, "MSTD00070ERR");
                    Log("MSTD00070ERR", "ERR", "btnOkConfirmationCheck_Click", _m.Message("MSTD00070ERR", "RV"));
                }
            }
        }

        protected void CloseOk()
        {
            string _Command = resx.K2ProcID("Form_Registration_Approval");
            foreach (string s in (logic.RVList["OK"] as List<string>))
            {
                string _no;
                string _year;
                string docno = s.Replace(":", "");

                if (docno.Length > 4)
                {
                    _no = docno.Substring(0, s.Length - 4);
                    _year = docno.Substring(s.Length - 4, 4);
                    if (logic.PVRVWorkflow.Go(docno, _Command, UserData, ""))
                        logic.RVList.CloseDocumentAtLast(_no.Int(), _year.Int());
                }
            }
        }

        protected void ButtonCheckAcc_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonCheckAcc_Click", "Check By Accounting");
            List<RVListData> listData = GetSelectedRowValues(GetSelectedTabGrid());
            ErrorData _Err = new ErrorData();
            String cashier = logic.RVList.getCashierName();

            if (listData.Count > 0)
            {
                ///added by Akhmad Nuryanto, 22/6/2012
                SAPMode = Common.Enum.SAPMode.Check;
                lblConfirmationPost.Text = _m.Message("MSTD00008CON", "Check by Accounting");
                ModalPopupConfirmationPost.Show();
                /// End addition by Akhmad Nuryanto
            }
            else
            {
                Nag("MSTD00009WRN");
            }

            if (_Err.ErrID == 2)
            {
                Nag("MSTD00002ERR", _Err.ErrMsg);
            }
        }

        protected void ButtonDirectToMe_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonDirectToMe_Click", "Direct To Me");
            int rowCount = 0;
            
            switch (RVListTabs.ActiveTabIndex)
            {
                case 0:
                    rowCount = gridGeneralInfo.Selection.Count;
                    break;
                case 1:
                    rowCount = gridStatusUser.Selection.Count;
                    break;
                case 2:
                    rowCount = gridStatusFinance.Selection.Count;
                    break;
            }
            if (rowCount < 1)
            {
                Nag("MSTD00009WRN"); // no record selected
            }
            else
            {
                SetPopupDirectToMe();
            }
        }

        protected void btnOkConfirmationCheck_Click(object sender, EventArgs e)
        {
            logic.Say("btnOkConfirmationCheck_Click", "Ok");
            //bool result = false;

            List<RVListData> listData = null;
            if (RVListTabs.ActiveTabIndex == 0)
                listData = GetSelectedRowValues(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                listData = GetSelectedRowValues(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                listData = GetSelectedRowValues(gridStatusFinance);
        }


        private bool ValidatePrintTicket(List<RVListData> l)
        {
            int errCount = 0;
            int [] donestatus = new int [] {61, 63, 64, 65, 68, 70, 84, 85, 86, 99};
            string cashierUsername = logic.Sys.CashierUserName;
            foreach (RVListData d in l)
            {
                int statusCd = d.STATUS_CD.Int();
                string payMethod = d.PAY_METHOD_CD.ToUpper();
                if (donestatus.Contains(statusCd))
                {
                    d.ERR = logic.Msg.Message("MSTD00194WRN", d.RV_NO);
                    errCount++;
                    continue;
                }
                if (statusCd != 57 || string.Compare(cashierUsername, d.NEXT_APPROVER, true) != 0)
                {
                    d.ERR = logic.Msg.Message("MSTD00139WRN", d.RV_NO, " Approved by user division");
                    errCount++;
                    continue;
                }
                if (!(payMethod.Equals("C") ))
                {
                    d.ERR = logic.Msg.Message("MSTD00138WRN", d.RV_NO, "cash");
                    errCount++;
                    continue;
                }
                if (d.DELETED)
                {
                    d.ERR = logic.Msg.Message("MSTD00002ERR", "Deleted Document " + d.RV_NO);
                    errCount++;
                    continue;
                }
                
            }
            return (l != null && l.Count > 0 && errCount < 1);
        }

        //private Boolean ValidatePrintTicket(ASPxGridView gridView)
        //{
        //    Boolean checkValid = true;
        //    if (gridView.Selection.Count >= 1)
        //    {
        //        for (int i = 0; i < gridView.VisibleRowCount; i++)
        //        {
        //            if (gridView.Selection.IsRowSelected(i))
        //            {
        //                String rvNo = gridView.GetRowValues(i, "RV_NO").ToString();
        //                HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(
        //                    i, gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;

        //                HiddenField hidGridPaymentMethod = gridView.FindRowCellTemplateControl(
        //                    i, gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridPaymentMethod") as HiddenField;
        //                if (hidGridPaymentMethod.Value != null && !hidGridPaymentMethod.Value.ToString().ToUpper().Equals("C"))
        //                {
        //                    checkValid = false;
        //                    Nag("MSTD00138WRN", rvNo, "cash");
        //                    break;
        //                }


        //                HiddenField activityDate = gridView.FindRowCellTemplateControl(
        //                    i, gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridActivityDate") as HiddenField;
        //                int rvType = Convert.ToInt32(gridView.GetRowValues(i, "RV_TYPE_CD"));

        //            }
        //        }
        //    }
        //    else if (gridView.Selection.Count == 0)
        //    {
        //        litMessage.Text = _m.SetMessage(_m.Message("MSTD00009WRN"), "WRN");
        //        Nag("MSTD00009WRN");
        //        checkValid = false;
        //    }
        //    return checkValid;
        //}

        private Boolean ValidatePrintCover(ASPxGridView gridView)
        {
            Boolean checkValid = true;
            if (gridView.Selection.Count >= 1)
            {

            }
            else if (gridView.Selection.Count == 0)
            {
                Nag("MSTD00009WRN");
                checkValid = false;
            }
            return checkValid;
        }

        protected ASPxGridView GetSelectedTabGrid()
        {
            ASPxGridView g = null;
            switch (RVListTabs.ActiveTabIndex)
            {
                case 0: g = gridGeneralInfo; break;
                case 1: g = gridStatusUser; break;
                case 2: g = gridStatusFinance; break;
            }

            return g;
        }

        protected void ButtonPrintTicket_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonPrintTicket_Click", "Print Ticket");
            List<RVListData> listData = null;
            Boolean valid = false;

            listData = GetSelectedRowValues(GetSelectedTabGrid());
            if (listData == null || listData.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            valid = ValidatePrintTicket(listData);
            if (!valid)
            {
                StringBuilder n = new StringBuilder("");
                foreach (RVListData d in listData)
                {
                    if (!d.ERR.isEmpty())
                    {
                        n.AppendLine(d.ERR);
                    }
                }
                string errs = n.ToString();
                if (!errs.isEmpty())
                {
                    lit.Text = MessageLogic.Wrap(errs, "ERR");
                }
                return;
            }
            //if (RVListTabs.ActiveTabIndex == 0)
            //{
            //    valid = ValidatePrintTicket(gridGeneralInfo);
            //    if (valid)
            //        listData = GetSelectedRowValues(gridGeneralInfo);
            //}
            //else if (RVListTabs.ActiveTabIndex == 1)
            //{
            //    valid = ValidatePrintTicket(gridStatusUser);
            //    if (valid)
            //        listData = GetSelectedRowValues(gridStatusUser);
            //}
            //else if (RVListTabs.ActiveTabIndex == 2)
            //{
            //    valid = ValidatePrintTicket(gridStatusFinance);
            //    if (valid)
            //        listData = GetSelectedRowValues(gridStatusFinance);
            //}

            if (valid)
            {
                ErrorData _Err = new ErrorData();
                String cashier = logic.RVList.getCashierName();

                if (listData.Count > 0)
                {
                    logic.RVPrint.printTicket(this,
                                            Path.GetDirectoryName( Server.MapPath(AppSetting.CompanyLogo)),
                                            Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE", "DIR")),
                                            UserData,
                                            listData,
                                            cashier,
                                            ref _Err);
                }


                if (_Err.ErrID == 2)
                {
                    Nag("MSTD00002ERR", _Err.ErrMsg);
                }
            }
        }

        protected void ButtonPrintCover_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonPrintCover_Click", "Print Cover");
            List<RVListData> listData = null;
            Boolean valid = false;
            if (RVListTabs.ActiveTabIndex == 0)
            {
                valid = ValidatePrintCover(gridGeneralInfo);
                if (valid)
                    listData = GetSelectedRowValues(gridGeneralInfo);
            }
            else if (RVListTabs.ActiveTabIndex == 1)
            {
                valid = ValidatePrintCover(gridStatusUser);
                if (valid)
                    listData = GetSelectedRowValues(gridStatusUser);
            }
            else if (RVListTabs.ActiveTabIndex == 2)
            {
                valid = ValidatePrintCover(gridStatusFinance);
                if (valid)
                    listData = GetSelectedRowValues(gridStatusFinance);
            }

            if (valid)
            {
                ErrorData _Err = new ErrorData();

                if (listData.Count > 0)
                {

                    logic.RVPrint.printCover(this,
                                            Path.GetDirectoryName(Server.MapPath(AppSetting.CompanyLogo)),
                                            Server.MapPath(logic.Sys.GetText("PDF_TEMPLATE", "DIR")),
                                            UserData,
                                            listData,
                                            ref _Err);
                }

                if (_Err.ErrID == 2)
                {
                    Nag("MSTD00002ERR", _Err.ErrMsg);
                }
            }
        }

        private void ProcessHistory_Click(ASPxGridView gridView)
        {
            if (gridView.Selection.Count < 1)
            {
                Nag("MSTD00009WRN");
                return;
            }
            if (gridView.Selection.Count > 1)
            {
                Nag("MSTD00016WRN");
                return;
            }

            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (!gridView.Selection.IsRowSelected(i)) continue;

                String rvNo = gridView.GetRowValues(i, "RV_NO").ToString();
                logic.Say("ProcessHistory_Click", "History " + rvNo);
                HiddenField hidGridRVYear = gridView.FindRowCellTemplateControl(i,
                    gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;
                HiddenField hidGridStatusCd = gridView.FindRowCellTemplateControl(i,
                    gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridStatusCd") as HiddenField;
                //Page.ClientScript.RegisterStartupScript(this.GetType(),
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                "ELVIS_LogHistory",
                "openWin('../70History/History.aspx?rv_no=" + rvNo + "&rv_year=" + hidGridRVYear.Value.ToString() + "','ELVIS_LogHistory');",
                true);

            }

        }

        protected void ButtonHistory_Click(object sender, EventArgs e)
        {
            if (RVListTabs.ActiveTabIndex == 0)
                ProcessHistory_Click(gridGeneralInfo);
            else if (RVListTabs.ActiveTabIndex == 1)
                ProcessHistory_Click(gridStatusUser);
            else if (RVListTabs.ActiveTabIndex == 2)
                ProcessHistory_Click(gridStatusFinance);
        }

        protected void ButtonDownload_Click(object sender, EventArgs e)
        {
            logic.Say("ButtonDownload_Click", "Download");
            if (validateInputSearch())
            {
                try
                {
                
                    Xmit.Send(logic.RVList.Get(
                            UserName,
                            Server.MapPath(Common.AppSetting.CompanyLogo),
                            SearchCriteria()
                        ), HttpContext.Current, "RV" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls");
            
                    
                    //string imagePath = Server.MapPath("~/App_Themes/BMS_Theme/Images/toyota-logo.png");
                    //UserData userdata = (UserData)Session["UserData"];
                    //string _FileName = "RV" + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                    //this.Response.Clear();
                    //this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    //this.Response.Charset = "";
                    //this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //this.Response.ContentType = "application/vnd.ms-excel";

                    //try
                    //{
                    //    this.Response.BinaryWrite(logic.RVList.Get(userdata.USERNAME, imagePath, SearchCriteria()));

                    //}
                    //catch (Exception ex)
                    //{
                    //    LoggingLogic.err(ex);
                    //    litMessage.Text = "Cannot get file: " + ex.Message;
                    //}
                    //this.Response.End();
                }
                catch (Exception Ex)
                {
                    LoggingLogic.err(Ex);
                    Nag("MSTD00002ERR", "Error Download List : " + Ex.Message);
                    throw;
                }
            }
        }

        protected void btnPrintWorklistReport_Click(object sender, EventArgs e)
        {
            popUpWorklistReport.Hide();

            string me = "btnPrintWorklistReport_Click";
            int pid = 0;
            string userName = UserName;

            pid = log.Log("MSTD00001INF", me + " START",
                me, userName, me, 0);

            //string fullName = "";
            string serverPath = Server.MapPath("~/");
            byte[] WorklistReportBytes = null;
            try
            {
                // gather all attachment name, with PV No 
                string filename = "WORKLIST_RV_" + DateTime.Now.ToString("yyyyMMdd_hhmm") + ".xls";
                WorklistReportBytes = logic.PVList.WorklistReport(
                    ProcessID
                    , UserData
                    , serverPath
                    , 2
                    , dtRVDateFrom.Date, dtRVDateTo.Date
                    , sel.SelectedValues().Replace(';', ',')
                    , DelayToleranceText.Text.Int());
                if (WorklistReportBytes != null)
                {
                    Xmit.Send(WorklistReportBytes, HttpContext.Current, filename);
                }
            }
            catch (IOException iox)
            {
                Nag("MSTD00002ERR", iox.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "IOException " + me + " " + iox.Message + "\r\n" + iox.StackTrace);
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message + "[ " + Convert.ToString(pid) + "]");
                log.Log("MSTD00002ERR", "Exception " + me + " " + ex.Message + "\r\n" + ex.StackTrace);
            }
            // export file
            //string filename = System.IO.Path.GetFileName(fullName);
            if (WorklistReportBytes == null) //  string.IsNullOrEmpty(fullName) || string.IsNullOrEmpty(filename))
            {
                Nag("MSTD00018INF");
                return;
            }

            //try
            //{
            //    string outLink = AppSetting.TempFileUpload.Substring(1) 
            //        + DateTime.Now.ToString("yyyy") + "/"
            //        + filename;

            //    ScriptManager.RegisterStartupScript(this,
            //        this.GetType(), "openWinLogin",
            //        "openWin('" + outLink + "','" + "PV_LIST " + "');",
            //        true);
            //}
            //catch (Exception Ex)
            //{
            //    string _ErrMsg = (!string.IsNullOrEmpty(
            //        Convert.ToString(Ex.InnerException)))
            //            ? Convert.ToString(Ex.InnerException)
            //            : Ex.Message;
            //    log.Log("MSTD00002ERR", "Exception " + me + " " + LoggingLogic.ExceptionMessageTrace(Ex));

            //    Nag("MSTD00002ERR", _ErrMsg);
            //}
            log.Log("MSTD00001INF", me + " FINISH", me);

        }

        private bool validateDateFromTo(string what, ASPxDateEdit a, ASPxDateEdit b)
        {
            if ((!string.IsNullOrEmpty(a.Text) && string.IsNullOrEmpty(b.Text)) ||
              (string.IsNullOrEmpty(a.Text) && !string.IsNullOrEmpty(b.Text)))
            {
                if (string.IsNullOrEmpty(a.Text))
                {
                    Nag("MSTD00017WRN", what + " From");
                    return false;
                }
                if (string.IsNullOrEmpty(b.Text))
                {

                    Nag("MSTD00017WRN", what + " To");
                    return false;
                }
            }
            else
            {
                if (b.Date.CompareTo(a.Date) < 0)
                {
                    Nag("MSTD00029ERR", what + " From", what + " To");
                    return false;
                }
            }
            return true;
        }

        protected void ButtonPrintWorklist_Click(object sender, EventArgs e)
        {
            bool Ok = false;

            Ok = validateDateFromTo("RV Date", dtRVDateFrom, dtRVDateTo);
            if (!Ok)
                return;

            if (sel.SelectedValues().isEmpty())
            {
                Nag("MSTD00017WRN", "Issuing Division");
                return;
            }

            popUpWorklistReport.Show();
        }

        protected void lvVendorCd_Click(object sender, ImageClickEventArgs e)
        {
            ModalButton = Common.Enum.ModalButton.VendorCd;
            LitPopUpMessage.Text = "";
            panelModal.GroupingText = "List Vendor Code";
            lblPopUpSearchCd.Text = "Vendor Code";
            lblPopUpSearchDesc.Text = "Vendor Name";
            gridPopUpVendorCd.Visible = true;
            gridPopUpVendorCd.DataSource = null;
            ClearModal();
            popUpList.Show();
        }

        protected void otherAmount_Click(object sender, EventArgs e)
        {

        }

        protected void btnClearPopUp_Click(object sender, EventArgs e)
        {
            ClearModal();
        }

        protected void btnSearchPopUp_Click(object sender, EventArgs e)
        {
            switch (ModalButton)
            {
                case Common.Enum.ModalButton.VendorCd:
                    LoadGridPopUpVendorCd();
                    break;
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            switch (ModalButton)
            {
                case Common.Enum.ModalButton.VendorCd:
                    {
                        if (gridPopUpVendorCd.Selection.Count == 1)
                        {
                            for (int i = 0; i <= gridPopUpVendorCd.VisibleRowCount; i++)
                            {
                                if (gridPopUpVendorCd.Selection.IsRowSelected(i))
                                {
                                    TextBoxVendorCode.Text = gridPopUpVendorCd.GetRowValues(i, "VENDOR_CD").ToString();
                                    TextBoxVendorDesc.Text = gridPopUpVendorCd.GetRowValues(i, "VENDOR_NAME").ToString();
                                }
                            }
                        }
                        else if (gridPopUpVendorCd.Selection.Count > 1)
                        {
                            LitPopUpMessage.Text = _m.SetMessage(_m.Message("MSTD00016WRN"), "MSTD00016WRN");
                            return;
                        }
                        else
                        {
                            LitPopUpMessage.Text = _m.SetMessage(_m.Message("MSTD00009WRN"), "MSTD00009WRN");
                            return;
                        }
                        ClearModal();
                        gridPopUpVendorCd.Selection.UnselectAll();
                        gridPopUpVendorCd.Visible = false;
                        popUpList.Hide();
                    }
                    break;
            }
        }

        protected void btnClosePopUp_Click(object sender, EventArgs e)
        {
            ClearModal();
            switch (ModalButton)
            {
                case Common.Enum.ModalButton.VendorCd:
                    gridPopUpVendorCd.Selection.UnselectAll();
                    gridPopUpVendorCd.Visible = false;
                    break;
            }
            popUpList.Hide();
        }



        protected void ButtonCloseTotalAmountCurr_Click(object sender, EventArgs e)
        {
            gridTotalAmountCurr.Visible = false;
            ModalPopupTotalAmountCurr.Hide();
        }

        protected void LoadGridPopUpTotalAmountCurr(object sender, EventArgs e)
        {
            gridTotalAmountCurr.Visible = true;
            LitMsgTotalAmountCurr.Text = "";
            panelTotalAmountCurr.GroupingText = "Total Amount Sum-up / Currency";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            //Literal litGridRVNo = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "litGridRVNo") as Literal;
            //HiddenField hidGridRVYear = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;

            string docNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_NO").str();
            string docYear = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_YEAR").str();

            List<OtherAmountData> _list = logic.Look.GetAmounts(docNo, docYear);

            gridTotalAmountCurr.DataSource = _list;
            gridTotalAmountCurr.DataBind();
            ModalPopupTotalAmountCurr.Show();
        }

        protected void gridTotalAmountCurr_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoTotalAmountCurr = gridTotalAmountCurr.FindRowCellTemplateControl(e.VisibleIndex,
                    gridTotalAmountCurr.Columns["NO"] as GridViewDataColumn, "litGridNoTotalAmountCurr") as Literal;
                litGridNoTotalAmountCurr.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }
        private void SetPopupDirectToMe()
        {
            ddlReason.SelectedIndex = -1;

            popUpDirect.Show();
        }
        private List<RVListData> GetSelectedRowValues(ASPxGridView gridView)
        {
            List<RVListData> listData = new List<RVListData>();
            RVListData data;
            Object[] dataTemp;

            string[] valueToGet = { "RV_NO", "RV_YEAR", "RV_TYPE_NAME" };

            for (int i = 0; i < gridView.Selection.Count; i++)
            {
                data = new RVListData();
                dataTemp = gridView.GetSelectedFieldValues(valueToGet)[i] as Object[];


                if (dataTemp[0] != null)
                    data.RV_NO = dataTemp[0].ToString();
                else
                    data.RV_NO = "";

                if (dataTemp[1] != null)
                {
                    //data.RV_DATE = DateTime.Parse(dataTemp[1].ToString());
                    data.RV_YEAR = dataTemp[1].str();
                }
                else
                {
                    data.RV_YEAR = StrTo.NULL_DATE.Year.str(); // data.RV_DATE.ToString("yyyy");
                }

                // Start Rinda Rahayu 20160331
                if (dataTemp[2] != null)
                {
                    data.RV_TYPE_NAME = dataTemp[2].ToString();
                }
                else
                {
                    data.RV_TYPE_NAME = "";
                }
                // End Rinda Rahayu 20160331

                logic.RVList.Fill(data);

                listData.Add(data);
            }

            return listData;
        }

        protected void RVNoFrom_TextChanged(object sender, EventArgs e)
        {
            text_ValueChanged(sender, TextBoxRVNoTo);
        }

        protected void RVNoTo_TextChanged(object sender, EventArgs e)
        {
            text_ValueChanged(sender, TextBoxRVNoFrom);
        }

        protected void VendorCode_TextChanged(object sender, EventArgs e)
        {
            ASPxGridLookup txtVendorCode = TextBoxVendorCode;
            if (!String.IsNullOrEmpty(txtVendorCode.Text))
            {
                List<VendorCdData> _list = logic.Look.GetVendorCodeData(txtVendorCode.Text, "");
                if (_list.Count > 0)
                {
                    VendorCdData firstVendorCode = _list[0];
                    TextBoxVendorDesc.Text = firstVendorCode.VENDOR_NAME;
                }
                else
                {
                    Nag("MSTD00021WRN", "Vendor " + txtVendorCode.Text);
                    TextBoxVendorDesc.Text = "";
                }
            }
            else
            {
                TextBoxVendorDesc.Text = "";
            }
        }

        protected void gridSapDocNo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoSapDocNo = gridSapDocNo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridSapDocNo.Columns["NO"] as GridViewDataColumn, "litGridNoSapDocNo") as Literal;
                litGridNoSapDocNo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonClosePopUpSapDocNo_Click(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = false;
            ModalPopupSapDocNo.Hide();
        }

        protected void LoadGridPopUpSapDocNo(object sender, EventArgs e)
        {
            gridSapDocNo.Visible = true;
            LitMsgSapDocNo.Text = "";
            panelSapDocNo.GroupingText = "SAP Document Number";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;

            string docNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_NO").str();
            string docYear = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_YEAR").str();
            
            //Literal litGridRVNo = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "litGridRVNo") as Literal;
            //HiddenField hidGridRVYear = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;
            List<SapDocNoData> _list = logic.Look.GetSapDocNo(docNo, docYear);
            gridSapDocNo.DataSource = _list;
            gridSapDocNo.DataBind();
            ModalPopupSapDocNo.Show();
        }

        protected void gridBudgetNo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoBudgetNo = gridBudgetNo.FindRowCellTemplateControl(
                    e.VisibleIndex, gridBudgetNo.Columns["NO"] as GridViewDataColumn, "litGridNoBudgetNo") as Literal;
                litGridNoBudgetNo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonCloseBudgetNo_Click(object sender, EventArgs e)
        {
            gridBudgetNo.Visible = false;
            ModalPopupBudgetNo.Hide();
        }

        protected void ButtonClosePopUpBudgetNo_Click(object sender, EventArgs e)
        {
            gridBudgetNo.Visible = false;
            ModalPopupBudgetNo.Hide();
        }

        protected void gridAttachment_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoAttachment = gridAttachment.FindRowCellTemplateControl(
                    e.VisibleIndex, gridAttachment.Columns["NO"] as GridViewDataColumn, "litGridNoAttachment") as Literal;
                litGridNoAttachment.Text = Convert.ToString((e.VisibleIndex + 1));
            }
        }

        protected void ButtonClosePopUpAttachment_Click(object sender, EventArgs e)
        {
            gridAttachment.Visible = false;
            ModalPopupAttachment.Hide();
        }

        protected void LoadGridPopUpAttachment(object sender, EventArgs e)
        {
            gridAttachment.Visible = true;
            LitMsgAttachment.Text = "";
            panelAttachment.GroupingText = "Attachment";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;

            string docNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_NO").str();
            string docYear = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_YEAR").str();


            //Literal litGridRVNo = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "litGridRVNo") as Literal;
            //HiddenField hidGridRVYear = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;
            List<AttachmentData> _list = logic.Look.GetAttachmentData(docNo, docYear);
            foreach (AttachmentData a in _list)
            {
                a.URL = Common.AppSetting.FTP_DOWNLOADHTTP + a.URL;                
            }
            gridAttachment.DataSource = _list;
            gridAttachment.DataBind();
            ModalPopupAttachment.Show();
        }

        protected void LoadGridPopUpVersion(object sender, EventArgs e)
        {
            gridVersion.Visible = true;
            LitMsgVersion.Text = "";
            panelVersion.GroupingText = "Version";
            LinkButton linkIndex = (LinkButton)sender;
            GridViewDataRowTemplateContainer container = linkIndex.NamingContainer as GridViewDataRowTemplateContainer;
            string docNo = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_NO").str();
            string docYear = gridGeneralInfo.GetRowValues(container.VisibleIndex, "RV_YEAR").str();

            //Literal litGridRVNo = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "litGridRVNo") as Literal;
            //HiddenField hidGridRVYear = gridGeneralInfo.FindRowCellTemplateControl(container.VisibleIndex,
            //                gridGeneralInfo.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;
            List<RVFormHistoryData> _list = logic.RVList.GetHistoryData(docNo, docYear);
            Session["RV_NO"] = docNo;
            Session["RV_YEAR"] = docYear;
            Session["GRID_HISTORY"] = _list;
            gridVersion.DataSource = _list;
            gridVersion.DataBind();
            gridVersion.DetailRows.ExpandAllRows();
            ModalPopupVersion.Show();
        }

        protected void ButtonClosePopUpVersion_Click(object sender, EventArgs e)
        {
            gridVersion.Visible = false;
            ModalPopupVersion.Hide();
            Session["RV_NO"] = null;
            Session["RV_YEAR"] = null;
            Session["GRID_HISTORY"] = null;
        }

        protected void gridRVDetail_DataSelect(object sender, EventArgs e)
        {
            ASPxGridView g = sender as ASPxGridView;
            if (g != null)
            {
                if (Session == null || Session["RV_NO"] == null)
                {
                    g.DataSource = null;
                    g.Visible = false;
                    return;
                }
                int rvNo = Convert.ToInt32(Session["RV_NO"].ToString());
                int rvYear = Convert.ToInt16(Session["RV_YEAR"].ToString());
                int version = Convert.ToInt16(g.GetMasterRowKeyValue());
                List<RVFormHistoryData> _list = (List<RVFormHistoryData>)Session["GRID_HISTORY"];
                if (_list != null && _list.Count > 0)
                {
                    bool noMatch = true;
                    foreach (var d in _list)
                    {
                        if (d.RVNumber == rvNo
                            && d.RVYear == rvYear
                            && d.Version == version
                            && d.Details != null
                            && d.Details.Count > 0)
                        {
                            noMatch = false;
                            g.Visible = true;
                            g.DataSource = d.Details;
                            break;
                        }
                    }

                    if (noMatch)
                    {
                        g.DataSource = null;
                        g.Visible = false;
                    }

                }
                else
                {
                    g.DataSource = null;
                    g.Visible = false;
                }
            }
        }

        protected void gridRVDetail_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                try
                {
                    object del = e.GetValue("IsDeleted");
                    if (del != null)
                    {
                        bool deleted = Convert.ToBoolean(del);
                        if (deleted)
                            e.Row.Style.Add(HtmlTextWriterStyle.BackgroundColor, red);
                    }
                }
                catch (Exception ex)
                {
                    Handle(ex);
                    throw;
                }
            }
        }

        protected void gridRVDetail_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            SetVersionCellColor(e, "CostCenterCode");
            SetVersionCellColor(e, "CostCenterName");
            SetVersionCellColor(e, "Description");
            SetVersionCellColor(e, "CurrencyCode");
            SetVersionCellColor(e, "Amount");
            SetVersionCellColor(e, "TaxCode");
            SetVersionCellColor(e, "TaxNumber");
            SetVersionCellColor(e, "InvoiceNumber");
        }

        private void SetVersionCellColor(ASPxGridViewTableDataCellEventArgs e, String fieldName)
        {
            if (e.DataColumn == null) return;

            if (e.DataColumn.FieldName == fieldName)
            {
                if (e.GetValue(fieldName + "Change") != null)
                {
                    try
                    {
                        object cha = e.GetValue(fieldName + "Change");
                        if (cha != null)
                        {
                            bool changed = Convert.ToBoolean(cha);
                            if (changed)
                                e.Cell.Style.Add("background-color", yellow);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
        }


        #region Function, added by Akhmad Nuryanto
        private List<string> GetSelectedRow(ASPxGridView gridView)
        {
            List<string> listData = new List<string>();
            string data = "";

            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (gridView.Selection.IsRowSelected(i))
                {
                    String rvNo = gridView.GetRowValues(i, "RV_NO").ToString();
                    HiddenField hidGridRVYear = gridView.FindRowCellTemplateControl(
                        i, gridView.Columns["RV_NO"] as GridViewDataColumn, "hidGridRVYear") as HiddenField;

                    if (rvNo != null && !rvNo.Equals("") && hidGridRVYear.Value != null && !hidGridRVYear.Value.Equals(""))
                    {
                        data = rvNo + ":" + hidGridRVYear.Value.ToString();
                        listData.Add(data);
                    }
                }
            }

            return listData;
        }
        #endregion

        protected List<int> getSelectedStatus()
        {
            ASPxListBox statusList = (ASPxListBox)DropDownListLastStatus.FindControl("LastStatusListBox");
            SelectedItemCollection selectedItems = statusList.SelectedItems;
            List<int> lstSelectedStatus = new List<int>();
            if ((selectedItems != null) && (selectedItems.Count > 0))
            {
                ListEditItem item;
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    item = selectedItems[i];
                    lstSelectedStatus.Add(Convert.ToInt32(item.Value));
                }
            }

            return lstSelectedStatus;
        }
        protected VoucherSearchCriteria SearchCriteria()
        {
            return new VoucherSearchCriteria(
               dtRVDateFrom.Text,
               dtRVDateTo.Text,
               TextBoxRVNoFrom.Text,
               TextBoxRVNoTo.Text,
               DropDownListRVType.SelectedValue,
               TextBoxVendorCode.Text,
               sel.SelectedValues(CommonFunction.CommaJoin(UserData.Divisions, ";"), (UserData!=null)?  UserData.isFinanceDivision: false),
               getSelectedStatus(),
               DropDownListNotice.SelectedValue,
               (ddlTransactionType.Value != null) ? ddlTransactionType.Value.ToString() : null,
               ddlPaymentMethod.SelectedValue,
               ddlVendorGroup.SelectedValue,
               dtActivityDateFrom.Text,
               dtActivityDateTo.Text,
               dtSubmitDateFrom.Text,
               dtSubmitDateTo.Text,
               null, // Planning payment Date From 
               null, // Planning payment Date To
               dtReceivedDateFrom.Text,
               dtReceivedDateTo.Text,
               null,
               null,
               dtVerifiedFrom.Text,
               dtVerifiedTo.Text,
               DropDownListWorkflowStatus.SelectedValue,
               DropDownListHoldFlag.SelectedValue,
               null,
               null,
               CreatedByEdit.Text,
               NextApproverEdit.Text,
               CancelFlag.Checked,
               DeleteFlag.Checked);
        }

        private bool inWorklist(string folio)
        {
            Ticker.In("inWorklist");
            if (Worklist == null) return false;
            bool has = false;
            foreach (var x in Worklist)
            {
                if (x.Folio.Equals(folio)) { has = true; break; }
            }
            Ticker.Spin("inWorkList: " + has);

            Ticker.Out();
            return has;
        }
    }
}
