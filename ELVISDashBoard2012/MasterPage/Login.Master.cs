﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELVISDashboard.MasterPage
{
    public partial class Login : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenField hidPageTitle = ContentPlaceHolder1.FindControl("hidPageTitle") as HiddenField;
            litPageTitle.Text = hidPageTitle.Value;
        }
    }
}
