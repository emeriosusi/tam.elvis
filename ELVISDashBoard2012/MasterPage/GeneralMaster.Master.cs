﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Common;
using Common.Data;
using Common.Function;
using BusinessLogic;

namespace ELVISDashboard.MasterPage
{
    public partial class GeneralMaster : System.Web.UI.MasterPage
    {
        private string _ScreenID;
        private string _UserID;
        //private List<RoleAcessData> _RoleAcessList;
        GlobalResourceData _globalResxData = new GlobalResourceData();
        //LoggingLogic _logging = new LoggingLogic();
        public event EventHandler ProceedButtonClicked;
        
        protected UserData UserData
        {
            get { return Session["userData"] as UserData; }
        }

        protected string UserName
        {
            get
            {
                UserData u = UserData;
                if (u != null)
                    return u.USERNAME;
                else
                    return "";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (UserData == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    litUser.Text = UserData.FIRST_NAME + " " + UserData.LAST_NAME;
                }
                _UserID = UserName;
                HiddenField hidPageTitle = ContentPlaceHolder1.FindControl("hidPageTitle") as HiddenField;
                if (hidPageTitle != null)
                {
                    litPageTitle.Text = hidPageTitle.Value;
                }
                HiddenField hidScreenID = ContentPlaceHolder1.FindControl("hidScreenID") as HiddenField;
                if (hidScreenID != null)
                {
                    _ScreenID = hidScreenID.Value;
                }
                // lblTime.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm") + "  |  ";
            }
        }

        protected void Home_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void Logout_Click(object sender, ImageClickEventArgs e)
        {
           // Response.Redirect("../Default.aspx");
            string _loc = "Logout_Click";
            LogicFactory logic = LogicFactory.Get();

            int cookieCount = Request.Cookies.Count;
            if (cookieCount > 0)
            {
                bool kode = false;
                for (int i = 0; i < cookieCount; i++)
                {
                    string keyName = Request.Cookies[i].Name;
                    string value = Request.Cookies[i].Value;
                    try
                    {
                        new Common.Function.GlobalResourceData().GetResxObject("ScreenID", keyName);
                        if (value == "1")
                        {
                            kode = true;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggingLogic.err(ex);
                    }
                }
                if (kode == false)
                {
                    #region logging logout
                    try
                    {
                        //int _processId = 0;
                        //string _errMsg = string.Format(_globalResxData.GetResxObject("Message", "MSTD00005INF"), UserName);
                        
                        //_logging.Log("MSTD00005INF", _errMsg, _loc, UserName, "", _processId, Request.UserHostAddress);
                        
                        logic.Say(_loc, logic.Msg.Message("MSTD00005INF", UserName));
                    }
                    catch (Exception ex) 
                    {
                        LoggingLogic.err(ex);
                    }
                    #endregion
                    EndSession();
                    // Session.Clear();
                }
            }
            else
            {
                #region logging logout
                try
                {
                    //int _processId = 0;
                    //string _errMsg = string.Format(_globalResxData.GetResxObject("Message", "MSTD00005INF"), UserName);
                    //_logging.Log("MSTD00005INF", _errMsg, _loc, UserName, "", _processId, Request.UserHostAddress);
                    logic.Say(_loc, logic.Msg.Message("MSTD00005INF", UserName));
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                }
                #endregion
                EndSession();
                // Session.Clear();
            }
           
        }

        protected void EndSession()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Elvis", "$(document).ready(function(){ Logout();});", true);
            Response.Cookies.Clear();
        }
        #region btnApproval or btnRejection

        #region ApprovalType
        public Common.Enum.ApprovalEnum ApprovalType
        {
            set
            {
                ViewState["ApprovalType"] = value;
            }
            get
            {
                return (Common.Enum.ApprovalEnum)ViewState["ApprovalType"];
            }
        }
        #endregion

        #region btnApprove_Click
        public void btnApprove_Click(object sender, EventArgs e, string DocNo, string DocYear)
        {
            ApprovalType = Common.Enum.ApprovalEnum.Approve;
            SetPopUpApproval(DocNo, DocYear);
            popUpApproval.Show();
        }
        #endregion

        #region btnReject_Click
        public void btnReject_Click(object sender, EventArgs e, string DocNo, string DocYear)
        {
            ApprovalType = Common.Enum.ApprovalEnum.Reject;
            SetPopUpApproval(DocNo, DocYear);
            popUpApproval.Show();
        }
        #endregion

        #region btnHold_Click
        public void btnHold_Click(object sender, EventArgs e, string DocNo, string DocYear)
        {
            SetPopUpApproval(DocNo, DocYear);
            popUpApproval.Show();
        }
        #endregion

        #region SetPopUpApproval
        private void SetPopUpApproval(string DocNo, string DocYear)
        {
            //gridPopUpApproval.DataSource = _ECIPackageRegistrationLogic.SearchR_ECID(txtPackageNo.Text);
            //gridPopUpApproval.DataBind();
            
            //txtApprovalComment.Visible = true;
            //lblNoticeLabel.Visible = true;

            string[] aGroupingText = new string[] {"Idle", "Approve", "Reject", "Hold", "Un-Hold" };
            pnlApproval.GroupingText = aGroupingText[(int)ApprovalType] + " Confirmation";
            lblNoticeLabel.Text = // aGroupingText[(int)ApprovalType] + 
                                "Reason:";
            NoticeDiv.Visible = (ApprovalType == Common.Enum.ApprovalEnum.Reject) 
                             || (ApprovalType == Common.Enum.ApprovalEnum.UnHold);
            HoldDiv.Visible = (ApprovalType == Common.Enum.ApprovalEnum.Hold);
            RejectDiv.Visible = ApprovalType == Common.Enum.ApprovalEnum.Reject;
            List<ComboClassData> aCategory = new List<ComboClassData>();
            aCategory.Add(new ComboClassData() { COMBO_CD = "0", COMBO_NAME = "Reject" });
            aCategory.Add(new ComboClassData() { COMBO_CD = "1", COMBO_NAME = "Cancel" });

            ddlCategory.DataSource = aCategory;
            ddlCategory.DataTextField = "COMBO_NAME";
            ddlCategory.DataValueField = "COMBO_NAME";
            ddlCategory.DataBind();

            //if (ApprovalType == Common.Enum.ApprovalEnum.Approve)
            //{
            //    txtApprovalComment.Visible = false;
            //    lblNoticeLabel.Visible = false;
            //    pnlApproval.GroupingText = "Approval Confirmation";
            //}
            //else
            //    if (ApprovalType == Common.Enum.ApprovalEnum.Reject)
            //    {
            //        pnlApproval.GroupingText = "Rejection Confirmation";
            //    }
            //    else
            //        if (ApprovalType == Common.Enum.ApprovalEnum.Hold)
            //        {
            //            pnlApproval.GroupingText = "Hold Confirmation";
            //        }
            //        else
            //            if (ApprovalType == Common.Enum.ApprovalEnum.UnHold)
            //            {
            //                pnlApproval.GroupingText = "Un-Hold Confirmation";
            //            }

            txtApprovalDocNo.Text = DocNo;
            txtApprovalDocYear.Text = DocYear;
        }
        #endregion

        #region btnApproval_Click
        public void btnApprovalProceed_Click(object sender, EventArgs e)
        {
            ProceedButtonClicked(sender, e);
        }

        #endregion

        public void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList d = sender as DropDownList;
            
            if (d != null && d.SelectedIndex >= 0)
                lblNoticeLabel.Text = Convert.ToString(d.SelectedValue) +
                    " Reason:";
            
        }
        #endregion
    }
}
