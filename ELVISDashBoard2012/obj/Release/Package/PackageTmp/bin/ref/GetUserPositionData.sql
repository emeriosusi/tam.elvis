-- GetUserPositionData

SELECT 
 u.CLASS,
 u.POSITION_ID,
 u.POSITION_NAME,
 u.DIVISION_ID,
 u.DIVISION_NAME,
 u.DEPARTMENT_ID,
 u.DEPARTMENT_NAME,
 u.SECTION_ID,
 u.SECTION_NAME
FROM vw_User u
WHERE UPPER(u.USERNAME) = UPPER('{0}')
