-- WorklistPV

SELECT 
    CONVERT(VARCHAR(15), dbo.fn_DocNoYearToReff(w.PV_NO, w.PV_YEAR)) AS [Folio], 
    CONVERT(VARCHAR(10), w.PV_NO) AS [DocNumber], 
    CONVERT(VARCHAR(4), w.PV_YEAR) AS [DocYear], 
    w.PV_DATE AS [DocDate], 
    CONVERT(VARCHAR(10), w.PV_NO) 
     + (CASE WHEN (w.NO_OF_NOTICE > 0) THEN '(' + CONVERT(VARCHAR(15), w.NO_OF_NOTICE) + ')' ELSE '' END) 
     + (CASE WHEN w.NEED_REPLY > 0 THEN '*' ELSE '' END) AS [HypValue], 
    w.TRANSACTION_NAME + '-' + w.VENDOR_NAME AS [Description], 
    w.TOTAL_AMOUNT_IDR AS [TotalAmount], 
    w.NO_OF_NOTICE as [Notices], 
    w.PAID_DATE AS [PaidDate], 
    w.HOLD_FLAG AS [HoldFlag], 
    w.NEED_REPLY AS [NeedReply], 
    w.DIVISION_NAME AS [DivName], 
    w.REJECTED AS [Rejected], 
    w.STATUS_CD, 
    w.status_name as [StatusName], 
    w.pv_type_name AS [PvTypeName], 
    CONVERT(VARCHAR(20), w.ACTIVITY_DATE_FROM, 106) AS [ActivityDateFrom], 
    CONVERT(VARCHAR(20), w.ACTIVITY_DATE_TO, 106) AS [ActivityDateTo], 
    w.PAY_METHOD_NAME as [PayMethodName], 
    w.BUDGET_NO as [BudgetNo], 
    CONVERT(BIT, CASE WHEN w.NO_NOTICE_WORKLIST > 0 THEN 1 ELSE 0 END) AS [WorklistNotice] ,
	w.TRANSACTION_CD as [TransCd] /* FID.Ridwan 11072018 */ 
FROM VW_PV_WORKLIST_DESCRIPTION w                 
WHERE w.PV_NO IN ({0})