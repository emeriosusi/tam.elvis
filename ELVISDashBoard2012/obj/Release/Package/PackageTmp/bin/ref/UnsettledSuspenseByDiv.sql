-- UnsettledSuspenseByDiv

SELECT 
	COUNT(1) UnsettledSuspenseByDiv
FROM TB_R_PV_H h 
LEFT JOIN SAP_DB.dbo.TB_M_VENDOR v  ON v.VENDOR_CD = h.VENDOR_CD
LEFT JOIN TB_R_DOC_C c
       ON c.DOC_NO = h.PV_NO AND c.DOC_YEAR = h.PV_YEAR 
WHERE h.PV_TYPE_CD = 2 
AND c.SETTLEMENT_STATUS = 1
AND ISNULL(h.CANCEL_FLAG, 0) != 1
AND ISNULL(h.DELETED, 0) != 1
AND V.VENDOR_CD IS NOT NULL
AND 
	(SELECT TOP 1 ACTUAL_DT 
	FROM TB_R_DISTRIBUTION_STATUS 
	WHERE REFF_NO = dbo.fn_DocNoYearToReff(h.PV_NO, h.PV_YEAR) 
	ORDER BY STATUS_CD DESC) IS NOT NULL
AND h.DIVISION_ID = '{0}'
