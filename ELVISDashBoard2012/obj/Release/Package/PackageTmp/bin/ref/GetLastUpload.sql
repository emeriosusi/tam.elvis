-- GetLastUpload
SELECT
   PROCESS_ID as [Pid],
   SEQ_NO as [SequenceNumber] ,
   COST_CENTER_CD as [CostCenterCode],
   CURRENCY_CD as [CurrencyCode],
   DESCRIPTION as [Description],
   AMOUNT as [Amount],
   WBS_NO as [WbsNumber],
   INVOICE_NO as [InvoiceNumber],
   INVOICE_DATE as [InvoiceDate],
   ITEM_TRANSACTION_CD 	as [ItemTransaction],
   TAX_NO as [TaxNumber],
   TAX_CD as [TaxCode],
   TAX_DT as [TaxDate], 
   GL_ACCOUNT as [GlAccount], 
   ITEM_NO as [ItemNo],
   DPP_AMOUNT as [DppAmount],
   TAX_ASSIGNMENT as [TaxAssignment],
   ACC_INF_ASSIGNMENT as [AccInfAssignment],
   --EFB
   WHT_TAX_CODE,
   WHT_TAX_TARIFF,
   WHT_TAX_ADDITIONAL_INFO,
   WHT_DPP_PPh_AMOUNT    
FROM dbo.TB_T_PV_D
WHERE PROCESS_ID = @pid
