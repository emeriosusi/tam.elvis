-- GetRoleAccessRight

SELECT 
    [OBJECT_ID],
    [OBJECT_ID] AS [REG_NO],
    ROLE_ID,
    ROLE_NAME,
    SCREEN_ID,
    USERNAME
FROM vw_RoleAccessRight
WHERE UPPER(USERNAME) = UPPER('{0}')
