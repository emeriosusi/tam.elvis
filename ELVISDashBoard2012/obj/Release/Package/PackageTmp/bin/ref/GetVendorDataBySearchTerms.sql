declare 
	@@term VARCHAR(10) = @terms
		
SELECT top 1
	  v.vendor_cd
	, v.vendor_name
	, v.vendor_group_cd
	, g.vendor_group_name
	, v.search_terms
	, v.contact_person
	, v.division_id
	, d.division_name
	, v.created_by
	, v.created_dt
	, v.suspense_count
FROM vw_Vendor v
  LEFT JOIN tb_m_vendor_group g ON g.vendor_group_cd = v.vendor_group_cd
  LEFT JOIN vw_Division d ON d.division_id = v.division_id
WHERE  v.SEARCH_TERMS = @@term
