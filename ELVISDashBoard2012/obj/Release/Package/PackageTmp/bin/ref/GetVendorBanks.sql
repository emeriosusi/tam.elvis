-- GetVendorBanks
SELECT 
	AccountHolder = v.ACCT_HOLDER,
	Account = v.BANK_ACCOUNT,
	[Key] = v.BANK_KEY,
	Country = v.CTRY,
	Name = v.NAME_OF_BANK,
	[Type] = LTRIM(RTRIM(v.BANK_TYPE)),
	VendorCode = LTRIM(RTRIM(v.VENDOR_CD)),
	Currency = v.CURRENCY
FROM 
	vw_VendorBank v 
WHERE LTRIM(RTRIM(v.VENDOR_CD)) = @vendorCode 