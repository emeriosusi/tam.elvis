-- GetUserInfoByName 
SELECT TOP 1 
       REG_NO,
       USERNAME,
       FIRST_NAME,
       LAST_NAME,
       EMAIL,
       APPLICATION_ID,
       ROLE_ID,
       ROLE_NAME,
       OBJECT_ID,
       SCREEN_ID
FROM vw_User_Role_Detail
WHERE 
	USERNAME = @USERNAME  
