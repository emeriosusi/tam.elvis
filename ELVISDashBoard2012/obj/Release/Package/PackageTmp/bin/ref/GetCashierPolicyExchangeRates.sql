-- GetCashierPolicyExchangeRates
-- DECLARE @docType INT = 0;

SELECT 
	er.CurrencyCode,
	er.Rate,
	er.ValidFrom,
	CASE WHEN @docType = 1 THEN c.AMOUNT_MIN_PV 
		WHEN @docType = 2 THEN c.AMOUNT_MIN_RV
		ELSE NULL
	END AS [AmountMin] 
		
FROM 
(
	-- getExchangeRates
	select LTRIM(RTRIM(v.CURRENCY_CD)) [CurrencyCode],
		v.EXCHANGE_RATE [Rate], 
		v.VALID_FROM [ValidFrom] 
	from vw_ExchangeRate v
)  ER 
LEFT JOIN TB_M_CURRENCY c ON LTRIM(RTRIM(C.CURRENCY_CD)) = er.CurrencyCode