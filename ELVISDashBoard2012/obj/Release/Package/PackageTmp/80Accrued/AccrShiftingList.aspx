﻿<%@ Page Title="Accrued Shifting Retrieval Screen" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="AccrShiftingList.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrShiftingList" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xx" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">
        function convertConfirmation() {
            var selection = confirm("Convert Settlement into PV and/or RV ?");
            if (selection) {
                $("#hidDialogConfirmation").val("y");
            } else {
                $("#hidDialogConfirmation").val("n");
            }
            loading();
            return selection;
        }

        function hidePop() {
            var pop = $find("ConfirmationDeletePopUp");
            pop.hide();
        }

        function assignPopClick() {
            if ($("#rvLink").length > 0) {
                $("#rvLink").click(function () {
                    if ($("#pvLink").length < 1) 
                        hidePop();
                });
            }

            if ($("#pvLink").length > 0) {
                $("#pvLink").click(function () {
                    if ($("#rvLink").length < 1)
                        hidePop();
                });
            }
        }

        function search() {
            var btn = $("#btnSearch");
            if (btn.length > 0) {
                btn.click();
            };
            return false;
        }

        function OnKeyDown(s, event) {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                search(); 
                return false; 
            }
        }

        function OnSelectSuspense(s, e) {
            var sus = $("#ddlSuspenseNo"); 
            if (sus != null && sus.length > 0) {
                search();   
            } else {                
            }
        }
    </script>

<style type="text/css">
   .boxDesc
   {
       border: 1px solid #afafaf;
       border-style: inset;
       background-color: #efefef;
       display: block;              
       overflow: auto;
       padding-left: 5px;
       height: 22px;
       float: left;
       vertical-align: middle;
   }
   
   #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 80px;
   }
   #divVendorName
   {
       width: 254px;
   }
</style>
</asp:Content>
<asp:Content ID="xPre" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />

              <div class="contentsection" style="width:987px !important;">
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                <tr>
                    <td valign="baseline" style="width: 125px">
                        Issuing Division
                    </td>
                    <td valign="top" colspan="3">
                        <dx:ASPxDropDownEdit ID="IssuingDivisionDropDown" ClientInstanceName="IssuingDivisionDropDown"
                            runat="server" Width="210px">
                            <DropDownWindowStyle BackColor="#EDEDED" />
                            <DropDownWindowTemplate>
                                <dx:ASPxListBox runat="server" ID="IssuingDivisionListBox" ClientInstanceName="IssuingDivisionListBox"
                                    SelectionMode="CheckColumn" Width="300px" Height="300px" TextField="Description"                                            
                                    ValueField="Code" DataSourceID="dsDivision">
                                    <Border BorderStyle="None" />
                                    <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                    <ClientSideEvents SelectedIndexChanged="function(s,e) {
                                            var selectedItems = s.GetSelectedItems();
                                            var selectedText = GetSelectedListItemsText(selectedItems);
                                            IssuingDivisionDropDown.SetText(selectedText);
                                    }" />
                                </dx:ASPxListBox>
                                <table style="width: 100%" cellspacing="0" cellpadding="4">
                                    <tr>
                                        <td align="right">
                                            <dx:ASPxButton ID="btClearDivisionList" AutoPostBack="False" runat="server" Text="Clear"
                                                CssClass="display-inline-table">
                                                <ClientSideEvents Click="function(s, e){ IssuingDivisionListBox.UnselectAll(); IssuingDivisionDropDown.SetText(''); }" />
                                            </dx:ASPxButton>
                                            <dx:ASPxButton ID="btCloseDivisionList" AutoPostBack="False" runat="server" Text="Close"
                                                CssClass="display-inline-table">
                                                <ClientSideEvents Click="function(s, e){ IssuingDivisionDropDown.HideDropDown(); }" />
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </DropDownWindowTemplate>
                        </dx:ASPxDropDownEdit>


                        <asp:DropDownList ID="DropDownListIssuingDivision" runat="server" Width="240px" CssClass="hidden"/>
                    </td>
                    <td valign="baseline" style="width: 175px">
                        Budget Year
                    </td>
                    <td valign="top" style="width: 380px">
                        <asp:TextBox ID="txtPVYear" runat="server" Width="50px" MaxLength="4" TabIndex="2"                            
                            ClientIDMode="Static" 
                            onchange="extractNumber (this, 0, false);"
							onblur = "extractNumber (this, 0, false);"
							onkeyup = "extractNumber (this, 0, false);"
							onkeypress = "return blockNonNumbers (this, event, false, false);"                            
                            onkeydown = "OnKeyDown"
                            >
                            
                            </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline">
                        Accrued Shifting No
                    </td>
                    <td valign="top" colspan="3">
                        <asp:TextBox ID="txtAccrNo" runat="server" Width="125px" MaxLength="13" onkeydown = "OnKeyDown"></asp:TextBox>
                    </td>

                    <td valign="baseline">
                        Submission Status
                    </td>
                    <td valign="top">
                        <asp:DropDownList runat="server" Width="103px" ID="ddlSubSts" AutoPostBack="false"/>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline">
                        Created Date
                    </td>
                    <td valign="top" style="width:75px">
                        <dx:ASPxDateEdit ID="dtCreatedDateFrom" runat="server" Width="100px" DisplayFormatString="dd MMM yyyy" 
                            EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                            AutoPostBack="false" ClientIDMode="Static" >
                            <ClientSideEvents DateChanged="OnDateChanged" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td valign="top" style="width:24px">
                        To
                    </td>
                    <td valign="top" style="width:150px">
                        <dx:ASPxDateEdit ID="dtCreatedDateTo" runat="server" DisplayFormatString="dd MMM yyyy"
                            EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                            AutoPostBack="false" ClientIDMode="Static">
                            <ClientSideEvents DateChanged="OnDateChanged" />
                        </dx:ASPxDateEdit>
                    </td>

                    <td valign="baseline">
                        Workflow Status
                    </td>
                    <td valign="top">
                        <asp:DropDownList ID="ddlWorkflowStatus" runat="server" Width="103px" />
                    </td>
                </tr>
                <tr>
                    <td valign="baseline">
                        Total Amount
                    </td>
                    <td valign="top" colspan="3">
                        <asp:TextBox ID="txtTotAmountFrom" runat="server" Width="90px" MaxLength="15"
                            onchange="extractNumber (this, 0, false);"
                            onblur="extractNumber (this, 0, false);" onkeyup="extractNumber (this, 0, false);"
                            onkeypress="return blockNonNumbers (this, event, false, false);">
                        </asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp; To&nbsp;&nbsp;&nbsp; &nbsp;
                        <asp:TextBox ID="txtTotAmountTo" runat="server" Width="90px" MaxLength="15" 
                            onchange="extractNumber (this, 0, false);"
                            onblur="extractNumber (this, 0, false);" onkeyup="extractNumber (this, 0, false);"
                            onkeypress="return blockNonNumbers (this, event, false, false);">
                        </asp:TextBox>
                    </td>
                </tr>
                
             </table>
                <div class="rowbtn" style="text-align: right">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" ClientIDMode="Static"
                        onclick="btnSearch_Click" OnClientClick="loading()"  />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnClear" runat="server"
                            OnClick="btnClear_Click" Text="Clear" />
                </div>
            </div>
            <div class="rowbtn" style="text-align: right; width: 983px;">
                <asp:Button ID="btnNew" runat="server" Text="New" OnClick="btnNew_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnView" runat="server" Text="View" OnClick="btnView_Click" />
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Accrued Shifting Retrieval" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />



    <div style="clear:both"></div>
    <div style="clear:both"></div>

    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
            <asp:PostBackTrigger ControlID="btnDownload" />
        </Triggers>
        <ContentTemplate>
            <table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="100%">
                <tr>                       
                    <td>
                        <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" 
                            AutoGenerateColumns="False"
                            ClientInstanceName="gridGeneralInfo" 
                            OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                            KeyFieldName="SHIFTING_NO" 
                            OnCustomCallback="grid_CustomCallback" 
                            EnableCallBacks="false"
                            Styles-AlternatingRow-CssClass="even">
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                    CellStyle-VerticalAlign="Middle" >
                                    <EditButton Visible="false" Text=" ">
                                    </EditButton>
                                    <UpdateButton Visible="false" Text=" ">
                                    </UpdateButton>
                                    <NewButton Visible="false" Text=" ">
                                    </NewButton>
                                    <CancelButton Visible="false" Text=" ">
                                    </CancelButton>
                                    <CellStyle VerticalAlign="Middle">
                                    </CellStyle>
                                    <HeaderTemplate>
                                        <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientInstanceName="SelectAllCheckBox"
                                            ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked(); }"
                                            CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                        </dx:ASPxCheckBox>
                                    </HeaderTemplate>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn Caption="WF Sts" FieldName="WORKFLOW_STATUS" Width="35px"
                                    VisibleIndex="1">
                                    <DataItemTemplate>
                                        <div class="dynamicDiv" style='background-color: <%# Status_Color(Eval("WORKFLOW_STATUS")) %>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Accured Shifting No." FieldName="SHIFTING_NO" Width="125px"
                                    VisibleIndex="2" CellStyle-HorizontalAlign="Left" >
                                    <DataItemTemplate>
                                        <asp:HyperLink runat="server" ID="hypDetail" Text="Detail" NavigateUrl="#"/>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Issuing Division" FieldName="DIVISION_NAME" VisibleIndex="3"
                                    Width="50px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn Caption="Created Date" FieldName="CREATED_DT"
                                    VisibleIndex="4">
                                    <PropertiesDateEdit DisplayFormatString="{0:dd.MM.yyyy}">
                                    </PropertiesDateEdit>
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataDateColumn Caption="Created By" FieldName="CREATED_BY"
                                    VisibleIndex="5">
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataDateColumn Caption="Updated Date" FieldName="CHANGED_DT"
                                    VisibleIndex="6">
                                    <PropertiesDateEdit DisplayFormatString="{0:dd.MM.yyyy}">
                                    </PropertiesDateEdit>
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataDateColumn Caption="Updated By" FieldName="CHANGED_BY"
                                    VisibleIndex="7">
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn Caption="Submission Status" FieldName="STATUS_NAME" VisibleIndex="8"
                                    Width="100px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="PIC" VisibleIndex="9">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Current" FieldName="PIC_CURRENT" VisibleIndex="0"
                                            Width="100px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Next" FieldName="PIC_NEXT" VisibleIndex="1"
                                            Width="100px">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
        
                                <dx:GridViewDataTextColumn
                                    Caption="Total Amount Shifted" FieldName="TOT_AMT"
                                    VisibleIndex="10" Width="100px">
                                    <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                    <DataItemTemplate>
                                        <asp:Literal runat="server" 
                                            ID="litGridTotalAmountIDR" 
                                            Text='<%# CommonFunction.Eval_Curr("IDR", Eval("TOT_AMOUNT")) %>' />
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                            <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                            <SettingsLoadingPanel ImagePosition="Top" />
                            <Styles>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                </Header>
                                <AlternatingRow CssClass="even">
                                </AlternatingRow>
                                <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                    <Paddings PaddingTop="1px" PaddingBottom="1px" />
                                </Cell>
                                <Row CssClass="doubleRow" />
                            </Styles>
                            <Settings ShowStatusBar="Visible" />
                            <SettingsPager Visible="false" />
                            <Templates>
                                <StatusBar>
                                    <div style="text-align: left;">
                                        Records per page: <select onchange="gridGeneralInfo.PerformCallback(this.value);">
                                            <option value="5" <%# WriteSelectedIndexInfo(5) %>>5</option>
                                            <option value="10" <%# WriteSelectedIndexInfo(10) %>>10</option>
                                            <option value="15" <%# WriteSelectedIndexInfo(15) %>>15</option>
                                            <option value="20" <%# WriteSelectedIndexInfo(20) %>>20</option>
                                            <option value="25" <%# WriteSelectedIndexInfo(25) %>>25</option>
                                            <option value="50" <%# WriteSelectedIndexInfo(50) %>>50</option>
                                            <option value="100" <%# WriteSelectedIndexInfo(100) %>>100</option>
                                            <option value="150" <%# WriteSelectedIndexInfo(150) %>>150</option>
                                            <option value="200" <%# WriteSelectedIndexInfo(200) %>>200</option>
                                            <option value="250" <%# WriteSelectedIndexInfo(250) %>>250</option>
                                            <option value="500" <%# WriteSelectedIndexInfo(500) %>>500</option>
                                        </select>&nbsp; <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">&lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a> &nbsp;
                                        Page <input type="text" onchange="if(!gridGeneralInfo.InCallback()) gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                            onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                            value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                            style="width: 20px" />of <%# gridGeneralInfo.PageCount%>&nbsp; <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%><a
                                            title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp; <a title="Last"
                                                href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; 
                                    </div>
                                </StatusBar>
                            </Templates>
                        </dx:ASPxGridView>
                        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                            EntityTypeName=""
                            OnSelecting="LinqDataSource1_Selecting">
                        </asp:LinqDataSource>
                        <asp:LinqDataSource ID="dsDivision" runat="server" OnSelecting="dsDivision_Selecting"/>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnButtons" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 980px;">
                <tr>
                    <td width="100px">
                        <asp:Button ID="btnDownload" runat="server" OnClick="btnDownload_Click" Text="Download" CssClass="xlongButton" SkinID="xlongButton" />
                    </td>
                    <td colspan="3" valign="top" style="width: 270px" align="right">
                        <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="rowbtn">
        <asp:UpdatePanel runat="server" ID="updPnlLegend">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlLegendInfo" GroupingText="Legend" Width="983px">
                    <div style="float: left; width: 50%">
                        &nbsp;&nbsp;&nbsp;Work Flow Status : 
                        <table>
                            <tr>
                                <td> &nbsp; </td>
                                <td style="background-color: #ff0000; width: 20px"> &nbsp; </td>
                                <td style="width: 50px"> Delay </td>
                                <td> &nbsp; </td>
                                <td style="background-color: #00ff00; width: 20px"> &nbsp; </td>
                                <td style="width: 75px"> On time </td>
                                <td> &nbsp; </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear: both">
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete" OkControlID="BtnOkConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn buttonRight">
                            <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" 
                                OnClientClick="loading();HiddenDeleteOkButton.click();"/>
                                &nbsp; 
                            <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" 
                                OnClick="BtnCancelConfirmationDelete_Click" />
                        </div>
                            <asp:Button ID="HiddenDeleteOkButton" ClientIDMode="Static" Text=" " runat="server" Width="0px" Height="0px" 
                                OnClick="btnOkConfirmationDelete_Click" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

 
</asp:Content>
