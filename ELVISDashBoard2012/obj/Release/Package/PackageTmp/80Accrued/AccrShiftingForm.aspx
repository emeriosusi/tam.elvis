﻿<%@ Page Title="Accrued Shifting Form" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="AccrShiftingForm.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrShiftingForm" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<%@ Register Src="../UserControl/FormHint.ascx" TagName="FormHint" TagPrefix="fh" %>
<asp:Content ID="xx" ContentPlaceHolderID="pre" runat="server">
    <script src="../approval.js" type="text/javascript"></script>
    <link href="../approval.css" rel="stylesheet" type="text/css" />
    <script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>
    <script type="text/javascript">
        function convertConfirmation() {
            var selection = confirm("Convert Settlement into PV and/or RV ?");
            if (selection) {
                $("#hidDialogConfirmation").val("y");
            } else {
                $("#hidDialogConfirmation").val("n");
            }
            loading();
            return selection;
        }

        function hidePop() {
            var pop = $find("ConfirmationSavePopUp");
            pop.hide();
        }

        function assignPopClick() {
            if ($("#rvLink").length > 0) {
                $("#rvLink").click(function () {
                    if ($("#pvLink").length < 1) 
                        hidePop();
                });
            }

            if ($("#pvLink").length > 0) {
                $("#pvLink").click(function () {
                    if ($("#rvLink").length < 1)
                        hidePop();
                });
            }
        }

        function search() {
            var btn = $("#btnSearch");
            if (btn.length > 0) {
                btn.click();
            };
            return false;
        }

        function OnKeyDown(s, event) {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                search(); 
                return false; 
            }
        }

        function BookingNoChanged(s, e) {
            var t = s.GetValue();
            gridGeneralInfo.PerformCallback('p:update:bookNo:' + t);
        }

        function WbsNoOldChanged(s, e) {
            var g = s.GetGridView();
            var idxFocused = g.GetFocusedRowIndex();
            g.GetRowValues(idxFocused, 'KEYS', function (values) {
                gridGeneralInfo.PerformCallback('p:update:keys:' + values);
            });


            //var t = s.GetValue();
            //gridGeneralInfo.PerformCallback('p:update:keys:' + t);
        }

        function WbsNoOldRowChanged(s, e) {
            var t = s.GetValue();
            gridGeneralInfo.PerformCallback('p:update:wbsNoOld:' + t);
        }

        function ShiftingType_TextChanged(s, e) {
            var t = s.GetValue();
            var txt = s.lastSuccessText;
            gridGeneralInfo.PerformCallback('p:update:shiftingType:' + t + ':' + txt);
        }

        function txtGridShiftingAmt_TextChanged(s, e) {
            var values = s.lastChangedValue;
            gridGeneralInfo.PerformCallback('p:update:shiftingAmt:' + values);
        }

        function ExtAmountChanged(s, e) {
            var values = s.lastChangedValue;
            gridGeneralInfo.PerformCallback('p:update:extAmount:' + values);
        }

        function ViewActivity(s) {

            var selectedId = s.id.split("_");
            var values = selectedId[selectedId.length - 1];
            console.log(values);
            console.log(gridActivity);
            gridActivity.PerformCallback('p:update:' + values);
        }

        function SelectActivity(s, e) {
            console.log(s);
            console.log(s.value);
            console.log(e);
            
            return;
        }

        function isNumberKey(evt) {
            var thestring = evt.inputElement.value;
            var thenum = thestring.replace(/\D/g, "");
            evt.inputElement.value = thenum;
            return true;
        }

        function isNumberValue(evt) {
            var thestring = evt.inputElement.value;
            var thenum = thestring.replace(/\D/g, "");
            gridGeneralInfo.PerformCallback('p:update:extAmount:' + thenum);
        }

        function RowDataChanged(s, e) {
            var g = s.GetGridView();
            gridGeneralInfo.PerformCallback('p:update');
            g.Refresh();
        }

        function OnSelectSuspense(s, e) {
            var sus = $("#ddlSuspenseNo"); 
            if (sus != null && sus.length > 0) {
                search();   
            } else {                
            }
        }

        function GetCheckBoxValue(s, e) {
            console.log(s);
            var val = s.GetValue();
            

            var name = s.name;

            var selectedCkBox = name.split("_");
            var values = selectedCkBox[selectedCkBox.length - 1];
            console.log(values);
            var checked = s.GetChecked();
            if (checked) {
                gridActivity.PerformCallback('p:update:active:' + values);
            } else {
                gridActivity.PerformCallback('p:update:inactive:' + values);
            }
        }

        function IsEmptyReason(s, e) {
            if ($("#txtReviseComment").val() == "") {
                alert("Please input reason first!");
                return false;
            }
            $("#popdlgRevise").hide();
            loading();
            return true;
        }

        $(document).keydown(BlockNonIntendedBackspace);

        function hidePop() {
            var pop = $find("popUpRevise");
            pop.hide();
        }
    </script>

<style type="text/css">
   .boxDesc
   {
       border: 1px solid #afafaf;
       border-style: inset;
       background-color: #efefef;
       display: block;              
       overflow: auto;
       padding-left: 5px;
       height: 22px;
       float: left;
       vertical-align: middle;
   }
   
   #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 80px;
   }
   #divVendorName
   {
       width: 254px;
   }
</style>
</asp:Content>
<asp:Content ID="xPre" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br />
    <br />
    <asp:UpdatePanel runat="server" ID="pnupdateLitMessage">
        <ContentTemplate>
            <asp:Literal runat="server" ID="messageControl" Visible="false" EnableViewState="false"
                OnLoad="evt_messageControl_onLoad" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />

            <div class="contentsection" style="width:987px !important;">
                <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                    <tr>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Issuing Division <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxIssueDiv" Width="100px" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 150px" class="td-layout-item" colspan="2">
                            Exchange Rate
                        </td>
                        <td valign="middle" style="width: 150px" class="td-layout-item" colspan="2">
                            PIC
                        </td>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Total Amount Shifted <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 125px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxTotAmt" Width="110px" CssClass="display-inline-table" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Accrued Shifting No <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxAccruedNo" Width="100px" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-item">
                            USD to IDR <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxUsdIdr" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 80px" class="td-layout-item">
                            Current <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxPicCur" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Submission Status <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 125px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxSubmSts" Width="110px" CssClass="display-inline-table" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Created Date <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxUpdateDt" Width="100px" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-item">
                            JPY to IDR <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxJpyIdr" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 80px" class="td-layout-item">
                            Next <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 100px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxPicNext" Width="98%" CssClass="display-inline-table" />
                        </td>
                        <td valign="middle" style="width: 130px" class="td-layout-item">
                            Workflow Status <span class="right-bold">:</span>
                        </td>
                        <td valign="middle" style="width: 125px" class="td-layout-value value-pv">
                            <dx:ASPxLabel runat="server" ID="tboxWFSts" Width="110px" CssClass="display-inline-table" />
                        </td>
                    </tr>
                </table>

            </div>
            <div class="rowbtn" >
                <table width="980px">
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnEditRow" runat="server" Text="Edit" OnClick="btEdit_Click"  OnClientClick="loading();"
                                    />
                        </td>  
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Accrued Shifting Form" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static" />

    <div style="clear:both"></div>
    <div style="clear:both"></div>

    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            
        </Triggers>
        <ContentTemplate>
            <table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="100%">
                <tr>                       
                    <td>
                        <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" 
                            AutoGenerateColumns="False"
                            ClientIDMode="Static"
                            ClientInstanceName="gridGeneralInfo" 
                            KeyFieldName="DisplaySequenceNumber" 
                            OnCustomCallback="grid_CustomCallback" 
                            OnCustomButtonCallback="grid_CustomButtonCallback"
                            OnCellEditorInitialize="evt_gridGeneralInfo_onCellEditorInitialize"
                            OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                            OnSelectionChanged="evt_gridGeneralInfo_onSelectionChanged" 
                            OnHtmlDataCellPrepared="gridGeneralInfo_HtmlDataCellPrepared"
                            >
                            <SettingsBehavior AllowFocusedRow="false" AllowDragDrop="False" AllowSelectByRowClick="True" ColumnResizeMode="Control"
                                AllowSelectSingleRowOnly="True" AllowSort="False" ProcessSelectionChangedOnServer="True" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" UseFixedTableLayout="True"
                                VerticalScrollableHeight="300" />
                            <SettingsText CommandCancel=" " CommandEdit=" " CommandUpdate="  " />
                            <Styles>
                                <AlternatingRow Enabled="True" CssClass="value-pv"/>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                </Header>
                                <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                    <Paddings PaddingTop="1px" PaddingBottom="1px" />
                                </Cell>
                                <Row CssClass="doubleRow" />
                            </Styles>
                            <SettingsLoadingPanel ImagePosition="Top" />
                            <Columns>
                                <dx:GridViewDataColumn Width="30px" FieldName="DeletionControl" Visible="false" VisibleIndex="0" >
                                    <CellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle" />
                                    <EditCellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <common:KeyImageButton ID="imgDeleteAllRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/b_delete.png"
                                            OnClick="evt_imgDeleteAllRow_clicked" CssClass="pointed" />
                                    </HeaderTemplate>
                                    <DataItemTemplate>
                                        <common:KeyImageButton ID="imgDeleteRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/b_delete.png"
                                            Key="<%# Bind('DisplaySequenceNumber') %>" OnClick="evt_imgDeleteRow_clicked"
                                            CssClass="pointed" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <common:KeyImageButton ID="imgAddRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png"
                                            Key="<%# Bind('DisplaySequenceNumber') %>" OnClick="evt_imgAddRow_clicked"   OnClientClick="RowDataChanged" CssClass="pointed"  />


                                    </EditItemTemplate>
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataTextColumn Width="30px" FieldName="DisplaySequenceNumber" Caption="No">
                                    <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <DataItemTemplate>
                                        <dx:ASPxLabel ID="lblNo" runat="server" Text="<%# Container.VisibleIndex + 1 %>" />
                                        <dx:ASPxLabel ID="lblSequenceNumber" runat="server" Text="<%# Bind('DisplaySequenceNumber') %>" Visible="false" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <dx:ASPxLabel ID="edit_lblSequenceNumber" runat="server" Text="<%# Bind('DisplaySequenceNumber') %>" Visible="false" />
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="Shifting No." HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Middle" Visible="false">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Shifting No." FieldName="SHIFTING_NO" 
                                            Width="150px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Middle" Visible="false">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Legacy Budget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" HeaderStyle-BackColor="LightBlue" >
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="WBS No Old" FieldName="WBS_NO_OLD" 
                                            Width="200px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                            <EditItemTemplate>
                                                <dx:ASPxGridLookup runat="server" ID="ddlGridWbsNoOld" ClientInstanceName="ddlGridWbsNoOld"
                                                    ClientIDMode="Static" SelectionMode="Single" KeyFieldName="KEYS" TextFormatString="{0}"
                                                    AutoPostBack="false" Width="98%" CssClass="display-inline-table" IncrementalFilteringMode="Contains" OnLoad="LookupWbsNoOld_Load" 
                                                   >
                                                    <ClientSideEvents TextChanged="WbsNoOldChanged" />        
                                                    <GridViewProperties EnableCallBacks="false">
                                                        <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                        <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                                        <SettingsPager PageSize="7" />
                                                    </GridViewProperties>
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                        <dx:GridViewDataTextColumn Caption="WBS No Old" FieldName="WBS_NO_OLD" Width="100px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="WBS Description" FieldName="WBS_DESC_OLD" Width="150px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="WBS No New" FieldName="WBS_NO_PR" Width="100px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="WBS Description" FieldName="WBS_DESC_PR" Width="150px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Booking No PR/PO" FieldName="BOOKING_NO_PR" Width="100px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Remaining" FieldName="REMAINING_PR" Width="100px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Booking No Direct" FieldName="BOOKING_NO_DIRECT" Width="100px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Remaining" FieldName="REMAINING_DIRECT" Width="100px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>   
                                                        <dx:GridViewDataTextColumn Caption="KEYS" FieldName="KEYS" Width="100px" Visible="false">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                </dx:ASPxGridLookup>
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="WBS Description" FieldName="WBS_DESC_OLD" 
                                            Width="200px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="PR/PO Budget" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" HeaderStyle-BackColor="LightGreen">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="WBS No New" FieldName="WBS_NO_PR" 
                                            Width="200px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0" Style-Font-Size="4"/>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="WBS Description" FieldName="WBS_DESC_PR" 
                                            Width="200px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Booking No." FieldName="BOOKING_NO_PR" 
                                            Width="125px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Remaining" FieldName="REMAINING_PR" 
                                            Width="100px" CellStyle-HorizontalAlign="Right" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-HorizontalAlign="Right" DisplayFormatString="#,##0" DisplayFormatInEditMode="true"/>
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Direct" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" HeaderStyle-BackColor="LightPink" >
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Booking No" FieldName="BOOKING_NO_DIRECT" 
                                            Width="125px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Remaining" FieldName="REMAINING_DIRECT"
                                            Width="100px" CellStyle-HorizontalAlign="Right" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-HorizontalAlign="Right" DisplayFormatString="#,##0" DisplayFormatInEditMode="true" />
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Shifting" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" HeaderStyle-BackColor="LightYellow" >
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Amount" FieldName="SHIFTING_AMT"  
                                            Width="150px" CellStyle-HorizontalAlign="Right" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit DisplayFormatString="#,##0"   />
                                            <EditItemTemplate>
                                                <dx:ASPxTextBox runat="server" ID="txtGridShiftingAmt" Width="98%" ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="txtGridShiftingAmt_TextChanged"/>       
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="From" FieldName="PV_TYPE_NAME"
                                            Width="125px" CellStyle-HorizontalAlign="Left" CellStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                            <PropertiesTextEdit Style-Border-BorderWidth="0"/>
                                            <EditItemTemplate>
                                                <dx:ASPxComboBox runat="server" Width="98%" ID="shiftingType" ClientIDMode="static" 
                                                    ValueType="System.String" CssClass="display-inline-table" 
                                                    TextField="Description" ValueField="Code"
                                                    DropDownWidth="100px" DropDownStyle="DropDown" OnLoad="shiftingType_Load">
                                                    <ClientSideEvents TextChanged="ShiftingType_TextChanged" />
                                                </dx:ASPxComboBox>
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <%--<Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                            <SettingsLoadingPanel ImagePosition="Top" />
                            <Styles>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                </Header>
                                <AlternatingRow CssClass="even">
                                </AlternatingRow>
                                <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                    <Paddings PaddingTop="1px" PaddingBottom="1px" />
                                </Cell>
                                <Row CssClass="doubleRow" />
                            </Styles>--%>
                            
                        </dx:ASPxGridView>
                        <asp:LinqDataSource ID="dsDivision" runat="server" OnSelecting="dsDivision_Selecting"/>

                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnButtons" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
        </Triggers>
        <ContentTemplate>
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 990px;">
                <tr>
                    <!--<td width="100px">
                        <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="btnDownload_Click" OnClientClick="loading();" CssClass="xlongButton" SkinID="xlongButton" Visible="false" Enabled="false" />
                    </td>-->
                    <td width="440px">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="loading();" OnClick="btnSubmit_Click" />
                        <dx:aspxbutton id="btnApprove" runat="server" text="Approve" width="80px"
                            enabledefaultappearance="false" enabletheming="false" native="true"
                            font-bold="true" backcolor="#00a800" forecolor="WhiteSmoke" hoverstyle-backcolor="#003300"
                            onclick="btnApprove_Click">
                            <ClientSideEvents Click="function(sender, event) { loading() }" />
                        </dx:aspxbutton>
                        <%--<asp:Button ID="btnApprove" runat="server" Text="Approve" OnClientClick="loading();" OnClick="btnApprove_Click" />
                        <asp:Button ID="btnRevise" runat="server" Text="Revise" OnClientClick="loading();" OnClick="btnRevise_Click" />--%>
                        
                        <asp:Button ID="btnPosted" runat="server" Text="Post to SAP" OnClientClick="loading();" OnClick="btnPosted_Click" CssClass="xlongButton" SkinID="xlongButton" />
                    </td>
                    <td width="300px" align="right">
                        <dx:aspxbutton id="btnRevise" runat="server" text="Revise" width="80px"
                            enabledefaultappearance="false" enabletheming="false" native="true"
                            font-bold="true" backcolor="Red" forecolor="WhiteSmoke" hoverstyle-backcolor="#800000"
                            onclick="btnRevise_Click">
                            <ClientSideEvents Click="function(sender, event) { loading() }" />
                        </dx:aspxbutton>

                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="loading();" OnClick="btnSave_Click"
                                     />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="loading();" OnClick="btnCancel_Click"
                                    />
                        <dx:aspxbutton id="btnClose" runat="server" text="Close" width="80px"
                            enabledefaultappearance="false" enabletheming="false" native="true"
                            font-bold="true" backcolor="Blue" forecolor="WhiteSmoke" hoverstyle-backcolor="#00000033"
                            onclick="btnClose_Click">
                            <ClientSideEvents Click="function(sender, event) { closeWin() }" />
                        </dx:aspxbutton>
                        <%--<asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />--%>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <table border="0" cellpadding="4" cellspacing="1" style="table-layout: fixed; width: 980px;">
        
        <tr style="height: 130px;">
            <td align="left" style="width: 490px" colspan="2">
                <asp:Panel runat="server" ID="pnAttachment" GroupingText="Attachment" Height="130px">
                    <table>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="pnupdateAttachmentGrid">
                                    <ContentTemplate>
                                        <div id="EntertainmentAttachDiv" runat="server" visible="false">
                                            <a href="../Template/Excel/Template_Entertainment_Sheet.xls">Download Entertainment Template</a>
                                        </div>
                                        <dx:ASPxGridView runat="server" ID="gridAttachment" Width="464px" ClientInstanceName="gridAttachment"
                                            ClientIDMode="Static" KeyFieldName="SequenceNumber" EnableCallBacks="false" AutoGenerateColumns="false"
                                            SettingsBehavior-AllowSort="false" SettingsBehavior-AllowSelectByRowClick="false"
                                            SettingsBehavior-AllowSelectSingleRowOnly="false" SettingsBehavior-AllowFocusedRow="false"
                                            SettingsBehavior-AllowDragDrop="False" OnHtmlRowCreated="evt_gridAttachment_onHtmlRowCreated">
                                            <SettingsBehavior AllowFocusedRow="false" AllowDragDrop="False" AllowSelectByRowClick="True" 
                                                AllowSelectSingleRowOnly="True" AllowSort="False" ProcessSelectionChangedOnServer="True" />
                                            <SettingsEditing Mode="Inline" />
                                            <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" UseFixedTableLayout="True"
                                                VerticalScrollableHeight="263" />
                                            <SettingsText CommandCancel=" " CommandEdit=" " CommandUpdate="  " />
                                            <Styles>
                                                <AlternatingRow Enabled="True" CssClass="value-pv"/>
                                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                </Header>
                                            </Styles>
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="SequenceNumber" Caption="No" Width="30px">
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel ID="lblNoAttachment" runat="server" Text="<%# Container.VisibleIndex + 1 %>" />
                                                        <dx:ASPxLabel runat="server" ID="lblAttachmentNumber" Text="<%# Bind('SequenceNumberInString') %>" Visible="false" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Category" Width="170px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblAttachmentCategory" Text="<%# Bind('CategoryName') %>" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="File Name" Width="214px">
                                                    <CellStyle HorizontalAlign="Left" />
                                                    <DataItemTemplate>
                                                        <common:BlankTargetedHyperlink runat="server" ID="lblAttachmentFileName" Text="<%# Bind('FileName') %>"
                                                            NavigateUrl="<%# Bind('Url') %>" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataColumn FieldName="#" Width="30px">
                                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <DataItemTemplate>
                                                        <common:KeyImageButton runat="server" ID="imgAddAttachment" Key="<%# Bind('SequenceNumber') %>"
                                                            CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png" OnClick="evt_imgAddAttachment_onClick">
                                                        </common:KeyImageButton>
                                                        <common:KeyImageButton runat="server" ID="imgDeleteAttachment" Key="<%# Bind('SequenceNumber') %>"
                                                            CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/red-cross.png" OnClick="evt_imgDeleteAttachment_onClick" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager PageSize="10" Mode="ShowAllRecords" AlwaysShowPager="false" />
                                        </dx:ASPxGridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td align="left" colspan="2" style="padding-left: 46px;">
                <asp:Panel runat="server" ID="pnNotice" GroupingText="Notice" Width="350px">
                    <asp:UpdatePanel runat="server" ID="upnlNotice">
                        <ContentTemplate>
                            <dx:ASPxPanel runat="server" ID="pnNotes">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <div style="display: inline; float: left; width: 100%; border: 1px solid grey; background-color: #f6f6f6;
                                            padding-top: 10px;">
                                            <div style="padding-left: 10px;">
                                                <div style="width: 68%; float: left">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 7px; vertical-align: middle;">
                                                                To&nbsp;:&nbsp;&nbsp;
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="ddlSendTo" runat="server" EnableCallbackMode="true" IncrementalFilteringMode="Contains"
                                                                    ValueType="System.String" ValueField="Username" DropDownButton-Visible="true"
                                                                    AllowMouseWheel="true" OnLoad="evt_ddlSendTo_onLoad" Width="260px" CallbackPageSize="10"
                                                                    TextFormatString="{0}" AutoPostBack="true" MaxLength="61">
                                                                    <Columns>
                                                                        <dx:ListBoxColumn FieldName="FULLNAME" Caption="Name" />
                                                                        <dx:ListBoxColumn FieldName="Username" Caption="UserName" Visible="true" />
                                                                    </Columns>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="rowright" style="vertical-align: top !important; float: left; width: 100%;
                                                border-top: 1px dashed grey; margin: 5px 0px 5px 1px">
                                                <table>
                                                    <tr>
                                                        <td valign="middle">
                                                            <asp:TextBox Enabled="true" runat="server" ID="txtNotice" Style="width: 300px; margin: 5px 5px 5px 5px;
                                                                height: 60px;" TextMode="MultiLine" />
                                                        </td>
                                                        <td valign="middle">
                                                            <dx:ASPxButton runat="server" ID="btSendNotice" Text="Send Notice" Height="60px" OnClick="evt_btSendNotice_onClick" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <asp:Literal runat="server" ID="litNoticeMessage"></asp:Literal>
                                        </div>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                            <div id="noticeWrap">
                                <div id="noticeComment" runat="server" clientidmode="Static">
                                    <asp:Repeater runat="server" ID="rptrNotice" OnItemDataBound="evt_rptrNotice_onItemDataBound">
                                        <ItemTemplate>
                                            <asp:Literal runat="server" ID="litOpenDivNotice"></asp:Literal>
                                            <asp:Literal runat="server" ID="litRptrNoticeComment"></asp:Literal>
                                            <asp:Literal runat="server" ID="litCloseDivNotice"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
            
        </tr>
        <tr>
            <td colspan="4">
                <div class="row appovtopline">
                    <div class="appovbigtitle lefty">
                        <div class="lefty">
                            <div class="box Approved"></div>
                            Approved
                        </div>
                        <div class="lefty">
                            <div class="box Waiting"></div>
                            Waiting
                        </div>
                        <div class="lefty">
                            <div class="box Rejected"></div>
                            Rejected
                        </div>
                        <div class="lefty">
                            <div class="box Skipped"></div>
                            Skip
                        </div>
                        <div class="lefty">
                            <div class="box Concurred"></div>
                            Concurrent
                        </div>
                        <div class="lefty">
                            <div class=""></div>
                        </div>
                    </div>
                    <div class="righty">
                    </div>
                </div>
            </td>
        </tr>
        <tr>

            <td align="left" style="width: 300px" colspan="2">
                <asp:Panel runat="server" ID="Panel1" GroupingText="Approval History">
                    <table>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                    <ContentTemplate>
                                        <dx:ASPxGridView runat="server" ID="gridApprovalHistory" OnLoad="gridApprovalHistory_Load" Width="460px" ClientInstanceName="gridApprovalHistory"
                                            ClientIDMode="Static" KeyFieldName="SequenceNumber" EnableCallBacks="false" AutoGenerateColumns="false"
                                            SettingsBehavior-AllowSort="false" SettingsBehavior-AllowSelectByRowClick="false"
                                            SettingsBehavior-AllowSelectSingleRowOnly="false" SettingsBehavior-AllowFocusedRow="false"
                                            SettingsBehavior-AllowDragDrop="False" OnHtmlRowCreated="gridApprovalHistory_HtmlRowCreated">
                                            <SettingsBehavior AllowFocusedRow="false" AllowDragDrop="False" AllowSelectByRowClick="false" 
                                                AllowSelectSingleRowOnly="True" AllowSort="False" ProcessSelectionChangedOnServer="True" />
                                            <SettingsEditing Mode="Inline" />
                                            <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" UseFixedTableLayout="True"
                                                VerticalScrollableHeight="100" />
                                            <SettingsText CommandCancel=" " CommandEdit=" " CommandUpdate="  " />
                                            <Styles>
                                                <AlternatingRow Enabled="True" CssClass="value-pv"/>
                                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                </Header>
                                            </Styles>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Role" Width="100px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblHistoryRole" Text="<%# Bind('ROLE_NAME') %>" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Name" Width="150px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblHistoryName" Text="<%# Bind('NAME') %>" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Status" Name="status" Width="100px">
                                                    <DataItemTemplate>
                                                        <asp:Label runat="server" ID="lblHistoryStatusCd" Text="<%# Bind('STATUS') %>" Visible="false" />
                                                        <asp:Image runat="server" ID="imgStatus" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Date" Width="100px">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblHistoryDate" Text='<%# CommonFunction.Eval_Date(Eval("ACTUAL_DT"), true) %>' />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <SettingsPager PageSize="10" Mode="ShowAllRecords" AlwaysShowPager="false" />
                                        </dx:ASPxGridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>

            
        </tr>
    </table>
    
    <!-- File Upload -->
    <asp:HiddenField ID="popUploadAttachment" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="popdlgUploadAttachment" BehaviorID="popdlgUploadAttachment"
        runat="server" DropShadow="true" TargetControlID="popUploadAttachment" PopupControlID="pnpopUploadAttachment"
        BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="pnpopUploadAttachment" CssClass="speakerPopupList"
            ClientIDMode="Static">
            <asp:UpdatePanel runat="server" ID="pnupdateFileUploadPopup">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btSendUploadAttachment" />
                </Triggers>
                <ContentTemplate>
                    <div class="uploaddiv">
                        <div class="row">
                            <label>
                                Category</label>
                            <div class="uploadr">
                                <dx:ASPxComboBox runat="server" ID="cboxAttachmentCategory" AutoPostBack="false"
                                    ClientIDMode="Static" ClientInstanceName="cboxAttachmentCategory" TextField="Description"
                                    ValueField="Code" ValueType="System.String" OnLoad="evt_cboxAttachmentCategory_onLoad" />
                            </div>
                        </div>
                        <div class="row">
                            <label>
                                File</label>
                            <div class="uploadr">
                                <asp:FileUpload runat="server" ID="uploadAttachment" ClientIDMode="Static" />
                            </div>
                        </div>
                        <div class="rowbtn">
                            <div style="width: 1px; height: 30px; clear: both;">
                                &nbsp</div>
                            <div class="btnRightLong">
                                <asp:Button ID="btSendUploadAttachment" runat="server" Text="Upload" OnClick="evt_btSendUploadAttachment_clicked"
                                    OnClientClick="return UploadAttachment_Click()" />
                                <asp:Button ID="btCloseUploadAttachment" runat="server" Text="Close" OnClick="evt_btCloseUploadAttachment_clicked" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <asp:Button ID="hidBtnPopup" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="popupActivity" runat="server" TargetControlID="hidBtnPopup"
        PopupControlID="panelActivity" CancelControlID="btnCloseActivity" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelActivity" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="upnModal">
                <ContentTemplate>
                    <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left" width="400px">
                        <tr>
                            <td valign="middle" style="width:110px" class="td-layout-item">
                                Booking No. <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" class="td-layout-item">
                                <dx:ASPxLabel runat="server" ID="lblBookingNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <dx:ASPxGridView ID="gridActivity" runat="server" Width="100%" ClientInstanceName="gridActivity"
                                    ClientIDMode="Static" 
                                    OnHtmlRowCreated="gridActivity_HtmlRowCreated" 
                                    OnCustomCallback="gridActivity_CustomCallback" EnableRowsCache="false" EnableCallBacks="true" 
                                    KeyFieldName="ACTIVITY_DES" >
                                    <Columns>
                                        <%--<dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px" Name="checkList" Caption="Check"   
                                            CellStyle-VerticalAlign="Middle" >
                                           
                                            <CellStyle VerticalAlign="Middle">
                                            </CellStyle>
                                            <HeaderTemplate>

                                            </HeaderTemplate>
                                        </dx:GridViewCommandColumn>--%>
                                        <dx:GridViewDataCheckColumn Caption="Check" Name="checkList" VisibleIndex="1" Width="40px">
                                            <DataItemTemplate>
                                          
                                                <dx:ASPxCheckBox runat="server" ID="checkList" 
                                                    Checked="<%# Bind('IS_CHECKED') %>" CheckState="Unchecked"  >
                                                    <ClientSideEvents CheckedChanged="function(s, e) { 
                                                        GetCheckBoxValue(s, e); 
                                                    }" />
                                                </dx:ASPxCheckBox>

                                            </DataItemTemplate>
                                        </dx:GridViewDataCheckColumn>
                                        <dx:GridViewDataColumn Caption="Activity Description" FieldName="ACTIVITY_DES" VisibleIndex="2">

                                        </dx:GridViewDataColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>

                            <td style="text-align: right; float:right;width:390px" class="td-layout-item">
                                <asp:Button runat="server" ID="btnSaveActivity" Text="Save" OnClick="btnSaveActivity_Click" />
                                <asp:Button runat="server" ID="btnCloseActivity" Text="Close" OnClick="btnCloseActivity_Click" />
                                
                                <asp:Button runat="server" ID="btnCancelActivity" Text="Cancel" OnClick="btnCancelActivity_Click" />

                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <asp:UpdatePanel ID="upnlConfirmationSave" runat="server">
        <Triggers>

        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationSave" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationSavePopUp" runat="server" TargetControlID="hidBtnConfirmationSave"
                PopupControlID="ConfirmationSavePanel" CancelControlID="BtnCancelConfirmationSave" OkControlID="BtnOkConfirmationSave"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationSavePanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationSave" runat="server" Text=""></asp:Label>
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn buttonRight">
                            <asp:Button ID="BtnOkConfirmationSave" Text="Yes" runat="server" 
                                OnClientClick="loading();HiddenSaveOkButton.click();"/>
                                &nbsp; 
                            <asp:Button ID="BtnCancelConfirmationSave" Text="No" runat="server" 
                                OnClick="BtnCancelConfirmationSave_Click" />
                        </div>
                            <asp:Button ID="HiddenSaveOkButton" ClientIDMode="Static" Text=" " runat="server" Width="0px" Height="0px" 
                                OnClick="btnOkConfirmationSave_Click" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="upnlConfirmationPost" runat="server">
        <Triggers>

        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationPost" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationPostPopUp" runat="server" TargetControlID="hidBtnConfirmationPost"
                PopupControlID="ConfirmationPostPanel" CancelControlID="BtnCancelConfirmationPost" OkControlID="BtnOkConfirmationPost"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationPostPanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationPost" runat="server" Text=""></asp:Label>
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn buttonRight">
                            <asp:Button ID="BtnOkConfirmationPost" Text="Yes" runat="server" 
                                OnClientClick="loading();HiddenPostOkButton.click();"/>
                                &nbsp; 
                            <asp:Button ID="BtnCancelConfirmationPost" Text="No" runat="server" 
                                OnClick="BtnCancelConfirmationPost_Click" />
                        </div>
                            <asp:Button ID="HiddenPostOkButton" ClientIDMode="Static" Text=" " runat="server" Width="0px" Height="0px" 
                                OnClick="btnOkConfirmationPost_Click" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:HiddenField ID="popRevise" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="popdlgRevise" BehaviorID="popdlgRevise"
        runat="server" DropShadow="true" TargetControlID="popRevise" PopupControlID="pnpopRevise"
        BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="pnpopRevise" CssClass="speakerPopupList" ClientIDMode="Static" Height="250px">
            <asp:UpdatePanel runat="server" ID="pnupdateFileRevise">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnReviseProceed" />
                </Triggers>
                <ContentTemplate>
                    <div class="uploaddiv">
                        <div class="row">&nbsp;
                                <label>Accrued No</label>
                                <asp:TextBox ID="txtReviseShiftingNo" runat="server" Enabled="false"/>
                        </div>
                        <div class="row">
                            <div id="RejectDiv" runat="server" >
                                <label>Category</label>
                                <asp:DropDownList ID="ddlReviseCategory" runat="server"/>                                
                            </div>
                        </div>
                        <div id="NoticeDiv" runat="server">
                            <div id="WhatDiv">&nbsp;</div>
                            <div class="row">
                                <asp:Label runat="server" ID="lblNoticeLabel" Text=" Reason:"/>
                            </div>
                            <div class="row">
                                <asp:TextBox ID="txtReviseComment" ClientIDMode="Static" runat="server"  
                                    Width="98%" Height="50px" TextMode="MultiLine" BackColor="#EEEEEE"/>
                            </div>
                        </div>
                        <div class="row" style="width:500px">&nbsp;</div>
                        <div class="rowbtn">
                            <div class="btnRightLong">
                                <asp:Button ID="btnReviseProceed" runat="server" Text="Proceed" OnClick="evt_btnReviseProceed_Click" OnClientClick="return IsEmptyReason()" />
                                <asp:Button ID="btnReviseCancel" runat="server" Text="Cancel" OnClick="btnReviseCancel_Click" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
 
</asp:Content>
