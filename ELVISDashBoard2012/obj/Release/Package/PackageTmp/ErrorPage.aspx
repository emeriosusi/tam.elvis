﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="ELVISDashBoard.ErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">

    <div  class="errorPage"></div>
    <div class="errorPageP">
            <p style="text-align:center">Something went wrong, Please go back to <a href="Home.aspx">Home</a> page</p>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
