﻿<%@ Page Title="Power Of Attorney" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master" AutoEventWireup="true" CodeBehind="PoA.aspx.cs" Inherits="ELVISDashBoard.Administration.PoA" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register src="../UserControl/Calendar.ascx" tagname="Calendar" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register assembly="DevExpress.Web.v12.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="SS" ContentPlaceHolderID="pre" runat="server">
<style type="text/css">
.rowlabel 
{
    margin: 0px 0px 0px 9px;
    width: 135px;
    float: left;
    display: block;
    line-height: 25px;
    vertical-align: middle;
}    
.rowd 
{
    margin: 0px 0px 0px 0px;
    display: block;
    line-height: 25px;
    vertical-align: middle;
}
.nomargin
{
    margin: 0px 0px 0px 0px !important;
}    
.dateEntry
{
    float: left;
    vertical-align: middle;
    margin: 0px 0px 0px 0px;
    padding: 3px 0px 0px 0px;
    margin: 0px 0px 0px 0px;
    display: block;
}
.vmid 
{
    vertical-align:middle;
}
.fill
{
    margin-left: 10px;
    margin-right: 10px;
    padding-left: 5px;
    padding-right: 5px;
    display: block;
}

</style>

<script type="text/javascript">
    function closePop() {
        var pop = $find("ConfirmationDeletePopUp");
        pop.hide();
    }
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Power Of Attorney" /> 
    <asp:HiddenField runat="server" ID="hidScreenID" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
<Triggers>
   <%-- <asp:AsyncPostBackTrigger ControlID="btnCancel" />
    <asp:AsyncPostBackTrigger ControlID="btnAdd" />
    <asp:AsyncPostBackTrigger ControlID="btnEdit" />
    <asp:AsyncPostBackTrigger ControlID="btnSearch" />
    <asp:AsyncPostBackTrigger ControlID="btnClear" />
    <asp:AsyncPostBackTrigger ControlID="btnSave" />--%>
</Triggers>
<ContentTemplate> 

<asp:Literal runat="server" ID="litOpenDiv"></asp:Literal>
    <asp:Image ID="imgMessageSuccess" runat="server" SkinID="successMessage" Visible="false" />
    <asp:Image ID="imgMessageError" runat="server" SkinID="errorMessage" Visible="false" />
    <asp:Literal runat="server" ID="litMessage" EnableViewState ="false"></asp:Literal>
    <asp:HiddenField ID="hidPageMode" runat="server" />
<asp:Literal runat="server" ID="LitCloseDiv"></asp:Literal>

</ContentTemplate>
</asp:UpdatePanel>

   
<asp:UpdatePanel runat="server" ID="UpdatePanel2">
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnCancel" />
    <asp:AsyncPostBackTrigger ControlID="btnAdd" />
    <asp:AsyncPostBackTrigger ControlID="btnEdit" />
    <asp:AsyncPostBackTrigger ControlID="btnSearch" />
    <asp:AsyncPostBackTrigger ControlID="btnClear" />
    <asp:AsyncPostBackTrigger ControlID="btnSave" />
</Triggers>
<ContentTemplate> 
<div class="contentsection">
    <div>
      <div class="row nomargin">
        <div>
        <div class="rowlabel">POA Number</div>
            <div class="rowwarphalf">   
                :&nbsp<asp:Textbox runat="server" Id="txtPOANumber"></asp:Textbox>                
            </div>
        </div>  
       </div>
        <div class="row nomargin">
            <div>
                <div class="rowlabel">Grantor Of Attorney</div>
                <div class="rowwarphalf">   
                 :&nbsp<asp:Textbox runat="server" Id="txtGrantorOfAttorney"></asp:Textbox>
                </div>
             </div>  
       </div>
       <div class="row nomargin">
            <div>
            <div class="rowlabel">Attorney</div>
                <div class="rowwarphalf" >   
                    :&nbsp;<asp:DropDownList ID="ddlAttorney" runat="server" Width="142">                        
                    </asp:DropDownList>                                      
                </div>
                <div class="rowwarpright" style="text-align:right">
                    
                </div>
            </div>
       </div>
       <div class="row nomargin">
            <div>
                <div class="rowlabel">Status Office</div>
                <div class="rowwarphalf">
                    :&nbsp;<asp:DropDownList ID="ddlOfficeStatus" runat="server" Width="142">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Text="In Office" Value="1"></asp:ListItem>
                        <asp:ListItem  Text="Out Of Office" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                  <div class="rowwarpright" style="text-align:right;">
                </div>
            </div>
       </div>
       <div class="row nomargin">
            <div class="rowlabel">Valid From</div>
            <div class="lefty rowd"> :&nbsp;</div>
            <div class="lefty rowd" id="fromToDiv" runat="server">
                <div class="dateEntry rowd">
                        <dx:ASPxDateEdit ID="dtValidFrom" runat="server" Width="100px" DisplayFormatString="dd MMM yyyy" 
                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                        AutoPostBack="false" ClientIDMode="Static" >
                        <ClientSideEvents DateChanged="OnDateChanged" />
                     </dx:ASPxDateEdit>
                </div>
                <div class="lefty vmid rowd" style="padding-left:10px;padding-right:5px;">To : </div>
                <div class="dateEntry rowd">
                        <dx:ASPxDateEdit ID="dtValidTo" runat="server" Width="100px" DisplayFormatString="dd MMM yyyy" 
                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                                        AutoPostBack="false" ClientIDMode="Static" >
                                        <ClientSideEvents DateChanged="OnDateChanged" />
                                    </dx:ASPxDateEdit>            
                </div>
            </div>
            <div class="btnRightLong">
                    <asp:Button runat="server" id="btnSearch" Text="Search" UseSubmitBehavior="true"
                        onclick="btnSearch_Click" OnClientClick="loading()"/>
                    <asp:Button runat="server" id="btnClear" Text="Clear" onclick="btnClear_Click"/>
            </div>
       </div>
     </div>
</div>
<br />

<div class="row">
            <div class="rowwrap" style="text-align:right;">
                    <asp:Button runat="server" id="btnAdd" Text="Add" onclick="btnAdd_Click" OnClientClick="loading();"/>
                    &nbsp;
                    <asp:Button runat="server" id="btnEdit" Text="Edit" onclick="btnEdit_Click" OnClientClick="loading();"/>
                    &nbsp;
                    <asp:Button runat="server" id="btnDelete" Text="Delete" onclick="btnDelete_Click"/>
            </div>
</div>

</ContentTemplate>
</asp:UpdatePanel>

<asp:UpdatePanel runat="server" ID="upnlGrid">
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnAdd" />
    <asp:AsyncPostBackTrigger ControlID="btnEdit" />
    <asp:AsyncPostBackTrigger ControlID="btnSearch" />
    <asp:AsyncPostBackTrigger ControlID="btnCancel" />
    <asp:AsyncPostBackTrigger ControlID="btnSave" />
    <asp:AsyncPostBackTrigger ControlID="btnClear" />
</Triggers>
<ContentTemplate>

<div style="height:305px;width:100%;margin:auto;">

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" 
         ClientInstanceName="grid"
        AutoGenerateColumns="False" OnHtmlRowCreated="grid_HtmlRowCreated" 
        ClientIDMode="AutoID" KeyFieldName="POA_NUMBER" width="990px"
        DataSourceID="LinqDataSource1" 
        onhtmldatacellprepared="ASPxGridView1_HtmlDataCellPrepared" 
        onhtmlrowprepared="ASPxGridView1_HtmlRowPrepared" 
        onpageindexchanged="ASPxGridView1_PageIndexChanged"
        Styles-AlternatingRow-CssClass="even">
         <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
         <SettingsPager AlwaysShowPager="True">
         </SettingsPager>
         <SettingsEditing Mode="Inline" />
        <Columns>
            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" 
                Width="30px">
                <EditButton Visible="false" Text=" " ></EditButton>
                <UpdateButton Visible="false" Text=" " ></UpdateButton>
                <NewButton Visible="false" Text=" " ></NewButton>
                <CancelButton Visible="false" Text=" " ></CancelButton>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewCommandColumn>
           <dx:GridViewDataTextColumn FieldName="POA_NUMBER" ReadOnly="True" VisibleIndex="0" Caption="POA Number">
                <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </EditCellStyle>
                <EditItemTemplate>
                    <asp:Literal runat="server" ID="litGridPOANumber"></asp:Literal>
                </EditItemTemplate>
                             <HeaderStyle HorizontalAlign="Center" />
                             <CellStyle HorizontalAlign="Center">
                             </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GRANTOR" VisibleIndex="1" Caption="Grantor Of Attorney" Width="130px">
                <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                <EditFormCaptionStyle HorizontalAlign="Center"/>
                <HeaderStyle HorizontalAlign="Center" />                         
                <CellStyle HorizontalAlign="Center"/>
                <EditItemTemplate>                                        
                     <dx:ASPxComboBox runat="server" 
                        ID="ddlGrantor" Width="126px" 
                        InvalidStyle-HorizontalAlign="NotSet" 
                        DataSourceID="dsGrantor" 
                        ValueType="System.String"
                        ValueField="Code"
                        IncrementalFilteringMode="Contains"
                        IncrementalFilteringDelay="1000"
                        OnInit="ddlGrantor_Init"
                        TextFormatString="{0}">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Description" Caption="Name" />
                            <dx:ListBoxColumn FieldName="Code" Caption="ID" />
                        </Columns>
                     </dx:ASPxComboBox>
                </EditItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ATTORNEY" VisibleIndex="2" 
                Caption="Attorney" Width="120px">
                <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" />
                <EditItemTemplate>
                    <dx:ASPxComboBox runat="server" ID="dgAttorney" width="120"
                        DataSourceID="dsAttorney" 
                        DropDownStyle="DropDownList"
                        ValueField="Code"
                        ValueType="System.String"
                        IncrementalFilteringMode="Contains"
                        IncrementalFilteringDelay="1000"
                        OnInit="dgAttorney_Init"
                        TextFormatString="{0}">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Description" Caption="Name" />
                            <dx:ListBoxColumn FieldName="Code" Caption="ID" />
                        </Columns>
                    </dx:ASPxComboBox>
                </EditItemTemplate>
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="REASON" VisibleIndex="3" Caption="Reason" 
                Width="200px">
                <EditCellStyle VerticalAlign="Middle"/>
                <EditItemTemplate>
                <asp:TextBox runat="server" ID="txtGridReason" Width="190"></asp:TextBox>
                </EditItemTemplate>
                         <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="OFFICE_STATUS" VisibleIndex="4" 
                Caption="Status Office" Width="120px" >
                         <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                         </EditCellStyle>
                         <DataItemTemplate>                           
                             <asp:Literal ID="litOfficeStatus" runat="server"></asp:Literal>
                         </DataItemTemplate>
                         <EditItemTemplate></EditItemTemplate>
               <%--          <EditItemTemplate>
                            <asp:DropDownList runat="server" ID="dgOfficeStatus" width="90px" CssClass="hidden">
                                <asp:ListItem Value=""></asp:ListItem>
                                <asp:ListItem Text="In Office" value="1"></asp:ListItem>
                                <asp:ListItem  Text="Out Of Office" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                         </EditItemTemplate>--%>
                         <HeaderStyle HorizontalAlign="Center" />
                         <CellStyle HorizontalAlign="Center">
                         </CellStyle>
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn FieldName="VALID_FROM" VisibleIndex="4" Caption="Valid From" 
                Width="135px">
                <CellStyle HorizontalAlign="Center"/>
                <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy}"/>
                <EditItemTemplate>
                    <dx:ASPxDateEdit ID="dtGridValidFrom" runat="server" Width="131px" DisplayFormatString="dd MMM yyyy" 
                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                        AutoPostBack="false" ClientIDMode="Static" >
                        <ClientSideEvents DateChanged="OnDateChanged" />
                    </dx:ASPxDateEdit>
                </EditItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="VALID_TO" VisibleIndex="4" Caption="Valid To" 
                Width="135px">
                <CellStyle HorizontalAlign="Center"/>
                <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy}"/>
                <EditItemTemplate>
                    <dx:ASPxDateEdit ID="dtGridValidTo" runat="server" Width="131px" DisplayFormatString="dd MMM yyyy" 
                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                        AutoPostBack="false" ClientIDMode="Static" >
                        <ClientSideEvents DateChanged="OnDateChanged" />
                    </dx:ASPxDateEdit>
                </EditItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>

        <dx:GridViewBandColumn Caption="Created" VisibleIndex="5">
        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
            <Columns>
                <dx:GridViewDataTextColumn Caption="Date" FieldName="CREATED_DT" VisibleIndex="0" Width="135px" SortIndex="0" SortOrder="Descending">
                    <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Settings SortMode="Value" AllowSort="True" />
                    <EditItemTemplate>
                    </EditItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1" Width="100px">
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <EditItemTemplate>
                    </EditItemTemplate>
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:GridViewBandColumn>
        <dx:GridViewBandColumn Caption="Changed" VisibleIndex="6">
        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
            <Columns>
                <dx:GridViewDataTextColumn Caption="Date" FieldName="CHANGED_DT" VisibleIndex="0" Width="135px">
                    <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}"/>
                    <EditItemTemplate>
                    </EditItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="By" FieldName="CHANGED_BY" VisibleIndex="1" Width="100px">
                    <EditItemTemplate>
                    </EditItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:GridViewBandColumn>
        <dx:GridViewBandColumn Caption="Realization" VisibleIndex="7">
        <HeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" />
        <Columns>
        <dx:GridViewDataTextColumn Caption="Granted" VisibleIndex="11" FieldName="GRANTED_DATE" Width="135px">
                    <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}"/>
                    <EditItemTemplate>
                    </EditItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Revoked" VisibleIndex="12" FieldName="REVOKED_DATE" Width="135px">
                    <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}"/>
                    <EditItemTemplate>
                    </EditItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </dx:GridViewDataTextColumn>
        </Columns>
        </dx:GridViewBandColumn>
        </Columns>
         <SettingsEditing Mode="Inline" />
         <Settings ShowVerticalScrollBar="True"
             VerticalScrollableHeight="250" 
             ShowHorizontalScrollBar="True" />
         <SettingsEditing Mode="Inline" />
         <SettingsLoadingPanel ImagePosition="left" Mode="ShowAsPopup" />
         <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />
    </dx:ASPxGridView>
    
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" 
        ContextTypeName="DataLayer.Model.ELVIS_DBEntities" EntityTypeName=""         
        TableName="TB_R_POA" >
    </asp:LinqDataSource>

        
    
    <asp:LinqDataSource ID="dsGrantor" runat="server"
        OnSelecting="dsGrantor_Selecting" />
    <asp:LinqDataSource ID="dsAttorney" runat="server"
        OnSelecting="dsAttorney_Selecting" />

         <div class="row" style="text-align:right">
             <asp:Button ID="btnSave" runat="server" text="Save" onclick="btnSave_Click" OnClientClick="loading();" />
             &nbsp;
             <asp:Button ID="btnCancel" runat="server" text="Cancel" 
                 onclick="btnCancel_Click1" />          
         </div>
         <div class="row" style="text-align:right">
            <asp:Button ID="btnClose" runat="server" text="Close" onclick="btnClose_Click" OnClientClick="closeWin()"/>
         </div>
</div>  
  </ContentTemplate>
    </asp:UpdatePanel> 

    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnOkConfirmationDelete" />
            <asp:AsyncPostBackTrigger ControlID="HiddenDeleteOkButton" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" 
                ClientIDMode="Static"
                TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" 
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn">
                            <div class="btnRightLong">
                            <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" OnClientClick="closePop();loading();"
                                OnClick="btnOkConfirmationDelete_Click"
                            />
                        &nbsp; <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" />

                            <asp:Button ID="HiddenDeleteOkButton" ClientIDMode="Static" Text=" " runat="server" Width="0px" Height="0px" 
                                OnClick="btnOkConfirmationDelete_Click"/>
                            </div>
                        </div>
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
