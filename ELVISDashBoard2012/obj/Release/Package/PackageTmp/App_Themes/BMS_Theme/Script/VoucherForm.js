﻿function expandScreenMessage() {
    var hdExpandedFlag = $('#hdExpandFlag');
    var speed = "fast";
    if (hdExpandedFlag.val() == "true") {
        $('#message-detail-link').text("more");
        $('#messageBox-all').slideUp(speed);
        $('#messageBox').removeClass("expanded-message-box");
        hdExpandedFlag.val("false");
    } else {
        $('#message-detail-link').text("close");
        $('#messageBox-all').slideDown(speed);
        $('#messageBox').addClass("expanded-message-box");
        hdExpandedFlag.val("true");
    }
}
var __columnFieldName_DELETION_CONTROL = "DeletionControl";
var __columnFieldName_SEQUENCE_NUMBER = "DisplaySequenceNumber";
var __columnFieldName_COST_CENTER_CODE = "CostCenterCode";
var __columnFieldName_COST_CENTER_NAME = "CostCenterName";
var __columnFieldName_DESCRIPTION = "Description";
var __columnFieldName_DESCRIPTION_DEFAULT_WORDING = "StandardDescriptionWording";
var __columnFieldName_CURRENCY_CODE = "CurrencyCode";
var __columnFieldName_AMOUNT = "Amount";
var __columnFieldName_INVOICE_NUMBER = "InvoiceNumber";
var __columnFieldName_TAX_CODE = "TaxCode";
var __columnFieldName_TAX_NUMBER = "TaxNumber";

var FOCUSED_COLUMN_NAME_FIELD = "hdGridFocusedColumn";

function VoucherForm_setFocusedColumn(columnName) {
    $('#'.concat(FOCUSED_COLUMN_NAME_FIELD)).val(columnName);
}

function VoucherForm_isFocusRequested(columnName) {
    var focusedColumn = $('#'.concat(FOCUSED_COLUMN_NAME_FIELD)).val();
    return focusedColumn == columnName;
}

function VoucherForm_loadFocus(sender, columnName) {
    if (VoucherForm_isFocusRequested(columnName)) {
        sender.Focus();
    }
}

/* 
    Component events 
*/
function evt_ddlTaxCode_TextChanged(sender, event) {
    var selectedValue = sender.GetText();
    tboxTaxNumber.SetVisible(selectedValue == 'V1');
}
function evt_ddlTaxCode_KeyPress(sender, event) {
    var keyCode = event.htmlEvent.keyCode;
    if (keyCode == '9') {
        var value = sender.GetText();
        var focusedColumn = "";
        if (value == 'V1') {
            focusedColumn = __columnFieldName_TAX_NUMBER;
        }
        VoucherForm_setFocusedColumn(focusedColumn);
    }
}
function evt_ddlCostCenterCode_ValueChanged(sender, event) {
    var gridView = sender.GetGridView();
    var idxFocused = gridView.GetFocusedRowIndex();
    gridView.GetRowValues(idxFocused, 'Description', function (values) {
        tboxCostCenterName.SetText(values);
        tboxDescription.Focus();
    });
}

function Detail_TextChanged(s, e) {
    var g = s.GetGridView();
    var idxFocused = g.GetFocusedRowIndex();
    g.GetRowValues(idxFocused, 'Code', function (values) {
        gridPVDetail.PerformCallback('p:update:CostCenterCode:' + values); 
    });          
    g.Refresh();
}

function CurrencyCode_TextChanged(s, e) {
    var t = s.GetValue();
    var g = eval('gridPVDetail');
    var idxFocused = g.GetFocusedRowIndex();
    gridPVDetail.PerformCallback('p:update:CurrencyCode:' + t);
}

function Amount_Focused(s, e) {
    var t = coCurrencyCode.Text;
    var roundCurrencies = $("hidRoundCurrency").val();
    s.SelectAll();
}

function TransactionType_TextChanged(sender,event) {
    var gridView = sender.GetGridView();
    var idxFocused = gridView.GetFocusedRowIndex();
    gridView.GetRowValues(idxFocused, 'StandardWording', function (values) {
        gridPVDetail.PerformCallback('p:update:TxCode');
    });    
}

function BankType_TextChanged(sender, event) {
    var g = sender.GetGridView();
    var bt = sender.GetText();
    var i = g.GetFocusedRowIndex();
    g.GetRowValues(i, 'Type', function (values) {        
        gridPVDetail.PerformCallback('p:update:BankType:'+bt);
    });
}
function tboxAmount_blur(s, e) {
    var x = s;
}

function UploadAttachment_Click(s, e) {
    var cat = eval(cboxAttachmentCategory);
    if (cat != null) {
        var i = cat.GetSelectedIndex();
        if (i < 0 || cat.GetText() == "") {
            alert("Select Category before upload");            
            return false;
        }
    }
    var fn = $("#uploadAttachment").val();
    if (fn == "" || fn == null) {
        alert("Browse file to Upload");      
        return false;
    }
    if (fn.indexOf("&") >= 0 || fn.indexOf("+") >= 0 || fn.indexOf("%") >= 0 || fn.indexOf("?") >= 0 || fn.indexOf("*") >= 0) {
        return confirm("File name contains banned & + % * ? chars \r\n Rename, then press Cancel, select other(renamed) file\r\n or Proceed Upload (replacing banned chars with '_' ) ?");
    }
    return true;
}

function BeforeUpload_Click(s, e) {
    var fn = $("#fuInvoiceList").val();
    if (!fn || fn.length() < 1) {
        alert("Select file");
        return false;
    }
    return true;
}

function BeforePostToSAP_Click(s, e) {
    var vc = $("#hidVendorGroup").val();
    var pm = $("#hidPaymentMethod").val();
    var popOneTimeVendor = $find("popOneTimeVendor");
    $("#hidOneTimeVendorValid").val("0");

    if (!vc || vc.length < 1) {
        alert("No Vendor Group Code");
    } else {
        var vCode = parseInt(vc, 10);
        if (vCode == 4) {
            popOneTimeVendor.show();
            $("#btnPostOneTimeVendor").removeAttr("disabled");
            if (pm == "A") {
            
                $("#bankAccountRow").addClass("hidden");
                $("#bankKeyRow").addClass("hidden");
            } else {
                
                $("#bankAccountRow").removeClass("hidden");
                $("#bankKeyRow").removeClass("hidden");
            }
        }
        else {
            loading();
            
            $("#hidOneTimeVendorValid").val("1");
            return true;
        }
    }
    return false;
}
/// unused 
function evt_lpkgridBankType_onFocusedRow(sender, evt) {
    var hdSelectedBankType = $("#hdSelectedBankType");
    var focusedRowIndex = sender.GetFocusedRowIndex();
    hdSelectedBankType.val(focusedRowIndex);
    __doPostBack("hdSelectedBankType", focusedRowIndex.toString());
}

function BlockNonIntendedBackspace(e) {
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
        e.preventDefault();        
    }
}