﻿var getCookies = function () {
    var pairs = document.cookie.split(";");
    var cookies = {};
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split("=");
        cookies[pair[0]] = unescape(pair[1]);
    }
    return cookies;
}

var getCookieNames = function () {
    var p = document.cookie.split(";");
    var names = Array();
    for (var i = 0; i < p.length; i++) {
        var x = p[i].split("=");

        names[i] = x[0];
    }
    return names;
}

function setCookie(namo, value, ago) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + ago);
    var cooko = escape(value) + ((ago == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = namo + "=" + cooko;
}

function GetCookie(key) {
    var c = getCookies();
    return c[key];
}

var isFocused = true;
var ReloadDelay = 5000;
var periodicReload = false;
var fseq = 0

function download(filename, text) {
    fseq++;
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain; charset=utf-8,' + encodeURIComponent(text));

    element.setAttribute('download', filename + fseq + ".txt");

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function GetUserData() {
    $.ajax({
        type: 'POST',
        url: '/ws.asmx/userdata',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',

        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-type",
                           "application/json; charset=utf-8");
        },
        success: function (dx) {
            var q = dx.d;
            if (q != null) {
                download("UserData", q);
                // $('#UserDataDiv').html(q.USERNAME);
            } else {
                $('#ErrDiv').html("None");
            }
        },

        error: function (r, er) {
            $('#ErrDiv').html(r.responseText);
        }

    });
}

function GetSession() {
    var n = $("#VarText").val();

    $.ajax({
        type: 'POST',
        url: '/ws.asmx/session',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({ "name": n }),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-type",
                           "application/json; charset=utf-8");
        },
        success: function (dx) {
            // console.log(dx);
            var q = dx.d;
            if (q != null) {
                if (!n || n.length == 0)
                    download("session", q);
                else
                    download(n, q);
                // $('#UserDataDiv').html(q);
            } else {
                $('#ErrDiv').html("none");
            }
        },

        error: function (r, er) {
            $('#ErrDiv').html(r.responseText);
        }

    });
}


function GetHeap() {
    var n = $("#VarText").val();

    $.ajax({
        type: 'POST',
        url: '/ws.asmx/heap',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({ "name": n }),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-type",
                           "application/json; charset=utf-8");
        },
        success: function (dx) {
            
            var q = dx.d;
            if (q != null) {
                if (!n || n.length == 0)
                    download("heap", q);
                else
                    download(n, q);                
            } else {
                $('#ErrDiv').html("");
            }
        },

        error: function (r, er) {
            $('#ErrDiv').html(r.responseText);
        }

    });
}

function Reload() {
    if (periodicReload && isFocused) {

        location.reload();
    }
}
var iId;
function ToogleReload() {
    periodicReload = !periodicReload;
    if (periodicReload) {
        $('#Ticking').val('Untick');

        if (GetCookie('Tick') == null) {
            setCookie('Tick', 1, 1000);
        } else {
        }
        iId = window.setInterval(Reload, ReloadDelay);
    } else {
        $('#Ticking').val('Tick');
        window.clearInterval(iId);
        iId = 0;
    }
}

function FillSessionVars() {
    $.ajax({
        type: 'POST',
        url: '/ws.asmx/session',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({ "name": "" }),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-type",
                           "application/json; charset=utf-8");
        },
        success: function (dx) {
            var q = dx.d;
            if (q != null) {
                var qo = JSON.parse(q);
                $.each(qo, function (i, a) {
                    var v = $("#SessionVars tbody:first-child")
                        .clone()
                        .attr("id", "row" + i)
                        .removeClass("hide")
                        .insertAfter("#SessionVars tbody:last-child");
                    // console.log('i:', i, ' a:', a);
                    $(v).find(".colnm").html(a);
                });
            } else {
                $('#ErrDiv').html("none");
            }
        },

        error: function (r, er) {
            $('#ErrDiv').html(r.responseText);
        }

    });
}
$(document).ready(function () {
    $(window).focus(function () {
        isFocused = true;
    });

    $(window).blur(function () {
        isFocused = false;
    });


    $("#Ticking").click(function (event) {
        event.preventDefault();
        ToogleReload();
    });

    $("#UserDataButton").click(function (event) {
        event.preventDefault();
        GetUserData();
    });


    $("#GetSessionButton").click(function (event) {
        event.preventDefault();
        GetSession();
    });

    $("#GetHeapButton").click(function (event) {
        event.preventDefault();
        GetHeap();
    });

    if (GetCookie('Tick') == 1) {
        ToogleReload();
    }

    FillSessionVars();

})