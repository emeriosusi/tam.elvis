﻿var clusterStatus = [];
var clusterData = [];
var transactionData = [];
var clusterItems = [];
var clusterTransaction = [];
var dataOK = []; 
var errs = [];
var datas = {};
var fields = {}; 
var defaultGridOptions = {}
var gridOptions = {}
var PutDataQueue = [];

function Init() {
datas = {
    "cluster": [],
    "transaction": [] 
};

fields = {
    "cluster": [],

    "transaction":
    [
        { name: "TrCode", title:"Code", type: "text", width: 30, editing: false, align: "center", sorter: function (id1, id2) { return id1 - id2; } },
        { name: "TrName", title:"Name", type: "text", width: 200, editing: false },
        { name: "TrLevel", title:"Level", type: "text", width: 30, editing: false, align: "center", sorter: function (le1, le2) { return le1 - le2; } },
        { name: "ClusterId", type: "select", items: [], valueField: "ClusterId", textField: "ClusterName", title: "Cluster", width: 40, editing: true },
        { name: "Act", type: "control", title: "Do", width: 30, deleteButton: false }
]};

defaultGridOption = {
    width: "100%",
    height: GetGridHeight(),

    inserting: true,
    editing: true,
    sorting: true,
    paging: true,
    pageSize: 12
}

gridOptions = {
    'transaction': 
               { inserting: false, filtering: true, autoload: true,
                   onItemUpdating: Grid_OnUpdating, onItemUpdated: Transaction_OnUpdated , 
                   onPageChanged: Transaction_OnPageChanged
                 },
    'cluster': { onItemDeleted: Cluster_OnDel, onItemInserting: Cluster_OnInserting, onItemInserted: Cluster_OnEdit, 
                 onItemUpdating: Grid_OnUpdating, onItemUpdated: Grid_OnUpdated }
};

}

var gridActions = {
    'transaction': SaveTransactionCluster, 
    'cluster': SaveClusterData
}

function getClusterItems() {
    clusterItems = [{"ClusterId": null, "ClusterName": ""}];
    
    for(var di = 0; di < clusterData.length; di++) {
            var cd = clusterData[di];
            clusterItems.push({ "ClusterId": cd.ClusterId, "ClusterName": cd.ClusterName });
    }    
}

function Cluster_OnDel(args) {   
    PutData("cluster", { "id": args.item.id, "name": "", "text": "", "act": "1" }, function (d) { say("Data Deleted ","Delete") });
    var deleteIndex = -1;
    for (var ci = 0; ci < clusterData.length; ci++) {
        if (clusterData[ci].ClusterId == args.item.id) {
            deleteIndex = ci;
            break;
        }
    }
    if (deleteIndex>=0)
        clusterData.splice(deleteIndex, 1);
}
function Cluster_OnInserting(args) {    
    args.item['f0'] = 1;
    args.item['f7'] = 1;     
}


var isUpdating = 0;
var prevPage = 0;
function Transaction_OnUpdated(args) {
    isUpdating = 0;
}

function Transaction_OnPageChanged(args) {
    if (!isUpdating) 
        prevPage = args.pageIndex;
}

function Grid_OnUpdating(args) {
    isUpdating = 1;
}

function Grid_OnUpdated(args) {
    isUpdating = 0;
}

function strtoa(s) {
    var na = []; 
    var sa = s.split(',');
    for(var si = 0; si < sa.length; si++) {
        var n = parseInt(sa[si]);
        if (n!=NaN)
            na.push(n);
    }
    return na;
}

function Cluster_OnEdit(args) {
    
    var atext = fieldsValueToCSV(args.item); 

    PutData("cluster", { "id": args.item.id, "name": args.item.Name, "text": atext, "act": "0" }, function (d) { say("Data Saved","Save") });
    var cindex = -1;
    for (var ci = 0; ci < clusterData.length; ci++) {
        if (clusterData[ci].ClusterId == args.item.id) {
            
            clusterData[ci].ClusterName = args.item.Name;
            clusterData[ci].StatusCodeList = atext;
            clusterData[ci].Status = strtoa(atext);
            cindex = ci;
            break;
        }
    }

    if (cindex < 0) {
        clusterData.push({ ClusterId: args.item.id, ClusterName: args.item.Name, StatusCodeList: atext, Status: strtoa(atext) });
    }
    
}

function sameContentslist(a1,a2) {
    var aa1 = (a1) ? a1.split(",") : [];
    var aa2 = (a2) ? a2.split(",") : [];
    var same = false;
    do {
        if (aa1.length != aa2.length) break;
        var notIn = false;
        for (var a1i = 0; a1i < aa1.length; a1i++) {
            if (aa2.indexOf(aa1[a1i]) < 0){
                notIn = true;
                break;
            }
        }
        if (notIn) break;
        same = true;
    } while(false);
    return same;
}


// The download function takes a CSV string, the filename and mimeType as parameters
// Scroll/look down at the bottom of this snippet to see how download is called
var download = function (content, fileName, mimeType) {
    var a = document.createElement('a');
    mimeType = mimeType || 'application/octet-stream';

    if (navigator.msSaveBlob) { // IE10
        navigator.msSaveBlob(new Blob([content], {
            type: mimeType
        }), fileName);
    } else if (URL && 'download' in a) { //html5 A[download]
        a.href = URL.createObjectURL(new Blob([content], {
            type: mimeType
        }));
        a.setAttribute('download', fileName);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    } else {
        location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
    }
}


function fieldsValueToCSV(item) {
    var atext = "";
    for (var nf in item) {
        if (nf.substr(0, 1) == "f" && parseInt(nf.substr(1)) != NaN) {
            if (item[nf]) {
                atext = atext + ((atext.length > 0)?",":"") + parseInt(nf.substr(1));
            }
        }
    }
    return atext;
}

function SaveClusterData() {
    var clusterNewData = $("#gcluster").jsGrid("option", "data");
    var changed = false;
    for (var cni = 0; cni < clusterNewData.length; cni++) {
        var item = clusterNewData[cni];
        var atext = fieldsValueToCSV(item);
        var cindex = -1;
        for (var ci = 0; ci < clusterData.length; ci++) {
            if (clusterData[ci].ClusterId == item.id) {
                cindex = ci;
                if (
                        (item.Name != clusterData[ci].ClusterName)
                     || (!sameContentslist(clusterData[ci].StatusCodeList, atext))
                    )
                {
                    clusterData[ci].ClusterName = item.Name;
                    clusterData[ci].StatusCodeList = atext; 
                    clusterData[ci].Status = strtoa(atext); 

                    PutData("cluster", { "id": item.id, "name": item.Name, "text": atext, "act": "0" }, null);
                    changed = true;


                }
                break;
            }
        }
        if (cindex < 0) {
            clusterData.push({ ClusterId: item.id, ClusterName: item.Name, StatusCodeList: atext, Status: strtoa(atext) });
            changed = true;
        }
    }
    if (changed) {
        say("Cluster Data saved", "Save");
    }
}

function SaveTransactionCluster() {
    var fgrid = $("#gtransaction").jsGrid("getFilter");
    var lastPageIndex = prevPage;
    isUpdating = 1;
    $("#gtransaction").jsGrid("clearFilter");
    
    var tdata = $("#gtransaction").jsGrid("option", "data");
    var tn = {};
    var saveCount = 0;
    for (var ci = 0; ci < clusterData.length; ci++) {
        tn[clusterData[ci].ClusterId] = [];
    }

    for (var ti = 0; ti < tdata.length; ti++) {
        var td = tdata[ti];
        var tcode = (td.TrCode) ? parseInt(td.TrCode) : null;
        var cid = (td.ClusterId)? parseInt(td.ClusterId) : null;
        if (tn[cid])
            tn[cid].push(tcode);
    }
    
    for (var tx in tn) {
        var atx = tn[tx];
        var changed = false;
        var atext = atx.join(",");
        var ctindex = -1;
        for(var cti = 0;cti < clusterTransaction.length; cti++) {
            if (parseInt(clusterTransaction[cti].ClusterId) == tx) {
                ctindex = cti;
                changed = !sameContentslist(clusterTransaction[cti].TransactionIdList, atext);
            }
        }
        if (changed || (ctindex < 0)) {
            PutData("clusterTransaction", { "id": tx, "text": atext, "act": "0" }, null);
            saveCount++;
            if (ctindex >= 0) {
                clusterTransaction[ctindex].TransactionIdList = atext;
                clusterTransaction[ctindex].TransactionId = strtoa(atext);
            }
            else {
                clusterTransaction.push({ClusterId: tx, TransactionIdList: atext, TransactionId: strtoa(atext)});
            }
        }
    }
    DequeueData(function () {
        say("Cluster Transaction Data saved", "Save");

        $("#gtransaction").jsGrid("search", fgrid);
        isUpdating = 0;
        $("#gtransaction").jsGrid("openPage", lastPageIndex);
    });
}

function getGridOptions(gridName) {
    var g = JSON.parse(JSON.stringify(defaultGridOption));
    for (var k in gridOptions[gridName]) {
        g[k] = gridOptions[gridName][k];
    }
    
    g.fields = fields[gridName];

    if (gridName=="transaction") {
        g.controller = {
            data: datas[gridName],
            loadData: function (filter) {
                return $.grep(this.data, function (item) {
                    // Check if all the fields are empty
                    var all_empty = true;
                    for (var field in filter) {
                        if (filter[field]) {
                            all_empty = false;
                        }
                    }

                    // If all the search fields are empty return all the rows
                    if (all_empty) {
                        return true;
                    }

                    // If some search field has content check if it matches the table entries
                    for (var field in filter) {

                        // Ignore the search in empty fields
                        if (!filter[field]) continue;


                        if (item[field] && item[field].toLowerCase().indexOf(filter[field].toLowerCase()) >= 0) {
                            return true;
                        }
                    }

                    return false;
                });
            }
        }
    } else 
        g.data = datas[gridName];
    return g;
}   
  

function GetGridHeight() {
    var divtop =  document.getElementById("topLine");
    var divfoot = document.getElementById("PageFooterDiv");
    var gHeight = window.height - (divtop.offsetHeight + divtop.offsetTop) - divfoot.offsetHeight - 100;
    return gHeight;
}


function say(what, title, level) {
    var noleSay = ["success", "info", "warning", "error", "none"];
    var nole = noleSay[1];
    if (level) {
        if ((level >= 0) && (level < 5))
            nole = noleSay[level];
    }
    var n = createNotification({
        closeOnClick: false,
        displayCloseButton: true,
        positionClass: "nfc-top-right",
        showDuration: 7000,
        theme: nole
    })({ title: title, message: what })

    if (level >= 3)
        errs.push(what);
}

function DequeueData(fn) {
    if (PutDataQueue.length < 1) {
        if (fn)
            fn();
    }    
    var o = PutDataQueue[0];
    if (o != undefined) {
        PutDataQueue.splice(0, 1);
        
        if (o.m != undefined && o.d != undefined)
            PutData(o.m, o.d, function (q) {
                
                DequeueData(fn);
            });
    }    
}

function PutData(m, d, onAk) {
    var rGetData = null;
    
    if (onAk == null || onAk == undefined) {
        var oQueue = new Object();
        oQueue.m = m;
        oQueue.d = d;
        PutDataQueue.push(oQueue);
    } else {
        $.ajax({
            type: "POST",
            url: '/ws.asmx/' + m,
            contentType: 'application/json; charset=utf-8',

            data: JSON.stringify(d),
            success: function (dx) {
                var q = dx.d;
                var dataOut = (q != null) ? JSON.parse(q) : null;
                if (onAk) {
                    onAk(q);
                }
                if (dataOut == null) {
                    say("error put " + method, "Err", 3);
                }
            },

            error: function (r, er) {
                say(r.responseText, "Error", 3);
            }
        });
    }
}

function GetData(method, param, out) {
    var rGetData = null;
    $.ajax({
        type: 'POST',
        url: '/ws.asmx/' + method,
        contentType: 'application/json; charset=utf-8',

        data: JSON.stringify(param),
        success: function (dx) {
            var q = dx.d;
            if (q != null) {
                var dataout = JSON.parse(q);
                for(var datai = 0; datai < dataout.length; datai++) {
                    out.push(dataout[datai]);
                }
                OnDataOk(method);
            } else {
                say("error get " + method, "Err", 3);
                out = null;
            }
        },

        error: function (r, er) {
            say(r.responseText, "Error", 3);
            out = null;
        }
    });
}

function OnDataOk(m) {
    dataOK.push(m);
    
    if (dataOK.indexOf('cluster') >= 0 
     && dataOK.indexOf('clusterStatus') >= 0) {
        var dataCluster = [];
        
        for(var di = 0; di < clusterData.length; di++) {
            var row = new Object();
            var cd = clusterData[di];
            row["id"] = cd.ClusterId;
            row["Name"] = cd.ClusterName;

            for (var i = 0; i < clusterStatus.length; i++) {
                var cs = clusterStatus[i];
                row["f" + cs.StatusCode]= (cd.Status.indexOf(parseInt(cs.StatusCode)) >= 0) ? 1: 0;                
            }
            dataCluster.push(row);            
        }
        datas['cluster'] = dataCluster;

        var fieldCluster =  
            [{ name: "id", type: "text", width: 20, validate: "required", align: "center" },
             { name: "Name", type: "text", width: 80, validate: "required" }];
        for (var i = 0; i < clusterStatus.length; i++) {
            var cs = clusterStatus[i];
            fieldCluster.push( {name: "f" + cs.StatusCode, type: "checkbox", title: cs.StatusName, width: 40 });            
        }
        fieldCluster.push({ name: "Act", type: "control", title: "Do", width: 30 });
        fields['cluster'] = fieldCluster;
     
    }
    
    if (dataOK.indexOf('transactionCluster')>=0)
    {
        datas['transaction'] = transactionData;
    }

    if (dataOK.indexOf('cluster') >= 0
     && dataOK.indexOf('clusterStatus') >= 0
     && dataOK.indexOf('transactionCluster') >= 0) {
        getClusterItems();
        setLookupField();
        $("#clusterButton").click();
    }
}

function setLookupField() {
    for (var i = 0; i < fields['transaction'].length; i++) {
        if (fields['transaction'][i].name == "ClusterId") {
            fields['transaction'][i]["items"] = clusterItems;
        }
    }        
}

/* begin hack for jsGrid  to autosave edit on other row click */
var baseEditItem = jsGrid.Grid.prototype.editItem;

jsGrid.Grid.prototype.editItem = function () {
    if (this._editingRow) {
        this.updateItem();
    }
    baseEditItem.apply(this, arguments);
};
/* end hack */
var qgrid = null;
$(function () {

    Init();

    $("#pageTitle").html($("#hidPageTitle").val());

    $(".tablinks").click(function (e) {
        e.preventDefault();
        var divshow = $(this).attr("data-show");

        $(".tab button").removeClass("active");
        $(this).addClass("active");
        if (divshow == 'transaction') {
            getClusterItems();
            setLookupField();

        } else {

        }

        $("#gwrapper div.tabcontent").hide();

        $("#gwrapper #tab" + divshow).show();

        qgrid = $("#g" + divshow).jsGrid(getGridOptions(divshow));
    });

    $("#SaveButton").click(function (e) {
        e.preventDefault();
        var tl = $(".tablinks.active");
        var tx = "";
        if (tl) {
            tx = tl.attr("data-show")
            if (tx && document.getElementById("#g" + tx))
                try {
                    $("#g" + tx).jsGrid("updateItem");
                } catch (error) {
                    console.log("err: ", error);
                }
            gridActions[tx]();
        }

    });

    $("#DownloadButton").click(function (e) {
        var tl = $(".tablinks.active");
        var tx = "";
        if (tl) {
            tx = tl.attr("data-show")
        }
        var cilo = {}
        getClusterItems();
        var tab_id = (tx=="transaction")? 1:0;
        
        if (tab_id)
            for (var cie = 0; cie < clusterItems.length; cie++) {
                if (clusterItems[cie].ClusterId) 
                    cilo[clusterItems[cie].ClusterId] = clusterItems[cie].ClusterName;
            }


        var filo = {}
        if (tab_id==0)
        for (var fie = 0; fie < fields['cluster'].length; fie++) {
            var fx = fields['cluster'][fie];
            filo[fx["name"]] = fx["title"];
        }

        if (tx) {
            var dt = $("#g" + tx).jsGrid("option", "data");
            
            var dtext = ""
            var ni = 0;
            var ci = 0;
            var dtlen = dt.length;

            for (var di = 0; di < dtlen; di++) {
                var t = dt[di];
                if (ni < 1) {
                    ci = 0;
                    for (var le in t) {
                        if (ci > 0)
                            dtext += ",";
                        if (tab_id==0 && filo[le]) {
                            dtext += filo[le];
                        } else {
                            dtext += le;
                        }
                        ci++;
                    }
                    dtext += "\r\n";
                }
                ci = 0
                for (var le in t) {
                    if (ci > 0)
                        dtext += ",";
                    if (le == "ClusterId" && tab_id) {
                        if (t[le] != null && cilo[t[le]])
                            dtext += cilo[t[le]];
                        else
                            dtext += "";
                    }
                    else {
                        dtext += t[le];
                    }
                    ci++;
                }
                dtext += "\r\n";
                ni++;
            }
            download(dtext, tx + ".csv", 'text/csv;encoding:utf-8');
        }
    });

    document.getElementById("CloseButton").onclick = function (e) {
        closeWin();
    }

    document.getElementById("baseForm").onsubmit = function (e) {
        e.preventDefault();
    };

    GetData("clusterStatus", { "id": null }, clusterStatus);
    GetData("cluster", { "id": null, "name": "", "text": "", "act": "0" }, clusterData);
    GetData("transactionCluster", { "id": null }, transactionData);
    GetData("clusterTransaction", { "id": null, "text": "", "act": "0" }, clusterTransaction);
});


