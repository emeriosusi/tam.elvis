<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <style type="text/css">
body { font-face: Calibri, Arial, sans-serif; font-size: 10pt; }
.comment { border:1px solid black; padding: 5px 5px 5px 5px;}
.bahasa { color:blue;}
.doc { padding-left:40px;}
.row { clear: both; display: block; }
.label{ float: left; display: inline-block; width: 50pt; }
.split{ float: left; display: inline-block; width: 10pt; padding: 0px 0px 5px 0px; }
.data{ float: left; display: inline-block; };
  </style>
  <title>poa</title>
</head>
<body>
[<span class="bahasa">Bahasa Indonesia</span>]<br>
<br>
Kepada Bapak/Ibu @Name,<br>
<br>
Diinformasikan bahwa <br>
<div class="row">&nbsp;</div>
<table style="text-align: left; width: 100%;" border="0"
 cellpadding="2" cellspacing="0">
  <tbody>
    <tr>
      <td style="width: 200px;">Nama</td>
      <td style="width: 30px;">:</td>
      <td><b>@grantorName</b></td>
    </tr>
    <tr>
      <td style="width: 50px;">Jabatan</td>
      <td>:</td>
      <td>@grantorPos</td>
    </tr>
  </tbody>
</table>
<br>
<div class="row"> &nbsp; </div>
(sebagai pemberi kuasa), memberikan kuasa kepada : <br>
<br>
<table style="text-align: left; width: 100%;" border="0"
 cellpadding="2" cellspacing="0">
  <tbody>
    <tr>
      <td style="width: 200px;">Nama</td>
      <td style="width: 30px;">:</td>
      <td><b>@attorneyName</b></td>
    </tr>
    <tr>
      <td style="width: 50px;">Jabatan</td>
      <td>:</td>
      <td>@attorneyPos</td>
    </tr>
  </tbody>
</table>
<div class="row">
(sebagai penerima kuasa) <br>
<br>
untuk menjalankan peran pada sistem Electronic Voucher Integrated
System (ELVIS) yang mulai berlaku mulai tanggal <b>@validFrom</b>
hingga <b>@validTo</b>.<br>
<br>
Salam, <br>
<br>
ELVIS Administrator<br>
<br>
<hr>
<br>
[<span class="bahasa">English</span>]<br>
<br>
Dear Mr. / Mrs. @NameEN,<br>
<br>
It is informed that <br>
<div class="row">&nbsp;</div>
<table style="text-align: left; width: 100%;" border="0"
 cellpadding="2" cellspacing="0">
  <tbody>
    <tr>
      <td style="width: 200px;">Name</td>
      <td style="width: 30px;">:</td>
      <td><b>@grantorNameEN</b></td>
    </tr>
    <tr>
      <td style="width: 50px;">Title</td>
      <td>:</td>
      <td>@grantorPosEN</td>
    </tr>
  </tbody>
</table>
<div class="row"> &nbsp; </div>
(hereinafter reffered to as the "Grantor of Attorney"), hereby confers
power of attorney to : <br>
<br>
<table style="text-align: left; width: 100%;" border="0"
 cellpadding="2" cellspacing="0">
  <tbody>
    <tr>
      <td style="width: 200px;">Name</td>
      <td style="width: 30px;">:</td>
      <td><b>@attorneyNameEN</b></td>
    </tr>
    <tr>
      <td style="width: 50px;">Title</td>
      <td>:</td>
      <td>@attorneyPosEN</td>
    </tr>
  </tbody>
</table>
<div class="row"> &nbsp; </div>
(hereinafter reffered to as "Attorney") <br>
<br>
To represent and act on Electronic Voucher Integrated System (ELVIS) on
behalf of the Grantor of Attorney which is started from <b>@validFromEN</b>
to <b>@validToEN</b><br>
<br>
Best Regards, <br>
<br>
ELVIS Administrator<br>
<br>
</div>
</body>
</html>
