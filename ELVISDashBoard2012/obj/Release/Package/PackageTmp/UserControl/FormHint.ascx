﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormHint.ascx.cs" Inherits="ELVISDashBoard.UserControl.FormHint" %>
<div id="pnGridUsageHint" class="pvform-grid-usage-hint-panel">
    <div id="title">
        Hint</div>
    <div id="content">
        <p>
            To add a new row on the grid, do following actions:</p>
        <ul>
            <li>While on Value column, press 'Tab'</li>
            <li>Click green plus sign "+" on the left most side of the editing row.</li>
        </ul>
    </div>
</div>
