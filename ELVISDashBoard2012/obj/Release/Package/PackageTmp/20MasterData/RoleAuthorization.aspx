﻿<%@ Page Title="Role Authorization" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="RoleAuthorization.aspx.cs" Inherits="ELVISDashBoard._20MasterData.RoleAuthorization" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xPre" ContentPlaceHolderID="pre" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>
    <style type="text/css">
        .gridTest
        {
            width: 970px;
            background-color: #FFFFCC;
            border: 5px solid yellow;
            margin: 0px 0px 0px 0px;
            padding: 5px 5px 5px 5px;
        }
        .dwrap
        {
            float: left;
            height: 30px;
            vertical-align: middle;
            padding: 0px 10px 0px 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Role Authorization Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <div class="row">
                    <label>
                        Role</label>
                    <div class="rowwarphalfLeft">
                        <dx:ASPxComboBox runat="server" ID="ddlRole" />
                    </div>
                    <label>
                        Screen</label>
                    <div class="rowwarphalfLeft">
                        <dx:ASPxComboBox runat="server" ID="ddlScreen" />
                    </div>
                </div>
                <div class="row">
                    <label>
                        Object
                    </label>
                    <div class="rowwarphalfLeft">
                        <asp:TextBox runat="server" ID="txtObject" Columns="30" MaxLength="200" />
                    </div>
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" OnClientClick="loading()" />
                    </div>
                </div>
            </div>
            <div class="rowbtn">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click"
                        OnClientClick="loading()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                ClientInstanceName="gridGeneralInfo" KeyFieldName="ROLE_ID" DataSourceID="dsRoles"
                
                OnCommandButtonInitialize="grid_CommandButtonInit"
                OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated" OnCustomCallback="grid_CustomCallback"
                OnDetailRowExpandedChanged="grid_ExpandedChanged" OnPageIndexChanged="gridGeneralInfo_PageIndexChanged">
                <Settings ShowGroupButtons="false" ShowGroupedColumns="false" ShowGroupPanel="false"
                    ShowStatusBar="Hidden" ShowVerticalScrollBar="true" VerticalScrollableHeight="400" />
                <SettingsEditing Mode="Inline" />
                <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="false" Visible="false" />
                <SettingsCustomizationWindow Enabled="false" />
                <SettingsBehavior AllowDragDrop="false" AllowGroup="false" />
                <SettingsDetail ShowDetailRow="true" ShowDetailButtons="true" AllowOnlyOneMasterRowExpanded="true" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                    <AlternatingRow CssClass="even" />
                </Styles>
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px">
                        <EditButton Visible="false" Text=" ">
                        </EditButton>
                        <UpdateButton Visible="false" Text=" ">
                        </UpdateButton>
                        <NewButton Visible="false" Text=" ">
                        </NewButton>
                        <CancelButton Visible="false" Text=" ">
                        </CancelButton>
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="gridGeneralInfoSelectAllCheckBox" runat="server" 
                                ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); }"
                                CheckState="Unchecked" Style="text-align: center">
                            </dx:ASPxCheckBox>
                        </HeaderTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ROLE_ID" VisibleIndex="2" Width="170px"
                        CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridRoleID" Width="168px" MaxLength="20" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Role Name" FieldName="ROLE_NAME" Width="270px"
                        VisibleIndex="3" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridRolEnAME" Width="268px" MaxLength="200" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Created" VisibleIndex="8">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CREATED_DT" VisibleIndex="0"
                                Width="110px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy}" />
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1" Width="100px">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Changed" VisibleIndex="9">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CHANGED_DT" VisibleIndex="0"
                                Width="110px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy}" />
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CHANGED_BY" VisibleIndex="1" Width="100px">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                </Columns>
                <Templates>
                    <DetailRow>
                        <dx:ASPxGridView ID="Screen" runat="server" DataSourceID="dsScreen" ClientInstanceName="Screen"
                            Width="500px" KeyFieldName="ScreenKey" AutoGenerateColumns="false" 
                            OnCommandButtonInitialize="grid_CommandButtonInit"
                            OnCustomUnboundColumnData="Screen_CustomUnbound"
                            OnDetailRowExpandedChanged="Screen_ExpandedChanged" OnBeforePerformDataSelect="Screen_DataSelect">
                            <Settings ShowGroupPanel="false" />
                            <SettingsBehavior AllowSort="true" ColumnResizeMode="Control" />
                            <SettingsPager Mode="ShowAllRecords" Visible="false" />
                            <SettingsDetail ShowDetailRow="true" ShowDetailButtons="true" AllowOnlyOneMasterRowExpanded="true" />
                            <SettingsCustomizationWindow Enabled="false" />
                            <Templates>
                                <DetailRow>
                                    <dx:ASPxGridView ID="RoleDetail" runat="server" DataSourceID="dsRoleDetail" ClientInstanceName="RoleDetail"
                                        Width="220px" AutoGenerateColumns="false" KeyFieldName="RoleDetailKey" 
                                        OnCommandButtonInitialize="grid_CommandButtonInit"
                                        OnCustomUnboundColumnData="RoleDetail_CustomUnbound"
                                        OnBeforePerformDataSelect="RoleDetail_DataSelect">
                                        <SettingsPager Mode="ShowAllRecords" Visible="false" />
                                        <SettingsCustomizationWindow Enabled="false" />
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px">
                                                <EditButton Visible="false" Text=" ">
                                                </EditButton>
                                                <UpdateButton Visible="false" Text=" ">
                                                </UpdateButton>
                                                <NewButton Visible="false" Text=" ">
                                                </NewButton>
                                                <CancelButton Visible="false" Text=" ">
                                                </CancelButton>
                                                <HeaderTemplate>
                                                    <dx:ASPxCheckBox ID="RoleDetailSelectAllCheckBox" runat="server" 
                                                        ClientSideEvents-CheckedChanged="function(s, e) { RoleDetail.SelectAllRowsOnPage(s.GetChecked()); }"
                                                        CheckState="Unchecked" Style="text-align: center">
                                                    </dx:ASPxCheckBox>
                                                </HeaderTemplate>
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataColumn FieldName="RoleDetailKey" Visible="false" UnboundType="string" />
                                            <dx:GridViewDataColumn FieldName="OBJECT_ID" Caption="Object" Width="200px">
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                    <div class="row">
                                        <div class="dwrap">
                                            <asp:Button ID="NewRoleDetail" runat="server" Text="New" OnClick="NewRoleDetail_Click" />
                                        </div>
                                        <div class="dwrap">
                                            <dx:ASPxGridLookup runat="server" ID="ddlObject" ClientInstanceName="ddlObject" SelectionMode="Multiple"
                                                KeyFieldName="OBJECT_ID" ClientIDMode="Static" Width="200px" IncrementalFilteringMode="Contains"
                                                IncrementalFilteringDelay="500" DataSourceID="dsScreenObject">
                                                <GridViewProperties EnableCallBacks="false">
                                                    <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                    <Settings ShowFilterRow="false" ShowStatusBar="Auto" UseFixedTableLayout="true" ShowColumnHeaders="false" />
                                                    <SettingsPager PageSize="10" />
                                                </GridViewProperties>
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Object" FieldName="OBJECT_ID" Visible="true"
                                                        Width="200px">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                            </dx:ASPxGridLookup>
                                        </div>
                                        <div class="dwrap">
                                            <asp:Button ID="AddRoleDetail" runat="server" Text="Add" OnClick="AddRoleDetail_Click" />
                                            <asp:Button ID="DelRoleDetail" runat="server" Text="Delete" OnClick="DelRoleDetail_Click" />
                                        </div>
                                    </div>
                                </DetailRow>
                            </Templates>
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                    CellStyle-HorizontalAlign="Center">
                                    <EditButton Visible="false" Text=" ">
                                    </EditButton>
                                    <UpdateButton Visible="false" Text=" ">
                                    </UpdateButton>
                                    <NewButton Visible="false" Text=" ">
                                    </NewButton>
                                    <CancelButton Visible="false" Text=" ">
                                    </CancelButton>
                                    <HeaderTemplate>
                                        <dx:ASPxCheckBox ID="ScreenSelectAllCheckBox" runat="server" 
                                            ClientSideEvents-CheckedChanged="function(s, e) { Screen.SelectAllRowsOnPage(s.GetChecked()); }"
                                            CheckState="Unchecked" Style="text-align: center">
                                        </dx:ASPxCheckBox>
                                    </HeaderTemplate>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataColumn FieldName="ScreenKey" Visible="false" UnboundType="string" />
                                <dx:GridViewDataColumn FieldName="SCREEN_ID" Caption="Screen ID" Width="220px" />
                                <dx:GridViewDataColumn FieldName="CAPTION" Caption="Caption" Width="280px" />
                            </Columns>
                        </dx:ASPxGridView>
                        <div class="row">
                            <div class="dwrap">
                                <asp:Button ID="NewScreen" runat="server" Text="New" OnClick="NewScreen_Click" />
                            </div>
                            <div class="dwrap">
                                <dx:ASPxGridLookup runat="server" ID="ddlMenu" ClientInstanceName="ddlMenu" SelectionMode="Single"
                                    KeyFieldName="ScreenID" TextFormatString="{0}" ClientIDMode="Static" Width="300px"
                                    IncrementalFilteringMode="Contains" OnValueChanged="ddlMenu_ValueChanged" IncrementalFilteringDelay="1000"
                                    DataSourceID="dsMenus">
                                    <GridViewProperties EnableCallBacks="false">
                                        <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                        <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" ShowColumnHeaders="false" />
                                        <SettingsPager PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Screen Id" FieldName="ScreenID" Visible="true"
                                            Width="200px">
                                            <Settings AutoFilterCondition="Contains" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Caption" FieldName="MenuCaption" Width="200px">
                                            <Settings AutoFilterCondition="Contains" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:ASPxGridLookup>
                            </div>
                            <div class="dwrap">
                                <asp:Button ID="AddScreen" runat="server" Text="Add" OnClick="AddScreen_Click" />
                                <asp:Button ID="DelScreen" runat="server" Text="Delete" OnClick="DelScreen_Click" />
                            </div>
                        </div>
                    </DetailRow>
                </Templates>
            </dx:ASPxGridView>
            <asp:LinqDataSource ID="dsRoles" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="" OnSelecting="dsRoles_Selecting" TableName="TB_M_SYSTEM">
            </asp:LinqDataSource>
            <asp:LinqDataSource ID="dsScreen" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new(ROLE_ID,SCREEN_ID,CAPTION,object_count)" TableName="vw_RoleScreen"
                OnSelecting="dsScreen_Selecting" AutoGenerateWhereClause="True">
            </asp:LinqDataSource>
            <asp:LinqDataSource ID="dsRoleDetail" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new(ROLE_ID,SCREEN_ID,OBJECT_ID)" OnSelecting="dsRoleDetail_Selecting"
                TableName="vw_RoleDetail" />
            <asp:LinqDataSource ID="dsMenus" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new(MenuID,LevelID,Position,ScreenID,MenuCaption,ParentID,Path)" TableName="vw_Menu_Level"
                OrderBy="LevelID" />
            <asp:LinqDataSource ID="dsScreenObject" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new(OBJECT_ID)" OnSelecting="dsScreenObject_Selecting"
                TableName="vw_ScreenObject" />
            <div class="rowbtn">
                <div class="btnleft">
                    <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                        SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                </div>
                <div class="btnright">
                    <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click"
                        OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click"
                        OnClientClick="loading()" />&nbsp;
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnCancelConfirmationDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
    <!-- Panel for adding New data -->
    <asp:UpdatePanel ID="upAddNewObject" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CancelSaveNew" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidNewData" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="popNew" runat="server" TargetControlID="hidNewData" PopupControlID="AddNewPanel"
                CancelControlID="CancelSaveNew" BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="AddNewPanel" runat="server" CssClass="informationPopUpLong">
                    <center>
                        <div id="ObjectAddDiv" runat="server">
                            <div class="row">
                                <label>
                                    New Object</label>
                                <div class="rowwarphalfLeft">
                                    <asp:TextBox ID="NewObject" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div id="ScreenAddDiv" runat="server">
                            <div class="row">
                                New Screen
                            </div>
                            <div class="row">
                                <label>
                                    ScreenID</label>
                                <div class="rowwarphalfLeft">
                                    <asp:TextBox ID="NewScreenID" runat="server" Columns="20" MaxLength="200" /></div>
                            </div>
                            <div class="row">
                                <label>
                                    Caption</label>
                                <div class="rowwarphalfLeft">
                                    <asp:TextBox ID="NewCaption" runat="server" Columns="50" MaxLength="200" /></div>
                            </div>
                            <div class="row">
                                <label>
                                    Path</label>
                                <div class="rowwarphalfLeft">
                                    <asp:TextBox ID="NewPath" runat="server" Columns="50" MaxLength="200" /></div>
                            </div>
                            <div class="row">
                                <label>
                                    MenuID</label>
                                <div class="rowwarphalfLeft">
                                    <asp:TextBox ID="NewMenuID" runat="server" Columns="5" MaxLength="10" /></div>
                            </div>
                            <div class="row">
                                <label>
                                    ParentID</label>
                                <div class="rowwarphalfLeft">
                                    <dx:ASPxGridLookup runat="server" ID="NewParentIDLookup" ClientInstanceName="ddlParent" SelectionMode="Single"
                                        KeyFieldName="MenuID" TextFormatString="{0}" ClientIDMode="Static" Width="80px"
                                        DataSourceID="dsMenus">
                                        <GridViewProperties EnableCallBacks="false">
                                            <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                            <Settings ShowFilterRow="false" ShowStatusBar="Auto" UseFixedTableLayout="true" ShowColumnHeaders="false" />
                                            <SettingsPager PageSize="12" />
                                        </GridViewProperties>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="MenuId" FieldName="MenuID" Visible="true" Width="50px">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Screen Id" FieldName="ScreenID" Visible="true" Width="150px">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Caption" FieldName="MenuCaption" Visible="true" Width="200px">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Parent" FieldName="ParentID" Visible="true" Width="50px">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Position" FieldName="Position" Visible="true" Width="50px">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridLookup>        
                                </div>                            
                            </div>
                        </div>
                    </center>
                    <div class="btnRightLong">
                        <asp:Button ID="OkSaveNew" Text="OK" runat="server" OnClick="OkSaveNew_Click" OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="CancelSaveNew" Text="Cancel" runat="server" />
                    </div>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- End of panel for adding New data -->
</asp:Content>

