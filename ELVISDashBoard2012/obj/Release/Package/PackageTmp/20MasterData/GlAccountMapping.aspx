﻿<%@ Page Title="GL Account Mapping" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="GlAccountMapping.aspx.cs" Inherits="ELVISDashBoard._20MasterData.GlAccountMapping" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="GL Account Mapping Master Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <div class="row">
                    <label>
                        Transaction Code</label>
                    <div class="rowwarphalfLeft">
                        <asp:DropDownList runat="server" ID="ddlTransaction" AutoPostBack="false"/>                        
                    </div>
                    <label>
                        Item Transaction</label>
                    <div class="rowwarphalfLeft">
                        <asp:DropDownList runat="server" ID="ddlItemTransaction" AutoPostBack="false"/>                        
                    </div>
                </div>
                <div class="row">
                    <label>
                        GL Account
                    </label>
                    <div class="rowwarphalfLeft">
                        <asp:TextBox runat="server" ID="txtGlAccount" Columns="10" MaxLength="10"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="MaskTxtGLAccount" runat="server" TargetControlID="txtGlAccount"
                            Mask="9999999999" AutoComplete="false" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus"
                            OnInvalidCssClass="MaskedEditError" MaskType="Number" DisplayMoney="None" AcceptNegative="None"
                            CultureName="id-ID" ErrorTooltipEnabled="True" PromptCharacter=" " />
                    </div>
                </div>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" OnClientClick="loading()" />
                    </div>
                </div>
            </div>
            <div class="rowbtn">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click"
                        OnClientClick="loading()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                KeyFieldName="TRANSACTION_ITEM" OnCustomCallback="grid_CustomCallback" EnableCallBacks="false"
                OnCustomUnboundColumnData="grid_CustomUnbound" OnPageIndexChanged="gridGeneralInfo_PageIndexChanged"
                Styles-AlternatingRow-CssClass="even">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px">
                        <EditButton Visible="false" Text=" ">
                        </EditButton>
                        <UpdateButton Visible="false" Text=" ">
                        </UpdateButton>
                        <NewButton Visible="false" Text=" ">
                        </NewButton>
                        <CancelButton Visible="false" Text=" ">
                        </CancelButton>
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); }"
                                CheckState="Unchecked" Style="text-align: center">
                            </dx:ASPxCheckBox>
                        </HeaderTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn FieldName="TRANSACTION_ITEM" Visible="false" UnboundType="String">
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="CODE" Visible="false" UnboundType="String">
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="ITEM_TRANSACTION_CD" Visible="false" UnboundType="String">
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="NEW_ITEM_TRANSACTION_CD" Visible="false" UnboundType="String">
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn FieldName="SUM_UP_FLAG" Visible="false" UnboundType="Integer">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" Width="30px">
                        <DataItemTemplate>
                            <asp:Literal runat="server" ID="litGridNo"></asp:Literal>
                        </DataItemTemplate>
                        <EditItemTemplate>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Transaction Code" FieldName="TRANSACTION_NAME"
                        VisibleIndex="2" Width="160px">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridTransactionCode" Width="158px" MaxLength="10">
                            </dx:ASPxComboBox>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Item Transaction" FieldName="ITEM_TRANSACTION_DESC"
                        Width="180px" VisibleIndex="3" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridItemTransaction" Width="172px" MaxLength="10" />
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="GL Account" FieldName="GL_ACCOUNT" Width="100px"
                        VisibleIndex="4" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtGridGlAccount" Width="92px" MaxLength="10" onchange="extractNumber (this, 0, false);"
                                onblur="extractNumber (this, 0, false);" onkeyup="extractNumber (this, 0, false);"
                                onkeypress="return blockNonNumbers (this, event, false, false);" />
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="New Item Transaction" FieldName="NEW_ITEM_TRANSACTION_DESC"
                        Width="180px" VisibleIndex="5" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridNewItemTransaction" Width="172px" MaxLength="10" />
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="New Formula" FieldName="NEW_FORMULA" Width="100px"
                        VisibleIndex="6" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtGridNewFormula" Width="92px" MaxLength="6" onchange="extractNumber (this, 4, false);"
                                onblur="extractNumber (this, 4, false);" onkeyup="extractNumber (this, 4, false);"
                                onkeypress="return blockNonNumbers (this, event, true, false);" />
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Sum Up Flag" FieldName="SUM_UP_FLAG_NAME" VisibleIndex="7"
                        Width="120px">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridSumUpFlag" Width="118px" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Created" VisibleIndex="8">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CREATED_DT" VisibleIndex="0" Width="135px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}"/>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1" Width="100px">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    <dx:GridViewBandColumn Caption="Changed" VisibleIndex="9">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CHANGED_DT" VisibleIndex="0" Width="135px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}"/>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CHANGED_BY" VisibleIndex="1" Width="100px">
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                </Columns>
                <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                <SettingsEditing Mode="Inline" />
                <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                <SettingsLoadingPanel ImagePosition="Top" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
                <Settings ShowStatusBar="Visible" />
                <SettingsPager Visible="false" />
                <Templates>
                    <StatusBar>
                        <div style="text-align: left;">
                            Records per page:
                            <select onchange="gridGeneralInfo.PerformCallback(this.value);">
                                <option value="5" <%# WriteSelectedIndex(5) %>>5</option>
                                <option value="10" <%# WriteSelectedIndex(10) %>>10</option>
                                <option value="15" <%# WriteSelectedIndex(15) %>>15</option>
                                <option value="20" <%# WriteSelectedIndex(20) %>>20</option>
                                <option value="25" <%# WriteSelectedIndex(25) %>>25</option>
                            </select>&nbsp;
                            <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">
                                &lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a>
                            &nbsp; Page
                            <input type="text" onchange="gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                style="width: 20px" />
                            of
                            <%# gridGeneralInfo.PageCount%>&nbsp;
                            <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%> 
                            <a title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp;
                            <a title="Last" href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">
                                &gt;&gt;</a> &nbsp;
                        </div>
                    </StatusBar>
                </Templates>
                <SettingsPager AlwaysShowPager="true">
                </SettingsPager>
            </dx:ASPxGridView>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new (TRANSACTION_CD, TRANSACTION_NAME, ITEM_TRANSACTION_CD, ITEM_TRANSACTION_DESC, GL_ACCOUNT, NEW_ITEM_TRANSACTION_CD, NEW_ITEM_TRANSACTION_DESC, NEW_FORMULA, SUM_UP_FLAG, SUM_UP_FLAG_NAME, CREATED_DT, CREATED_BY, CHANGED_DT, CHANGED_BY)"
                OnSelecting="LinqDataSource1_Selecting" TableName="vw_GlAccountMapping">
            </asp:LinqDataSource>
            <div class="rowbtn">
                <div class="btnleft">
                    <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                        SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                </div>
                <div class="btnright">
                    <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click"
                        OnClientClick="loading()" />
                    &nbsp;
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click"
                        OnClientClick="loading()" />&nbsp;
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
</asp:Content>
