-- getDivisions
SELECT 
    d.DIVISION_ID AS [DivisionID]
    , d.DIVISION_NAME AS [DivisionName]
    , ISNULL(d.PRODUCTION_FLAG, 0) AS [ProductionFlag]
FROM vw_Division D
