-- GetUserDivisions
DECLARE @@divs VARCHAR(800) = ISNULL(@p1 ,'');

SELECT 
    v.DIVISION_ID   [Code]
  , v.DIVISION_NAME [Description]
FROM (SELECT 1 A, CASE WHEN @@divs='' THEN 1 ELSE 0 END GET_ALL ) X 
LEFT JOIN (
	SELECT DISTINCT
		ROW_NUMBER() OVER(ORDER BY (SELECT 1)) SEQ,  items DIVISION_ID
	FROM dbo.fn_explode(',', @@divs) F
) D ON 1=1
JOIN vw_division v 
ON (X.GET_ALL=1 OR v.DIVISION_ID = d.DIVISION_ID)