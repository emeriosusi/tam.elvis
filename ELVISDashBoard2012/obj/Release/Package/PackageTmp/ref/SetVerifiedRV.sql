-- SetVerifiedRV
declare
    /*@docNo INT = 123456 , @docYear INT = 2018 , @username VARCHAR(50)='sabid.ismulani', */
    @@now DATETIME = DATEADD(DAY, 0, DATEDIFF(DAY,0, GETDATE())) ;

UPDATE TB_R_RV_H
SET SUBMIT_HC_DOC_DATE = @@now,
    COUNTER_FLAG = 1,
    CHANGED_BY = @username, CHANGED_DATE = GETDATE(),
    HOLD_FLAG = (
        SELECT CASE WHEN
            ('ELVIS_COUNTER' IN (
            SELECT r.ROLE_ID
            FROM (SELECT @username USERNAME, 'ELVIS' APPID) x
            JOIN TB_M_USER mu ON mu.USERNAME = x.USERNAME
            LEFT JOIN TMMIN_ROLE.dbo.TB_M_USER u on u.USERNAME = x.USERNAME
            LEFT JOIN TMMIN_ROLE.dbo.TB_M_ROLE_HEADER r on r.USERNAME = x.USERNAME  
				and r.APPLICATION_ID = x.APPID
            GROUP BY r.ROLE_ID
            ))
        THEN 0
        ELSE HOLD_FLAG END
    )
WHERE RV_NO = @docNo and RV_YEAR = @docYear;

SELECT @@ROWCOUNT R;