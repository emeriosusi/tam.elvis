-- GetUnsettledByDivision

 -- declare @p1 int = NULL
 SELECT x.DIVISION_ID, x.DOC_NO, x.DOC_YEAR, x.VENDOR_CD, x.VENDOR_NAME, x.DAYS_OUTSTANDING, x.TOTAL_AMOUNT, x.STATUS_CD, x.PV_DATE	
 FROM 
 (
SELECT 
  DIV.DIVISION_ID, h.PV_NO DOC_NO, h.PV_YEAR DOC_YEAR 
, MAX(h.VENDOR_CD) [VENDOR_CD], MAX(v.VENDOR_NAME)[VENDOR_NAME]
, DATEDIFF(day, h.ACTIVITY_DATE_TO, GETDATE()) [DAYS_OUTSTANDING]
, SUM(h.TOTAL_AMOUNT) [TOTAL_AMOUNT]
, h.status_cd, h.PV_DATE
FROM (select NULLIF(@p1,0) DIV_CD) D
JOIN TB_R_PV_H h ON (d.DIV_CD IS NULL OR h.DIVISION_ID = d.DIV_CD)
JOIN TMMIN_ROLE.dbo.TB_M_DIVISION div ON div.DIVISION_ID = H.DIVISION_ID AND (D.DIV_CD IS NULL OR DIV.DIVISION_ID= D.DIV_CD)
LEFT JOIN (select 99 STATUS_CD) exs on exs.STATUS_CD = h.STATUS_CD
LEFT JOIN SAP_DB.dbo.TB_M_VENDOR v  ON v.VENDOR_CD = h.VENDOR_CD
LEFT JOIN TB_R_PV_H hs on hs.SUSPENSE_NO = h.PV_NO  AND HS.SUSPENSE_YEAR = h.PV_YEAR
LEFT JOIN TB_R_RV_H rs on rs.SUSPENSE_NO = h.PV_NO and rs.SUSPENSE_YEAR = h.PV_YEAR
WHERE ISNULL(h.CANCEL_FLAG, 0) = 0       -- not canceled
  AND ISNULL(h.DELETED, 0) = 0           -- not deleted
  AND h.PV_TYPE_CD = 2
  AND NOT (
	     (hs.SUBMIT_HC_DOC_DATE IS NOT NULL AND hs.COUNTER_FLAG = 1
	      AND ISNULL(hs.CANCEL_FLAG,0) = 0 AND ISNULL(hs.DELETED, 0) = 0)
	OR   (rs.SUBMIT_HC_DOC_DATE IS NOT NULL AND rs.COUNTER_FLAG = 1
	      AND ISNULL(rs.CANCEL_FLAG,0) = 0 AND ISNULL(rs.DELETED, 0) = 0)
  ) 
  AND (dbo.fn_GetSettlementStatus(dbo.fn_DocNoYearToReff(h.PV_NO, h.PV_YEAR)) < 2) 
  AND exs.STATUS_CD IS NULL 
  AND (CONVERT(DATE, GETDATE()) >= h.ACTIVITY_DATE_TO)   
  AND (SELECT TOP 1 ACTUAL_DT
    FROM TB_R_DISTRIBUTION_STATUS
    WHERE REFF_NO = dbo.fn_DocNoYearToReff(h.PV_NO, h.PV_YEAR)
    ORDER BY STATUS_CD DESC) IS NOT NULL  -- final approved	
GROUP BY div.DIVISION_ID, h.PV_NO, h.PV_YEAR, h.ACTIVITY_DATE_TO, h.STATUS_CD, h.PV_DATE
) [x]