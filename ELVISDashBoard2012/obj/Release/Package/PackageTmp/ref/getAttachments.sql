-- getAttachments
select REFERENCE_NO AS [ReferenceNumber], REF_SEQ_NO AS [SequenceNumber]
	 , [DESCRIPTION] AS [Description]
	 , DIRECTORY AS [PATH], FILE_NAME AS [FileName]
	 , ATTACH_CD AS [CategoryCode]
  from TB_R_ATTACHMENT
 where REFERENCE_NO = @p1
