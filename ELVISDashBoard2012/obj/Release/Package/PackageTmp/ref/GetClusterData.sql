-- GetClusterData
DECLARE @@id INT = @id, @@name varchar(50) = NULLIF(@name,'')

SELECT [ClusterId]=SYSTEM_VALUE_NUM  , [ClusterName]=SYSTEM_CD, [StatusCodeList] =SYSTEM_VALUE_TXT
FROM TB_M_SYSTEM 
WHERE SYSTEM_TYPE = 'CLUSTER' 
AND (@@id IS NULL OR SYSTEM_VALUE_NUM = @@id)
AND (@@name IS NULL OR SYSTEM_CD = @@name )