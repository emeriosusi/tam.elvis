-- GetSuspenseCountByDiv
SELECT sum(isnull(V.suspense_count, 0)) SUS_COUNT 
FROM VW_VENDOR V 
WHERE ISNULL(V.CONTACT_PERSON, '') = CONVERT(VARCHAR, 51);
