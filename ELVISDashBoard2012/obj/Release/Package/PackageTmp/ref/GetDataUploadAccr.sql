﻿-- GetLastUpload
SELECT
   PROCESS_ID as [Pid],
   SEQ_NO as [SequenceNumber] ,
   ACCRUED_NO as [AccruedNo],
   ACTIVITY_DES as [ActivityDescription],
   WBS_NO_OLD as [WbsNumber],
   PV_TYPE_CD as [PvTypeCd],
   PV_TYPE_NAME  as [PvTypeCd],
   COST_CENTER as [InvoiceDate],
   CURRENCY_CD 	as [ItemTransaction],
   AMOUNT as [TaxNumber],
   AMOUNT_IDR as [TaxCode],
   SUSPENSE_NO_OLD  as [TaxDate], 
   BOOKING_NO  as [GlAccount], 
   CREATED_DT  as [ItemNo],
   CREATED_BY  as [DppAmount],
   TAX_ASSIGNMENT as [TaxAssignment],
   ACC_INF_ASSIGNMENT as [AccInfAssignment]   
FROM dbo.TB_T_PV_D
WHERE PROCESS_ID = @pid
