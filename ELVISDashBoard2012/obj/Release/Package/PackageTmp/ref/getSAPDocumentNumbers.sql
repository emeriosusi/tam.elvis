--- getSAPDocumentNumbers
SELECT 
	ClearingDocNumber = t.SAP_CLEARING_DOC_NO,
	InvoiceNumber = t.INVOICE_NO,
	SAPDocNumber = t.SAP_DOC_NO,
	CurrencyCode = t.CURRENCY_CD,
	PayPropDocNo = t.PAYPROP_DOC_NO,
	PayPropId = t.PAYPROP_ID
FROM TB_R_SAP_DOC_NO t
WHERE t.DOC_NO = @docNo AND t.DOC_YEAR = @docYear 
