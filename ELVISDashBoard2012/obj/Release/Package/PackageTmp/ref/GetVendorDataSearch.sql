declare 
	  @@vendorCd varchar(10) = @code
	, @@group int = @group
	, @@search VARCHAR(10) = @terms
	, @@name varchar(40) = @name
	, @@div varchar(10) = @div
	, @@scfrom varchar(20) = @from
	, @@scto Varchar(20) = @to
	, @@likeS int = 0
	, @@likeC int = 0
	, @@likeN int = 0
;
		
set @@vendorCd = REPLACE(@@vendorCd, '*', '%');
set @@search= REPLACE(@@search, '*', '%');
set @@name= REPLACE(@@name, '*', '%');
IF CHARINDEX('%', @@vendorCd) > 0 
BEGIN
	set @@likeC = 1;
END; 

IF CHARINDEX('%', @@search) > 0 
BEGIN
	set @@likeS = 1;
END; 

IF CHARINDEX('%', @@name) > 0 
BEGIN
	set @@likeN = 1;
END; 


SELECT 
	  v.vendor_cd
	, v.vendor_name
	, v.vendor_group_cd
	, g.vendor_group_name
	, v.search_terms
	, v.contact_person
	, v.division_id
	, d.division_name
	, v.created_by
	, v.created_dt
	, v.suspense_count
FROM vw_Vendor v
  LEFT JOIN tb_m_vendor_group g ON g.vendor_group_cd = v.vendor_group_cd
  LEFT JOIN vw_Division d ON d.division_id = v.division_id
WHERE (NULLIF(LTRIM(RTRIM(@@vendorCd)),'') IS NULL 
	   OR (@@likeC = 0 AND v.VENDOR_CD = @@vendorCd )
	   OR (@@likeC = 1 AND v.VENDOR_CD LIKE @@vendorCd )
	    )
	    
  AND (NULLIF(LTRIM(RTRIM(@@name)),'') IS NULL 
       OR (@@likeN = 0 AND v.VENDOR_NAME = @@name )
       OR (@@likeN = 1 AND v.VENDOR_NAME LIKE @@name )
       )
       
  AND (NULLIF(LTRIM(RTRIM(@@search)),'') IS NULL 
       OR (@@likeS = 0 AND v.SEARCH_TERMS = @@search )
       OR (@@likeS = 1 AND v.SEARCH_TERMS LIKE @@search )
       )

  AND (NULLIF(@@group,0) IS NULL 
       OR (v.VENDOR_GROUP_CD = @@group )
       )
  AND (NULLIF(LTRIM(RTRIM(@@div)),'') IS NULL 
       OR v.DIVISION_ID = @@div)
  AND (   @@scfrom IS NULL OR ISNUMERIC(@@scfrom) <> 1 
	   or @@scto IS NULL OR ISNUMERIC(@@scto) <> 1 
	   OR (SUSPENSE_COUNT BETWEEN convert(int,@@scfrom) AND convert(int, @@scto))
	)   