<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Login.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="ELVISDashBoard.LoginPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="App_Themes/BMS_Theme/Script/ParentChild.js" type="text/javascript"></script>
    <script type="text/javascript">
        function closeLogin() {
            window.open("","_self");
            window.close();
        }

        function clearLogin() {
            uid = document.getElementById("txtUserID");
            uid.value = "";
            
            pwd = document.getElementById("txtPassword");
            pwd.value = "";
            uid.focus();
        }

        function Logout() {
            var b = document.getElementById("LogoutButton");
            if (b != null) {
                b.click();
            }
        }
    </script>
    <style type="text/css">
    .hide {display: none;}
    </style>
    <%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPengumuman" runat="server">--%>
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Login Screen" />
    <div class="row">
        <div style="float:left;width:64px;height:141px;display:block;clear:none;">&nbsp;</div>
        <div style="float:left;display:block;clear:none;">
            <img src="/App_Themes/BMS_Theme/Images/tam-fff.png" height="141" width="150">
        </div>
    </div>
    <div class="row">
        <label>
            User ID
        </label>
        <div class="rowlogin">
            <asp:TextBox runat="server" ID="txtUserID" ValidationGroup="login" Width="155px" ClientIDMode="Static" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqTxtUserID" runat="server" ErrorMessage="*" ControlToValidate="txtUserID"
                ValidationGroup="login"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <label>
            Password
        </label>
        <div class="rowlogin">
            <asp:TextBox runat="server" ID="txtPassword" ValidationGroup="login" TextMode="Password" ClientIDMode="Static"  
                Width="155px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqTxtPassword" runat="server" ErrorMessage="*" ControlToValidate="txtPassword"
                ValidationGroup="login"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row" style="text-align: center;">
        <label>
        </label>
        <div class="rowlogin">
            <asp:Button runat="server"  ID="btnLogin" Text="Login" ValidationGroup="login" OnClick="btnLogin_Click" />
            &nbsp;&nbsp;
            <input type="reset" value="Clear" class="shortButton" onclick="clearLogin()"/>
            
        </div>
        
        <%--<asp:Button runat="server" ID="btnTestLog" onclick="btnTestLog_Click" Text="Log" />--%>
    </div>
    <div class="row" style="text-align: center;">
        <asp:Literal runat="server" ID="litMessage"></asp:Literal>
        <asp:Button ID="LogoutButton" ClientIDMode="Static" runat="server" OnClick="LogoutButton_Click" Height="0" Width="0" CssClass="hide"  />
    </div>
   
</asp:Content>
