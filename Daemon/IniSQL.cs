﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace ElvisWorkflowStatusUpdater
{
    public class IniSQL
    {
        IniSections data = null;
        public IniSQL()
        {
            if (data == null)
            {
                string p;
                p = Path.Combine(Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]), "q.sql");
                Ini ini = new Ini(p);
                data = new IniSections();
                foreach (string s in ini.Sections)
                {
                    data[s] = ini.Section[s];
                }
            }
        }

        public string this[string Index]
        {
            get
            {
                if (data != null)
                    return data[Index];
                else
                    return null;
            }
        }
    }
}
