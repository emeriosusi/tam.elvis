﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ElvisWorkflowStatusUpdater
{
    public class SqlContext
    {
        private SqlCommand command;
        private SqlConnection connection;
        private List<SqlCommand> commands = new List<SqlCommand>();
        private SqlDataReader r;
        private SqlTransaction tx = null;

        public int LastResult = 0;

        public SqlContext(string connectionString)
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
            command = connection.CreateCommand();
        }
        public SqlCommand NewProc(string procName)
        {
            SqlCommand q = new SqlCommand(procName, connection);
            q.CommandType = CommandType.StoredProcedure;
            return q;
        }

        public SqlCommand NewCommand(string sql)
        {
            SqlCommand q = new SqlCommand(sql, connection);
            if (tx != null)
            {
                q.Transaction = tx;
            }
            commands.Add(q);
            return q;
        }

        public SqlCommand Command
        {
            get
            {
                return command;
            }
        }

        public string sql
        {
            set
            {
                command.CommandText = value;
            }
        }

        public SqlParameter AddParam(string paramname, SqlDbType t)
        {
            return command.Parameters.Add(paramname, t);
        }

        public void SetParam(string name, object o)
        {
            command.Parameters[name].Value = o;
        }

        public int Do()
        {
            LastResult = command.ExecuteNonQuery();
            return LastResult;
        }

        public bool Read()
        {
            if (r == null)
            {
                r = command.ExecuteReader();
            }
            return r.Read();
        }

        public void CloseReader()
        {
            if (r != null ) { 
                r.Close();
                
                r = null; 
            }
        }

        public object Value(int col)
        {
            if (r != null && col < r.FieldCount)
            {
                if (!r.IsDBNull(col))
                    return r[col];
            }
            return null;
        }

        public int? NullInt(int col)
        {
            object v = Value(col);
            if (v != null) return Convert.ToInt32(v); else return null;
        }

        public int IntVal(int col, int value = 0)
        {
            object v = Value(col);
            if (v != null) return Convert.ToInt32(v); else return value;
        }

        public void Begin()
        {
            tx = connection.BeginTransaction();
            command.Transaction = tx;
            foreach (SqlCommand c in commands)
            {
                c.Transaction = tx;
            }
        }

        public void Commit()
        {
            if (tx!=null) 
                tx.Commit();
        }

        public void RollBack()
        {
            if (tx != null)
            {
                tx.Rollback();
            }
        }

        public void Close()
        {
            if (connection != null)
            {
                foreach (SqlCommand q in commands)
                    q.Dispose();
                command.Dispose();
                connection.Close();
            }
        }
    }
}
