﻿/*
    @Lufty
    PV Workflow status updater.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ElvisWorkflowStatusUpdater
{
    public class WorkflowUpdater
    {
        public string ConnectionString;
         
        public TimeSpan elapsedTime;
        public int terminated;
        public bool usingThreading;
        public IniSQL Q = null;
        
        public void init()
        {
            ConnectionString =  AppSetting.ConnectionString;
            //stopWatch = Stopwatch.StartNew();
            terminated = 0;
            usingThreading = false;
            Q = new IniSQL();
        }

        public void del()
        {
        }

        public void run()
        {

        }

        public int GetRowCount()
        {
            int rowCount = 0;
            /* Count how much PV on the table */
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);
            sqlConnection.Open();
            SqlCommand sql = sqlConnection.CreateCommand();
            sql.CommandText = Q["COUNT_PV"]; 
            SqlDataReader dataReader = sql.ExecuteReader();
            dataReader.Read();
            rowCount = dataReader.GetInt32(0);
            dataReader.Close();
            sqlConnection.Close();
            return rowCount;
        }

        static void Main(string[] args)
        {
            WorkflowUpdater w = new WorkflowUpdater();

            int doFlag = 0xFF;
            if (args.Length > 0)
            {
                doFlag = args[0].Int(0xFF);
            }

            w.init();
            try
            {
                int dataPartition = 0;
                int rowCount = w.GetRowCount();
                WF.Init();

                if (
                     AppSetting.UseThread
                     && rowCount > AppSetting.ThreadingMinData 
                   )
                {
                    w.usingThreading = true;
                    dataPartition = rowCount / AppSetting.MaxThread;
                }

                if (w.usingThreading)
                {
                    int processorCounter = 1;

                    List<Action> processors = new List<Action>(AppSetting.MaxThread);
                    WF.say("Using {0} thread", AppSetting.MaxThread);
                    WF.say("Document processing started at {0}"
                        , DateTime.Now.ToString()
                        , AppSetting.MaxThread);
                    
                    for (int i = 0; i < 6; i++)
                    {
                        DocumentUpdater proc = new DocumentUpdater(
                            "Thread #" + processorCounter,
                            //0, 0, 
                            w, doFlag & (1 << i));
                        processors.Add(delegate() { proc.Start(); });
                        processorCounter++;
                    }
                    Parallel.Invoke(processors.ToArray<Action>());
                }
                else
                {
                    WF.say("Serial process");
                    DocumentUpdater proc = new DocumentUpdater(
                        "Process",                         
                        w, doFlag);
                    proc.run();

                    
                }
                WF.say("\r\nDone");
            }
            catch (Exception ex)
            {
                WF.err(ex);
            }
            Console.WriteLine("END");
        }
    }
}
